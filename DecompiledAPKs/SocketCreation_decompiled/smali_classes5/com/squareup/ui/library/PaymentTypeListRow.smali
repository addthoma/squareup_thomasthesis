.class public Lcom/squareup/ui/library/PaymentTypeListRow;
.super Lcom/squareup/marin/widgets/BorderedLinearLayout;
.source "PaymentTypeListRow.java"


# instance fields
.field private dragHandle:Lcom/squareup/glyph/SquareGlyphView;

.field private paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/library/PaymentTypeListRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    sget p2, Lcom/squareup/widgets/pos/R$layout;->settings_applet_payment_types_list_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/library/PaymentTypeListRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/library/PaymentTypeListRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 30
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setBorderWidth(I)V

    const/4 p1, 0x1

    .line 32
    invoke-virtual {p0, p1, p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setHorizontalBorders(ZZ)V

    .line 34
    sget p1, Lcom/squareup/widgets/pos/R$id;->drag_handle:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    .line 35
    sget p1, Lcom/squareup/widgets/pos/R$id;->toggle_button_row:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object p1, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/library/PaymentTypeListRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/noho/R$dimen;->noho_row_standard_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 37
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setMinimumHeight(I)V

    const/16 p1, 0x10

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setGravity(I)V

    return-void
.end method

.method private setEmptyRow(I)V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/library/PaymentTypeListRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/library/PaymentTypeListRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    .line 75
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 73
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setButtonEnabled(Z)V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->showButton(Z)V

    return-void
.end method


# virtual methods
.method public getDragHandle()Lcom/squareup/glyph/SquareGlyphView;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 43
    invoke-super {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->onFinishInflate()V

    return-void
.end method

.method public resetRow()V
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->dragHandle:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/ui/library/PaymentTypeListRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setButtonEnabled(Z)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->showButton(Z)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public setEmptyDisabledRow()V
    .locals 1

    .line 67
    sget v0, Lcom/squareup/widgets/pos/R$string;->payment_types_settings_empty_disabled:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/library/PaymentTypeListRow;->setEmptyRow(I)V

    return-void
.end method

.method public setEmptyRow()V
    .locals 1

    .line 63
    sget v0, Lcom/squareup/widgets/pos/R$string;->payment_types_settings_empty:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/library/PaymentTypeListRow;->setEmptyRow(I)V

    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/library/PaymentTypeListRow;->paymentToggleButtonRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
