.class public Lcom/squareup/ui/library/DiscountEntryScreenRunner;
.super Ljava/lang/Object;
.source "DiscountEntryScreenRunner.java"


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public finishDiscountEntryMoney()V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/library/DiscountEntryMoneyScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public finishDiscountEntryPercent()V
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/library/DiscountEntryPercentScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public goToDiscountEntryMoney(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen;-><init>(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/protos/client/Employee;)V

    .line 32
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->flow:Lflow/Flow;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToDiscountEntryPercent(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/ui/library/DiscountEntryPercentScreen;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/library/DiscountEntryPercentScreen;-><init>(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/protos/client/Employee;)V

    .line 21
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->flow:Lflow/Flow;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
