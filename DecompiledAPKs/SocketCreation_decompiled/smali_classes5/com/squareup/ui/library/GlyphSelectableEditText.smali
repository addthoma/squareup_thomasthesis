.class public Lcom/squareup/ui/library/GlyphSelectableEditText;
.super Lcom/squareup/widgets/SelectableEditText;
.source "GlyphSelectableEditText.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/SelectableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->GlyphSelectableEditText:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 19
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->GlyphSelectableEditText_glyph:I

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 20
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    if-eqz p2, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/library/GlyphSelectableEditText;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 23
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/library/GlyphSelectableEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    .line 24
    invoke-virtual {v0, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p2

    .line 25
    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    .line 26
    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p1

    .line 27
    invoke-virtual {p0, p1, v1, v1, v1}, Lcom/squareup/ui/library/GlyphSelectableEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method
