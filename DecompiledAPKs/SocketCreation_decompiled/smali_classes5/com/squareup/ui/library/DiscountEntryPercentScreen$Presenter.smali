.class Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "DiscountEntryPercentScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/DiscountEntryPercentScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter$PercentageEntryKeypadListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/library/DiscountEntryPercentView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;"
        }
    .end annotation
.end field

.field private final discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

.field private final features:Lcom/squareup/settings/server/Features;

.field private listener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final res:Lcom/squareup/util/Res;

.field private state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

.field private final vibrator:Landroid/os/Vibrator;

.field private final workingDiscountBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/library/DiscountEntryScreenRunner;Landroid/os/Vibrator;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/ui/library/DiscountEntryScreenRunner;",
            "Landroid/os/Vibrator;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 81
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 84
    iput-object p3, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    .line 85
    iput-object p4, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->vibrator:Landroid/os/Vibrator;

    .line 86
    iput-object p5, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 87
    iput-object p6, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    .line 88
    iput-object p7, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 89
    iput-object p8, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    .line 90
    iput-object p9, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    .line 91
    iput-object p10, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)Landroid/os/Vibrator;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->vibrator:Landroid/os/Vibrator;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;Z)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->displayPercentage(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private displayPercentage(Z)V
    .locals 4

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    invoke-static {v0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$500(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "0"

    .line 126
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/library/DiscountEntryPercentView;

    iget-object v2, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/configure/item/R$string;->percent:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "content"

    .line 127
    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 128
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 126
    invoke-virtual {v1, v2}, Lcom/squareup/ui/library/DiscountEntryPercentView;->setAmountText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    .line 130
    invoke-static {v1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$600(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Lcom/squareup/util/Percentage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/util/Percentage;->isPositive()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 129
    :goto_0
    invoke-direct {p0, v2}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->updateDiscountValueColor(Z)V

    return-void
.end method

.method private updateDiscountValueColor(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryPercentView;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/DiscountEntryPercentView;->setAmountColor(I)V

    goto :goto_0

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryPercentView;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/DiscountEntryPercentView;->setAmountColor(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public onCancelSelected()V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->finishDiscountEntryPercent()V

    return-void
.end method

.method public onCommitSelected()Z
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_PERCENT:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    .line 143
    invoke-static {v2}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$700(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Lcom/squareup/configure/item/WorkingDiscount;

    move-result-object v2

    .line 142
    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingDiscount;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$800(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;Z)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->finishDiscountEntryPercent()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 95
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryPercentScreen;

    .line 96
    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen;->access$000(Lcom/squareup/ui/library/DiscountEntryPercentScreen;)Lcom/squareup/configure/item/WorkingDiscount;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen;->access$000(Lcom/squareup/ui/library/DiscountEntryPercentScreen;)Lcom/squareup/configure/item/WorkingDiscount;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    .line 98
    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen;->access$100(Lcom/squareup/ui/library/DiscountEntryPercentScreen;)Lcom/squareup/protos/client/Employee;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    .line 97
    invoke-static {v0, v1, v2, p1, v3}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->buildLoadedWorkingDiscountState(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/BundleKey;Lcom/squareup/protos/client/Employee;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    goto :goto_0

    .line 100
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->buildEmptyWorkingDiscountState(Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 106
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    invoke-static {v0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$200(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;Landroid/os/Bundle;)V

    .line 109
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    invoke-static {v1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$300(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/library/-$$Lambda$eWEQTZuxvcgYRUZZnC0XdUSyHu8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/-$$Lambda$eWEQTZuxvcgYRUZZnC0XdUSyHu8;-><init>(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->apply:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/library/-$$Lambda$phQKph0LTll4qSxM7yAY7UbjeQU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/-$$Lambda$phQKph0LTll4qSxM7yAY7UbjeQU;-><init>(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 113
    new-instance p1, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter$PercentageEntryKeypadListener;

    invoke-direct {p1, p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter$PercentageEntryKeypadListener;-><init>(Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;)V

    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->listener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    const/4 p1, 0x1

    .line 114
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->displayPercentage(Z)V

    .line 115
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryPercentView;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->listener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/DiscountEntryPercentView;->setListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    invoke-static {v0, p1}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$400(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;Landroid/os/Bundle;)V

    .line 120
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method onStartVisualTransition()V
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_PERCENT:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/ui/library/DiscountEntryPercentScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;

    .line 156
    invoke-static {v2}, Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;->access$700(Lcom/squareup/ui/library/DiscountEntryPercentScreen$PercentWorkingDiscountState;)Lcom/squareup/configure/item/WorkingDiscount;

    move-result-object v2

    .line 155
    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingDiscount;)V

    return-void
.end method
