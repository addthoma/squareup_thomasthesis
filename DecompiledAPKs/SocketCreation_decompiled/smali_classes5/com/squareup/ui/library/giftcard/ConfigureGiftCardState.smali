.class Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;
.super Ljava/lang/Object;
.source "ConfigureGiftCardState.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field transient swipedCard:Lcom/squareup/Card;

.field workingItem:Lcom/squareup/configure/item/WorkingItem;

.field private final workingItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/BundleKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    return-void
.end method

.method static empty(Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;)",
            "Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;-><init>(Lcom/squareup/BundleKey;)V

    return-object v0
.end method

.method static loaded(Lcom/squareup/BundleKey;Lcom/squareup/configure/item/WorkingItem;)Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;",
            "Lcom/squareup/configure/item/WorkingItem;",
            ")",
            "Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;-><init>(Lcom/squareup/BundleKey;)V

    .line 32
    iput-object p1, v0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    return-object v0
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 37
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/library/giftcard/ConfigureGiftCardState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method
