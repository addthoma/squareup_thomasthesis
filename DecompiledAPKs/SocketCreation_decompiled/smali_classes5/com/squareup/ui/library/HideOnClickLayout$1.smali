.class Lcom/squareup/ui/library/HideOnClickLayout$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "HideOnClickLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/library/HideOnClickLayout;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/library/HideOnClickLayout;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/HideOnClickLayout;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout$1;->this$0:Lcom/squareup/ui/library/HideOnClickLayout;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 34
    iget-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout$1;->this$0:Lcom/squareup/ui/library/HideOnClickLayout;

    invoke-static {p1}, Lcom/squareup/ui/library/HideOnClickLayout;->access$000(Lcom/squareup/ui/library/HideOnClickLayout;)Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 35
    iget-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout$1;->this$0:Lcom/squareup/ui/library/HideOnClickLayout;

    invoke-static {p1}, Lcom/squareup/ui/library/HideOnClickLayout;->access$100(Lcom/squareup/ui/library/HideOnClickLayout;)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout$1;->this$0:Lcom/squareup/ui/library/HideOnClickLayout;

    invoke-static {p1}, Lcom/squareup/ui/library/HideOnClickLayout;->access$200(Lcom/squareup/ui/library/HideOnClickLayout;)Lcom/squareup/util/NonDebouncedOnClickListener;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/library/HideOnClickLayout$1;->this$0:Lcom/squareup/ui/library/HideOnClickLayout;

    invoke-static {p1}, Lcom/squareup/ui/library/HideOnClickLayout;->access$200(Lcom/squareup/ui/library/HideOnClickLayout;)Lcom/squareup/util/NonDebouncedOnClickListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/library/HideOnClickLayout$1;->this$0:Lcom/squareup/ui/library/HideOnClickLayout;

    invoke-static {v0}, Lcom/squareup/ui/library/HideOnClickLayout;->access$100(Lcom/squareup/ui/library/HideOnClickLayout;)Landroid/view/View;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/util/NonDebouncedOnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method
