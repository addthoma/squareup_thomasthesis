.class public Lcom/squareup/ui/library/UploadItemBitmapTask;
.super Ljava/lang/Object;
.source "UploadItemBitmapTask.java"

# interfaces
.implements Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field public final bitmapFile:Lcom/squareup/ui/library/UploadItem;

.field transient bus:Lcom/squareup/badbus/BadEventSink;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient cogs:Lcom/squareup/cogs/Cogs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient fileThreadExecutor:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient imageService:Lcom/squareup/server/ImageService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final itemId:Ljava/lang/String;

.field transient itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/ui/library/UploadItem;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->itemId:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bitmapFile:Lcom/squareup/ui/library/UploadItem;

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/squareup/server/SquareCallback;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemImage;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bitmapFile:Lcom/squareup/ui/library/UploadItem;

    invoke-virtual {v1}, Lcom/squareup/ui/library/UploadItem;->getFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UploadItemBitmapTask could not find photo file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bitmapFile:Lcom/squareup/ui/library/UploadItem;

    .line 52
    invoke-virtual {v2}, Lcom/squareup/ui/library/UploadItem;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    .line 51
    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 53
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void

    .line 57
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bitmapFile:Lcom/squareup/ui/library/UploadItem;

    invoke-virtual {v1}, Lcom/squareup/ui/library/UploadItem;->getMimeType()Lokhttp3/MediaType;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bitmapFile:Lcom/squareup/ui/library/UploadItem;

    invoke-virtual {v2}, Lcom/squareup/ui/library/UploadItem;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-static {v1, v2}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/io/File;)Lokhttp3/RequestBody;

    move-result-object v1

    .line 58
    iget-object v2, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bitmapFile:Lcom/squareup/ui/library/UploadItem;

    .line 59
    invoke-virtual {v2}, Lcom/squareup/ui/library/UploadItem;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "image"

    invoke-static {v3, v2, v1}, Lokhttp3/MultipartBody$Part;->createFormData(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Part;

    move-result-object v1

    .line 61
    iget-object v2, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->imageService:Lcom/squareup/server/ImageService;

    .line 62
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/squareup/server/ImageService;->upload(Ljava/lang/String;Lokhttp3/MultipartBody$Part;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$6WTa9_MfRxM7wp4Nejuk2GFuMrI;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$6WTa9_MfRxM7wp4Nejuk2GFuMrI;-><init>(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/shared/catalog/models/CatalogItemImage;)V

    .line 64
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/UploadItemBitmapTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 0

    .line 102
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/ui/library/UploadItemBitmapTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/UploadItemBitmapTask;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public synthetic lambda$execute$4$UploadItemBitmapTask(Lcom/squareup/server/SquareCallback;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 64
    new-instance v0, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$J9TE2UmjH5eXAT5DmtvjwZI7O4Q;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$J9TE2UmjH5eXAT5DmtvjwZI7O4Q;-><init>(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/shared/catalog/models/CatalogItemImage;)V

    new-instance p2, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$Ks9fVEmNZef1Kb_r6hDcj1BVmDE;

    invoke-direct {p2, p1}, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$Ks9fVEmNZef1Kb_r6hDcj1BVmDE;-><init>(Lcom/squareup/server/SquareCallback;)V

    invoke-virtual {p3, v0, p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$0$UploadItemBitmapTask(Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/server/ItemImageUploadResponse;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->itemId:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->readItems(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->itemId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 77
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->builder()Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;

    move-result-object p1

    iget-object p2, p2, Lcom/squareup/server/ItemImageUploadResponse;->image_url:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->setUrl(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemImage$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemImage;

    move-result-object p1

    .line 78
    new-instance p2, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-direct {p2, v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;)V

    .line 79
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setImageId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObject;

    const/4 v2, 0x0

    .line 81
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object p2

    aput-object p2, v0, v2

    const/4 p2, 0x1

    aput-object p1, v0, p2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p3, p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v1
.end method

.method public synthetic lambda$null$1$UploadItemBitmapTask(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->clearOverride(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bitmapFile:Lcom/squareup/ui/library/UploadItem;

    invoke-virtual {v0}, Lcom/squareup/ui/library/UploadItem;->getFile()Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, Lcom/squareup/util/Files;->deleteSilently(Ljava/io/File;Ljava/util/concurrent/Executor;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/CatalogCallback;->call(Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method

.method public synthetic lambda$null$2$UploadItemBitmapTask(Lcom/squareup/server/SquareCallback;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/server/ItemImageUploadResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 66
    invoke-virtual {p1, p3}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->itemId:Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v0, p1, v1

    iget-object v0, p3, Lcom/squareup/server/ItemImageUploadResponse;->image_url:Ljava/lang/String;

    const/4 v1, 0x1

    aput-object v0, p1, v1

    const-string v0, "ImageService: Uploaded item image (id=%s) %s"

    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->bus:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;

    iget-object v1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->itemId:Ljava/lang/String;

    iget-object v2, p3, Lcom/squareup/server/ItemImageUploadResponse;->image_url:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/library/UploadItemBitmapTask;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v0, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$WPUp6fLlnBKAf7mNTh4R7j4IlRs;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$WPUp6fLlnBKAf7mNTh4R7j4IlRs;-><init>(Lcom/squareup/ui/library/UploadItemBitmapTask;Lcom/squareup/shared/catalog/models/CatalogItemImage;Lcom/squareup/server/ItemImageUploadResponse;)V

    new-instance p2, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$c7vaVU3V6bZjs0Ab6PC0wRqraWY;

    invoke-direct {p2, p0}, Lcom/squareup/ui/library/-$$Lambda$UploadItemBitmapTask$c7vaVU3V6bZjs0Ab6PC0wRqraWY;-><init>(Lcom/squareup/ui/library/UploadItemBitmapTask;)V

    invoke-interface {p1, v0, p2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method
