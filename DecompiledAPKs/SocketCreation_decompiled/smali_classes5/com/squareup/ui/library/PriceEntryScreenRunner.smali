.class public Lcom/squareup/ui/library/PriceEntryScreenRunner;
.super Ljava/lang/Object;
.source "PriceEntryScreenRunner.java"


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreenRunner;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public finishPriceEntry()V
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreenRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/library/PriceEntryScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public goToPriceEntry(Lcom/squareup/configure/item/WorkingItem;)V
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreenRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/library/PriceEntryScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/library/PriceEntryScreen;-><init>(Lcom/squareup/configure/item/WorkingItem;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
