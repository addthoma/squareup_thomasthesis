.class synthetic Lcom/squareup/ui/LoadMoreRow$1;
.super Ljava/lang/Object;
.source "LoadMoreRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/LoadMoreRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$LoadMoreState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 31
    invoke-static {}, Lcom/squareup/ui/LoadMoreState;->values()[Lcom/squareup/ui/LoadMoreState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/LoadMoreRow$1;->$SwitchMap$com$squareup$ui$LoadMoreState:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/LoadMoreRow$1;->$SwitchMap$com$squareup$ui$LoadMoreState:[I

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    invoke-virtual {v1}, Lcom/squareup/ui/LoadMoreState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/LoadMoreRow$1;->$SwitchMap$com$squareup$ui$LoadMoreState:[I

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->NO_CONNECTION_RETRY:Lcom/squareup/ui/LoadMoreState;

    invoke-virtual {v1}, Lcom/squareup/ui/LoadMoreState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/LoadMoreRow$1;->$SwitchMap$com$squareup$ui$LoadMoreState:[I

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->LOADING:Lcom/squareup/ui/LoadMoreState;

    invoke-virtual {v1}, Lcom/squareup/ui/LoadMoreState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
