.class public Lcom/squareup/ui/help/orders/OrdersCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrdersCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private orderHistoryRow:Landroid/view/View;

.field private orderLegacyReaderRow:Landroid/view/View;

.field private orderReaderRow:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 57
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 58
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 59
    sget v0, Lcom/squareup/applet/help/R$id;->order_reader_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderReaderRow:Landroid/view/View;

    .line 60
    sget v0, Lcom/squareup/applet/help/R$id;->order_legacy_reader_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderLegacyReaderRow:Landroid/view/View;

    .line 61
    sget v0, Lcom/squareup/applet/help/R$id;->order_history_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderHistoryRow:Landroid/view/View;

    return-void
.end method

.method private getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    sget v1, Lcom/squareup/applet/help/R$string;->orders:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/orders/OrdersCoordinator;->bindViews(Landroid/view/View;)V

    .line 33
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/ui/help/orders/OrdersCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 37
    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderReaderRow:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$Xtj9Or0aiCwm060NBmFbHxzdhIA;

    invoke-direct {v2, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$Xtj9Or0aiCwm060NBmFbHxzdhIA;-><init>(Lcom/squareup/ui/help/orders/OrdersCoordinator;)V

    .line 38
    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    .line 37
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderLegacyReaderRow:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$eeomfUYY920aOlRO8lMv3TpAGEE;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$eeomfUYY920aOlRO8lMv3TpAGEE;-><init>(Lcom/squareup/ui/help/orders/OrdersCoordinator;Landroid/content/Context;)V

    .line 40
    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 39
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderHistoryRow:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$cD-xHNg4Z8_b0lyu1vlVQLXUBG8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$cD-xHNg4Z8_b0lyu1vlVQLXUBG8;-><init>(Lcom/squareup/ui/help/orders/OrdersCoordinator;)V

    .line 42
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    new-instance v0, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$EU-u-5jpRDwSSOfGFDnd5Fl2BGg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$EU-u-5jpRDwSSOfGFDnd5Fl2BGg;-><init>(Lcom/squareup/ui/help/orders/OrdersCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$OrdersCoordinator(Landroid/view/View;)V
    .locals 0

    .line 38
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->orderReader()V

    return-void
.end method

.method public synthetic lambda$attach$1$OrdersCoordinator(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    .line 40
    iget-object p2, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->orderLegacyReader(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic lambda$attach$2$OrdersCoordinator(Landroid/view/View;)V
    .locals 0

    .line 42
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->orderHistory()V

    return-void
.end method

.method public synthetic lambda$attach$4$OrdersCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->ordersScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$IarjKs6s-famixzqHs_Q76xF4KI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrdersCoordinator$IarjKs6s-famixzqHs_Q76xF4KI;-><init>(Lcom/squareup/ui/help/orders/OrdersCoordinator;)V

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$3$OrdersCoordinator(Lcom/squareup/ui/help/orders/OrdersScreenData;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderReaderRow:Landroid/view/View;

    iget-boolean v1, p1, Lcom/squareup/ui/help/orders/OrdersScreenData;->showOrderReader:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderLegacyReaderRow:Landroid/view/View;

    iget-boolean v1, p1, Lcom/squareup/ui/help/orders/OrdersScreenData;->showOrderLegacyReader:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrdersCoordinator;->orderHistoryRow:Landroid/view/View;

    iget-boolean p1, p1, Lcom/squareup/ui/help/orders/OrdersScreenData;->showOrderHistory:Z

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
