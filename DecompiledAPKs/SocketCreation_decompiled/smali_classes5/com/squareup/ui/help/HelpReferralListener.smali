.class Lcom/squareup/ui/help/HelpReferralListener;
.super Ljava/lang/Object;
.source "HelpReferralListener.java"

# interfaces
.implements Lcom/squareup/referrals/ReferralListener;


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/help/HelpReferralListener;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public onLoadFailure()V
    .locals 5

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/help/HelpReferralListener;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    sget v3, Lcom/squareup/pinpad/R$string;->please_try_again:I

    sget v4, Lcom/squareup/applet/help/R$string;->please_check_your_network_connection_try_again:I

    invoke-direct {v2, v3, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onUserDismissed()V
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/help/HelpReferralListener;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method
