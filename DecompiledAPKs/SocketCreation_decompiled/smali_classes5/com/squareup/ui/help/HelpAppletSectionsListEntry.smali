.class public abstract Lcom/squareup/ui/help/HelpAppletSectionsListEntry;
.super Lcom/squareup/applet/AppletSectionsListEntry;
.source "HelpAppletSectionsListEntry.java"


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    return-void
.end method


# virtual methods
.method public badgeCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 16
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
