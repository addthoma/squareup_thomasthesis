.class public final Lcom/squareup/ui/help/HelpAppletScopeRunner;
.super Ljava/lang/Object;
.source "HelpAppletScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;
.implements Lcom/squareup/ui/help/help/HelpScreen$Runner;


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final applet:Lcom/squareup/ui/help/HelpApplet;

.field private final bannerButtonResolver:Lcom/squareup/onboarding/BannerButtonResolver;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final createItemTutorialRunner:Lcom/squareup/items/tutorial/CreateItemTutorialRunner;

.field private final deepLinks:Lcom/squareup/ui/main/DeepLinks;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

.field private final flow:Lflow/Flow;

.field private final helpAppletSettings:Lcom/squareup/ui/help/api/HelpAppletSettings;

.field private final helpBadge:Lcom/squareup/ui/help/HelpBadge;

.field private final helpContentStaticLinks:Lcom/squareup/ui/help/api/HelpContentStaticLinks;

.field private final helpSection:Lcom/squareup/ui/help/help/HelpSection;

.field private final invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

.field private final jumbotronMessageProducer:Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

.field private final messagingController:Lcom/squareup/ui/help/MessagingController;

.field private final onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

.field private final ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final r12TutorialLauncher:Lcom/squareup/ui/main/R12ForceableContentLauncher;

.field private final r6TutorialLauncher:Lcom/squareup/ui/main/R6ForceableContentLauncher;

.field private final settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

.field private final tour:Lcom/squareup/tour/Tour;

.field private final tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

.field private final tutorialsBadge:Lcom/squareup/ui/help/tutorials/TutorialsBadge;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/ui/help/tutorials/TutorialsBadge;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/ui/help/orders/OrdersVisibility;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/ui/main/R6ForceableContentLauncher;Lcom/squareup/ui/main/R12ForceableContentLauncher;Lcom/squareup/items/tutorial/CreateItemTutorialRunner;Lcom/squareup/ui/settings/SettingsAppletGateway;Lcom/squareup/ui/help/api/HelpContentStaticLinks;Lcom/squareup/ui/help/api/HelpAppletSettings;Lcom/squareup/onboarding/BannerButtonResolver;Lcom/squareup/tour/Tour;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/feetutorial/FeeTutorial;Lcom/squareup/settings/server/Features;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->applet:Lcom/squareup/ui/help/HelpApplet;

    move-object v1, p2

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    move-object v1, p3

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->device:Lcom/squareup/util/Device;

    move-object v1, p4

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    move-object v1, p5

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p6

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p7

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

    move-object v1, p8

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->jumbotronMessageProducer:Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    move-object v1, p9

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    move-object v1, p10

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

    move-object v1, p11

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->tutorialsBadge:Lcom/squareup/ui/help/tutorials/TutorialsBadge;

    move-object v1, p12

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    move-object v1, p13

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    move-object/from16 v1, p14

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

    move-object/from16 v1, p15

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    move-object/from16 v1, p16

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->r6TutorialLauncher:Lcom/squareup/ui/main/R6ForceableContentLauncher;

    move-object/from16 v1, p17

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->r12TutorialLauncher:Lcom/squareup/ui/main/R12ForceableContentLauncher;

    move-object/from16 v1, p18

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->createItemTutorialRunner:Lcom/squareup/items/tutorial/CreateItemTutorialRunner;

    move-object/from16 v1, p19

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    move-object/from16 v1, p20

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpContentStaticLinks:Lcom/squareup/ui/help/api/HelpContentStaticLinks;

    move-object/from16 v1, p21

    .line 165
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpAppletSettings:Lcom/squareup/ui/help/api/HelpAppletSettings;

    move-object/from16 v1, p22

    .line 166
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->bannerButtonResolver:Lcom/squareup/onboarding/BannerButtonResolver;

    move-object/from16 v1, p23

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->tour:Lcom/squareup/tour/Tour;

    move-object/from16 v1, p24

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object/from16 v1, p25

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->messagingController:Lcom/squareup/ui/help/MessagingController;

    move-object/from16 v1, p26

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpSection:Lcom/squareup/ui/help/help/HelpSection;

    move-object/from16 v1, p27

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    move-object/from16 v1, p28

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/help/HelpAppletScopeRunner;)Lcom/squareup/onboarding/OnboardingStarter;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->onboardingStarter:Lcom/squareup/onboarding/OnboardingStarter;

    return-object p0
.end method

.method static synthetic lambda$announcementDetailsScreenData$2(Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;Ljava/util/List;)Lio/reactivex/ObservableSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 434
    invoke-static {p1}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/help/-$$Lambda$HelpAppletScopeRunner$G0gAoXEWstb4FGrXgmTWTETKcpE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/-$$Lambda$HelpAppletScopeRunner$G0gAoXEWstb4FGrXgmTWTETKcpE;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;)V

    .line 435
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p0

    sget-object p1, Lcom/squareup/ui/help/-$$Lambda$YXFXmZegBoFKqL0F8UVX1BgOSfI;->INSTANCE:Lcom/squareup/ui/help/-$$Lambda$YXFXmZegBoFKqL0F8UVX1BgOSfI;

    .line 436
    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$1(Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;Lcom/squareup/server/messages/Message;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 435
    iget-object p1, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;->announcementId:Ljava/lang/String;

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method private startOrderMagstripeWorkflow()V
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;->INSTANCE:Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public aboutScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/about/AboutScreenData;",
            ">;"
        }
    .end annotation

    .line 508
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpAppletSettings:Lcom/squareup/ui/help/api/HelpAppletSettings;

    invoke-interface {v0}, Lcom/squareup/ui/help/api/HelpAppletSettings;->aboutVersionString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/-$$Lambda$5YTGyoqxeGWzvcw-wXraZwdCq3o;->INSTANCE:Lcom/squareup/ui/help/-$$Lambda$5YTGyoqxeGWzvcw-wXraZwdCq3o;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public adjustPointsLoyaltyTutorialClicked()V
    .locals 3

    .line 351
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;

    sget-object v2, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ADJUST_POINTS:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    invoke-direct {v1, v2}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;-><init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 352
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    invoke-interface {v0}, Lcom/squareup/ui/help/HelpBadge;->refresh()V

    return-void
.end method

.method public announcementButtonClicked(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 441
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/help/announcements/events/TapAnnouncementButtonEvent;

    invoke-direct {v0, p2, p3}, Lcom/squareup/ui/help/announcements/events/TapAnnouncementButtonEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 442
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/main/DeepLinks;->handleInternalDeepLink(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 444
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {p1, p3}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public announcementDetailsScreenData(Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreenData;",
            ">;"
        }
    .end annotation

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->jumbotronMessageProducer:Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    invoke-virtual {v0}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/-$$Lambda$HelpAppletScopeRunner$Ut_JDzgWzPTjUjwbe5k_oYWbgZ8;

    invoke-direct {v1, p1}, Lcom/squareup/ui/help/-$$Lambda$HelpAppletScopeRunner$Ut_JDzgWzPTjUjwbe5k_oYWbgZ8;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;)V

    .line 434
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public announcementSelected(Lcom/squareup/server/messages/Message;)V
    .locals 3

    .line 419
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->jumbotronMessageProducer:Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->markMessageRead(Lcom/squareup/server/messages/Message;)V

    .line 420
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/help/announcements/events/ViewAnnouncementDetailEvent;

    invoke-direct {v1, p1}, Lcom/squareup/ui/help/announcements/events/ViewAnnouncementDetailEvent;-><init>(Lcom/squareup/server/messages/Message;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 421
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/help/announcements/events/LegacyViewMessageDetailEvent;

    invoke-direct {v1, p1}, Lcom/squareup/ui/help/announcements/events/LegacyViewMessageDetailEvent;-><init>(Lcom/squareup/server/messages/Message;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/help/announcements/AnnouncementsScreen;

    new-instance v2, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;

    iget-object p1, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    invoke-direct {v2, p1}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    .line 425
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-nez p1, :cond_0

    .line 427
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    :cond_0
    return-void
.end method

.method public announcementsScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/announcements/AnnouncementsScreenData;",
            ">;"
        }
    .end annotation

    .line 402
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->jumbotronMessageProducer:Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    invoke-virtual {v0}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/-$$Lambda$HelpAppletScopeRunner$vLvknCCXixLIMIXWIyTlKtkOfEc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/-$$Lambda$HelpAppletScopeRunner$vLvknCCXixLIMIXWIyTlKtkOfEc;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V

    .line 403
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bannerButtonObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/onboarding/BannerButton;",
            ">;"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    new-instance v0, Lcom/squareup/onboarding/BannerButton;

    sget-object v1, Lcom/squareup/onboarding/BannerButton$Kind;->NONE:Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/BannerButton;-><init>(Lcom/squareup/onboarding/BannerButton$Kind;)V

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->bannerButtonResolver:Lcom/squareup/onboarding/BannerButtonResolver;

    invoke-virtual {v0}, Lcom/squareup/onboarding/BannerButtonResolver;->bannerButton()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public createItemTutorialClicked()V
    .locals 1

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->createItemTutorialRunner:Lcom/squareup/items/tutorial/CreateItemTutorialRunner;

    invoke-interface {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialRunner;->showCreateItemTutorial()V

    return-void
.end method

.method public enrollLoyaltyTutorialClicked()V
    .locals 3

    .line 346
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;

    sget-object v2, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ENROLL:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    invoke-direct {v1, v2}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;-><init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    invoke-interface {v0}, Lcom/squareup/ui/help/HelpBadge;->refresh()V

    return-void
.end method

.method public feeTutorialClicked()V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    invoke-interface {v0}, Lcom/squareup/feetutorial/FeeTutorial;->activate()V

    return-void
.end method

.method public firstInvoiceTutorialClicked()V
    .locals 1

    .line 338
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->forceStartFirstInvoiceTutorial()V

    return-void
.end method

.method public firstPaymentTutorialClicked()V
    .locals 2

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SUPPORT_PAYMENT_TUTORIAL_MANUALLY_STARTED:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SUPPORT_CASH_PAYMENT_TUTORIAL_MANUALLY_STARTED:Lcom/squareup/analytics/RegisterActionName;

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialApi;->forceStartFirstPaymentTutorial()V

    return-void
.end method

.method public getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 1

    const/4 v0, 0x0

    .line 224
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method public getActionBarConfig(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 0

    .line 252
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfigBuilder(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method public getActionBarConfigBuilder(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 240
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    if-nez p2, :cond_1

    .line 241
    iget-object p2, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {p2}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 246
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setTitleNoUpButton(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_1

    .line 242
    :cond_1
    :goto_0
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 243
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/help/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v1, p2}, Lcom/squareup/ui/help/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 244
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    :goto_1
    return-object v0
.end method

.method public helpContentClicked(Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;)V
    .locals 2

    .line 212
    iget v0, p2, Lcom/squareup/ui/help/HelpAppletContent;->titleStringId:I

    sget v1, Lcom/squareup/applet/help/R$string;->change_bank_account:I

    if-ne v0, v1, :cond_0

    .line 214
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-interface {p1}, Lcom/squareup/ui/settings/SettingsAppletGateway;->activateBankAccountSettings()V

    .line 215
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_BANK_ACCOUNT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    goto :goto_0

    .line 217
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p2, Lcom/squareup/ui/help/HelpAppletContent;->linkStringId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    .line 219
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object p2, p2, Lcom/squareup/ui/help/HelpAppletContent;->registerTapName:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    :goto_0
    return-void
.end method

.method public helpScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/help/HelpScreenData;",
            ">;"
        }
    .end annotation

    .line 204
    new-instance v0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/help/help/HelpScreenData$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpContentStaticLinks:Lcom/squareup/ui/help/api/HelpContentStaticLinks;

    .line 205
    invoke-interface {v1}, Lcom/squareup/ui/help/api/HelpContentStaticLinks;->forLearnMore()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->setLearnMoreContentContent(Ljava/util/List;)Lcom/squareup/ui/help/help/HelpScreenData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpContentStaticLinks:Lcom/squareup/ui/help/api/HelpContentStaticLinks;

    .line 206
    invoke-interface {v1}, Lcom/squareup/ui/help/api/HelpContentStaticLinks;->forFrequentlyAskedQuestions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->setFrequentlyAskedQuestionsContent(Ljava/util/List;)Lcom/squareup/ui/help/help/HelpScreenData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpContentStaticLinks:Lcom/squareup/ui/help/api/HelpContentStaticLinks;

    .line 207
    invoke-interface {v1}, Lcom/squareup/ui/help/api/HelpContentStaticLinks;->forTroubleShooting()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->setTroubleshootingContent(Ljava/util/List;)Lcom/squareup/ui/help/help/HelpScreenData$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->build()Lcom/squareup/ui/help/help/HelpScreenData;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$announcementsScreenData$0$HelpAppletScopeRunner(Ljava/util/List;)Lcom/squareup/ui/help/announcements/AnnouncementsScreenData;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 406
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->MESSAGE_CENTER_LIST:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    goto :goto_0

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/help/announcements/events/ViewAnnouncementListEvent;

    invoke-direct {v1, p1}, Lcom/squareup/ui/help/announcements/events/ViewAnnouncementListEvent;-><init>(Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 410
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/help/announcements/events/LegacyViewMessageListEvent;

    invoke-direct {v1, p1}, Lcom/squareup/ui/help/announcements/events/LegacyViewMessageListEvent;-><init>(Ljava/util/List;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 413
    :goto_0
    new-instance v0, Lcom/squareup/ui/help/announcements/AnnouncementsScreenData;

    invoke-direct {v0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementsScreenData;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public launchMessagingActivityOverHelpApplet()V
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpSection:Lcom/squareup/ui/help/help/HelpSection;

    invoke-virtual {v1}, Lcom/squareup/ui/help/help/HelpSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->messagingController:Lcom/squareup/ui/help/MessagingController;

    invoke-interface {v0}, Lcom/squareup/ui/help/MessagingController;->launchMessagingActivity()V

    return-void
.end method

.method public launchTrack(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1

    .line 481
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->HELP_ORDER_HISTORY_TRACK:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 482
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {p1, p2}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method public librariesClicked()V
    .locals 3

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/help/about/AboutScreen;

    sget-object v2, Lcom/squareup/ui/help/about/LibrariesScreen;->INSTANCE:Lcom/squareup/ui/help/about/LibrariesScreen;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public librariesScreenData()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/help/about/LibrariesScreenData;",
            ">;"
        }
    .end annotation

    .line 273
    new-instance v0, Lcom/squareup/ui/help/about/LibrariesScreenData;

    sget-object v1, Lcom/squareup/ui/help/about/AndroidLibraries;->INSTANCE:Lcom/squareup/ui/help/about/AndroidLibraries;

    .line 274
    invoke-virtual {v1}, Lcom/squareup/ui/help/about/AndroidLibraries;->getLibrariesUsed()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpAppletSettings:Lcom/squareup/ui/help/api/HelpAppletSettings;

    .line 275
    invoke-interface {v2}, Lcom/squareup/ui/help/api/HelpAppletSettings;->canClickLibraryLinks()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/help/about/LibrariesScreenData;-><init>(Ljava/util/List;Z)V

    .line 276
    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public libraryClicked(Ljava/lang/String;)V
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method public licenseClicked(Ljava/lang/String;)V
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method public ntepClicked()V
    .locals 3

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/help/about/AboutScreen;

    sget-object v2, Lcom/squareup/ui/help/about/NtepScreen;->INSTANCE:Lcom/squareup/ui/help/about/NtepScreen;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public onBannerClicked(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    .line 195
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->onboardingClicked()V

    goto :goto_0

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/DeepLinks;->handleInternalDeepLink(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->applet:Lcom/squareup/ui/help/HelpApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/help/HelpApplet;->select()V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    invoke-interface {v0}, Lcom/squareup/ui/help/HelpBadge;->newTutorials()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->tutorialsBadge:Lcom/squareup/ui/help/tutorials/TutorialsBadge;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/help/-$$Lambda$THkmEpXo8h9SY-KoRZvMvxRoqk0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/help/-$$Lambda$THkmEpXo8h9SY-KoRZvMvxRoqk0;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsBadge;)V

    .line 179
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 178
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitOrderHistory()V
    .locals 1

    .line 477
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onOrderReaderFromHistory(Landroid/app/Activity;)V
    .locals 4

    .line 490
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

    invoke-virtual {p1}, Lcom/squareup/ui/help/orders/OrdersVisibility;->getReaderScreenType()Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    move-result-object p1

    .line 491
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    .line 502
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderHardwareUrl()Ljava/lang/String;

    move-result-object p1

    .line 503
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void

    .line 496
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/help/orders/OrderHistoryScreen;

    aput-object v2, v1, v0

    invoke-static {p1, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 497
    invoke-direct {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->startOrderMagstripeWorkflow()V

    return-void

    .line 493
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v2, Lcom/squareup/ui/help/orders/OrderHardwareScreen;->INSTANCE:Lcom/squareup/ui/help/orders/OrderHardwareScreen;

    new-array v1, v1, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/help/orders/OrderHistoryScreen;

    aput-object v3, v1, v0

    invoke-static {p1, v2, v1}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public onboardingClicked()V
    .locals 3

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->CREATE_BANK_ACCOUNT:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/help/HelpAppletScopeRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner$1;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public orderHistory()V
    .locals 3

    .line 473
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/help/orders/OrdersScreen;

    sget-object v2, Lcom/squareup/ui/help/orders/OrderHistoryScreen;->INSTANCE:Lcom/squareup/ui/help/orders/OrderHistoryScreen;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public orderLegacyReader(Landroid/content/Context;)V
    .locals 1

    .line 467
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_REORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 468
    iget-object p1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 469
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderHardwareUrl()Ljava/lang/String;

    move-result-object v0

    .line 468
    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method public orderReader()V
    .locals 3

    .line 458
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

    invoke-virtual {v0}, Lcom/squareup/ui/help/orders/OrdersVisibility;->getReaderScreenType()Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->HARDWARE:Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    if-ne v0, v1, :cond_0

    .line 460
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/help/orders/OrdersScreen;

    sget-object v2, Lcom/squareup/ui/help/orders/OrderHardwareScreen;->INSTANCE:Lcom/squareup/ui/help/orders/OrderHardwareScreen;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goFromTo(Lflow/Flow;Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_0

    .line 462
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->startOrderMagstripeWorkflow()V

    :goto_0
    return-void
.end method

.method public ordersScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/orders/OrdersScreenData;",
            ">;"
        }
    .end annotation

    .line 449
    new-instance v0, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

    .line 450
    invoke-virtual {v1}, Lcom/squareup/ui/help/orders/OrdersVisibility;->showOrderReader()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderReader(Z)Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

    .line 451
    invoke-virtual {v1}, Lcom/squareup/ui/help/orders/OrdersVisibility;->showOrderReaderLegacy()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderLegacyReader(Z)Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

    .line 452
    invoke-virtual {v1}, Lcom/squareup/ui/help/orders/OrdersVisibility;->showOrderHistory()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->showOrderHistory(Z)Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;

    move-result-object v0

    .line 454
    invoke-virtual {v0}, Lcom/squareup/ui/help/orders/OrdersScreenData$Builder;->build()Lcom/squareup/ui/help/orders/OrdersScreenData;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public privacyPolicyClicked()V
    .locals 2

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 299
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/SupportUrlSettings;->getPrivacyPolicyUrl()Ljava/lang/String;

    move-result-object v1

    .line 298
    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_PRIVACY_POLICY:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public r12TutorialClicked()V
    .locals 4

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->R12_MEET_THE_READER_MODAL:Lcom/squareup/analytics/RegisterViewName;

    const-string v3, "Help Applet"

    invoke-direct {v1, v2, v3}, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 330
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->r12TutorialLauncher:Lcom/squareup/ui/main/R12ForceableContentLauncher;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/R12ForceableContentLauncher;->showContent(Ljava/lang/Object;)V

    return-void
.end method

.method public r6TutorialClicked()V
    .locals 2

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->r6TutorialLauncher:Lcom/squareup/ui/main/R6ForceableContentLauncher;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/R6ForceableContentLauncher;->showContent(Ljava/lang/Object;)V

    return-void
.end method

.method public redeemRewardsLoyaltyTutorialClicked()V
    .locals 3

    .line 356
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;

    sget-object v2, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->REDEMPTION:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    invoke-direct {v1, v2}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;-><init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 357
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    invoke-interface {v0}, Lcom/squareup/ui/help/HelpBadge;->refresh()V

    return-void
.end method

.method public seeMoreOrderHistory(Landroid/app/Activity;)V
    .locals 2

    .line 486
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    sget v1, Lcom/squareup/applet/help/R$string;->order_history_url:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method public sellerAgreementClicked()V
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 293
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/SupportUrlSettings;->getSellerAgreementUrl()Ljava/lang/String;

    move-result-object v1

    .line 292
    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_SELLER_AGREEMENT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public tutorialsContentClicked(Lcom/squareup/ui/help/HelpAppletContent;)V
    .locals 2

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p1, Lcom/squareup/ui/help/HelpAppletContent;->registerTapName:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 362
    invoke-virtual {p1}, Lcom/squareup/ui/help/HelpAppletContent;->handleClick()Z

    move-result v0

    if-nez v0, :cond_0

    .line 363
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScopeRunner$2;->$SwitchMap$com$squareup$analytics$RegisterTapName:[I

    iget-object p1, p1, Lcom/squareup/ui/help/HelpAppletContent;->registerTapName:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {p1}, Lcom/squareup/analytics/RegisterTapName;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 395
    :pswitch_0
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->redeemRewardsLoyaltyTutorialClicked()V

    goto :goto_0

    .line 392
    :pswitch_1
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->adjustPointsLoyaltyTutorialClicked()V

    goto :goto_0

    .line 389
    :pswitch_2
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->enrollLoyaltyTutorialClicked()V

    goto :goto_0

    .line 386
    :pswitch_3
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->createItemTutorialClicked()V

    goto :goto_0

    .line 383
    :pswitch_4
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->firstInvoiceTutorialClicked()V

    goto :goto_0

    .line 380
    :pswitch_5
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->r6TutorialClicked()V

    goto :goto_0

    .line 377
    :pswitch_6
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->r12TutorialClicked()V

    goto :goto_0

    .line 374
    :pswitch_7
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->firstPaymentTutorialClicked()V

    goto :goto_0

    .line 371
    :pswitch_8
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->feeTutorialClicked()V

    goto :goto_0

    .line 368
    :pswitch_9
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->whatsNewClicked()V

    goto :goto_0

    .line 365
    :pswitch_a
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->onboardingClicked()V

    :cond_0
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public versionClicked(Landroid/content/Context;)V
    .locals 2

    .line 256
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 257
    sget v1, Lcom/squareup/debitcard/R$string;->play_store_intent_uri:I

    .line 258
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 257
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 259
    invoke-static {v0, p1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public whatsNewClicked()V
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletScopeRunner;->tour:Lcom/squareup/tour/Tour;

    invoke-interface {v0}, Lcom/squareup/tour/Tour;->showAllWhatsNewScreen()V

    return-void
.end method
