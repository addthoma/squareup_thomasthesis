.class final Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;
.super Ljava/lang/Object;
.source "MessagehubState.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/MessagehubState;->singleAllowConversation()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/help/messages/ConversationAllowableState;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/messagehub/service/AllowConversationResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;

    invoke-direct {v0}, Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;->INSTANCE:Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/help/messages/ConversationAllowableState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/messagehub/service/AllowConversationResponse;",
            ">;)",
            "Lcom/squareup/ui/help/messages/ConversationAllowableState;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    .line 137
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    iget-object v0, v0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->allow:Ljava/lang/Boolean;

    const-string v1, "it.response.allow"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    new-instance v0, Lcom/squareup/ui/help/messages/ConversationAllowableState$Allowed;

    .line 139
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    iget-object p1, p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->custom_issue_fields:Ljava/util/List;

    const-string v1, "it.response.custom_issue_fields"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-direct {v0, p1}, Lcom/squareup/ui/help/messages/ConversationAllowableState$Allowed;-><init>(Ljava/util/List;)V

    check-cast v0, Lcom/squareup/ui/help/messages/ConversationAllowableState;

    goto :goto_0

    .line 142
    :cond_0
    new-instance v0, Lcom/squareup/ui/help/messages/ConversationAllowableState$Disallowed;

    .line 143
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    iget-object v1, v1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->view_title:Ljava/lang/String;

    const-string v2, "it.response.view_title"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    iget-object v2, v2, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->help_tip_title:Ljava/lang/String;

    const-string v3, "it.response.help_tip_title"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    iget-object p1, p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->help_tip_subtitle:Ljava/lang/String;

    const-string v3, "it.response.help_tip_subtitle"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/help/messages/ConversationAllowableState$Disallowed;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/help/messages/ConversationAllowableState;

    goto :goto_0

    .line 150
    :cond_1
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_2

    .line 151
    sget-object p1, Lcom/squareup/ui/help/messages/ConversationAllowableState$ServerError;->INSTANCE:Lcom/squareup/ui/help/messages/ConversationAllowableState$ServerError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/help/messages/ConversationAllowableState;

    :goto_0
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/MessagehubState$singleAllowConversation$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/help/messages/ConversationAllowableState;

    move-result-object p1

    return-object p1
.end method
