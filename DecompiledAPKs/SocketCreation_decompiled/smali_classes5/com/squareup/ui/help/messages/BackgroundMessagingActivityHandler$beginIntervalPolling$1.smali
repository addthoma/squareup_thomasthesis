.class final Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$beginIntervalPolling$1;
.super Ljava/lang/Object;
.source "BackgroundMessagingActivityHandler.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->beginIntervalPolling()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "test",
        "(Ljava/lang/Long;)Z"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$beginIntervalPolling$1;->this$0:Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Ljava/lang/Long;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$beginIntervalPolling$1;->this$0:Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;

    invoke-static {p1}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->access$getMessagingController$p(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)Lcom/squareup/ui/help/MessagingController;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/help/MessagingController;->helpshiftConversationActive()Z

    move-result p1

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 27
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$beginIntervalPolling$1;->test(Ljava/lang/Long;)Z

    move-result p1

    return p1
.end method
