.class public final Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;
.super Ljava/lang/Object;
.source "BackgroundMessagingActivityHandler.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBackgroundMessagingActivityHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BackgroundMessagingActivityHandler.kt\ncom/squareup/ui/help/messages/BackgroundMessagingActivityHandler\n*L\n1#1,118:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B!\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u000f\u001a\u00020\rH\u0002J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\rH\u0016J\u0008\u0010\u0014\u001a\u00020\rH\u0002J\u0008\u0010\u0015\u001a\u00020\rH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u0010\u0012\u000c\u0012\n \u000e*\u0004\u0018\u00010\r0\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;",
        "Lmortar/Scoped;",
        "foregrounedActivityListener",
        "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
        "messagingController",
        "Lcom/squareup/ui/help/MessagingController;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lcom/squareup/ui/help/MessagingController;Lio/reactivex/Scheduler;)V",
        "pollingSerialDisposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "requests",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "beginIntervalPolling",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "stopIntervalPolling",
        "updateMessagesCount",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final foregrounedActivityListener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final messagingController:Lcom/squareup/ui/help/MessagingController;

.field private final pollingSerialDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final requests:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lcom/squareup/ui/help/MessagingController;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "foregrounedActivityListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagingController"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->foregrounedActivityListener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    iput-object p2, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->messagingController:Lcom/squareup/ui/help/MessagingController;

    iput-object p3, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->mainScheduler:Lio/reactivex/Scheduler;

    .line 32
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->pollingSerialDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 33
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->requests:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$beginIntervalPolling(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->beginIntervalPolling()V

    return-void
.end method

.method public static final synthetic access$getMessagingController$p(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)Lcom/squareup/ui/help/MessagingController;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->messagingController:Lcom/squareup/ui/help/MessagingController;

    return-object p0
.end method

.method public static final synthetic access$getRequests$p(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->requests:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$stopIntervalPolling(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->stopIntervalPolling()V

    return-void
.end method

.method public static final synthetic access$updateMessagesCount(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->updateMessagesCount()V

    return-void
.end method

.method private final beginIntervalPolling()V
    .locals 6

    .line 86
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x41

    invoke-static/range {v0 .. v5}, Lio/reactivex/Observable;->interval(JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$beginIntervalPolling$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$beginIntervalPolling$1;-><init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->takeWhile(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 90
    new-instance v1, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$beginIntervalPolling$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$beginIntervalPolling$2;-><init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->pollingSerialDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v1, v0}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private final stopIntervalPolling()V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->pollingSerialDisposable:Lio/reactivex/disposables/SerialDisposable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private final updateMessagesCount()V
    .locals 3

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->messagingController:Lcom/squareup/ui/help/MessagingController;

    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$updateMessagesCount$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$updateMessagesCount$1;-><init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    check-cast v2, Landroid/os/Handler$Callback;

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    invoke-interface {v0, v1}, Lcom/squareup/ui/help/MessagingController;->getNotificationCount(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->messagingController:Lcom/squareup/ui/help/MessagingController;

    const/4 v1, 0x0

    .line 41
    invoke-interface {v0, v1}, Lcom/squareup/ui/help/MessagingController;->messagingViewState(Z)Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    new-instance v1, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$1;-><init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "messagingController\n    \u2026 beginIntervalPolling() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->foregrounedActivityListener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    invoke-virtual {v0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->getForegroundedActivity()Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$2;-><init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$3;-><init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "foregrounedActivityListe\u2026g()\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->requests:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 71
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x41

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->throttleLatest(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 72
    new-instance v1, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler$onEnterScope$4;-><init>(Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "requests\n        .thrott\u2026{ updateMessagesCount() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/BackgroundMessagingActivityHandler;->stopIntervalPolling()V

    return-void
.end method
