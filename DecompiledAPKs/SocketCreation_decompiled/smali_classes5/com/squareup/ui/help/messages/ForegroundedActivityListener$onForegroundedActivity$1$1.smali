.class final Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$1;
.super Ljava/lang/Object;
.source "ForegroundedActivityListener.kt"

# interfaces
.implements Lio/reactivex/functions/Cancellable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "cancel"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $listener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;

.field final synthetic this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;

    iput-object p2, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$1;->$listener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;

    iget-object v0, v0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    invoke-static {v0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->access$getActivityListener$p(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)Lcom/squareup/ActivityListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$1;->$listener:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;

    check-cast v1, Lcom/squareup/ActivityListener$ResumedPausedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ActivityListener;->unregisterResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V

    return-void
.end method
