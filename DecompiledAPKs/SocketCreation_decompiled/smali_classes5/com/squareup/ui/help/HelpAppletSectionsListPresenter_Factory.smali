.class public final Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;
.super Ljava/lang/Object;
.source "HelpAppletSectionsListPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final messagingControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;"
        }
    .end annotation
.end field

.field private final messagingTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->helpAppletProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->messagingControllerProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->messagingTutorialCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
            ">;)",
            "Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/help/HelpApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;
    .locals 8

    .line 61
    new-instance v7, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;-><init>(Lcom/squareup/ui/help/HelpApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->helpAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/help/HelpApplet;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->messagingControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/help/MessagingController;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->messagingTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->newInstance(Lcom/squareup/ui/help/HelpApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter_Factory;->get()Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method
