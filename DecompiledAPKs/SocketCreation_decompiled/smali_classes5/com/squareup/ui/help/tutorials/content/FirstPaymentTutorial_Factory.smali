.class public final Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;
.super Ljava/lang/Object;
.source "FirstPaymentTutorial_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;",
        ">;"
    }
.end annotation


# instance fields
.field private final orderEntryAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/Res;)Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;-><init>(Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;->newInstance(Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/Res;)Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial_Factory;->get()Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;

    move-result-object v0

    return-object v0
.end method
