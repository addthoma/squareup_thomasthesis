.class public Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;
.super Ljava/lang/Object;
.source "TroubleshootingVisibility.java"


# instance fields
.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method


# virtual methods
.method public showEmailLedger()Z
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EMAIL_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public showSendDiagnostics()Z
    .locals 4

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS:Lcom/squareup/settings/server/Features$Feature;

    .line 67
    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->EMAIL_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    .line 70
    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS:Lcom/squareup/settings/server/Features$Feature;

    .line 71
    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method public showTroubleshootingSection()Z
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->showSendDiagnostics()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->showEmailLedger()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->showUploadLedger()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public showUploadLedger()Z
    .locals 4

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS:Lcom/squareup/settings/server/Features$Feature;

    .line 56
    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_SUPPORT_LEDGER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->UPLOAD_LEDGER_AND_DIAGNOSTICS:Lcom/squareup/settings/server/Features$Feature;

    .line 59
    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    return v1
.end method
