.class public final Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;
.super Lcom/squareup/ui/help/HelpAppletContent;
.source "HelpContentStaticLinkData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "()V",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;

    invoke-direct {v0}, Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;->INSTANCE:Lcom/squareup/ui/help/help/SquareFeesStaticLinkData;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .line 40
    sget v1, Lcom/squareup/applet/help/R$string;->square_fees:I

    .line 41
    sget v0, Lcom/squareup/applet/help/R$string;->square_fees_url:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 42
    sget-object v4, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_HELP_CONTENT:Lcom/squareup/analytics/RegisterTapName;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x12

    const/4 v7, 0x0

    move-object v0, p0

    .line 39
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/help/HelpAppletContent;-><init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
