.class public interface abstract Lcom/squareup/ui/help/help/HelpScreen$Runner;
.super Ljava/lang/Object;
.source "HelpScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/help/HelpScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end method

.method public abstract helpContentClicked(Landroid/content/Context;Lcom/squareup/ui/help/HelpAppletContent;)V
.end method

.method public abstract helpScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/help/HelpScreenData;",
            ">;"
        }
    .end annotation
.end method
