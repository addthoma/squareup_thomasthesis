.class public final Lcom/squareup/ui/help/HelpAppletScope_HelpAppletModule_ProvideOrderMagstripeSectionFactory;
.super Ljava/lang/Object;
.source "HelpAppletScope_HelpAppletModule_ProvideOrderMagstripeSectionFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/HelpAppletScope_HelpAppletModule_ProvideOrderMagstripeSectionFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Class<",
        "*>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/help/HelpAppletScope_HelpAppletModule_ProvideOrderMagstripeSectionFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/ui/help/HelpAppletScope_HelpAppletModule_ProvideOrderMagstripeSectionFactory$InstanceHolder;->access$000()Lcom/squareup/ui/help/HelpAppletScope_HelpAppletModule_ProvideOrderMagstripeSectionFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideOrderMagstripeSection()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 26
    invoke-static {}, Lcom/squareup/ui/help/HelpAppletScope$HelpAppletModule;->provideOrderMagstripeSection()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method


# virtual methods
.method public get()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 18
    invoke-static {}, Lcom/squareup/ui/help/HelpAppletScope_HelpAppletModule_ProvideOrderMagstripeSectionFactory;->provideOrderMagstripeSection()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/help/HelpAppletScope_HelpAppletModule_ProvideOrderMagstripeSectionFactory;->get()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method
