.class public final Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;
.super Lcom/squareup/applet/AppletSectionsListPresenter;
.source "HelpAppletSectionsListPresenter.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/help/HelpAppletScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0014J\u0008\u0010\u0013\u001a\u00020\u0010H\u0014R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;",
        "Lcom/squareup/applet/AppletSectionsListPresenter;",
        "helpApplet",
        "Lcom/squareup/ui/help/HelpApplet;",
        "flow",
        "Lflow/Flow;",
        "device",
        "Lcom/squareup/util/Device;",
        "messagingController",
        "Lcom/squareup/ui/help/MessagingController;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "messagingTutorialCreator",
        "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
        "(Lcom/squareup/ui/help/HelpApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "Companion",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$Companion;

.field public static final MESSAGING_HOW_TO_TUTORIAL:Ljava/lang/String; = "How to message Square Support"

.field public static final MESSAGING_LEAVING_SCOPE:Ljava/lang/String; = "Messaging out of scope"

.field public static final MESSAGING_REPLY_TUTORIAL:Ljava/lang/String; = "How to reply to Square Support"


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final messagingController:Lcom/squareup/ui/help/MessagingController;

.field private final messagingTutorialCreator:Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->Companion:Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/help/HelpApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "helpApplet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagingController"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagingTutorialCreator"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-virtual {p1}, Lcom/squareup/ui/help/HelpApplet;->getSections()Lcom/squareup/applet/AppletSectionsList;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/applet/AppletSectionsListPresenter;-><init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;)V

    iput-object p3, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    iput-object p4, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->messagingController:Lcom/squareup/ui/help/MessagingController;

    iput-object p5, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object p6, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->messagingTutorialCreator:Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    return-void
.end method

.method public static final synthetic access$getMessagingController$p(Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;)Lcom/squareup/ui/help/MessagingController;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->messagingController:Lcom/squareup/ui/help/MessagingController;

    return-object p0
.end method

.method public static final synthetic access$getMessagingTutorialCreator$p(Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;)Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->messagingTutorialCreator:Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    return-object p0
.end method


# virtual methods
.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter$onEnterScope$1;-><init>(Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "device.screenSize.subscr\u2026y()\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/help/HelpAppletSectionsListPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Messaging out of scope"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 44
    invoke-super {p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->onExitScope()V

    return-void
.end method
