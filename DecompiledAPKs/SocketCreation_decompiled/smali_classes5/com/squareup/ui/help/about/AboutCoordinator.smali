.class public Lcom/squareup/ui/help/about/AboutCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AboutCoordinator.java"


# static fields
.field private static final SEVENTY_SIX_DAYS:J = 0x187635000L

.field private static final SPOC_VERSION:Ljava/lang/String; = "SPOC1.0"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final application:Landroid/app/Application;

.field private final clock:Lcom/squareup/util/Clock;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private librariesRow:Lcom/squareup/ui/account/view/SmartLineRow;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private ntepRow:Lcom/squareup/ui/account/view/SmartLineRow;

.field private final res:Lcom/squareup/util/Res;

.field private spocVersionRow:Lcom/squareup/widgets/list/NameValueRow;

.field private versionRow:Lcom/squareup/widgets/list/NameValueRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/util/Clock;)V
    .locals 0
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->res:Lcom/squareup/util/Res;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 54
    iput-object p4, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->features:Lcom/squareup/settings/server/Features;

    .line 55
    iput-object p5, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->application:Landroid/app/Application;

    .line 56
    iput-object p6, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 112
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 113
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 114
    sget v0, Lcom/squareup/applet/help/R$id;->version_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->versionRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 115
    sget v0, Lcom/squareup/applet/help/R$id;->spoc_version_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->spocVersionRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 116
    sget v0, Lcom/squareup/applet/help/R$id;->ntep_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->ntepRow:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 117
    sget v0, Lcom/squareup/applet/help/R$id;->libraries_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object p1, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->librariesRow:Lcom/squareup/ui/account/view/SmartLineRow;

    return-void
.end method

.method private getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    sget v1, Lcom/squareup/applet/help/R$string;->about:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private getAppPackageInfo()Landroid/content/pm/PackageInfo;
    .locals 3

    .line 105
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->application:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 6

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/about/AboutCoordinator;->bindViews(Landroid/view/View;)V

    .line 62
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 65
    iget-object v2, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-direct {p0, v1}, Lcom/squareup/ui/help/about/AboutCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 67
    iget-object v1, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->versionRow:Lcom/squareup/widgets/list/NameValueRow;

    new-instance v2, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$BCdXhYM6Fid4-JaTQl6XwhuUdSs;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$BCdXhYM6Fid4-JaTQl6XwhuUdSs;-><init>(Lcom/squareup/ui/help/about/AboutCoordinator;Landroid/content/Context;)V

    .line 68
    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 67
    invoke-virtual {v1, v0}, Lcom/squareup/widgets/list/NameValueRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SHOW_SPOC_VERSION_NUMBER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->clock:Lcom/squareup/util/Clock;

    .line 74
    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/squareup/ui/help/about/AboutCoordinator;->getAppPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-wide v4, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    sub-long/2addr v2, v4

    const-wide v4, 0x187635000L

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->spocVersionRow:Lcom/squareup/widgets/list/NameValueRow;

    const-string v2, "SPOC1.0"

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->spocVersionRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setVisibility(I)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->ntepRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v2, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$iFMbmqLpiRPsdH6tWEBMiV1F6pM;

    invoke-direct {v2, p0}, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$iFMbmqLpiRPsdH6tWEBMiV1F6pM;-><init>(Lcom/squareup/ui/help/about/AboutCoordinator;)V

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->ntepRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const-string v2, "CC 19-065"

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->ntepRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->ntepRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setVisibility(I)V

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->librariesRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v1, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$wEKxSJEm9PbEVFVm_BoDiO1j5jw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$wEKxSJEm9PbEVFVm_BoDiO1j5jw;-><init>(Lcom/squareup/ui/help/about/AboutCoordinator;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    new-instance v0, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$i6yPL4r7kKszgRRgfEHGma8h4tA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$i6yPL4r7kKszgRRgfEHGma8h4tA;-><init>(Lcom/squareup/ui/help/about/AboutCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$AboutCoordinator(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    .line 68
    iget-object p2, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->versionClicked(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic lambda$attach$1$AboutCoordinator(Landroid/view/View;)V
    .locals 0

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->ntepClicked()V

    return-void
.end method

.method public synthetic lambda$attach$2$AboutCoordinator(Landroid/view/View;)V
    .locals 0

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->librariesClicked()V

    return-void
.end method

.method public synthetic lambda$attach$4$AboutCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->aboutScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$Ra-Ltly6xy7vNVieW1LMcRqMis0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/about/-$$Lambda$AboutCoordinator$Ra-Ltly6xy7vNVieW1LMcRqMis0;-><init>(Lcom/squareup/ui/help/about/AboutCoordinator;)V

    .line 90
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$3$AboutCoordinator(Lcom/squareup/ui/help/about/AboutScreenData;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/applet/help/R$string;->support_version:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/ui/help/about/AboutScreenData;->versionString:Ljava/lang/String;

    const-string/jumbo v1, "version_name"

    .line 92
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 94
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/help/about/AboutCoordinator;->versionRow:Lcom/squareup/widgets/list/NameValueRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method
