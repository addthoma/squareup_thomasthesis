.class public Lcom/squareup/ui/help/HelpDeepLinksHandler;
.super Ljava/lang/Object;
.source "HelpDeepLinksHandler.java"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# static fields
.field private static final ANNOUNCEMENTS:Ljava/lang/String; = "announcements"

.field public static final HELP_MESSAGES_DEEP_LINK:Landroid/net/Uri;

.field private static final MESSAGES:Ljava/lang/String; = "messages"

.field private static final NAVIGATION_ID:Ljava/lang/String; = "navigationID"

.field private static final ORDER_READER:Ljava/lang/String; = "helpAppletOrderReader"

.field private static final REFERRALS:Ljava/lang/String; = "helpAppletReferrals"

.field private static final TOURS_AND_TUTORIALS:Ljava/lang/String; = "toursAndTutorials"


# instance fields
.field private final announcementsSection:Lcom/squareup/ui/help/announcements/AnnouncementsSection;

.field private final helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

.field private final helpSection:Lcom/squareup/ui/help/help/HelpSection;

.field private final ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

.field private final referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

.field private final tutorialsSection:Lcom/squareup/ui/help/tutorials/TutorialsSection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "square-register://help/navigate?navigationID=messages"

    .line 30
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->HELP_MESSAGES_DEEP_LINK:Landroid/net/Uri;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/help/orders/OrdersVisibility;Lcom/squareup/ui/help/referrals/ReferralsVisibility;Lcom/squareup/ui/help/HelpAppletHistoryBuilder;Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/ui/help/announcements/AnnouncementsSection;Lcom/squareup/ui/help/tutorials/TutorialsSection;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpSection:Lcom/squareup/ui/help/help/HelpSection;

    .line 46
    iput-object p5, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->announcementsSection:Lcom/squareup/ui/help/announcements/AnnouncementsSection;

    .line 47
    iput-object p6, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->tutorialsSection:Lcom/squareup/ui/help/tutorials/TutorialsSection;

    return-void
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 7

    .line 51
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "help"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 52
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 54
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    new-array v2, v2, [Lcom/squareup/container/ContainerTreeKey;

    .line 55
    invoke-interface {v1, v2}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistoryOverMaster([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    :cond_0
    const-string v1, "/navigate"

    .line 57
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "navigationID"

    .line 58
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, -0x1

    .line 59
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "helpAppletOrderReader"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "toursAndTutorials"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_2
    const-string v1, "announcements"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_3
    const-string v1, "messages"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_4
    const-string v1, "helpAppletReferrals"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :cond_1
    :goto_0
    if-eqz v0, :cond_a

    if-eq v0, v6, :cond_5

    if-eq v0, v5, :cond_4

    if-eq v0, v4, :cond_3

    if-eq v0, v3, :cond_2

    goto/16 :goto_1

    .line 102
    :cond_2
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    new-array v3, v6, [Lcom/squareup/container/ContainerTreeKey;

    iget-object v4, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->tutorialsSection:Lcom/squareup/ui/help/tutorials/TutorialsSection;

    .line 104
    invoke-virtual {v4}, Lcom/squareup/ui/help/tutorials/TutorialsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v4

    aput-object v4, v3, v2

    .line 103
    invoke-interface {v1, v3}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistoryOverMaster([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 97
    :cond_3
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    new-array v3, v6, [Lcom/squareup/container/ContainerTreeKey;

    iget-object v4, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->announcementsSection:Lcom/squareup/ui/help/announcements/AnnouncementsSection;

    .line 99
    invoke-virtual {v4}, Lcom/squareup/ui/help/announcements/AnnouncementsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v4

    aput-object v4, v3, v2

    .line 98
    invoke-interface {v1, v3}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistoryOverMaster([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 91
    :cond_4
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    new-array v3, v5, [Lcom/squareup/container/ContainerTreeKey;

    iget-object v4, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpSection:Lcom/squareup/ui/help/help/HelpSection;

    .line 93
    invoke-virtual {v4}, Lcom/squareup/ui/help/help/HelpSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v4

    aput-object v4, v3, v2

    sget-object v2, Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;->INSTANCE:Lcom/squareup/ui/help/messages/HelpMessagesBootstrapScreen;

    aput-object v2, v3, v6

    .line 92
    invoke-interface {v1, v3}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistoryOverMaster([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 70
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->ordersVisibility:Lcom/squareup/ui/help/orders/OrdersVisibility;

    invoke-virtual {p1}, Lcom/squareup/ui/help/orders/OrdersVisibility;->getReaderScreenType()Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;

    move-result-object p1

    .line 71
    sget-object v0, Lcom/squareup/ui/help/HelpDeepLinksHandler$1;->$SwitchMap$com$squareup$ui$help$orders$OrdersVisibility$ScreenType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/help/orders/OrdersVisibility$ScreenType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    if-eq v0, v6, :cond_9

    if-eq v0, v5, :cond_8

    if-eq v0, v4, :cond_7

    if-ne v0, v3, :cond_6

    .line 83
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    new-array v3, v6, [Lcom/squareup/container/ContainerTreeKey;

    sget-object v4, Lcom/squareup/ui/help/orders/OrderHardwareScreen;->INSTANCE:Lcom/squareup/ui/help/orders/OrderHardwareScreen;

    aput-object v4, v3, v2

    .line 84
    invoke-interface {v1, v3}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistory([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 86
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown order reader type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_7
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    new-array v3, v6, [Lcom/squareup/container/ContainerTreeKey;

    sget-object v4, Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;->INSTANCE:Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;

    aput-object v4, v3, v2

    .line 80
    invoke-interface {v1, v3}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistory([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 76
    :cond_8
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    new-array v2, v2, [Lcom/squareup/container/ContainerTreeKey;

    .line 77
    invoke-interface {v1, v2}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistory([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 73
    :cond_9
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 61
    :cond_a
    iget-object p1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    invoke-virtual {p1}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->showReferralsSection()Z

    move-result p1

    if-eqz p1, :cond_b

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->referralsVisibility:Lcom/squareup/ui/help/referrals/ReferralsVisibility;

    invoke-virtual {p1}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->markReferralViewed()V

    .line 63
    sget-object p1, Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;->INSTANCE:Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;

    .line 64
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v1, Lcom/squareup/ui/main/HistoryFactory$Simple;

    iget-object v3, p0, Lcom/squareup/ui/help/HelpDeepLinksHandler;->helpAppletHistoryBuilder:Lcom/squareup/ui/help/HelpAppletHistoryBuilder;

    new-array v4, v6, [Lcom/squareup/container/ContainerTreeKey;

    aput-object p1, v4, v2

    .line 65
    invoke-interface {v3, v4}, Lcom/squareup/ui/help/HelpAppletHistoryBuilder;->recreateHistoryOverMaster([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    invoke-direct {v0, v1}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object v0

    .line 109
    :cond_b
    :goto_1
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x43e7ac45 -> :sswitch_4
        -0x1b8afeb4 -> :sswitch_3
        0x21b15c0c -> :sswitch_2
        0x5dc16d79 -> :sswitch_1
        0x616e37b6 -> :sswitch_0
    .end sparse-switch
.end method
