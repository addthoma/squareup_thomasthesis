.class public Lcom/squareup/ui/help/referrals/ReferralsVisibility;
.super Ljava/lang/Object;
.source "ReferralsVisibility.java"


# static fields
.field static final DEFAULT_HAS_VIEWED:Ljava/lang/Boolean;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final hasViewedReferral:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final referralBadgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    .line 18
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->DEFAULT_HAS_VIEWED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->referralBadgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->features:Lcom/squareup/settings/server/Features;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->hasViewedReferral:Lcom/squareup/settings/LocalSetting;

    .line 28
    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->DISMISS_REFERRAL_BADGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->markReferralViewed()V

    goto :goto_0

    .line 31
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->updateBadgeCount()V

    :goto_0
    return-void
.end method

.method private updateBadgeCount()V
    .locals 2

    .line 65
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->hasViewedReferral()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 67
    iget-object v1, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->referralBadgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public badgeCount()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    .line 54
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/referrals/-$$Lambda$ReferralsVisibility$geMm6gDYcykYf2d4HfgQ1J0cBFk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/referrals/-$$Lambda$ReferralsVisibility$geMm6gDYcykYf2d4HfgQ1J0cBFk;-><init>(Lcom/squareup/ui/help/referrals/ReferralsVisibility;)V

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public hasViewedReferral()Z
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->hasViewedReferral:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->DEFAULT_HAS_VIEWED:Ljava/lang/Boolean;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$badgeCount$0$ReferralsVisibility(Ljava/lang/Boolean;)Lio/reactivex/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 56
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 57
    iget-object p1, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->referralBadgeCount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    .line 59
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public markReferralViewed()V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->hasViewedReferral:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 49
    invoke-direct {p0}, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->updateBadgeCount()V

    return-void
.end method

.method public showReferralsSection()Z
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/help/referrals/ReferralsVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
