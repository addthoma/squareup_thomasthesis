.class public final Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;
.super Ljava/lang/Object;
.source "SetupGuideSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/setupguide/SetupGuideSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final setupGuideAccessProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/setupguide/SetupGuideAccess;",
            ">;"
        }
    .end annotation
.end field

.field private final setupGuideLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/setupguide/SetupGuideAccess;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideLogger;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;->setupGuideAccessProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;->setupGuideLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/setupguide/SetupGuideAccess;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideLogger;",
            ">;)",
            "Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/help/setupguide/SetupGuideAccess;Lcom/squareup/setupguide/SetupGuideLogger;)Lcom/squareup/ui/help/setupguide/SetupGuideSection;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/help/setupguide/SetupGuideSection;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/setupguide/SetupGuideSection;-><init>(Lcom/squareup/ui/help/setupguide/SetupGuideAccess;Lcom/squareup/setupguide/SetupGuideLogger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/setupguide/SetupGuideSection;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;->setupGuideAccessProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/setupguide/SetupGuideAccess;

    iget-object v1, p0, Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;->setupGuideLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/setupguide/SetupGuideLogger;

    invoke-static {v0, v1}, Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;->newInstance(Lcom/squareup/ui/help/setupguide/SetupGuideAccess;Lcom/squareup/setupguide/SetupGuideLogger;)Lcom/squareup/ui/help/setupguide/SetupGuideSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/help/setupguide/SetupGuideSection_Factory;->get()Lcom/squareup/ui/help/setupguide/SetupGuideSection;

    move-result-object v0

    return-object v0
.end method
