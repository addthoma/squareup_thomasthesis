.class final Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3$1;
.super Ljava/lang/Object;
.source "JediHelpCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediHelpCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediHelpCoordinator.kt\ncom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3$1\n*L\n1#1,162:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 32
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3$1;->accept(Lkotlin/Unit;)V

    return-void
.end method

.method public final accept(Lkotlin/Unit;)V
    .locals 12

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;->$screen:Lcom/squareup/ui/help/jedi/JediHelpScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/help/jedi/JediHelpScreen;->getScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7f

    const/4 v11, 0x0

    .line 91
    invoke-static/range {v0 .. v11}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->copy$default(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;->$screen:Lcom/squareup/ui/help/jedi/JediHelpScreen;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScreen;->setScreenData(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getJediPanelView$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/jedi/ui/JediPanelView;

    move-result-object v0

    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData;

    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;

    iget-object v1, v1, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getJediHelpScopeRunner$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Lcom/squareup/jedi/JediComponentInputHandler;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/jedi/ui/JediPanelView;->update(Lcom/squareup/jedi/JediHelpScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.jedi.JediComponentInputHandler"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method
