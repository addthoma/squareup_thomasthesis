.class public final Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "JediWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediWorkflowViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediWorkflowViewFactory;",
            ">;)",
            "Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/jedi/JediWorkflow;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/jedi/JediWorkflowViewFactory;)Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/jedi/JediWorkflow;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/jedi/JediWorkflowViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/jedi/JediWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/BrowserLauncher;

    iget-object v3, p0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/jedi/JediWorkflowViewFactory;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/jedi/JediWorkflow;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/jedi/JediWorkflowViewFactory;)Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner_Factory;->get()Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
