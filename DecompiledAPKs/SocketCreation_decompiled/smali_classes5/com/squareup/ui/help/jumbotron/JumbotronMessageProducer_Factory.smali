.class public final Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;
.super Ljava/lang/Object;
.source "JumbotronMessageProducer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final jumbotronServiceKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/messages/MessagesService;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/messages/MessagesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->messagesSettingProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->messagesServiceProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->jumbotronServiceKeyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/messages/MessagesService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;",
            ">;)",
            "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/server/messages/MessagesService;Lcom/squareup/CountryCode;Ljavax/inject/Provider;Lcom/squareup/util/Clock;Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;)Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;>;",
            "Lcom/squareup/server/messages/MessagesService;",
            "Lcom/squareup/CountryCode;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;",
            ")",
            "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;"
        }
    .end annotation

    .line 70
    new-instance v8, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/server/messages/MessagesService;Lcom/squareup/CountryCode;Ljavax/inject/Provider;Lcom/squareup/util/Clock;Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->messagesSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->messagesServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/server/messages/MessagesService;

    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/CountryCode;

    iget-object v5, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->jumbotronServiceKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/server/messages/MessagesService;Lcom/squareup/CountryCode;Ljavax/inject/Provider;Lcom/squareup/util/Clock;Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;)Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer_Factory;->get()Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    move-result-object v0

    return-object v0
.end method
