.class public final Lcom/squareup/ui/SuffixPlugin;
.super Ljava/lang/Object;
.source "SuffixPlugin.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEditRow$Plugin;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSuffixPlugin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SuffixPlugin.kt\ncom/squareup/ui/SuffixPlugin\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\"\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\u0010\u0010$\u001a\u00020%2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0016J\u0018\u0010*\u001a\u00020%2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0016R\u0011\u0010\n\u001a\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\r\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000cR\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u00168VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00148BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u000cR\u000e\u0010 \u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010#\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/ui/SuffixPlugin;",
        "Lcom/squareup/noho/NohoEditRow$Plugin;",
        "context",
        "Landroid/content/Context;",
        "text",
        "",
        "textAppearanceId",
        "",
        "hintTextAppearanceId",
        "(Landroid/content/Context;Ljava/lang/String;II)V",
        "currentSuffixTextAppearanceId",
        "getCurrentSuffixTextAppearanceId",
        "()I",
        "displayText",
        "displayTextWidth",
        "",
        "editText",
        "Lcom/squareup/noho/NohoEditRow;",
        "getHintTextAppearanceId",
        "hintTextPaint",
        "Landroid/text/TextPaint;",
        "isClickable",
        "",
        "()Z",
        "paint",
        "getPaint",
        "()Landroid/text/TextPaint;",
        "getText",
        "()Ljava/lang/String;",
        "setText",
        "(Ljava/lang/String;)V",
        "getTextAppearanceId",
        "textPaint",
        "watcher",
        "com/squareup/ui/SuffixPlugin$watcher$1",
        "Lcom/squareup/ui/SuffixPlugin$watcher$1;",
        "attach",
        "",
        "measure",
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "editRect",
        "Landroid/graphics/Rect;",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawingInfo",
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private displayText:Ljava/lang/String;

.field private displayTextWidth:F

.field private editText:Lcom/squareup/noho/NohoEditRow;

.field private final hintTextAppearanceId:I

.field private hintTextPaint:Landroid/text/TextPaint;

.field private text:Ljava/lang/String;

.field private final textAppearanceId:I

.field private textPaint:Landroid/text/TextPaint;

.field private final watcher:Lcom/squareup/ui/SuffixPlugin$watcher$1;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 7

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/SuffixPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/SuffixPlugin;->text:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/ui/SuffixPlugin;->textAppearanceId:I

    iput p4, p0, Lcom/squareup/ui/SuffixPlugin;->hintTextAppearanceId:I

    .line 46
    new-instance p2, Lcom/squareup/ui/SuffixPlugin$watcher$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/SuffixPlugin$watcher$1;-><init>(Lcom/squareup/ui/SuffixPlugin;)V

    iput-object p2, p0, Lcom/squareup/ui/SuffixPlugin;->watcher:Lcom/squareup/ui/SuffixPlugin$watcher$1;

    .line 63
    iget p2, p0, Lcom/squareup/ui/SuffixPlugin;->textAppearanceId:I

    invoke-static {p1, p2}, Lcom/squareup/noho/CanvasExtensionsKt;->createTextPaintFromTextAppearance(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/SuffixPlugin;->textPaint:Landroid/text/TextPaint;

    .line 65
    iget p2, p0, Lcom/squareup/ui/SuffixPlugin;->hintTextAppearanceId:I

    invoke-static {p1, p2}, Lcom/squareup/noho/CanvasExtensionsKt;->createTextPaintFromTextAppearance(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->hintTextPaint:Landroid/text/TextPaint;

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->text:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->displayText:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Ljava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_0

    const-string p2, ""

    .line 41
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/SuffixPlugin;-><init>(Landroid/content/Context;Ljava/lang/String;II)V

    return-void
.end method

.method public static final synthetic access$getEditText$p(Lcom/squareup/ui/SuffixPlugin;)Lcom/squareup/noho/NohoEditRow;
    .locals 1

    .line 39
    iget-object p0, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez p0, :cond_0

    const-string v0, "editText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setEditText$p(Lcom/squareup/ui/SuffixPlugin;Lcom/squareup/noho/NohoEditRow;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method

.method private final getPaint()Landroid/text/TextPaint;
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_0

    const-string v1, "editText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->hintTextPaint:Landroid/text/TextPaint;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->textPaint:Landroid/text/TextPaint;

    :goto_2
    return-object v0
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->watcher:Lcom/squareup/ui/SuffixPlugin$watcher$1;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->description(Lcom/squareup/noho/NohoEditRow$Plugin;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public detach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->detach(Lcom/squareup/noho/NohoEditRow$Plugin;Lcom/squareup/noho/NohoEditRow;)V

    return-void
.end method

.method public focusChanged()V
    .locals 0

    .line 39
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->focusChanged(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-void
.end method

.method public final getCurrentSuffixTextAppearanceId()I
    .locals 2

    .line 59
    invoke-direct {p0}, Lcom/squareup/ui/SuffixPlugin;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/SuffixPlugin;->textPaint:Landroid/text/TextPaint;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/SuffixPlugin;->textAppearanceId:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/squareup/ui/SuffixPlugin;->hintTextAppearanceId:I

    :goto_0
    return v0
.end method

.method public final getHintTextAppearanceId()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/squareup/ui/SuffixPlugin;->hintTextAppearanceId:I

    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->text:Ljava/lang/String;

    return-object v0
.end method

.method public final getTextAppearanceId()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/ui/SuffixPlugin;->textAppearanceId:I

    return v0
.end method

.method public isClickable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 4

    const-string v0, "editRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "editText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getOriginalPaddingLeft()I

    move-result v0

    sub-int/2addr p1, v0

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getOriginalPaddingRight()I

    move-result v0

    sub-int/2addr p1, v0

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    :goto_2
    if-eqz v0, :cond_8

    .line 80
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_9

    goto :goto_4

    :cond_9
    const-string v0, ""

    .line 82
    :goto_4
    iget-object v3, p0, Lcom/squareup/ui/SuffixPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v3, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v3}, Lcom/squareup/noho/NohoEditRow;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    int-to-float p1, p1

    sub-float/2addr p1, v0

    .line 86
    invoke-direct {p0}, Lcom/squareup/ui/SuffixPlugin;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/SuffixPlugin;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_b

    iget-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->text:Ljava/lang/String;

    goto :goto_5

    .line 87
    :cond_b
    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->text:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/squareup/ui/SuffixPlugin;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1, p1, v3}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 86
    :goto_5
    iput-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->displayText:Ljava/lang/String;

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->hintTextPaint:Landroid/text/TextPaint;

    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->displayText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result p1

    iget-object v0, p0, Lcom/squareup/ui/SuffixPlugin;->textPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/squareup/ui/SuffixPlugin;->displayText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iput p1, p0, Lcom/squareup/ui/SuffixPlugin;->displayTextWidth:F

    .line 92
    new-instance p1, Lcom/squareup/noho/NohoEditRow$PluginSize;

    sget-object v0, Lcom/squareup/noho/NohoEditRow$Side;->END:Lcom/squareup/noho/NohoEditRow$Side;

    iget v1, p0, Lcom/squareup/ui/SuffixPlugin;->displayTextWidth:F

    float-to-int v1, v1

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/noho/NohoEditRow$PluginSize;-><init>(Lcom/squareup/noho/NohoEditRow$Side;II)V

    return-object p1
.end method

.method public onClick()Z
    .locals 1

    .line 39
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->onClick(Lcom/squareup/noho/NohoEditRow$Plugin;)Z

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V
    .locals 3

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawingInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getTotalRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    iget v1, p0, Lcom/squareup/ui/SuffixPlugin;->displayTextWidth:F

    sub-float/2addr v0, v1

    .line 100
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result p2

    .line 101
    iget-object v1, p0, Lcom/squareup/ui/SuffixPlugin;->displayText:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/ui/SuffixPlugin;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-static {p1, v1, v0, p2, v2}, Lcom/squareup/noho/CanvasExtensionsKt;->drawTextCenteredAt(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/text/TextPaint;)V

    return-void
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/SuffixPlugin;->text:Ljava/lang/String;

    return-void
.end method
