.class Lcom/squareup/ui/cart/CartDiff;
.super Landroidx/recyclerview/widget/DiffUtil$Callback;
.source "CartDiff.java"


# instance fields
.field private final newList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;"
        }
    .end annotation
.end field

.field private final oldList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Landroidx/recyclerview/widget/DiffUtil$Callback;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/cart/CartDiff;->oldList:Ljava/util/List;

    .line 16
    iput-object p2, p0, Lcom/squareup/ui/cart/CartDiff;->newList:Ljava/util/List;

    return-void
.end method

.method private getNewDisplayItem(I)Lcom/squareup/ui/cart/CartAdapterItem;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiff;->newList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem;

    return-object p1
.end method

.method private getOldDisplayItem(I)Lcom/squareup/ui/cart/CartAdapterItem;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiff;->oldList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem;

    return-object p1
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartDiff;->getOldDisplayItem(I)Lcom/squareup/ui/cart/CartAdapterItem;

    move-result-object p1

    .line 34
    invoke-direct {p0, p2}, Lcom/squareup/ui/cart/CartDiff;->getNewDisplayItem(I)Lcom/squareup/ui/cart/CartAdapterItem;

    move-result-object p2

    .line 35
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public areItemsTheSame(II)Z
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartDiff;->getOldDisplayItem(I)Lcom/squareup/ui/cart/CartAdapterItem;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/cart/CartAdapterItem;->getId()I

    move-result p1

    .line 29
    invoke-direct {p0, p2}, Lcom/squareup/ui/cart/CartDiff;->getNewDisplayItem(I)Lcom/squareup/ui/cart/CartAdapterItem;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/ui/cart/CartAdapterItem;->getId()I

    move-result p2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getNewListSize()I
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiff;->newList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiff;->oldList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
