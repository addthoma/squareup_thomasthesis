.class public abstract Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;
.super Lmortar/ViewPresenter;
.source "CartHeaderBasePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/squareup/ui/cart/header/CartHeaderBaseView;",
        ">",
        "Lmortar/ViewPresenter<",
        "TV;>;"
    }
.end annotation


# instance fields
.field private lastSaleQuantity:I

.field protected final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field protected final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final pauser:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field protected final res:Lcom/squareup/util/Res;

.field private final saleQuantity:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final saleTextOrNull:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field protected final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;)V
    .locals 2

    .line 42
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 35
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->saleTextOrNull:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->saleQuantity:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 38
    iput v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->lastSaleQuantity:I

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->pauser:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    .line 46
    iput-object p5, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->res:Lcom/squareup/util/Res;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    return-void
.end method

.method private isFirstItem()Z
    .locals 1

    .line 179
    iget v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->lastSaleQuantity:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isRemovingItem(I)Z
    .locals 1

    .line 175
    iget v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->lastSaleQuantity:I

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private isScannedItem()Z
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getLastItemEntryMethod()Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->SCANNED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$null$2(Lcom/squareup/ui/cart/header/CartHeaderBaseView;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/util/Optional;)V
    .locals 1

    .line 87
    invoke-virtual {p2}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    invoke-virtual {p2}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->showCurrentSale(ZLjava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_0
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->showNoSale(Z)V

    :goto_0
    return-void
.end method

.method private maybeUpdateCartCountIcon(Lcom/squareup/ui/cart/header/CartHeaderBaseView;I)V
    .locals 1

    .line 160
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->isScannedItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->isFirstItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    invoke-direct {p0, p2}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->isRemovingItem(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    invoke-direct {p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->shouldDeferToFlyByAnimation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 164
    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/ui/cart/header/CartHeaderBaseView;->setSaleQuantity(I)V

    .line 166
    :cond_1
    iput p2, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->lastSaleQuantity:I

    .line 167
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getFlyByAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->clearQuantity()V

    return-void
.end method

.method private shouldDeferToFlyByAnimation()Z
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getFlyByAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->hasUsableData()Z

    move-result v1

    .line 189
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->hasQuantityToAnimate()Z

    move-result v0

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method protected getSaleText()Ljava/lang/String;
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCustomerDisplayNameOrBlank()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasDiscounts()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return-object v0

    .line 118
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->current_sale:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$4$CartHeaderBasePresenter(Lcom/squareup/ui/cart/header/CartHeaderBaseView;Ljava/lang/Integer;)V
    .locals 0

    .line 101
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->maybeUpdateCartCountIcon(Lcom/squareup/ui/cart/header/CartHeaderBaseView;I)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$CartHeaderBasePresenter(Lkotlin/Unit;)V
    .locals 0

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->updateSaleTextAndQuantity()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$CartHeaderBasePresenter(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->onCartUpdated()V

    return-void
.end method

.method public synthetic lambda$onLoad$3$CartHeaderBasePresenter(Lcom/squareup/ui/cart/header/CartHeaderBaseView;)Lrx/Subscription;
    .locals 3

    .line 83
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 84
    iget-object v1, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->saleTextOrNull:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 85
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$LHILqL-7Zl5AnbZi6rq7bm0dz5M;

    invoke-direct {v2, p1, v0}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$LHILqL-7Zl5AnbZi6rq7bm0dz5M;-><init>(Lcom/squareup/ui/cart/header/CartHeaderBaseView;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 86
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    const/4 v1, 0x1

    .line 93
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$CartHeaderBasePresenter(Lcom/squareup/ui/cart/header/CartHeaderBaseView;)Lrx/Subscription;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->saleQuantity:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 100
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$fVFLamATEFmm5bSxLC0zvNN-u5o;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$fVFLamATEFmm5bSxLC0zvNN-u5o;-><init>(Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;Lcom/squareup/ui/cart/header/CartHeaderBaseView;)V

    .line 101
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method abstract onCartUpdated()V
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->pauser:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    new-instance v1, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter$1;-><init>(Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;)V

    invoke-interface {v0, p1, v1}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 71
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->onCustomerChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$s5JEGjlVZMsVmOph2kRFo_TziR0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$s5JEGjlVZMsVmOph2kRFo_TziR0;-><init>(Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;)V

    .line 72
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 70
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cartChanges()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$P-95_UHc8KIxlywwyw12BchstP4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$P-95_UHc8KIxlywwyw12BchstP4;-><init>(Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 77
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/header/CartHeaderBaseView;

    .line 81
    new-instance v0, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$6msJIwfkhg1DYAwky0lcVE2xS_c;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$6msJIwfkhg1DYAwky0lcVE2xS_c;-><init>(Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;Lcom/squareup/ui/cart/header/CartHeaderBaseView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    new-instance v0, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$FeKuAyCJdXgXW_5gW6SSFi8phFE;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/cart/header/-$$Lambda$CartHeaderBasePresenter$FeKuAyCJdXgXW_5gW6SSFi8phFE;-><init>(Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;Lcom/squareup/ui/cart/header/CartHeaderBaseView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected updateSaleTextAndQuantity()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->saleQuantity:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->saleTextOrNull:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p0}, Lcom/squareup/ui/cart/header/CartHeaderBasePresenter;->getSaleText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
