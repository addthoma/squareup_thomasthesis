.class Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;
.super Lcom/squareup/ui/EmptyAnimationListener;
.source "CartHeaderPhoneView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->initializeSaleAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

.field final synthetic val$currentSaleInSet:Landroid/view/animation/AnimationSet;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/header/CartHeaderPhoneView;Landroid/view/animation/AnimationSet;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;->this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    iput-object p2, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;->val$currentSaleInSet:Landroid/view/animation/AnimationSet;

    invoke-direct {p0}, Lcom/squareup/ui/EmptyAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;->this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->noSale:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;->this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleContainer:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;->val$currentSaleInSet:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/cart/header/CartHeaderPhoneView$2;->this$0:Lcom/squareup/ui/cart/header/CartHeaderPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/cart/header/CartHeaderPhoneView;->currentSaleContainer:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
