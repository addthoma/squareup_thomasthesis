.class Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;
.super Landroidx/viewpager/widget/PagerAdapter;
.source "DiningOptionViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/DiningOptionViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiningOptionPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/DiningOptionViewPager;)V
    .locals 1

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;

    invoke-direct {p0}, Landroidx/viewpager/widget/PagerAdapter;-><init>()V

    .line 103
    new-instance v0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;

    invoke-direct {v0, p0, p1, p1}, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;-><init>(Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;Lcom/squareup/ui/cart/DiningOptionViewPager;Landroidx/viewpager/widget/ViewPager;)V

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;I)I
    .locals 0

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->getRealDiningOptionPosition(I)I

    move-result p0

    return p0
.end method

.method private getRealDiningOptionPosition(I)I
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;

    iget-object v0, v0, Lcom/squareup/ui/cart/DiningOptionViewPager;->presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->diningOptionCount()I

    move-result v0

    add-int/lit8 p1, p1, -0x1

    add-int/2addr p1, v0

    .line 174
    rem-int/2addr p1, v0

    return p1
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .line 158
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;

    iget-object v0, v0, Lcom/squareup/ui/cart/DiningOptionViewPager;->presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->diningOptionCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 0

    const/4 p1, -0x2

    return p1
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .line 137
    new-instance v0, Lcom/squareup/marketfont/MarketTextView;

    iget-object v1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;

    .line 138
    invoke-virtual {v1}, Lcom/squareup/ui/cart/DiningOptionViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 139
    iget-object v1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/DiningOptionViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 140
    sget v2, Lcom/squareup/marin/R$dimen;->marin_text_section_header:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 141
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v2, v1

    .line 142
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 143
    iget-object v1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;

    iget-object v1, v1, Lcom/squareup/ui/cart/DiningOptionViewPager;->presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    .line 144
    invoke-direct {p0, p2}, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->getRealDiningOptionPosition(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->getDiningOptionDisplayName(I)Ljava/lang/String;

    move-result-object v1

    .line 143
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v1, 0x11

    .line 145
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 146
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v1, v2}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 148
    new-instance v1, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$2;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$2;-><init>(Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
