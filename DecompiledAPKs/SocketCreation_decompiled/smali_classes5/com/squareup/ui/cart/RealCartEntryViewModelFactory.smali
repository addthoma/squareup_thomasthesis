.class public final Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;
.super Ljava/lang/Object;
.source "RealCartEntryViewModelFactory.kt"

# interfaces
.implements Lcom/squareup/ui/cart/CartEntryViewModelFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;,
        Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCartEntryViewModelFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCartEntryViewModelFactory.kt\ncom/squareup/ui/cart/RealCartEntryViewModelFactory\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,752:1\n18#2:753\n18#2:754\n*E\n*S KotlinDebug\n*F\n+ 1 RealCartEntryViewModelFactory.kt\ncom/squareup/ui/cart/RealCartEntryViewModelFactory\n*L\n604#1:753\n699#1:754\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u000e\u0018\u00002\u00020\u0001:\u0002PQB7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ2\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0008\u0008\u0001\u0010 \u001a\u00020!H\u0002J2\u0010\u0016\u001a\u00020\u00172\u0006\u0010\"\u001a\u00020#2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0008\u0008\u0001\u0010 \u001a\u00020!H\u0002J\u0010\u0010$\u001a\u00020%2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J*\u0010&\u001a\u00020%2\u0008\u0008\u0001\u0010\'\u001a\u00020!2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u001bH\u0016J \u0010)\u001a\u00020%2\u0006\u0010*\u001a\u00020#2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J \u0010+\u001a\u00020%2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020!2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\n\u0010/\u001a\u0004\u0018\u000100H\u0002J \u00101\u001a\u00020%2\u0006\u00102\u001a\u00020#2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J+\u00103\u001a\u00020%2\u0008\u0008\u0001\u0010\'\u001a\u00020!2\n\u0008\u0001\u00104\u001a\u0004\u0018\u00010!2\u0006\u0010\"\u001a\u00020\u0013H\u0016\u00a2\u0006\u0002\u00105J2\u00102\u001a\u0002062\u0006\u00102\u001a\u00020#2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0008\u0008\u0001\u0010 \u001a\u00020!H\u0002J\n\u00107\u001a\u0004\u0018\u000108H\u0002J:\u00109\u001a\u00020%2\u0006\u0010,\u001a\u00020-2\u0006\u0010:\u001a\u00020\u001b2\u0008\u0010;\u001a\u0004\u0018\u00010\u00132\u0006\u0010<\u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010=\u001a\u00020\u001bH\u0016J\u001a\u0010>\u001a\u0004\u0018\u0001002\u0006\u0010>\u001a\u00020?2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0018\u0010@\u001a\u00020#2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010*\u001a\u00020#H\u0002J<\u0010A\u001a\u0004\u0018\u0001082\u0006\u0010,\u001a\u00020-2\u0008\u0010;\u001a\u0004\u0018\u00010\u00132\u0006\u0010B\u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010=\u001a\u00020\u001bH\u0002J\"\u0010C\u001a\u00020D2\u0006\u0010,\u001a\u00020-2\u0008\u0010;\u001a\u0004\u0018\u00010\u00132\u0006\u0010=\u001a\u00020\u001bH\u0002J*\u0010E\u001a\u00020D2\u0006\u0010,\u001a\u00020-2\u0008\u0010;\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010=\u001a\u00020\u001bH\u0002J\"\u0010F\u001a\u00020%2\u0008\u0008\u0001\u0010\'\u001a\u00020!2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0018\u0010G\u001a\u00020%2\u0006\u0010*\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J(\u0010H\u001a\u00020%2\u0006\u0010*\u001a\u00020#2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010I\u001a\u00020\u001bH\u0016J*\u0010H\u001a\u00020%2\u0008\u0008\u0001\u0010\'\u001a\u00020!2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010I\u001a\u00020\u001bH\u0016J\"\u0010J\u001a\u00020%2\u0008\u0008\u0001\u0010\'\u001a\u00020!2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\"\u0010K\u001a\u00020%2\u0008\u0008\u0001\u0010\'\u001a\u00020!2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u001a\u0010L\u001a\u00020%2\u0008\u0008\u0001\u0010\'\u001a\u00020!2\u0006\u0010M\u001a\u00020\u0013H\u0016J\u000c\u0010 \u001a\u00020\u0010*\u00020\u001fH\u0002J\u000c\u0010N\u001a\u00020#*\u00020-H\u0002J\u000c\u0010O\u001a\u00020#*\u00020-H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006R"
    }
    d2 = {
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;",
        "Lcom/squareup/ui/cart/CartEntryViewModelFactory;",
        "application",
        "Landroid/app/Application;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "config",
        "Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;Lcom/squareup/text/DurationFormatter;)V",
        "darkGrayDisabledLightGray",
        "Landroid/content/res/ColorStateList;",
        "darkGrayDisabledMediumGray",
        "defaultItemizationName",
        "",
        "longNonfinalSeparator",
        "Lcom/squareup/phrase/Phrase;",
        "amount",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Amount;",
        "price",
        "Lcom/squareup/protos/common/Money;",
        "enabled",
        "",
        "weight",
        "Lcom/squareup/marketfont/MarketFont$Weight;",
        "appearance",
        "Lcom/squareup/ui/cart/Appearance;",
        "color",
        "",
        "formattedValue",
        "",
        "comp",
        "Lcom/squareup/ui/cart/CartEntryViewModel;",
        "discount",
        "labelId",
        "showWarning",
        "discountItem",
        "label",
        "emptyCustomAmount",
        "item",
        "Lcom/squareup/checkout/CartItem;",
        "width",
        "hideQuantity",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;",
        "lineItem",
        "name",
        "loyaltyPoints",
        "subLabelId",
        "(ILjava/lang/Integer;Ljava/lang/String;)Lcom/squareup/ui/cart/CartEntryViewModel;",
        "Lcom/squareup/ui/cart/CartEntryViewModel$Name;",
        "noModifiers",
        "Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;",
        "orderItem",
        "allowGlyph",
        "cartDiningOption",
        "wrapModifierList",
        "canSeeSeating",
        "quantity",
        "Ljava/math/BigDecimal;",
        "strikethroughIfNeeded",
        "subLabel",
        "wrapAndCommaSeparate",
        "subLabelInCommaSeparatedList",
        "Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;",
        "subLabelInNewlineSeparatedList",
        "subTotal",
        "surcharge",
        "taxes",
        "forceShowAmount",
        "tip",
        "total",
        "unappliedCoupon",
        "subtitle",
        "compReasonPhrased",
        "voidReasonPhrased",
        "Config",
        "Factory",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

.field private final darkGrayDisabledLightGray:Landroid/content/res/ColorStateList;

.field private final darkGrayDisabledMediumGray:Landroid/content/res/ColorStateList;

.field private final defaultItemizationName:Ljava/lang/String;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final longNonfinalSeparator:Lcom/squareup/phrase/Phrase;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;Lcom/squareup/text/DurationFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidCompSettings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iput-object p4, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    iput-object p6, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    .line 83
    sget p2, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray_disabled_medium_gray:I

    .line 82
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->darkGrayDisabledMediumGray:Landroid/content/res/ColorStateList;

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->darkGrayDisabledLightGray:Landroid/content/res/ColorStateList;

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->defaultItemizationName:Ljava/lang/String;

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/utilities/R$string;->list_pattern_long_nonfinal_separator:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->longNonfinalSeparator:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method private final amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;
    .locals 9

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    move-object v3, p0

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    move v8, p5

    .line 486
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p1

    return-object p1
.end method

.method private final amount(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;
    .locals 7

    .line 501
    invoke-direct {p0, p4, p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->strikethroughIfNeeded(Lcom/squareup/ui/cart/Appearance;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 502
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {p1, p5}, Lcom/squareup/util/Res;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 503
    iget-object v5, p4, Lcom/squareup/ui/cart/Appearance;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 505
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    .line 506
    iget-object p4, p4, Lcom/squareup/ui/cart/Appearance;->glyphColorStateListId:Ljava/lang/Integer;

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p4

    goto :goto_0

    :cond_0
    sget p4, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 505
    :goto_0
    invoke-interface {p1, p4}, Lcom/squareup/util/Res;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    .line 509
    new-instance p1, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-object v0, p1

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/cart/CartEntryViewModel$Amount;-><init>(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/content/res/ColorStateList;)V

    return-object p1
.end method

.method private final color(Lcom/squareup/ui/cart/Appearance;)Landroid/content/res/ColorStateList;
    .locals 0

    .line 735
    iget-boolean p1, p1, Lcom/squareup/ui/cart/Appearance;->strikethrough:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->darkGrayDisabledMediumGray:Landroid/content/res/ColorStateList;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->darkGrayDisabledLightGray:Landroid/content/res/ColorStateList;

    :goto_0
    return-object p1
.end method

.method private final compReasonPhrased(Lcom/squareup/checkout/CartItem;)Ljava/lang/CharSequence;
    .locals 2

    .line 723
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->comped_with_reason:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 724
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getCompReason()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 725
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "res.phrase(R.string.comp\u2026Reason)\n        .format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method private final name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;
    .locals 1

    .line 452
    invoke-direct {p0, p4, p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->strikethroughIfNeeded(Lcom/squareup/ui/cart/Appearance;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 453
    new-instance p4, Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p5}, Lcom/squareup/util/Res;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p5

    invoke-direct {p4, p1, p5, p3, p2}, Lcom/squareup/ui/cart/CartEntryViewModel$Name;-><init>(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;Lcom/squareup/marketfont/MarketFont$Weight;Z)V

    return-object p4
.end method

.method private final noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method private final quantity(Ljava/math/BigDecimal;Lcom/squareup/ui/cart/Appearance;)Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;
    .locals 4

    .line 464
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_2

    .line 465
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 466
    invoke-virtual {v0, p1}, Lcom/squareup/quantity/PerUnitFormatter;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 467
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->asSuffix()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 468
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 469
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 470
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 471
    check-cast p1, Ljava/lang/CharSequence;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    check-cast v0, Landroid/text/style/CharacterStyle;

    invoke-static {p1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    .line 472
    iget-boolean v0, p2, Lcom/squareup/ui/cart/Appearance;->strikethrough:Z

    if-eqz v0, :cond_0

    .line 473
    new-instance v0, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v0}, Landroid/text/style/StrikethroughSpan;-><init>()V

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v2

    const/16 v3, 0x11

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 476
    :cond_0
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {p0, p2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->color(Lcom/squareup/ui/cart/Appearance;)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;-><init>(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V

    return-object v0

    .line 470
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method private final strikethroughIfNeeded(Lcom/squareup/ui/cart/Appearance;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 520
    iget-boolean p1, p1, Lcom/squareup/ui/cart/Appearance;->strikethrough:Z

    if-eqz p1, :cond_0

    .line 521
    new-instance p1, Landroid/text/style/StrikethroughSpan;

    invoke-direct {p1}, Landroid/text/style/StrikethroughSpan;-><init>()V

    check-cast p1, Landroid/text/style/CharacterStyle;

    invoke-static {p2, p1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    :cond_0
    return-object p2
.end method

.method private final subLabel(Lcom/squareup/checkout/CartItem;Ljava/lang/String;ZZLcom/squareup/ui/cart/Appearance;Z)Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;
    .locals 2

    .line 542
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isService()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v1, "item.selectedVariation"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v0

    if-nez v0, :cond_2

    .line 536
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result v0

    if-nez v0, :cond_2

    .line 537
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 538
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->hasSelectedModifiers()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    if-nez v0, :cond_2

    .line 541
    invoke-virtual {p1, p2}, Lcom/squareup/checkout/CartItem;->shouldShowDiningOption(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    if-nez v0, :cond_2

    const/4 p1, 0x0

    return-object p1

    :cond_2
    if-eqz p3, :cond_3

    .line 548
    invoke-direct {p0, p1, p2, p6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->subLabelInCommaSeparatedList(Lcom/squareup/checkout/CartItem;Ljava/lang/String;Z)Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;

    move-result-object p1

    goto :goto_2

    .line 550
    :cond_3
    invoke-direct {p0, p1, p2, p5, p6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->subLabelInNewlineSeparatedList(Lcom/squareup/checkout/CartItem;Ljava/lang/String;Lcom/squareup/ui/cart/Appearance;Z)Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;

    move-result-object p1

    .line 555
    :goto_2
    new-instance p2, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    iget-object p3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getSubLabelFontSize()F

    move-result p3

    invoke-direct {p2, p1, p3, p4}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;FZ)V

    return-object p2
.end method

.method private final subLabelInCommaSeparatedList(Lcom/squareup/checkout/CartItem;Ljava/lang/String;Z)Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;
    .locals 6

    .line 563
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_0

    .line 565
    iget-object p3, p1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    if-eqz p3, :cond_0

    .line 566
    iget-object p3, p1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-virtual {p3, v1}, Lcom/squareup/checkout/OrderDestination;->description(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 568
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 572
    :cond_0
    iget-object p3, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v1, "selectedVariation"

    .line 575
    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 577
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 578
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 579
    invoke-virtual {p3}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v2

    const-string v3, "selectedVariation.unitAbbreviation"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 580
    iget-object v2, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {p3}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 581
    invoke-virtual {v1}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v1

    .line 582
    invoke-virtual {v1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 583
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iget-object v3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v3}, Lcom/squareup/quantity/ItemQuantities;->toQuantityEntrySuffix(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 576
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isService()Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {p3}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-nez v1, :cond_2

    .line 589
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v4

    const-string v5, "item.duration"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-static {v1, v4, v3, v2, v5}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 593
    invoke-virtual {p3}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    :cond_3
    iget-object p3, p1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {p3}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_4
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedMap;

    .line 597
    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/OrderModifier;

    .line 598
    iget-object v5, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-virtual {v4, v5}, Lcom/squareup/checkout/OrderModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 602
    :cond_5
    iget-object p3, p1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    check-cast p3, Ljava/lang/CharSequence;

    if-eqz p3, :cond_6

    invoke-static {p3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_7

    :cond_6
    const/4 v3, 0x1

    :cond_7
    if-nez v3, :cond_8

    .line 604
    iget-object p3, p1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    check-cast p3, Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->application:Landroid/app/Application;

    check-cast v1, Landroid/content/Context;

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 753
    new-instance v4, Lcom/squareup/fonts/FontSpan;

    invoke-static {v3, v2}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v2

    invoke-direct {v4, v1, v2}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v4, Landroid/text/style/CharacterStyle;

    .line 604
    invoke-static {p3, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p3

    .line 603
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    :cond_8
    invoke-virtual {p1, p2}, Lcom/squareup/checkout/CartItem;->shouldShowDiningOption(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_a

    .line 609
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p2

    if-eqz p2, :cond_9

    invoke-virtual {p2}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_9
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 612
    :cond_a
    :goto_1
    iget-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {p2}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result p2

    if-eqz p2, :cond_b

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result p2

    if-eqz p2, :cond_b

    .line 613
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->compReasonPhrased(Lcom/squareup/checkout/CartItem;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 616
    :cond_b
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->longNonfinalSeparator:Lcom/squareup/phrase/Phrase;

    invoke-static {p1}, Lcom/squareup/util/ListPhrase;->from(Lcom/squareup/phrase/Phrase;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    .line 617
    new-instance p2, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p1, v0}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string p3, "listPhrase.format(subLabel)"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p1}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;-><init>(Ljava/lang/CharSequence;)V

    check-cast p2, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;

    return-object p2
.end method

.method private final subLabelInNewlineSeparatedList(Lcom/squareup/checkout/CartItem;Ljava/lang/String;Lcom/squareup/ui/cart/Appearance;Z)Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    .line 626
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/List;

    const-string v4, ""

    if-eqz p4, :cond_0

    .line 628
    iget-object v5, v1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    if-eqz v5, :cond_0

    .line 629
    iget-object v5, v1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    iget-object v6, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-virtual {v5, v6}, Lcom/squareup/checkout/OrderDestination;->description(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 631
    new-instance v6, Lcom/squareup/ui/cart/NameValuePair;

    check-cast v5, Ljava/lang/CharSequence;

    move-object v7, v4

    check-cast v7, Ljava/lang/CharSequence;

    invoke-direct {v6, v5, v7, v2}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 635
    :cond_0
    iget-object v5, v1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v6, "selectedVariation"

    .line 636
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v6

    .line 639
    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v7

    const-string/jumbo v8, "unitAbbreviation"

    if-eqz v7, :cond_1

    .line 641
    new-instance v7, Lcom/squareup/ui/cart/NameValuePair;

    .line 642
    iget-object v9, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 643
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v9

    .line 644
    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v9

    .line 645
    invoke-virtual {v9}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v9

    .line 646
    invoke-virtual {v9}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v9

    .line 647
    iget-object v10, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 648
    invoke-virtual {v10, v6}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v10

    .line 649
    iget-object v11, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result v12

    invoke-virtual {v10, v11, v12}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v10

    .line 650
    invoke-virtual {v10}, Lcom/squareup/quantity/PerUnitFormatter;->asSuffix()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v10

    .line 651
    invoke-virtual {v10}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v10

    .line 652
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 653
    iget-object v10, v1, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iget-object v12, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-static {v10, v12}, Lcom/squareup/quantity/ItemQuantities;->toQuantityEntrySuffix(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    check-cast v10, Ljava/lang/CharSequence;

    .line 641
    invoke-direct {v7, v9, v10, v2}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)V

    .line 640
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result v7

    const/4 v9, 0x2

    const/4 v10, 0x0

    const/4 v11, 0x0

    if-eqz v7, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->isService()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 663
    new-instance v7, Lcom/squareup/ui/cart/NameValuePair;

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-direct {v7, v5, v11, v2}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 665
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result v7

    const-string v12, "item.duration"

    if-eqz v7, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->isService()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 667
    new-instance v7, Lcom/squareup/ui/cart/NameValuePair;

    .line 668
    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " ("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v14, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v15

    invoke-static {v15, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v14, v15, v10, v9, v11}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v12, 0x29

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    check-cast v12, Ljava/lang/CharSequence;

    .line 667
    invoke-direct {v7, v5, v12, v2}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)V

    .line 666
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 673
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->isService()Z

    move-result v7

    if-nez v7, :cond_4

    .line 674
    new-instance v7, Lcom/squareup/ui/cart/NameValuePair;

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-direct {v7, v5, v11}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 676
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->isService()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v5}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v5

    if-nez v5, :cond_5

    .line 677
    new-instance v5, Lcom/squareup/ui/cart/NameValuePair;

    iget-object v7, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v13

    invoke-static {v13, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7, v13, v10, v9, v11}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-direct {v5, v7, v11}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 681
    :cond_5
    :goto_0
    iget-object v5, v1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {v5}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/SortedMap;

    .line 682
    invoke-interface {v7}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/checkout/OrderModifier;

    .line 683
    move-object v13, v11

    check-cast v13, Ljava/lang/CharSequence;

    .line 685
    invoke-virtual {v12}, Lcom/squareup/checkout/OrderModifier;->isFreeModifier()Z

    move-result v14

    if-nez v14, :cond_7

    .line 686
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v14, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 687
    invoke-virtual {v12}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v14

    .line 688
    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v14, v6}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v14

    .line 689
    invoke-virtual {v14}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v14

    .line 690
    invoke-virtual {v14}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    check-cast v13, Ljava/lang/CharSequence;

    .line 693
    :cond_7
    new-instance v14, Lcom/squareup/ui/cart/NameValuePair;

    iget-object v15, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-virtual {v12, v15}, Lcom/squareup/checkout/OrderModifier;->getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v12

    check-cast v12, Ljava/lang/CharSequence;

    invoke-direct {v14, v12, v13, v2}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)V

    invoke-interface {v3, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 698
    :cond_8
    iget-object v5, v1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    check-cast v5, Ljava/lang/CharSequence;

    if-eqz v5, :cond_9

    invoke-static {v5}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    :cond_9
    const/4 v10, 0x1

    :cond_a
    if-nez v10, :cond_c

    .line 699
    iget-object v5, v1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    check-cast v5, Ljava/lang/CharSequence;

    iget-object v6, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->application:Landroid/app/Application;

    check-cast v6, Landroid/content/Context;

    sget-object v7, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 754
    new-instance v8, Lcom/squareup/fonts/FontSpan;

    invoke-static {v7, v9}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v7

    invoke-direct {v8, v6, v7}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v8, Landroid/text/style/CharacterStyle;

    .line 699
    invoke-static {v5, v8}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v5

    .line 700
    iget-boolean v6, v2, Lcom/squareup/ui/cart/Appearance;->strikethrough:Z

    if-eqz v6, :cond_b

    .line 701
    new-instance v6, Lcom/squareup/ui/cart/NameValuePair;

    new-instance v7, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v7}, Landroid/text/style/StrikethroughSpan;-><init>()V

    check-cast v7, Landroid/text/style/CharacterStyle;

    invoke-static {v5, v7}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-direct {v6, v5, v11}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 703
    :cond_b
    new-instance v6, Lcom/squareup/ui/cart/NameValuePair;

    check-cast v5, Ljava/lang/CharSequence;

    invoke-direct {v6, v5, v11}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 707
    :cond_c
    :goto_2
    invoke-virtual/range {p1 .. p2}, Lcom/squareup/checkout/CartItem;->shouldShowDiningOption(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 708
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v5

    if-eqz v5, :cond_d

    invoke-virtual {v5}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    move-object v6, v4

    check-cast v6, Ljava/lang/CharSequence;

    new-instance v7, Lcom/squareup/ui/cart/NameValuePair;

    invoke-direct {v7, v5, v6, v2}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/ui/cart/Appearance;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_d
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Required value was null."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 711
    :cond_e
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 712
    new-instance v2, Lcom/squareup/ui/cart/NameValuePair;

    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->voidReasonPhrased(Lcom/squareup/checkout/CartItem;)Ljava/lang/CharSequence;

    move-result-object v5

    move-object v6, v4

    check-cast v6, Ljava/lang/CharSequence;

    invoke-direct {v2, v5, v6}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 715
    :cond_f
    iget-object v2, v0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v2}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 716
    new-instance v2, Lcom/squareup/ui/cart/NameValuePair;

    invoke-direct/range {p0 .. p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->compReasonPhrased(Lcom/squareup/checkout/CartItem;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v2, v1, v4}, Lcom/squareup/ui/cart/NameValuePair;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 719
    :cond_10
    new-instance v1, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Ellipsized;

    invoke-direct {v1, v3}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Ellipsized;-><init>(Ljava/util/List;)V

    check-cast v1, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;

    return-object v1
.end method

.method private final voidReasonPhrased(Lcom/squareup/checkout/CartItem;)Ljava/lang/CharSequence;
    .locals 2

    .line 729
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->void_with_reason:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 730
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getVoidReason()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 731
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "res.phrase(R.string.void\u2026Reason)\n        .format()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public comp(Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 10

    const-string v0, "price"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 97
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->cart_comps:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/lang/CharSequence;

    .line 99
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v5

    .line 100
    sget-object v6, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 101
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v7

    const/4 v4, 0x1

    move-object v2, p0

    .line 96
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object v1

    .line 103
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v2

    .line 107
    iget-object v3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v7

    .line 108
    sget-object v8, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 109
    iget-object v3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v9

    const/4 v6, 0x1

    move-object v4, p0

    move-object v5, p1

    .line 104
    invoke-direct/range {v4 .. v9}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p1

    .line 111
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object v3

    .line 95
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public discount(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 9

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 121
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 124
    sget-object v5, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    move-object v1, p0

    move v3, p3

    .line 120
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 127
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    .line 131
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v6

    if-eqz p4, :cond_0

    .line 132
    sget-object p4, Lcom/squareup/ui/cart/Appearance;->WARNED:Lcom/squareup/ui/cart/Appearance;

    goto :goto_0

    :cond_0
    sget-object p4, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    :goto_0
    move-object v7, p4

    .line 133
    iget-object p4, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v8

    move-object v3, p0

    move-object v4, p2

    move v5, p3

    .line 128
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p2

    .line 135
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object p3

    .line 119
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public discountItem(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 8

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    :cond_0
    move-object v1, p2

    .line 144
    new-instance p2, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v5

    .line 149
    sget-object v6, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v7

    move-object v2, p0

    move-object v3, p1

    move v4, p3

    .line 145
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 152
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v6

    const-string v0, "negativePrice"

    .line 154
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v3

    .line 157
    sget-object v4, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getDiscount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v5

    move-object v0, p0

    move v2, p3

    .line 153
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p3

    .line 160
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object v0

    .line 144
    invoke-direct {p2, p1, v6, p3, v0}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object p2
.end method

.method public emptyCustomAmount(Lcom/squareup/checkout/CartItem;IZ)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 11

    const-string p2, "item"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    iget-object p2, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->defaultItemizationName:Ljava/lang/String;

    goto :goto_2

    :cond_2
    iget-object p2, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 170
    :goto_2
    invoke-static {p1, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactoryKt;->appearance(Lcom/squareup/checkout/CartItem;Z)Lcom/squareup/ui/cart/Appearance;

    move-result-object v0

    .line 171
    new-instance v8, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 173
    move-object v2, p2

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    .line 175
    iget-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 177
    iget-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    move-object v1, p0

    move-object v5, v0

    .line 172
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p2

    .line 179
    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    const-string v2, "item.quantity"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->quantity(Ljava/math/BigDecimal;Lcom/squareup/ui/cart/Appearance;)Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v9

    .line 181
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v2

    const-string v1, "item.total()"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 185
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    move-object v1, p0

    .line 180
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object v10

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    move v5, p3

    move-object v6, v0

    .line 187
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->subLabel(Lcom/squareup/checkout/CartItem;Ljava/lang/String;ZZLcom/squareup/ui/cart/Appearance;Z)Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object p1

    .line 171
    invoke-direct {v8, p2, v9, v10, p1}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v8
.end method

.method public lineItem(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 9

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 206
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v5

    .line 207
    sget-object v6, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 208
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v7

    move-object v2, p0

    move-object v3, p1

    move v4, p3

    .line 203
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 210
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    .line 214
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItemAmount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v6

    .line 215
    sget-object v7, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 216
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItemAmount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v8

    move-object v3, p0

    move-object v4, p2

    move v5, p3

    .line 211
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p2

    .line 218
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object p3

    .line 202
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public loyaltyPoints(ILjava/lang/Integer;Ljava/lang/String;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 8

    const-string v0, "formattedValue"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    .line 403
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object p2

    goto :goto_0

    .line 404
    :cond_0
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    new-instance v1, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;

    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-interface {v2, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {v1, p2}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;

    iget-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getSubLabelFontSize()F

    move-result p2

    const/4 v2, 0x1

    invoke-direct {v0, v1, p2, v2}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;FZ)V

    move-object p2, v0

    .line 407
    :goto_0
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 409
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x1

    .line 411
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getLoyaltyTitle()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 412
    sget-object v5, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 413
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getLoyaltyTitle()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    move-object v1, p0

    .line 408
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 415
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    .line 417
    move-object v3, p3

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x1

    .line 419
    iget-object p3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v5

    .line 420
    sget-object v6, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 421
    iget-object p3, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v7

    move-object v2, p0

    .line 416
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p3

    .line 407
    invoke-direct {v0, p1, v1, p3, p2}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public orderItem(Lcom/squareup/checkout/CartItem;ZLjava/lang/String;ZZZ)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 13

    move-object v7, p0

    move-object v6, p1

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    iget-object v0, v6, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->defaultItemizationName:Ljava/lang/String;

    goto :goto_2

    :cond_2
    iget-object v0, v6, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 230
    :goto_2
    invoke-static {p1, p2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactoryKt;->appearance(Lcom/squareup/checkout/CartItem;Z)Lcom/squareup/ui/cart/Appearance;

    move-result-object v8

    .line 231
    iget-object v1, v6, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v2, "item.selectedVariation"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 232
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    goto :goto_3

    .line 234
    :cond_3
    iget-object v1, v6, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    const-string v2, "item.quantity"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->quantity(Ljava/math/BigDecimal;Lcom/squareup/ui/cart/Appearance;)Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    :goto_3
    move-object v9, v1

    .line 236
    new-instance v10, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 238
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    .line 240
    iget-object v0, v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v3

    .line 242
    iget-object v0, v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItems()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v5

    move-object v0, p0

    move/from16 v2, p5

    move-object v4, v8

    .line 237
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object v11

    .line 246
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v0, "item.total()"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    iget-object v0, v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItemAmount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v3

    .line 250
    iget-object v0, v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getItemAmount()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v5

    move-object v0, p0

    .line 245
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object v12

    move-object v1, p1

    move-object/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move-object v5, v8

    move/from16 v6, p6

    .line 252
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->subLabel(Lcom/squareup/checkout/CartItem;Ljava/lang/String;ZZLcom/squareup/ui/cart/Appearance;Z)Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object v0

    .line 236
    invoke-direct {v10, v11, v9, v12, v0}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v10
.end method

.method public subTotal(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 9

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 269
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    .line 271
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getSubtotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 272
    sget-object v5, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 273
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getSubtotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    move-object v1, p0

    move v3, p3

    .line 268
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 275
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    .line 279
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getSubtotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v6

    .line 280
    sget-object v7, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 281
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getSubtotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v8

    move-object v3, p0

    move-object v4, p2

    move v5, p3

    .line 276
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p2

    .line 283
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object p3

    .line 267
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public surcharge(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 9

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 352
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 354
    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    .line 356
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getAutoGratuity()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 357
    sget-object v5, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 358
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getAutoGratuity()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    const/4 v3, 0x1

    move-object v1, p0

    .line 353
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 360
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    .line 364
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getAutoGratuity()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v6

    .line 365
    sget-object v7, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 366
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getAutoGratuity()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v8

    const/4 v5, 0x1

    move-object v3, p0

    move-object v4, p2

    .line 361
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p2

    .line 368
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object v2

    .line 352
    invoke-direct {v0, p1, v1, p2, v2}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public taxes(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 1

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 291
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->taxes(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    return-object p1
.end method

.method public taxes(Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 7

    const-string v0, "label"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 306
    iget-object v0, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_1

    if-eqz p4, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    .line 307
    :cond_1
    :goto_0
    iget-object p4, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTaxes()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    iget-object p4, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTaxes()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p4

    invoke-virtual {p4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v5

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p2

    .line 311
    :goto_1
    new-instance p4, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 315
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTaxes()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 316
    sget-object v5, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTaxes()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    .line 312
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 319
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object p3

    .line 321
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object v0

    .line 311
    invoke-direct {p4, p1, p3, p2, v0}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object p4
.end method

.method public tip(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 9

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 329
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 331
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    .line 333
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTip()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 334
    sget-object v5, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 335
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTip()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    move-object v1, p0

    move v3, p3

    .line 330
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 337
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    .line 341
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTip()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v6

    .line 342
    sget-object v7, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 343
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTip()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v8

    move-object v3, p0

    move-object v4, p2

    move v5, p3

    .line 338
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p2

    .line 345
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object p3

    .line 329
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public total(ILcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 9

    const-string v0, "price"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 379
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    .line 381
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 382
    sget-object v5, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 383
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    move-object v1, p0

    move v3, p3

    .line 378
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 385
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    .line 389
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v6

    .line 390
    sget-object v7, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 391
    iget-object v2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getTotal()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v8

    move-object v3, p0

    move-object v4, p2

    move v5, p3

    .line 386
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->amount(Lcom/squareup/protos/common/Money;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Amount;

    move-result-object p2

    .line 393
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->noModifiers()Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    move-result-object p3

    .line 377
    invoke-direct {v0, p1, v1, p2, p3}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method

.method public unappliedCoupon(ILjava/lang/String;)Lcom/squareup/ui/cart/CartEntryViewModel;
    .locals 7

    const-string v0, "subtitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 431
    new-instance v0, Lcom/squareup/ui/cart/CartEntryViewModel;

    .line 433
    iget-object v1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    .line 435
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getLoyaltyTitle()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getWeight()Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object v4

    .line 436
    sget-object v5, Lcom/squareup/ui/cart/Appearance;->DEFAULT:Lcom/squareup/ui/cart/Appearance;

    .line 437
    iget-object p1, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getLoyaltyTitle()Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;->getColor()I

    move-result v6

    const/4 v3, 0x1

    move-object v1, p0

    .line 432
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->name(Ljava/lang/CharSequence;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/ui/cart/Appearance;I)Lcom/squareup/ui/cart/CartEntryViewModel$Name;

    move-result-object p1

    .line 439
    invoke-direct {p0}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->hideQuantity()Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;

    move-result-object v1

    .line 441
    new-instance v2, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;

    new-instance v3, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {v3, p2}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue$Static;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;

    iget-object p2, p0, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;->config:Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    invoke-virtual {p2}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;->getSubLabelFontSize()F

    move-result p2

    const/4 v4, 0x1

    invoke-direct {v2, v3, p2, v4}, Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$SubLabelValue;FZ)V

    const/4 p2, 0x0

    .line 431
    invoke-direct {v0, p1, v1, p2, v2}, Lcom/squareup/ui/cart/CartEntryViewModel;-><init>(Lcom/squareup/ui/cart/CartEntryViewModel$Name;Lcom/squareup/ui/cart/CartEntryViewModel$Quantity;Lcom/squareup/ui/cart/CartEntryViewModel$Amount;Lcom/squareup/ui/cart/CartEntryViewModel$SubLabel;)V

    return-object v0
.end method
