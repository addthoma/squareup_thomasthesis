.class public final Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "CartTaxesScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final cartEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;"
        }
    .end annotation
.end field

.field private final cartScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final taxFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/TaxFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/TaxFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->cartEditorProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->cartScreenRunnerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p4, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p5, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p6, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p7, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->taxFormatterProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p8, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p9, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p10, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/TaxFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;)",
            "Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;"
        }
    .end annotation

    .line 76
    new-instance v11, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/cart/TaxFormatter;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/log/cart/TransactionInteractionsLogger;)Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;
    .locals 12

    .line 84
    new-instance v11, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/cart/TaxFormatter;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/log/cart/TransactionInteractionsLogger;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;
    .locals 11

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->cartEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/cart/CartFeesModel$Session;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->cartScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/cart/CartScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->taxFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/cart/TaxFormatter;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->newInstance(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/cart/TaxFormatter;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/log/cart/TransactionInteractionsLogger;)Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartTaxesScreen_Presenter_Factory;->get()Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
