.class public abstract Lcom/squareup/ui/cart/CartEntryModule;
.super Ljava/lang/Object;
.source "CartEntryModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartEntryModule$Checkout;,
        Lcom/squareup/ui/cart/CartEntryModule$Cart;,
        Lcom/squareup/ui/cart/CartEntryModule$SplitTicket;,
        Lcom/squareup/ui/cart/CartEntryModule$TwoTone;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCartCartEntryViewModelFactory(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .locals 14
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 84
    new-instance v11, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    sget v0, Lcom/squareup/marin/R$dimen;->marin_text_default:I

    move-object/from16 v12, p3

    .line 85
    invoke-interface {v12, v0}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    new-instance v2, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v2, v0, v3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v3, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v3, v0, v4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v4, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v4, v0, v5}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v5, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v5, v0, v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v6, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v7, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v6, v0, v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v8, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v7, v0, v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v8, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v9, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v8, v0, v9}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v9, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v10, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v9, v0, v10}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v10, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/noho/R$color;->noho_standard_cyan:I

    sget-object v13, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v10, v0, v13}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;-><init>(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)V

    .line 97
    new-instance v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object v5, v11

    move-object/from16 v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;-><init>(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;Lcom/squareup/text/DurationFormatter;)V

    return-object v7
.end method

.method static provideSplitTicketCartEntryViewModelFactory(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .locals 14
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 58
    new-instance v11, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    sget v0, Lcom/squareup/marin/R$dimen;->marin_text_help:I

    move-object/from16 v12, p3

    .line 59
    invoke-interface {v12, v0}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    new-instance v2, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v2, v0, v3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v3, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v3, v0, v4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v4, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v4, v0, v5}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v5, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v5, v0, v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v6, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v7, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v6, v0, v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v8, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v7, v0, v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v8, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v9, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v8, v0, v9}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v9, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v10, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v9, v0, v10}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v10, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/noho/R$color;->noho_standard_cyan:I

    sget-object v13, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v10, v0, v13}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;-><init>(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)V

    .line 71
    new-instance v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object v5, v11

    move-object/from16 v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;-><init>(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;Lcom/squareup/text/DurationFormatter;)V

    return-object v7
.end method

.method static provideTwoToneCartEntryViewModelFactory(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;
    .locals 14
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 32
    new-instance v11, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;

    sget v0, Lcom/squareup/marin/R$dimen;->marin_text_default:I

    move-object/from16 v12, p3

    .line 33
    invoke-interface {v12, v0}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    new-instance v2, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v2, v0, v3}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v3, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v3, v0, v4}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v4, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v4, v0, v5}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v5, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v5, v0, v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v6, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    sget-object v7, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v6, v0, v7}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    sget-object v8, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v7, v0, v8}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v8, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    sget-object v9, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v8, v0, v9}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v9, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    sget-object v10, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v9, v0, v10}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    new-instance v10, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;

    sget v0, Lcom/squareup/noho/R$color;->noho_standard_cyan:I

    sget-object v13, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v10, v0, v13}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;-><init>(ILcom/squareup/marketfont/MarketFont$Weight;)V

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;-><init>(FLcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config$Style;)V

    .line 45
    new-instance v7, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object v5, v11

    move-object/from16 v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/cart/RealCartEntryViewModelFactory;-><init>(Landroid/app/Application;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/RealCartEntryViewModelFactory$Config;Lcom/squareup/text/DurationFormatter;)V

    return-object v7
.end method


# virtual methods
.method abstract provideCheckoutCartEntryViewInflater(Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater$CheckoutCartEntryViewInflater;)Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
