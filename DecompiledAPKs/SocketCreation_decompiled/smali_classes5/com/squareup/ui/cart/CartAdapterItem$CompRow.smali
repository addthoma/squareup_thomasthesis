.class public Lcom/squareup/ui/cart/CartAdapterItem$CompRow;
.super Ljava/lang/Object;
.source "CartAdapterItem.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartAdapterItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CompRow"
.end annotation


# instance fields
.field public final money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    iput-object p1, p0, Lcom/squareup/ui/cart/CartAdapterItem$CompRow;->money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .line 214
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->COMP:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->access$000(Lcom/squareup/ui/cart/CartAdapterItem$RowType;)I

    move-result v0

    return v0
.end method

.method public getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 210
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->COMP:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object v0
.end method
