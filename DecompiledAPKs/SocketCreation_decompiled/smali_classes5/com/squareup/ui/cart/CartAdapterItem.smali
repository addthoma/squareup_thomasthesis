.class public interface abstract Lcom/squareup/ui/cart/CartAdapterItem;
.super Ljava/lang/Object;
.source "CartAdapterItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$TicketLineRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$TotalRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$SurchargeRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$CompRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$EmptyCustomAmountRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$CartRowItem;,
        Lcom/squareup/ui/cart/CartAdapterItem$TicketNoteRow;,
        Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    }
.end annotation


# virtual methods
.method public abstract getId()I
.end method

.method public abstract getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;
.end method
