.class Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;
.super Ljava/lang/Object;
.source "DiningOptionViewPager.java"

# interfaces
.implements Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;-><init>(Lcom/squareup/ui/cart/DiningOptionViewPager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;

.field final synthetic val$pager:Landroidx/viewpager/widget/ViewPager;

.field final synthetic val$this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;Lcom/squareup/ui/cart/DiningOptionViewPager;Landroidx/viewpager/widget/ViewPager;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->this$1:Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;

    iput-object p2, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->val$this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;

    iput-object p3, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->val$pager:Landroidx/viewpager/widget/ViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 3

    if-eqz p1, :cond_0

    return-void

    .line 109
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->this$1:Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->getCount()I

    move-result p1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->val$pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->val$pager:Landroidx/viewpager/widget/ViewPager;

    add-int/lit8 p1, p1, -0x2

    invoke-virtual {v0, p1, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    sub-int/2addr p1, v2

    if-ne v0, p1, :cond_2

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->val$pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1, v2, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    return-void
.end method

.method public onPageSelected(I)V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->this$1:Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->this$0:Lcom/squareup/ui/cart/DiningOptionViewPager;

    iget-object v0, v0, Lcom/squareup/ui/cart/DiningOptionViewPager;->presenter:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    iget-object v1, p0, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter$1;->this$1:Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;

    invoke-static {v1, p1}, Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;->access$000(Lcom/squareup/ui/cart/DiningOptionViewPager$DiningOptionPagerAdapter;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->setCurrentDiningOption(I)V

    return-void
.end method
