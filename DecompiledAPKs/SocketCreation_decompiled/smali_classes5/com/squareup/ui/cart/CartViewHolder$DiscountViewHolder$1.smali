.class Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;)V
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->access$100(Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;->getAdapterPosition()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->onDiscountRowClicked(I)V

    return-void
.end method
