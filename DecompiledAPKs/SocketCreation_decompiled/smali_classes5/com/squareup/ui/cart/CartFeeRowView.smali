.class Lcom/squareup/ui/cart/CartFeeRowView;
.super Landroid/widget/LinearLayout;
.source "CartFeeRowView.java"


# instance fields
.field private final deleteButton:Lcom/squareup/glyph/SquareGlyphView;

.field private final feeView:Landroid/widget/TextView;

.field private final presenter:Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

.field private final preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

.field private row:Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

.field private final warningText:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/squareup/ui/cart/AbstractCartFeesPresenter;)V
    .locals 1

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/cart/CartFeeRowView;->presenter:Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    .line 42
    sget v0, Lcom/squareup/orderentry/R$layout;->item_fee_list_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/cart/CartFeeRowView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 44
    sget p1, Lcom/squareup/orderentry/R$id;->preserved_label:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/PreservedLabelView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    .line 45
    sget p1, Lcom/squareup/orderentry/R$id;->fee:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView;->feeView:Landroid/widget/TextView;

    .line 46
    sget p1, Lcom/squareup/orderentry/R$id;->delete:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView;->deleteButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView;->deleteButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v0, Lcom/squareup/ui/cart/CartFeeRowView$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/cart/CartFeeRowView$1;-><init>(Lcom/squareup/ui/cart/CartFeeRowView;Lcom/squareup/ui/cart/AbstractCartFeesPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    sget p1, Lcom/squareup/orderentry/R$id;->warning:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView;->warningText:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/cart/CartFeeRowView;)Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/cart/CartFeeRowView;->row:Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/cart/CartFeeRowView;)Lcom/squareup/ui/cart/AbstractCartFeesPresenter;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/cart/CartFeeRowView;->presenter:Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    return-object p0
.end method

.method private fadeIn()V
    .locals 3

    const/4 v0, 0x0

    .line 99
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartFeeRowView;->setAlpha(F)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartFeeRowView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 101
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 102
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartFeeRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10e0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 103
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private getContentDescription(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Ljava/lang/String;
    .locals 3

    .line 107
    new-instance v0, Lcom/squareup/util/Res$RealRes;

    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartFeeRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    .line 109
    iget-object v1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    instance-of v1, v1, Lcom/squareup/checkout/Discount;

    const-string v2, "rowname"

    if-eqz v1, :cond_0

    .line 110
    sget v1, Lcom/squareup/orderentry/R$string;->remove_discount_content_description:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 111
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 113
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 114
    :cond_0
    iget-object v1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->fee:Lcom/squareup/checkout/Adjustment;

    instance-of v1, v1, Lcom/squareup/checkout/Tax;

    if-eqz v1, :cond_1

    .line 115
    sget v1, Lcom/squareup/orderentry/R$string;->remove_tax_content_description:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 116
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 118
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method doDelete()V
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeeRowView;->presenter:Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeeRowView;->row:Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->prepDelete(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 87
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartFeeRowView;->setAlpha(F)V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartFeeRowView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 89
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartFeeRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10e0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/CartFeeRowView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/CartFeeRowView$2;-><init>(Lcom/squareup/ui/cart/CartFeeRowView;)V

    .line 91
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method enter()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeeRowView;->presenter:Lcom/squareup/ui/cart/AbstractCartFeesPresenter;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartFeeRowView;->row:Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->isRestoring(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartFeeRowView;->fadeIn()V

    goto :goto_0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 79
    invoke-virtual {p0, v0}, Lcom/squareup/ui/cart/CartFeeRowView;->setAlpha(F)V

    :goto_0
    return-void
.end method

.method init(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;Lcom/squareup/text/Formatter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;",
            ">;)V"
        }
    .end annotation

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/cart/CartFeeRowView;->row:Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeeRowView;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->applicableQuantity:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeeRowView;->preservedLabelView:Lcom/squareup/widgets/PreservedLabelView;

    iget-object v1, p1, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->applicableQuantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartFeeRowView;->feeView:Landroid/widget/TextView;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object p2, p0, Lcom/squareup/ui/cart/CartFeeRowView;->deleteButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartFeeRowView;->getContentDescription(Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 71
    iget-object p2, p0, Lcom/squareup/ui/cart/CartFeeRowView;->warningText:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->getWarning()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object p2, p0, Lcom/squareup/ui/cart/CartFeeRowView;->warningText:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeesModel$CartFeeRow;->getWarning()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
