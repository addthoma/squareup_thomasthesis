.class public final synthetic Lcom/squareup/ui/cart/-$$Lambda$S3ePA-jbPVR0gfjtAEOBMh43zcU;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/cart/-$$Lambda$S3ePA-jbPVR0gfjtAEOBMh43zcU;->f$0:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/cart/-$$Lambda$S3ePA-jbPVR0gfjtAEOBMh43zcU;->f$0:Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;

    check-cast p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/DiningOptionViewPagerPresenter;->onCurrentDiningOptionChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V

    return-void
.end method
