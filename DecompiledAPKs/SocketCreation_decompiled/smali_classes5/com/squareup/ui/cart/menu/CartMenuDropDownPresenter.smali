.class public Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;
.super Lmortar/ViewPresenter;
.source "CartMenuDropDownPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;",
        ">;"
    }
.end annotation


# instance fields
.field private final cartMenuPresenter:Lcom/squareup/ui/cart/menu/CartMenuPresenter;

.field private dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/menu/CartMenuPresenter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->cartMenuPresenter:Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 40
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->closeDropDown()V

    return-void
.end method

.method public hasAtLeastOneEnabledOption()Z
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->cartMenuPresenter:Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->hasAtLeastOneEnabledOption()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onLoad$0$CartMenuDropDownPresenter()V
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->closeDropDown()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 25
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 26
    iget-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->cartMenuPresenter:Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    new-instance v0, Lcom/squareup/ui/cart/menu/-$$Lambda$CartMenuDropDownPresenter$NNzG_bl63eMZTT8NT9LFDWRYo10;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/menu/-$$Lambda$CartMenuDropDownPresenter$NNzG_bl63eMZTT8NT9LFDWRYo10;-><init>(Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->setListener(Lcom/squareup/ui/cart/menu/CartMenuView$Listener;)V

    .line 27
    iget-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz p1, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    :cond_0
    return-void
.end method

.method public setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V
    .locals 1

    .line 48
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    goto :goto_0

    .line 51
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    :goto_0
    return-void
.end method

.method public toggle()V
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->isDropDownVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->cartMenuPresenter:Lcom/squareup/ui/cart/menu/CartMenuPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->updateOptions()V

    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->toggleDropDown()V

    return-void
.end method
