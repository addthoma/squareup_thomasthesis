.class public Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;
.super Lcom/squareup/ui/DropDownContainer;
.source "CartMenuDropDownContainer.java"


# instance fields
.field presenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/DropDownContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 27
    new-instance p1, Landroid/animation/LayoutTransition;

    invoke-direct {p1}, Landroid/animation/LayoutTransition;-><init>()V

    const/4 p2, 0x0

    const/4 v0, 0x2

    .line 29
    invoke-virtual {p1, v0, p2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    const/4 v1, 0x3

    .line 30
    invoke-virtual {p1, v1, p2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    const-wide/16 v2, 0x0

    .line 31
    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 32
    invoke-virtual {p1, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 33
    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 34
    invoke-virtual {p1, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const-wide/16 v4, 0x7d

    .line 35
    invoke-virtual {p1, v0, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 36
    invoke-virtual {p1, v1, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 38
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v1, p2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 39
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v0, p2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 40
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 41
    new-instance p2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1, p2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 43
    invoke-virtual {p1, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 44
    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 45
    invoke-virtual {p1, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 46
    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 47
    invoke-virtual {p1, v1, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 48
    invoke-virtual {p1, v0, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 50
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/orderentry/R$string;->cart_menu_drop_down_content_description:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 53
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected inject()V
    .locals 2

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/cart/CartComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartComponent;

    invoke-interface {v0, p0}, Lcom/squareup/ui/cart/CartComponent;->inject(Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->presenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->dropView(Ljava/lang/Object;)V

    .line 68
    invoke-super {p0}, Lcom/squareup/ui/DropDownContainer;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 62
    invoke-super {p0}, Lcom/squareup/ui/DropDownContainer;->onFinishInflate()V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;->presenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
