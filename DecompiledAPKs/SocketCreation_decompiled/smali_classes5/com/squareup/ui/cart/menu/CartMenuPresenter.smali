.class public Lcom/squareup/ui/cart/menu/CartMenuPresenter;
.super Lmortar/ViewPresenter;
.source "CartMenuPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/cart/menu/CartMenuView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cartScreenFinisher:Lcom/squareup/ui/cart/CartScreenFinisher;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final device:Lcom/squareup/util/Device;

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private final flow:Lflow/Flow;

.field private listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

.field private final openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;

.field private final ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/util/Device;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/ui/cart/CartScreenFinisher;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 93
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v1, p1

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p2

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    move-object v1, p3

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->eventSink:Lcom/squareup/badbus/BadEventSink;

    move-object v1, p4

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    move-object v1, p5

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    move-object v1, p6

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->device:Lcom/squareup/util/Device;

    move-object v1, p7

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    move-object v1, p8

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    move-object v1, p9

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->res:Lcom/squareup/util/Res;

    move-object v1, p10

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p11

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    move-object v1, p12

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->cartScreenFinisher:Lcom/squareup/ui/cart/CartScreenFinisher;

    move-object v1, p13

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object/from16 v1, p14

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object/from16 v1, p15

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    move-object/from16 v1, p16

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    move-object/from16 v1, p17

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/cart/menu/CartMenuPresenter;)Lflow/Flow;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    return-object p0
.end method

.method private getClearSaleText()Ljava/lang/String;
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_clear_new_items:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->clear_items:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 377
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->clear_sale:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hasReceiptPrinter()Z
    .locals 4

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method private hasTicketPrinter()Z
    .locals 4

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    return v0
.end method

.method private maybeShowOpenTicketsHomeScreen()V
    .locals 2

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->forLoadTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private printVoidTicketIfEnabled()V
    .locals 4

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 391
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getCurrentTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getUnsavedVoidedItems()Ljava/util/List;

    move-result-object v3

    .line 390
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/print/OrderPrintingDispatcher;->printVoidTicket(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private saveCartToTicket()V
    .locals 2

    .line 396
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->updateCurrentTicketBeforeReset()V

    .line 398
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->setPendingTicketSavedAlert()V

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;

    invoke-direct {v1}, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void
.end method


# virtual methods
.method hasAtLeastOneEnabledOption()Z
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isClearSaleEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isEditTicketEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowAddCustomer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowViewCustomer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isPrintBillEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isMergeTicketEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isMoveTicketEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isSplitTicketEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method isClearSaleEnabled()Z
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasNonLockedItems()Z

    move-result v0

    return v0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method isEditTicketEnabled()Z
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowEditTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isMergeTicketEnabled()Z
    .locals 2

    .line 170
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowMergeTicket()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 171
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->ticketCountsCache:Lcom/squareup/tickets/TicketCountsCache;

    .line 172
    invoke-virtual {v0}, Lcom/squareup/tickets/TicketCountsCache;->getAllTicketsCount()I

    move-result v0

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method isMoveTicketEnabled()Z
    .locals 1

    .line 180
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowMoveTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isPrintBillEnabled()Z
    .locals 1

    .line 162
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowPrintBill()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isReprintTicketEnabled()Z
    .locals 1

    .line 190
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowReprintTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isSplitTicketEnabled()Z
    .locals 1

    .line 199
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowSplitTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCartItemCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isTransferTicketEnabled()Z
    .locals 1

    .line 208
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowTransferTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method onClearSaleClicked()V
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->removeUnlockedItems()V

    goto :goto_0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->preserveCustomerOnNextReset()V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->ORDER_ENTRY_CLEAR_SALE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->returnAllCoupons()V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    .line 253
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->cartScreenFinisher:Lcom/squareup/ui/cart/CartScreenFinisher;

    invoke-interface {v0}, Lcom/squareup/ui/cart/CartScreenFinisher;->finishCartScreen()V

    return-void
.end method

.method onCustomerClicked()V
    .locals 8

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/seller/SellerScope;->INSTANCE:Lcom/squareup/ui/seller/SellerScope;

    iget-object v2, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v3, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    .line 266
    invoke-static {v1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScope;->newViewInTransactionCrmScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/crm/flow/InCrmScope;

    move-result-object v1

    .line 265
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/seller/SellerScope;->INSTANCE:Lcom/squareup/ui/seller/SellerScope;

    sget-object v2, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->ORDER_ENTRY:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->crm_add_customer_title:I

    sget-object v4, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->REQUIRE_AT_LEAST_ONE_FIELD:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    sget-object v5, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->start(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method onEditTicketClicked()V
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/ui/ticket/TicketDetailScreen;->forEditingTransactionTicket()Lcom/squareup/ui/ticket/TicketDetailScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 114
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->updateOptions()V

    return-void
.end method

.method onMergeTicketClicked()V
    .locals 2

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_MERGE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 311
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/ui/ticket/TicketListScreen;->forMergeTransactionTicket()Lcom/squareup/ui/ticket/TicketListScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onMoveTicketClicked()V
    .locals 6

    .line 316
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_MOVE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 318
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 319
    new-instance v1, Lcom/squareup/ui/ticket/TicketInfo;

    iget-object v2, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTicketId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 320
    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/ui/ticket/TicketInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 319
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v1}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 323
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/ui/ticket/MoveTicketScreen;

    invoke-direct {v2, v0}, Lcom/squareup/ui/ticket/MoveTicketScreen;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onPrintBillClicked()V
    .locals 3

    .line 283
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->hasReceiptPrinter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    .line 285
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order;->markBillPrinted(Ljava/util/Date;)V

    .line 288
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v1

    if-nez v1, :cond_1

    .line 290
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forSavingTransactionToNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->PRINT_BILL:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    .line 291
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->afterAction(Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    .line 292
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildNewTicketScreen()Lcom/squareup/ui/ticket/NewTicketScreen;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 295
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildTicketDetailScreen()Lcom/squareup/ui/ticket/TicketDetailScreen;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 297
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    return-void

    .line 301
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printBillStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 302
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->printVoidTicketIfEnabled()V

    .line 303
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->saveCartToTicket()V

    .line 305
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 306
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->maybeShowOpenTicketsHomeScreen()V

    return-void
.end method

.method onReprintTicketClicked()V
    .locals 3

    .line 327
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->hasTicketPrinter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintSavedTicketFromTransaction(Lcom/squareup/payment/Transaction;)V

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 332
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v2

    .line 331
    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 334
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->printVoidTicketIfEnabled()V

    .line 335
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->saveCartToTicket()V

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 338
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->maybeShowOpenTicketsHomeScreen()V

    return-void
.end method

.method onSplitTicketClicked()V
    .locals 2

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->SPLIT_TICKET_START:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/ticket/SplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/SplitTicketScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 348
    :cond_0
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketScreen;->forSavingTransactionToNewTicket()Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->SPLIT_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    .line 349
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->afterAction(Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)Lcom/squareup/ui/ticket/EditTicketScreen$Builder;

    move-result-object v0

    .line 350
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 351
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildNewTicketScreen()Lcom/squareup/ui/ticket/NewTicketScreen;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 353
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->buildTicketDetailScreen()Lcom/squareup/ui/ticket/TicketDetailScreen;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method onTransferTicketClicked()V
    .locals 3

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->OPEN_TICKETS_VIEW_TRANSFER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    invoke-interface {v0}, Lcom/squareup/ui/cart/menu/CartMenuView$Listener;->closeMenu()V

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_MANAGE_ALL:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/cart/menu/CartMenuPresenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter$1;-><init>(Lcom/squareup/ui/cart/menu/CartMenuPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public setListener(Lcom/squareup/ui/cart/menu/CartMenuView$Listener;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->listener:Lcom/squareup/ui/cart/menu/CartMenuView$Listener;

    return-void
.end method

.method shouldShowAddCustomer()Z
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method shouldShowEditTicket()Z
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    return v0
.end method

.method shouldShowMergeTicket()Z
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isMergeTicketsAllowed()Z

    move-result v0

    return v0
.end method

.method shouldShowMoveTicket()Z
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isMoveTicketsAllowed()Z

    move-result v0

    return v0
.end method

.method shouldShowPrintBill()Z
    .locals 1

    .line 158
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->hasReceiptPrinter()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method shouldShowReprintTicket()Z
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->hasTicketPrinter()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method shouldShowSplitTicket()Z
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isSplitTicketsAllowed()Z

    move-result v0

    return v0
.end method

.method shouldShowTransferTicket()Z
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isTicketTransferAllowed()Z

    move-result v0

    return v0
.end method

.method shouldShowViewCustomer()Z
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method updateOptions()V
    .locals 2

    .line 212
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/menu/CartMenuView;

    .line 215
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isClearSaleEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setClearSaleEnabled(Z)V

    .line 216
    invoke-direct {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->getClearSaleText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setClearSaleText(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowEditTicket()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setEditTicketVisible(Z)V

    .line 219
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isEditTicketEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setEditTicketEnabled(Z)V

    .line 221
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowAddCustomer()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setAddCustomerVisible(Z)V

    .line 222
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowViewCustomer()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setViewCustomerVisible(Z)V

    .line 224
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowPrintBill()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setPrintBillVisible(Z)V

    .line 225
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isPrintBillEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setPrintBillEnabled(Z)V

    .line 227
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowMergeTicket()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setMergeTicketVisible(Z)V

    .line 228
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isMergeTicketEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setMergeTicketEnabled(Z)V

    .line 230
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowMoveTicket()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setMoveTicketVisible(Z)V

    .line 231
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isMoveTicketEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setMoveTicketEnabled(Z)V

    .line 233
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowReprintTicket()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setReprintTicketVisible(Z)V

    .line 234
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isReprintTicketEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setReprintTicketEnabled(Z)V

    .line 236
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowSplitTicket()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setSplitTicketVisible(Z)V

    .line 237
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isSplitTicketEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setSplitTicketEnabled(Z)V

    .line 239
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->shouldShowTransferTicket()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setTransferTicketVisible(Z)V

    .line 240
    invoke-virtual {p0}, Lcom/squareup/ui/cart/menu/CartMenuPresenter;->isTransferTicketEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuView;->setTransferTicketEnabled(Z)V

    return-void
.end method
