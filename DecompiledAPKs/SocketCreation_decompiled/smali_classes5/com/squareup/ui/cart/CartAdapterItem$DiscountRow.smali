.class public Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;
.super Ljava/lang/Object;
.source "CartAdapterItem.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartAdapterItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DiscountRow"
.end annotation


# instance fields
.field public final hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem:Z

.field public final isDiscountEvent:Z

.field public final money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;ZZ)V
    .locals 0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput-object p1, p0, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;->money:Lcom/squareup/protos/common/Money;

    .line 187
    iput-boolean p2, p0, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;->hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem:Z

    .line 188
    iput-boolean p3, p0, Lcom/squareup/ui/cart/CartAdapterItem$DiscountRow;->isDiscountEvent:Z

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .line 196
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->DISCOUNTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->access$000(Lcom/squareup/ui/cart/CartAdapterItem$RowType;)I

    move-result v0

    return v0
.end method

.method public getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 192
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->DISCOUNTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object v0
.end method
