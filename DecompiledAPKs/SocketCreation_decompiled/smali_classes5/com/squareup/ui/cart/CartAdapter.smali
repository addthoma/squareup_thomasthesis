.class Lcom/squareup/ui/cart/CartAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "CartAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/cart/CartViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

.field private cartItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;"
        }
    .end annotation
.end field

.field private final cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 1

    .line 38
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/cart/CartAdapter;->features:Lcom/squareup/settings/server/Features;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    const/4 p1, 0x1

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartAdapter;->setHasStableIds(Z)V

    return-void
.end method

.method private getRowLayout(Lcom/squareup/ui/cart/CartAdapterItem$RowType;)I
    .locals 1

    .line 106
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_NOTE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    if-ne p1, v0, :cond_0

    .line 107
    sget p1, Lcom/squareup/orderentry/R$layout;->cart_ticket_note_list_row:I

    return p1

    .line 108
    :cond_0
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_LINE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    if-ne p1, v0, :cond_1

    .line 109
    sget p1, Lcom/squareup/orderentry/R$layout;->cart_ticket_line_row:I

    return p1

    .line 111
    :cond_1
    sget p1, Lcom/squareup/orderentry/R$layout;->cart_sale_list_row:I

    return p1
.end method


# virtual methods
.method getCartItem(I)Lcom/squareup/ui/cart/CartAdapterItem;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem;

    return-object p1
.end method

.method public getItemCount()I
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem;

    invoke-interface {p1}, Lcom/squareup/ui/cart/CartAdapterItem;->getId()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem;

    invoke-interface {p1}, Lcom/squareup/ui/cart/CartAdapterItem;->getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->getValue()I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/ui/cart/CartViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/cart/CartAdapter;->onBindViewHolder(Lcom/squareup/ui/cart/CartViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/cart/CartViewHolder;I)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/cart/CartAdapterItem;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/cart/CartViewHolder;->bind(Lcom/squareup/ui/cart/CartAdapterItem;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/cart/CartAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/cart/CartViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/cart/CartViewHolder;
    .locals 3

    .line 57
    invoke-static {p2}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->fromValue(I)Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    move-result-object p2

    .line 58
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 59
    invoke-direct {p0, p2}, Lcom/squareup/ui/cart/CartAdapter;->getRowLayout(Lcom/squareup/ui/cart/CartAdapterItem$RowType;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 61
    sget-object v1, Lcom/squareup/ui/cart/CartAdapter$1;->$SwitchMap$com$squareup$ui$cart$CartAdapterItem$RowType:[I

    invoke-virtual {p2}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->ordinal()I

    move-result p2

    aget p2, v1, p2

    packed-switch p2, :pswitch_data_0

    .line 85
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unknown viewType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 83
    :pswitch_0
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2, v1}, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 81
    :pswitch_1
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2, v1}, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 79
    :pswitch_2
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$NoopViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2}, Lcom/squareup/ui/cart/CartViewHolder$NoopViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 77
    :pswitch_3
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$TotalViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2}, Lcom/squareup/ui/cart/CartViewHolder$TotalViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 75
    :pswitch_4
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$SurchargeViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2}, Lcom/squareup/ui/cart/CartViewHolder$SurchargeViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 73
    :pswitch_5
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2, v1}, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 71
    :pswitch_6
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$CompViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2}, Lcom/squareup/ui/cart/CartViewHolder$CompViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 69
    :pswitch_7
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2, v1}, Lcom/squareup/ui/cart/CartViewHolder$DiscountViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 67
    :pswitch_8
    new-instance p2, Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p2, v0, p1, v1}, Lcom/squareup/ui/cart/CartViewHolder$EmptyCustomAmountViewHolder;-><init>(Landroid/view/View;Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p2

    .line 65
    :pswitch_9
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->features:Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, p2, v0, v1, v2}, Lcom/squareup/ui/cart/CartViewHolder$CartItemViewHolder;-><init>(Lcom/squareup/settings/server/Features;Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    .line 63
    :pswitch_a
    new-instance p1, Lcom/squareup/ui/cart/CartViewHolder$TicketNoteViewHolder;

    iget-object p2, p0, Lcom/squareup/ui/cart/CartAdapter;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    invoke-direct {p1, v0, p2}, Lcom/squareup/ui/cart/CartViewHolder$TicketNoteViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method setCartItems(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/cart/CartDiff;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/cart/CartDiff;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v0}, Landroidx/recyclerview/widget/DiffUtil;->calculateDiff(Landroidx/recyclerview/widget/DiffUtil$Callback;)Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 48
    iget-object v1, p0, Lcom/squareup/ui/cart/CartAdapter;->cartItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 49
    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method
