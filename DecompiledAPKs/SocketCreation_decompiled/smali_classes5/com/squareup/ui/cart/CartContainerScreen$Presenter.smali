.class Lcom/squareup/ui/cart/CartContainerScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CartContainerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartContainerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/cart/CartContainerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final barcodeScannedListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field caretViewForActionbar:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

.field private final cartDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneCartScreenFinisher:Lcom/squareup/ui/cart/CartScreenFinisher$Phone;

.field private final res:Lcom/squareup/util/Res;

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/ui/cart/CartScreenFinisher$Phone;Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;",
            "Lcom/squareup/ui/cart/CartScreenFinisher$Phone;",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 78
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 80
    iput-object p2, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    .line 81
    iput-object p3, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 82
    iput-object p4, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 83
    iput-object p5, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 84
    iput-object p6, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    .line 85
    iput-object p7, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->cartDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    .line 86
    iput-object p8, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->phoneCartScreenFinisher:Lcom/squareup/ui/cart/CartScreenFinisher$Phone;

    .line 87
    iput-object p10, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    .line 88
    iput-object p11, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    .line 89
    new-instance p1, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$eyX7-RAtuJmuBhprrL4Kyf--8MI;

    invoke-direct {p1, p0, p9}, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$eyX7-RAtuJmuBhprrL4Kyf--8MI;-><init>(Lcom/squareup/ui/cart/CartContainerScreen$Presenter;Lcom/squareup/ui/seller/SellerScopeRunner;)V

    iput-object p1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->barcodeScannedListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    return-void
.end method

.method private finishCartScreen()V
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->cartDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->close()V

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->phoneCartScreenFinisher:Lcom/squareup/ui/cart/CartScreenFinisher$Phone;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartScreenFinisher$Phone;->finishCartScreen()V

    return-void
.end method

.method private isUnobscured()Z
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    sget-object v1, Lcom/squareup/ui/cart/CartContainerScreen;->INSTANCE:Lcom/squareup/ui/cart/CartContainerScreen;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/TopScreenChecker;->isUnobscured(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$8Gh_Z4zFoQyFElbBzLtCW792e80(Lcom/squareup/ui/cart/CartContainerScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->finishCartScreen()V

    return-void
.end method

.method private onCartChanged()V
    .locals 1

    .line 132
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->updateActionBar(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    :cond_0
    return-void
.end method

.method private onTicketSaved()V
    .locals 1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->finishCartScreen()V

    :cond_0
    return-void
.end method

.method private updateActionBar(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_cart_total:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 164
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    goto :goto_0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->cart_total:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 168
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "amount"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 169
    iget-object v1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->cartDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    .line 170
    invoke-virtual {v0}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->hasAtLeastOneEnabledOption()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomViewEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 169
    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/ui/cart/CartContainerView;)V
    .locals 2

    .line 149
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    .line 150
    iput-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->caretViewForActionbar:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->barcodeScannedListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 153
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/ui/cart/CartContainerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->dropView(Lcom/squareup/ui/cart/CartContainerView;)V

    return-void
.end method

.method public synthetic lambda$new$0$CartContainerScreen$Presenter(Lcom/squareup/ui/seller/SellerScopeRunner;Ljava/lang/String;)V
    .locals 1

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->isUnobscured()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p1, p2}, Lcom/squareup/ui/seller/SellerScopeRunner;->maybeProcessBarcodeScanned(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$CartContainerScreen$Presenter(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 97
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->onCartChanged()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$CartContainerScreen$Presenter(Lcom/squareup/payment/OrderEntryEvents$TicketSaved;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->onTicketSaved()V

    return-void
.end method

.method public synthetic lambda$onLoad$3$CartContainerScreen$Presenter(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .line 105
    new-instance v0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    iput-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->caretViewForActionbar:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$ABwtecpEbELcyUUFSIUi247fxoE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$ABwtecpEbELcyUUFSIUi247fxoE;-><init>(Lcom/squareup/ui/cart/CartContainerScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$TicketSaved;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$fISblMPp15EPKu0rtVitaLihDMk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$fISblMPp15EPKu0rtVitaLihDMk;-><init>(Lcom/squareup/ui/cart/CartContainerScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 102
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    new-instance v0, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$8Gh_Z4zFoQyFElbBzLtCW792e80;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$8Gh_Z4zFoQyFElbBzLtCW792e80;-><init>(Lcom/squareup/ui/cart/CartContainerScreen$Presenter;)V

    .line 103
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$9Tzi-twhFxJfWn9x2MrgqrSyn0Y;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartContainerScreen$Presenter$9Tzi-twhFxJfWn9x2MrgqrSyn0Y;-><init>(Lcom/squareup/ui/cart/CartContainerScreen$Presenter;)V

    iget-object v1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->open_menu:I

    .line 109
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->cartDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$zdudwk4B_hnRqeBmxHbf0FDwsak;

    invoke-direct {v1, v0}, Lcom/squareup/ui/cart/-$$Lambda$zdudwk4B_hnRqeBmxHbf0FDwsak;-><init>(Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;)V

    .line 110
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->cartDropDownPresenter:Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    new-instance v1, Lcom/squareup/ui/cart/CartContainerScreen$Presenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter$1;-><init>(Lcom/squareup/ui/cart/CartContainerScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->barcodeScannedListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 128
    invoke-direct {p0, p1}, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->updateActionBar(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    return-void
.end method

.method onStartVisualTransition()V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/cart/CartContainerScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_CONTAINER:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logEvent(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method
