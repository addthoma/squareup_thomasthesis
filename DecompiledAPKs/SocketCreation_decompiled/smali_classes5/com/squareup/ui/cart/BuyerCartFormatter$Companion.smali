.class public final Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;
.super Ljava/lang/Object;
.source "BuyerCartFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/BuyerCartFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u001c\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u001a\u0004\u0008\u0006\u0010\u0007R\u001c\u0010\u0008\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\t\u0010\u0002\u001a\u0004\u0008\n\u0010\u0007R\u001c\u0010\u000b\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000c\u0010\u0002\u001a\u0004\u0008\r\u0010\u0007R\u001c\u0010\u000e\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000f\u0010\u0002\u001a\u0004\u0008\u0010\u0010\u0007R\u001c\u0010\u0011\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0012\u0010\u0002\u001a\u0004\u0008\u0013\u0010\u0007R\u001c\u0010\u0014\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0015\u0010\u0002\u001a\u0004\u0008\u0016\u0010\u0007R\u001c\u0010\u0017\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0018\u0010\u0002\u001a\u0004\u0008\u0019\u0010\u0007R\u001c\u0010\u001a\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u001b\u0010\u0002\u001a\u0004\u0008\u001c\u0010\u0007R\u001c\u0010\u001d\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u001e\u0010\u0002\u001a\u0004\u0008\u001f\u0010\u0007\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;",
        "",
        "()V",
        "AUTO_GRAT_CLIENT_ID",
        "",
        "AUTO_GRAT_CLIENT_ID$annotations",
        "getAUTO_GRAT_CLIENT_ID",
        "()Ljava/lang/String;",
        "COMP_CLIENT_ID",
        "COMP_CLIENT_ID$annotations",
        "getCOMP_CLIENT_ID",
        "DISCOUNT_CLIENT_ID",
        "DISCOUNT_CLIENT_ID$annotations",
        "getDISCOUNT_CLIENT_ID",
        "PURCHASE_HEADER_CLIENT_ID",
        "PURCHASE_HEADER_CLIENT_ID$annotations",
        "getPURCHASE_HEADER_CLIENT_ID",
        "RETURN_DISCOUNT_CLIENT_ID",
        "RETURN_DISCOUNT_CLIENT_ID$annotations",
        "getRETURN_DISCOUNT_CLIENT_ID",
        "RETURN_HEADER_CLIENT_ID",
        "RETURN_HEADER_CLIENT_ID$annotations",
        "getRETURN_HEADER_CLIENT_ID",
        "RETURN_TAX_CLIENT_ID",
        "RETURN_TAX_CLIENT_ID$annotations",
        "getRETURN_TAX_CLIENT_ID",
        "RETURN_TIP_CLIENT_ID",
        "RETURN_TIP_CLIENT_ID$annotations",
        "getRETURN_TIP_CLIENT_ID",
        "TAX_CLIENT_ID",
        "TAX_CLIENT_ID$annotations",
        "getTAX_CLIENT_ID",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 589
    invoke-direct {p0}, Lcom/squareup/ui/cart/BuyerCartFormatter$Companion;-><init>()V

    return-void
.end method

.method public static synthetic AUTO_GRAT_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic COMP_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic DISCOUNT_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic PURCHASE_HEADER_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic RETURN_DISCOUNT_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic RETURN_HEADER_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic RETURN_TAX_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic RETURN_TIP_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic TAX_CLIENT_ID$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getAUTO_GRAT_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 597
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getAUTO_GRAT_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getCOMP_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 593
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getCOMP_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDISCOUNT_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 591
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getDISCOUNT_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPURCHASE_HEADER_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 599
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getPURCHASE_HEADER_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRETURN_DISCOUNT_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 601
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getRETURN_DISCOUNT_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRETURN_HEADER_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 607
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getRETURN_HEADER_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRETURN_TAX_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 603
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getRETURN_TAX_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRETURN_TIP_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 605
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getRETURN_TIP_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTAX_CLIENT_ID()Ljava/lang/String;
    .locals 1

    .line 595
    invoke-static {}, Lcom/squareup/ui/cart/BuyerCartFormatter;->access$getTAX_CLIENT_ID$cp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
