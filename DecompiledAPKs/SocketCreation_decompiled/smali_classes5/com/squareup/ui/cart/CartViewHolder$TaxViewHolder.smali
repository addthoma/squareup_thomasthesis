.class Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TaxViewHolder"
.end annotation


# instance fields
.field private final cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 203
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 204
    iput-object p2, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 205
    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;
    .locals 0

    .line 197
    iget-object p0, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    return-object p0
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 4

    .line 209
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    iget v1, p1, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;->taxResId:I

    iget-object v2, p1, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;->additionalTaxFees:Lcom/squareup/protos/common/Money;

    iget-boolean p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$TaxRow;->hasInterestingTaxState:Z

    const/4 v3, 0x0

    .line 211
    invoke-interface {v0, v1, v2, p1, v3}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->taxes(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    .line 215
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    new-instance v0, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder$1;-><init>(Lcom/squareup/ui/cart/CartViewHolder$TaxViewHolder;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartEntryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
