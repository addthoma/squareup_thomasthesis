.class public abstract Lcom/squareup/ui/buyercart/BuyerCartViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "BuyerCartViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;,
        Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartFooterViewHolder;,
        Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BuyerCartHeaderViewHolder;,
        Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranSectionHeaderViewHolder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008&\u0018\u00002\u00020\u0001:\u0004\t\n\u000b\u000cB\u000f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/BuyerCartViewHolder;",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "bind",
        "",
        "item",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "BranCartFooterViewHolder",
        "BranCartHeaderViewHolder",
        "BranSectionHeaderViewHolder",
        "BuyerCartHeaderViewHolder",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public abstract bind(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;)V
.end method
