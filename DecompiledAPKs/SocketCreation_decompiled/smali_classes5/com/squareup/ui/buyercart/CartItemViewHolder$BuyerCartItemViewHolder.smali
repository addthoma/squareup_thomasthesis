.class public final Lcom/squareup/ui/buyercart/CartItemViewHolder$BuyerCartItemViewHolder;
.super Lcom/squareup/ui/buyercart/CartItemViewHolder;
.source "CartItemViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/CartItemViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuyerCartItemViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0015\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0010\u00a2\u0006\u0002\u0008\u000cR\u0014\u0010\u0005\u001a\u00020\u00068PX\u0090\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/CartItemViewHolder$BuyerCartItemViewHolder;",
        "Lcom/squareup/ui/buyercart/CartItemViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "styleId",
        "",
        "getStyleId$buyer_cart_release",
        "()I",
        "getMarginTopInPx",
        "resources",
        "Landroid/content/res/Resources;",
        "getMarginTopInPx$buyer_cart_release",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyercart/CartItemViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getMarginTopInPx$buyer_cart_release(Landroid/content/res/Resources;)I
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    sget v0, Lcom/squareup/ui/buyercart/R$dimen;->buyer_cart_item_description_top_padding:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    float-to-int p1, p1

    return p1
.end method

.method public getStyleId$buyer_cart_release()I
    .locals 1

    .line 112
    sget v0, Lcom/squareup/ui/buyercart/R$style;->Label_LightGray_BuyerCart:I

    return v0
.end method
