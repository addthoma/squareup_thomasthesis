.class public final Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;
.super Lcom/squareup/ui/buyercart/CartItemViewHolder;
.source "CartItemViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/CartItemViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BranCartItemViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0015\u0010\u000f\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0011H\u0010\u00a2\u0006\u0002\u0008\u0012R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u00020\u00078PX\u0090\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;",
        "Lcom/squareup/ui/buyercart/CartItemViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "cartDiffHighlight",
        "shortAnimTimeMs",
        "",
        "styleId",
        "getStyleId$buyer_cart_release",
        "()I",
        "bind",
        "",
        "adapterItem",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "getMarginTopInPx",
        "resources",
        "Landroid/content/res/Resources;",
        "getMarginTopInPx$buyer_cart_release",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cartDiffHighlight:Landroid/view/View;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyercart/CartItemViewHolder;-><init>(Landroid/view/View;)V

    .line 82
    sget v0, Lcom/squareup/ui/buyercart/R$id;->cart_diff_highlight:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.cart_diff_highlight)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;->cartDiffHighlight:Landroid/view/View;

    .line 84
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 v0, 0x10e0000

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;->shortAnimTimeMs:I

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;)V
    .locals 1

    const-string v0, "adapterItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-super {p0, p1}, Lcom/squareup/ui/buyercart/CartItemViewHolder;->bind(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;)V

    .line 95
    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;

    .line 97
    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getShowDiffHighlight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;->cartDiffHighlight:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->isGone(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;->cartDiffHighlight:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->slideInFromLeft(Landroid/view/View;I)V

    goto :goto_0

    .line 100
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem$BranCartItem;->getShowDiffHighlight()Z

    move-result p1

    if-nez p1, :cond_1

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;->cartDiffHighlight:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public getMarginTopInPx$buyer_cart_release(Landroid/content/res/Resources;)I
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget v0, Lcom/squareup/ui/buyercart/R$dimen;->cart_item_description_top_padding:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    float-to-int p1, p1

    return p1
.end method

.method public getStyleId$buyer_cart_release()I
    .locals 1

    .line 87
    sget v0, Lcom/squareup/ui/buyercart/R$style;->Label_LightGray:I

    return v0
.end method
