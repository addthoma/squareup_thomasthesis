.class public abstract Lcom/squareup/ui/buyercart/CartItemViewHolder;
.super Lcom/squareup/ui/buyercart/BuyerCartViewHolder;
.source "CartItemViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;,
        Lcom/squareup/ui/buyercart/CartItemViewHolder$BuyerCartItemViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCartItemViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CartItemViewHolder.kt\ncom/squareup/ui/buyercart/CartItemViewHolder\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n*L\n1#1,155:1\n45#2:156\n17#2,22:157\n*E\n*S KotlinDebug\n*F\n+ 1 CartItemViewHolder.kt\ncom/squareup/ui/buyercart/CartItemViewHolder\n*L\n48#1:156\n48#1,22:157\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008&\u0018\u00002\u00020\u0001:\u0002%&B\u000f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J*\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\rH\u0002J2\u0010\u001f\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0015\u0010!\u001a\u00020\r2\u0006\u0010\"\u001a\u00020#H \u00a2\u0006\u0002\u0008$R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000c\u001a\u00020\rX\u00a0\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/ui/buyercart/CartItemViewHolder;",
        "Lcom/squareup/ui/buyercart/BuyerCartViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "description",
        "Landroid/widget/TextView;",
        "perItemDiscounts",
        "Landroid/view/ViewGroup;",
        "perUnitPriceAndQuantity",
        "price",
        "quantity",
        "styleId",
        "",
        "getStyleId$buyer_cart_release",
        "()I",
        "taxesBreakdown",
        "title",
        "bind",
        "",
        "adapterItem",
        "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
        "createDiscountRow",
        "name",
        "",
        "context",
        "Landroid/content/Context;",
        "layoutParams",
        "Landroid/widget/LinearLayout$LayoutParams;",
        "createLayoutParams",
        "marginTopInPx",
        "createTaxRow",
        "amount",
        "getMarginTopInPx",
        "resources",
        "Landroid/content/res/Resources;",
        "getMarginTopInPx$buyer_cart_release",
        "BranCartItemViewHolder",
        "BuyerCartItemViewHolder",
        "buyer-cart_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final description:Landroid/widget/TextView;

.field private final perItemDiscounts:Landroid/view/ViewGroup;

.field private final perUnitPriceAndQuantity:Landroid/widget/TextView;

.field private final price:Landroid/widget/TextView;

.field private final quantity:Landroid/widget/TextView;

.field private final taxesBreakdown:Landroid/view/ViewGroup;

.field private final title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyercart/BuyerCartViewHolder;-><init>(Landroid/view/View;)V

    .line 35
    sget v0, Lcom/squareup/ui/buyercart/R$id;->item_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.item_name)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->title:Landroid/widget/TextView;

    .line 36
    sget v0, Lcom/squareup/ui/buyercart/R$id;->item_price:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.item_price)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->price:Landroid/widget/TextView;

    .line 37
    sget v0, Lcom/squareup/ui/buyercart/R$id;->item_quantity:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.item_quantity)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->quantity:Landroid/widget/TextView;

    .line 39
    sget v0, Lcom/squareup/ui/buyercart/R$id;->item_per_unit_price_and_quantity:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.\u2026_unit_price_and_quantity)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->perUnitPriceAndQuantity:Landroid/widget/TextView;

    .line 40
    sget v0, Lcom/squareup/ui/buyercart/R$id;->item_description:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.item_description)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->description:Landroid/widget/TextView;

    .line 41
    sget v0, Lcom/squareup/ui/buyercart/R$id;->per_item_discounts:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.per_item_discounts)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->perItemDiscounts:Landroid/view/ViewGroup;

    .line 42
    sget v0, Lcom/squareup/ui/buyercart/R$id;->tax_breakdown:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.tax_breakdown)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->taxesBreakdown:Landroid/view/ViewGroup;

    return-void
.end method

.method private final createDiscountRow(Ljava/lang/String;Landroid/content/Context;ILandroid/widget/LinearLayout$LayoutParams;)Landroid/view/View;
    .locals 1

    .line 124
    new-instance v0, Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {v0, p2}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    .line 125
    invoke-virtual {v0, p2, p3}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 126
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    check-cast p4, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p4}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final createLayoutParams(I)Landroid/widget/LinearLayout$LayoutParams;
    .locals 2

    .line 150
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x0

    .line 151
    invoke-virtual {v0, v1, p1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    return-object v0
.end method

.method private final createTaxRow(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;ILandroid/widget/LinearLayout$LayoutParams;)Landroid/view/View;
    .locals 2

    .line 138
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/ui/buyercart/R$string;->buyer_cart_tax_additive_row:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 139
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 140
    check-cast p2, Ljava/lang/CharSequence;

    const-string v0, "amount"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 141
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 142
    new-instance p2, Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {p2, p3}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    .line 143
    invoke-virtual {p2, p3, p4}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 144
    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    check-cast p5, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2, p5}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    check-cast p2, Landroid/view/View;

    return-object p2
.end method


# virtual methods
.method public bind(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;)V
    .locals 10

    const-string v0, "adapterItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;

    .line 46
    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v0

    .line 47
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->name:Ljava/lang/String;

    .line 48
    iget-object v2, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->title:Landroid/widget/TextView;

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 156
    :cond_0
    check-cast v1, Ljava/lang/CharSequence;

    .line 158
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    const/4 v5, 0x0

    move v6, v3

    const/4 v3, 0x0

    const/4 v7, 0x0

    :goto_0
    if-gt v3, v6, :cond_6

    if-nez v7, :cond_1

    move v8, v3

    goto :goto_1

    :cond_1
    move v8, v6

    .line 163
    :goto_1
    invoke-interface {v1, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    const/16 v9, 0x20

    if-gt v8, v9, :cond_2

    const/4 v8, 0x1

    goto :goto_2

    :cond_2
    const/4 v8, 0x0

    :goto_2
    if-nez v7, :cond_4

    if-nez v8, :cond_3

    const/4 v7, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    if-nez v8, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    :cond_6
    :goto_3
    add-int/2addr v6, v4

    .line 178
    invoke-interface {v1, v3, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 156
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->price:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->price:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->quantity:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;->getQuantity()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->perUnitPriceAndQuantity:Landroid/widget/TextView;

    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->per_unit_price_and_quantity:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->perUnitPriceAndQuantity:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->per_unit_price_and_quantity:Ljava/lang/String;

    if-eqz v1, :cond_7

    goto :goto_4

    :cond_7
    const/4 v4, 0x0

    :goto_4
    invoke-static {p1, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 53
    iget-object p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->description:Landroid/widget/TextView;

    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->description:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->description:Landroid/widget/TextView;

    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->description:Ljava/lang/String;

    if-nez v1, :cond_8

    const/16 v5, 0x8

    :cond_8
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->perItemDiscounts:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string v1, "perItemDiscounts.resources"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyercart/CartItemViewHolder;->getMarginTopInPx$buyer_cart_release(Landroid/content/res/Resources;)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyercart/CartItemViewHolder;->createLayoutParams(I)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object p1

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->perItemDiscounts:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 58
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/comms/protos/seller/Discount;

    .line 59
    iget-object v3, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->perItemDiscounts:Landroid/view/ViewGroup;

    if-nez v2, :cond_9

    .line 61
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_9
    iget-object v2, v2, Lcom/squareup/comms/protos/seller/Discount;->name:Ljava/lang/String;

    if-nez v2, :cond_a

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_a
    iget-object v4, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->perItemDiscounts:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "perItemDiscounts.context"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/CartItemViewHolder;->getStyleId$buyer_cart_release()I

    move-result v5

    .line 60
    invoke-direct {p0, v2, v4, v5, p1}, Lcom/squareup/ui/buyercart/CartItemViewHolder;->createDiscountRow(Ljava/lang/String;Landroid/content/Context;ILandroid/widget/LinearLayout$LayoutParams;)Landroid/view/View;

    move-result-object v2

    .line 59
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_5

    .line 67
    :cond_b
    iget-object v1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->taxesBreakdown:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 68
    iget-object v0, v0, Lcom/squareup/comms/protos/seller/DisplayItem;->taxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/comms/protos/seller/Tax;

    .line 69
    iget-object v7, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->taxesBreakdown:Landroid/view/ViewGroup;

    .line 71
    iget-object v2, v1, Lcom/squareup/comms/protos/seller/Tax;->name:Ljava/lang/String;

    iget-object v3, v1, Lcom/squareup/comms/protos/seller/Tax;->amount:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/buyercart/CartItemViewHolder;->taxesBreakdown:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v1, "taxesBreakdown.context"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/CartItemViewHolder;->getStyleId$buyer_cart_release()I

    move-result v5

    move-object v1, p0

    move-object v6, p1

    .line 70
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/buyercart/CartItemViewHolder;->createTaxRow(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;ILandroid/widget/LinearLayout$LayoutParams;)Landroid/view/View;

    move-result-object v1

    .line 69
    invoke-virtual {v7, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_6

    :cond_c
    return-void
.end method

.method public abstract getMarginTopInPx$buyer_cart_release(Landroid/content/res/Resources;)I
.end method

.method public abstract getStyleId$buyer_cart_release()I
.end method
