.class Lcom/squareup/ui/activity/BulkSettleRunner$1;
.super Ljava/lang/Object;
.source "BulkSettleRunner.java"

# interfaces
.implements Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/BulkSettleRunner;->settleTips(Ljava/util/Collection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/BulkSettleRunner;

.field final synthetic val$tendersToSettle:Ljava/util/Collection;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/BulkSettleRunner;Ljava/util/Collection;)V
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner$1;->this$0:Lcom/squareup/ui/activity/BulkSettleRunner;

    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettleRunner$1;->val$tendersToSettle:Ljava/util/Collection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 192
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner$1;->this$0:Lcom/squareup/ui/activity/BulkSettleRunner;

    iget-object p2, p0, Lcom/squareup/ui/activity/BulkSettleRunner$1;->val$tendersToSettle:Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/BulkSettleRunner;->onSettleFailed(I)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 186
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettleRunner$1;->onSuccess(Ljava/lang/Void;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/Void;)V
    .locals 0

    .line 188
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleRunner$1;->this$0:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-static {p1}, Lcom/squareup/ui/activity/BulkSettleRunner;->access$000(Lcom/squareup/ui/activity/BulkSettleRunner;)Lflow/Flow;

    move-result-object p1

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method
