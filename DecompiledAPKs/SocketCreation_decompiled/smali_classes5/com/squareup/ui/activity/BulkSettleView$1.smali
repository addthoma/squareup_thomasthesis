.class Lcom/squareup/ui/activity/BulkSettleView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "BulkSettleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/BulkSettleView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/BulkSettleView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/BulkSettleView;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleView$1;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 101
    iget-object p2, p0, Lcom/squareup/ui/activity/BulkSettleView$1;->this$0:Lcom/squareup/ui/activity/BulkSettleView;

    iget-object p2, p2, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->setSearchQuery(Ljava/lang/String;)V

    return-void
.end method
