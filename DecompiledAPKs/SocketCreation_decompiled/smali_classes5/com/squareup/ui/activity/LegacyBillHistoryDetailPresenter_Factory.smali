.class public final Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;
.super Ljava/lang/Object;
.source "LegacyBillHistoryDetailPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final billHistoryRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final currentBillProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final expiryCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTransactionsHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final settleTipsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SettleTipsSectionPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SettleTipsSectionPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p2, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->currentBillProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p3, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p4, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p5, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p6, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p7, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p8, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p9, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p10, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->settleTipsPresenterProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p11, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p12, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->billHistoryRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SettleTipsSectionPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ">;)",
            "Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;"
        }
    .end annotation

    .line 88
    new-instance v13, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/activity/CurrentBill;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/util/Device;Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/ui/activity/BillHistoryRunner;)Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/activity/CurrentBill;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/ui/activity/SettleTipsSectionPresenter;",
            "Lcom/squareup/activity/ExpiryCalculator;",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ")",
            "Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;"
        }
    .end annotation

    .line 97
    new-instance v13, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;-><init>(Lflow/Flow;Lcom/squareup/activity/CurrentBill;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/util/Device;Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/ui/activity/BillHistoryRunner;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;
    .locals 13

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->currentBillProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/activity/CurrentBill;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->settleTipsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/activity/ExpiryCalculator;

    iget-object v0, p0, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->billHistoryRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/activity/CurrentBill;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/settings/server/Features;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/util/Device;Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/ui/activity/BillHistoryRunner;)Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter_Factory;->get()Lcom/squareup/ui/activity/LegacyBillHistoryDetailPresenter;

    move-result-object v0

    return-object v0
.end method
