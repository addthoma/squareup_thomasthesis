.class final Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2;
.super Ljava/lang/Object;
.source "BillHistoryPresenterHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt;->addTendersCustomerInfo(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;Lcom/squareup/crm/RolodexServiceHelper;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
        "kotlin.jvm.PlatformType",
        "tender",
        "Lcom/squareup/billhistory/model/TenderHistory;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method constructor <init>(Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2;->$rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/billhistory/model/TenderHistory;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "tender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2;->$rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 58
    invoke-static {v0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt;->access$optionalContactFor(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/billhistory/model/TenderHistory;)Lio/reactivex/Single;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2$1;-><init>(Lcom/squareup/billhistory/model/TenderHistory;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2;->apply(Lcom/squareup/billhistory/model/TenderHistory;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
