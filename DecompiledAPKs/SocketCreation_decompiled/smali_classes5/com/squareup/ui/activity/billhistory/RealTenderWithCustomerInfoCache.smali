.class public final Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;
.super Ljava/lang/Object;
.source "RealTenderWithCustomerInfoCache.kt"

# interfaces
.implements Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0019\u0010\u0008\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\u0005H\u0096\u0002J\'\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00062\u0006\u0010\u000b\u001a\u00020\u00052\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0096\u0002R \u0010\u0003\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
        "()V",
        "cache",
        "Landroidx/collection/LruCache;",
        "Lcom/squareup/transactionhistory/TransactionIds;",
        "",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
        "get",
        "ids",
        "set",
        "key",
        "value",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cache:Landroidx/collection/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LruCache<",
            "Lcom/squareup/transactionhistory/TransactionIds;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Landroidx/collection/LruCache;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroidx/collection/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;->cache:Landroidx/collection/LruCache;

    return-void
.end method


# virtual methods
.method public get(Lcom/squareup/transactionhistory/TransactionIds;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transactionhistory/TransactionIds;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "ids"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;->cache:Landroidx/collection/LruCache;

    invoke-virtual {v0, p1}, Landroidx/collection/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    return-object p1
.end method

.method public set(Lcom/squareup/transactionhistory/TransactionIds;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transactionhistory/TransactionIds;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;->cache:Landroidx/collection/LruCache;

    invoke-virtual {v0, p1, p2}, Landroidx/collection/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    return-object p1
.end method
