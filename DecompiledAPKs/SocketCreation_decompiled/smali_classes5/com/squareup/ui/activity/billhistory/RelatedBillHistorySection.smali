.class public Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;
.super Landroid/widget/LinearLayout;
.source "RelatedBillHistorySection.java"


# instance fields
.field billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final dayFormatter:Ljava/text/DateFormat;

.field private final employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

.field employees:Lcom/squareup/permissions/Employees;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final relatedBill:Lcom/squareup/server/payment/RelatedBillHistory;

.field resources:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;Z)V
    .locals 8

    const/4 v0, 0x0

    .line 92
    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    .line 93
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->setOrientation(I)V

    .line 95
    const-class v1, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;

    invoke-interface {v1, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;->inject(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;)V

    .line 96
    sget v1, Lcom/squareup/billhistoryui/R$layout;->bill_history_related_bill_section:I

    invoke-static {p1, v1, p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 98
    sget v1, Lcom/squareup/billhistoryui/R$id;->related_bill_employee:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/ShorteningTextView;

    iput-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    .line 100
    iget-object v1, p2, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iput-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 101
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->timeFormatter:Ljava/text/DateFormat;

    .line 102
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->dayFormatter:Ljava/text/DateFormat;

    .line 103
    iput-object p3, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->relatedBill:Lcom/squareup/server/payment/RelatedBillHistory;

    .line 105
    sget p1, Lcom/squareup/billhistoryui/R$id;->refund_title:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    .line 106
    invoke-direct {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getTextForRefundState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    sget p1, Lcom/squareup/billhistoryui/R$id;->related_bill_timestamp:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    .line 109
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createRefundTimestamp(Lcom/squareup/server/payment/RelatedBillHistory;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    sget p1, Lcom/squareup/billhistoryui/R$id;->related_bill_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 112
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createReason(Lcom/squareup/server/payment/RelatedBillHistory;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 114
    iget-object v1, p2, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    .line 115
    invoke-static {v1}, Lcom/squareup/checkout/OrderDestination;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 116
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_0
    move-object v7, v1

    .line 117
    invoke-virtual {p3}, Lcom/squareup/server/payment/RelatedBillHistory;->getReturnedLineItems()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 118
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    .line 119
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 120
    :goto_1
    invoke-virtual {p3}, Lcom/squareup/server/payment/RelatedBillHistory;->getReturnedLineItems()Ljava/util/List;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move v4, p4

    move-object v5, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addRefundedItemization(Ljava/util/List;Landroid/widget/LinearLayout;ZLjava/util/List;Z)V

    .line 124
    :cond_2
    invoke-virtual {p3}, Lcom/squareup/server/payment/RelatedBillHistory;->getPurchaseLineItems()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 125
    invoke-virtual {p3}, Lcom/squareup/server/payment/RelatedBillHistory;->getPurchaseLineItems()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v0

    invoke-direct {p0, v0, p1, p4, v7}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addPurchasedItemization(Lcom/squareup/protos/client/bills/Cart$LineItems;Landroid/widget/LinearLayout;ZLjava/util/List;)V

    .line 128
    :cond_3
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createPayments(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;)Ljava/util/List;

    move-result-object p4

    if-eqz p4, :cond_4

    .line 130
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_2
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 131
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 134
    :cond_4
    invoke-virtual {p3}, Lcom/squareup/server/payment/RelatedBillHistory;->getTenders()Ljava/util/List;

    move-result-object p4

    if-eqz p4, :cond_6

    .line 135
    invoke-virtual {p3}, Lcom/squareup/server/payment/RelatedBillHistory;->getTenders()Ljava/util/List;

    move-result-object p4

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :cond_5
    :goto_3
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender;

    .line 136
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v3, p2, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    .line 137
    invoke-virtual {p3}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/squareup/billhistory/model/TenderHistory;->fromHistoricalTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    .line 136
    invoke-virtual {v1, v2, v3, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createReceiptRow(Landroid/content/Context;ZLjava/lang/String;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 139
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_3

    :cond_6
    return-void
.end method

.method private addDiscountRows(Ljava/util/List;Landroid/widget/LinearLayout;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "ZZ)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 254
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 255
    invoke-direct {p0, v0, p3, p4}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createDiscountRow(Lcom/squareup/protos/client/bills/DiscountLineItem;ZZ)Landroid/view/View;

    move-result-object v0

    .line 256
    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private addFeeLineItemRows(Ljava/util/List;Landroid/widget/LinearLayout;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "ZZ)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 312
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 313
    invoke-direct {p0, v0, p3, p4}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createFeeRow(Lcom/squareup/protos/client/bills/FeeLineItem;ZZ)Landroid/view/View;

    move-result-object v0

    .line 314
    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private addPurchasedItemization(Lcom/squareup/protos/client/bills/Cart$LineItems;Landroid/widget/LinearLayout;ZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$LineItems;",
            "Landroid/widget/LinearLayout;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)V"
        }
    .end annotation

    .line 175
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addPurchasedItemizationRows(Ljava/util/List;Landroid/widget/LinearLayout;ZLjava/util/List;)V

    .line 177
    iget-object p4, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p4, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p4

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    .line 178
    iget-object p4, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-direct {p0, p4, p2, p3}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addPurchasedSplitDiscountRows(Ljava/util/List;Landroid/widget/LinearLayout;Z)V

    goto :goto_0

    .line 180
    :cond_0
    iget-object p4, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    invoke-direct {p0, p4, p2, p3, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addDiscountRows(Ljava/util/List;Landroid/widget/LinearLayout;ZZ)V

    .line 183
    :goto_0
    iget-object p4, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    invoke-direct {p0, p4, p2, p3, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addFeeLineItemRows(Ljava/util/List;Landroid/widget/LinearLayout;ZZ)V

    .line 185
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addRoundingAdjustmentLineItemRow(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Landroid/widget/LinearLayout;ZZ)V

    return-void
.end method

.method private addPurchasedItemizationRows(Ljava/util/List;Landroid/widget/LinearLayout;ZLjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)V"
        }
    .end annotation

    .line 239
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization;

    .line 240
    new-instance v1, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v1}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    sget-object v3, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    .line 241
    invoke-virtual {v1, v0, v2, v3, p4}, Lcom/squareup/checkout/CartItem$Builder;->fromItemizationHistory(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, p3, v3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createItemizationRows(Landroid/content/Context;Lcom/squareup/checkout/CartItem;ZLjava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 245
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    .line 246
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private addPurchasedSplitDiscountRows(Ljava/util/List;Landroid/widget/LinearLayout;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "Z)V"
        }
    .end annotation

    .line 295
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization;

    .line 296
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    if-nez v1, :cond_1

    goto :goto_0

    .line 300
    :cond_1
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    .line 302
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    const/4 v2, 0x0

    .line 303
    invoke-direct {p0, v1, p3, v2}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createDiscountRow(Lcom/squareup/protos/client/bills/DiscountLineItem;ZZ)Landroid/view/View;

    move-result-object v1

    .line 304
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private addRefundedItemization(Ljava/util/List;Landroid/widget/LinearLayout;ZLjava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;Z)V"
        }
    .end annotation

    .line 147
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    .line 149
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-direct {p0, v1, p2, p3, p4}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addReturnedItemizationRows(Ljava/util/List;Landroid/widget/LinearLayout;ZLjava/util/List;)V

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-direct {p0, v1, p2, p3}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addReturnedSplitDiscountRows(Ljava/util/List;Landroid/widget/LinearLayout;Z)V

    goto :goto_1

    .line 155
    :cond_0
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    invoke-direct {p0, v1, p2, p3, v2}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addDiscountRows(Ljava/util/List;Landroid/widget/LinearLayout;ZZ)V

    .line 158
    :goto_1
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    invoke-direct {p0, v1, p2, p3, v2}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addFeeLineItemRows(Ljava/util/List;Landroid/widget/LinearLayout;ZZ)V

    .line 160
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    invoke-direct {p0, v1, p2, p3, v3}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addReturnedSurchargeLineItemsRows(Ljava/util/List;Landroid/widget/LinearLayout;ZLcom/squareup/util/Res;)V

    .line 165
    :cond_1
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    invoke-direct {p0, v1, p2, p5}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addTipRows(Ljava/util/List;Landroid/widget/LinearLayout;Z)V

    .line 167
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-direct {p0, v0, p2, p3, v2}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->addRoundingAdjustmentLineItemRow(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Landroid/widget/LinearLayout;ZZ)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private addReturnedItemizationRows(Ljava/util/List;Landroid/widget/LinearLayout;ZLjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)V"
        }
    .end annotation

    .line 223
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 225
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    .line 226
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-nez v2, :cond_1

    sget-object v2, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-eq v1, v2, :cond_1

    goto :goto_0

    .line 230
    :cond_1
    invoke-direct {p0, v0, p3, p4}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createReturnedItemRow(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;ZLjava/util/List;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private addReturnedSplitDiscountRows(Ljava/util/List;Landroid/widget/LinearLayout;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "Z)V"
        }
    .end annotation

    .line 271
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 272
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    if-nez v1, :cond_1

    goto :goto_0

    .line 276
    :cond_1
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    .line 278
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    const/4 v2, 0x1

    .line 279
    invoke-direct {p0, v1, p3, v2}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createDiscountRow(Lcom/squareup/protos/client/bills/DiscountLineItem;ZZ)Landroid/view/View;

    move-result-object v1

    .line 280
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private addReturnedSurchargeLineItemsRows(Ljava/util/List;Landroid/widget/LinearLayout;ZLcom/squareup/util/Res;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "Z",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 192
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 196
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;

    .line 197
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;->surcharge_line_item:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    .line 198
    invoke-static {v0}, Lcom/squareup/checkout/Surcharge;->fromProto(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/checkout/Surcharge;

    move-result-object v0

    .line 199
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 197
    invoke-virtual {v1, v0, p4, p3, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createReturnSurchargeRow(Lcom/squareup/checkout/Surcharge;Lcom/squareup/util/Res;ZLandroid/content/Context;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private addRoundingAdjustmentLineItemRow(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Landroid/widget/LinearLayout;ZZ)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    .line 326
    :cond_0
    invoke-direct {p0, p1, p3, p4}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createRoundingAdjustmentRow(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;ZZ)Landroid/view/View;

    move-result-object p1

    .line 325
    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private addTipRows(Ljava/util/List;Landroid/widget/LinearLayout;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;",
            "Landroid/widget/LinearLayout;",
            "Z)V"
        }
    .end annotation

    .line 205
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 209
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    .line 210
    iget-object v1, v1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 211
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_0
    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 215
    invoke-direct {p0, v0, p3}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createReturnedTip(Lcom/squareup/protos/common/Money;Z)Landroid/view/View;

    move-result-object p1

    .line 216
    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private createDiscountRow(Lcom/squareup/protos/client/bills/DiscountLineItem;ZZ)Landroid/view/View;
    .locals 2

    .line 513
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createDiscountLineItem(Landroid/content/Context;Lcom/squareup/protos/client/bills/DiscountLineItem;ZZ)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    return-object p1
.end method

.method private createEmployeeFullName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;
    .locals 5

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 344
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/billhistoryui/R$string;->uppercase_receipt_detail_refund_giver:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    .line 345
    invoke-virtual {v3, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "firstname"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    .line 346
    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "lastname"

    invoke-virtual {v2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 347
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo p1, "\u00a0"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private createEmployeeShortName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;
    .locals 4

    .line 351
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    .line 353
    iget-object v1, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    .line 354
    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 355
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$string;->uppercase_receipt_detail_refund_giver:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    .line 356
    invoke-virtual {v2, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "firstname"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    .line 357
    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    const-string v0, "lastname"

    invoke-virtual {v1, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 358
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 360
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo p1, "\u00a0"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private createFeeRow(Lcom/squareup/protos/client/bills/FeeLineItem;ZZ)Landroid/view/View;
    .locals 2

    .line 519
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createFeeLineItem(Landroid/content/Context;Lcom/squareup/protos/client/bills/FeeLineItem;ZZ)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    return-object p1
.end method

.method private createPayments(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ")",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 424
    invoke-virtual {p2}, Lcom/squareup/server/payment/RelatedBillHistory;->isRefund()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getTenderHistoriesForRefund(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 426
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getTenderHistoriesForSale(Lcom/squareup/server/payment/RelatedBillHistory;)Ljava/util/List;

    move-result-object v0

    .line 428
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 430
    invoke-virtual {p2}, Lcom/squareup/server/payment/RelatedBillHistory;->getDestination()Lcom/squareup/protos/client/bills/Refund$Destination;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Lcom/squareup/server/payment/RelatedBillHistory;->getDestination()Lcom/squareup/protos/client/bills/Refund$Destination;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Refund$Destination;->gift_card_destination:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    goto :goto_1

    :cond_1
    move-object v2, v3

    :goto_1
    const/4 v4, 0x0

    if-eqz v2, :cond_2

    .line 434
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 435
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getRefundedMoneyAsString(Lcom/squareup/server/payment/RelatedBillHistory;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 436
    new-instance v0, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 437
    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 438
    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;->gift_card_pan_suffix:Ljava/lang/String;

    .line 439
    invoke-direct {p0, v2}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getGiftCardDescription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    .line 440
    invoke-virtual {v0, p2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 441
    invoke-virtual {p2, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p2

    .line 442
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->getNote()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setNote(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 443
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    .line 436
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v1

    :cond_2
    if-eqz v0, :cond_7

    .line 448
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    .line 452
    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_6

    .line 453
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistory;

    .line 455
    invoke-virtual {p2}, Lcom/squareup/server/payment/RelatedBillHistory;->isRefund()Z

    move-result v5

    if-nez v5, :cond_4

    .line 456
    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v6, v2, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v5, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    goto :goto_3

    .line 457
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v5

    if-nez v5, :cond_5

    .line 458
    iget-object v5, v2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-direct {p0, p2, v5}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getRefundedMoneyAsString(Lcom/squareup/server/payment/RelatedBillHistory;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_5
    move-object v5, v3

    .line 460
    :goto_3
    new-instance v6, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object v7, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 461
    invoke-virtual {v6, v7}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v6

    .line 462
    invoke-virtual {v2}, Lcom/squareup/billhistory/model/TenderHistory;->getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    .line 463
    invoke-virtual {v2, v7}, Lcom/squareup/billhistory/model/TenderHistory;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v6

    .line 464
    invoke-virtual {v6, v5}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v5

    sget-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 465
    invoke-virtual {v5, v6}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v5

    .line 466
    invoke-virtual {v2}, Lcom/squareup/billhistory/model/TenderHistory;->getNote()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/squareup/ui/account/view/LineRow$Builder;->setNote(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v2

    .line 467
    invoke-virtual {v2}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object v2

    .line 460
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_6
    return-object v1

    :cond_7
    :goto_4
    return-object v3
.end method

.method private createReason(Lcom/squareup/server/payment/RelatedBillHistory;)Landroid/view/View;
    .locals 4

    .line 475
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->isExchange()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 476
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getReason()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->exchange:I

    .line 477
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->exchange_with_reason:I

    .line 478
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 479
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getReason()Ljava/lang/String;

    move-result-object v1

    const-string v2, "reason"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 480
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 481
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 485
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getReason()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/activity/R$string;->refund:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 488
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getReasonOption()Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    move-result-object v0

    .line 489
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getReason()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    const/4 v3, 0x0

    .line 487
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/util/RefundReasonsHelperKt;->getReasonName(Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/util/Res;Z)Ljava/lang/String;

    move-result-object v0

    .line 494
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->isExchange()Z

    move-result p1

    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REFUNDED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 498
    new-instance v1, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    .line 499
    invoke-virtual {v1, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 500
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 501
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 502
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    return-object p1
.end method

.method private createRefundTimestamp(Lcom/squareup/server/payment/RelatedBillHistory;)Ljava/lang/CharSequence;
    .locals 3

    .line 364
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getCreatedAt()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 368
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getCreatedAt()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    if-nez p1, :cond_1

    return-object v1

    .line 373
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$string;->uppercase_receipt_detail_refund_timestamp:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->dayFormatter:Ljava/text/DateFormat;

    .line 374
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->timeFormatter:Ljava/text/DateFormat;

    .line 375
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 376
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private createReturnedItemRow(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;ZLjava/util/List;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .line 507
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    .line 508
    invoke-static {p1, v2, p3}, Lcom/squareup/checkout/CartItem;->fromRefundedItemization(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;Lcom/squareup/util/Res;Ljava/util/List;)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    const/4 p3, 0x0

    .line 507
    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createRefundItemization(Landroid/content/Context;Lcom/squareup/checkout/CartItem;ZLjava/lang/String;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    return-object p1
.end method

.method private createReturnedTip(Lcom/squareup/protos/common/Money;Z)Landroid/view/View;
    .locals 2

    .line 524
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 525
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createTipRow(Landroid/content/Context;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    move-result-object p1

    return-object p1
.end method

.method private createRoundingAdjustmentRow(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;ZZ)Landroid/view/View;
    .locals 2

    .line 530
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->billHistoryRowFactory:Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryRowFactory;->createSwedishRoundingAdjustmentRow(Landroid/content/Context;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;ZZ)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method private getGiftCardDescription(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 563
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    invoke-static {v0, v1, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getRefundedMoneyAsString(Lcom/squareup/server/payment/RelatedBillHistory;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 414
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 415
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 416
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 417
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    const-string p1, ""

    :goto_1
    return-object p1
.end method

.method private getTenderHistoriesForRefund(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 399
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 402
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 403
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedTenders()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistory;

    .line 404
    invoke-virtual {p2}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v3

    iget-object v4, v2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    .line 405
    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 406
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    iget-object v2, v2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private getTenderHistoriesForSale(Lcom/squareup/server/payment/RelatedBillHistory;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 383
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 384
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTenders()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    return-object v0

    .line 386
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTenders()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Tender;

    .line 387
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/billhistory/model/TenderHistory;->fromHistoricalTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private getTextForRefundState()Ljava/lang/String;
    .locals 5

    .line 535
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->relatedBill:Lcom/squareup/server/payment/RelatedBillHistory;

    invoke-virtual {v0}, Lcom/squareup/server/payment/RelatedBillHistory;->getRefundHistoryState()Lcom/squareup/server/payment/RefundHistoryState;

    move-result-object v0

    .line 536
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->relatedBill:Lcom/squareup/server/payment/RelatedBillHistory;

    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->isExchange()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/activity/R$string;->uppercase_exchange:I

    .line 537
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/activity/R$string;->uppercase_refund:I

    .line 538
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-nez v0, :cond_1

    return-object v1

    .line 543
    :cond_1
    sget-object v2, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection$1;->$SwitchMap$com$squareup$server$payment$RefundHistoryState:[I

    invoke-virtual {v0}, Lcom/squareup/server/payment/RefundHistoryState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 557
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown refund state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :pswitch_0
    return-object v1

    .line 549
    :pswitch_1
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->uppercase_refund_failed:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 546
    :pswitch_2
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->resources:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/activity/R$string;->uppercase_refund_pending:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public synthetic lambda$null$0$RelatedBillHistorySection(Lcom/squareup/permissions/Employee;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createEmployeeFullName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ShorteningTextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->createEmployeeShortName(Lcom/squareup/permissions/Employee;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/ShorteningTextView;->setShortText(Ljava/lang/CharSequence;)V

    .line 338
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->employeeNameView:Lcom/squareup/widgets/ShorteningTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/ShorteningTextView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$RelatedBillHistorySection()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->employees:Lcom/squareup/permissions/Employees;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;->relatedBill:Lcom/squareup/server/payment/RelatedBillHistory;

    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/Employees;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/billhistory/-$$Lambda$RelatedBillHistorySection$4Thv2JFiXzJHgObCEVQNHEiNMqM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$RelatedBillHistorySection$4Thv2JFiXzJHgObCEVQNHEiNMqM;-><init>(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;)V

    .line 335
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 331
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 333
    new-instance v0, Lcom/squareup/ui/activity/billhistory/-$$Lambda$RelatedBillHistorySection$HK2bpu1K8qqcW1P2oCplwwDrYfs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/billhistory/-$$Lambda$RelatedBillHistorySection$HK2bpu1K8qqcW1P2oCplwwDrYfs;-><init>(Lcom/squareup/ui/activity/billhistory/RelatedBillHistorySection;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
