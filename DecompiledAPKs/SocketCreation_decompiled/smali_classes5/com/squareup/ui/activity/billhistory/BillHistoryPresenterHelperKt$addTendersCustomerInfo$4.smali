.class final Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;
.super Ljava/lang/Object;
.source "BillHistoryPresenterHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt;->addTendersCustomerInfo(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;Lcom/squareup/crm/RolodexServiceHelper;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u00020\u00012(\u0010\u0002\u001a$\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;",
        "it",
        "",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
        "kotlin.jvm.PlatformType",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $tenderWithCustomerInfoCache:Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;

.field final synthetic $this_addTendersCustomerInfo:Lcom/squareup/billhistory/model/BillHistory;


# direct methods
.method constructor <init>(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;->$this_addTendersCustomerInfo:Lcom/squareup/billhistory/model/BillHistory;

    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;->$tenderWithCustomerInfoCache:Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/List;)Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfo;",
            ">;)",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;->$this_addTendersCustomerInfo:Lcom/squareup/billhistory/model/BillHistory;

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;-><init>(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)V

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;->$tenderWithCustomerInfoCache:Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;->$this_addTendersCustomerInfo:Lcom/squareup/billhistory/model/BillHistory;

    invoke-static {v2}, Lcom/squareup/activity/TransactionHistoriesKt;->getTransactionIds(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;->set(Lcom/squareup/transactionhistory/TransactionIds;Ljava/util/List;)Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;->apply(Ljava/util/List;)Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;

    move-result-object p1

    return-object p1
.end method
