.class public final Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt;
.super Ljava/lang/Object;
.source "BillHistoryPresenterHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBillHistoryPresenterHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BillHistoryPresenterHelper.kt\ncom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt\n*L\n1#1,88:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\"\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0000\u001a \u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u0001*\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u001a\u0012\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000e*\u00020\u0003H\u0002\u00a8\u0006\u000f"
    }
    d2 = {
        "addTendersCustomerInfo",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "tenderWithCustomerInfoCache",
        "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
        "rolodex",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "optionalContactFor",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "tender",
        "Lcom/squareup/billhistory/model/TenderHistory;",
        "tendersAsObservable",
        "Lio/reactivex/Observable;",
        "bill-history-ui_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$optionalContactFor(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/billhistory/model/TenderHistory;)Lio/reactivex/Single;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt;->optionalContactFor(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/billhistory/model/TenderHistory;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final addTendersCustomerInfo(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;Lcom/squareup/crm/RolodexServiceHelper;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$addTendersCustomerInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderWithCustomerInfoCache"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rolodex"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {p0}, Lcom/squareup/activity/TransactionHistoriesKt;->getTransactionIds(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;->get(Lcom/squareup/transactionhistory/TransactionIds;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    new-instance p1, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryWithTendersContactInfo;-><init>(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/List;)V

    .line 49
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "Single.just(\n        Bil\u2026hCustomerInfo = it)\n    )"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 55
    :cond_0
    invoke-static {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt;->tendersAsObservable(Lcom/squareup/billhistory/model/BillHistory;)Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2;

    invoke-direct {v1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$2;-><init>(Lcom/squareup/crm/RolodexServiceHelper;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p2

    .line 62
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$$inlined$compareByDescending$1;

    invoke-direct {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$$inlined$compareByDescending$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->toSortedList(Ljava/util/Comparator;)Lio/reactivex/Single;

    move-result-object p2

    .line 63
    new-instance v0, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$addTendersCustomerInfo$4;-><init>(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    const-string/jumbo p1, "tendersAsObservable()\n  \u2026it\n        result\n      }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final optionalContactFor(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/billhistory/model/TenderHistory;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation

    .line 79
    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->contactToken:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "it"

    .line 80
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    .line 81
    sget-object p1, Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$optionalContactFor$1$1;->INSTANCE:Lcom/squareup/ui/activity/billhistory/BillHistoryPresenterHelperKt$optionalContactFor$1$1;

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    .line 87
    :cond_0
    sget-object p0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {p0}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object p0

    invoke-static {p0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "Single.just(Optional.empty())"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method private static final tendersAsObservable(Lcom/squareup/billhistory/model/BillHistory;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 72
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getFirstBillTenders()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    invoke-static {p0}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "Observable.fromIterable(firstBillTenders)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
