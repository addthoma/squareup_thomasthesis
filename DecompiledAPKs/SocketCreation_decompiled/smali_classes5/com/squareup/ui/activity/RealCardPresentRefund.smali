.class public final Lcom/squareup/ui/activity/RealCardPresentRefund;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/ui/activity/CardPresentRefund;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u000eB\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/activity/RealCardPresentRefund;",
        "Lcom/squareup/ui/activity/CardPresentRefund;",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "cardReaderListeners",
        "Lcom/squareup/cardreader/CardReaderListeners;",
        "scope",
        "Lmortar/MortarScope;",
        "(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/CardReaderListeners;Lmortar/MortarScope;)V",
        "requestContactlessRefund",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/activity/ReaderResult;",
        "refundMoney",
        "Lcom/squareup/protos/common/Money;",
        "Factory",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final scope:Lmortar/MortarScope;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/CardReaderListeners;Lmortar/MortarScope;)V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iput-object p2, p0, Lcom/squareup/ui/activity/RealCardPresentRefund;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iput-object p3, p0, Lcom/squareup/ui/activity/RealCardPresentRefund;->scope:Lmortar/MortarScope;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/CardReaderListeners;Lmortar/MortarScope;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/activity/RealCardPresentRefund;-><init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/CardReaderListeners;Lmortar/MortarScope;)V

    return-void
.end method

.method public static final synthetic access$getCardReaderListeners$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/cardreader/CardReaderListeners;
    .locals 0

    .line 84
    iget-object p0, p0, Lcom/squareup/ui/activity/RealCardPresentRefund;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-object p0
.end method

.method public static final synthetic access$getNfcProcessor$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lcom/squareup/ui/NfcProcessor;
    .locals 0

    .line 84
    iget-object p0, p0, Lcom/squareup/ui/activity/RealCardPresentRefund;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    return-object p0
.end method

.method public static final synthetic access$getScope$p(Lcom/squareup/ui/activity/RealCardPresentRefund;)Lmortar/MortarScope;
    .locals 0

    .line 84
    iget-object p0, p0, Lcom/squareup/ui/activity/RealCardPresentRefund;->scope:Lmortar/MortarScope;

    return-object p0
.end method


# virtual methods
.method public requestContactlessRefund(Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/activity/ReaderResult;",
            ">;"
        }
    .end annotation

    const-string v0, "refundMoney"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    new-instance v0, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/RealCardPresentRefund$requestContactlessRefund$1;-><init>(Lcom/squareup/ui/activity/RealCardPresentRefund;Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.create { emitter \u2026(throwable)\n      }\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
