.class public final Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;
.super Ljava/lang/Object;
.source "SettleTipsSectionPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/SettleTipsSectionPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final billHistoryRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final currentBillProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final expiryCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTransactionsHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settleTipConnectivityUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SettleTipConnectivityUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderSettlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderTipSettler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderTipSettler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SettleTipConnectivityUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ">;)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p2, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p3, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p4, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->tenderSettlerProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p5, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p6, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p7, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->settleTipConnectivityUtilsProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p8, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->currentBillProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p9, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p10, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p11, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p12, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p13, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p14, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->billHistoryRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderTipSettler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/ExpiryCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SettleTipConnectivityUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ">;)",
            "Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;"
        }
    .end annotation

    .line 100
    new-instance v15, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/papersignature/TenderTipSettler;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/ui/activity/SettleTipConnectivityUtils;Lcom/squareup/activity/CurrentBill;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/EmployeeManagement;Lflow/Flow;Lcom/squareup/ui/activity/BillHistoryRunner;)Lcom/squareup/ui/activity/SettleTipsSectionPresenter;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/papersignature/TenderTipSettler;",
            "Lcom/squareup/activity/ExpiryCalculator;",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            "Lcom/squareup/ui/activity/SettleTipConnectivityUtils;",
            "Lcom/squareup/activity/CurrentBill;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/activity/BillHistoryRunner;",
            ")",
            "Lcom/squareup/ui/activity/SettleTipsSectionPresenter;"
        }
    .end annotation

    .line 110
    new-instance v15, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/papersignature/TenderTipSettler;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/ui/activity/SettleTipConnectivityUtils;Lcom/squareup/activity/CurrentBill;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/EmployeeManagement;Lflow/Flow;Lcom/squareup/ui/activity/BillHistoryRunner;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/SettleTipsSectionPresenter;
    .locals 15

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->tenderSettlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/papersignature/TenderTipSettler;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->expiryCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/activity/ExpiryCalculator;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->settleTipConnectivityUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->currentBillProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/activity/CurrentBill;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->employeeManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/settings/server/EmployeeManagementSettings;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->billHistoryRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/ui/activity/BillHistoryRunner;

    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/papersignature/TenderTipSettler;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/ui/activity/SettleTipConnectivityUtils;Lcom/squareup/activity/CurrentBill;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/EmployeeManagement;Lflow/Flow;Lcom/squareup/ui/activity/BillHistoryRunner;)Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter_Factory;->get()Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    move-result-object v0

    return-object v0
.end method
