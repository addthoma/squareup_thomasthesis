.class Lcom/squareup/ui/activity/TransactionsHistoryView$2;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "TransactionsHistoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/TransactionsHistoryView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/TransactionsHistoryView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/TransactionsHistoryView;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView$2;->this$0:Lcom/squareup/ui/activity/TransactionsHistoryView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 124
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView$2;->this$0:Lcom/squareup/ui/activity/TransactionsHistoryView;

    invoke-static {p1}, Lcom/squareup/ui/activity/TransactionsHistoryView;->access$000(Lcom/squareup/ui/activity/TransactionsHistoryView;)Lcom/squareup/ui/activity/SearchBar;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/ui/activity/DeprecatedSearchBar;

    if-eqz p1, :cond_0

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView$2;->this$0:Lcom/squareup/ui/activity/TransactionsHistoryView;

    iget-object p1, p1, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;->onSearchAction(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
