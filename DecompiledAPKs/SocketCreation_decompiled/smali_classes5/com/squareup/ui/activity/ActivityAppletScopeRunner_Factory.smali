.class public final Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;
.super Ljava/lang/Object;
.source "ActivityAppletScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/ActivityAppletScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final allTransactionsHistoryLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AllTransactionsHistoryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final appletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cacheUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCacheUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final currentBillProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;"
        }
    .end annotation
.end field

.field private final instantDepositRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTransactionsHistoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final searchResultsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/SearchResultsLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final showFullHistoryPermissionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCounterSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AllTransactionsHistoryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/SearchResultsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->busProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->currentBillProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->appletProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->allTransactionsHistoryLoaderProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->searchResultsLoaderProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->cacheUpdaterProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->tenderCounterSchedulerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->instantDepositRunnerProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->showFullHistoryPermissionControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/CurrentBill;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/LegacyTransactionsHistory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/AllTransactionsHistoryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/activity/SearchResultsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TenderStatusCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;",
            ">;)",
            "Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;"
        }
    .end annotation

    .line 85
    new-instance v12, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/activity/CurrentBill;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/activity/SearchResultsLoader;Lcom/squareup/papersignature/TenderStatusCacheUpdater;Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)Lcom/squareup/ui/activity/ActivityAppletScopeRunner;
    .locals 13

    .line 95
    new-instance v12, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/activity/ActivityAppletScopeRunner;-><init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/activity/CurrentBill;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/activity/SearchResultsLoader;Lcom/squareup/papersignature/TenderStatusCacheUpdater;Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/ActivityAppletScopeRunner;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->currentBillProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/activity/CurrentBill;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->appletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/activity/ActivityApplet;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->legacyTransactionsHistoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/activity/LegacyTransactionsHistory;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->allTransactionsHistoryLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/activity/AllTransactionsHistoryLoader;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->searchResultsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/activity/SearchResultsLoader;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->cacheUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/papersignature/TenderStatusCacheUpdater;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->tenderCounterSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->instantDepositRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/instantdeposit/InstantDepositRunner;

    iget-object v0, p0, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->showFullHistoryPermissionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/activity/CurrentBill;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/activity/LegacyTransactionsHistory;Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/activity/SearchResultsLoader;Lcom/squareup/papersignature/TenderStatusCacheUpdater;Lcom/squareup/papersignature/TendersAwaitingTipCountScheduler;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/ui/activity/ShowFullHistoryPermissionController;)Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/activity/ActivityAppletScopeRunner_Factory;->get()Lcom/squareup/ui/activity/ActivityAppletScopeRunner;

    move-result-object v0

    return-object v0
.end method
