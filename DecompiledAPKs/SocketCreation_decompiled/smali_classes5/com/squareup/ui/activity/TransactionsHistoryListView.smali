.class public Lcom/squareup/ui/activity/TransactionsHistoryListView;
.super Lcom/squareup/stickylistheaders/StickyListHeadersListView;
.source "TransactionsHistoryListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;
    }
.end annotation


# instance fields
.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const-class p2, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;->inject(Lcom/squareup/ui/activity/TransactionsHistoryListView;)V

    const/4 p1, 0x0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->setDrawingListUnderStickyHeader(Z)V

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->setSelectorPadding(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onAttachedToWindow$0$TransactionsHistoryListView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->onRowClicked(I)V

    return-void
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->getAdapter()Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 5

    .line 50
    invoke-super {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->onAttachedToWindow()V

    .line 52
    new-instance v0, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    iget-object v3, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    .line 53
    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/activity/TransactionsHistoryListView$LegacyTransactionsHistoryAdapter;-><init>(Landroid/content/Context;Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;Z)V

    .line 52
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->setAdapter(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    .line 55
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryListView$VFF7Zfx-stfNRQv6K_Y9OAfz4d8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$TransactionsHistoryListView$VFF7Zfx-stfNRQv6K_Y9OAfz4d8;-><init>(Lcom/squareup/ui/activity/TransactionsHistoryListView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENHANCED_TRANSACTION_SEARCH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    .line 58
    invoke-virtual {v0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->isDisplayedAsSidebar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$drawable;->transactions_history_list_row_divider:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$dimen;->transactions_history_divider:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 60
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->setDividerHeight(I)V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x0

    .line 68
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/TransactionsHistoryListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryListView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;->dropView(Lcom/squareup/ui/activity/TransactionsHistoryListView;)V

    .line 70
    invoke-super {p0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->onDetachedFromWindow()V

    return-void
.end method
