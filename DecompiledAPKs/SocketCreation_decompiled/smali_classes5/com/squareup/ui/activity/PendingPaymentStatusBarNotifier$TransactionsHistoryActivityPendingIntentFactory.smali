.class Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier$TransactionsHistoryActivityPendingIntentFactory;
.super Ljava/lang/Object;
.source "PendingPaymentStatusBarNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/PendingPaymentStatusBarNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TransactionsHistoryActivityPendingIntentFactory"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 1

    const-string v0, "SALES_HISTORY"

    .line 112
    invoke-static {p1, v0}, Lcom/squareup/ui/main/PosIntentParser;->createPendingIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object p1

    return-object p1
.end method
