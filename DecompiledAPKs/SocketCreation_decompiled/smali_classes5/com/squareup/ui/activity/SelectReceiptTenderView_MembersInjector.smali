.class public final Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;
.super Ljava/lang/Object;
.source "SelectReceiptTenderView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/SelectReceiptTenderView;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SelectReceiptTenderPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SelectReceiptTenderPresenter;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/SelectReceiptTenderPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/SelectReceiptTenderView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/activity/SelectReceiptTenderView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/activity/SelectReceiptTenderView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/activity/SelectReceiptTenderView;Ljava/lang/Object;)V
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/ui/activity/SelectReceiptTenderPresenter;

    iput-object p1, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView;->presenter:Lcom/squareup/ui/activity/SelectReceiptTenderPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/SelectReceiptTenderView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/activity/SelectReceiptTenderView;Lcom/squareup/text/Formatter;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;->injectPresenter(Lcom/squareup/ui/activity/SelectReceiptTenderView;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/activity/SelectReceiptTenderView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/SelectReceiptTenderView_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/SelectReceiptTenderView;)V

    return-void
.end method
