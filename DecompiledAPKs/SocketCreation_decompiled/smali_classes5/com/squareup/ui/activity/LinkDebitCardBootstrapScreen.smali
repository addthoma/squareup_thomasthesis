.class Lcom/squareup/ui/activity/LinkDebitCardBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "LinkDebitCardBootstrapScreen.java"


# instance fields
.field private linkDebitCardWorkflowStartArg:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;


# direct methods
.method constructor <init>(Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/ui/activity/LinkDebitCardBootstrapScreen;->linkDebitCardWorkflowStartArg:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/activity/LinkDebitCardBootstrapScreen;->linkDebitCardWorkflowStartArg:Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;

    invoke-static {p1, v0}, Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;)V

    return-void
.end method

.method public getDirectionOverride()Lflow/Direction;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 2

    .line 20
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardScope;

    sget-object v1, Lcom/squareup/ui/activity/ActivityAppletScope;->INSTANCE:Lcom/squareup/ui/activity/ActivityAppletScope;

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/LinkDebitCardScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method
