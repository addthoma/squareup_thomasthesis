.class public Lcom/squareup/ui/activity/BulkSettlePresenter;
.super Lmortar/ViewPresenter;
.source "BulkSettlePresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/BulkSettlePresenter$Row;,
        Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/activity/BulkSettleView;",
        ">;"
    }
.end annotation


# static fields
.field static final ALL_TIPS_HEADER_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

.field private static final SORT_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

.field static final VIEW_ALL_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

.field static final YOUR_TIPS_HEADER_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;


# instance fields
.field private final bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field currentEmployeeTenderCount:I

.field private final currentEmployeeToken:Ljava/lang/String;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final employeeManagementMode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

.field private final employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

.field private final employees:Lcom/squareup/permissions/Employees;

.field private final expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field final rows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/BulkSettlePresenter$Row;",
            ">;"
        }
    .end annotation
.end field

.field private final tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

.field private final timeFormat:Ljava/text/DateFormat;

.field viewAllTipsPermissionGranted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->YOUR_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter;->YOUR_TIPS_HEADER_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    .line 83
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->ALL_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter;->ALL_TIPS_HEADER_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    .line 84
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->VIEW_ALL:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter;->VIEW_ALL_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    .line 86
    new-instance v0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->SORT:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;)V

    sput-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter;->SORT_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Lcom/squareup/activity/ExpiryCalculator;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/Employees;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/activity/BulkSettleRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/activity/TendersAwaitingTipLoader;",
            "Lcom/squareup/activity/ExpiryCalculator;",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/permissions/Employees;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/ui/activity/BulkSettleRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 113
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 115
    iput-object p2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    .line 116
    iput-object p3, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->timeFormat:Ljava/text/DateFormat;

    .line 117
    iput-object p4, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 118
    iput-object p5, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    .line 119
    iput-object p6, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    .line 120
    iput-object p7, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 121
    iput-object p8, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 122
    iput-object p10, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employees:Lcom/squareup/permissions/Employees;

    .line 123
    iput-object p11, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 124
    iput-object p12, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    .line 126
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    .line 128
    sget-object p1, Lcom/squareup/permissions/Permission;->SETTLE_ALL_TIPS:Lcom/squareup/permissions/Permission;

    invoke-interface {p8, p1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 129
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeTipFilteringAllowed()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->viewAllTipsPermissionGranted:Z

    .line 130
    invoke-interface {p8}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currentEmployeeToken:Ljava/lang/String;

    .line 131
    invoke-virtual {p9}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagementMode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 133
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/BulkSettlePresenter;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->permitAndDisplayAllTips()V

    return-void
.end method

.method private addTenderRows(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;)V"
        }
    .end annotation

    .line 515
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/TenderEditState;

    .line 516
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    new-instance v2, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    sget-object v3, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    invoke-direct {v2, v3, v0}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/ui/activity/TenderEditState;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private allocateTenders(Ljava/util/List;)Lkotlin/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;)",
            "Lkotlin/Pair<",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;>;"
        }
    .end annotation

    .line 481
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 482
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 484
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currentEmployeeToken:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 485
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->nullCurrentEmployeeTokenCanSettleTips()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 486
    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 488
    :cond_0
    invoke-interface {v1, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 490
    :goto_0
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    :cond_1
    const/4 v2, 0x0

    .line 494
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 495
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/activity/TenderEditState;

    .line 496
    iget-object v4, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currentEmployeeToken:Ljava/lang/String;

    iget-object v5, v3, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    iget-object v5, v5, Lcom/squareup/billhistory/model/TenderHistory;->employeeToken:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 497
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 499
    :cond_2
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 502
    :cond_3
    new-instance p1, Lkotlin/Pair;

    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method private employeeTipFilteringAllowed()Z
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->isEldmEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 313
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isPaperSignatureFilteringAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private findPercentageForTender(Lcom/squareup/ui/activity/TenderRowViewHolder;Lcom/squareup/billhistory/model/TenderHistory;)Ljava/lang/String;
    .locals 3

    .line 553
    invoke-virtual {p1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->getTipOptionLabels()Ljava/util/List;

    move-result-object v0

    .line 554
    invoke-virtual {p1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->getTipOptionAmounts()Ljava/util/List;

    move-result-object v1

    .line 555
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 557
    invoke-virtual {p1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->isDisplayingPercentages()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    .line 558
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_1

    .line 559
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2, p2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 560
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getMainTextColor(Z)I
    .locals 0

    if-eqz p1, :cond_0

    .line 277
    sget p1, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray:I

    :goto_0
    return p1
.end method

.method private getNonTenderRowCount()I
    .locals 4

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    .line 189
    iget-object v2, v2, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->type:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    sget-object v3, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    if-eq v2, v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private hasPermissionToSettleAllTips()Z
    .locals 2

    .line 388
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v1, Lcom/squareup/permissions/Permission;->SETTLE_ALL_TIPS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    return v0
.end method

.method private isSortableByEmployee()Z
    .locals 2

    .line 306
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeTipFilteringAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagementMode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->viewAllTipsPermissionGranted:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isTenderAt(I)Z
    .locals 1

    if-ltz p1, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getRowCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 229
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getRowType(I)Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private mobileEmployeeWithNoShowableTenders()Z
    .locals 1

    .line 371
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->pinAccessRequiredToShowTenders()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->pinAccessNotAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private noTenderAt(I)Z
    .locals 1

    if-ltz p1, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getRowCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 224
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getRowType(I)Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private notifyRowsChanged()V
    .locals 1

    .line 525
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleView;->notifyRowsChanged()V

    :cond_0
    return-void
.end method

.method private nullCurrentEmployeeTokenCanSettleTips()Z
    .locals 2

    .line 511
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagementMode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private permitAndDisplayAllTips()V
    .locals 1

    const/4 v0, 0x1

    .line 163
    iput-boolean v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->viewAllTipsPermissionGranted:Z

    .line 164
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->updateView()V

    return-void
.end method

.method private pinAccessNotAvailable()Z
    .locals 2

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagementMode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private pinAccessRequiredToShowTenders()Z
    .locals 4

    .line 376
    iget v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currentEmployeeTenderCount:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    .line 377
    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 379
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->hasPermissionToSettleAllTips()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v3}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasTenders()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    return v1
.end method

.method private rebuildRows()V
    .locals 5

    .line 429
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 430
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter;->SORT_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeTipFilteringAllowed()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 437
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    .line 438
    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getTenders()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->allocateTenders(Ljava/util/List;)Lkotlin/Pair;

    move-result-object v0

    .line 439
    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 440
    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 441
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    iput v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currentEmployeeTenderCount:I

    .line 443
    iget v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currentEmployeeTenderCount:I

    if-lez v2, :cond_0

    .line 444
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    sget-object v3, Lcom/squareup/ui/activity/BulkSettlePresenter;->YOUR_TIPS_HEADER_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445
    invoke-direct {p0, v1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->addTenderRows(Ljava/util/Set;)V

    .line 447
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 448
    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$2;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeManagementMode:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    invoke-virtual {v2}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_4

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 460
    iget-boolean v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->viewAllTipsPermissionGranted:Z

    if-eqz v1, :cond_1

    .line 461
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    sget-object v2, Lcom/squareup/ui/activity/BulkSettlePresenter;->ALL_TIPS_HEADER_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 462
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->addTenderRows(Ljava/util/Set;)V

    goto :goto_1

    .line 463
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->shouldAddViewAllRow()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter;->ALL_TIPS_HEADER_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 465
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter;->VIEW_ALL_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 456
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    sget-object v2, Lcom/squareup/ui/activity/BulkSettlePresenter;->ALL_TIPS_HEADER_ROW:Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 457
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->addTenderRows(Ljava/util/Set;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    .line 471
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getTenderCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 472
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    new-instance v2, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    sget-object v3, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->TENDER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    iget-object v4, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v4, v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getTenderAt(I)Lcom/squareup/ui/activity/TenderEditState;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;Lcom/squareup/ui/activity/TenderEditState;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 476
    :cond_4
    :goto_1
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->notifyRowsChanged()V

    return-void
.end method

.method private shouldAddViewAllRow()Z
    .locals 1

    .line 521
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private showActionBarPrimaryButton()V
    .locals 5

    .line 575
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->numSettleableTenders()I

    move-result v0

    .line 576
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->bulk_settle_settle_button_nonzero:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "count"

    .line 577
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 578
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 579
    iget-object v3, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/billhistoryui/R$string;->bulk_settle_confirm_settle_button:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 580
    invoke-virtual {v3, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 581
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 583
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/activity/BulkSettleView;

    const/4 v3, 0x1

    .line 584
    invoke-virtual {v2, v3}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonEnabled(Z)V

    .line 585
    invoke-virtual {v2, v1, v0}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showDisabledActionBarPrimaryButton()V
    .locals 3

    .line 568
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    const/4 v1, 0x1

    .line 569
    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonVisible(Z)V

    const/4 v1, 0x0

    .line 570
    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonEnabled(Z)V

    .line 571
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->bulk_settle_settle_button_zero:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showLoading()V
    .locals 1

    .line 609
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleView;->showLoading()V

    :cond_0
    return-void
.end method

.method private showLoadingError()V
    .locals 3

    .line 603
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getErrorTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/BulkSettleView;->showErrorMessage(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showNoSearchResults()V
    .locals 2

    .line 596
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleView;->showNoSearchResultsMessage()V

    .line 598
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonVisible(Z)V

    :cond_0
    return-void
.end method

.method private showNoTenders()V
    .locals 2

    .line 589
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleView;->showNoTendersMessage()V

    .line 591
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonVisible(Z)V

    :cond_0
    return-void
.end method

.method private showTendersList()V
    .locals 1

    .line 615
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 616
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleView;->showTendersList()V

    :cond_0
    return-void
.end method

.method private updateRowTipAndTotal(Lcom/squareup/ui/activity/TenderRowViewHolder;I)V
    .locals 4

    .line 531
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-virtual {p2}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->tenderHistory()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p2

    .line 534
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 535
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/BulkSettlePresenter;->findPercentageForTender(Lcom/squareup/ui/activity/TenderRowViewHolder;Lcom/squareup/billhistory/model/TenderHistory;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "total"

    if-nez v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->bulk_settle_tender_row_total:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p2, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 539
    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 540
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 542
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->bulk_settle_tender_row_total_with_percentage:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "percentage"

    .line 543
    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p2, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 544
    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 545
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 548
    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/TenderRowViewHolder;->updateQuickTipEditor(Lcom/squareup/billhistory/model/TenderHistory;)V

    .line 549
    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/TenderRowViewHolder;->updateTotal(Ljava/lang/String;)V

    return-void
.end method

.method private updateSettleButton()V
    .locals 4

    .line 392
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    if-nez v0, :cond_0

    return-void

    .line 397
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/BulkSettleRunner;->isSettleInFlight()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 398
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showDisabledActionBarPrimaryButton()V

    return-void

    .line 402
    :cond_1
    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$2;->$SwitchMap$com$squareup$ui$activity$TendersAwaitingTipLoader$LoadState:[I

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getLoadState()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    const/4 v3, 0x2

    if-eq v1, v3, :cond_5

    const/4 v3, 0x3

    if-eq v1, v3, :cond_3

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    goto :goto_1

    .line 418
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown load state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getLoadState()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 409
    :cond_3
    invoke-virtual {v0, v2}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonVisible(Z)V

    .line 411
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->numSettleableTenders()I

    move-result v0

    if-nez v0, :cond_4

    .line 412
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showDisabledActionBarPrimaryButton()V

    goto :goto_0

    .line 414
    :cond_4
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showActionBarPrimaryButton()V

    :goto_0
    return-void

    :cond_5
    :goto_1
    const/4 v1, 0x0

    .line 406
    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonVisible(Z)V

    return-void
.end method

.method private updateView()V
    .locals 3

    .line 335
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->rebuildRows()V

    .line 336
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->updateSettleButton()V

    .line 338
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->isSettleInFlight()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showLoading()V

    return-void

    .line 343
    :cond_0
    sget-object v0, Lcom/squareup/ui/activity/BulkSettlePresenter$2;->$SwitchMap$com$squareup$ui$activity$TendersAwaitingTipLoader$LoadState:[I

    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getLoadState()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    const/4 v1, 0x2

    if-eq v0, v1, :cond_7

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 363
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showLoadingError()V

    return-void

    .line 366
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid load state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getLoadState()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 349
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->employeeTipFilteringAllowed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->mobileEmployeeWithNoShowableTenders()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 350
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showNoTenders()V

    return-void

    .line 353
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    .line 354
    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasTenders()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getShownTenderCount()I

    move-result v0

    if-nez v0, :cond_5

    .line 355
    :cond_4
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showNoSearchResults()V

    goto :goto_0

    .line 356
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasTenders()Z

    move-result v0

    if-nez v0, :cond_6

    .line 357
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showNoTenders()V

    goto :goto_0

    .line 359
    :cond_6
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showTendersList()V

    :goto_0
    return-void

    .line 346
    :cond_7
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->showLoading()V

    return-void
.end method


# virtual methods
.method public bindHeaderRow(Lcom/squareup/ui/activity/HeaderRowViewHolder;)V
    .locals 2

    .line 239
    iget-object v0, p1, Lcom/squareup/ui/activity/HeaderRowViewHolder;->rowType:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    sget-object v1, Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;->YOUR_TIPS_HEADER:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    if-ne v0, v1, :cond_0

    .line 240
    invoke-virtual {p1}, Lcom/squareup/ui/activity/HeaderRowViewHolder;->adjustPaddingTop()V

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->bulk_settle_uppercase_header_your_tips:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 243
    :cond_0
    iget v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currentEmployeeTenderCount:I

    if-nez v0, :cond_1

    .line 244
    invoke-virtual {p1}, Lcom/squareup/ui/activity/HeaderRowViewHolder;->adjustPaddingTop()V

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->bulk_settle_uppercase_header_all_tips:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 248
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/HeaderRowViewHolder;->setTitle(Ljava/lang/String;)V

    return-void
.end method

.method public bindSortRow(Lcom/squareup/ui/activity/SortRowViewHolder;)V
    .locals 2

    .line 233
    sget-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->EMPLOYEE:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->isSortableByEmployee()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/activity/SortRowViewHolder;->setSortFieldVisible(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;Z)V

    .line 234
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getSortField()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/SortRowViewHolder;->setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V

    return-void
.end method

.method public bindTenderRow(Lcom/squareup/ui/activity/TenderRowViewHolder;I)V
    .locals 4

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->tenderHistory()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    .line 254
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->timeFormat:Ljava/text/DateFormat;

    iget-object v2, v0, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 255
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->isSortableByEmployee()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 256
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->employees:Lcom/squareup/permissions/Employees;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setTimeAndEmployeeName(Ljava/lang/String;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/permissions/Employees;)V

    goto :goto_0

    .line 258
    :cond_0
    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setTimeWithNoEmployeeName(Ljava/lang/String;)V

    .line 261
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->bulk_settle_receipt_number:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    const-string v3, "receipt_number"

    .line 262
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 263
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 261
    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setReceiptNumber(Ljava/lang/String;)V

    .line 264
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->amountExcludingTip()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setAmount(Ljava/lang/String;)V

    .line 266
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getSortField()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setBoldColumn(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V

    .line 268
    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->expiryCalculator:Lcom/squareup/activity/ExpiryCalculator;

    invoke-virtual {v1, v0}, Lcom/squareup/activity/ExpiryCalculator;->isTipExpiringSoon(Lcom/squareup/billhistory/model/TenderHistory;)Z

    move-result v0

    .line 271
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getMainTextColor(Z)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/TenderRowViewHolder;->setTextColor(I)V

    .line 273
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/BulkSettlePresenter;->updateRowTipAndTotal(Lcom/squareup/ui/activity/TenderRowViewHolder;I)V

    return-void
.end method

.method public canKeyboardMoveToNextRowFrom(I)Z
    .locals 1

    .line 211
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getNextTenderPositionFrom(I)I

    move-result p1

    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method getNextTenderPositionFrom(I)I
    .locals 1

    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 218
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->noTenderAt(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->isTenderAt(I)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public getOtherEmployeeTenderCount()I
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getAllTendersCount()I

    move-result v0

    iget v1, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currentEmployeeTenderCount:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getRowCount()I
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRowType(I)Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    iget-object p1, p1, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->type:Lcom/squareup/ui/activity/BulkSettlePresenter$RowType;

    return-object p1
.end method

.method public getShownTenderCount()I
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getNonTenderRowCount()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getSortField()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->getSortField()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$BulkSettlePresenter(Ljava/lang/Boolean;)V
    .locals 0

    .line 141
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->updateView()V

    return-void
.end method

.method public synthetic lambda$null$2$BulkSettlePresenter(Lkotlin/Unit;)V
    .locals 0

    .line 145
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->updateView()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$BulkSettlePresenter()Lrx/Subscription;
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->settleInFlight()Lrx/Observable;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$BulkSettlePresenter$SoblAgCJKeDUQniUZsT_NvEOTXU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$BulkSettlePresenter$SoblAgCJKeDUQniUZsT_NvEOTXU;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    .line 141
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$3$BulkSettlePresenter()Lrx/Subscription;
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->datasetChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$BulkSettlePresenter$1GpaNi6gkHLAcbBseiqwmWBWV4g;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$BulkSettlePresenter$1GpaNi6gkHLAcbBseiqwmWBWV4g;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    .line 145
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->onBackPressed()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BulkSettleView;

    .line 138
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$BulkSettlePresenter$ineUbr0tb-H_c8QrUQAY1Ks0D04;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$BulkSettlePresenter$ineUbr0tb-H_c8QrUQAY1Ks0D04;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 143
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$BulkSettlePresenter$TU6VxyeAUWvOBm81-57x2ccz4c4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$BulkSettlePresenter$TU6VxyeAUWvOBm81-57x2ccz4c4;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onKeyboardActionNextClicked(I)V
    .locals 1

    .line 201
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 204
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->canKeyboardMoveToNextRowFrom(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getNextTenderPositionFrom(I)I

    move-result p1

    .line 206
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/BulkSettleView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/BulkSettleView;->moveToRowByKeyboard(I)V

    :cond_1
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 149
    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BulkSettleView;

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->bulk_settle_add_tips_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/BulkSettleView;->setUpButtonText(Ljava/lang/String;)V

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->updateView()V

    return-void
.end method

.method public onRequestViewAllTips()V
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->SETTLE_ALL_TIPS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/activity/BulkSettlePresenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/activity/BulkSettlePresenter$1;-><init>(Lcom/squareup/ui/activity/BulkSettlePresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public onSettleClicked()V
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleRunner;->onSettleClicked()V

    return-void
.end method

.method public onTipAmountChanged(Lcom/squareup/ui/activity/TenderRowViewHolder;Ljava/lang/Long;Z)V
    .locals 1

    .line 283
    invoke-virtual {p1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->getTenderPosition()I

    move-result v0

    .line 284
    invoke-virtual {p0, v0, p2, p3}, Lcom/squareup/ui/activity/BulkSettlePresenter;->setTipForTenderAt(ILjava/lang/Long;Z)V

    .line 285
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->updateSettleButton()V

    .line 286
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->updateRowTipAndTotal(Lcom/squareup/ui/activity/TenderRowViewHolder;I)V

    return-void
.end method

.method setSearchQuery(Ljava/lang/String;)V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->setSearchQuery(Ljava/lang/String;)V

    return-void
.end method

.method public setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V
    .locals 1

    .line 295
    sget-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->EMPLOYEE:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->isSortableByEmployee()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->tendersLoader:Lcom/squareup/ui/activity/TendersAwaitingTipLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V

    return-void
.end method

.method setTipForTenderAt(ILjava/lang/Long;Z)V
    .locals 1

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;

    invoke-static {p1}, Lcom/squareup/ui/activity/BulkSettlePresenter$Row;->access$100(Lcom/squareup/ui/activity/BulkSettlePresenter$Row;)Lcom/squareup/ui/activity/TenderEditState;

    move-result-object p1

    .line 322
    iget-boolean v0, p1, Lcom/squareup/ui/activity/TenderEditState;->wasQuickTipUsed:Z

    or-int/2addr p3, v0

    iput-boolean p3, p1, Lcom/squareup/ui/activity/TenderEditState;->wasQuickTipUsed:Z

    if-nez p2, :cond_0

    .line 325
    iget-object p2, p1, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {p2}, Lcom/squareup/billhistory/model/TenderHistory;->withoutTip()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    .line 326
    iget-object p2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/activity/BulkSettleRunner;->removeTender(Lcom/squareup/ui/activity/TenderEditState;)V

    goto :goto_0

    .line 328
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p2, p3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 329
    iget-object p3, p1, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    const/4 v0, 0x0

    invoke-virtual {p3, p2, v0}, Lcom/squareup/billhistory/model/TenderHistory;->withTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p2

    iput-object p2, p1, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    .line 330
    iget-object p2, p0, Lcom/squareup/ui/activity/BulkSettlePresenter;->bulkSettleRunner:Lcom/squareup/ui/activity/BulkSettleRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/activity/BulkSettleRunner;->addTender(Lcom/squareup/ui/activity/TenderEditState;)V

    :goto_0
    return-void
.end method
