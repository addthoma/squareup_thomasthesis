.class public Lcom/squareup/ui/activity/RefundFailureDialogScreen;
.super Lcom/squareup/ui/activity/InActivityAppletScope;
.source "RefundFailureDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/activity/RefundFailureDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/RefundFailureDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/RefundFailureDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$RefundFailureDialogScreen$yQrMuITap0Nro3f4rsehA1Odm3Q;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$RefundFailureDialogScreen$yQrMuITap0Nro3f4rsehA1Odm3Q;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/RefundFailureDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/activity/InActivityAppletScope;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/activity/RefundFailureDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/RefundFailureDialogScreen;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/ui/activity/RefundFailureDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/RefundFailureDialogScreen;
    .locals 1

    .line 40
    const-class v0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    .line 41
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 40
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    .line 42
    new-instance v0, Lcom/squareup/ui/activity/RefundFailureDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/RefundFailureDialogScreen;-><init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 34
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/activity/InActivityAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundFailureDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method getFailure()Lcom/squareup/register/widgets/FailureAlertDialogFactory;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundFailureDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    return-object v0
.end method
