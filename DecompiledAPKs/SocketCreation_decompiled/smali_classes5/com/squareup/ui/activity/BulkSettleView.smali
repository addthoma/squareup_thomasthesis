.class public Lcom/squareup/ui/activity/BulkSettleView;
.super Lcom/squareup/widgets/PairLayout;
.source "BulkSettleView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;,
        Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;
    }
.end annotation


# instance fields
.field private actionBarPrimaryButton:Lcom/squareup/ui/ResizingConfirmButton;

.field private actionBarUpButton:Lcom/squareup/glyph/SquareGlyphView;

.field private actionBarUpButtonText:Lcom/squareup/marketfont/MarketTextView;

.field private content:Landroid/view/ViewGroup;

.field currency:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private noSearchResults:Landroid/view/View;

.field presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchBar:Lcom/squareup/ui/XableEditText;

.field private final sortRow:Lcom/squareup/ui/activity/SortRowViewHolder;

.field private final tendersAdapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

.field private tendersList:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    const-class p2, Lcom/squareup/ui/activity/BulkSettleScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/activity/BulkSettleScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/activity/BulkSettleScreen$Component;->inject(Lcom/squareup/ui/activity/BulkSettleView;)V

    .line 83
    iget-object p2, p0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-static {p2, p1}, Lcom/squareup/ui/activity/SortRowViewHolder;->inflate(Lcom/squareup/ui/activity/BulkSettlePresenter;Landroid/content/Context;)Lcom/squareup/ui/activity/SortRowViewHolder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleView;->sortRow:Lcom/squareup/ui/activity/SortRowViewHolder;

    .line 84
    new-instance p1, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    invoke-direct {p1, p0}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;-><init>(Lcom/squareup/ui/activity/BulkSettleView;)V

    iput-object p1, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersAdapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/BulkSettleView;)Lcom/squareup/ui/activity/SortRowViewHolder;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/activity/BulkSettleView;->sortRow:Lcom/squareup/ui/activity/SortRowViewHolder;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 251
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_up_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarUpButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 252
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_up_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarUpButtonText:Lcom/squareup/marketfont/MarketTextView;

    .line 253
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ResizingConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarPrimaryButton:Lcom/squareup/ui/ResizingConfirmButton;

    .line 254
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_glyph_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 255
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->content:Landroid/view/ViewGroup;

    .line 256
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_search:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->searchBar:Lcom/squareup/ui/XableEditText;

    .line 257
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tenders_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersList:Landroidx/recyclerview/widget/RecyclerView;

    .line 258
    sget v0, Lcom/squareup/librarylist/R$id;->no_search_results:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->noSearchResults:Landroid/view/View;

    return-void
.end method

.method private showContentView(I)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 195
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 197
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, p1, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    .line 198
    :goto_1
    invoke-static {v2, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public synthetic lambda$moveToRowByKeyboard$2$BulkSettleView(I)V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersList:Landroidx/recyclerview/widget/RecyclerView;

    .line 189
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;

    .line 190
    invoke-virtual {p1}, Lcom/squareup/ui/activity/BulkSettleView$BulkSettleViewHolder;->onKeyboardMovedFromPreviousRow()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$BulkSettleView()V
    .locals 0

    .line 111
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$BulkSettleView()V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->onSettleClicked()V

    return-void
.end method

.method public moveToRowByKeyboard(I)V
    .locals 1

    if-ltz p1, :cond_1

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersAdapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->getItemCount()I

    move-result v0

    if-gt v0, p1, :cond_0

    goto :goto_0

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersList:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 186
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleView$CeXRzQZdaARi-aoy6BUfIXDtAYk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleView$CeXRzQZdaARi-aoy6BUfIXDtAYk;-><init>(Lcom/squareup/ui/activity/BulkSettleView;I)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/BulkSettleView;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public notifyRowsChanged()V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersAdapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->dropView(Ljava/lang/Object;)V

    .line 120
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 88
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/activity/BulkSettleView;->bindViews()V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersList:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 93
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettleView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 94
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 95
    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersList:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersList:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersAdapter:Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->searchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setSelectAllOnFocus(Z)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->searchBar:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/activity/BulkSettleView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/BulkSettleView$1;-><init>(Lcom/squareup/ui/activity/BulkSettleView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarUpButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/activity/BulkSettleView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/BulkSettleView$2;-><init>(Lcom/squareup/ui/activity/BulkSettleView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarPrimaryButton:Lcom/squareup/ui/ResizingConfirmButton;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleView$ovdyU34e1aNvMrSw0DS749PkHEo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleView$ovdyU34e1aNvMrSw0DS749PkHEo;-><init>(Lcom/squareup/ui/activity/BulkSettleView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ResizingConfirmButton;->setOnInitialClickListener(Lcom/squareup/ui/ResizingConfirmButton$OnInitialClickListener;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarPrimaryButton:Lcom/squareup/ui/ResizingConfirmButton;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleView$ny_dPUbzSxrn-43fGOyUa9xdhog;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$BulkSettleView$ny_dPUbzSxrn-43fGOyUa9xdhog;-><init>(Lcom/squareup/ui/activity/BulkSettleView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ResizingConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->presenter:Lcom/squareup/ui/activity/BulkSettlePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/BulkSettlePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setPrimaryButtonEnabled(Z)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarPrimaryButton:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->setEnabled(Z)V

    return-void
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    .line 136
    invoke-virtual {p0, p1, p1}, Lcom/squareup/ui/activity/BulkSettleView;->setPrimaryButtonText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarPrimaryButton:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ResizingConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarPrimaryButton:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ResizingConfirmButton;->setConfirmText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPrimaryButtonVisible(Z)V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarPrimaryButton:Lcom/squareup/ui/ResizingConfirmButton;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setUpButtonText(Ljava/lang/String;)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->actionBarUpButtonText:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showErrorMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/activity/BulkSettleView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 164
    sget p1, Lcom/squareup/billhistoryui/R$id;->bulk_settle_glyph_message:I

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/BulkSettleView;->showContentView(I)V

    return-void
.end method

.method public showLoading()V
    .locals 1

    .line 168
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_loading:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettleView;->showContentView(I)V

    return-void
.end method

.method public showNoSearchResultsMessage()V
    .locals 2

    .line 155
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tenders_list_container:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettleView;->showContentView(I)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->noSearchResults:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersList:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    return-void
.end method

.method public showNoTendersMessage()V
    .locals 3

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p0, Lcom/squareup/ui/activity/BulkSettleView;->currency:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/util/ProtoGlyphs;->circleCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/BulkSettleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$string;->bulk_settle_no_payments:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 151
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_glyph_message:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettleView;->showContentView(I)V

    return-void
.end method

.method public showTendersList()V
    .locals 2

    .line 172
    sget v0, Lcom/squareup/billhistoryui/R$id;->bulk_settle_tenders_list_container:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/BulkSettleView;->showContentView(I)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->tendersList:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/activity/BulkSettleView;->noSearchResults:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
