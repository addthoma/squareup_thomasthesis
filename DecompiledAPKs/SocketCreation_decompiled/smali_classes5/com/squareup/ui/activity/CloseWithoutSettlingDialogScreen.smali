.class public Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "CloseWithoutSettlingDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen$Factory;,
        Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$CloseWithoutSettlingDialogScreen$klTeAg4Cm6G8ksC89LDV70PlEnU;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$CloseWithoutSettlingDialogScreen$klTeAg4Cm6G8ksC89LDV70PlEnU;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;
    .locals 1

    .line 54
    const-class v0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    .line 55
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 54
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    .line 56
    new-instance v0, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;-><init>(Lcom/squareup/register/widgets/FailureAlertDialogFactory;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 48
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/activity/CloseWithoutSettlingDialogScreen;->failure:Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/ui/activity/BulkSettleScope;->INSTANCE:Lcom/squareup/ui/activity/BulkSettleScope;

    return-object v0
.end method
