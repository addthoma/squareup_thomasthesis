.class public final Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;
.super Ljava/lang/Object;
.source "BillHistoryDetailEmptyView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBillHistoryDetailEmptyView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BillHistoryDetailEmptyView.kt\ncom/squareup/ui/activity/BillHistoryDetailEmptyViewKt\n+ 2 MosaicBlock.kt\ncom/squareup/blueprint/mosaic/MosaicBlockKt\n+ 3 ImageUiModel.kt\ncom/squareup/mosaic/components/ImageUiModelKt\n*L\n1#1,57:1\n15#2,8:58\n20#2,2:66\n23#2:70\n15#2,8:71\n20#2,4:79\n15#3,2:68\n*E\n*S KotlinDebug\n*F\n+ 1 BillHistoryDetailEmptyView.kt\ncom/squareup/ui/activity/BillHistoryDetailEmptyViewKt\n*L\n41#1,8:58\n41#1,2:66\n41#1:70\n49#1,8:71\n49#1,4:79\n41#1,2:68\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\"\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00020\u0004H\u0002\u001a,\u0010\u0006\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u00020\u00042\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008H\u0002\u001a,\u0010\t\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\n*\u00020\u000b\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0003*\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u00020\u000cH\u0002\u001a\u0014\u0010\r\u001a\u00020\u0001*\u00020\u000e2\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0008\u00a8\u0006\u0010"
    }
    d2 = {
        "cardImageModel",
        "",
        "P",
        "",
        "Lcom/squareup/blueprint/BlueprintContext;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "cardLabelModel",
        "resId",
        "",
        "smallSpacing",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "Lcom/squareup/blueprint/LinearBlock;",
        "updateEmptyView",
        "Lcom/squareup/mosaic/core/Root;",
        "labelResId",
        "bill-history-ui_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$cardImageModel(Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;->cardImageModel(Lcom/squareup/blueprint/BlueprintContext;)V

    return-void
.end method

.method public static final synthetic access$cardLabelModel(Lcom/squareup/blueprint/BlueprintContext;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;->cardLabelModel(Lcom/squareup/blueprint/BlueprintContext;I)V

    return-void
.end method

.method public static final synthetic access$smallSpacing(Lcom/squareup/blueprint/LinearBlock;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt;->smallSpacing(Lcom/squareup/blueprint/LinearBlock;)V

    return-void
.end method

.method private static final cardImageModel(Lcom/squareup/blueprint/BlueprintContext;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;)V"
        }
    .end annotation

    .line 62
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 66
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 62
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 42
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    .line 68
    new-instance v2, Lcom/squareup/mosaic/components/ImageUiModel;

    invoke-interface {v1}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/mosaic/components/ImageUiModel;-><init>(Ljava/lang/Object;)V

    .line 42
    move-object v3, v2

    check-cast v3, Lcom/squareup/mosaic/core/DrawableModelContext;

    sget v4, Lcom/squareup/billhistoryui/R$drawable;->bill_details_empty_state_icon:I

    invoke-static {v3, v4}, Lcom/squareup/mosaic/drawables/SimpleDrawableModelKt;->resource(Lcom/squareup/mosaic/core/DrawableModelContext;I)V

    check-cast v2, Lcom/squareup/mosaic/core/UiModel;

    .line 68
    invoke-interface {v1, v2}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    .line 43
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 62
    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private static final cardLabelModel(Lcom/squareup/blueprint/BlueprintContext;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/BlueprintContext<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "TP;>;I)V"
        }
    .end annotation

    .line 75
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicBlock;

    .line 79
    invoke-interface {p0}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    .line 75
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/blueprint/mosaic/MosaicBlock;-><init>(Ljava/lang/Object;ZZ)V

    .line 50
    move-object v1, v0

    check-cast v1, Lcom/squareup/mosaic/core/UiModelContext;

    new-instance v2, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$cardLabelModel$$inlined$model$lambda$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$cardLabelModel$$inlined$model$lambda$1;-><init>(I)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/mosaic/components/LabelUiModelKt;->label(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V

    .line 56
    check-cast v0, Lcom/squareup/blueprint/Block;

    .line 75
    invoke-interface {p0, v0}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    return-void
.end method

.method private static final smallSpacing(Lcom/squareup/blueprint/LinearBlock;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Lcom/squareup/blueprint/UpdateContext;",
            "P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/blueprint/LinearBlock<",
            "TC;TP;>;)V"
        }
    .end annotation

    .line 46
    move-object v0, p0

    check-cast v0, Lcom/squareup/blueprint/Block;

    sget v1, Lcom/squareup/billhistoryui/R$dimen;->bill_history_detail_empty_view_spacing_between_icon_and_text:I

    invoke-static {v0, v1}, Lcom/squareup/blueprint/BlockKt;->dimen(Lcom/squareup/blueprint/Block;I)Lcom/squareup/resources/DimenModel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/blueprint/LinearBlock;->spacing(Lcom/squareup/resources/DimenModel;)V

    return-void
.end method

.method public static final updateEmptyView(Lcom/squareup/mosaic/core/Root;I)V
    .locals 1

    const-string v0, "$this$updateEmptyView"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$updateEmptyView$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/activity/BillHistoryDetailEmptyViewKt$updateEmptyView$1;-><init>(I)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0}, Lcom/squareup/mosaic/core/Root;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
