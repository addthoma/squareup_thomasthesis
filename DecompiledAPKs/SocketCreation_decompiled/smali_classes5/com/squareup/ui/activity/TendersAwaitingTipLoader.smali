.class public Lcom/squareup/ui/activity/TendersAwaitingTipLoader;
.super Ljava/lang/Object;
.source "TendersAwaitingTipLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;,
        Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;,
        Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;,
        Lcom/squareup/ui/activity/TendersAwaitingTipLoader$Listener;
    }
.end annotation


# instance fields
.field private final employees:Lcom/squareup/permissions/Employees;

.field private errorMessage:Ljava/lang/String;

.field private errorTitle:Ljava/lang/String;

.field private listener:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$Listener;

.field private loadState:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

.field private final res:Lcom/squareup/util/Res;

.field private searchQuery:Ljava/lang/String;

.field private final searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private sortField:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

.field private final tenderComparator:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;

.field private final tenderLister:Lcom/squareup/papersignature/TenderTipLister;

.field private tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/papersignature/TenderTipLister;Lcom/squareup/util/Res;Lcom/squareup/permissions/Employees;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenderLister:Lcom/squareup/papersignature/TenderTipLister;

    .line 79
    iput-object p2, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->res:Lcom/squareup/util/Res;

    .line 80
    iput-object p3, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->employees:Lcom/squareup/permissions/Employees;

    .line 82
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    .line 83
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchResults:Ljava/util/List;

    .line 84
    new-instance p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;-><init>(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;)V

    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenderComparator:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;

    .line 86
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;->TIME:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->sortField:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    .line 87
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->NOT_LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->loadState:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Ljava/util/List;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->onTendersLoaded(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->onTenderLoadFailed(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->sortField:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)Lcom/squareup/permissions/Employees;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->employees:Lcom/squareup/permissions/Employees;

    return-object p0
.end method

.method private fireStateChanged()V
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->listener:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$Listener;

    if-eqz v0, :cond_0

    .line 282
    invoke-interface {v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$Listener;->onLoaderStateChanged()V

    :cond_0
    return-void
.end method

.method private matchesSearchQuery(Lcom/squareup/billhistory/model/TenderHistory;)Z
    .locals 2

    .line 275
    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->receiptNumber:Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchQuery:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 277
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method private onTenderLoadFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 230
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->no_internet_connection:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->errorTitle:Ljava/lang/String;

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/billhistoryui/R$string;->restore_connectivity_to_settle_plural_noperiod:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->errorMessage:Ljava/lang/String;

    goto :goto_0

    .line 234
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->errorTitle:Ljava/lang/String;

    .line 235
    iput-object p2, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->errorMessage:Ljava/lang/String;

    .line 238
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 239
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->ERROR:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->setLoadState(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;)V

    return-void
.end method

.method private onTendersLoaded(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 200
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 202
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->wrapTenderHistories(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    .line 204
    invoke-direct {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->sortTenders()V

    .line 205
    sget-object p1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADED:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->setLoadState(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;)V

    return-void
.end method

.method private rebuildSearchResults()V
    .locals 3

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 263
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 267
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 268
    iget-object v1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/TenderEditState;

    iget-object v1, v1, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-direct {p0, v1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->matchesSearchQuery(Lcom/squareup/billhistory/model/TenderHistory;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchResults:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private setLoadState(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;)V
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->loadState:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    if-eq p1, v0, :cond_0

    .line 244
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->loadState:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    .line 245
    invoke-direct {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->fireStateChanged()V

    :cond_0
    return-void
.end method

.method private sortTenders()V
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenderComparator:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$TenderSortComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    invoke-direct {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->rebuildSearchResults()V

    :cond_0
    return-void
.end method

.method private wrapTenderHistories(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;"
        }
    .end annotation

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 215
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 219
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->shouldPrintTipEntryInput()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 221
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    .line 222
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->withoutTip()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v1

    .line 224
    :cond_1
    new-instance v2, Lcom/squareup/ui/activity/TenderEditState;

    invoke-direct {v2, v1}, Lcom/squareup/ui/activity/TenderEditState;-><init>(Lcom/squareup/billhistory/model/TenderHistory;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public getAllTendersCount()I
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorTitle()Ljava/lang/String;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->errorTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadState()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->loadState:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    return-object v0
.end method

.method public getSortField()Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->sortField:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    return-object v0
.end method

.method public getTenderAt(I)Lcom/squareup/ui/activity/TenderEditState;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/TenderEditState;

    return-object p1
.end method

.method public getTenderCount()I
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getTenderIds()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 152
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/activity/TenderEditState;

    .line 154
    iget-object v2, v2, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    iget-object v2, v2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getTenders()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/activity/TenderEditState;",
            ">;"
        }
    .end annotation

    .line 141
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 143
    iget-object v1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 144
    iget-object v3, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasSearchQuery()Z
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchQuery:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasTenders()Z
    .locals 3

    .line 115
    invoke-virtual {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->hasSearchQuery()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchResults:Ljava/util/List;

    .line 116
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenders:Ljava/util/List;

    .line 117
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public load()V
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->loadState:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    sget-object v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADING:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    if-ne v0, v1, :cond_0

    return-void

    .line 164
    :cond_0
    sget-object v0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;->LOADING:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->setLoadState(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$LoadState;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->tenderLister:Lcom/squareup/papersignature/TenderTipLister;

    new-instance v1, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader$1;-><init>(Lcom/squareup/ui/activity/TendersAwaitingTipLoader;)V

    invoke-virtual {v0, v1}, Lcom/squareup/papersignature/TenderTipLister;->getTendersAwaitingTip(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method

.method public setListener(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$Listener;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->listener:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$Listener;

    return-void
.end method

.method public setSearchQuery(Ljava/lang/String;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchQuery:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->searchQuery:Ljava/lang/String;

    .line 109
    invoke-direct {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->rebuildSearchResults()V

    .line 110
    invoke-direct {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->fireStateChanged()V

    :cond_0
    return-void
.end method

.method public setSortField(Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;)V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->sortField:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    if-eq v0, p1, :cond_0

    .line 96
    iput-object p1, p0, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->sortField:Lcom/squareup/ui/activity/TendersAwaitingTipLoader$SortField;

    .line 97
    invoke-direct {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->sortTenders()V

    .line 98
    invoke-direct {p0}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->fireStateChanged()V

    :cond_0
    return-void
.end method

.method setTestTenders(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation

    .line 195
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/TendersAwaitingTipLoader;->onTendersLoaded(Ljava/util/List;)V

    return-void
.end method
