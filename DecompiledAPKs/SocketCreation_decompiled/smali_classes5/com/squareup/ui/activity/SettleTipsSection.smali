.class public Lcom/squareup/ui/activity/SettleTipsSection;
.super Landroid/widget/LinearLayout;
.source "SettleTipsSection.java"


# instance fields
.field presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final settleProgressPopup:Lcom/squareup/caller/ProgressPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const-class p2, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/activity/BillHistoryDetailScreen$Component;->inject(Lcom/squareup/ui/activity/SettleTipsSection;)V

    const/4 p2, 0x1

    .line 33
    invoke-virtual {p0, p2}, Lcom/squareup/ui/activity/SettleTipsSection;->setOrientation(I)V

    .line 35
    new-instance p2, Lcom/squareup/caller/ProgressPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/activity/SettleTipsSection;->settleProgressPopup:Lcom/squareup/caller/ProgressPopup;

    return-void
.end method

.method private setUpTransitions()V
    .locals 9

    .line 61
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    const/4 v1, 0x2

    .line 62
    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    const/4 v2, 0x0

    .line 63
    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    const/4 v3, 0x4

    .line 64
    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    const/4 v4, 0x3

    .line 65
    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    const/4 v5, 0x1

    .line 66
    invoke-virtual {v0, v5}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    const-wide/16 v6, 0x0

    .line 67
    invoke-virtual {v0, v5, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 68
    new-instance v8, Lcom/squareup/ui/activity/SettleTipsSection$1;

    invoke-direct {v8, p0}, Lcom/squareup/ui/activity/SettleTipsSection$1;-><init>(Lcom/squareup/ui/activity/SettleTipsSection;)V

    invoke-virtual {v0, v8}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 75
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/SettleTipsSection;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 78
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 79
    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 80
    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 81
    invoke-virtual {v0, v5}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 82
    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 83
    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 84
    invoke-virtual {v0, v3, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 85
    new-instance v1, Lcom/squareup/ui/activity/SettleTipsSection$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/SettleTipsSection$2;-><init>(Lcom/squareup/ui/activity/SettleTipsSection;)V

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSection;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSection;->presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    iget-object v0, v0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleProgressPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSection;->settleProgressPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSection;->presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->dropView(Lcom/squareup/ui/activity/SettleTipsSection;)V

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSection;->presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    iget-object v0, v0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->settleProgressPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipsSection;->settleProgressPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipsSection;->presenter:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method removeRow(Landroid/view/View;)V
    .locals 3

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/activity/SettleTipsSection;->setUpTransitions()V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSection;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 55
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 56
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/SettleTipsSection;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSection;->removeView(Landroid/view/View;)V

    return-void
.end method

.method removeTransitions()V
    .locals 2

    const/4 v0, 0x0

    .line 96
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/SettleTipsSection;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SettleTipsSection;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method
