.class Lcom/squareup/ui/activity/SelectReceiptTenderView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SelectReceiptTenderView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/SelectReceiptTenderView;->addTender(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/SelectReceiptTenderView;

.field final synthetic val$tenderId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/SelectReceiptTenderView;Ljava/lang/String;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView$1;->this$0:Lcom/squareup/ui/activity/SelectReceiptTenderView;

    iput-object p2, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView$1;->val$tenderId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView$1;->this$0:Lcom/squareup/ui/activity/SelectReceiptTenderView;

    iget-object p1, p1, Lcom/squareup/ui/activity/SelectReceiptTenderView;->presenter:Lcom/squareup/ui/activity/SelectReceiptTenderPresenter;

    iget-object v0, p0, Lcom/squareup/ui/activity/SelectReceiptTenderView$1;->val$tenderId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/SelectReceiptTenderPresenter;->tenderSelected(Ljava/lang/String;)V

    return-void
.end method
