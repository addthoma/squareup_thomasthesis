.class Lcom/squareup/ui/activity/QuickTipEditorPresenter;
.super Ljava/lang/Object;
.source "QuickTipEditorPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;
    }
.end annotation


# instance fields
.field private isDisplayingPercentages:Z

.field private isQuickTipOptionUsed:Z

.field private isQuickTipOptionsEnabled:Z

.field private final maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private skipUpdate:Z

.field private tender:Lcom/squareup/billhistory/model/TenderHistory;

.field private tipAmountListener:Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;

.field private tipOptionAmounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private tipOptionLabels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tipOptions:Lcom/squareup/tipping/TipOptions;

.field private final tippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

.field private view:Lcom/squareup/ui/activity/QuickTipEditor;


# direct methods
.method constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/tipping/TipOptions;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/tipping/TipOptions;",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p3, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 68
    iput-object p4, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipOptions:Lcom/squareup/tipping/TipOptions;

    .line 69
    iput-object p5, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    .line 71
    new-instance p4, Lcom/squareup/money/MaxMoneyScrubber;

    const-wide/16 v0, 0x0

    .line 72
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p4, p3, p2, p1}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object p4, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    return-void
.end method

.method private fireActionNextPressed()V
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipAmountListener:Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;

    if-eqz v0, :cond_0

    .line 203
    invoke-interface {v0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;->onKeyboardActionNextClicked()V

    :cond_0
    return-void
.end method

.method private fireTipAmountChanged(Ljava/lang/Long;)V
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipAmountListener:Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 210
    iput-boolean v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->skipUpdate:Z

    .line 211
    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;->onTipAmountChanged(Ljava/lang/Long;)V

    const/4 p1, 0x0

    .line 212
    iput-boolean p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->skipUpdate:Z

    :cond_0
    return-void
.end method

.method private updateView()V
    .locals 3

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    if-nez v0, :cond_0

    goto :goto_0

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    invoke-virtual {v1, v0}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->calculateTipOptions(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/util/List;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipOptions:Lcom/squareup/tipping/TipOptions;

    invoke-virtual {v1, v0}, Lcom/squareup/tipping/TipOptions;->getTipOptionLabels(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipOptionLabels:Ljava/util/List;

    .line 184
    invoke-static {v0}, Lcom/squareup/tipping/TipOptions;->getTipOptionAmounts(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipOptionAmounts:Ljava/util/List;

    .line 185
    invoke-static {v0}, Lcom/squareup/tipping/TipOptions;->isDisplayingPercentages(Ljava/util/List;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isDisplayingPercentages:Z

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->shouldDisplayQuickTipOptions(Lcom/squareup/billhistory/model/TenderHistory;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionsEnabled:Z

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipOptionLabels:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipOptionAmounts:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/activity/QuickTipEditor;->setTipOptions(Ljava/util/List;Ljava/util/List;)V

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tippingCalculator:Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    iget-object v2, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {v1, v2}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;->getCustomTipMaxMoney(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 193
    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/activity/QuickTipEditor;->showTipAmount(Ljava/lang/Long;)V

    goto :goto_0

    .line 194
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionsEnabled:Z

    if-eqz v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->showTipOptions()V

    goto :goto_0

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/QuickTipEditor;->showTipAmount(Ljava/lang/Long;)V

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public dropView()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    iget-object v1, v1, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->removeScrubbingTextWatcher(Lcom/squareup/text/HasSelectableText;)V

    const/4 v0, 0x0

    .line 88
    iput-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    return-void
.end method

.method getTipAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->isTipAmountVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    iget-object v1, v1, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/OnDeleteEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTipOptionAmounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipOptionAmounts:Ljava/util/List;

    return-object v0
.end method

.method public getTipOptionLabels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipOptionLabels:Ljava/util/List;

    return-object v0
.end method

.method public isDisplayingPercentages()Z
    .locals 1

    .line 107
    iget-boolean v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isDisplayingPercentages:Z

    return v0
.end method

.method public isQuickTipOptionUsed()Z
    .locals 1

    .line 111
    iget-boolean v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionUsed:Z

    return v0
.end method

.method isQuickTipOptionsEnabled()Z
    .locals 1

    .line 126
    iget-boolean v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionsEnabled:Z

    return v0
.end method

.method onActionNextPressed()V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    if-nez v0, :cond_0

    return-void

    .line 149
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->isTipAmountEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionsEnabled:Z

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->showTipOptions()V

    .line 152
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->fireActionNextPressed()V

    return-void
.end method

.method onDeleteKey()V
    .locals 1

    .line 156
    iget-boolean v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionsEnabled:Z

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->showTipOptions()V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    iget-object v0, v0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method onFocusChange(Z)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 167
    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->isTipAmountEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionsEnabled:Z

    if-eqz p1, :cond_1

    .line 168
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/QuickTipEditor;->showTipOptions()V

    :cond_1
    return-void
.end method

.method onTipAmountChanged(Z)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    if-nez v0, :cond_0

    return-void

    .line 140
    :cond_0
    iput-boolean p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->isQuickTipOptionUsed:Z

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v0, v0, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/OnDeleteEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-nez p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    .line 142
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->fireTipAmountChanged(Ljava/lang/Long;)V

    return-void
.end method

.method onTipOptionSelected(Ljava/lang/Long;)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/QuickTipEditor;->showTipAmount(Ljava/lang/Long;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/QuickTipEditor;->focusOnTipAmount()V

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 132
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->onTipAmountChanged(Z)V

    return-void
.end method

.method setOnTipAmountListener(Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;)V
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tipAmountListener:Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;

    return-void
.end method

.method public setTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->tender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 93
    iget-boolean p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->skipUpdate:Z

    if-nez p1, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->updateView()V

    :cond_0
    return-void
.end method

.method public takeView(Lcom/squareup/ui/activity/QuickTipEditor;)V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    if-nez v0, :cond_0

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p1, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object p1, p1, Lcom/squareup/ui/activity/QuickTipEditor;->tipAmountView:Lcom/squareup/widgets/OnDeleteEditText;

    invoke-virtual {v1, p1}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->maxMoneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 83
    invoke-direct {p0}, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->updateView()V

    return-void

    .line 77
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Should not already have a view "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/activity/QuickTipEditorPresenter;->view:Lcom/squareup/ui/activity/QuickTipEditor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
