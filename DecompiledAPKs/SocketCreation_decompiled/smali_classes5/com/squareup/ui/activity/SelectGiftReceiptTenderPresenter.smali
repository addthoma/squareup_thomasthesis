.class Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;
.super Lcom/squareup/activity/AbstractActivityCardPresenter;
.source "SelectGiftReceiptTenderPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/activity/AbstractActivityCardPresenter<",
        "Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;",
        ">;"
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/activity/AbstractActivityCardPresenter;-><init>(Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;)V

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private updateView(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;)V
    .locals 5

    .line 50
    invoke-virtual {p1}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;->clearTenders()V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v0

    .line 54
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 55
    iget-object v2, v1, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v3, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    if-eq v2, v3, :cond_0

    .line 56
    iget-object v2, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->res:Lcom/squareup/util/Res;

    .line 57
    invoke-virtual {v1, v4}, Lcom/squareup/billhistory/model/TenderHistory;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v4

    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 56
    invoke-virtual {p1, v2, v3, v4, v1}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;->addTender(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method protected getActionBarText()Ljava/lang/String;
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->print_gift_receipt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$SelectGiftReceiptTenderPresenter(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 37
    iget-object p2, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {p2}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->updateView(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$1$SelectGiftReceiptTenderPresenter(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->onTransactionsHistoryChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$SelectGiftReceiptTenderPresenter$GaiJi_FwKXhgA_iOfwRruewHML0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$SelectGiftReceiptTenderPresenter$GaiJi_FwKXhgA_iOfwRruewHML0;-><init>(Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;)V

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 28
    invoke-super {p0, p1}, Lcom/squareup/activity/AbstractActivityCardPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->createDefaultActionBarConfigBuilder()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 34
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$SelectGiftReceiptTenderPresenter$NNiaCpz3A8dBqKuVDvYThKirI0Y;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$SelectGiftReceiptTenderPresenter$NNiaCpz3A8dBqKuVDvYThKirI0Y;-><init>(Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->updateView(Lcom/squareup/ui/activity/SelectGiftReceiptTenderView;)V

    return-void
.end method

.method tenderSelected(Ljava/lang/String;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->printGiftReceiptForSelectedTender(Ljava/lang/String;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/activity/SelectGiftReceiptTenderPresenter;->runner:Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;

    invoke-interface {p1}, Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;->goBack()V

    return-void
.end method
