.class synthetic Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;
.super Ljava/lang/Object;
.source "LegacyTransactionsHistoryListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

.field static final synthetic $SwitchMap$com$squareup$protos$client$instantdeposits$EligibilityBlocker:[I

.field static final synthetic $SwitchMap$com$squareup$ui$activity$LegacyTransactionsHistoryListPresenter$RowType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 508
    invoke-static {}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->values()[Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$protos$client$instantdeposits$EligibilityBlocker:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$protos$client$instantdeposits$EligibilityBlocker:[I

    sget-object v2, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    invoke-virtual {v2}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$protos$client$instantdeposits$EligibilityBlocker:[I

    sget-object v3, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNVERIFIED_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    invoke-virtual {v3}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$protos$client$instantdeposits$EligibilityBlocker:[I

    sget-object v4, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->FUNDING_SOURCE_VERIFICATION_EXPIRED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    invoke-virtual {v4}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 467
    :catch_2
    invoke-static {}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->values()[Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    :try_start_3
    sget-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    sget-object v4, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->LOADING:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v4}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    sget-object v4, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->CAN_MAKE_DEPOSIT:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v4}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    sget-object v4, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->NOT_ELIGIBLE:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v4}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    .line 276
    :catch_5
    invoke-static {}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->values()[Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$ui$activity$LegacyTransactionsHistoryListPresenter$RowType:[I

    :try_start_6
    sget-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$ui$activity$LegacyTransactionsHistoryListPresenter$RowType:[I

    sget-object v4, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->BILL:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    invoke-virtual {v4}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$ui$activity$LegacyTransactionsHistoryListPresenter$RowType:[I

    sget-object v3, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->LOAD_MORE:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    invoke-virtual {v3}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$3;->$SwitchMap$com$squareup$ui$activity$LegacyTransactionsHistoryListPresenter$RowType:[I

    sget-object v1, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->SHOW_ALL:Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/LegacyTransactionsHistoryListPresenter$RowType;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    return-void
.end method
