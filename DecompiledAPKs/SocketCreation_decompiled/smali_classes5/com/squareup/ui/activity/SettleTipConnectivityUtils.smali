.class public Lcom/squareup/ui/activity/SettleTipConnectivityUtils;
.super Ljava/lang/Object;
.source "SettleTipConnectivityUtils.java"


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 17
    iput-object p2, p0, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public getNoInternetPopupScreen(Z)Lcom/squareup/register/widgets/WarningDialogScreen;
    .locals 3

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->no_internet_connection:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->res:Lcom/squareup/util/Res;

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/billhistoryui/R$string;->restore_connectivity_to_settle_plural:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/billhistoryui/R$string;->restore_connectivity_to_settle_single:I

    :goto_0
    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 25
    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v2, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-direct {v2, v0, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    return-object v1
.end method

.method public isOffline()Z
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/activity/SettleTipConnectivityUtils;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
