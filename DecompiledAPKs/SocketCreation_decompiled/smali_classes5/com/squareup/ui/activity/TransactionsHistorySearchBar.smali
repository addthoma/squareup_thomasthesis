.class public final Lcom/squareup/ui/activity/TransactionsHistorySearchBar;
.super Ljava/lang/Object;
.source "SearchBar.kt"

# interfaces
.implements Lcom/squareup/ui/activity/SearchBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/TransactionsHistorySearchBar$SearchPlugin;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSearchBar.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SearchBar.kt\ncom/squareup/ui/activity/TransactionsHistorySearchBar\n*L\n1#1,155:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0004\u0008\u0000\u0018\u00002\u00020\u0001:\u0001#B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u0006H\u0016J\n\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0012H\u0016J\u0008\u0010\u0014\u001a\u00020\u0012H\u0016J\u0010\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0019H\u0016J\u0012\u0010\u001a\u001a\u00020\u00062\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u00062\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u00062\u0006\u0010\"\u001a\u00020\u0012H\u0016R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/activity/TransactionsHistorySearchBar;",
        "Lcom/squareup/ui/activity/SearchBar;",
        "viewGroup",
        "Landroid/view/ViewGroup;",
        "onClearButtonClicked",
        "Lkotlin/Function0;",
        "",
        "(Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)V",
        "searchBar",
        "Lcom/squareup/noho/NohoEditRow;",
        "addTextChangedListener",
        "listener",
        "Landroid/text/TextWatcher;",
        "clearFocus",
        "getText",
        "Landroid/text/Editable;",
        "isEnabled",
        "enabled",
        "",
        "isFocused",
        "requestFocus",
        "setHint",
        "hint",
        "",
        "setOnClickListener",
        "Landroid/view/View$OnClickListener;",
        "setOnEditorActionListener",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "setOnFocusChangeListener",
        "Landroid/view/View$OnFocusChangeListener;",
        "setText",
        "text",
        "",
        "setVisibleOrGone",
        "visible",
        "SearchPlugin",
        "bill-history-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onClearButtonClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final searchBar:Lcom/squareup/noho/NohoEditRow;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function0;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "viewGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClearButtonClicked"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->onClearButtonClicked:Lkotlin/jvm/functions/Function0;

    .line 92
    check-cast p1, Landroid/view/View;

    sget p2, Lcom/squareup/billhistoryui/R$id;->transactions_history_search_bar:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    .line 96
    new-instance p2, Lcom/squareup/ui/activity/TransactionsHistorySearchBar$SearchPlugin;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, v0}, Lcom/squareup/ui/activity/TransactionsHistorySearchBar$SearchPlugin;-><init>(Landroid/content/Context;)V

    check-cast p2, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 98
    new-instance p2, Lcom/squareup/noho/ClearPlugin;

    .line 99
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$color;->transactions_history_search_bar_icon:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/squareup/noho/ClearPlugin$Visibility;

    .line 101
    sget-object v3, Lcom/squareup/noho/ClearPlugin$Visibility;->FOCUSED:Lcom/squareup/noho/ClearPlugin$Visibility;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Lcom/squareup/noho/ClearPlugin$Visibility;->WITH_TEXT:Lcom/squareup/noho/ClearPlugin$Visibility;

    const/4 v5, 0x1

    aput-object v3, v2, v5

    invoke-static {v2}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    .line 102
    iget-object v3, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->onClearButtonClicked:Lkotlin/jvm/functions/Function0;

    .line 98
    invoke-direct {p2, v0, v1, v2, v3}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 97
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 105
    invoke-virtual {p1, v5}, Lcom/squareup/noho/NohoEditRow;->setSelectAllOnFocus(Z)V

    .line 106
    invoke-virtual {p1, v4}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public clearFocus()V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->clearFocus()V

    return-void
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public isEnabled(Z)V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setEnabled(Z)V

    return-void
.end method

.method public isFocused()Z
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->isFocused()Z

    move-result v0

    return v0
.end method

.method public requestFocus()Z
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->requestFocus()Z

    move-result v0

    return v0
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 1

    const-string v0, "hint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVisibleOrGone(Z)V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistorySearchBar;->searchBar:Lcom/squareup/noho/NohoEditRow;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
