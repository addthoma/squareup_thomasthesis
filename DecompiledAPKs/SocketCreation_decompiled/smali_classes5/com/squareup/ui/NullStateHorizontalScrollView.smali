.class public Lcom/squareup/ui/NullStateHorizontalScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "NullStateHorizontalScrollView.java"


# instance fields
.field private containerView:Landroid/view/ViewGroup;

.field private final maxTextSize:I

.field private final minTextSize:I

.field private nullStateText:Ljava/lang/String;

.field private final textBounds:Landroid/graphics/Rect;

.field private final textPaint:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textBounds:Landroid/graphics/Rect;

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/NullStateHorizontalScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 37
    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_header_title:I

    .line 38
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->maxTextSize:I

    .line 39
    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_default:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->minTextSize:I

    .line 41
    new-instance v1, Landroid/text/TextPaint;

    const/16 v2, 0x81

    invoke-direct {v1, v2}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textPaint:Landroid/text/TextPaint;

    .line 42
    iget-object v1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textPaint:Landroid/text/TextPaint;

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, v2}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 43
    iget-object v1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textPaint:Landroid/text/TextPaint;

    sget v2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->maxTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 47
    sget-object v0, Lcom/squareup/widgets/R$styleable;->NullStateHorizontalScrollView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 49
    sget p2, Lcom/squareup/widgets/R$styleable;->NullStateHorizontalScrollView_nullStateText:I

    .line 50
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 49
    invoke-virtual {p0, p2}, Lcom/squareup/ui/NullStateHorizontalScrollView;->setNullStateText(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    const/4 p1, 0x0

    .line 53
    invoke-virtual {p0, p1}, Lcom/squareup/ui/NullStateHorizontalScrollView;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 75
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onDraw(Landroid/graphics/Canvas;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->nullStateText:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->containerView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 77
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->containerView:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 79
    iget-object v2, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->nullStateText:Ljava/lang/String;

    int-to-float v1, v1

    int-to-float v0, v0

    iget-object v3, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v2, v1, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 57
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    const/4 v0, 0x0

    .line 58
    invoke-virtual {p0, v0}, Lcom/squareup/ui/NullStateHorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->containerView:Landroid/view/ViewGroup;

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 67
    invoke-super {p0, p1, p2}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->nullStateText:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textPaint:Landroid/text/TextPaint;

    iget-object p2, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->nullStateText:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/ui/NullStateHorizontalScrollView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->minTextSize:I

    int-to-float v1, v1

    iget v2, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->maxTextSize:I

    int-to-float v2, v2

    invoke-static {p1, p2, v0, v1, v2}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFF)V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textPaint:Landroid/text/TextPaint;

    iget-object p2, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->nullStateText:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method public setNullStateText(Ljava/lang/String;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/NullStateHorizontalScrollView;->nullStateText:Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Lcom/squareup/ui/NullStateHorizontalScrollView;->invalidate()V

    return-void
.end method
