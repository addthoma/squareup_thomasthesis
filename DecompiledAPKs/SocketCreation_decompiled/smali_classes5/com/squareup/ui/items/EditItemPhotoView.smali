.class public Lcom/squareup/ui/items/EditItemPhotoView;
.super Lcom/squareup/widgets/PairLayout;
.source "EditItemPhotoView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Lcom/squareup/picasso/Target;


# instance fields
.field private animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private hint:Landroid/widget/TextView;

.field private image:Landroid/widget/ImageView;

.field private imageSize:I

.field private itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

.field private observer:Landroid/view/ViewTreeObserver;

.field photoFactory:Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field toastFactory:Lcom/squareup/util/ToastFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemPhotoView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditItemPhotoScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemPhotoScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditItemPhotoScreen$Component;->inject(Lcom/squareup/ui/items/EditItemPhotoView;)V

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemPhotoPresenter;->cancelClicked()V

    const/4 v0, 0x1

    return v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/squareup/ui/Bitmaps;->createBitmapFromView(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 108
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;

    invoke-virtual {v1, p0, v0}, Lcom/squareup/ui/items/EditItemPhotoPresenter;->onBackPressed(Lcom/squareup/ui/items/EditItemPhotoView;Landroid/graphics/Bitmap;)Z

    move-result v0

    return v0
.end method

.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v0, Lcom/squareup/registerlib/R$id;->edit_photo_error_message:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->hint:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemPhotoPresenter;->bitmapFailed()V

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 0

    .line 82
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    iget-object p2, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/widget/ImageView;->getId()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 87
    new-instance p1, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    iget-object p2, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    invoke-direct {p1, p2}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;-><init>(Landroid/widget/ImageView;)V

    sget-object p2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, p2}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->hint:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemPhotoPresenter;->bitmapLoaded()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemPhotoPresenter;->dropView(Ljava/lang/Object;)V

    .line 66
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 50
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 52
    sget v0, Lcom/squareup/registerlib/R$id;->edit_photo_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    .line 53
    sget v0, Lcom/squareup/registerlib/R$id;->edit_photo_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->hint:Landroid/widget/TextView;

    .line 54
    sget v0, Lcom/squareup/registerlib/R$id;->edit_photo_animator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->observer:Landroid/view/ViewTreeObserver;

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->observer:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemPhotoPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onGlobalLayout()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->observer:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->image:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->observer:Landroid/view/ViewTreeObserver;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->observer:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    invoke-virtual {v0}, Lcom/squareup/widgets/SquareViewAnimator;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    invoke-virtual {v1}, Lcom/squareup/widgets/SquareViewAnimator;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->imageSize:I

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->presenter:Lcom/squareup/ui/items/EditItemPhotoPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditItemPhotoPresenter;->contentMeasured(Lcom/squareup/ui/items/EditItemPhotoView;)V

    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method photoSaved(Z)V
    .locals 2

    if-nez p1, :cond_0

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v0, Lcom/squareup/edititem/R$string;->merchant_profile_saving_image_failed:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    :cond_0
    return-void
.end method

.method setContent(Landroid/net/Uri;)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->photoFactory:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Z)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemPhotoView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    iget v0, p0, Lcom/squareup/ui/items/EditItemPhotoView;->imageSize:I

    invoke-virtual {p1, v0, p0}, Lcom/squareup/ui/photo/ItemPhoto;->into(ILcom/squareup/picasso/Target;)V

    return-void
.end method
