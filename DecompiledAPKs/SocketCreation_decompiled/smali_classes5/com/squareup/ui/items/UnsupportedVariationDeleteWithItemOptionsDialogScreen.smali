.class public final Lcom/squareup/ui/items/UnsupportedVariationDeleteWithItemOptionsDialogScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "UnsupportedVariationDeleteWithItemOptionsDialogScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/UnsupportedVariationDeleteWithItemOptionsDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/UnsupportedVariationDeleteWithItemOptionsDialogScreen$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u0004B\u0007\u0008\u0000\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/items/UnsupportedVariationDeleteWithItemOptionsDialogScreen;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "()V",
        "Factory",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    .line 16
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
