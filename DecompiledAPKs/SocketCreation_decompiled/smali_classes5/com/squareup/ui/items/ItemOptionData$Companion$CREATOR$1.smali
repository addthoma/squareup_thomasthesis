.class public final Lcom/squareup/ui/items/ItemOptionData$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "ItemOptionData.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemOptionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/items/ItemOptionData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemOptionData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemOptionData.kt\ncom/squareup/ui/items/ItemOptionData$Companion$CREATOR$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,123:1\n1642#2,2:124\n*E\n*S KotlinDebug\n*F\n+ 1 ItemOptionData.kt\ncom/squareup/ui/items/ItemOptionData$Companion$CREATOR$1\n*L\n83#1,2:124\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/ui/items/ItemOptionData$Companion$CREATOR$1",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/ui/items/ItemOptionData;",
        "createFromParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/ui/items/ItemOptionData;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/ItemOptionData;
    .locals 4

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 83
    new-instance v2, Lkotlin/ranges/IntRange;

    const/4 v3, 0x1

    invoke-direct {v2, v3, v0}, Lkotlin/ranges/IntRange;-><init>(II)V

    check-cast v2, Ljava/lang/Iterable;

    .line 124
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lkotlin/collections/IntIterator;

    invoke-virtual {v2}, Lkotlin/collections/IntIterator;->nextInt()I

    .line 85
    sget-object v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 85
    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->parse([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object v2

    .line 84
    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 88
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    :cond_0
    new-instance p1, Lcom/squareup/ui/items/ItemOptionData;

    check-cast v1, Ljava/util/Collection;

    invoke-direct {p1, v1}, Lcom/squareup/ui/items/ItemOptionData;-><init>(Ljava/util/Collection;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 79
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/ItemOptionData$Companion$CREATOR$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/ItemOptionData;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/items/ItemOptionData;
    .locals 0

    .line 95
    new-array p1, p1, [Lcom/squareup/ui/items/ItemOptionData;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 79
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/ItemOptionData$Companion$CREATOR$1;->newArray(I)[Lcom/squareup/ui/items/ItemOptionData;

    move-result-object p1

    return-object p1
.end method
