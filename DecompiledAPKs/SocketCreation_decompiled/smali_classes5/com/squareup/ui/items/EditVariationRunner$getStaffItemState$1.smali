.class final Lcom/squareup/ui/items/EditVariationRunner$getStaffItemState$1;
.super Ljava/lang/Object;
.source "EditVariationRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditVariationRunner;->getStaffItemState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;",
        "staffInfos",
        "",
        "Lcom/squareup/appointmentsapi/StaffInfo;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditVariationRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditVariationRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$getStaffItemState$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/List;)Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/appointmentsapi/StaffInfo;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;"
        }
    .end annotation

    const-string v0, "staffInfos"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 456
    new-instance v0, Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;

    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner$getStaffItemState$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditVariationState;->getVariationInEditing()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getEmployeeTokens()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditVariationRunner$getStaffItemState$1;->apply(Ljava/util/List;)Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;

    move-result-object p1

    return-object p1
.end method
