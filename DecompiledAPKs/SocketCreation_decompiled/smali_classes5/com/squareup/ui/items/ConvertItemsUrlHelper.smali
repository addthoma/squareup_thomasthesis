.class public final Lcom/squareup/ui/items/ConvertItemsUrlHelper;
.super Ljava/lang/Object;
.source "ConvertItemsUrlHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
        "",
        "server",
        "Lcom/squareup/http/Server;",
        "(Lcom/squareup/http/Server;)V",
        "getConvertItemsURL",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final server:Lcom/squareup/http/Server;


# direct methods
.method public constructor <init>(Lcom/squareup/http/Server;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "server"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/ConvertItemsUrlHelper;->server:Lcom/squareup/http/Server;

    return-void
.end method


# virtual methods
.method public final getConvertItemsURL()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/items/ConvertItemsUrlHelper;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->isProduction()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://squareup.com/dashboard/items/services/convert-items"

    goto :goto_0

    :cond_0
    const-string v0, "https://squareupstaging.com/dashboard/items/services/convert-items"

    :goto_0
    return-object v0
.end method
