.class public final Lcom/squareup/ui/items/EditItemVariationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditItemVariationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemVariationCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditItemVariationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditItemVariationCoordinator.kt\ncom/squareup/ui/items/EditItemVariationCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,261:1\n1103#2,7:262\n*E\n*S KotlinDebug\n*F\n+ 1 EditItemVariationCoordinator.kt\ncom/squareup/ui/items/EditItemVariationCoordinator\n*L\n229#1,7:262\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 82\u00020\u0001:\u00018BO\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0016J\u0018\u0010,\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010-\u001a\u00020.H\u0002J\u0018\u0010/\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u00100\u001a\u000201H\u0002J\u0008\u00102\u001a\u00020.H\u0002J \u00103\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u00104\u001a\u00020.2\u0006\u00105\u001a\u000206H\u0002J\u000c\u00107\u001a\u00020)*\u00020+H\u0002R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditItemVariationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/items/EditItemScopeRunner;",
        "editInventoryStateController",
        "Lcom/squareup/ui/items/EditInventoryStateController;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "duplicateSkuValidator",
        "Lcom/squareup/ui/items/DuplicateSkuValidator;",
        "errorsBarPresenter",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "res",
        "Lcom/squareup/util/Res;",
        "editItemVariationRunner",
        "Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;Lcom/squareup/tutorialv2/TutorialCore;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "errorsBarView",
        "Lcom/squareup/ui/ErrorsBarView;",
        "nameEditText",
        "Landroid/widget/EditText;",
        "nameReadOnlyField",
        "Lcom/squareup/noho/NohoRow;",
        "priceEditText",
        "Lcom/squareup/noho/NohoEditText;",
        "priceHelpText",
        "Lcom/squareup/widgets/MessageView;",
        "removeButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "skuEditText",
        "stockCountRow",
        "Lcom/squareup/ui/items/widgets/StockCountRow;",
        "unitTypeSelector",
        "unitTypeSelectorHelpText",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "configureActionBar",
        "isNewVariation",
        "",
        "configureInputFields",
        "screenData",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;",
        "shouldEnabledPrimaryButton",
        "updateHelpText",
        "hasInclusiveTaxesApplied",
        "unitAbbreviation",
        "",
        "bindViews",
        "Companion",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/EditItemVariationCoordinator$Companion;

.field private static final SKU_SEARCH_DELAY_MS:J = 0xc8L


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

.field private final editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

.field private final editItemVariationRunner:Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private errorsBarView:Lcom/squareup/ui/ErrorsBarView;

.field private final moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private nameEditText:Landroid/widget/EditText;

.field private nameReadOnlyField:Lcom/squareup/noho/NohoRow;

.field private priceEditText:Lcom/squareup/noho/NohoEditText;

.field private priceHelpText:Lcom/squareup/widgets/MessageView;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private removeButton:Lcom/squareup/marketfont/MarketButton;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

.field private skuEditText:Landroid/widget/EditText;

.field private stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private unitTypeSelector:Lcom/squareup/noho/NohoRow;

.field private unitTypeSelectorHelpText:Lcom/squareup/widgets/MessageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/EditItemVariationCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->Companion:Lcom/squareup/ui/items/EditItemVariationCoordinator$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInventoryStateController"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "duplicateSkuValidator"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBarPresenter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editItemVariationRunner"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p4, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p5, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

    iput-object p6, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iput-object p7, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p8, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->editItemVariationRunner:Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    iput-object p9, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method public static final synthetic access$configureActionBar(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;Z)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->configureActionBar(Landroid/view/View;Z)V

    return-void
.end method

.method public static final synthetic access$configureInputFields(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;)V

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getDuplicateSkuValidator$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/ui/items/DuplicateSkuValidator;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

    return-object p0
.end method

.method public static final synthetic access$getEditItemVariationRunner$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->editItemVariationRunner:Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    return-object p0
.end method

.method public static final synthetic access$getNameEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Landroid/widget/EditText;
    .locals 1

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "nameEditText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSkuEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Landroid/widget/EditText;
    .locals 1

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->skuEditText:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "skuEditText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setNameEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/widget/EditText;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic access$setSkuEditText$p(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/widget/EditText;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->skuEditText:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic access$shouldEnabledPrimaryButton(Lcom/squareup/ui/items/EditItemVariationCoordinator;)Z
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->shouldEnabledPrimaryButton()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$updateHelpText(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;ZLjava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->updateHelpText(Landroid/view/View;ZLjava/lang/String;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 108
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 109
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_errors_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->errorsBarView:Lcom/squareup/ui/ErrorsBarView;

    .line 110
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_name_input_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    .line 111
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_name_readonly_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameReadOnlyField:Lcom/squareup/noho/NohoRow;

    .line 112
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_price_input_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    .line 113
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_sku_input_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->skuEditText:Landroid/widget/EditText;

    .line 114
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_stock_count_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/widgets/StockCountRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    .line 115
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_help_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceHelpText:Lcom/squareup/widgets/MessageView;

    .line 116
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_remove_variation_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->removeButton:Lcom/squareup/marketfont/MarketButton;

    .line 117
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_unit_type:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    .line 118
    sget v0, Lcom/squareup/edititem/R$id;->edit_item_variation_unit_type_help_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->unitTypeSelectorHelpText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final configureActionBar(Landroid/view/View;Z)V
    .locals 5

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 127
    new-instance v2, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;ZLandroid/view/View;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 129
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz p2, :cond_1

    .line 130
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/edititem/R$string;->add_variation:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/edititem/R$string;->edit_variation:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 130
    :goto_0
    check-cast v3, Ljava/lang/CharSequence;

    .line 128
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 133
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/common/strings/R$string;->done:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 134
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->shouldEnabledPrimaryButton()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 135
    new-instance v2, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;ZLandroid/view/View;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 137
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;)V
    .locals 11

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    const-string v1, "nameEditText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$1;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameReadOnlyField:Lcom/squareup/noho/NohoRow;

    const-string v2, "nameReadOnlyField"

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 157
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getShouldUseReadOnlyNameField()Z

    move-result v0

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz v0, :cond_5

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameReadOnlyField:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_0

    .line 161
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameReadOnlyField:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0, v4}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 166
    :goto_0
    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    .line 170
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v5, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    const-string v6, "priceEditText"

    if-nez v5, :cond_8

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v5, Lcom/squareup/text/HasSelectableText;

    sget-object v7, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v2, v5, v7, v1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)Lcom/squareup/text/ScrubbingTextWatcher;

    .line 172
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v5, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v5, :cond_9

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v2, v5, v1}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 173
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v2, :cond_a

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    new-instance v5, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;

    invoke-direct {v5, p0, p1, p2, v1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$2;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;Ljava/lang/String;)V

    check-cast v5, Landroid/text/TextWatcher;

    invoke-virtual {v2, v5}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 179
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v2, :cond_b

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    iget-object v5, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 182
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getVariationId()Ljava/lang/String;

    move-result-object v6

    .line 183
    new-instance v2, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;

    invoke-direct {v2, p0, v6}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$skuValidationRunnable$1;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Ljava/lang/String;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 188
    iget-object v5, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->skuEditText:Landroid/widget/EditText;

    const-string v7, "skuEditText"

    if-nez v5, :cond_c

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    new-instance v8, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;

    invoke-direct {v8, p0, p1, v2}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$3;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    check-cast v8, Landroid/text/TextWatcher;

    invoke-virtual {v5, v8}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 197
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->skuEditText:Landroid/widget/EditText;

    if-nez v2, :cond_d

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getSku()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

    invoke-virtual {v2}, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSku()Lrx/Observable;

    move-result-object v2

    .line 201
    new-instance v5, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;

    invoke-direct {v5, p0, v6, p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$4;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Ljava/lang/String;Landroid/view/View;)V

    check-cast v5, Lrx/functions/Action1;

    invoke-virtual {v2, v5}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v2

    const-string v5, "duplicateSkuValidator.va\u2026  )\n          }\n        }"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    invoke-static {v2, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 213
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getVariationMerchantCatalogToken()Ljava/lang/String;

    move-result-object v7

    .line 214
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result v3

    move v9, v3

    goto :goto_1

    :cond_e
    const/4 v9, 0x0

    .line 215
    :goto_1
    iget-object v5, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    .line 216
    iget-object v8, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->stockCountRow:Lcom/squareup/ui/items/widgets/StockCountRow;

    if-nez v8, :cond_f

    const-string v2, "stockCountRow"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    move-object v10, v1

    .line 215
    invoke-virtual/range {v5 .. v10}, Lcom/squareup/ui/items/EditItemScopeRunner;->createStockCountRowAction(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/items/widgets/StockCountRow;ILjava/lang/String;)Lcom/squareup/ui/items/StockCountRowAction;

    move-result-object v2

    .line 218
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState()Lrx/Observable;

    move-result-object v3

    .line 219
    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v3, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v2

    const-string v3, "editInventoryStateContro\u2026ribe(stockCountRowAction)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    invoke-static {v2, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 223
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getHasInclusiveTaxesApplied()Z

    move-result v2

    invoke-direct {p0, p1, v2, v1}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->updateHelpText(Landroid/view/View;ZLjava/lang/String;)V

    .line 226
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;->getShouldHideDeleteButton()Z

    move-result p2

    const-string v1, "removeButton"

    if-eqz p2, :cond_11

    .line 227
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->removeButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p2, :cond_10

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p2, v4}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    goto :goto_2

    .line 229
    :cond_11
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->removeButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p2, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    check-cast p2, Landroid/view/View;

    .line 262
    new-instance v1, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    :goto_2
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    const-string/jumbo v1, "unitTypeSelector"

    if-nez p2, :cond_13

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 235
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/edititem/R$string;->edit_item_unit_type_default:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_3

    .line 237
    :cond_14
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->edit_item_unit_type_value:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 238
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const-string/jumbo v3, "unit_name"

    invoke-virtual {p1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 239
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecisionHint(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v2, "precision"

    invoke-virtual {p1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 240
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 234
    :goto_3
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 241
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_15

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 242
    :cond_15
    new-instance p2, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$6;

    invoke-direct {p2, p0}, Lcom/squareup/ui/items/EditItemVariationCoordinator$configureInputFields$6;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    check-cast p2, Landroid/view/View$OnClickListener;

    .line 241
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final shouldEnabledPrimaryButton()Z
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    if-nez v0, :cond_0

    const-string v1, "nameEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final updateHelpText(Landroid/view/View;ZLjava/lang/String;)V
    .locals 3

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->priceHelpText:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "priceHelpText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->editItemVariationRunner:Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    invoke-interface {v1}, Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;->getIncludedTax()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 256
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 252
    invoke-static {p2, v1, p1, v2, p3}, Lcom/squareup/ui/items/PriceHelpTextHelper;->buildPriceHelpTextForVariation(ZLcom/squareup/protos/common/Money;Landroid/content/res/Resources;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator;->bindViews(Landroid/view/View;)V

    .line 75
    new-instance v0, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$1;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->editItemVariationRunner:Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$1;-><init>(Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->sku_error_message_batched:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->setBatchedErrorMessage(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->unitTypeSelectorHelpText:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string/jumbo v1, "unitTypeSelectorHelpText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 82
    :cond_0
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    sget v2, Lcom/squareup/edititem/R$string;->edit_item_unit_type_selector_help_text:I

    const-string v3, "learn_more"

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 84
    sget v2, Lcom/squareup/edititem/R$string;->unit_type_hint_url:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 85
    sget v2, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 86
    sget v2, Lcom/squareup/noho/R$color;->noho_text_help_link:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->editItemVariationRunner:Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;->editItemVariationScreenData()Lrx/Observable;

    move-result-object v0

    .line 91
    new-instance v1, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$2;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;Landroid/view/View;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "editItemVariationRunner.\u2026isNewVariation)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->editItemVariationRunner:Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;

    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;->scannedBarcode()Lrx/Observable;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditItemVariationCoordinator$attach$3;-><init>(Lcom/squareup/ui/items/EditItemVariationCoordinator;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "editItemVariationRunner.\u2026ext.setText(it)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemVariationCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Item Variation Screen Shown"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method
