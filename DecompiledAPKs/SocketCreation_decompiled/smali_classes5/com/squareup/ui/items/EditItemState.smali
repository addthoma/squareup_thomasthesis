.class public Lcom/squareup/ui/items/EditItemState;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;,
        Lcom/squareup/ui/items/EditItemState$ModifierStates;,
        Lcom/squareup/ui/items/EditItemState$ModifierState;,
        Lcom/squareup/ui/items/EditItemState$TaxStates;,
        Lcom/squareup/ui/items/EditItemState$TaxState;,
        Lcom/squareup/ui/items/EditItemState$ItemData;,
        Lcom/squareup/ui/items/EditItemState$ReadItemOptionDataCogsTask;,
        Lcom/squareup/ui/items/EditItemState$ReadItemDataCogsTask;,
        Lcom/squareup/ui/items/EditItemState$ItemImageState;
    }
.end annotation


# static fields
.field private static final BITMAP_PATH_KEY:Ljava/lang/String; = "ITEM_BITMAP_FILE"

.field private static final BITMAP_STATE_KEY:Ljava/lang/String; = "ITEM_BITMAP_STATE"

.field private static final CATALOG_VERSION_KEY:Ljava/lang/String; = "CATALOG_VERSION_KEY"

.field private static final EXISTING_VARIATION_KEY:Ljava/lang/String; = "EXISTING_VARIATION_KEY"

.field private static final HAS_LOCALLY_DISABLED_VARIATION_KEY:Ljava/lang/String; = "HAS_LOCALLY_DISABLED_VARIATION_KEY"

.field private static final IMAGE_FILE_KEY:Ljava/lang/String; = "IMAGE_FILE"

.field private static final ITEM_DATA_KEY:Ljava/lang/String; = "ITEM_DATA_KEY"

.field private static final ITEM_DATA_ORIGINAL_KEY:Ljava/lang/String; = "ITEM_DATA_ORIGINAL_KEY"

.field private static final ITEM_OPTION_DATA_KEY:Ljava/lang/String; = "ITEM_OPTION_DATA_KEY"

.field private static final ITEM_OPTION_DATA_ORIGINAL_KEY:Ljava/lang/String; = "ITEM_OPTION_DATA_ORIGINAL_KEY"

.field private static final ITEM_PHOTO_IMAGE_URL:Ljava/lang/String; = "ITEM_PHOTO_IMAGE_URL"

.field private static final ITEM_PHOTO_OBJECT_ID:Ljava/lang/String; = "ITEM_PHOTO_OBJECT_ID"

.field private static final MODIFIER_STATES_KEY:Ljava/lang/String; = "MODIFIER_STATES_KEY"

.field private static final NEW_ITEM_KEY:Ljava/lang/String; = "NEW_ITEM"

.field private static final SHOULD_SHOW_DEFAULT_UNITS_KEY:Ljava/lang/String; = "SHOULD_SHOW_DEFAULT_UNITS"

.field private static final TAX_STATES_KEY:Ljava/lang/String; = "TAX_STATES_KEY"


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cacheDir:Ljava/io/File;

.field catalogVersion:J

.field existingVariationIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private hasLocallyDisabledVariation:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

.field private final imageUploader:Lcom/squareup/ui/items/ImageUploader;

.field isNewItem:Z

.field private itemBitmapFile:Ljava/io/File;

.field private itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

.field private itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

.field itemId:Ljava/lang/String;

.field itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

.field private final itemImageStateBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/items/EditItemState$ItemImageState;",
            ">;"
        }
    .end annotation
.end field

.field private itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

.field itemOptionDataOriginal:Lcom/squareup/ui/items/ItemOptionData;

.field itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private modifiedItemVariations:Lcom/squareup/catalog/EditItemVariationsState;

.field modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

.field newImageToEdit:Landroid/net/Uri;

.field pendingDeletions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private shouldShowDefaultUnits:Z

.field taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/ImageUploader;Ljava/io/File;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/util/concurrent/Executor;)V
    .locals 1

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->pendingDeletions:Ljava/util/Set;

    .line 131
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->imageUploader:Lcom/squareup/ui/items/ImageUploader;

    .line 132
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState;->cacheDir:Ljava/io/File;

    .line 133
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    .line 134
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemState;->badBus:Lcom/squareup/badbus/BadBus;

    .line 135
    iput-object p6, p0, Lcom/squareup/ui/items/EditItemState;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    .line 136
    const-class p1, Lcom/squareup/ui/items/EditItemState$ItemImageState;

    const-string p2, "ITEM_BITMAP_STATE"

    invoke-static {p2, p1}, Lcom/squareup/BundleKey;->forEnum(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemImageStateBundleKey:Lcom/squareup/BundleKey;

    .line 137
    new-instance p1, Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-direct {p1}, Lcom/squareup/ui/items/EditItemState$TaxStates;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    .line 138
    new-instance p1, Lcom/squareup/ui/items/EditItemState$ModifierStates;

    invoke-direct {p1}, Lcom/squareup/ui/items/EditItemState$ModifierStates;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    .line 139
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 140
    sget-object p1, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->UNKNOWN:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->hasLocallyDisabledVariation:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    return-void
.end method

.method public static synthetic lambda$y7IEtzN-gbzNeBc8n72gSb3zZCk(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemState;->onImageUrlUpdated(Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;)V

    return-void
.end method

.method private onImageUrlUpdated(Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;)V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/ui/photo/ItemPhoto;->getItemId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    invoke-virtual {v0}, Lcom/squareup/ui/photo/ItemPhoto;->getItemId()Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-virtual {p1}, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;->getItemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    invoke-virtual {p1}, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;->getUri()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/photo/ItemPhoto;->withNewUri(Landroid/net/Uri;)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    :cond_0
    return-void
.end method

.method private static replaceItemOptionIdsInItemData(Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemState$ItemData;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAllItemOptionIds()Ljava/util/List;

    move-result-object v0

    .line 289
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->removeAllOptions()Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 290
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 292
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 293
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 297
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    new-instance v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    invoke-direct {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;-><init>()V

    invoke-virtual {v3, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->addOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    goto :goto_0

    .line 300
    :cond_1
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-static {p0, p1}, Lcom/squareup/ui/items/EditItemState;->replaceItemOptionIdsInVariationData(Ljava/util/List;Ljava/util/Map;)V

    return-void
.end method

.method private static replaceItemOptionIdsInVariationData(Ljava/util/List;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 307
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 309
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object v1

    .line 310
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->removeAllOptionValues()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 311
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 312
    iget-object v3, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    .line 313
    iget-object v4, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 314
    iget-object v3, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 317
    :cond_1
    iget-object v4, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    .line 318
    iget-object v5, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-interface {p1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 319
    iget-object v2, v2, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Ljava/lang/String;

    .line 324
    :cond_2
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    invoke-direct {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;-><init>()V

    .line 325
    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->setItemOptionID(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v2

    .line 326
    invoke-virtual {v2, v4}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;

    move-result-object v2

    .line 327
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    move-result-object v2

    .line 324
    invoke-virtual {v0, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method addServerIdAndVersionToItemAndVariations(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
            ">;)V"
        }
    .end annotation

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    .line 257
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v0, :cond_0

    .line 259
    iget-object v1, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    .line 260
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v2, v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    move-result-object v2

    .line 261
    invoke-virtual {v2, v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setMerchantCatalogObjectReference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 262
    iput-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    .line 263
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    iput-object v1, v2, Lcom/squareup/ui/items/EditItemState$TaxStates;->itemId:Ljava/lang/String;

    .line 264
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    iput-object v1, v2, Lcom/squareup/ui/items/EditItemState$ModifierStates;->itemId:Ljava/lang/String;

    .line 267
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 269
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v3, :cond_2

    .line 271
    iget-object v4, v3, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v4

    .line 272
    invoke-virtual {v4, v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setMerchantCatalogObjectReference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_2
    if-eqz v0, :cond_1

    .line 275
    iget-object v3, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    goto :goto_0

    :cond_3
    return-void
.end method

.method public assignedItemOptionsHaveChanged()Z
    .locals 2

    .line 470
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    if-eqz v0, :cond_0

    .line 471
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getItemOptionSetIds()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 473
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 476
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getItemOptionSetIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method buildModifiedVariations()Lcom/squareup/catalog/EditItemVariationsState;
    .locals 13

    .line 487
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 488
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 489
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 492
    :cond_0
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 494
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 495
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v5

    invoke-interface {v1, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/2addr v4, v6

    goto :goto_1

    .line 499
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 500
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 501
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 506
    iget-object v4, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v4, v4, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v10, 0x0

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 507
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    if-nez v9, :cond_3

    .line 509
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    goto :goto_2

    .line 511
    :cond_3
    invoke-virtual {v9}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v11

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 512
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v11

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 513
    invoke-virtual {v9}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v9

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-static {v9, v5}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v3, 0x1

    const/4 v10, 0x1

    goto :goto_2

    .line 521
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move v9, v3

    :cond_5
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 522
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    if-nez v4, :cond_5

    .line 524
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v3

    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x1

    goto :goto_3

    .line 529
    :cond_6
    new-instance v0, Lcom/squareup/catalog/EditItemVariationsState;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 530
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->canEditItemWithItemOptions()Z

    move-result v11

    move-object v5, v0

    move-object v6, v2

    invoke-direct/range {v5 .. v11}, Lcom/squareup/catalog/EditItemVariationsState;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;ZZZ)V

    return-object v0
.end method

.method calculateIncludedTax(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 6

    .line 584
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 586
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 587
    iget-object v4, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/ui/items/EditItemState$TaxStates;->isTaxAppliedToItem(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 588
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 589
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTax;->isInclusive()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    const/4 p1, 0x0

    return-object p1

    .line 600
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/checkout/Tax;->mapCatalogTaxes(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    .line 601
    invoke-static {v0}, Lcom/squareup/checkout/Tax;->mapCatalogTaxes(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    .line 603
    new-instance v2, Lcom/squareup/payment/Order$Builder;

    invoke-direct {v2}, Lcom/squareup/payment/Order$Builder;-><init>()V

    iget-object v3, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 604
    invoke-virtual {v2, v3}, Lcom/squareup/payment/Order$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/payment/Order$Builder;

    move-result-object v2

    .line 605
    invoke-virtual {v2, v1}, Lcom/squareup/payment/Order$Builder;->availableTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 606
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->getRoundingType()Lcom/squareup/calc/constants/RoundingType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order$Builder;->roundingType(Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 607
    invoke-virtual {v1}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v1

    .line 609
    new-instance v2, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v2}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 610
    invoke-virtual {v2, p1}, Lcom/squareup/checkout/CartItem$Builder;->variablePrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 611
    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 612
    invoke-virtual {v1, p1}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 614
    invoke-virtual {v1, p1}, Lcom/squareup/payment/Order;->getInclusiveTaxesForItem(Lcom/squareup/checkout/CartItem;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method createNewEmptyVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 2

    .line 428
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method deleteBitmap()V
    .locals 2

    .line 568
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->clearOverride(Ljava/lang/String;)V

    .line 569
    sget-object v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DELETED:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    const/4 v0, 0x0

    .line 570
    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemBitmapFile:Ljava/io/File;

    return-void
.end method

.method findChangedItemOptionsAndValues()Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;
    .locals 2

    .line 641
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionDataOriginal:Lcom/squareup/ui/items/ItemOptionData;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

    invoke-static {v0, v1}, Lcom/squareup/ui/items/ItemOptionData;->findChangedItemOptions(Lcom/squareup/ui/items/ItemOptionData;Lcom/squareup/ui/items/ItemOptionData;)Lcom/squareup/ui/items/ItemOptionData$ChangedItemOptions;

    move-result-object v0

    return-object v0
.end method

.method getDeletedVariationCatalogTokens()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 535
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 536
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 537
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 540
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 541
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 542
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    if-nez v4, :cond_1

    .line 544
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 546
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;
    .locals 1

    .line 629
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    return-object v0
.end method

.method getItemOptionData()Lcom/squareup/ui/items/ItemOptionData;
    .locals 1

    .line 633
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

    return-object v0
.end method

.method getModifiedItemVariations()Lcom/squareup/catalog/EditItemVariationsState;
    .locals 1

    .line 480
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->modifiedItemVariations:Lcom/squareup/catalog/EditItemVariationsState;

    if-nez v0, :cond_0

    .line 481
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemState;->buildModifiedVariations()Lcom/squareup/catalog/EditItemVariationsState;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->modifiedItemVariations:Lcom/squareup/catalog/EditItemVariationsState;

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->modifiedItemVariations:Lcom/squareup/catalog/EditItemVariationsState;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 333
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getOriginalVariationIds()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 662
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 663
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 664
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method hasInclusiveTaxesApplied()Z
    .locals 4

    .line 618
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->allEnabledTaxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogTax;

    .line 619
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/items/EditItemState$TaxStates;->isTaxAppliedToItem(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 620
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax;->isInclusive()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method hasItemChanged()Z
    .locals 4

    .line 432
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    const/4 v1, 0x0

    if-eqz v0, :cond_9

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    if-nez v2, :cond_0

    goto :goto_1

    .line 433
    :cond_0
    invoke-virtual {v0, v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    return v2

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionDataOriginal:Lcom/squareup/ui/items/ItemOptionData;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/items/ItemOptionData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    return v2

    .line 435
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$TaxStates;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_3

    return v2

    .line 436
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_4

    return v2

    .line 437
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    sget-object v3, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DELETED:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    if-eq v0, v3, :cond_8

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    sget-object v3, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DIRTY:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    if-ne v0, v3, :cond_5

    goto :goto_0

    .line 438
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->pendingDeletions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_6

    return v2

    .line 439
    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->newImageToEdit:Landroid/net/Uri;

    if-eqz v0, :cond_7

    return v2

    :cond_7
    return v1

    :cond_8
    :goto_0
    return v2

    :cond_9
    :goto_1
    return v1
.end method

.method hasItemEverUsedItemOptions()Z
    .locals 1

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->hasItemOptions()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    if-eqz v0, :cond_0

    .line 465
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->hasItemOptions()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method hasLoallyDisabledVariation()Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->hasLocallyDisabledVariation:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    return-object v0
.end method

.method isItemAssignedWithIemOptions()Z
    .locals 1

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->hasItemOptions()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$saveToCatalogStoreAndSync$0$EditItemState(Lcom/squareup/cogs/WriteBuilder;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 2

    .line 241
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    if-nez v0, :cond_0

    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 244
    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/cogs/WriteBuilder;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$saveToCatalogStoreAndSync$1$EditItemState(Ljava/util/List;Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 247
    invoke-interface {p4}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    .line 249
    iget-object p4, p0, Lcom/squareup/ui/items/EditItemState;->pendingDeletions:Ljava/util/Set;

    invoke-interface {p4, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    const/4 p1, 0x0

    .line 250
    invoke-interface {p2, p3, p1}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method

.method public loadDataFromLibrary(ZLcom/squareup/ui/items/EditItemState$ItemData;Lcom/squareup/ui/items/ItemOptionData;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;J)V
    .locals 0

    .line 153
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

    .line 155
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionDataOriginal:Lcom/squareup/ui/items/ItemOptionData;

    if-nez p1, :cond_0

    .line 159
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    if-eqz p3, :cond_0

    return-void

    .line 162
    :cond_0
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    .line 163
    iget-object p3, p2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    .line 164
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditItemState$ItemData;->cloneItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    .line 165
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemState;->pendingDeletions:Ljava/util/Set;

    invoke-interface {p3}, Ljava/util/Set;->clear()V

    .line 166
    sget-object p3, Lcom/squareup/ui/items/EditItemState$ItemImageState;->CLEAN:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    .line 167
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    .line 168
    iput-object p6, p0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    .line 169
    iget-boolean p3, p0, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    if-nez p3, :cond_1

    .line 170
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object p5, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    invoke-virtual {p3, p5, p4}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p3

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 172
    :cond_1
    iput-wide p7, p0, Lcom/squareup/ui/items/EditItemState;->catalogVersion:J

    if-nez p1, :cond_2

    .line 174
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemState;->getOriginalVariationIds()Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->existingVariationIds:Ljava/util/Set;

    .line 175
    iget-object p1, p2, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/items/EditItemState;->shouldShowDefaultUnits:Z

    :cond_2
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 373
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;

    .line 374
    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemState$y7IEtzN-gbzNeBc8n72gSb3zZCk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemState$y7IEtzN-gbzNeBc8n72gSb3zZCk;-><init>(Lcom/squareup/ui/items/EditItemState;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 373
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 4

    if-eqz p1, :cond_3

    .line 381
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemState;->hasItemChanged()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ITEM_DATA_KEY"

    .line 382
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    :cond_0
    const-string v1, "ITEM_PHOTO_OBJECT_ID"

    .line 385
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 386
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "ITEM_PHOTO_IMAGE_URL"

    .line 387
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 388
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual {v3, v1, v2}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 391
    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemState$ItemData;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    const-string v0, "ITEM_DATA_ORIGINAL_KEY"

    .line 392
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemState$ItemData;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    const-string v0, "IMAGE_FILE"

    .line 395
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->newImageToEdit:Landroid/net/Uri;

    const-string v0, "NEW_ITEM"

    .line 397
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    .line 399
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemImageStateBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    const-string v0, "ITEM_BITMAP_FILE"

    .line 401
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 402
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemBitmapFile:Ljava/io/File;

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemBitmapFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Z)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    :cond_2
    const-string v0, "SHOULD_SHOW_DEFAULT_UNITS"

    .line 406
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/items/EditItemState;->shouldShowDefaultUnits:Z

    const-string v0, "HAS_LOCALLY_DISABLED_VARIATION_KEY"

    .line 408
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 409
    invoke-static {}, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->values()[Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    move-result-object v1

    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->hasLocallyDisabledVariation:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    const-string v0, "TAX_STATES_KEY"

    .line 411
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemState$TaxStates;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    const-string v0, "MODIFIER_STATES_KEY"

    .line 413
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemState$ModifierStates;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    const-string v0, "CATALOG_VERSION_KEY"

    .line 415
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/items/EditItemState;->catalogVersion:J

    .line 417
    new-instance v0, Ljava/util/LinkedHashSet;

    const-string v1, "EXISTING_VARIATION_KEY"

    .line 418
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->existingVariationIds:Ljava/util/Set;

    const-string v0, "ITEM_OPTION_DATA_KEY"

    .line 420
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/ItemOptionData;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

    const-string v0, "ITEM_OPTION_DATA_ORIGINAL_KEY"

    .line 421
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/ItemOptionData;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionDataOriginal:Lcom/squareup/ui/items/ItemOptionData;

    :cond_3
    :goto_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    if-eqz v0, :cond_1

    .line 342
    invoke-virtual {v0}, Lcom/squareup/ui/photo/ItemPhoto;->getItemId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ITEM_PHOTO_OBJECT_ID"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    invoke-virtual {v0}, Lcom/squareup/ui/photo/ItemPhoto;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ITEM_PHOTO_IMAGE_URL"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    const-string v1, "ITEM_DATA_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemDataOriginal:Lcom/squareup/ui/items/EditItemState$ItemData;

    const-string v1, "ITEM_DATA_ORIGINAL_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->newImageToEdit:Landroid/net/Uri;

    const-string v1, "IMAGE_FILE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 352
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    const-string v1, "NEW_ITEM"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemImageStateBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 357
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemBitmapFile:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 358
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ITEM_BITMAP_FILE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditItemState;->shouldShowDefaultUnits:Z

    const-string v1, "SHOULD_SHOW_DEFAULT_UNITS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 362
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->hasLocallyDisabledVariation:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->ordinal()I

    move-result v0

    const-string v1, "HAS_LOCALLY_DISABLED_VARIATION_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    const-string v1, "TAX_STATES_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    const-string v1, "MODIFIER_STATES_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 365
    iget-wide v0, p0, Lcom/squareup/ui/items/EditItemState;->catalogVersion:J

    const-string v2, "CATALOG_VERSION_KEY"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 366
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->existingVariationIds:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v1, "EXISTING_VARIATION_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

    const-string v1, "ITEM_OPTION_DATA_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionDataOriginal:Lcom/squareup/ui/items/ItemOptionData;

    const-string v1, "ITEM_OPTION_DATA_ORIGINAL_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public replaceItemOptionClientIdsWithServerIds(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    invoke-static {v0, p1}, Lcom/squareup/ui/items/EditItemState;->replaceItemOptionIdsInItemData(Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/util/Map;)V

    return-void
.end method

.method resetBitmap()V
    .locals 3

    .line 574
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemBitmapFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 575
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, Lcom/squareup/util/Files;->deleteSilently(Ljava/io/File;Ljava/util/concurrent/Executor;)V

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->clearOverride(Ljava/lang/String;)V

    .line 578
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Z)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 579
    sget-object v0, Lcom/squareup/ui/items/EditItemState$ItemImageState;->CLEAN:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    return-void
.end method

.method saveToCatalogStoreAndSync(Lcom/squareup/cogs/Cogs;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Z",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 191
    sget-object v0, Lcom/squareup/ui/items/EditItemState$1;->$SwitchMap$com$squareup$ui$items$EditItemState$ItemImageState:[I

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState$ItemImageState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 202
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unhandled image state "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->clearImageId()Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    goto :goto_0

    .line 193
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->imageUploader:Lcom/squareup/ui/items/ImageUploader;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->itemBitmapFile:Ljava/io/File;

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/items/ImageUploader;->uploadImage(Ljava/lang/String;Ljava/io/File;)V

    .line 205
    :goto_0
    invoke-static {}, Lcom/squareup/cogs/CogsTasks;->write()Lcom/squareup/cogs/WriteBuilder;

    move-result-object v0

    if-eqz p2, :cond_4

    const/4 p2, 0x0

    .line 216
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_4

    .line 217
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 218
    invoke-virtual {v1, p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 219
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v2

    .line 220
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 221
    invoke-virtual {v0, v2}, Lcom/squareup/cogs/WriteBuilder;->update(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;

    :cond_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 226
    :cond_4
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemState;->itemData:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object p2, p2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object p2

    .line 228
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->pendingDeletions:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 230
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-static {v2}, Lcom/squareup/ui/items/EditItemState$TaxStates;->access$300(Lcom/squareup/ui/items/EditItemState$TaxStates;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/cogs/WriteBuilder;->update(Ljava/util/Collection;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    .line 231
    invoke-static {v3}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->access$200(Lcom/squareup/ui/items/EditItemState$ModifierStates;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cogs/WriteBuilder;->update(Ljava/util/Collection;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    .line 232
    invoke-static {v3}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->access$100(Lcom/squareup/ui/items/EditItemState$ModifierStates;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cogs/WriteBuilder;->update(Ljava/util/Collection;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object v2

    .line 233
    invoke-virtual {v2, p2}, Lcom/squareup/cogs/WriteBuilder;->update(Lcom/squareup/shared/catalog/models/CatalogObject;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object p2

    .line 234
    invoke-virtual {p2, v1}, Lcom/squareup/cogs/WriteBuilder;->delete(Ljava/util/Collection;)Lcom/squareup/cogs/WriteBuilder;

    move-result-object p2

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    .line 235
    invoke-static {v2}, Lcom/squareup/ui/items/EditItemState$TaxStates;->access$000(Lcom/squareup/ui/items/EditItemState$TaxStates;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/squareup/cogs/WriteBuilder;->delete(Ljava/util/Collection;)Lcom/squareup/cogs/WriteBuilder;

    .line 237
    new-instance p2, Lcom/squareup/ui/items/-$$Lambda$EditItemState$dkC4Yi2pEyNBsur43v2W-Ehvo3g;

    invoke-direct {p2, p0, v0}, Lcom/squareup/ui/items/-$$Lambda$EditItemState$dkC4Yi2pEyNBsur43v2W-Ehvo3g;-><init>(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/cogs/WriteBuilder;)V

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;

    invoke-direct {v0, p0, v1, p1, p3}, Lcom/squareup/ui/items/-$$Lambda$EditItemState$8dhkDBiKSymR_K0lwAOGKuPtH4M;-><init>(Lcom/squareup/ui/items/EditItemState;Ljava/util/List;Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-interface {p1, p2, v0}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method setBitmapFile(Ljava/io/File;)V
    .locals 3

    .line 554
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->newImageToEdit:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->newImageToEdit:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 556
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemState;->cacheDir:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 557
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1}, Lcom/squareup/util/Files;->deleteSilently(Ljava/io/File;Ljava/util/concurrent/Executor;)V

    .line 560
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemBitmapFile:Ljava/io/File;

    .line 561
    invoke-virtual {p1}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object p1

    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object p1

    .line 562
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->setOverride(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemState;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Z)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 564
    sget-object p1, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DIRTY:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    return-void
.end method

.method setHasLocallyDisabledVariation(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 455
    sget-object p1, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->YES:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->NO:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->hasLocallyDisabledVariation:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    return-void
.end method

.method setItemOptionData(Lcom/squareup/ui/items/ItemOptionData;)V
    .locals 0

    .line 637
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

    return-void
.end method

.method shouldShowDefaultUnits()Z
    .locals 1

    .line 444
    iget-boolean v0, p0, Lcom/squareup/ui/items/EditItemState;->shouldShowDefaultUnits:Z

    return v0
.end method

.method updateItemOptionDataAfterSave(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)V"
        }
    .end annotation

    .line 650
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionDataOriginal:Lcom/squareup/ui/items/ItemOptionData;

    .line 651
    invoke-virtual {v1}, Lcom/squareup/ui/items/ItemOptionData;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 653
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 655
    new-instance p1, Lcom/squareup/ui/items/ItemOptionData;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/ui/items/ItemOptionData;-><init>(Ljava/util/Collection;)V

    .line 657
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionDataOriginal:Lcom/squareup/ui/items/ItemOptionData;

    .line 658
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemState;->itemOptionData:Lcom/squareup/ui/items/ItemOptionData;

    return-void
.end method
