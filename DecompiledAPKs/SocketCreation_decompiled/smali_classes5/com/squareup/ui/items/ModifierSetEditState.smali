.class public final Lcom/squareup/ui/items/ModifierSetEditState;
.super Ljava/lang/Object;
.source "ModifierSetEditState.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;,
        Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;
    }
.end annotation


# static fields
.field private static final ID_KEY:Ljava/lang/String; = "MODIFIER_SET_ID"

.field private static final ITEM_KEYS:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

.field private static final ITEM_KEYS_ORIGINAL:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;


# instance fields
.field private modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

.field private modifierSetDataOriginal:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

.field private noNeedToLoadAgain:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 29
    new-instance v0, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    const-string v1, "MODIFIER_SET_KEY"

    const-string v2, "ASSIGNED_ITEMS_IDS"

    const-string v3, "MODIFIER_OPTIONS"

    const-string v4, "REMOVED_OPTIONS"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/items/ModifierSetEditState;->ITEM_KEYS:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    .line 31
    new-instance v0, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    const-string v1, "MODIFIER_SET_KEY_ORIGINAL"

    const-string v2, "ASSIGNED_ITEMS_IDS_ORIGINAL"

    const-string v3, "MODIFIER_OPTIONS_ORIGINAL"

    const-string v4, "REMOVED_OPTIONS_ORIGINAL"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/items/ModifierSetEditState;->ITEM_KEYS_ORIGINAL:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static fromByteArray([B)Ljava/io/Serializable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">([B)TT;"
        }
    .end annotation

    .line 164
    :try_start_0
    new-instance v0, Ljava/io/ObjectInputStream;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 166
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/io/Serializable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 168
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static loadModifierSetData(Landroid/os/Bundle;Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;)Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;
    .locals 6

    .line 95
    iget-object v0, p1, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->MODIFIER_SET_KEY:Ljava/lang/String;

    .line 96
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->objectFromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 97
    new-instance v1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemModifierList;)V

    .line 99
    iget-object v0, p1, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->ASSIGNED_ITEMS_KEY:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 100
    array-length v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_0

    aget-object v5, v0, v4

    .line 101
    invoke-virtual {v1, v5}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->addAssignedItemId(Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 103
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->REMOVED_OPTIONS_KEY:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 104
    array-length v2, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_1

    aget-object v5, v0, v4

    .line 105
    invoke-virtual {v1, v5}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->addRemovedOptionId(Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 107
    :cond_1
    iget-object p1, p1, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->MODIFIER_OPTIONS_KEY:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/items/ModifierSetEditState;->fromByteArray([B)Ljava/io/Serializable;

    move-result-object p0

    check-cast p0, [[B

    .line 108
    invoke-virtual {v1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierOptions()Ljava/util/List;

    move-result-object p1

    .line 109
    array-length v0, p0

    :goto_2
    if-ge v3, v0, :cond_2

    aget-object v2, p0, v3

    .line 110
    invoke-static {v2}, Lcom/squareup/ui/items/ModifierSetEditState;->fromByteArray([B)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {v2}, Lcom/squareup/api/items/ItemModifierOption;->newBuilder()Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    return-object v1
.end method

.method private static saveModiferSetData(Landroid/os/Bundle;Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;)V
    .locals 6

    .line 125
    iget-object v0, p2, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->MODIFIER_SET_KEY:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierSet()Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 126
    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getAssignedItemIds()Ljava/util/Set;

    move-result-object v0

    .line 127
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 129
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    add-int/lit8 v5, v3, 0x1

    .line 130
    aput-object v4, v1, v3

    move v3, v5

    goto :goto_0

    .line 132
    :cond_0
    iget-object v0, p2, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->ASSIGNED_ITEMS_KEY:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 134
    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getRemovedOptionIds()Ljava/util/Set;

    move-result-object v0

    .line 135
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    .line 137
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    add-int/lit8 v5, v3, 0x1

    .line 138
    aput-object v4, v1, v3

    move v3, v5

    goto :goto_1

    .line 140
    :cond_1
    iget-object v0, p2, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->REMOVED_OPTIONS_KEY:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 142
    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierOptions()Ljava/util/List;

    move-result-object p1

    .line 143
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [[B

    .line 144
    :goto_2
    array-length v1, v0

    if-ge v2, v1, :cond_2

    .line 145
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/items/ModifierSetEditState;->toByteArray(Ljava/io/Serializable;)[B

    move-result-object v1

    aput-object v1, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 147
    :cond_2
    iget-object p1, p2, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->MODIFIER_OPTIONS_KEY:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->toByteArray(Ljava/io/Serializable;)[B

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method private static toByteArray(Ljava/io/Serializable;)[B
    .locals 2

    .line 152
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 153
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 154
    invoke-virtual {v1, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 155
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 158
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    return-object v0
.end method

.method public getModifierSetDataOriginal()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetDataOriginal:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 75
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method hasModifierSetDataChanged()Z
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetDataOriginal:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    if-nez v1, :cond_0

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public loaded()Z
    .locals 1

    .line 79
    iget-boolean v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->noNeedToLoadAgain:Z

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 83
    iget-boolean v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->noNeedToLoadAgain:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 86
    iput-boolean v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->noNeedToLoadAgain:Z

    .line 88
    sget-object v0, Lcom/squareup/ui/items/ModifierSetEditState;->ITEM_KEYS:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    iget-object v0, v0, Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;->MODIFIER_SET_KEY:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    sget-object v0, Lcom/squareup/ui/items/ModifierSetEditState;->ITEM_KEYS:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/ModifierSetEditState;->loadModifierSetData(Landroid/os/Bundle;Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;)Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    .line 90
    sget-object v0, Lcom/squareup/ui/items/ModifierSetEditState;->ITEM_KEYS_ORIGINAL:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/ModifierSetEditState;->loadModifierSetData(Landroid/os/Bundle;Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;)Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetDataOriginal:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    :cond_1
    :goto_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x1

    .line 117
    iput-boolean v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->noNeedToLoadAgain:Z

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    if-eqz v0, :cond_0

    .line 119
    sget-object v1, Lcom/squareup/ui/items/ModifierSetEditState;->ITEM_KEYS:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/items/ModifierSetEditState;->saveModiferSetData(Landroid/os/Bundle;Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetDataOriginal:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    sget-object v1, Lcom/squareup/ui/items/ModifierSetEditState;->ITEM_KEYS_ORIGINAL:Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/items/ModifierSetEditState;->saveModiferSetData(Landroid/os/Bundle;Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;Lcom/squareup/ui/items/ModifierSetEditState$ItemKeys;)V

    :cond_0
    return-void
.end method

.method public saveCopyOfItemData()V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-static {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->access$000(Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;)Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/api/items/ItemModifierList$Builder;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->setName(Ljava/lang/String;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->cloneModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetDataOriginal:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    return-void
.end method

.method public setModifierSetData(Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/ModifierSetEditState;->modifierSetData:Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->setName(Ljava/lang/String;)V

    return-void
.end method
