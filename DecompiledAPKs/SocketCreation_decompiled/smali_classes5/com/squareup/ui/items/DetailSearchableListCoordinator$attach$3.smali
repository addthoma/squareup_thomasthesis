.class public final Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$3;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "DetailSearchableListCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0007H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/ui/items/DetailSearchableListCoordinator$attach$3",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "onTextChanged",
        "",
        "text",
        "",
        "start",
        "",
        "before",
        "count",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 136
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    const-string/jumbo p2, "text"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    invoke-static {p2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->access$getSearchTextRelay$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
