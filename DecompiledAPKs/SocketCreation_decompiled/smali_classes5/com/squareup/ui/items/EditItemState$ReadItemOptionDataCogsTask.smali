.class Lcom/squareup/ui/items/EditItemState$ReadItemOptionDataCogsTask;
.super Ljava/lang/Object;
.source "EditItemState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ReadItemOptionDataCogsTask"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static readItemOptionDataFromCogs(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/ItemOptionData;
    .locals 1

    .line 723
    const-class v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 724
    invoke-interface {p0, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllCatalogConnectV2Objects(Ljava/lang/Class;)Ljava/util/List;

    move-result-object p0

    .line 726
    new-instance v0, Lcom/squareup/ui/items/ItemOptionData;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/ItemOptionData;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method
