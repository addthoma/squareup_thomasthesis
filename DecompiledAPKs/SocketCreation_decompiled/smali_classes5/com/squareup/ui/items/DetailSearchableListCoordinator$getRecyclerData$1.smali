.class public final Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;
.super Ljava/lang/Object;
.source "DetailSearchableListCoordinator.kt"

# interfaces
.implements Lcom/squareup/cycler/DataSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCoordinator;->getRecyclerData(Lcom/squareup/ui/items/DetailSearchableListScreen;)Lcom/squareup/cycler/DataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/DataSource<",
        "Lcom/squareup/ui/items/DetailSearchableListRow;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0011\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0008\u001a\u00020\u0004H\u0096\u0002R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1",
        "Lcom/squareup/cycler/DataSource;",
        "Lcom/squareup/ui/items/DetailSearchableListRow;",
        "size",
        "",
        "getSize",
        "()I",
        "get",
        "i",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $listDataSource:Lcom/squareup/cycler/DataSource;

.field final synthetic $screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

.field final synthetic $shouldShowCreateButton:Z

.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListCoordinator;ZLcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/cycler/DataSource;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/ui/items/DetailSearchableListScreen;",
            "Lcom/squareup/cycler/DataSource;",
            ")V"
        }
    .end annotation

    .line 387
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    iput-boolean p2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$shouldShowCreateButton:Z

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$listDataSource:Lcom/squareup/cycler/DataSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)Lcom/squareup/ui/items/DetailSearchableListRow;
    .locals 3

    .line 389
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$shouldShowCreateButton:Z

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    .line 392
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$listDataSource:Lcom/squareup/cycler/DataSource;

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    add-int/lit8 p1, p1, -0x1

    invoke-interface {v1, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->access$getRes$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Lcom/squareup/util/Res;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->access$toDetailSearchableObjectRow(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListRow;

    goto :goto_0

    .line 391
    :cond_1
    new-instance p1, Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->access$getRes$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Lcom/squareup/util/Res;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    invoke-virtual {v2}, Lcom/squareup/ui/items/DetailSearchableListScreen;->getState()Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getShouldShowCovertItemButton()Z

    move-result v2

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Z)V

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListRow;

    goto :goto_0

    .line 395
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$listDataSource:Lcom/squareup/cycler/DataSource;

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-interface {v1, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->access$getRes$p(Lcom/squareup/ui/items/DetailSearchableListCoordinator;)Lcom/squareup/util/Res;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/squareup/ui/items/DetailSearchableListCoordinator;->access$toDetailSearchableObjectRow(Lcom/squareup/ui/items/DetailSearchableListCoordinator;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListRow;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 0

    .line 387
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->get(I)Lcom/squareup/ui/items/DetailSearchableListRow;

    move-result-object p1

    return-object p1
.end method

.method public getSize()I
    .locals 2

    .line 399
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$listDataSource:Lcom/squareup/cycler/DataSource;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListCoordinator$getRecyclerData$1;->$shouldShowCreateButton:Z

    add-int/2addr v0, v1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 387
    invoke-static {p0}, Lcom/squareup/cycler/DataSource$DefaultImpls;->isEmpty(Lcom/squareup/cycler/DataSource;)Z

    move-result v0

    return v0
.end method
