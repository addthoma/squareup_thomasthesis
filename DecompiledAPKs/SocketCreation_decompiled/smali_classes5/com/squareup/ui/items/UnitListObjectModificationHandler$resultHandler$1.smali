.class final Lcom/squareup/ui/items/UnitListObjectModificationHandler$resultHandler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "UnitListObjectModificationHandler.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/UnitListObjectModificationHandler;->resultHandler(Lcom/squareup/ui/items/DetailSearchableListState;)Lkotlin/jvm/functions/Function1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "+",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "it",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/items/DetailSearchableListState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/UnitListObjectModificationHandler$resultHandler$1;->$state:Lcom/squareup/ui/items/DetailSearchableListState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/EditUnitResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitResult$EditSaved;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 53
    :cond_0
    instance-of p1, p1, Lcom/squareup/items/unit/EditUnitResult$EditDiscarded;

    if-eqz p1, :cond_1

    :goto_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 56
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    iget-object v1, p0, Lcom/squareup/ui/items/UnitListObjectModificationHandler$resultHandler$1;->$state:Lcom/squareup/ui/items/DetailSearchableListState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xf7

    const/4 v12, 0x0

    const-string v6, ""

    invoke-static/range {v2 .. v12}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 53
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/items/unit/EditUnitResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/UnitListObjectModificationHandler$resultHandler$1;->invoke(Lcom/squareup/items/unit/EditUnitResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
