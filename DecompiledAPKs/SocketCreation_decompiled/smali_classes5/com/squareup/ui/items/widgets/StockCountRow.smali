.class public Lcom/squareup/ui/items/widgets/StockCountRow;
.super Landroid/widget/LinearLayout;
.source "StockCountRow.java"


# static fields
.field private static final DEFAULT_LABEL_WEIGHT:F = 20.0f

.field private static final STOCK_FIELD_ALIGN_LEFT:I = 0x0

.field private static final STOCK_FIELD_ALIGN_RIGHT:I = 0x1

.field private static final WEIGHT_SUM:F = 100.0f


# instance fields
.field private final DEFAULT_LABEL_PADDING_LEFT:I

.field private final DEFAULT_STOCK_COUNT_CONTAINER_PADDING_LEFT:I

.field private final DEFAULT_STOCK_COUNT_CONTAINER_PADDING_RIGHT:I

.field private final borderPainter:Lcom/squareup/noho/EdgePainter;

.field private labelLayoutWeight:F

.field private labelPaddingLeft:I

.field private labelText:I

.field private shouldShowLabel:Z

.field private stockCountLabel:Lcom/squareup/marketfont/MarketTextView;

.field private stockCountProgressSpinner:Landroid/view/View;

.field private stockField:Landroid/widget/LinearLayout;

.field private stockFieldAlignment:I

.field private stockFieldPaddingLeft:I

.field private viewStockCountField:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/StockCountRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->responsive_16_20_24:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->DEFAULT_LABEL_PADDING_LEFT:I

    const/4 v0, 0x0

    .line 56
    iput v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->DEFAULT_STOCK_COUNT_CONTAINER_PADDING_LEFT:I

    .line 57
    iget v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->DEFAULT_LABEL_PADDING_LEFT:I

    iput v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->DEFAULT_STOCK_COUNT_CONTAINER_PADDING_RIGHT:I

    .line 59
    sget-object v1, Lcom/squareup/edititem/R$styleable;->StockCountRow:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 61
    sget v1, Lcom/squareup/edititem/R$styleable;->StockCountRow_showLabel:I

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->shouldShowLabel:Z

    .line 62
    iget-boolean v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->shouldShowLabel:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/edititem/R$string;->stock:I

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    :goto_0
    iput v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->labelText:I

    .line 63
    sget v1, Lcom/squareup/edititem/R$styleable;->StockCountRow_labelPaddingLeft:I

    iget v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->DEFAULT_LABEL_PADDING_LEFT:I

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->labelPaddingLeft:I

    .line 65
    iget-boolean v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->shouldShowLabel:Z

    if-eqz v1, :cond_1

    sget v1, Lcom/squareup/edititem/R$styleable;->StockCountRow_sqLabelLayoutWeight:I

    const/high16 v2, 0x41a00000    # 20.0f

    .line 66
    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iput v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->labelLayoutWeight:F

    .line 68
    iget-boolean v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->shouldShowLabel:Z

    if-eqz v1, :cond_2

    sget v1, Lcom/squareup/edititem/R$styleable;->StockCountRow_stockFieldPaddingLeft:I

    iget v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->DEFAULT_STOCK_COUNT_CONTAINER_PADDING_LEFT:I

    .line 69
    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    iput v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockFieldPaddingLeft:I

    .line 72
    sget v1, Lcom/squareup/edititem/R$styleable;->StockCountRow_stockFieldAlignment:I

    .line 73
    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockFieldAlignment:I

    .line 76
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/StockCountRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$dimen;->noho_divider_hairline:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/StockCountRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/noho/R$color;->noho_divider_hairline:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 78
    new-instance v3, Lcom/squareup/noho/EdgePainter;

    invoke-direct {v3, p0, v1, v2}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    iput-object v3, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->borderPainter:Lcom/squareup/noho/EdgePainter;

    .line 79
    sget v1, Lcom/squareup/edititem/R$styleable;->StockCountRow_sqHideBorder:I

    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 80
    iget-object v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->borderPainter:Lcom/squareup/noho/EdgePainter;

    not-int v1, v1

    and-int/lit8 v1, v1, 0xf

    invoke-virtual {v2, v1}, Lcom/squareup/noho/EdgePainter;->addEdges(I)V

    .line 82
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    const/4 p2, 0x0

    .line 85
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/widgets/StockCountRow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 86
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/StockCountRow;->setWillNotDraw(Z)V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/items/widgets/StockCountRow;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/noho/R$dimen;->noho_row_height:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 87
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/widgets/StockCountRow;->setMinimumHeight(I)V

    const/16 p2, 0x10

    .line 89
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/widgets/StockCountRow;->setGravity(I)V

    .line 91
    sget p2, Lcom/squareup/edititem/R$layout;->stock_count_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/items/widgets/StockCountRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method private cleanUpChildren()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockCountProgressSpinner:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setClickable(Z)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 124
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->borderPainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 7

    .line 95
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 96
    sget v0, Lcom/squareup/edititem/R$id;->stock_count_row_stock_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockField:Landroid/widget/LinearLayout;

    .line 97
    sget v0, Lcom/squareup/edititem/R$id;->stock_count_row_view_stock_count_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    .line 98
    sget v0, Lcom/squareup/edititem/R$id;->stock_count_row_progress_spinner:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockCountProgressSpinner:Landroid/view/View;

    .line 99
    sget v0, Lcom/squareup/edititem/R$id;->stock_count_row_label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockCountLabel:Lcom/squareup/marketfont/MarketTextView;

    const/high16 v0, 0x42c80000    # 100.0f

    .line 101
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/widgets/StockCountRow;->setWeightSum(F)V

    const/4 v1, 0x0

    .line 102
    invoke-virtual {p0, v1}, Lcom/squareup/ui/items/widgets/StockCountRow;->setOrientation(I)V

    .line 104
    iget-object v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v2, v3}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 105
    iget-object v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    iget v3, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockFieldAlignment:I

    const v4, 0x800005

    const v5, 0x800003

    const/4 v6, 0x1

    if-ne v3, v6, :cond_0

    const v3, 0x800005

    goto :goto_0

    :cond_0
    const v3, 0x800003

    :goto_0
    or-int/lit8 v3, v3, 0x10

    invoke-virtual {v2, v3}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 108
    iget v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->labelText:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 109
    iget-object v3, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockCountLabel:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v3, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 111
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockCountLabel:Lcom/squareup/marketfont/MarketTextView;

    iget v3, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->labelPaddingLeft:I

    invoke-virtual {v2, v3, v1, v1, v1}, Lcom/squareup/marketfont/MarketTextView;->setPadding(IIII)V

    .line 112
    iget-object v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockCountLabel:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v2}, Lcom/squareup/marketfont/MarketTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 113
    iget v3, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->labelLayoutWeight:F

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 114
    iget-object v3, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockCountLabel:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v3, v2}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    iget-object v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockField:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 117
    iget v3, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->labelLayoutWeight:F

    sub-float/2addr v0, v3

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockField:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockField:Landroid/widget/LinearLayout;

    iget v2, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockFieldPaddingLeft:I

    iget v3, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->DEFAULT_STOCK_COUNT_CONTAINER_PADDING_RIGHT:I

    invoke-virtual {v0, v2, v1, v3, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockField:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockFieldAlignment:I

    if-ne v1, v6, :cond_2

    goto :goto_1

    :cond_2
    const v4, 0x800003

    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    return-void
.end method

.method public showLoadingStockCountSpinner()V
    .locals 2

    .line 137
    invoke-direct {p0}, Lcom/squareup/ui/items/widgets/StockCountRow;->cleanUpChildren()V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->stockCountProgressSpinner:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showViewStockCountField(Ljava/lang/String;Lcom/squareup/debounce/DebouncedOnClickListener;)V
    .locals 1

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/items/widgets/StockCountRow;->cleanUpChildren()V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 133
    iget-object p1, p0, Lcom/squareup/ui/items/widgets/StockCountRow;->viewStockCountField:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
