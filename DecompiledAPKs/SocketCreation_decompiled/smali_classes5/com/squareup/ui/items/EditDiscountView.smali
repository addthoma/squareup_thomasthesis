.class public Lcom/squareup/ui/items/EditDiscountView;
.super Lcom/squareup/widgets/PairLayout;
.source "EditDiscountView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/items/BaseEditObjectView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditDiscountView$SavedState;
    }
.end annotation


# instance fields
.field private amountView:Lcom/squareup/widgets/SelectableEditText;

.field private banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

.field private container:Landroid/view/View;

.field currencyCode:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private dateRangeSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

.field private final defaultDiscountName:Ljava/lang/String;

.field private discountDeleteButton:Lcom/squareup/ui/ConfirmButton;

.field private discountNameDisplay:Landroid/widget/TextView;

.field private discountNameView:Landroid/widget/EditText;

.field private discountSwitch:Lcom/squareup/widgets/CheckableGroup;

.field private discountSwitchAmount:Lcom/squareup/marketfont/MarketRadioButton;

.field private discountSwitchPercentage:Lcom/squareup/marketfont/MarketRadioButton;

.field private discountedItemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private imageTile:Landroid/view/View;

.field private itemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private optionsSection:Landroid/view/View;

.field percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private percentageView:Lcom/squareup/widgets/SelectableEditText;

.field presenter:Lcom/squareup/ui/items/EditDiscountScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private pricingRuleHelpText:Lcom/squareup/widgets/MessageView;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private scheduleSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

.field shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private tagImageView:Landroid/widget/ImageView;

.field private taxBasisHelpText:Lcom/squareup/widgets/MessageView;

.field private taxBasisToggleRow:Lcom/squareup/noho/NohoCheckableRow;

.field private textTile:Lcom/squareup/orderentry/TextTile;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditDiscountScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditDiscountScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditDiscountScreen$Component;->inject(Lcom/squareup/ui/items/EditDiscountView;)V

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/edititem/R$string;->new_discount:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->defaultDiscountName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditDiscountView;)Ljava/lang/String;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/ui/items/EditDiscountView;->defaultDiscountName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditDiscountView;)Landroid/widget/TextView;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountNameDisplay:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/items/EditDiscountView;Ljava/lang/String;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditDiscountView;->maybeUpdateTextTileName(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/items/EditDiscountView;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->updateTagAmount()V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 399
    sget v0, Lcom/squareup/edititem/R$id;->discount_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountNameView:Landroid/widget/EditText;

    .line 400
    sget v0, Lcom/squareup/edititem/R$id;->discount_percentage:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    .line 401
    sget v0, Lcom/squareup/edititem/R$id;->discount_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    .line 402
    sget v0, Lcom/squareup/edititem/R$id;->discount_name_display:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountNameDisplay:Landroid/widget/TextView;

    .line 403
    sget v0, Lcom/squareup/edititem/R$id;->discount_switch:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    .line 404
    sget v0, Lcom/squareup/edititem/R$id;->discount_switch_percentage:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketRadioButton;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitchPercentage:Lcom/squareup/marketfont/MarketRadioButton;

    .line 405
    sget v0, Lcom/squareup/edititem/R$id;->discount_switch_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketRadioButton;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitchAmount:Lcom/squareup/marketfont/MarketRadioButton;

    .line 406
    sget v0, Lcom/squareup/edititem/R$id;->discount_delete:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountDeleteButton:Lcom/squareup/ui/ConfirmButton;

    .line 407
    sget v0, Lcom/squareup/edititem/R$id;->discount_tag_imageview:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->tagImageView:Landroid/widget/ImageView;

    .line 408
    sget v0, Lcom/squareup/edititem/R$id;->edit_discount_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->container:Landroid/view/View;

    .line 409
    sget v0, Lcom/squareup/edititem/R$id;->banner:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/AppliedLocationsBanner;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

    .line 410
    sget v0, Lcom/squareup/edititem/R$id;->discount_icon:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->imageTile:Landroid/view/View;

    .line 411
    sget v0, Lcom/squareup/edititem/R$id;->discount_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/TextTile;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->textTile:Lcom/squareup/orderentry/TextTile;

    .line 412
    sget v0, Lcom/squareup/edititem/R$id;->discount_options_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->optionsSection:Landroid/view/View;

    .line 413
    sget v0, Lcom/squareup/edititem/R$id;->discount_modify_tax_basis:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->taxBasisToggleRow:Lcom/squareup/noho/NohoCheckableRow;

    .line 414
    sget v0, Lcom/squareup/edititem/R$id;->tax_basis_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->taxBasisHelpText:Lcom/squareup/widgets/MessageView;

    .line 415
    sget v0, Lcom/squareup/edititem/R$id;->items_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DiscountRuleSectionView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->itemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    .line 416
    sget v0, Lcom/squareup/edititem/R$id;->discounted_items_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DiscountRuleSectionView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountedItemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    .line 417
    sget v0, Lcom/squareup/edititem/R$id;->schedule_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DiscountRuleSectionView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->scheduleSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    .line 418
    sget v0, Lcom/squareup/edititem/R$id;->date_range_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DiscountRuleSectionView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->dateRangeSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    .line 419
    sget v0, Lcom/squareup/edititem/R$id;->pricing_rule_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->pricingRuleHelpText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private maybeUpdateTextTileName(Ljava/lang/String;)V
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->textTile:Lcom/squareup/orderentry/TextTile;

    if-eqz v0, :cond_0

    const-string v1, ""

    .line 365
    invoke-virtual {v0, p1, v1}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showAmountView()V
    .locals 2

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setVisibility(I)V

    .line 306
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->updateTagAmount()V

    return-void
.end method

.method private showPercentageView()V
    .locals 2

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setVisibility(I)V

    .line 312
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->updateTagAmount()V

    return-void
.end method

.method private updateTagAmount()V
    .locals 3

    .line 278
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->isPercentage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->getPercentageText()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Numbers;->parseFormattedPercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v0

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->empty_discount_standalone_percent_character:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 286
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_2

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 290
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 294
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->tagImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/marin/widgets/MarinTagDrawable;

    if-eqz v1, :cond_3

    .line 295
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->tagImageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Lcom/squareup/marin/widgets/MarinTagDrawable;

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinTagDrawable;->setText(Ljava/lang/String;)V

    goto :goto_1

    .line 297
    :cond_3
    new-instance v1, Lcom/squareup/marin/widgets/MarinTagDrawable;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/marin/widgets/MarinTagDrawable;-><init>(Landroid/content/Context;)V

    .line 298
    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinTagDrawable;->setText(Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->tagImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public getAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->priceHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getDoNotModifyTaxBasis()Z
    .locals 1

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->taxBasisToggleRow:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountNameView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getPercentageText()Ljava/lang/String;
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 332
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public isPercentage()Z
    .locals 2

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    sget v1, Lcom/squareup/edititem/R$id;->discount_switch_percentage:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$onFinishInflate$0$EditDiscountView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    const/4 p1, -0x1

    if-eq p3, p1, :cond_2

    if-ne p3, p2, :cond_0

    goto :goto_1

    .line 165
    :cond_0
    sget p1, Lcom/squareup/edititem/R$id;->discount_switch_amount:I

    if-ne p2, p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 166
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditDiscountView;->onSwitch(Z)V

    :cond_2
    :goto_1
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->presenter:Lcom/squareup/ui/items/EditDiscountScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->showConfirmDiscardDialogOrFinish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->presenter:Lcom/squareup/ui/items/EditDiscountScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 176
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 103
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 104
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->bindViews()V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->optionsSection:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ENABLE_TAX_BASIS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->taxBasisHelpText:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/edititem/R$string;->tax_basis_help_text:I

    const-string v3, "learn_more"

    .line 109
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->tax_basis_help_url:I

    .line 110
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->learn_more:I

    .line 111
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 112
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 108
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->pricingRuleHelpText:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/edititem/R$string;->discount_rules_help_text:I

    const-string v3, "dashboard"

    .line 115
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->dashboard_discount_url:I

    .line 116
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->square_dashboard:I

    .line 117
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 114
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountDeleteButton:Lcom/squareup/ui/ConfirmButton;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->presenter:Lcom/squareup/ui/items/EditDiscountScreen$Presenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$if3eQJEC8PI_y9h6oAg7xvZ9srU;

    invoke-direct {v2, v1}, Lcom/squareup/ui/items/-$$Lambda$if3eQJEC8PI_y9h6oAg7xvZ9srU;-><init>(Lcom/squareup/ui/items/EditDiscountScreen$Presenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountNameView:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/items/EditDiscountView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditDiscountView$1;-><init>(Lcom/squareup/ui/items/EditDiscountView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/items/EditDiscountView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditDiscountView$2;-><init>(Lcom/squareup/ui/items/EditDiscountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/items/EditDiscountView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditDiscountView$3;-><init>(Lcom/squareup/ui/items/EditDiscountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->priceHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    sget-object v2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    .line 144
    new-instance v0, Lcom/squareup/text/PercentageWholeUnitAmountScrubber;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->res:Lcom/squareup/util/Res;

    sget-object v2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-direct {v0, v1, v2}, Lcom/squareup/text/PercentageWholeUnitAmountScrubber;-><init>(Lcom/squareup/util/Res;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    .line 146
    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {v1, v0, v2}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    .line 149
    sget v0, Lcom/squareup/proto_utilities/R$string;->percent_character_pattern:I

    .line 150
    invoke-static {p0, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "value"

    const-string v2, "0"

    .line 151
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "0123456789%,.\u00a0"

    invoke-static {v1}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitchPercentage:Lcom/squareup/marketfont/MarketRadioButton;

    const-string v1, "%"

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketRadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitchAmount:Lcom/squareup/marketfont/MarketRadioButton;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketRadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditDiscountView$yVJBMfVfyEjIBhnQ6e6mm1LuAB8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditDiscountView$yVJBMfVfyEjIBhnQ6e6mm1LuAB8;-><init>(Lcom/squareup/ui/items/EditDiscountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 168
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->updateTagAmount()V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->textTile:Lcom/squareup/orderentry/TextTile;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/TextTile;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->presenter:Lcom/squareup/ui/items/EditDiscountScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/EditDiscountScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 340
    check-cast p1, Lcom/squareup/ui/items/EditDiscountView$SavedState;

    .line 341
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditDiscountView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/squareup/widgets/PairLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->clearChecked()V

    .line 343
    invoke-static {p1}, Lcom/squareup/ui/items/EditDiscountView$SavedState;->access$500(Lcom/squareup/ui/items/EditDiscountView$SavedState;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 344
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->showPercentageView()V

    goto :goto_0

    .line 346
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->showAmountView()V

    :goto_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 336
    new-instance v0, Lcom/squareup/ui/items/EditDiscountView$SavedState;

    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditDiscountView;->isPercentage()Z

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/items/EditDiscountView$SavedState;-><init>(Landroid/os/Parcelable;ZLcom/squareup/ui/items/EditDiscountView$1;)V

    return-object v0
.end method

.method onSwitch(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 186
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->showAmountView()V

    .line 187
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    goto :goto_0

    .line 189
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->showPercentageView()V

    .line 190
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    :goto_0
    return-void
.end method

.method public requestInitialFocus()V
    .locals 1

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->container:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method public setTextTile(Z)V
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->imageTile:Landroid/view/View;

    if-eqz v0, :cond_0

    xor-int/lit8 v1, p1, 0x1

    .line 352
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->textTile:Lcom/squareup/orderentry/TextTile;

    if-eqz v0, :cond_1

    .line 355
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :cond_1
    return-void
.end method

.method public showDiscountLoaded(Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;ZZ)V
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountNameView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 213
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditDiscountView;->maybeUpdateTextTileName(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 215
    invoke-virtual {p2}, Lcom/squareup/util/Percentage;->basisPoints()I

    move-result p1

    .line 216
    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p3, :cond_1

    .line 219
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 222
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1}, Lcom/squareup/widgets/CheckableGroup;->clearChecked()V

    if-eqz p4, :cond_2

    .line 224
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    sget p2, Lcom/squareup/edititem/R$id;->discount_switch_percentage:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 225
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->showPercentageView()V

    goto :goto_0

    .line 227
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    sget p2, Lcom/squareup/edititem/R$id;->discount_switch_amount:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 228
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->showAmountView()V

    .line 230
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->discountDeleteButton:Lcom/squareup/ui/ConfirmButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    .line 231
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->discountNameView:Landroid/widget/EditText;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 233
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 234
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/CheckableGroup;->setEnabled(Z)V

    .line 235
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->taxBasisToggleRow:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p1, p5}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    return-void
.end method

.method public showDiscountLoading()V
    .locals 2

    .line 195
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->showPercentageView()V

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountNameView:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->amountView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->percentageView:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setEnabled(Z)V

    return-void
.end method

.method public showMultiUnitContent()V
    .locals 3

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountDeleteButton:Lcom/squareup/ui/ConfirmButton;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/edititem/R$string;->item_editing_delete_from_location_discount:I

    .line 272
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 271
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->banner:Lcom/squareup/ui/items/AppliedLocationsBanner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/AppliedLocationsBanner;->setVisibility(I)V

    return-void
.end method

.method public showNewDiscount()V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->clearChecked()V

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->discountSwitch:Lcom/squareup/widgets/CheckableGroup;

    sget v1, Lcom/squareup/edititem/R$id;->discount_switch_percentage:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    const-string v0, ""

    .line 206
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditDiscountView;->maybeUpdateTextTileName(Ljava/lang/String;)V

    .line 207
    invoke-direct {p0}, Lcom/squareup/ui/items/EditDiscountView;->showPercentageView()V

    return-void
.end method

.method public updateDiscountRule(Ljava/util/List;Lcom/squareup/ui/items/EditDiscountScreen$RuleType;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;",
            "Lcom/squareup/ui/items/EditDiscountScreen$RuleType;",
            ")V"
        }
    .end annotation

    .line 245
    sget-object v0, Lcom/squareup/ui/items/EditDiscountView$4;->$SwitchMap$com$squareup$ui$items$EditDiscountScreen$RuleType:[I

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v1, 0x2

    if-eq p2, v1, :cond_2

    const/4 v1, 0x3

    if-eq p2, v1, :cond_1

    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 256
    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->dateRangeSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setData(Ljava/util/List;)V

    goto :goto_0

    .line 259
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "invalid rule type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 253
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->scheduleSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setData(Ljava/util/List;)V

    goto :goto_0

    .line 250
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->discountedItemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setData(Ljava/util/List;)V

    goto :goto_0

    .line 247
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->itemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setData(Ljava/util/List;)V

    .line 262
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->pricingRuleHelpText:Lcom/squareup/widgets/MessageView;

    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->itemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    .line 263
    invoke-virtual {p2}, Lcom/squareup/ui/items/DiscountRuleSectionView;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->scheduleSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DiscountRuleSectionView;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountView;->dateRangeSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DiscountRuleSectionView;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_4

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    .line 262
    :cond_5
    :goto_1
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public updateHeaders(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountView;->itemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setTitle(Ljava/lang/String;)V

    .line 240
    iget-object p1, p0, Lcom/squareup/ui/items/EditDiscountView;->discountedItemsSection:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setTitle(Ljava/lang/String;)V

    return-void
.end method
