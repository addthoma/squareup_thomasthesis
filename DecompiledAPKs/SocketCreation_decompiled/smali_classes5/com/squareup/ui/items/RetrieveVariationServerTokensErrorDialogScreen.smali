.class public final Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "RetrieveVariationServerTokensErrorDialogScreen.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Factory;,
        Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 \u00112\u00020\u0001:\u0002\u0011\u0012B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;",
        "Lcom/squareup/ui/items/InEditItemScope;",
        "parent",
        "Lcom/squareup/ui/items/EditItemScope;",
        "shouldSaveItemVariationsUsingCogs",
        "",
        "(Lcom/squareup/ui/items/EditItemScope;Z)V",
        "getParent",
        "()Lcom/squareup/ui/items/EditItemScope;",
        "getShouldSaveItemVariationsUsingCogs",
        "()Z",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "Companion",
        "Factory",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Companion;


# instance fields
.field private final parent:Lcom/squareup/ui/items/EditItemScope;

.field private final shouldSaveItemVariationsUsingCogs:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;->Companion:Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Companion;

    .line 61
    sget-object v0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026dInt() == 1\n      )\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;Z)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    iput-object p1, p0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;->parent:Lcom/squareup/ui/items/EditItemScope;

    iput-boolean p2, p0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;->shouldSaveItemVariationsUsingCogs:Z

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 74
    iget-boolean p2, p0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;->shouldSaveItemVariationsUsingCogs:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public final getParent()Lcom/squareup/ui/items/EditItemScope;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;->parent:Lcom/squareup/ui/items/EditItemScope;

    return-object v0
.end method

.method public final getShouldSaveItemVariationsUsingCogs()Z
    .locals 1

    .line 29
    iget-boolean v0, p0, Lcom/squareup/ui/items/RetrieveVariationServerTokensErrorDialogScreen;->shouldSaveItemVariationsUsingCogs:Z

    return v0
.end method
