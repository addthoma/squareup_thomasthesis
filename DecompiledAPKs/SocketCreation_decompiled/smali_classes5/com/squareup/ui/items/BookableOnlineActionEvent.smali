.class public final Lcom/squareup/ui/items/BookableOnlineActionEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "BookableOnlineAction.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/items/BookableOnlineActionEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "bookable_online",
        "",
        "(Z)V",
        "getBookable_online",
        "()Z",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bookable_online:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/ui/items/BookableOnlineActionName;->BOOKABLE_ONLINE_ACTION_NAME:Lcom/squareup/ui/items/BookableOnlineActionName;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    .line 13
    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    iput-boolean p1, p0, Lcom/squareup/ui/items/BookableOnlineActionEvent;->bookable_online:Z

    return-void
.end method


# virtual methods
.method public final getBookable_online()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/ui/items/BookableOnlineActionEvent;->bookable_online:Z

    return v0
.end method
