.class public interface abstract Lcom/squareup/ui/items/EditDiscountScreen$Component;
.super Ljava/lang/Object;
.source "EditDiscountScreen.java"

# interfaces
.implements Lcom/squareup/ui/items/AppliedLocationsBanner$Component;


# annotations
.annotation runtime Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/catalog/online/CatalogOnlineModule;,
        Lcom/squareup/ui/items/EditDiscountScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/items/EditDiscountView;)V
.end method
