.class final Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;
.super Ljava/lang/Object;
.source "DetailSearchableListCogsObjectListHelper.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->apply(Lkotlin/Unit;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "libraryCursor",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;

    invoke-direct {v0}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/ui/items/DetailSearchableListData;
    .locals 3

    .line 64
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListData;

    const-string v1, "libraryCursor"

    .line 65
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v1

    .line 66
    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper;->Companion:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;

    invoke-static {v2, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->access$toDataSourceOfDetailSearchableListObject(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    .line 64
    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/items/DetailSearchableListData;-><init>(ILcom/squareup/cycler/DataSource;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$2;->call(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/ui/items/DetailSearchableListData;

    move-result-object p1

    return-object p1
.end method
