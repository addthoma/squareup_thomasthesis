.class public final Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditServicePriceTypeSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditServicePriceTypeSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditServicePriceTypeSelectionCoordinator.kt\ncom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator\n*L\n1#1,60:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u000c\u0010\u000e\u001a\u00020\n*\u00020\u000cH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;",
        "(Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "priceTypeGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "configureActionBar",
        "bindViews",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private priceTypeGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private final runner:Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;)V
    .locals 1

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->runner:Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;)Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->runner:Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 42
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 43
    sget v0, Lcom/squareup/edititem/R$id;->price_type_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->priceTypeGroup:Lcom/squareup/noho/NohoCheckableGroup;

    return-void
.end method

.method private final configureActionBar(Landroid/view/View;)V
    .locals 4

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 51
    new-instance v2, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;Landroid/view/View;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 53
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v3, Lcom/squareup/edititem/R$string;->edit_service_select_price_type:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 52
    invoke-virtual {v1, v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 57
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->bindViews(Landroid/view/View;)V

    .line 25
    new-instance v0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator$attach$1;-><init>(Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->configureActionBar(Landroid/view/View;)V

    .line 30
    iget-object p1, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->runner:Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;

    invoke-interface {p1}, Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen$EditServicePriceTypeSelectionRunner;->isVariablePriceType()Z

    move-result p1

    const-string v0, "priceTypeGroup"

    if-eqz p1, :cond_1

    .line 31
    iget-object p1, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->priceTypeGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/edititem/R$id;->variable_button:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    goto :goto_0

    .line 33
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->priceTypeGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v1, Lcom/squareup/edititem/R$id;->fixed_button:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    .line 36
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;->priceTypeGroup:Lcom/squareup/noho/NohoCheckableGroup;

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator$attach$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator$attach$2;-><init>(Lcom/squareup/ui/items/EditServicePriceTypeSelectionCoordinator;)V

    check-cast v0, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method
