.class final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "AssignUnitToVariationWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;",
        "parcel",
        "Landroid/os/Parcel;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/os/Parcel;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;
    .locals 13

    .line 131
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 132
    invoke-static {}, Lcom/squareup/ui/items/unit/ItemType;->values()[Lcom/squareup/ui/items/unit/ItemType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v3, v0, v1

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v0, "parcel.readString()!!"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v0, :cond_4

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    invoke-static {v6}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->fromByteArray([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v11

    .line 143
    new-instance v12, Lcom/squareup/items/unit/SelectableUnit;

    if-nez v7, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    if-nez v8, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    if-nez v10, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    const-string v6, "backingUnit"

    invoke-static {v11, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v6, v12

    invoke-direct/range {v6 .. v11}, Lcom/squareup/items/unit/SelectableUnit;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 142
    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_4
    const-string v0, "parcel"

    .line 146
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/util/Parcels;->readBooleanFromInt(Landroid/os/Parcel;)Z

    move-result v6

    .line 147
    invoke-static {p1}, Lcom/squareup/util/Parcels;->readBooleanFromInt(Landroid/os/Parcel;)Z

    move-result v7

    .line 148
    new-instance p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;

    .line 149
    move-object v5, v1

    check-cast v5, Ljava/util/List;

    move-object v1, p1

    .line 148
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V

    return-object p1
.end method

.method public bridge synthetic invoke(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 129
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope$Companion$CREATOR$1;->invoke(Landroid/os/Parcel;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;

    move-result-object p1

    return-object p1
.end method
