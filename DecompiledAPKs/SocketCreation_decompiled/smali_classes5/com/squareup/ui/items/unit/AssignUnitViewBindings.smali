.class public final Lcom/squareup/ui/items/unit/AssignUnitViewBindings;
.super Ljava/lang/Object;
.source "AssignUnitViewBindings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u001a\u0010\t\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0006\u0008\u0001\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000b0\nR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitViewBindings;",
        "",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "assignUnitLayoutConfig",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;",
        "(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)V",
        "getBindings",
        "",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignUnitLayoutConfig:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "recyclerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignUnitLayoutConfig"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitViewBindings;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitViewBindings;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitViewBindings;->assignUnitLayoutConfig:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    return-void
.end method

.method public static final synthetic access$getRecyclerFactory$p(Lcom/squareup/ui/items/unit/AssignUnitViewBindings;)Lcom/squareup/recycler/RecyclerFactory;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/ui/items/unit/AssignUnitViewBindings;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/items/unit/AssignUnitViewBindings;)Lcom/squareup/util/Res;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/ui/items/unit/AssignUnitViewBindings;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public final getBindings()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 20
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 21
    sget-object v2, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->Companion:Lcom/squareup/ui/items/unit/UnitSelectionScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 22
    iget-object v3, p0, Lcom/squareup/ui/items/unit/AssignUnitViewBindings;->assignUnitLayoutConfig:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    sget-object v4, Lcom/squareup/ui/items/unit/AssignUnitViewBindings$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-eq v3, v10, :cond_1

    if-ne v3, v9, :cond_0

    .line 24
    sget v3, Lcom/squareup/edititem/R$layout;->edit_item_unit_selection_sheet:I

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 23
    :cond_1
    sget v3, Lcom/squareup/edititem/R$layout;->edit_item_unit_selection_card:I

    :goto_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 26
    new-instance v6, Lcom/squareup/ui/items/unit/AssignUnitViewBindings$getBindings$1;

    invoke-direct {v6, p0}, Lcom/squareup/ui/items/unit/AssignUnitViewBindings$getBindings$1;-><init>(Lcom/squareup/ui/items/unit/AssignUnitViewBindings;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 20
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 30
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 31
    sget-object v2, Lcom/squareup/ui/items/unit/InventoryCountUpdateAlertScreen;->Companion:Lcom/squareup/ui/items/unit/InventoryCountUpdateAlertScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/InventoryCountUpdateAlertScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 32
    sget-object v3, Lcom/squareup/ui/items/unit/AssignUnitViewBindings$getBindings$2;->INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitViewBindings$getBindings$2;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 30
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    aput-object v1, v0, v10

    .line 34
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 35
    sget-object v2, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;->Companion:Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 36
    sget-object v3, Lcom/squareup/ui/items/unit/AssignUnitViewBindings$getBindings$3;->INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitViewBindings$getBindings$3;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 34
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    aput-object v1, v0, v9

    .line 19
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
