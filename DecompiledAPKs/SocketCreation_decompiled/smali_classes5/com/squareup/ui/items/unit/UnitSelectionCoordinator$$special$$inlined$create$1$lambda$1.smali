.class public final Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 UnitSelectionCoordinator.kt\ncom/squareup/ui/items/unit/UnitSelectionCoordinator\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,87:1\n157#2:88\n161#2,5:96\n1103#3,7:89\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$bind$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $createUnitButton$inlined:Landroid/view/View;

.field final synthetic $unitNumberLimitHelpText$inlined:Landroid/view/View;

.field final synthetic $unitSelectionHelpText$inlined:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;->$createUnitButton$inlined:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;->$unitSelectionHelpText$inlined:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;->$unitNumberLimitHelpText$inlined:Landroid/view/View;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;->$createUnitButton$inlined:Landroid/view/View;

    .line 89
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1$1;

    invoke-direct {v0, p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1$1;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;->getShouldDisableUnitCreation()Z

    move-result p1

    .line 97
    iget-object p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;->$createUnitButton$inlined:Landroid/view/View;

    const/16 v0, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/16 v2, 0x8

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;->$unitSelectionHelpText$inlined:Landroid/view/View;

    if-eqz p1, :cond_1

    const/16 v2, 0x8

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 99
    iget-object p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1$lambda$1;->$unitNumberLimitHelpText$inlined:Landroid/view/View;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :cond_2
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
