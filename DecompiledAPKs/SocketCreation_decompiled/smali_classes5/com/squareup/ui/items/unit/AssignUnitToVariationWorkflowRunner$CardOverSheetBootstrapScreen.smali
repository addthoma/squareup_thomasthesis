.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "AssignUnitToVariationWorkflowRunner.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardOverSheetBootstrapScreen"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAssignUnitToVariationWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AssignUnitToVariationWorkflowRunner.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,225:1\n1360#2:226\n1429#2,3:227\n*E\n*S KotlinDebug\n*F\n+ 1 AssignUnitToVariationWorkflowRunner.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen\n*L\n181#1:226\n181#1,3:227\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "itemType",
        "Lcom/squareup/ui/items/unit/ItemType;",
        "initialSelectedUnitId",
        "",
        "existingUnits",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "parent",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "res",
        "Lcom/squareup/util/Res;",
        "doesVariationHaveStock",
        "",
        "shouldShowDefaultStandardUnits",
        "(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/util/Res;ZZ)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final doesVariationHaveStock:Z

.field private final existingUnits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final initialSelectedUnitId:Ljava/lang/String;

.field private final itemType:Lcom/squareup/ui/items/unit/ItemType;

.field private final parent:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final res:Lcom/squareup/util/Res;

.field private final shouldShowDefaultStandardUnits:Z


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/util/Res;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/unit/ItemType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Lcom/squareup/util/Res;",
            "ZZ)V"
        }
    .end annotation

    const-string v0, "itemType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialSelectedUnitId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingUnits"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->initialSelectedUnitId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->existingUnits:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->res:Lcom/squareup/util/Res;

    iput-boolean p6, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->doesVariationHaveStock:Z

    iput-boolean p7, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->shouldShowDefaultStandardUnits:Z

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;

    sget-object v1, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;->CARD_OVER_SHEET:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 8

    .line 180
    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    iget-object v2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->itemType:Lcom/squareup/ui/items/unit/ItemType;

    iget-object v3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->initialSelectedUnitId:Ljava/lang/String;

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->existingUnits:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 226
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v0, v5}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 227
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 228
    check-cast v5, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 181
    iget-object v6, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->res:Lcom/squareup/util/Res;

    invoke-static {v5, v6}, Lcom/squareup/items/unit/SelectableUnitKt;->toSelectableUnit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/items/unit/SelectableUnit;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 229
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 182
    iget-boolean v5, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->doesVariationHaveStock:Z

    .line 183
    iget-boolean v6, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$CardOverSheetBootstrapScreen;->shouldShowDefaultStandardUnits:Z

    .line 179
    new-instance v7, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$Scope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;ZZ)V

    return-object v7
.end method
