.class final Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;
.super Lkotlin/jvm/internal/Lambda;
.source "UnitSelectionCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->update(Landroid/view/View;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnitSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnitSelectionCoordinator.kt\ncom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,242:1\n950#2:243\n*E\n*S KotlinDebug\n*F\n+ 1 UnitSelectionCoordinator.kt\ncom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2\n*L\n100#1:243\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $perDefaultStandardUnitsList:Ljava/util/List;

.field final synthetic $perUnitsList:Ljava/util/List;

.field final synthetic $screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$perUnitsList:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$perDefaultStandardUnitsList:Ljava/util/List;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 50
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getItemType()Lcom/squareup/ui/items/unit/ItemType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/ItemType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 96
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerService;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerService;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    check-cast v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 95
    :cond_1
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerItem;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerItem;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    check-cast v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    .line 99
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "singletonList(perItemTypeUnitType)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    .line 100
    iget-object v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$perUnitsList:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    iget-object v2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$perDefaultStandardUnitsList:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 243
    new-instance v2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2$$special$$inlined$sortedBy$1;

    invoke-direct {v2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2$$special$$inlined$sortedBy$1;-><init>()V

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 99
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 107
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 108
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;

    .line 109
    iget-object v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-virtual {v1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v1

    .line 110
    iget-object v2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->$screen:Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getState()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getShouldDisableUnitCreation()Z

    move-result v2

    .line 108
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion$CreateUnitButtonRow;-><init>(Lkotlin/jvm/functions/Function1;Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setExtraItem(Ljava/lang/Object;)V

    return-void
.end method
