.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;
.super Ljava/lang/Object;
.source "AssignUnitToVariationViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final assignUnitViewBindingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitViewBindings;",
            ">;"
        }
    .end annotation
.end field

.field private final editUnitViewBindingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitViewBindings;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;->editUnitViewBindingsProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;->assignUnitViewBindingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/unit/AssignUnitViewBindings;",
            ">;)",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/items/unit/ui/EditUnitViewBindings;Lcom/squareup/ui/items/unit/AssignUnitViewBindings;)Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;-><init>(Lcom/squareup/items/unit/ui/EditUnitViewBindings;Lcom/squareup/ui/items/unit/AssignUnitViewBindings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;->editUnitViewBindingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/unit/ui/EditUnitViewBindings;

    iget-object v1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;->assignUnitViewBindingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/unit/AssignUnitViewBindings;

    invoke-static {v0, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;->newInstance(Lcom/squareup/items/unit/ui/EditUnitViewBindings;Lcom/squareup/ui/items/unit/AssignUnitViewBindings;)Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory_Factory;->get()Lcom/squareup/ui/items/unit/AssignUnitToVariationViewFactory;

    move-result-object v0

    return-object v0
.end method
