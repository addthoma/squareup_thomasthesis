.class public final Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;
.super Ljava/lang/Object;
.source "EditModifierSetMainView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/EditModifierSetMainView;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/EditModifierSetMainView;",
            ">;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/items/EditModifierSetMainView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditModifierSetMainView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/EditModifierSetMainView;Ljava/lang/Object;)V
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->presenter:Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;

    return-void
.end method

.method public static injectPriceLocaleHelper(Lcom/squareup/ui/items/EditModifierSetMainView;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/items/EditModifierSetMainView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectSettings(Lcom/squareup/ui/items/EditModifierSetMainView;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainView;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/EditModifierSetMainView;)V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/EditModifierSetMainView;Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/items/EditModifierSetMainView;Lcom/squareup/text/Formatter;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->injectPriceLocaleHelper(Lcom/squareup/ui/items/EditModifierSetMainView;Lcom/squareup/money/PriceLocaleHelper;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->injectRes(Lcom/squareup/ui/items/EditModifierSetMainView;Lcom/squareup/util/Res;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->injectSettings(Lcom/squareup/ui/items/EditModifierSetMainView;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/ui/items/EditModifierSetMainView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditModifierSetMainView_MembersInjector;->injectMembers(Lcom/squareup/ui/items/EditModifierSetMainView;)V

    return-void
.end method
