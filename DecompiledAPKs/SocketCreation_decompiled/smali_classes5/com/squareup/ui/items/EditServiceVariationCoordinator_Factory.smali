.class public final Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;
.super Ljava/lang/Object;
.source "EditServiceVariationCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditServiceVariationCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final editServiceVariationRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->durationFormatterProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->editServiceVariationRunnerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;"
        }
    .end annotation

    .line 77
    new-instance v10, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/items/EditServiceVariationCoordinator;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/items/EditServiceVariationCoordinator;"
        }
    .end annotation

    .line 86
    new-instance v10, Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;-><init>(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditServiceVariationCoordinator;
    .locals 10

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/money/PriceLocaleHelper;

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/DurationFormatter;

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->editServiceVariationRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/catalogapi/CatalogIntegrationController;

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->newInstance(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator_Factory;->get()Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    move-result-object v0

    return-object v0
.end method
