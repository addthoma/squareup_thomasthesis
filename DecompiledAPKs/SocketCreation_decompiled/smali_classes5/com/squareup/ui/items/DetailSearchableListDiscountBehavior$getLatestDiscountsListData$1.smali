.class final Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DetailSearchableListDiscountBehavior.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior;->getLatestDiscountsListData(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/lang/String;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/i18n/Localizer;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "Lcom/squareup/prices/DiscountRulesLibraryCursor;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/prices/DiscountRulesLibraryCursor;",
        "kotlin.jvm.PlatformType",
        "libraryCursor",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "local",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

.field final synthetic $discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

.field final synthetic $discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

.field final synthetic $timeZone:Ljava/util/TimeZone;


# direct methods
.method constructor <init>(Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/shared/i18n/Localizer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->$discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->$timeZone:Ljava/util/TimeZone;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->$discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->$catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/prices/DiscountRulesLibraryCursor;
    .locals 7

    const-string v0, "libraryCursor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "local"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->$discountCursorFactory:Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    .line 67
    iget-object v4, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->$timeZone:Ljava/util/TimeZone;

    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->$discountBundleFactory:Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->$catalogLocalizer:Lcom/squareup/shared/i18n/Localizer;

    move-object v2, p2

    move-object v3, p1

    .line 66
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;->fromDiscountsCursor(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/TimeZone;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/shared/i18n/Localizer;)Lcom/squareup/prices/DiscountRulesLibraryCursor;

    move-result-object p1

    const-string p2, "discountCursorFactory.fr\u2026alogLocalizer\n          )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    check-cast p2, Lcom/squareup/shared/catalog/Catalog$Local;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListDiscountBehavior$getLatestDiscountsListData$1;->invoke(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/prices/DiscountRulesLibraryCursor;

    move-result-object p1

    return-object p1
.end method
