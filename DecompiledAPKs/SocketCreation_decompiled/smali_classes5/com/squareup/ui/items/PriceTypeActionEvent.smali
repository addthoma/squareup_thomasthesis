.class public final Lcom/squareup/ui/items/PriceTypeActionEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "ItemServicePriceTypeAction.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0017\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/items/PriceTypeActionEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "option",
        "Lcom/squareup/ui/items/PriceTypeActionOption;",
        "(Lcom/squareup/ui/items/PriceTypeActionOption;)V",
        "price_type",
        "",
        "price_type$annotations",
        "()V",
        "getPrice_type",
        "()Ljava/lang/String;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final price_type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/PriceTypeActionOption;)V
    .locals 1

    const-string v0, "option"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/ui/items/PriceTypeActionName;->PRICE_TYPE_ACTION_NAME:Lcom/squareup/ui/items/PriceTypeActionName;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    .line 18
    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 22
    invoke-virtual {p1}, Lcom/squareup/ui/items/PriceTypeActionOption;->getValue()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/PriceTypeActionEvent;->price_type:Ljava/lang/String;

    return-void
.end method

.method public static synthetic price_type$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getPrice_type()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/items/PriceTypeActionEvent;->price_type:Ljava/lang/String;

    return-object v0
.end method
