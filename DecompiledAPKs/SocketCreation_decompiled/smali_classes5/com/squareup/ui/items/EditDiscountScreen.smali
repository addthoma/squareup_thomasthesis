.class public Lcom/squareup/ui/items/EditDiscountScreen;
.super Lcom/squareup/ui/items/InEditDiscountScope;
.source "EditDiscountScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/items/EditDiscountScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditDiscountScreen$Component;,
        Lcom/squareup/ui/items/EditDiscountScreen$Module;,
        Lcom/squareup/ui/items/EditDiscountScreen$ComponentFactory;,
        Lcom/squareup/ui/items/EditDiscountScreen$RuleType;,
        Lcom/squareup/ui/items/EditDiscountScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditDiscountScreen;",
            ">;"
        }
    .end annotation
.end field

.field private static final NEW_DISCOUNT:Ljava/lang/String; = ""


# instance fields
.field final id:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 542
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreen$f07_0vXQ-GZ8j7mDwG5VfpQ-p4w;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditDiscountScreen$f07_0vXQ-GZ8j7mDwG5VfpQ-p4w;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditDiscountScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, ""

    .line 98
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditDiscountScreen;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/ui/items/InEditDiscountScope;-><init>()V

    .line 102
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen;->id:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditDiscountScreen;
    .locals 1

    .line 543
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 544
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditDiscountScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 539
    iget-object p2, p0, Lcom/squareup/ui/items/EditDiscountScreen;->id:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 106
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_DISCOUNT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 548
    sget v0, Lcom/squareup/edititem/R$layout;->edit_discount_view:I

    return v0
.end method
