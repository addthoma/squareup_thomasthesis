.class public final Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;
.super Ljava/lang/Object;
.source "DetailSearchableListUnitBehavior.kt"

# interfaces
.implements Lcom/squareup/ui/items/DetailSearchableListBehavior;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u0000 \'2\u00020\u0001:\u0001\'BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J$\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001b0\u00192\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J$\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001b0\u00192\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020!H\u0016J\u0016\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#2\u0006\u0010%\u001a\u00020&H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0013\u001a\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;",
        "Lcom/squareup/ui/items/DetailSearchableListBehavior;",
        "textBag",
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "shouldShowCreateFromSearchButton",
        "",
        "objectNumberLimit",
        "",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "locale",
        "Ljava/util/Locale;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;)V",
        "getObjectNumberLimit",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getShouldShowCreateFromSearchButton",
        "()Z",
        "getTextBag",
        "()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "handlePressingCreateButton",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "currentState",
        "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
        "event",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;",
        "handleSelectingObjectFromList",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;",
        "list",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "searchTerm",
        "",
        "Companion",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;

.field private static final unitMatchRegexStr:Ljava/lang/String; = ".*(\\s|^|-|\\()%s.*"


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final locale:Ljava/util/Locale;

.field private final objectNumberLimit:Ljava/lang/Integer;

.field private final res:Lcom/squareup/util/Res;

.field private final shouldShowCreateFromSearchButton:Z

.field private final textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->Companion:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;)V
    .locals 1

    const-string/jumbo v0, "textBag"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    iput-boolean p2, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->shouldShowCreateFromSearchButton:Z

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->objectNumberLimit:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->badBus:Lcom/squareup/badbus/BadBus;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->locale:Ljava/util/Locale;

    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    move v3, p2

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    .line 20
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;ZLjava/lang/Integer;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;)V

    return-void
.end method


# virtual methods
.method public getItemTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    .line 18
    invoke-static {p0}, Lcom/squareup/ui/items/DetailSearchableListBehavior$DefaultImpls;->getItemTypes(Lcom/squareup/ui/items/DetailSearchableListBehavior;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getObjectNumberLimit()Ljava/lang/Integer;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->objectNumberLimit:Ljava/lang/Integer;

    return-object v0
.end method

.method public getShouldShowCreateFromSearchButton()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->shouldShowCreateFromSearchButton:Z

    return v0
.end method

.method public getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public handlePressingCreateButton(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;)Lcom/squareup/workflow/WorkflowAction;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 32
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListState$Create;

    .line 33
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;->getPrePopulatedName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    move-object v6, p1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xf7

    const/4 v12, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v2 .. v12}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object p1

    .line 32
    invoke-direct {v1, p1}, Lcom/squareup/ui/items/DetailSearchableListState$Create;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    const/4 p1, 0x2

    const/4 p2, 0x0

    .line 31
    invoke-static {v0, v1, p2, p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public handleSelectingObjectFromList(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 42
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    .line 43
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object p2

    .line 44
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object p1

    .line 42
    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListState$Edit;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    const/4 p1, 0x0

    const/4 p2, 0x2

    .line 41
    invoke-static {v0, v1, p1, p2, p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public list(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->Companion:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->cogs:Lcom/squareup/cogs/Cogs;

    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->badBus:Lcom/squareup/badbus/BadBus;

    iget-object v4, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->locale:Ljava/util/Locale;

    iget-object v5, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->res:Lcom/squareup/util/Res;

    move-object v6, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;->access$getLatestUnitsListData(Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
