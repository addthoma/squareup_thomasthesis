.class public final Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "CategoryDetailScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final editItemGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p2, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p3, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p4, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p5, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p6, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p7, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p8, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p9, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p10, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->editItemGatewayProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p11, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p12, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;"
        }
    .end annotation

    .line 87
    new-instance v13, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;
    .locals 14

    .line 95
    new-instance v13, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;
    .locals 13

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->itemsAppletScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->editItemGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/items/EditItemGateway;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->orderEntryAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailScreen_Presenter_Factory;->get()Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
