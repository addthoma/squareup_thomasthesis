.class public Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;
.super Lcom/squareup/ui/items/DetailSearchableListPresenter;
.source "CategoryDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/CategoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
        "Lcom/squareup/ui/items/CategoryDetailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private categoryId:Ljava/lang/String;

.field private categoryName:Ljava/lang/String;

.field private latestCategoryName:Ljava/lang/String;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v10, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    .line 87
    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/items/DetailSearchableListPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    move-object v0, p2

    .line 89
    iput-object v0, v10, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    move-object/from16 v0, p8

    .line 90
    iput-object v0, v10, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v0, p12

    .line 91
    iput-object v0, v10, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method private getCategoryName()Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->latestCategoryName:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryName:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public static synthetic lambda$3EgyxB20FmeAp-wkOUl9r-__3Vg(Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method private onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 5

    .line 185
    invoke-virtual {p1}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 189
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v4, v0, v1

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->forceRefresh()V

    :cond_0
    new-array v0, v3, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 192
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    invoke-virtual {p1}, Lcom/squareup/cogs/CatalogUpdateEvent;->getUpdated()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogObject;

    .line 194
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v2, :cond_1

    .line 195
    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 196
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->updateCategoryName(Ljava/lang/String;)V

    :cond_2
    return-void

    .line 186
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "CategoryDetailScreen should never see multiple-batch update."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private shouldHideCreateItem()Z
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result v0

    return v0
.end method

.method private updateCategoryName(Ljava/lang/String;)V
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 209
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->latestCategoryName:Ljava/lang/String;

    .line 210
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->updateActionBarTitle()V

    .line 211
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/CategoryDetailView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/CategoryDetailView;->updateCategoryName(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected buildLibraryCursorInBackground(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
            "Lcom/squareup/ui/items/CategoryDetailView;",
            ">.ItemsQueryCallback;)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    .line 165
    const-class v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 166
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 167
    iget-object v1, p2, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    iget-object p2, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->findCatalogItemsForCategoryId(Ljava/lang/String;Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryId:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    invoke-virtual {p1, v1, p2, v0, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixSearchForItemsInCategory(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1
.end method

.method protected getActionBarTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 116
    invoke-direct {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getButtonCount()I
    .locals 1

    .line 159
    invoke-direct {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->shouldHideCreateItem()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    :goto_0
    return v0
.end method

.method getButtonText(I)Ljava/lang/String;
    .locals 1

    .line 150
    invoke-direct {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->shouldHideCreateItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/itemsapplet/R$string;->items_applet_edit_category:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    if-nez p1, :cond_1

    .line 153
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/registerlib/R$string;->create_item:I

    .line 154
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/itemsapplet/R$string;->items_applet_edit_category:I

    .line 155
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method getItemTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method protected isNested()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$CategoryDetailScreen$Presenter(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/String;
    .locals 2

    .line 102
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    iget-object v1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryId:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$1$CategoryDetailScreen$Presenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 103
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->updateCategoryName(Ljava/lang/String;)V

    return-void
.end method

.method onButtonClicked(I)V
    .locals 3

    .line 131
    invoke-direct {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->shouldHideCreateItem()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    .line 133
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->startEditCategoryFlow(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    sget-object v0, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    iget-object v1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryName:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlowInCategory(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_ITEM_IN_CATEGORY:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->startEditCategoryFlow(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 95
    invoke-super {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$CategoryDetailScreen$Presenter$3EgyxB20FmeAp-wkOUl9r-__3Vg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$CategoryDetailScreen$Presenter$3EgyxB20FmeAp-wkOUl9r-__3Vg;-><init>(Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 98
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/CategoryDetailScreen;

    .line 99
    iget-object v0, p1, Lcom/squareup/ui/items/CategoryDetailScreen;->categoryName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryName:Ljava/lang/String;

    .line 100
    iget-object p1, p1, Lcom/squareup/ui/items/CategoryDetailScreen;->categoryId:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->categoryId:Ljava/lang/String;

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$CategoryDetailScreen$Presenter$6tt1AbdsNtITxjJM3Vpv65HcPk8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$CategoryDetailScreen$Presenter$6tt1AbdsNtITxjJM3Vpv65HcPk8;-><init>(Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;)V

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$CategoryDetailScreen$Presenter$-QfISceNEdPnYnRFU95OZ-Vzz3g;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$CategoryDetailScreen$Presenter$-QfISceNEdPnYnRFU95OZ-Vzz3g;-><init>(Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;)V

    invoke-interface {p1, v0, v1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 107
    invoke-super {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 108
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/CategoryDetailView;

    invoke-direct {p0}, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->getCategoryName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/CategoryDetailView;->updateCategoryName(Ljava/lang/String;)V

    return-void
.end method

.method onRowClicked(I)V
    .locals 3

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v2

    .line 127
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    .line 126
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/ui/items/EditItemGateway;->startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    return-void
.end method

.method public rowsHaveThumbnails()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
