.class public final Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1;
.super Ljava/lang/Object;
.source "DetailSearchableListCogsObjectListHelper.kt"

# interfaces
.implements Lcom/squareup/cycler/DataSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->toDataSourceOfDetailSearchableListObject(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/cycler/DataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/DataSource<",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListCogsObjectListHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListCogsObjectListHelper.kt\ncom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1\n*L\n1#1,112:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0011\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0008\u001a\u00020\u0004H\u0096\u0002R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1",
        "Lcom/squareup/cycler/DataSource;",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "size",
        "",
        "getSize",
        "()I",
        "get",
        "i",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toDataSourceOfDetailSearchableListObject:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1;->$this_toDataSourceOfDetailSearchableListObject:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;
    .locals 3

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1;->$this_toDataSourceOfDetailSearchableListObject:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 77
    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToPosition(I)Z

    .line 78
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    const-string v1, "libraryEntry"

    .line 79
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    if-eqz v1, :cond_4

    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v0, 0x2

    if-ne v1, v0, :cond_4

    .line 83
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/api/items/Item$Type;->ordinal()I

    move-result v0

    aget v0, v1, v0

    if-eq v0, v2, :cond_1

    .line 85
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    goto :goto_0

    .line 84
    :cond_1
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    goto :goto_0

    .line 80
    :cond_2
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    if-eqz v0, :cond_3

    .line 81
    check-cast v0, Lcom/squareup/prices/DiscountRulesLibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/prices/DiscountRulesLibraryCursor;->discountDescription()Ljava/lang/String;

    move-result-object v0

    const-string v2, "(cursor as DiscountRules\u2026or).discountDescription()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-direct {v1, p1, v0}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    :goto_0
    return-object v0

    .line 81
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.prices.DiscountRulesLibraryCursor"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 87
    :cond_4
    new-instance p1, Lkotlin/NotImplementedError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "An operation is not implemented: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "CATALOG-8676: refactor old lists to use the workflow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 0

    .line 74
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1;->get(I)Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object p1

    return-object p1
.end method

.method public getSize()I
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1;->$this_toDataSourceOfDetailSearchableListObject:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 74
    invoke-static {p0}, Lcom/squareup/cycler/DataSource$DefaultImpls;->isEmpty(Lcom/squareup/cycler/DataSource;)Z

    move-result v0

    return v0
.end method
