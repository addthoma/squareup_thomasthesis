.class public Lcom/squareup/ui/items/RecyclerViewDragController;
.super Ljava/lang/Object;
.source "RecyclerViewDragController.java"

# interfaces
.implements Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;


# static fields
.field private static final ANIMATION_DURATION:I = 0x64


# instance fields
.field private final adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter<",
            "+",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            "*>;"
        }
    .end annotation
.end field

.field private disallowIntercept:Z

.field private draggedRowId:J

.field private final gestureDetector:Landroidx/core/view/GestureDetectorCompat;

.field private isDragging:Z

.field private isFirst:Z

.field private overlay:Landroid/widget/ImageView;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private startBounds:Landroid/graphics/Rect;

.field private startY:F


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/ImageView;Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView;",
            "Landroid/widget/ImageView;",
            "Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter<",
            "+",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            "*>;I)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput-boolean v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isDragging:Z

    const/4 v0, 0x1

    .line 25
    iput-boolean v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isFirst:Z

    const-wide/16 v0, -0x1

    .line 26
    iput-wide v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    const/4 v0, 0x0

    .line 27
    iput v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startY:F

    const/4 v0, 0x0

    .line 28
    iput-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    .line 38
    new-instance p2, Lcom/squareup/ui/items/RecyclerViewDragController$1;

    invoke-direct {p2, p0, p1, p3, p4}, Lcom/squareup/ui/items/RecyclerViewDragController$1;-><init>(Lcom/squareup/ui/items/RecyclerViewDragController;Landroidx/recyclerview/widget/RecyclerView;Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;I)V

    .line 72
    new-instance p3, Landroidx/core/view/GestureDetectorCompat;

    .line 73
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p3, p1, p2}, Landroidx/core/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->gestureDetector:Landroidx/core/view/GestureDetectorCompat;

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/ui/items/RecyclerViewDragController;Z)Z
    .locals 0

    .line 18
    iput-boolean p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isDragging:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/RecyclerViewDragController;FF)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/RecyclerViewDragController;->startDrag(FF)V

    return-void
.end method

.method private continueDrag(II)V
    .locals 6

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v1

    add-float/2addr v0, v1

    .line 137
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v1

    const/4 v2, 0x0

    if-lez v1, :cond_1

    .line 140
    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v3

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 142
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 145
    :cond_1
    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView;->getTop()I

    move-result v3

    :goto_0
    int-to-float v4, p2

    int-to-float v3, v3

    .line 148
    iget v5, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startY:F

    add-float/2addr v5, v3

    cmpg-float v5, v4, v5

    if-gez v5, :cond_3

    .line 149
    iget-object p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 150
    iget-object p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    .line 151
    iget-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildItemId(Landroid/view/View;)J

    move-result-wide v0

    .line 152
    iget-wide v2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    cmp-long p2, v0, v2

    if-eqz p2, :cond_2

    .line 153
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/RecyclerViewDragController;->swapViews(Landroid/view/View;)V

    :cond_2
    return-void

    .line 158
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticBottomRowsCount()I

    move-result v1

    if-lez v1, :cond_4

    .line 160
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getItemCount()I

    move-result v1

    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v3}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticBottomRowsCount()I

    move-result v3

    sub-int/2addr v1, v3

    .line 161
    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 163
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 164
    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    add-int/2addr p2, v3

    int-to-float p2, p2

    int-to-float v3, v1

    iget v5, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startY:F

    add-float/2addr v3, v5

    cmpl-float p2, p2, v3

    if-lez p2, :cond_4

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getHeight()I

    move-result p2

    sub-int/2addr v1, p2

    int-to-float p2, v1

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setTranslationY(F)V

    return-void

    .line 171
    :cond_4
    iget-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    iget v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startY:F

    sub-float v1, v4, v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 173
    iget-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    int-to-float p1, p1

    invoke-virtual {p2, p1, v4}, Landroidx/recyclerview/widget/RecyclerView;->findChildViewUnder(FF)Landroid/view/View;

    move-result-object p1

    .line 175
    iget-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/widget/ImageView;->getBottom()I

    move-result p2

    int-to-float p2, p2

    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v1

    add-float/2addr p2, v1

    .line 176
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 177
    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v1

    int-to-float v3, v3

    cmpl-float p2, p2, v3

    if-lez p2, :cond_5

    iget-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    iget p2, p2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr p2, v1

    int-to-float p2, p2

    cmpg-float p2, v0, p2

    if-gez p2, :cond_5

    const/4 v2, 0x1

    :cond_5
    if-nez v2, :cond_6

    if-eqz p1, :cond_6

    .line 181
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/RecyclerViewDragController;->swapViews(Landroid/view/View;)V

    :cond_6
    return-void
.end method

.method private endDrag()V
    .locals 3

    const/4 v0, 0x0

    .line 187
    iput-boolean v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isDragging:Z

    .line 189
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 190
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getDraggingRowView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 192
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v0

    .line 195
    iget-object v2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    sub-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 196
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    iget-wide v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    long-to-int v2, v1

    invoke-virtual {v0, v2}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getIndexForId(I)I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->notifyIndexChanged(I)V

    :goto_0
    const-wide/16 v0, -0x1

    .line 202
    iput-wide v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    iget-wide v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->setDraggingRowId(J)V

    return-void
.end method

.method private showOverlayFromView(Landroid/view/View;)V
    .locals 3

    .line 225
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 227
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_window_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/high16 v2, 0x3f400000    # 0.75f

    .line 228
    invoke-static {v1, v2}, Lcom/squareup/util/Colors;->withAlpha(IF)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 229
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 230
    invoke-virtual {p1, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 231
    iget-object p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTop(I)V

    return-void
.end method

.method private startDrag(FF)V
    .locals 3

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->findChildViewUnder(FF)Landroid/view/View;

    move-result-object p1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildItemId(Landroid/view/View;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    iget-wide v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->setDraggingRowId(J)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_1

    const/4 v1, 0x1

    .line 120
    :cond_1
    iput-boolean v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isFirst:Z

    .line 122
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p2, v0

    iput v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startY:F

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/RecyclerViewDragController;->showOverlayFromView(Landroid/view/View;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->overlay:Landroid/widget/ImageView;

    iget v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startY:F

    sub-float/2addr p2, v1

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 125
    iget-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    iget-wide v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    long-to-int v1, v0

    invoke-virtual {p2, v1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getIndexForId(I)I

    move-result p2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->notifyIndexChanged(I)V

    .line 128
    new-instance p2, Landroid/graphics/Rect;

    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    .line 130
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result p1

    invoke-direct {p2, v0, v1, v2, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    return-void
.end method

.method private swapViews(Landroid/view/View;)V
    .locals 5

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildItemId(Landroid/view/View;)J

    move-result-wide v0

    .line 209
    iget-object v2, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    iget-wide v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->draggedRowId:J

    long-to-int v4, v3

    invoke-virtual {v2, v4}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getIndexForId(I)I

    move-result v2

    .line 210
    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    long-to-int v1, v0

    invoke-virtual {v3, v1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getIndexForId(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v2, v1, :cond_0

    if-eq v0, v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->reorderItems(II)V

    .line 215
    :cond_0
    iget-boolean v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isFirst:Z

    if-eqz v1, :cond_1

    .line 216
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    const/4 v0, 0x0

    .line 217
    iput-boolean v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isFirst:Z

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->startBounds:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result p1

    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/MotionEvent;)Z
    .locals 1

    .line 77
    iget-boolean p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->disallowIntercept:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    return v0

    .line 80
    :cond_0
    iget-boolean p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isDragging:Z

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    return p1

    .line 83
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->gestureDetector:Landroidx/core/view/GestureDetectorCompat;

    invoke-virtual {p1, p2}, Landroidx/core/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    return v0
.end method

.method public onRequestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .line 106
    iput-boolean p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->disallowIntercept:Z

    return-void
.end method

.method public onTouchEvent(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/MotionEvent;)V
    .locals 3

    .line 89
    iget-boolean p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController;->isDragging:Z

    if-nez p1, :cond_0

    return-void

    .line 92
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    float-to-int p1, p1

    .line 93
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 95
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 96
    invoke-direct {p0}, Lcom/squareup/ui/items/RecyclerViewDragController;->endDrag()V

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p2

    const/4 v1, 0x3

    if-ne p2, v1, :cond_2

    .line 98
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/items/RecyclerViewDragController;->continueDrag(II)V

    .line 99
    invoke-direct {p0}, Lcom/squareup/ui/items/RecyclerViewDragController;->endDrag()V

    goto :goto_0

    .line 101
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/items/RecyclerViewDragController;->continueDrag(II)V

    :goto_0
    return-void
.end method
