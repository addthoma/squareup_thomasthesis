.class public final Lcom/squareup/ui/items/DetailSearchableListState$Companion;
.super Ljava/lang/Object;
.source "DetailSearchableListState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListState.kt\ncom/squareup/ui/items/DetailSearchableListState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,241:1\n180#2:242\n*E\n*S KotlinDebug\n*F\n+ 1 DetailSearchableListState.kt\ncom/squareup/ui/items/DetailSearchableListState$Companion\n*L\n118#1:242\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J3\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000cJ\u000e\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000fJ\u000c\u0010\u0010\u001a\u00020\u0008*\u00020\u0011H\u0002J\u000c\u0010\u0012\u001a\u00020\u0013*\u00020\u0011H\u0002\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListState$Companion;",
        "",
        "()V",
        "copyStateWithNewDatasource",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "state",
        "newListDataSource",
        "Lcom/squareup/cycler/DataSource;",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "unfilteredListSize",
        "",
        "objectNumberLimit",
        "(Lcom/squareup/ui/items/DetailSearchableListState;Lcom/squareup/cycler/DataSource;ILjava/lang/Integer;)Lcom/squareup/ui/items/DetailSearchableListState;",
        "fromSnapshot",
        "byteString",
        "Lokio/ByteString;",
        "readDetailSearchableObject",
        "Lokio/BufferedSource;",
        "readListData",
        "Lcom/squareup/ui/items/DetailSearchableListState$ListData;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListState$Companion;-><init>()V

    return-void
.end method

.method private final readDetailSearchableObject(Lokio/BufferedSource;)Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;
    .locals 2

    .line 138
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 139
    const-class v1, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    sget-object v1, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    return-object v0

    .line 142
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Attempted to unpack unhandled DetailSearchableObject"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final readListData(Lokio/BufferedSource;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;
    .locals 12

    .line 147
    new-instance v11, Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    .line 148
    new-instance v7, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    .line 149
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    .line 150
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v2

    .line 151
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v3

    .line 152
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v4

    .line 153
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v5

    .line 154
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListState$Companion$readListData$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListState$Companion$readListData$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/Integer;

    move-object v0, v7

    .line 148
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;-><init>(IIIIILjava/lang/Integer;)V

    .line 156
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v4

    .line 157
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v5

    .line 158
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v6

    .line 159
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v8

    .line 160
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v9, 0x3

    const/4 v10, 0x0

    move-object v0, v11

    move-object v3, v7

    move v7, v8

    move v8, p1

    .line 147
    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;-><init>(Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v11
.end method


# virtual methods
.method public final copyStateWithNewDatasource(Lcom/squareup/ui/items/DetailSearchableListState;Lcom/squareup/cycler/DataSource;ILjava/lang/Integer;)Lcom/squareup/ui/items/DetailSearchableListState;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/cycler/DataSource<",
            "+",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;I",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/squareup/ui/items/DetailSearchableListState;"
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "newListDataSource"

    move-object/from16 v3, p2

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    if-eqz p4, :cond_0

    .line 173
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move/from16 v4, p3

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    .line 178
    :goto_0
    instance-of v1, v0, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    if-eqz v1, :cond_1

    .line 179
    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    .line 180
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xbe

    const/4 v12, 0x0

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v12}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    .line 179
    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;->copy(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState;

    goto/16 :goto_1

    .line 186
    :cond_1
    instance-of v1, v0, Lcom/squareup/ui/items/DetailSearchableListState$Create;

    if-eqz v1, :cond_2

    .line 187
    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$Create;

    .line 188
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xbe

    const/4 v12, 0x0

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v12}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    .line 187
    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/DetailSearchableListState$Create;->copy(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)Lcom/squareup/ui/items/DetailSearchableListState$Create;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState;

    goto :goto_1

    .line 194
    :cond_2
    instance-of v1, v0, Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;

    if-eqz v1, :cond_3

    .line 195
    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;

    .line 196
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xbe

    const/4 v12, 0x0

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v12}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    .line 195
    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;->copy(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState;

    goto :goto_1

    .line 202
    :cond_3
    instance-of v1, v0, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    const/4 v13, 0x2

    const/4 v14, 0x0

    if-eqz v1, :cond_4

    .line 203
    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    .line 204
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xbe

    const/4 v12, 0x0

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v12}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    .line 203
    invoke-static {v1, v0, v14, v13, v14}, Lcom/squareup/ui/items/DetailSearchableListState$Edit;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$Edit;Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;ILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState;

    goto :goto_1

    .line 210
    :cond_4
    instance-of v1, v0, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    if-eqz v1, :cond_5

    .line 211
    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    .line 212
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/items/DetailSearchableListState;->getListData()Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xbe

    const/4 v12, 0x0

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v12}, Lcom/squareup/ui/items/DetailSearchableListState$ListData;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/cycler/DataSource;Ljava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/lang/String;ZZZZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object v0

    .line 211
    invoke-static {v1, v0, v14, v13, v14}, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;->copy$default(Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;ILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState;

    :goto_1
    return-object v0

    :cond_5
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final fromSnapshot(Lokio/ByteString;)Lcom/squareup/ui/items/DetailSearchableListState;
    .locals 3

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 119
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 120
    const-class v1, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListState;->Companion:Lcom/squareup/ui/items/DetailSearchableListState$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/DetailSearchableListState$Companion;->readListData(Lokio/BufferedSource;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListState$ShowList;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState;

    goto :goto_2

    .line 123
    :cond_0
    const-class v1, Lcom/squareup/ui/items/DetailSearchableListState$Create;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const-class v1, Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 124
    :goto_0
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;

    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListState;->Companion:Lcom/squareup/ui/items/DetailSearchableListState$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/DetailSearchableListState$Companion;->readListData(Lokio/BufferedSource;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListState$PreCreate;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState;

    goto :goto_2

    .line 126
    :cond_2
    const-class v1, Lcom/squareup/ui/items/DetailSearchableListState$Edit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    const-class v1, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 127
    :goto_1
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;

    .line 128
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListState;->Companion:Lcom/squareup/ui/items/DetailSearchableListState$Companion;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/DetailSearchableListState$Companion;->readDetailSearchableObject(Lokio/BufferedSource;)Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object v1

    .line 129
    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListState;->Companion:Lcom/squareup/ui/items/DetailSearchableListState$Companion;

    invoke-direct {v2, p1}, Lcom/squareup/ui/items/DetailSearchableListState$Companion;->readListData(Lokio/BufferedSource;)Lcom/squareup/ui/items/DetailSearchableListState$ListData;

    move-result-object p1

    .line 127
    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/items/DetailSearchableListState$PreEdit;-><init>(Lcom/squareup/ui/items/DetailSearchableListState$ListData;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState;

    :goto_2
    return-object v0

    .line 132
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
