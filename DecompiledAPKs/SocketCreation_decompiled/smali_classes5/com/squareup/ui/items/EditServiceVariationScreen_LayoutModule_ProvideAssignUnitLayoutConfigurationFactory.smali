.class public final Lcom/squareup/ui/items/EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory;
.super Ljava/lang/Object;
.source "EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory;->module:Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;

    return-void
.end method

.method public static create(Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;)Lcom/squareup/ui/items/EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/ui/items/EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory;-><init>(Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;)V

    return-object v0
.end method

.method public static provideAssignUnitLayoutConfiguration(Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;->provideAssignUnitLayoutConfiguration()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory;->module:Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;

    invoke-static {v0}, Lcom/squareup/ui/items/EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory;->provideAssignUnitLayoutConfiguration(Lcom/squareup/ui/items/EditServiceVariationScreen$LayoutModule;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceVariationScreen_LayoutModule_ProvideAssignUnitLayoutConfigurationFactory;->get()Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitLayoutConfiguration;

    move-result-object v0

    return-object v0
.end method
