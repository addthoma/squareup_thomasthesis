.class public Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;
.super Ljava/lang/Object;
.source "EditDiscountScreenModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountScreenModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

.field private localizer:Lcom/squareup/shared/i18n/Localizer;

.field private moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

.field private pricingRuleDiscountedProducts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;"
        }
    .end annotation
.end field

.field private pricingRuleProducts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;"
        }
    .end annotation
.end field

.field private res:Lcom/squareup/util/Res;

.field private shortDateFormat:Ljava/text/DateFormat;

.field private timeFormat:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 1

    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRuleProducts:Ljava/util/List;

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRuleDiscountedProducts:Ljava/util/List;

    .line 224
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/shared/i18n/Localizer;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRuleProducts:Ljava/util/List;

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRuleDiscountedProducts:Ljava/util/List;

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 217
    iput-object p2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->shortDateFormat:Ljava/text/DateFormat;

    .line 218
    iput-object p3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->timeFormat:Ljava/text/DateFormat;

    .line 219
    iput-object p4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->localizer:Lcom/squareup/shared/i18n/Localizer;

    .line 220
    iput-object p5, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/ui/items/EditDiscountScreenModel;
    .locals 12

    .line 250
    new-instance v11, Lcom/squareup/ui/items/EditDiscountScreenModel;

    iget-object v1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    iget-object v2, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    iget-object v3, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRuleProducts:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRuleDiscountedProducts:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v6, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->shortDateFormat:Ljava/text/DateFormat;

    iget-object v7, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->timeFormat:Ljava/text/DateFormat;

    iget-object v8, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->localizer:Lcom/squareup/shared/i18n/Localizer;

    iget-object v9, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->res:Lcom/squareup/util/Res;

    const/4 v10, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/items/EditDiscountScreenModel;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/shared/catalog/utils/InflatedPricingRule;Ljava/util/List;Ljava/util/List;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditDiscountScreenModel$1;)V

    return-object v11
.end method

.method public setDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-object p0
.end method

.method public setPricingRule(Lcom/squareup/shared/catalog/utils/InflatedPricingRule;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRule:Lcom/squareup/shared/catalog/utils/InflatedPricingRule;

    return-object p0
.end method

.method public setPricingRuleDiscountedProducts(Ljava/util/List;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;)",
            "Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;"
        }
    .end annotation

    .line 245
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRuleDiscountedProducts:Ljava/util/List;

    return-object p0
.end method

.method public setPricingRuleProducts(Ljava/util/List;)Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/EditDiscountHydratedProduct;",
            ">;)",
            "Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;"
        }
    .end annotation

    .line 239
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreenModel$Builder;->pricingRuleProducts:Ljava/util/List;

    return-object p0
.end method
