.class public final Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "EditItemCategorySelectionScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final stateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->stateProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/ui/items/EditItemState;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/ui/items/EditItemState;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")",
            "Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;-><init>(Ljavax/inject/Provider;Lcom/squareup/ui/items/EditItemState;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->stateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditItemState;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflow/Flow;

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/ui/items/EditItemState;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen_Presenter_Factory;->get()Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
