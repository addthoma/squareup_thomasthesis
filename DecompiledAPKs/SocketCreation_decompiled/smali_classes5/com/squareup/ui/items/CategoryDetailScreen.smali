.class public final Lcom/squareup/ui/items/CategoryDetailScreen;
.super Lcom/squareup/ui/items/InItemsAppletScope;
.source "CategoryDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/CategoryDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/CategoryDetailScreen$Component;,
        Lcom/squareup/ui/items/CategoryDetailScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATE_ITEM_BUTTON_POSITION:I = 0x0

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/CategoryDetailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final EDIT_CATEGORY_BUTTON_POSITION:I = 0x1


# instance fields
.field final categoryId:Ljava/lang/String;

.field final categoryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 230
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$CategoryDetailScreen$2XMdrZ1-thn3IUnke3ndJ65S9Bs;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$CategoryDetailScreen$2XMdrZ1-thn3IUnke3ndJ65S9Bs;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/CategoryDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/ui/items/InItemsAppletScope;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/items/CategoryDetailScreen;->categoryId:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/items/CategoryDetailScreen;->categoryName:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/CategoryDetailScreen;
    .locals 2

    .line 231
    new-instance v0, Lcom/squareup/ui/items/CategoryDetailScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/items/CategoryDetailScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 226
    iget-object p2, p0, Lcom/squareup/ui/items/CategoryDetailScreen;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 227
    iget-object p2, p0, Lcom/squareup/ui/items/CategoryDetailScreen;->categoryName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ITEMS_APPLET_EDIT_CATEGORY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 66
    const-class v0, Lcom/squareup/ui/items/ItemsAppletSection$Categories;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 234
    sget v0, Lcom/squareup/itemsapplet/R$layout;->category_detail_view:I

    return v0
.end method
