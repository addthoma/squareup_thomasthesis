.class Lcom/squareup/ui/NfcProcessor$2;
.super Ljava/lang/Object;
.source "NfcProcessor.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/NfcProcessor;->startMonitoringWithAutoFieldRestart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/NfcProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/NfcProcessor;)V
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/ui/NfcProcessor$2;->this$0:Lcom/squareup/ui/NfcProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public contactlessReaderAdded(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    .line 323
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor$2;->this$0:Lcom/squareup/ui/NfcProcessor;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/NfcProcessor;->access$300(Lcom/squareup/ui/NfcProcessor;Z)V

    return-void
.end method

.method public contactlessReaderReadyForPayment(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderRemoved(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    return-void
.end method

.method public contactlessReaderTimedOut(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    .line 319
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor$2;->this$0:Lcom/squareup/ui/NfcProcessor;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/NfcProcessor;->access$300(Lcom/squareup/ui/NfcProcessor;Z)V

    return-void
.end method
