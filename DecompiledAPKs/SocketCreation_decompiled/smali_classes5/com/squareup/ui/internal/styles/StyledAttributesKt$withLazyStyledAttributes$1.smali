.class public final Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StyledAttributes.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/internal/styles/StyledAttributesKt;->withLazyStyledAttributes(Landroid/content/Context;[IILkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Landroid/content/res/TypedArray;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStyledAttributes.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StyledAttributes.kt\ncom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1\n*L\n1#1,172:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Landroid/content/res/TypedArray;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $attrs:[I

.field final synthetic $defStyleAttr:I

.field final synthetic $this_withLazyStyledAttributes:Landroid/content/Context;

.field final synthetic $typedArrayInstance:Lkotlin/jvm/internal/Ref$ObjectRef;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkotlin/jvm/internal/Ref$ObjectRef;[II)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$this_withLazyStyledAttributes:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$typedArrayInstance:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p3, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$attrs:[I

    iput p4, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$defStyleAttr:I

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Landroid/content/res/TypedArray;
    .locals 5

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$typedArrayInstance:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Landroid/content/res/TypedArray;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$this_withLazyStyledAttributes:Landroid/content/Context;

    const/4 v1, 0x0

    .line 165
    iget-object v2, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$attrs:[I

    .line 166
    iget v3, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$defStyleAttr:I

    const/4 v4, 0x0

    .line 163
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->$typedArrayInstance:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object v0, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    const-string v1, "obtainStyledAttributes(\n\u2026typedArrayInstance = it }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/internal/styles/StyledAttributesKt$withLazyStyledAttributes$1;->invoke()Landroid/content/res/TypedArray;

    move-result-object v0

    return-object v0
.end method
