.class public Lcom/squareup/ui/FileViewerErrorDialog;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "FileViewerErrorDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/FileViewerErrorDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/FileViewerErrorDialog$Factory;,
        Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/FileViewerErrorDialog;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final errorType:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 90
    sget-object v0, Lcom/squareup/ui/-$$Lambda$FileViewerErrorDialog$2Xhl2MGyxEeY6i-IAPshYfMhvVA;->INSTANCE:Lcom/squareup/ui/-$$Lambda$FileViewerErrorDialog$2Xhl2MGyxEeY6i-IAPshYfMhvVA;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/FileViewerErrorDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/FileViewerErrorDialog;->errorType:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/FileViewerErrorDialog;
    .locals 1

    .line 91
    new-instance v0, Lcom/squareup/ui/FileViewerErrorDialog;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->valueOf(Ljava/lang/String;)Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/FileViewerErrorDialog;-><init>(Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 34
    iget-object p2, p0, Lcom/squareup/ui/FileViewerErrorDialog;->errorType:Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;

    invoke-virtual {p2}, Lcom/squareup/ui/FileViewerErrorDialog$FileErrorType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
