.class public Lcom/squareup/ui/DialogDestroyer;
.super Ljava/lang/Object;
.source "DialogDestroyer.java"


# static fields
.field private static final SERVICE:Ljava/lang/String; = "dialog-janitor"


# instance fields
.field private final dialogs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/app/Dialog;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/DialogDestroyer;->dialogs:Ljava/util/Map;

    return-void
.end method

.method public static get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;
    .locals 1

    const-string v0, "dialog-janitor"

    .line 23
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/DialogDestroyer;

    return-object p0
.end method

.method public static isSystemService(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "dialog-janitor"

    .line 18
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public dismiss(Landroid/app/Dialog;)V
    .locals 1

    .line 55
    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/DialogDestroyer;->dialogs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/DialogDestroyer;->dialogs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Dialog;

    .line 61
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/DialogDestroyer;->dialogs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public show(Landroid/app/Dialog;)V
    .locals 2

    .line 40
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/DialogDestroyer;->dialogs:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
