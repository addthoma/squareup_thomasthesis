.class public Lcom/squareup/ui/DelayedLoadingProgressBar;
.super Landroid/widget/ProgressBar;
.source "DelayedLoadingProgressBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;
    }
.end annotation


# static fields
.field private static final MIN_VISIBLE_TIME_MS:I = 0x1f4

.field private static final NOT_SHOWN_YET:I = -0x1

.field private static final START_DELAY_MS:I = 0x1f4


# instance fields
.field private callback:Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;

.field private final delayedHide:Ljava/lang/Runnable;

.field private final delayedShow:Ljava/lang/Runnable;

.field private dismissed:Z

.field private postedHide:Z

.field private postedShow:Z

.field private startTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    iput-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->dismissed:Z

    .line 26
    iput-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->postedHide:Z

    .line 27
    iput-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->postedShow:Z

    const-wide/16 p1, -0x1

    .line 28
    iput-wide p1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->startTime:J

    .line 30
    new-instance p1, Lcom/squareup/ui/DelayedLoadingProgressBar$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/DelayedLoadingProgressBar$1;-><init>(Lcom/squareup/ui/DelayedLoadingProgressBar;)V

    iput-object p1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->delayedHide:Ljava/lang/Runnable;

    .line 39
    new-instance p1, Lcom/squareup/ui/-$$Lambda$DelayedLoadingProgressBar$oRHfH05Qd2SGcJpTegmSbEzgR7E;

    invoke-direct {p1, p0}, Lcom/squareup/ui/-$$Lambda$DelayedLoadingProgressBar$oRHfH05Qd2SGcJpTegmSbEzgR7E;-><init>(Lcom/squareup/ui/DelayedLoadingProgressBar;)V

    iput-object p1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->delayedShow:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/ui/DelayedLoadingProgressBar;Z)Z
    .locals 0

    .line 14
    iput-boolean p1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->postedHide:Z

    return p1
.end method

.method static synthetic access$102(Lcom/squareup/ui/DelayedLoadingProgressBar;J)J
    .locals 0

    .line 14
    iput-wide p1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->startTime:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/DelayedLoadingProgressBar;)Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->callback:Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;

    return-object p0
.end method

.method private removeCallbacks()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->delayedHide:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->delayedShow:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public hide()V
    .locals 8

    const/4 v0, 0x1

    .line 72
    iput-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->dismissed:Z

    .line 73
    iget-object v1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->delayedShow:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 74
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->startTime:J

    sub-long/2addr v1, v3

    const-wide/16 v5, -0x1

    cmp-long v7, v3, v5

    if-eqz v7, :cond_1

    const-wide/16 v3, 0x1f4

    cmp-long v5, v1, v3

    if-ltz v5, :cond_0

    goto :goto_0

    .line 83
    :cond_0
    iget-boolean v5, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->postedHide:Z

    if-nez v5, :cond_2

    .line 84
    iget-object v5, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->delayedHide:Ljava/lang/Runnable;

    sub-long/2addr v3, v1

    invoke-virtual {p0, v5, v3, v4}, Lcom/squareup/ui/DelayedLoadingProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 85
    iput-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->postedHide:Z

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x4

    .line 78
    invoke-virtual {p0, v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->callback:Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;->onHide()V

    :cond_2
    :goto_1
    return-void
.end method

.method public synthetic lambda$new$0$DelayedLoadingProgressBar()V
    .locals 3

    const/4 v0, 0x0

    .line 40
    iput-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->postedShow:Z

    .line 41
    iget-boolean v1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->dismissed:Z

    if-nez v1, :cond_0

    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->startTime:J

    .line 43
    invoke-virtual {p0, v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->removeCallbacks()V

    .line 53
    invoke-super {p0}, Landroid/widget/ProgressBar;->onDetachedFromWindow()V

    return-void
.end method

.method public setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->callback:Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;

    return-void
.end method

.method public show()V
    .locals 3

    const-wide/16 v0, -0x1

    .line 96
    iput-wide v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->startTime:J

    const/4 v0, 0x0

    .line 97
    iput-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->dismissed:Z

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->delayedHide:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 99
    iget-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->postedShow:Z

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->delayedShow:Ljava/lang/Runnable;

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/ui/DelayedLoadingProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    .line 101
    iput-boolean v0, p0, Lcom/squareup/ui/DelayedLoadingProgressBar;->postedShow:Z

    :cond_0
    return-void
.end method
