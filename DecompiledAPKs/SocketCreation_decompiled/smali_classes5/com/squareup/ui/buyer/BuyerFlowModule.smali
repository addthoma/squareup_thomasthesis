.class public abstract Lcom/squareup/ui/buyer/BuyerFlowModule;
.super Ljava/lang/Object;
.source "BuyerFlowModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/checkoutflow/receipt/BillReceiptModule;,
        Lcom/squareup/checkoutflow/receipttutorial/ReceiptTutorialModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindErrorIntoBuyer(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Lcom/squareup/ui/buyer/ForBuyer;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindReceiptForLastPayment(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/ReceiptForLastPayment;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindTipIntoBuyer(Lcom/squareup/checkoutflow/core/tip/TipViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Lcom/squareup/ui/buyer/ForBuyer;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindTipIntoMainActivity(Lcom/squareup/checkoutflow/core/tip/TipViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideBillReceiptWorkflow(Lcom/squareup/ui/buyer/receipt/RealBillReceiptWorkflow;)Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBillTipWorkflow(Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;)Lcom/squareup/ui/buyer/tip/BillTipWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBuyerFlowReceiptManager(Lcom/squareup/ui/buyer/receiptlegacy/RealBuyerFlowReceiptManager;)Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBuyerFlowStarter(Lcom/squareup/ui/buyer/RealBuyerFlowStarter;)Lcom/squareup/ui/buyer/BuyerFlowStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBuyerFlowWorkflowRunner(Lcom/squareup/ui/buyer/workflow/RealBuyerFlowWorkflowRunner;)Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmvPaymentStarter(Lcom/squareup/ui/buyer/emv/RealEmvPaymentStarter;)Lcom/squareup/ui/buyer/emv/EmvPaymentStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideFormattedTotalProvider(Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)Lcom/squareup/ui/buyer/FormattedTotalProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
