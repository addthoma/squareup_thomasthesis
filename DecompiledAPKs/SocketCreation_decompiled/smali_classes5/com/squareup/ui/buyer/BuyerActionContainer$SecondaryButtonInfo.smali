.class public final Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;
.super Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;
.source "BuyerActionContainer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/BuyerActionContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SecondaryButtonInfo"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/BuyerActionContainer$SecondaryButtonInfo;",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;",
        "title",
        "Lcom/squareup/util/ViewString;",
        "command",
        "Lkotlin/Function0;",
        "",
        "(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 208
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;Lcom/squareup/util/ViewString;Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;)V

    return-void
.end method
