.class public final Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;
.super Ljava/lang/Object;
.source "TipReaderHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/tip/TipReaderHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultEmvCardInsertRemoveProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->defaultEmvCardInsertRemoveProcessorProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;)",
            "Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;"
        }
    .end annotation

    .line 65
    new-instance v8, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)Lcom/squareup/ui/buyer/tip/TipReaderHandler;
    .locals 9

    .line 73
    new-instance v8, Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/buyer/tip/TipReaderHandler;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/tip/TipReaderHandler;
    .locals 8

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->defaultEmvCardInsertRemoveProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/tip/TipReaderHandler_Factory;->get()Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    move-result-object v0

    return-object v0
.end method
