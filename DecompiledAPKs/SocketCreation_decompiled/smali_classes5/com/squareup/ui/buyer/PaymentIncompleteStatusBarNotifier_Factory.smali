.class public final Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;
.super Ljava/lang/Object;
.source "PaymentIncompleteStatusBarNotifier_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->appContextProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->notificationWrapperProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Landroid/app/NotificationManager;Landroid/app/Application;Lcom/squareup/analytics/Analytics;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;
    .locals 7

    .line 55
    new-instance v6, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;-><init>(Landroid/app/NotificationManager;Landroid/app/Application;Lcom/squareup/analytics/Analytics;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;
    .locals 5

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->appContextProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v3, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->notificationWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/notification/NotificationWrapper;

    iget-object v4, p0, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/AppNameFormatter;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->newInstance(Landroid/app/NotificationManager;Landroid/app/Application;Lcom/squareup/analytics/Analytics;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier_Factory;->get()Lcom/squareup/ui/buyer/PaymentIncompleteStatusBarNotifier;

    move-result-object v0

    return-object v0
.end method
