.class Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "EmvPaymentTakingWarningWorkflow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;->handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow$1;->this$0:Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow$1;->this$0:Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;->access$000(Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;)Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cancelPayment()V

    return-void
.end method
