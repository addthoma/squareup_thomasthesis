.class public final Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;
.super Ljava/lang/Object;
.source "PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/tender/BaseCardTender;",
        ">;"
    }
.end annotation


# instance fields
.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLastAddedTender(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/tender/BaseCardTender;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen$Module;->provideLastAddedTender(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/tender/BaseCardTender;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/tender/BaseCardTender;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/tender/BaseCardTender;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;->provideLastAddedTender(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/tender/BaseCardTender;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen_Module_ProvideLastAddedTenderFactory;->get()Lcom/squareup/payment/tender/BaseCardTender;

    move-result-object v0

    return-object v0
.end method
