.class public final synthetic Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

.field private final synthetic f$1:Lcom/squareup/payment/BillPayment;

.field private final synthetic f$2:Lcom/squareup/protos/common/Money;

.field private final synthetic f$3:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/buyer/RealBuyerFlowStarter;Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;->f$0:Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

    iput-object p2, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;->f$1:Lcom/squareup/payment/BillPayment;

    iput-object p3, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;->f$2:Lcom/squareup/protos/common/Money;

    iput-object p4, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;->f$3:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;->f$0:Lcom/squareup/ui/buyer/RealBuyerFlowStarter;

    iget-object v1, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;->f$1:Lcom/squareup/payment/BillPayment;

    iget-object v2, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;->f$2:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/ui/buyer/-$$Lambda$RealBuyerFlowStarter$S4zly6lIFGle5fBSUD5v4-Aossk;->f$3:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/ui/buyer/RealBuyerFlowStarter;->lambda$startEmoneyBuyerFlow$2$RealBuyerFlowStarter(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lflow/History;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
