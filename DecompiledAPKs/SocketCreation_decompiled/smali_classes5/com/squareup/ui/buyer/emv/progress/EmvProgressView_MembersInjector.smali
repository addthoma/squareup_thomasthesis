.class public final Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;
.super Ljava/lang/Object;
.source "EmvProgressView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectBuyerLocaleOverride(Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;->presenter:Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;->injectPresenter(Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;Lcom/squareup/ui/buyer/emv/progress/AbstractEmvProgressPresenter;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;->injectBuyerLocaleOverride(Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/progress/EmvProgressView_MembersInjector;->injectMembers(Lcom/squareup/ui/buyer/emv/progress/EmvProgressView;)V

    return-void
.end method
