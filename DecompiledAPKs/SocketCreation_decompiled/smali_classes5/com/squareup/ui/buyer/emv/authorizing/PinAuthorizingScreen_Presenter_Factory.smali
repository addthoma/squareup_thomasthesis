.class public final Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "PinAuthorizingScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerAmountTextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final emvRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerAmountTextProvider;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/NfcProcessor;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/TenderInEdit;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->buyerAmountTextProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen_Presenter_Factory;->get()Lcom/squareup/ui/buyer/emv/authorizing/PinAuthorizingScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
