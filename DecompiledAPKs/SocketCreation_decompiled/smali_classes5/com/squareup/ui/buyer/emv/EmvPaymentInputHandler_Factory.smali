.class public final Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;
.super Ljava/lang/Object;
.source "EmvPaymentInputHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final dippedCardTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final emvRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final readerHudManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueNavigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;)V"
        }
    .end annotation

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p4, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p5, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p6, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p7, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p8, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p9, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p10, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p11, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p12, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->readerIssueSinkProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p13, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->readerIssueNavigatorProvider:Ljavax/inject/Provider;

    .line 85
    iput-object p14, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    .line 86
    iput-object p15, p0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/swipe/SwipeValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;"
        }
    .end annotation

    .line 106
    new-instance v16, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static newInstance(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/giftcard/GiftCards;)Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;
    .locals 17

    .line 116
    new-instance v16, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;-><init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/giftcard/GiftCards;)V

    return-object v16
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;
    .locals 17

    move-object/from16 v0, p0

    .line 91
    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/NfcProcessor;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->emvRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->readerHudManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/cardreader/dipper/ReaderHudManager;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->dippedCardTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/cardreader/DippedCardTracker;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->readerIssueSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->readerIssueNavigatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->swipeValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/swipe/SwipeValidator;

    iget-object v1, v0, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/giftcard/GiftCards;

    invoke-static/range {v2 .. v16}, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->newInstance(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/giftcard/GiftCards;)Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler_Factory;->get()Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;

    move-result-object v0

    return-object v0
.end method
