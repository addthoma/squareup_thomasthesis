.class public Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;
.super Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;
.source "PaymentScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DipRequiredFallbackHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 114
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)V

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 1

    .line 118
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_SWIPE_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 119
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_must_dip_title:I

    .line 120
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_must_dip_msg:I

    .line 121
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;->cancelPaymentButton()Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$DipRequiredFallbackHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->goToPreparingPaymentScreen()V

    return-void
.end method
