.class public Lcom/squareup/ui/buyer/emv/EmvScope$ComponentFactory;
.super Ljava/lang/Object;
.source "EmvScope.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 526
    const-class v0, Lcom/squareup/ui/buyer/BuyerScopeComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/BuyerScopeComponent;

    .line 527
    check-cast p2, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 528
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvScope$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/buyer/emv/EmvScope$Module;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    .line 529
    invoke-interface {p1, v0}, Lcom/squareup/ui/buyer/BuyerScopeComponent;->emvPath(Lcom/squareup/ui/buyer/emv/EmvScope$Module;)Lcom/squareup/ui/buyer/emv/EmvScope$Component;

    move-result-object p1

    return-object p1
.end method
