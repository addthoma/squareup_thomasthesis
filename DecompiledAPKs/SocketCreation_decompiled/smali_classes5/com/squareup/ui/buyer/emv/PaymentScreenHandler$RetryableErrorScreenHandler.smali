.class public Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;
.super Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;
.source "PaymentScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RetryableErrorScreenHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 136
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)V

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 1

    .line 140
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 141
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->connection_error_title:I

    .line 142
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/ui/buyerflow/R$string;->connection_error_message:I

    .line 143
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;->retryPaymentButton()Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 145
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 149
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->cancelPayment()V

    return-void
.end method

.method protected retryPaymentButton()Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 3

    .line 153
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    sget v1, Lcom/squareup/common/strings/R$string;->retry:I

    new-instance v2, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler$1;-><init>(Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$RetryableErrorScreenHandler;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method
