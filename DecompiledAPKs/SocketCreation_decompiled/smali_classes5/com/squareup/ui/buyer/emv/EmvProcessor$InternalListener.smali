.class Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;
.super Ljava/lang/Object;
.source "EmvProcessor.java"

# interfaces
.implements Lcom/squareup/cardreader/EmvListener;
.implements Lcom/squareup/cardreader/PaymentCompletionListener;
.implements Lcom/squareup/cardreader/PinRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/EmvProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V
    .locals 0

    .line 594
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/ui/buyer/emv/EmvProcessor$1;)V
    .locals 0

    .line 594
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;-><init>(Lcom/squareup/ui/buyer/emv/EmvProcessor;)V

    return-void
.end method

.method private onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 1

    .line 763
    sget-object v0, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method


# virtual methods
.method public onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 659
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p3

    new-array p3, p3, [Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-interface {p1, p3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    invoke-interface {p2, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onListAccounts([Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V

    return-void
.end method

.method public onCardError()V
    .locals 5

    .line 597
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onCardError()V

    .line 598
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 599
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$600(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_ERROR:Lcom/squareup/analytics/ReaderEventName;

    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 600
    invoke-static {v4}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 599
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 5

    .line 604
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Skipping EmvListener#onCardRemovedDuringPayment because isCardPresenceRequired is false"

    .line 605
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 609
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onCardRemovedDuringPayment()V

    .line 610
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 611
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$600(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_REMOVED_DURING_PAYMENT:Lcom/squareup/analytics/ReaderEventName;

    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 612
    invoke-static {v4}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 611
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 0

    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 0

    .line 672
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 4

    .line 652
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onListApplications([Lcom/squareup/cardreader/EmvApplication;)V

    .line 653
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$600(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_APPLICATION_SELECTION:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 654
    invoke-static {v3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 653
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public onMagFallbackSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 4

    .line 645
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$800(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/hudtoaster/HudToaster;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {p1, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    .line 647
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$600(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 648
    invoke-static {v3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 647
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public onMagFallbackSwipeSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 3

    .line 636
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 637
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 638
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onMagSwipeApproved()V

    .line 639
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->submitFallbackSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    .line 640
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$600(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p2

    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    sget-object v1, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 641
    invoke-static {v2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 640
    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    xor-int/lit8 p3, p4, 0x1

    const-string p4, "Offline approval for dip transactions is not implemented"

    .line 704
    invoke-static {p3, p4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 706
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelAuthTimeoutRunner()V

    .line 707
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p4

    invoke-static {p3, p4}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1100(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderId;)V

    .line 709
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p3

    iget-object p4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p4}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;

    move-result-object p4

    invoke-interface {p4}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 710
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 711
    invoke-static {p3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object p3

    .line 712
    invoke-virtual {p3, p2}, Lcom/squareup/payment/tender/SmartCardTender;->setSmartCardCaptureArgs([B)V

    .line 714
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onPaymentApproved()V

    .line 715
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatus(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 716
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2, p1, p5}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1300(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 2

    .line 733
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 734
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    .line 736
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$700(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 737
    invoke-static {p3}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p2

    goto :goto_0

    .line 739
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object p2

    .line 740
    invoke-virtual {p2, p3}, Lcom/squareup/payment/tender/SmartCardTender;->getDeclinedMessageResources(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p2

    .line 742
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object p3

    const/4 v0, 0x1

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {p3, v0, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 743
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelAuthTimeoutRunner()V

    .line 745
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-static {p3, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1100(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderId;)V

    .line 746
    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p3

    invoke-interface {p3, p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onPaymentDeclined(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)V

    .line 747
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatus(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 748
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2, p1, p4}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1400(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 2

    .line 721
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 722
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object p2

    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v1, 0x1

    invoke-virtual {p2, v1, v0}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 723
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->cancelAuthTimeoutRunner()V

    .line 725
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1100(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderId;)V

    .line 726
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p2

    invoke-static {p3}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p3

    invoke-interface {p2, p3}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onPaymentDeclined(Lcom/squareup/cardreader/StandardMessageResources$MessageResources;)V

    .line 727
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatus(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 728
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2, p1, p4}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1400(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 753
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1, p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1500(Lcom/squareup/ui/buyer/emv/EmvProcessor;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method

.method public onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 758
    sget-object p2, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method

.method public onSigRequested()V
    .locals 1

    .line 667
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireSmartCardTenderInFlight()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTender;->setSignatureRequested()V

    return-void
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V
    .locals 1

    .line 678
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result p1

    const-string v0, "CancelTest::onSoftwarePinRequested for a reader other than the active reader"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 680
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->onPinPadReset()V

    .line 681
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V

    return-void
.end method

.method public onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 4

    .line 623
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$700(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setFallbackType(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    .line 624
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 625
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onSwipeChipCardForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    .line 627
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->TECHNICAL:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_TECHNICAL_FALLBACK:Lcom/squareup/analytics/ReaderEventName;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_SCHEME_FALLBACK:Lcom/squareup/analytics/ReaderEventName;

    .line 630
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$600(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 631
    invoke-static {v3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 630
    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public onUseChipCardDuringFallback()V
    .locals 5

    .line 616
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$100(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor$Listener;->onUseChipCardDuringFallback()V

    .line 617
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$300(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$200(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 618
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$600(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_USE_CHIP_CARD:Lcom/squareup/analytics/ReaderEventName;

    sget-object v3, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v4, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 619
    invoke-static {v4}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 618
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 687
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$400(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/CardReaderInfo;->setCardPresenceRequired(Z)V

    .line 689
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$900(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->getResult()Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;->NFC_SUCCESS:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Result;

    if-eq p2, v0, :cond_0

    .line 690
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$700(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/TenderInEdit;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p2

    .line 691
    invoke-virtual {p3}, Lcom/squareup/cardreader/CardInfo;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardholderName(Ljava/lang/String;)V

    .line 694
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$700(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/TenderInEdit;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    .line 696
    invoke-static {p3}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$1000(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Z

    move-result p3

    if-eqz p3, :cond_1

    sget-object p3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    goto :goto_0

    :cond_1
    sget-object p3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 695
    :goto_0
    invoke-virtual {p2, p3, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardAuthRequestData(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;[B)V

    .line 698
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvProcessor$InternalListener;->this$0:Lcom/squareup/ui/buyer/emv/EmvProcessor;

    invoke-static {p1}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->access$500(Lcom/squareup/ui/buyer/emv/EmvProcessor;)Lcom/squareup/payment/Transaction;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/emv/EmvProcessor;->authorizeSmartCardTender(Lcom/squareup/payment/BillPayment;)V

    return-void
.end method
