.class public final Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;
.super Lcom/squareup/ui/buyer/emv/InEmvScope;
.source "PinPadDialogScreen.java"

# interfaces
.implements Lcom/squareup/ui/buyer/PaymentExempt;
.implements Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogCardScreen;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$Component;
.end annotation

.annotation runtime Lcom/squareup/ui/buyer/RequiresBuyerInteraction;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;,
        Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$PinPadDialogScreen$1yU71koQ8dpA5pgiCr2woapFyZo;->INSTANCE:Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$PinPadDialogScreen$1yU71koQ8dpA5pgiCr2woapFyZo;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope;ZLcom/squareup/cardreader/CardInfo;ZZ)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/emv/InEmvScope;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    .line 33
    new-instance p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    invoke-direct {p1, p3, p2, p4, p5}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;-><init>(Lcom/squareup/cardreader/CardInfo;ZZZ)V

    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;->params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;
    .locals 7

    .line 68
    const-class v0, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/buyer/emv/EmvScope;

    .line 69
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 70
    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v3, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    .line 71
    :goto_1
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    if-ne p0, v3, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    .line 72
    :goto_2
    new-instance p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;

    const/4 v4, 0x0

    move-object v1, p0

    move v3, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;ZLcom/squareup/cardreader/CardInfo;ZZ)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 60
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/buyer/emv/InEmvScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;->emvPath:Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 62
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;->params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    iget-boolean p2, p2, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->canSkip:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;->params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    iget-boolean p2, p2, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->isFinalRetry:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget-object p2, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;->params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    iget-boolean p2, p2, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;->isRetry:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public params()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;->params:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    return-object v0
.end method
