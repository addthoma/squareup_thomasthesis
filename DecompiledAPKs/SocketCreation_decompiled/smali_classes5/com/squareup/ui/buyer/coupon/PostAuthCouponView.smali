.class public Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;
.super Landroid/widget/LinearLayout;
.source "PostAuthCouponView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private cancelButton:Lcom/squareup/marketfont/MarketButton;

.field private claimButton:Lcom/squareup/widgets/ScalingTextView;

.field private content:Landroid/widget/LinearLayout;

.field private final minButtonHeight:I

.field presenter:Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ribbon:Lcom/squareup/ui/CouponRibbonView;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->minButtonHeight:I

    .line 47
    const-class p2, Lcom/squareup/ui/buyer/coupon/PostAuthCouponScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/coupon/PostAuthCouponScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponScreen$Component;->inject(Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 113
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 114
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->coupon_redeem_claim_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->claimButton:Lcom/squareup/widgets/ScalingTextView;

    .line 115
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->coupon_redeem_cancel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->cancelButton:Lcom/squareup/marketfont/MarketButton;

    .line 116
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->coupon_redeem_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->title:Landroid/widget/TextView;

    .line 117
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->coupon_ribbon:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/CouponRibbonView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->ribbon:Lcom/squareup/ui/CouponRibbonView;

    .line 118
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->coupon_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->content:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 92
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$onFinishInflate$0$PostAuthCouponView()Lkotlin/Unit;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->presenter:Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->onBackPressed()Z

    .line 55
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$1$PostAuthCouponView(Landroid/view/View;II)V
    .locals 0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->claimButton:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {p1}, Lcom/squareup/widgets/ScalingTextView;->getMeasuredHeight()I

    move-result p1

    iget p2, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->minButtonHeight:I

    if-ge p1, p2, :cond_0

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->content:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result p1

    iget-object p2, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->content:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result p2

    add-int/2addr p1, p2

    .line 74
    iget p2, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->minButtonHeight:I

    iget-object p3, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->claimButton:Lcom/squareup/widgets/ScalingTextView;

    invoke-static {p3}, Lcom/squareup/util/Views;->getVerticalMargins(Landroid/view/View;)I

    move-result p3

    add-int/2addr p2, p3

    add-int/2addr p1, p2

    .line 76
    iget-object p2, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->title:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getLineHeight()I

    move-result p2

    iget-object p3, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->title:Landroid/widget/TextView;

    invoke-static {p3}, Lcom/squareup/util/Views;->getVerticalMargins(Landroid/view/View;)I

    move-result p3

    add-int/2addr p2, p3

    add-int/2addr p1, p2

    .line 77
    iget-object p2, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->content:Landroid/widget/LinearLayout;

    .line 78
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p2

    int-to-float p2, p2

    int-to-float p1, p1

    sub-float/2addr p2, p1

    iget-object p1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->ribbon:Lcom/squareup/ui/CouponRibbonView;

    invoke-virtual {p1}, Lcom/squareup/ui/CouponRibbonView;->getMeasuredHeight()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p2, p1

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->ribbon:Lcom/squareup/ui/CouponRibbonView;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/CouponRibbonView;->growBy(F)V

    :cond_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->presenter:Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->presenter:Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->dropView(Ljava/lang/Object;)V

    .line 88
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 51
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->bindViews()V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/ui/buyer/coupon/-$$Lambda$PostAuthCouponView$Qi5QTYmN0hPZJTg-eMHtK2UxJto;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/coupon/-$$Lambda$PostAuthCouponView$Qi5QTYmN0hPZJTg-eMHtK2UxJto;-><init>(Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->claimButton:Lcom/squareup/widgets/ScalingTextView;

    new-instance v1, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView$1;-><init>(Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->cancelButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView$2;-><init>(Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    new-instance v0, Lcom/squareup/ui/buyer/coupon/-$$Lambda$PostAuthCouponView$gp3PwHctIc5d3Cq5tZheNhtXJ28;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/coupon/-$$Lambda$PostAuthCouponView$gp3PwHctIc5d3Cq5tZheNhtXJ28;-><init>(Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->presenter:Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setRibbonValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->ribbon:Lcom/squareup/ui/CouponRibbonView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/CouponRibbonView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 104
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    .line 105
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method
