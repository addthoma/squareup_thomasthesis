.class public Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;
.super Lmortar/ViewPresenter;
.source "PostAuthCouponPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

.field private final buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field private final coupon:Lcom/squareup/protos/client/coupons/Coupon;

.field private final formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/ui/buyer/BuyerAmountTextProvider;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    .line 24
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->getAvailableCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 25
    iput-object p3, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    .line 26
    iput-object p4, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    return-void
.end method


# virtual methods
.method claimCoupon()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->claimCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V

    return-void
.end method

.method doNotClaimCoupon()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->finishCouponScreen()V

    return-void
.end method

.method onBackPressed()Z
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->buyerScopeRunner:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->confirmCancelPayment(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedTotalAmount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->setTotal(Ljava/lang/CharSequence;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->buyerAmountTextProvider:Lcom/squareup/ui/buyer/BuyerAmountTextProvider;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/BuyerAmountTextProvider;->getFormattedAmountDueAutoGratuityAndTip()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    iget-object v1, p0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponPresenter;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v1, v1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatShortestDescription(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponView;->setRibbonValue(Ljava/lang/CharSequence;)V

    return-void
.end method
