.class public abstract Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;
.super Ljava/lang/Object;
.source "BuyerActionContainer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/BuyerActionContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ButtonInfo"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008&\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;",
        "",
        "title",
        "Lcom/squareup/util/ViewString;",
        "command",
        "Lkotlin/Function0;",
        "",
        "subtitle",
        "titleSize",
        "Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;",
        "(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;Lcom/squareup/util/ViewString;Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;)V",
        "getCommand",
        "()Lkotlin/jvm/functions/Function0;",
        "getSubtitle",
        "()Lcom/squareup/util/ViewString;",
        "getTitle",
        "getTitleSize",
        "()Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final command:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final subtitle:Lcom/squareup/util/ViewString;

.field private final title:Lcom/squareup/util/ViewString;

.field private final titleSize:Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;


# direct methods
.method public constructor <init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;Lcom/squareup/util/ViewString;Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/util/ViewString;",
            "Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->title:Lcom/squareup/util/ViewString;

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->command:Lkotlin/jvm/functions/Function0;

    iput-object p3, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->subtitle:Lcom/squareup/util/ViewString;

    iput-object p4, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->titleSize:Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;

    return-void
.end method


# virtual methods
.method public final getCommand()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->command:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getSubtitle()Lcom/squareup/util/ViewString;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->subtitle:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final getTitle()Lcom/squareup/util/ViewString;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->title:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final getTitleSize()Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerActionContainer$ButtonInfo;->titleSize:Lcom/squareup/ui/buyer/BuyerActionContainer$TitleSize;

    return-object v0
.end method
