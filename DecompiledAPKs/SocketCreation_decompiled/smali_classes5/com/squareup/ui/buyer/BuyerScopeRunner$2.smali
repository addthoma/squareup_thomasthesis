.class Lcom/squareup/ui/buyer/BuyerScopeRunner$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "BuyerScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/BuyerScopeRunner;->goToFirstCrmScreen(Lcom/squareup/payment/PaymentReceipt;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/BuyerScopeRunner;

.field final synthetic val$receipt:Lcom/squareup/payment/PaymentReceipt;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/PaymentReceipt;)V
    .locals 0

    .line 703
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner$2;->this$0:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner$2;->val$receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 3

    .line 707
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner$2;->this$0:Lcom/squareup/ui/buyer/BuyerScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->access$000(Lcom/squareup/ui/buyer/BuyerScopeRunner;)Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeRunner$2;->val$receipt:Lcom/squareup/payment/PaymentReceipt;

    invoke-static {v1, v2}, Lcom/squareup/ui/crm/flow/CrmScope;->firstPostTransactionCrmScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/ui/crm/flow/InCrmScope;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/BuyerScopeRunner;->access$100(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method
