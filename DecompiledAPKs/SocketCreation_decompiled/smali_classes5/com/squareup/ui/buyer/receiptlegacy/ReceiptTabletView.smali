.class public Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;
.super Landroid/widget/FrameLayout;
.source "ReceiptTabletView.java"

# interfaces
.implements Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/ui/buyer/ForceOutgoingViewOnTop;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

.field private clickToEnd:Z

.field private contentFrame:Landroid/view/ViewGroup;

.field curatedImage:Lcom/squareup/merchantimages/CuratedImage;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private emailDisclaimer:Lcom/squareup/widgets/MessageView;

.field private emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

.field private emailSend:Landroid/widget/TextView;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private glyphSubtitle:Landroid/widget/TextView;

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private merchantImage:Landroid/widget/ImageView;

.field private noReceiptButton:Landroid/widget/TextView;

.field private onNewSaleClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private postReceiptFrame:Landroid/view/ViewGroup;

.field presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private printButton:Landroid/widget/TextView;

.field private printFormalReceiptButton:Landroid/view/View;

.field private receiptHint:Lcom/squareup/widgets/MessageView;

.field private receiptOptionsFrame:Landroid/view/ViewGroup;

.field private rect:Landroid/graphics/Rect;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private smsContainer:Landroid/view/View;

.field private smsDisclaimer:Lcom/squareup/widgets/MessageView;

.field private smsInput:Lcom/squareup/widgets/SelectableEditText;

.field private smsSend:Landroid/widget/TextView;

.field private switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 110
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->rect:Landroid/graphics/Rect;

    .line 111
    const-class p2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$TabletComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$TabletComponent;

    invoke-interface {p1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy$TabletComponent;->inject(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)Lcom/squareup/widgets/MessageView;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->receiptHint:Lcom/squareup/widgets/MessageView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)Landroid/view/ViewGroup;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->contentFrame:Landroid/view/ViewGroup;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)Landroid/widget/TextView;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->noReceiptButton:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    return-object p0
.end method

.method private clickedNewSale(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->rect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getNewSaleButtonRect(Landroid/graphics/Rect;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->rect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {v0, v1, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    return p1
.end method

.method private clickedSaveCard(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->rect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getSaveCardButtonRect(Landroid/graphics/Rect;)V

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->rect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {v0, v1, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    return p1
.end method

.method private initializeEmailInput()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$4;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/text/EmailScrubber;->watcher(Lcom/squareup/text/HasSelectableText;)Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->updateEmailEnabledStates()V

    return-void
.end method

.method private initializeSmsInput()V
    .locals 3

    .line 477
    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$13;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$13;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    .line 483
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private setPostReceiptVisible()V
    .locals 2

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->receiptOptionsFrame:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->postReceiptFrame:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public animateToGlyphState(Lcom/squareup/glyph/GlyphTypeface$Glyph;IZLcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setEnabled(Z)V

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    if-gez p2, :cond_0

    .line 305
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphSubtitle:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 307
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphSubtitle:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 308
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphSubtitle:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p4

    invoke-interface {p4, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    if-eqz p3, :cond_1

    .line 312
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setPostReceiptVisible()V

    goto :goto_1

    .line 314
    :cond_1
    invoke-static {}, Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;

    move-result-object p1

    .line 315
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->contentFrame:Landroid/view/ViewGroup;

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 316
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->receiptOptionsFrame:Landroid/view/ViewGroup;

    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->postReceiptFrame:Landroid/view/ViewGroup;

    const-wide/16 p3, 0x14d

    invoke-static {p1, p2, p3, p4}, Lcom/squareup/util/Views;->crossFade(Landroid/view/View;Landroid/view/View;J)V

    :goto_1
    return-void
.end method

.method public asView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public enableClickAnywhereToFinish()V
    .locals 1

    const/4 v0, 0x1

    .line 284
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->clickToEnd:Z

    return-void
.end method

.method public enableSendEmailButton(Z)V
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailSend:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public enableSendSmsButton(Z)V
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsSend:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public getEmailString()Ljava/lang/String;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSmsString()Ljava/lang/String;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 219
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public isPrintReceiptButtonVisible()Z
    .locals 1

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->printButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$setBackgroundImage$1$ReceiptTabletView(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
    .locals 0

    .line 399
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->merchantImage:Landroid/widget/ImageView;

    new-instance p3, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;

    invoke-direct {p3, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V

    invoke-virtual {p1, p2, p3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    return-void
.end method

.method public synthetic lambda$setInputControlPadding$0$ReceiptTabletView(Landroid/view/View;II)V
    .locals 3

    .line 257
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 258
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-virtual {p2}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->getPaddingLeft()I

    move-result p3

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailSend:Landroid/widget/TextView;

    .line 259
    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    add-int/2addr v1, p1

    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-virtual {v2}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->getPaddingBottom()I

    move-result v2

    .line 258
    invoke-virtual {p2, p3, v0, v1, v2}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setPadding(IIII)V

    .line 260
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsContainer:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result p2

    if-nez p2, :cond_0

    .line 261
    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p2}, Lcom/squareup/widgets/SelectableEditText;->getPaddingLeft()I

    move-result p3

    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsSend:Landroid/widget/TextView;

    .line 262
    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    add-int/2addr p1, v1

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->getPaddingBottom()I

    move-result v1

    .line 261
    invoke-virtual {p2, p3, v0, p1, v1}, Lcom/squareup/widgets/SelectableEditText;->setPadding(IIII)V

    :cond_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->dropView(Lcom/squareup/ui/buyer/receiptlegacy/AbstractReceiptPresenter$AbstractReceiptView;)V

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->merchantImage:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Lcom/squareup/merchantimages/CuratedImage;->cancelRequest(Landroid/widget/ImageView;)V

    .line 186
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 115
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 116
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    .line 118
    sget v0, Lcom/squareup/checkout/R$id;->merchant_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->merchantImage:Landroid/widget/ImageView;

    .line 119
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 120
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->glyph_subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphSubtitle:Landroid/widget/TextView;

    .line 122
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->sms_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsContainer:Landroid/view/View;

    .line 123
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->sms_receipt_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    .line 124
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPEECH_BUBBLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 126
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 127
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v2, v2}, Lcom/squareup/widgets/SelectableEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 130
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->sms_send_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsSend:Landroid/widget/TextView;

    .line 131
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->initializeSmsInput()V

    .line 132
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->sms_disclaimer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsDisclaimer:Lcom/squareup/widgets/MessageView;

    .line 134
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->print_receipt_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->printButton:Landroid/widget/TextView;

    .line 136
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->print_formal_receipt_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->printFormalReceiptButton:Landroid/view/View;

    .line 138
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_receipt_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    .line 139
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 141
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 142
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-virtual {v1, v0, v2, v2, v2}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    new-instance v1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 147
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_send_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailSend:Landroid/widget/TextView;

    .line 148
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->email_disclaimer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailDisclaimer:Lcom/squareup/widgets/MessageView;

    .line 150
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->no_receipt_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->noReceiptButton:Landroid/widget/TextView;

    .line 151
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_send_receipt_digital_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->receiptHint:Lcom/squareup/widgets/MessageView;

    .line 153
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->content_frame:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->contentFrame:Landroid/view/ViewGroup;

    .line 154
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->receipt_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->receiptOptionsFrame:Landroid/view/ViewGroup;

    .line 155
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->post_receipt_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->postReceiptFrame:Landroid/view/ViewGroup;

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->getNewSaleButton()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->onNewSaleClicked:Lrx/Observable;

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$1;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setOnCustomerClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$2;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setOnCustomerAddCardClicked(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 171
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->switch_language_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$3;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->takeView(Ljava/lang/Object;)V

    .line 180
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->initializeEmailInput()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 195
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->clickToEnd:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 196
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->clickedNewSale(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->clickedSaveCard(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->onClickAnywhere()V

    const/4 p1, 0x1

    return p1

    .line 201
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onNewSaleClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 417
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->onNewSaleClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1

    .line 190
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowVisibilityChanged(I)V

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->visibilityChanged(Z)V

    return-void
.end method

.method public resourcesUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 4

    .line 425
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 426
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    sget v2, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    .line 428
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    .line 427
    invoke-virtual {v1, v2, v3, v3, v3}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 429
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailSend:Landroid/widget/TextView;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->buyer_checkout_send_receipt:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsSend:Landroid/widget/TextView;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->buyer_checkout_send_receipt:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 432
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->printButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$7;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$7;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 437
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->printFormalReceiptButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$8;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 443
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailSend:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$9;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$9;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 449
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsSend:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$10;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$10;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 455
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$11;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$11;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 465
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->noReceiptButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$12;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$12;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setAmountSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setBackgroundImage(Lcom/squareup/picasso/RequestCreator;)V
    .locals 2

    .line 397
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->merchantImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 398
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->merchantImage:Landroid/widget/ImageView;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptTabletView$O2sDM4KCKBCvgIa4qux_CPKSxI8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptTabletView$O2sDM4KCKBCvgIa4qux_CPKSxI8;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/picasso/RequestCreator;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method public setDigitalReceiptHint(Ljava/lang/CharSequence;)V
    .locals 2

    .line 372
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->receiptHint:Lcom/squareup/widgets/MessageView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_0

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->receiptHint:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->receiptHint:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public setEmailInputHint(Ljava/lang/String;)V
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailInput:Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setEmailReceiptDisclaimer(Ljava/lang/CharSequence;)V
    .locals 1

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->emailDisclaimer:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setInputControlPadding()V
    .locals 1

    .line 255
    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptTabletView$pxkgJ5Fh_8vETPrjX3iuOyn58Lw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/receiptlegacy/-$$Lambda$ReceiptTabletView$pxkgJ5Fh_8vETPrjX3iuOyn58Lw;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method public setNewSaleText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setNewSaleButtonText(Ljava/lang/String;)V

    return-void
.end method

.method public setNoReceiptText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->noReceiptButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setPostReceiptVisible(ILcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    if-gez p1, :cond_0

    .line 322
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphSubtitle:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphSubtitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->glyphSubtitle:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setPostReceiptVisible()V

    return-void
.end method

.method public setPrintText(Ljava/lang/String;)V
    .locals 1

    .line 491
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->printButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSmsInputHint(Ljava/lang/String;)V
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSmsReceiptDisclaimer(Ljava/lang/CharSequence;)V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsDisclaimer:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSubtitle(ILcom/squareup/util/Res;)V
    .locals 1

    .line 352
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setCallToAction(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setCallToAction(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTicketName(Ljava/lang/CharSequence;)V
    .locals 1

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setTicketName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setTotal(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showBuyerLanguageSelection()V
    .locals 2

    .line 473
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method public showCustomerAddCardButton(Z)V
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showCustomerAddCardButton(Z)V

    return-void
.end method

.method public showCustomerButton(Z)V
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->showCustomerButton(Z)V

    return-void
.end method

.method public showPrintFormalReceiptButton()V
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->printFormalReceiptButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showPrintReceiptButton()V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->printButton:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public showSmsInput(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$5;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$5;-><init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;Lcom/squareup/locale/LocaleOverrideFactory;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 243
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletPresenter;->updateSmsEnabledStates()V

    return-void
.end method

.method public updateEmail(Lcom/squareup/util/Res;)V
    .locals 1

    .line 288
    sget v0, Lcom/squareup/activity/R$string;->receipt_email:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setEmailInputHint(Ljava/lang/String;)V

    return-void
.end method

.method public updateSms(Lcom/squareup/util/Res;)V
    .locals 1

    .line 292
    sget v0, Lcom/squareup/activity/R$string;->receipt_sms:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->setSmsInputHint(Ljava/lang/String;)V

    return-void
.end method
