.class public interface abstract Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;
.super Ljava/lang/Object;
.source "BuyerFlowReceiptManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0016\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u000cH\'J\u001e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\'J\u0008\u0010\u0010\u001a\u00020\u0011H&J\u0008\u0010\u0012\u001a\u00020\nH&J\u0010\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\nH&J\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0016\u001a\u00020\nH&J\u0008\u0010\u0017\u001a\u00020\nH&\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
        "",
        "getSeparatedPrintoutsStartArgsForReceiptSelection",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "selection",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;",
        "uniqueKey",
        "",
        "maybeAutoPrintReceipt",
        "Lio/reactivex/Observable;",
        "",
        "originalReceipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "maybePrintReceipt",
        "renderType",
        "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
        "printOrderStubAndTicket",
        "",
        "shouldReceiptScreenSkipSelection",
        "shouldSkipReceiptScreen",
        "isAnyBuyerFacingScreenShown",
        "willSeparatedPrintoutsLaunchForReceiptSelection",
        "willSeparatedPrintoutsLaunchWithNoPaperReceipt",
        "willSeparatedPrintoutsLaunchWithPaperReceipt",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getSeparatedPrintoutsStartArgsForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
.end method

.method public abstract maybeAutoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract maybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract printOrderStubAndTicket()V
.end method

.method public abstract shouldReceiptScreenSkipSelection()Z
.end method

.method public abstract shouldSkipReceiptScreen(Z)Z
.end method

.method public abstract willSeparatedPrintoutsLaunchForReceiptSelection(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)Z
.end method

.method public abstract willSeparatedPrintoutsLaunchWithNoPaperReceipt()Z
.end method

.method public abstract willSeparatedPrintoutsLaunchWithPaperReceipt()Z
.end method
