.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;
.super Ljava/lang/Object;
.source "ReceiptTabletView.java"

# interfaces
.implements Lcom/squareup/picasso/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->lambda$setBackgroundImage$1(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)V
    .locals 0

    .line 399
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Failed to load curated image"

    .line 411
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onSuccess()V
    .locals 2

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 402
    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 403
    iget-object v1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-static {v1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->access$000(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)Lcom/squareup/widgets/MessageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 404
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-static {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->access$100(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)Landroid/view/ViewGroup;

    move-result-object v0

    sget v1, Lcom/squareup/ui/buyerflow/R$drawable;->panel_background_translucent:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-static {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->access$200(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)Landroid/widget/TextView;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_barely_dark_translucent:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView$6;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;

    invoke-static {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;->access$300(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptTabletView;)Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerActionBar;->setAppearanceForImageBackground()V

    return-void
.end method
