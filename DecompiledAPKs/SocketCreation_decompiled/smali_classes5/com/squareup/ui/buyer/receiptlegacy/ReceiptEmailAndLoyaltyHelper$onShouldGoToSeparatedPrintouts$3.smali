.class final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;
.super Ljava/lang/Object;
.source "ReceiptEmailAndLoyaltyHelper.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onShouldGoToSeparatedPrintouts(Ljava/lang/String;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "receiptSelection",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $transactionUniqueKey:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;->$transactionUniqueKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;
    .locals 4

    const-string/jumbo v0, "transaction.requireRecei\u2026ayment().orderDisplayName"

    const-string/jumbo v1, "transaction.requireReceiptForLastPayment()"

    if-nez p1, :cond_0

    goto :goto_0

    .line 344
    :cond_0
    sget-object v2, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    .line 350
    :goto_0
    sget-object p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->Companion:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;

    .line 351
    iget-object v2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-static {v2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->access$getTransaction$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/payment/Transaction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/payment/PaymentReceipt;->getOrderDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 352
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;->$transactionUniqueKey:Ljava/lang/String;

    .line 350
    invoke-virtual {p1, v1, v0}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;->noPaperReceipt(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    goto :goto_2

    .line 345
    :cond_1
    sget-object v2, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;->Companion:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;

    .line 346
    iget-object v3, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-static {v3}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->access$getTransaction$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/payment/Transaction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/payment/PaymentReceipt;->getOrderDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;->$transactionUniqueKey:Ljava/lang/String;

    .line 348
    sget-object v3, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;->PAPER:Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    if-ne p1, v3, :cond_2

    sget-object p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;->NORMAL:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;

    goto :goto_1

    :cond_2
    sget-object p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;->FORMAL:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;

    .line 345
    :goto_1
    invoke-virtual {v2, v1, v0, p1}, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$Companion;->withPaperReceipt(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput$PaperReceiptType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToSeparatedPrintouts$3;->call(Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelectionType;)Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object p1

    return-object p1
.end method
