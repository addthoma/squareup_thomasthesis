.class final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;
.super Ljava/lang/Object;
.source "ReceiptEmailAndLoyaltyHelper.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onShouldGoToEmailCollectionScreen()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "TT;TT2;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0002*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "shouldShow",
        "<anonymous parameter 1>",
        "",
        "call",
        "(Ljava/lang/Boolean;Ljava/lang/Long;)Ljava/lang/Boolean;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;Ljava/lang/Long;)Ljava/lang/Boolean;
    .locals 0

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Ljava/lang/Boolean;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToEmailCollectionScreen$shouldShowEmailCollection$2;->call(Ljava/lang/Boolean;Ljava/lang/Long;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
