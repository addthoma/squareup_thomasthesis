.class public final Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "CancelNonEmvPaymentScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final authFailed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$EXpqYder4vYAJflnXCF6wBbsrJ8;->INSTANCE:Lcom/squareup/ui/buyer/-$$Lambda$CancelNonEmvPaymentScreen$EXpqYder4vYAJflnXCF6wBbsrJ8;

    .line 69
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Z)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    .line 33
    iput-boolean p2, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;->authFailed:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;)Z
    .locals 0

    .line 27
    iget-boolean p0, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;->authFailed:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;
    .locals 2

    .line 70
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 71
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 72
    :goto_0
    new-instance p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 63
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 65
    iget-boolean p2, p0, Lcom/squareup/ui/buyer/CancelNonEmvPaymentScreen;->authFailed:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
