.class public Lcom/squareup/ui/buyer/BuyerFacingScreensState;
.super Ljava/lang/Object;
.source "BuyerFacingScreensState.java"


# instance fields
.field private buyerFacingScreensShown:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public buyerFacingScreenShown()V
    .locals 1

    const/4 v0, 0x1

    .line 24
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerFacingScreensState;->buyerFacingScreensShown:Z

    return-void
.end method

.method public isAnyBuyerFacingScreenShown()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/BuyerFacingScreensState;->buyerFacingScreensShown:Z

    return v0
.end method
