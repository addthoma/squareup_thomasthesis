.class public final Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "SignLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderTipBar(Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\'\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0007H\u0016\u00a8\u0006\n\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$5",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doTextChanged",
        "",
        "charSequence",
        "",
        "i1",
        "",
        "i2",
        "i3",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res$inlined:Lcom/squareup/util/Res;

.field final synthetic $tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

.field final synthetic this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;->$tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;->$res$inlined:Lcom/squareup/util/Res;

    .line 324
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    const-string p2, "charSequence"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 332
    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$getTipAnimator$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/widgets/PersistentViewAnimator;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/widgets/PersistentViewAnimator;->getDisplayedChildId()I

    move-result p2

    iget-object p3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p3}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$getCustomTip$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/ui/XableEditText;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/ui/XableEditText;->getId()I

    move-result p3

    if-ne p2, p3, :cond_0

    .line 333
    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;->$tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    invoke-virtual {p2}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getSetCustomTip()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
