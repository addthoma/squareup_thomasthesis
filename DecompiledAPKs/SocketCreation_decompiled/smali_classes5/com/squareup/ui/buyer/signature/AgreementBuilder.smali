.class public Lcom/squareup/ui/buyer/signature/AgreementBuilder;
.super Ljava/lang/Object;
.source "AgreementBuilder.java"


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private agreement(Lcom/squareup/payment/Payer;Lcom/squareup/Card;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->res:Lcom/squareup/util/Res;

    invoke-static {v0, p1, p2}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->getCardholderName(Lcom/squareup/util/Res;Lcom/squareup/payment/Payer;Lcom/squareup/Card;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 52
    invoke-virtual {p2}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    iget v0, v0, Lcom/squareup/text/CardBrandResources;->buyerCardPhrase:I

    invoke-interface {p5, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-direct {p0, p1, p3, p5}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->getAgreementString(Ljava/lang/CharSequence;Ljava/lang/String;Lcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string p3, "refund_policy"

    if-eqz p4, :cond_0

    .line 55
    invoke-virtual {p1, p3, p4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    goto :goto_0

    :cond_0
    const-string p4, ""

    .line 57
    invoke-virtual {p1, p3, p4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    :goto_0
    const-string p3, "card_brand_phrase"

    .line 59
    invoke-virtual {p1, p3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 60
    invoke-static {p2}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->getUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "card_suffix"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private getAgreementString(Ljava/lang/CharSequence;Ljava/lang/String;Lcom/squareup/util/Res;)Lcom/squareup/phrase/Phrase;
    .locals 3

    .line 71
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 72
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 74
    sget p1, Lcom/squareup/transaction/R$string;->buyer_signature_disclaimer_no_name_no_auth:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string v2, "auth_number"

    if-eqz v0, :cond_1

    .line 76
    sget p1, Lcom/squareup/transaction/R$string;->buyer_signature_disclaimer_no_name:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 77
    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string v0, "cardholder_name"

    if-eqz v1, :cond_2

    .line 79
    sget p2, Lcom/squareup/transaction/R$string;->buyer_signature_disclaimer_no_auth:I

    invoke-interface {p3, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 80
    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    .line 82
    :cond_2
    sget v1, Lcom/squareup/transaction/R$string;->buyer_signature_disclaimer:I

    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 83
    invoke-virtual {p3, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 84
    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private static getCardholderName(Lcom/squareup/util/Res;Lcom/squareup/payment/Payer;Lcom/squareup/Card;)Ljava/lang/CharSequence;
    .locals 1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/payment/Payer;->getName()Ljava/lang/String;

    move-result-object p1

    .line 92
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p2}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 95
    :cond_0
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    const-string p0, ""

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static getUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 0

    .line 99
    invoke-virtual {p0}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, "####"

    :goto_0
    return-object p0
.end method


# virtual methods
.method public agreementTest(Lcom/squareup/payment/Payer;Lcom/squareup/Card;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 6

    if-eqz p4, :cond_0

    .line 33
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->agreement(Lcom/squareup/payment/Payer;Lcom/squareup/Card;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    .line 35
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->agreement(Lcom/squareup/payment/Payer;Lcom/squareup/Card;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public buildCardPaymentAgreementForDevice(Lcom/squareup/payment/RequiresCardAgreement;Ljava/lang/CharSequence;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 6

    .line 26
    invoke-interface {p1}, Lcom/squareup/payment/RequiresCardAgreement;->getPayer()Lcom/squareup/payment/Payer;

    move-result-object v1

    invoke-interface {p1}, Lcom/squareup/payment/RequiresCardAgreement;->getCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-interface {p1}, Lcom/squareup/payment/RequiresCardAgreement;->getAuthorizationCode()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->agreement(Lcom/squareup/payment/Payer;Lcom/squareup/Card;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public buildCardPaymentAgreementForPrinting()Ljava/lang/String;
    .locals 2

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->paper_signature_card_issuer_agreement:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCardholderName(Lcom/squareup/payment/RequiresCardAgreement;)Ljava/lang/String;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->res:Lcom/squareup/util/Res;

    invoke-interface {p1}, Lcom/squareup/payment/RequiresCardAgreement;->getPayer()Lcom/squareup/payment/Payer;

    move-result-object v1

    invoke-interface {p1}, Lcom/squareup/payment/RequiresCardAgreement;->getCard()Lcom/squareup/Card;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->getCardholderName(Lcom/squareup/util/Res;Lcom/squareup/payment/Payer;Lcom/squareup/Card;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
