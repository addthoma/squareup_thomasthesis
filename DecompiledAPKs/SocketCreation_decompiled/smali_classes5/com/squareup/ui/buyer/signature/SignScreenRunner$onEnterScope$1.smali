.class final Lcom/squareup/ui/buyer/signature/SignScreenRunner$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SignScreenRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignScreenRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "data",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/signature/SignScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/signature/SignScreenRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner$onEnterScope$1;->invoke(Lcom/squareup/locale/LocaleOverrideFactory;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignScreenRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/buyer/signature/SignScreenRunner;

    const-string v1, "data"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/ui/buyer/signature/SignScreenRunner;->access$initialSetup(Lcom/squareup/ui/buyer/signature/SignScreenRunner;Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method
