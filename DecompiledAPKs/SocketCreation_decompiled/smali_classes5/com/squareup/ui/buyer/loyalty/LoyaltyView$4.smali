.class Lcom/squareup/ui/buyer/loyalty/LoyaltyView$4;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "LoyaltyView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$4;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$4;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->access$000(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lcom/squareup/marketfont/MarketButton;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketButton;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$4;->this$0:Lcom/squareup/ui/buyer/loyalty/LoyaltyView;

    invoke-static {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->access$000(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lcom/squareup/marketfont/MarketButton;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketButton;->performClick()Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
