.class public Lcom/squareup/ui/buyer/loyalty/LoyaltyView;
.super Landroid/widget/FrameLayout;
.source "LoyaltyView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# static fields
.field private static final DELAY_MILLIS:I = 0x7d0

.field private static final FADE_IN_OUT_DURATION_MILLIS:I = 0x12c

.field private static final MAX_TIERS_TO_SHOW:I = 0x5


# instance fields
.field private addCardToCustomer:Lcom/squareup/glyph/SquareGlyphView;

.field private allDoneContainer:Landroid/view/View;

.field private allDoneGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private allDoneText:Lcom/squareup/marketfont/MarketTextView;

.field private buyerLoyaltyAccountContainer:Landroid/view/View;

.field private cashAppBanner:Landroid/view/View;

.field private claimYourStar:Lcom/squareup/marketfont/MarketButton;

.field private claimYourStarHelp:Lcom/squareup/widgets/MessageView;

.field curatedImage:Lcom/squareup/merchantimages/CuratedImage;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private customer:Lcom/squareup/glyph/SquareGlyphView;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private earnedPointsBottom:Lcom/squareup/widgets/MessageView;

.field private earnedPointsTop:Lcom/squareup/widgets/MessageView;

.field private earnedRewardsBottom:Lcom/squareup/widgets/MessageView;

.field private earnedRewardsTop:Lcom/squareup/widgets/MessageView;

.field private enrollContainer:Landroid/view/View;

.field private final enrollmentSlideUp:Landroid/animation/Animator;

.field private joinLoyalty:Lcom/squareup/marketfont/MarketButton;

.field private loyaltyCongratulationsTitle:Landroid/widget/TextView;

.field private loyaltyProgramName:Lcom/squareup/widgets/MessageView;

.field private loyaltyRewardStatus:Lcom/squareup/widgets/MessageView;

.field private merchantImage:Landroid/widget/ImageView;

.field private newSale:Lcom/squareup/marketfont/MarketButton;

.field private noThanks:Lcom/squareup/marketfont/MarketButton;

.field private onAddCardClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onClaimYourStarClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onNewSaleClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onSendBuyerLoyaltyStatusButtonClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private phoneEdit:Lcom/squareup/widgets/SelectableEditText;

.field phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private rewardContainer:Landroid/view/View;

.field private rewardRequirement:Lcom/squareup/widgets/MessageView;

.field private rewardStatusBrandImage:Landroid/widget/ImageView;

.field private rewardTiersBrandImage:Landroid/widget/ImageView;

.field private sendBuyerLoyaltyStatusButton:Lcom/squareup/marketfont/MarketButton;

.field private final shortAnimTimeMs:I

.field private showTiersView:Landroid/view/View;

.field private tierListContainer:Landroid/widget/LinearLayout;

.field private tierRequirement:Lcom/squareup/widgets/MessageView;

.field private tierStatus:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 115
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116
    const-class p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen$Component;->inject(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    .line 119
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const/high16 v0, 0x10e0000

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->shortAnimTimeMs:I

    .line 120
    sget p2, Lcom/squareup/ui/buyerflow/R$animator;->loyalty_enrollment_slide_up:I

    invoke-static {p1, p2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollmentSlideUp:Landroid/animation/Animator;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)Lcom/squareup/marketfont/MarketButton;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->claimYourStar:Lcom/squareup/marketfont/MarketButton;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->setAppearanceForImageBackground()V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 436
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_new_sale:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->newSale:Lcom/squareup/marketfont/MarketButton;

    .line 437
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_customer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->customer:Lcom/squareup/glyph/SquareGlyphView;

    .line 438
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_customer_add_card_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->addCardToCustomer:Lcom/squareup/glyph/SquareGlyphView;

    .line 439
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_view_reward_tiers:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showTiersView:Landroid/view/View;

    .line 440
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_tier_status:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierStatus:Lcom/squareup/widgets/MessageView;

    .line 441
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_tier_requirement:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierRequirement:Lcom/squareup/widgets/MessageView;

    .line 442
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->reward_tier_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierListContainer:Landroid/widget/LinearLayout;

    .line 443
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_join:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->joinLoyalty:Lcom/squareup/marketfont/MarketButton;

    .line 444
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_enroll_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollContainer:Landroid/view/View;

    .line 445
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_reward_status:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyRewardStatus:Lcom/squareup/widgets/MessageView;

    .line 446
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_reward_requirement:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardRequirement:Lcom/squareup/widgets/MessageView;

    .line 447
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_phone_edit:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    .line 448
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_claim_your_star:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->claimYourStar:Lcom/squareup/marketfont/MarketButton;

    .line 449
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_claim_your_star_help:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->claimYourStarHelp:Lcom/squareup/widgets/MessageView;

    .line 450
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_no_thanks:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->noThanks:Lcom/squareup/marketfont/MarketButton;

    .line 451
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_reward_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardContainer:Landroid/view/View;

    .line 452
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->reward_tiers_brand_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardTiersBrandImage:Landroid/widget/ImageView;

    .line 453
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->rewards_points_brand_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardStatusBrandImage:Landroid/widget/ImageView;

    .line 454
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_program_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyProgramName:Lcom/squareup/widgets/MessageView;

    .line 455
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_congratulations_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyCongratulationsTitle:Landroid/widget/TextView;

    .line 456
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_congratulations_points_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsTop:Lcom/squareup/widgets/MessageView;

    .line 457
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_congratulations_points_term:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsBottom:Lcom/squareup/widgets/MessageView;

    .line 458
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_congratulations_rewards_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsTop:Lcom/squareup/widgets/MessageView;

    .line 459
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_congratulations_rewards_term:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsBottom:Lcom/squareup/widgets/MessageView;

    .line 460
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_all_done_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneContainer:Landroid/view/View;

    .line 461
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->send_buyer_loyalty_status_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->sendBuyerLoyaltyStatusButton:Lcom/squareup/marketfont/MarketButton;

    .line 462
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_loyalty_account_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->buyerLoyaltyAccountContainer:Landroid/view/View;

    .line 463
    sget v0, Lcom/squareup/checkout/R$id;->merchant_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->merchantImage:Landroid/widget/ImageView;

    .line 464
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_all_done_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 465
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_all_done_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneText:Lcom/squareup/marketfont/MarketTextView;

    .line 466
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->loyalty_cash_app_banner:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->cashAppBanner:Landroid/view/View;

    return-void
.end method

.method private initRewardTiers(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 523
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 526
    sget v2, Lcom/squareup/ui/buyerflow/R$layout;->loyalty_reward_tier:I

    invoke-static {v2, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/loyalty/RewardTierView;

    .line 527
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/account/protos/RewardTier;

    iget-object v4, v4, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->getFormattedPoints(J)Ljava/lang/String;

    move-result-object v3

    .line 528
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/account/protos/RewardTier;

    iget-object v4, v4, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    .line 527
    invoke-virtual {v2, v3, v4}, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->setRewardData(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/RewardTierView;

    .line 529
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierListContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 531
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierListContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->refreshDrawableState()V

    return-void
.end method

.method private setAppearanceForImageBackground()V
    .locals 5

    .line 470
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    .line 471
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/marin/R$color;->marin_transparent:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    .line 472
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/squareup/marin/R$drawable;->marin_selector_black_transparent_fifty_border_white_transparent_twenty:I

    goto :goto_0

    :cond_0
    sget v2, Lcom/squareup/marin/R$drawable;->marin_selector_black_transparent_fifty_pressed:I

    .line 478
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->newSale:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v3, v0}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    .line 479
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->newSale:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v3, v2}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    .line 481
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->customer:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 482
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->customer:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v3, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 484
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->customer:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v3}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 485
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->customer:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v3, v2}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    .line 488
    :cond_1
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->addCardToCustomer:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 489
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->addCardToCustomer:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v3, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 491
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->addCardToCustomer:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v3}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 492
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->addCardToCustomer:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v3, v2}, Lcom/squareup/util/Views;->setBackgroundResourceCompat(Landroid/view/View;I)V

    .line 496
    :cond_2
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyProgramName:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v2, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 497
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyCongratulationsTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 498
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showTiersView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    const/4 v2, 0x0

    .line 499
    :goto_1
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierListContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 500
    iget-object v3, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierListContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/buyer/loyalty/RewardTierView;

    invoke-virtual {v3, v0}, Lcom/squareup/ui/buyer/loyalty/RewardTierView;->setTextColor(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 502
    :cond_3
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierStatus:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v2, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 503
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierRequirement:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v2, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 504
    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollContainer:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 505
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyRewardStatus:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 506
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardRequirement:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 507
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->claimYourStarHelp:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setTextColor(I)V

    .line 508
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->sendBuyerLoyaltyStatusButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    .line 511
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneGlyph:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 512
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    .line 513
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphView;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 514
    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneText:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    :cond_4
    return-void
.end method


# virtual methods
.method hideEnrollment()V
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method hideViewTiers()V
    .locals 2

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showTiersView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$null$3$LoyaltyView(Lcom/squareup/loyalty/AnimateProgressRewardText;)V
    .locals 2

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsTop:Lcom/squareup/widgets/MessageView;

    const/16 v1, 0x64

    invoke-virtual {p1, v1}, Lcom/squareup/loyalty/AnimateProgressRewardText;->getInterpolatedBalance(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$null$5$LoyaltyView(Lcom/squareup/loyalty/AnimateProgressRewardText;)V
    .locals 2

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsTop:Lcom/squareup/widgets/MessageView;

    const/16 v1, 0x64

    invoke-virtual {p1, v1}, Lcom/squareup/loyalty/AnimateProgressRewardText;->getInterpolatedBalance(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$LoyaltyView(Landroid/view/View;)V
    .locals 0

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->joinLoyaltyClicked()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$LoyaltyView(Landroid/view/View;)V
    .locals 0

    .line 168
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->clearFocus()V

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    return-void
.end method

.method public synthetic lambda$setBackgroundImage$7$LoyaltyView(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
    .locals 0

    .line 409
    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->merchantImage:Landroid/widget/ImageView;

    new-instance p3, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$5;

    invoke-direct {p3, p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$5;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-virtual {p1, p2, p3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    return-void
.end method

.method public synthetic lambda$setLogoImage$8$LoyaltyView(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
    .locals 0

    .line 425
    invoke-virtual {p1, p3, p4}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardTiersBrandImage:Landroid/widget/ImageView;

    .line 426
    invoke-virtual {p1, p2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    return-void
.end method

.method public synthetic lambda$setLogoImage$9$LoyaltyView(Lcom/squareup/picasso/RequestCreator;Landroid/view/View;II)V
    .locals 0

    .line 431
    invoke-virtual {p1, p3, p4}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardStatusBrandImage:Landroid/widget/ImageView;

    .line 432
    invoke-virtual {p1, p2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    return-void
.end method

.method public synthetic lambda$setRewardStatusPoints$4$LoyaltyView(Lcom/squareup/loyalty/AnimateProgressRewardText;)V
    .locals 3

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsTop:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x0

    .line 336
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 337
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    .line 338
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$X_8dsbDSGWo8EtVBPWM_dWWvWyg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$X_8dsbDSGWo8EtVBPWM_dWWvWyg;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/loyalty/AnimateProgressRewardText;)V

    .line 339
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 341
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method public synthetic lambda$setRewardStatusPoints$6$LoyaltyView(Lcom/squareup/loyalty/AnimateProgressRewardText;)V
    .locals 3

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsTop:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x0

    .line 371
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 372
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    .line 373
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$hXR1g7p_wafNxXA0yetCgyf6kio;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$hXR1g7p_wafNxXA0yetCgyf6kio;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/loyalty/AnimateProgressRewardText;)V

    .line 374
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 376
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method public synthetic lambda$showEnrollment$2$LoyaltyView()V
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public onAddCardClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onAddCardClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 174
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method onClaimYourStarClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onClaimYourStarClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->presenter:Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;->dropView(Ljava/lang/Object;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->merchantImage:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Lcom/squareup/merchantimages/CuratedImage;->cancelRequest(Landroid/widget/ImageView;)V

    .line 181
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 124
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 125
    invoke-direct {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->bindViews()V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->joinLoyalty:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$LjBwrtWPsu3JJJ6ESgDZLwPhOH0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$LjBwrtWPsu3JJJ6ESgDZLwPhOH0;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->customer:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$1;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->noThanks:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$2;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->claimYourStar:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onClaimYourStarClicked:Lrx/Observable;

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->sendBuyerLoyaltyStatusButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onSendBuyerLoyaltyStatusButtonClicked:Lrx/Observable;

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->newSale:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onNewSaleClicked:Lrx/Observable;

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->addCardToCustomer:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onAddCardClicked:Lrx/Observable;

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$3;

    iget-object v2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    invoke-direct {v1, p0, v2, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$3;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView$4;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$KaFCTeEwzQnacdplxHkwQLZixTA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$KaFCTeEwzQnacdplxHkwQLZixTA;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onNewSaleClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onNewSaleClicked:Lrx/Observable;

    return-object v0
.end method

.method onSendBuyerLoyaltyStatusButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->onSendBuyerLoyaltyStatusButtonClicked:Lrx/Observable;

    return-object v0
.end method

.method setBackgroundImage(Lcom/squareup/picasso/RequestCreator;)V
    .locals 2

    .line 408
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->merchantImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 409
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->merchantImage:Landroid/widget/ImageView;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$pEeJTdzEfctj0PlHvk3nqvA2yXM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$pEeJTdzEfctj0PlHvk3nqvA2yXM;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/picasso/RequestCreator;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method public setClaimYourStarEnabled(Z)V
    .locals 1

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->claimYourStar:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method setEnrollment(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;)V
    .locals 3

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyRewardStatus:Lcom/squareup/widgets/MessageView;

    iget-object v1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->enrollmentLoyaltyStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardRequirement:Lcom/squareup/widgets/MessageView;

    iget-object v1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardRequirementText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierStatus:Lcom/squareup/widgets/MessageView;

    iget-object v1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardTiersLoyaltyStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->tierRequirement:Lcom/squareup/widgets/MessageView;

    iget-object v1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardRequirementText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v0, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->rewardTiers:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->initRewardTiers(Ljava/util/List;)V

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->phoneEditHint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    iget-object v1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->defaultPhoneNumber:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->phoneNumber:Ljava/lang/String;

    .line 218
    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->receiptPhoneNumber:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->phoneNumber:Ljava/lang/String;

    .line 219
    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 217
    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setSelectAllOnFocus(Z)V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setSelection(I)V

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->claimYourStarHelp:Lcom/squareup/widgets/MessageView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;->claimHelpText:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setLogoImage(Lcom/squareup/picasso/RequestCreator;)V
    .locals 2

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardTiersBrandImage:Landroid/widget/ImageView;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$05xAy7CgOCQQEGQ4H9SeMIDKLbA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$05xAy7CgOCQQEGQ4H9SeMIDKLbA;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/picasso/RequestCreator;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 428
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardStatusBrandImage:Landroid/widget/ImageView;

    new-instance v1, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$wVW1C0ZtWYfq0kbTdE6QOxQziJo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$wVW1C0ZtWYfq0kbTdE6QOxQziJo;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/picasso/RequestCreator;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method setNewSaleText(I)V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->newSale:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    return-void
.end method

.method setRewardStatusPoints(Ljava/lang/String;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Lcom/squareup/loyalty/LoyaltyRewardText;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyProgramName:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->loyaltyCongratulationsTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    iget-object p6, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->joinLoyalty:Lcom/squareup/marketfont/MarketButton;

    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->loyalty_join_loyalty:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "program_name"

    .line 304
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 305
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 306
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 303
    invoke-virtual {p6, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 309
    instance-of p1, p2, Lcom/squareup/loyalty/StaticRewardText;

    const/16 p6, 0x8

    const-wide/16 v0, 0x12c

    const/4 v2, 0x0

    const-wide/16 v3, 0x7d0

    if-eqz p1, :cond_1

    .line 316
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_0

    .line 317
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsTop:Lcom/squareup/widgets/MessageView;

    check-cast p2, Lcom/squareup/loyalty/StaticRewardText;

    invoke-virtual {p2}, Lcom/squareup/loyalty/StaticRewardText;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsBottom:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 320
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsTop:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p6}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 321
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsBottom:Lcom/squareup/widgets/MessageView;

    check-cast p2, Lcom/squareup/loyalty/StaticRewardText;

    invoke-virtual {p2}, Lcom/squareup/loyalty/StaticRewardText;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 325
    :cond_1
    check-cast p2, Lcom/squareup/loyalty/AnimateProgressRewardText;

    .line 326
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsTop:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p2}, Lcom/squareup/loyalty/AnimateProgressRewardText;->getInitialText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsBottom:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedPointsTop:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1}, Lcom/squareup/widgets/MessageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 330
    invoke-virtual {p1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 331
    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 332
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p3, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$zPB-LZ9Wx8rSEoxun3Vo3NB00-8;

    invoke-direct {p3, p0, p2}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$zPB-LZ9Wx8rSEoxun3Vo3NB00-8;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/loyalty/AnimateProgressRewardText;)V

    .line 333
    invoke-virtual {p1, p3}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 342
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 345
    :goto_0
    instance-of p1, p4, Lcom/squareup/loyalty/StaticRewardText;

    if-eqz p1, :cond_3

    .line 352
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_2

    .line 353
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsTop:Lcom/squareup/widgets/MessageView;

    check-cast p4, Lcom/squareup/loyalty/StaticRewardText;

    invoke-virtual {p4}, Lcom/squareup/loyalty/StaticRewardText;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsBottom:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 356
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsTop:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p6}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 357
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsBottom:Lcom/squareup/widgets/MessageView;

    check-cast p4, Lcom/squareup/loyalty/StaticRewardText;

    invoke-virtual {p4}, Lcom/squareup/loyalty/StaticRewardText;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 360
    :cond_3
    check-cast p4, Lcom/squareup/loyalty/AnimateProgressRewardText;

    .line 361
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsTop:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p4}, Lcom/squareup/loyalty/AnimateProgressRewardText;->getInitialText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsBottom:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p5}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->earnedRewardsTop:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1}, Lcom/squareup/widgets/MessageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 365
    invoke-virtual {p1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 366
    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 367
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$ybR2ot05rHFoaSDZ1FENA5Ycti0;

    invoke-direct {p2, p0, p4}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$ybR2ot05rHFoaSDZ1FENA5Ycti0;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;Lcom/squareup/loyalty/AnimateProgressRewardText;)V

    .line 368
    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 377
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    :goto_1
    return-void
.end method

.method showAddCardButton(Z)V
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->addCardToCustomer:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showAllDone(Z)V
    .locals 2

    .line 382
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneContainer:Landroid/view/View;

    iget v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->shortAnimTimeMs:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    if-eqz p1, :cond_0

    .line 385
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->allDoneContainer:Landroid/view/View;

    invoke-static {}, Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method showBuyerLoyaltyAccountInformation()V
    .locals 2

    .line 399
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->buyerLoyaltyAccountContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showClaimYourStar()V
    .locals 2

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->claimYourStar:Lcom/squareup/marketfont/MarketButton;

    iget v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->shortAnimTimeMs:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method showCustomerButton(Z)V
    .locals 2

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->customer:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->customer:Lcom/squareup/glyph/SquareGlyphView;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method showEnrollment(ZZ)V
    .locals 2

    if-eqz p1, :cond_0

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollContainer:Landroid/view/View;

    iget v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->shortAnimTimeMs:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 244
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollmentSlideUp:Landroid/animation/Animator;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollmentSlideUp:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    if-nez p1, :cond_1

    .line 247
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->enrollmentSlideUp:Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->end()V

    :cond_1
    if-eqz p2, :cond_2

    .line 251
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->phoneEdit:Lcom/squareup/widgets/SelectableEditText;

    new-instance p2, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$FWHabrI1AIzE612nNm879q_gc30;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyView$FWHabrI1AIzE612nNm879q_gc30;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyView;)V

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method showRewardStatus(ZZ)V
    .locals 2

    .line 277
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardContainer:Landroid/view/View;

    iget v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->shortAnimTimeMs:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    if-eqz p1, :cond_0

    .line 280
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->rewardContainer:Landroid/view/View;

    invoke-static {}, Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    if-eqz p2, :cond_1

    .line 284
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->cashAppBanner:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method showRewardTiers(ZLjava/lang/String;)V
    .locals 3

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->joinLoyalty:Lcom/squareup/marketfont/MarketButton;

    iget-object v1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->loyalty_join_loyalty:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "program_name"

    .line 227
    invoke-virtual {v1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 228
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 229
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 226
    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    .line 231
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showTiersView:Landroid/view/View;

    iget p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->shortAnimTimeMs:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 233
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->showTiersView:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method updateBuyerLoyaltyAccountInformation()V
    .locals 2

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->sendBuyerLoyaltyStatusButton:Lcom/squareup/marketfont/MarketButton;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->loyalty_status_sent:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 404
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyView;->sendBuyerLoyaltyStatusButton:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method
