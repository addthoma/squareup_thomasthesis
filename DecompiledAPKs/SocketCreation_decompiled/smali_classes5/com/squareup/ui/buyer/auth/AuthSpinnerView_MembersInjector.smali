.class public final Lcom/squareup/ui/buyer/auth/AuthSpinnerView_MembersInjector;
.super Ljava/lang/Object;
.source "AuthSpinnerView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/buyer/auth/AuthSpinnerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/buyer/auth/AuthSpinnerView;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/buyer/auth/AuthSpinnerView;Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;->presenter:Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/buyer/auth/AuthSpinnerView;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/buyer/auth/AuthSpinnerView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView_MembersInjector;->injectPresenter(Lcom/squareup/ui/buyer/auth/AuthSpinnerView;Lcom/squareup/ui/buyer/auth/AuthSpinnerPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/buyer/auth/AuthSpinnerView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerView_MembersInjector;->injectMembers(Lcom/squareup/ui/buyer/auth/AuthSpinnerView;)V

    return-void
.end method
