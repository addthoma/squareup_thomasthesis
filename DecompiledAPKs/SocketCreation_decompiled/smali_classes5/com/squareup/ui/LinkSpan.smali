.class public abstract Lcom/squareup/ui/LinkSpan;
.super Landroid/text/style/ClickableSpan;
.source "LinkSpan.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/LinkSpan$Component;,
        Lcom/squareup/ui/LinkSpan$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_COLOR_ID:I


# instance fields
.field private final linkColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray_pressed:I

    sput v0, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0

    .line 28
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 29
    iput p1, p0, Lcom/squareup/ui/LinkSpan;->linkColor:I

    return-void
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .line 33
    iget v0, p0, Lcom/squareup/ui/LinkSpan;->linkColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    return-void
.end method
