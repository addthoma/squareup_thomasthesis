.class public final Lcom/squareup/ui/SquareActivity_MembersInjector;
.super Ljava/lang/Object;
.source "SquareActivity_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/SquareActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityResultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationChangeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final contentViewInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final focusedActivityScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final locationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaButtonDisablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentBundleManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p3, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p4, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p5, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p7, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p8, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p9, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p10, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p11, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p12, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p13, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/SquareActivity;",
            ">;"
        }
    .end annotation

    .line 95
    new-instance v14, Lcom/squareup/ui/SquareActivity_MembersInjector;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/SquareActivity_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

    return-void
.end method

.method public static injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    return-void
.end method

.method public static injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->configurationChangeMonitor:Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    return-void
.end method

.method public static injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->contentViewInitializer:Lcom/squareup/development/drawer/ContentViewInitializer;

    return-void
.end method

.method public static injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->focusedActivityScanner:Lcom/squareup/radiography/FocusedActivityScanner;

    return-void
.end method

.method public static injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->internetStatusMonitor:Lcom/squareup/internet/InternetStatusMonitor;

    return-void
.end method

.method public static injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->locationMonitor:Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    return-void
.end method

.method public static injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->mediaButtonDisabler:Lcom/squareup/ui/MediaButtonDisabler;

    return-void
.end method

.method public static injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/SquareActivity;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;)V"
        }
    .end annotation

    .line 152
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->minesweeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/ui/SquareActivity;->persistentBundleManager:Lcom/squareup/persistentbundle/PersistentBundleManager;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/SquareActivity;)V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/development/drawer/ContentViewInitializer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/MediaButtonDisabler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/SquareActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/ui/SquareActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMembers(Lcom/squareup/ui/SquareActivity;)V

    return-void
.end method
