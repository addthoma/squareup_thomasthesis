.class public Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;
.super Landroid/widget/LinearLayout;
.source "InstantDepositsResultView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private doneButton:Lcom/squareup/marketfont/MarketButton;

.field private glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private learnMoreButton:Lcom/squareup/marketfont/MarketButton;

.field presenter:Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class p2, Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultScreen$Component;->inject(Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 97
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 98
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposits_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->progressBar:Landroid/widget/ProgressBar;

    .line 99
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposits_glyph_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 100
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposits_done_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->doneButton:Lcom/squareup/marketfont/MarketButton;

    .line 101
    sget v0, Lcom/squareup/billhistoryui/R$id;->instant_deposits_learn_more_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->learnMoreButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onFinishInflate$0$InstantDepositsResultView()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->presenter:Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->finish()V

    return-void
.end method

.method public synthetic lambda$showLearnMoreButton$1$InstantDepositsResultView(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .line 81
    iget-object p3, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->presenter:Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->learnMore(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->presenter:Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->presenter:Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->dropView(Ljava/lang/Object;)V

    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->progressComplete()V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->bindViews()V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hide()V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/ui/instantdeposits/-$$Lambda$InstantDepositsResultView$frnrhpD8hZ2aqhzlprzHXBMGmsk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/instantdeposits/-$$Lambda$InstantDepositsResultView$frnrhpD8hZ2aqhzlprzHXBMGmsk;-><init>(Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->doneButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView$1;-><init>(Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->presenter:Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->progressComplete()V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public progressComplete()V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->show()V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->glyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->doneButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method public setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showLearnMoreButton(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->learnMoreButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/instantdeposits/-$$Lambda$InstantDepositsResultView$-3Bh2NYOlgU2JN2YXERhKBcjKHw;

    invoke-direct {v1, p0, p2, p3}, Lcom/squareup/ui/instantdeposits/-$$Lambda$InstantDepositsResultView$-3Bh2NYOlgU2JN2YXERhKBcjKHw;-><init>(Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    .line 80
    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object p2, p0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultView;->learnMoreButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
