.class Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;
.super Ljava/lang/Object;
.source "AddressBookLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/addressbook/AddressBookLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LoadContacts"
.end annotation


# instance fields
.field private final constraint:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field final synthetic this$0:Lcom/squareup/ui/addressbook/AddressBookLoader;


# direct methods
.method constructor <init>(Lcom/squareup/ui/addressbook/AddressBookLoader;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->this$0:Lcom/squareup/ui/addressbook/AddressBookLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput-object p2, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->context:Landroid/content/Context;

    .line 220
    iput-object p3, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->constraint:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public synthetic lambda$run$0$AddressBookLoader$LoadContacts(Landroid/database/MatrixCursor;)V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->this$0:Lcom/squareup/ui/addressbook/AddressBookLoader;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/ui/addressbook/AddressBookLoader;->access$102(Lcom/squareup/ui/addressbook/AddressBookLoader;Z)Z

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->this$0:Lcom/squareup/ui/addressbook/AddressBookLoader;

    invoke-static {v0, p1}, Lcom/squareup/ui/addressbook/AddressBookLoader;->access$200(Lcom/squareup/ui/addressbook/AddressBookLoader;Landroid/database/MatrixCursor;)V

    return-void
.end method

.method public run()V
    .locals 3

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->this$0:Lcom/squareup/ui/addressbook/AddressBookLoader;

    iget-object v1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->constraint:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/addressbook/AddressBookLoader;->loadSync(Landroid/content/Context;Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 225
    iget-object v1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;->this$0:Lcom/squareup/ui/addressbook/AddressBookLoader;

    invoke-static {v1}, Lcom/squareup/ui/addressbook/AddressBookLoader;->access$000(Lcom/squareup/ui/addressbook/AddressBookLoader;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/addressbook/-$$Lambda$AddressBookLoader$LoadContacts$EN4PtIBjqIfTChqu5jsIyOSfA5A;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/addressbook/-$$Lambda$AddressBookLoader$LoadContacts$EN4PtIBjqIfTChqu5jsIyOSfA5A;-><init>(Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;Landroid/database/MatrixCursor;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method
