.class public Lcom/squareup/ui/addressbook/AddressBookLoader;
.super Ljava/lang/Object;
.source "AddressBookLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;,
        Lcom/squareup/ui/addressbook/AddressBookLoader$Callback;,
        Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;
    }
.end annotation


# static fields
.field static final COMBINED_MATRIX_CURSOR_COLUMNS:[Ljava/lang/String;

.field private static final CONTACT_ID_COL:I = 0x0

.field private static final DATA_COL:I = 0x3

.field private static final DISPLAY_NAME_COL:I = 0x1

.field static final EMAIL_COLUMN_NAMES_TO_COMPARE:[Ljava/lang/String;

.field static final EMAIL_PROJECTION:[Ljava/lang/String;

.field static final ORDER_BY_CONTACT_ID:Ljava/lang/String; = "contact_id COLLATE NOCASE ASC"

.field static final PHONE_COLUMN_NAMES_TO_COMPARE:[Ljava/lang/String;

.field static final PHONE_PROJECTION:[Ljava/lang/String;

.field private static final PHOTO_URI_COL:I = 0x2


# instance fields
.field private callback:Lcom/squareup/ui/addressbook/AddressBookLoader$Callback;

.field private final executor:Ljava/util/concurrent/Executor;

.field private loading:Z

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-string v0, "data1"

    const-string v1, "photo_uri"

    const-string v2, "display_name"

    const-string v3, "contact_id"

    .line 29
    filled-new-array {v3, v2, v1, v0}, [Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/squareup/ui/addressbook/AddressBookLoader;->EMAIL_PROJECTION:[Ljava/lang/String;

    .line 34
    filled-new-array {v3, v2, v1, v0}, [Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/squareup/ui/addressbook/AddressBookLoader;->PHONE_PROJECTION:[Ljava/lang/String;

    .line 39
    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/squareup/ui/addressbook/AddressBookLoader;->EMAIL_COLUMN_NAMES_TO_COMPARE:[Ljava/lang/String;

    .line 41
    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/squareup/ui/addressbook/AddressBookLoader;->PHONE_COLUMN_NAMES_TO_COMPARE:[Ljava/lang/String;

    const-string v3, "_id"

    const-string v4, "data2"

    .line 44
    filled-new-array {v3, v2, v1, v0, v4}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/addressbook/AddressBookLoader;->COMBINED_MATRIX_CURSOR_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->executor:Ljava/util/concurrent/Executor;

    .line 60
    iput-object p2, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 61
    iput-object p3, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/addressbook/AddressBookLoader;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/ui/addressbook/AddressBookLoader;Z)Z
    .locals 0

    .line 21
    iput-boolean p1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->loading:Z

    return p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/addressbook/AddressBookLoader;Landroid/database/MatrixCursor;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/ui/addressbook/AddressBookLoader;->deliverResult(Landroid/database/MatrixCursor;)V

    return-void
.end method

.method private deliverResult(Landroid/database/MatrixCursor;)V
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->callback:Lcom/squareup/ui/addressbook/AddressBookLoader$Callback;

    if-nez v0, :cond_0

    .line 204
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->close()V

    return-void

    .line 207
    :cond_0
    invoke-interface {v0, p1}, Lcom/squareup/ui/addressbook/AddressBookLoader$Callback;->onContactsLoaded(Landroid/database/MatrixCursor;)V

    return-void
.end method

.method static getContactFromCursors(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/CursorJoiner$Result;)Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;
    .locals 14

    move-object v0, p0

    move-object v1, p1

    .line 172
    sget-object v2, Lcom/squareup/ui/addressbook/AddressBookLoader$1;->$SwitchMap$android$database$CursorJoiner$Result:[I

    invoke-virtual/range {p2 .. p2}, Landroid/database/CursorJoiner$Result;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eq v2, v6, :cond_1

    if-eq v2, v5, :cond_0

    .line 191
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 192
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 193
    new-instance v1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;

    .line 194
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 195
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 196
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object v7, v1

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 182
    :cond_0
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 183
    new-instance v0, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;

    .line 184
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 185
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 186
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 174
    :cond_1
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 175
    new-instance v1, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;

    .line 176
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 177
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 178
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v13, ""

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method load(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/ui/addressbook/AddressBookLoader$Callback;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 66
    iget-boolean v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->loading:Z

    if-eqz v0, :cond_0

    return-void

    .line 67
    :cond_0
    iput-object p3, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->callback:Lcom/squareup/ui/addressbook/AddressBookLoader$Callback;

    .line 68
    iget-object p3, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->executor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/addressbook/AddressBookLoader$LoadContacts;-><init>(Lcom/squareup/ui/addressbook/AddressBookLoader;Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    .line 69
    iput-boolean p1, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->loading:Z

    return-void
.end method

.method loadSync(Landroid/content/Context;Ljava/lang/String;)Landroid/database/MatrixCursor;
    .locals 8

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/addressbook/AddressBookLoader;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const-string v1, "Loading contacts should occur on a background thread."

    invoke-interface {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid(Ljava/lang/String;)V

    .line 119
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/squareup/ui/addressbook/AddressBookLoader;->COMBINED_MATRIX_CURSOR_COLUMNS:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/squareup/ui/addressbook/AddressBookLoader;->EMAIL_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "contact_id COLLATE NOCASE ASC"

    .line 121
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/squareup/ui/addressbook/AddressBookLoader;->PHONE_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "contact_id COLLATE NOCASE ASC"

    .line 126
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 131
    new-instance v2, Landroid/database/CursorJoiner;

    sget-object v3, Lcom/squareup/ui/addressbook/AddressBookLoader;->EMAIL_COLUMN_NAMES_TO_COMPARE:[Ljava/lang/String;

    sget-object v4, Lcom/squareup/ui/addressbook/AddressBookLoader;->PHONE_COLUMN_NAMES_TO_COMPARE:[Ljava/lang/String;

    invoke-direct {v2, v1, v3, p1, v4}, Landroid/database/CursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 133
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 135
    invoke-virtual {v2}, Landroid/database/CursorJoiner;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/CursorJoiner$Result;

    .line 136
    invoke-static {v1, p1, v4}, Lcom/squareup/ui/addressbook/AddressBookLoader;->getContactFromCursors(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/CursorJoiner$Result;)Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;

    move-result-object v4

    if-eqz p2, :cond_2

    .line 138
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 140
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(.*)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 141
    iget-object v6, v4, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->name:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, v4, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->email:Ljava/lang/String;

    .line 142
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, v4, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->phone:Ljava/lang/String;

    .line 143
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 144
    :cond_1
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 147
    :cond_2
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 153
    :cond_3
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 156
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 157
    iget-object v5, v2, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->id:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v2, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, v2, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->photo:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, v2, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->email:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget-object v2, v2, Lcom/squareup/ui/addressbook/AddressBookLoader$AndroidContact;->phone:Ljava/lang/String;

    aput-object v2, v3, v4

    .line 159
    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1

    .line 162
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 163
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v0

    .line 128
    :cond_5
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 123
    :cond_6
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method
