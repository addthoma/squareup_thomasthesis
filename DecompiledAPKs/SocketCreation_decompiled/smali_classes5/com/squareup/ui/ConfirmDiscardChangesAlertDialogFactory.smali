.class public Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;
.super Ljava/lang/Object;
.source "ConfirmDiscardChangesAlertDialogFactory.java"


# static fields
.field private static final DO_NOTHING:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/ui/-$$Lambda$ConfirmDiscardChangesAlertDialogFactory$20bTpxvjH0zvuUPVcJYLbh8I7mY;->INSTANCE:Lcom/squareup/ui/-$$Lambda$ConfirmDiscardChangesAlertDialogFactory$20bTpxvjH0zvuUPVcJYLbh8I7mY;

    sput-object v0, Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;->DO_NOTHING:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createConfirmDiscardChangesAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;)Landroid/app/AlertDialog;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;->DO_NOTHING:Ljava/lang/Runnable;

    invoke-static {p0, p1, v0}, Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;->createConfirmDiscardChangesAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method public static createConfirmDiscardChangesAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p0, Lcom/squareup/widgets/pos/R$string;->edit_item_confirm_discard_changes_dialog_title:I

    .line 28
    invoke-virtual {v0, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget v0, Lcom/squareup/widgets/pos/R$string;->edit_item_confirm_discard_changes_dialog_message:I

    .line 29
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget v0, Lcom/squareup/widgets/pos/R$string;->confirmation_popup_resume:I

    new-instance v1, Lcom/squareup/ui/-$$Lambda$ConfirmDiscardChangesAlertDialogFactory$OGUTXFUX-OEGg0kemAiBqH_M3gk;

    invoke-direct {v1, p2}, Lcom/squareup/ui/-$$Lambda$ConfirmDiscardChangesAlertDialogFactory$OGUTXFUX-OEGg0kemAiBqH_M3gk;-><init>(Ljava/lang/Runnable;)V

    .line 30
    invoke-virtual {p0, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    new-instance v0, Lcom/squareup/ui/-$$Lambda$ConfirmDiscardChangesAlertDialogFactory$9dGIS77lzKldJYl9MNZGLqtT6LM;

    invoke-direct {v0, p2}, Lcom/squareup/ui/-$$Lambda$ConfirmDiscardChangesAlertDialogFactory$9dGIS77lzKldJYl9MNZGLqtT6LM;-><init>(Ljava/lang/Runnable;)V

    .line 32
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget p2, Lcom/squareup/marin/R$color;->marin_blue:I

    .line 33
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget p2, Lcom/squareup/marin/R$color;->marin_white:I

    .line 34
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget p2, Lcom/squareup/common/strings/R$string;->confirmation_popup_discard:I

    new-instance v0, Lcom/squareup/ui/-$$Lambda$ConfirmDiscardChangesAlertDialogFactory$U6CsNStZlsxHHjoj9P3Abz6chD8;

    invoke-direct {v0, p1}, Lcom/squareup/ui/-$$Lambda$ConfirmDiscardChangesAlertDialogFactory$U6CsNStZlsxHHjoj9P3Abz6chD8;-><init>(Ljava/lang/Runnable;)V

    .line 35
    invoke-virtual {p0, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$createConfirmDiscardChangesAlertDialog$1(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 31
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method static synthetic lambda$createConfirmDiscardChangesAlertDialog$2(Ljava/lang/Runnable;Landroid/content/DialogInterface;)V
    .locals 0

    .line 32
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method static synthetic lambda$createConfirmDiscardChangesAlertDialog$3(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 36
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method static synthetic lambda$static$0()V
    .locals 0

    return-void
.end method
