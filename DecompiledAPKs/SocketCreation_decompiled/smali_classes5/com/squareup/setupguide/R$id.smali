.class public final Lcom/squareup/setupguide/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar_title:I = 0x7f0a013c

.field public static final bottom_text_view:I = 0x7f0a0247

.field public static final collapse_toolbar:I = 0x7f0a0363

.field public static final header_container:I = 0x7f0a07c9

.field public static final icon:I = 0x7f0a0818

.field public static final setup_guide_dialog_icon:I = 0x7f0a0e6d

.field public static final setup_guide_dialog_message:I = 0x7f0a0e6e

.field public static final setup_guide_dialog_primary:I = 0x7f0a0e6f

.field public static final setup_guide_dialog_secondary:I = 0x7f0a0e70

.field public static final setup_guide_dialog_title:I = 0x7f0a0e71

.field public static final setup_guide_intro_card:I = 0x7f0a0e72

.field public static final subtitle:I = 0x7f0a0f49

.field public static final title:I = 0x7f0a103f

.field public static final title_and_glyph:I = 0x7f0a1043

.field public static final toolbar:I = 0x7f0a104d

.field public static final top_text_view:I = 0x7f0a1056

.field public static final up_button_glyph:I = 0x7f0a10c1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
