.class public final Lcom/squareup/setupguide/NoSetupGuideOnboardingModule_ProvidesIntegrationRunnerFactory;
.super Ljava/lang/Object;
.source "NoSetupGuideOnboardingModule_ProvidesIntegrationRunnerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/NoSetupGuideOnboardingModule_ProvidesIntegrationRunnerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/setupguide/NoSetupGuideOnboardingModule_ProvidesIntegrationRunnerFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/setupguide/NoSetupGuideOnboardingModule_ProvidesIntegrationRunnerFactory$InstanceHolder;->access$000()Lcom/squareup/setupguide/NoSetupGuideOnboardingModule_ProvidesIntegrationRunnerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static providesIntegrationRunner()Lcom/squareup/setupguide/SetupGuideIntegrationRunner;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/setupguide/NoSetupGuideOnboardingModule;->providesIntegrationRunner()Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/setupguide/SetupGuideIntegrationRunner;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/setupguide/NoSetupGuideOnboardingModule_ProvidesIntegrationRunnerFactory;->providesIntegrationRunner()Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/setupguide/NoSetupGuideOnboardingModule_ProvidesIntegrationRunnerFactory;->get()Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    move-result-object v0

    return-object v0
.end method
