.class public final Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
.super Ljava/lang/Object;
.source "SetupPaymentsDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupPaymentsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0018\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\"BE\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0004\u0012\n\u0008\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\n\u0008\u0003\u0010\u0008\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0014\u001a\u00020\u0001H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0004H\u00c6\u0003J\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0017\u001a\u00020\u0004H\u00c6\u0003J\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000bJP\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00012\u0008\u0008\u0003\u0010\u0003\u001a\u00020\u00042\n\u0008\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00042\n\u0008\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00042\n\u0008\u0003\u0010\u0008\u001a\u0004\u0018\u00010\u0004H\u00c6\u0001\u00a2\u0006\u0002\u0010\u001bJ\u0013\u0010\u001c\u001a\u00020\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001f\u001a\u00020\u0004H\u00d6\u0001J\t\u0010 \u001a\u00020!H\u00d6\u0001R\u0015\u0010\u0008\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\n\n\u0002\u0010\u000c\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\n\n\u0002\u0010\u000c\u001a\u0004\u0008\u000f\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0015\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\n\n\u0002\u0010\u000c\u001a\u0004\u0008\u0012\u0010\u000bR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0011\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
        "",
        "key",
        "titleId",
        "",
        "messageId",
        "primaryButtonId",
        "secondaryButtonId",
        "iconId",
        "(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)V",
        "getIconId",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getKey",
        "()Ljava/lang/Object;",
        "getMessageId",
        "getPrimaryButtonId",
        "()I",
        "getSecondaryButtonId",
        "getTitleId",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "Factory",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final iconId:Ljava/lang/Integer;

.field private final key:Ljava/lang/Object;

.field private final messageId:Ljava/lang/Integer;

.field private final primaryButtonId:I

.field private final secondaryButtonId:Ljava/lang/Integer;

.field private final titleId:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->key:Ljava/lang/Object;

    iput p2, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->titleId:I

    iput-object p3, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->messageId:Ljava/lang/Integer;

    iput p4, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->primaryButtonId:I

    iput-object p5, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->secondaryButtonId:Ljava/lang/Integer;

    iput-object p6, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->iconId:Ljava/lang/Integer;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p7, 0x4

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 51
    move-object v0, v1

    check-cast v0, Ljava/lang/Integer;

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p3

    :goto_0
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    .line 53
    move-object v0, v1

    check-cast v0, Ljava/lang/Integer;

    move-object v7, v0

    goto :goto_1

    :cond_1
    move-object v7, p5

    :goto_1
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_2

    .line 54
    move-object v0, v1

    check-cast v0, Ljava/lang/Integer;

    move-object v8, v0

    goto :goto_2

    :cond_2
    move-object v8, p6

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v6, p4

    invoke-direct/range {v2 .. v8}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;-><init>(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->key:Ljava/lang/Object;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget p2, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->titleId:I

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->messageId:Ljava/lang/Integer;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->primaryButtonId:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->secondaryButtonId:Ljava/lang/Integer;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->iconId:Ljava/lang/Integer;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move p4, p8

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->copy(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->titleId:I

    return v0
.end method

.method public final component3()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->messageId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->primaryButtonId:I

    return v0
.end method

.method public final component5()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->secondaryButtonId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component6()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->iconId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;
    .locals 8

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-object v1, v0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;-><init>(Ljava/lang/Object;ILjava/lang/Integer;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->key:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->key:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->titleId:I

    iget v1, p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->titleId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->messageId:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->messageId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->primaryButtonId:I

    iget v1, p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->primaryButtonId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->secondaryButtonId:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->secondaryButtonId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->iconId:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->iconId:Ljava/lang/Integer;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getIconId()Ljava/lang/Integer;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->iconId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public final getMessageId()Ljava/lang/Integer;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->messageId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getPrimaryButtonId()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->primaryButtonId:I

    return v0
.end method

.method public final getSecondaryButtonId()Ljava/lang/Integer;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->secondaryButtonId:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getTitleId()I
    .locals 1

    .line 50
    iget v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->titleId:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->key:Ljava/lang/Object;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->titleId:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->messageId:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->primaryButtonId:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->secondaryButtonId:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->iconId:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->titleId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", messageId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->messageId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", primaryButtonId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->primaryButtonId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", secondaryButtonId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->secondaryButtonId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", iconId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;->iconId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
