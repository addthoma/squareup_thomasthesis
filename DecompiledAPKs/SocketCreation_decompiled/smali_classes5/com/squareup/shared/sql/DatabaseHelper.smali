.class public interface abstract Lcom/squareup/shared/sql/DatabaseHelper;
.super Ljava/lang/Object;
.source "DatabaseHelper.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;
.end method

.method public abstract getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;
.end method
