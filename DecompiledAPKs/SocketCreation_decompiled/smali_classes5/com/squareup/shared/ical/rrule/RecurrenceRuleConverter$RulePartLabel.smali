.class final enum Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;
.super Ljava/lang/Enum;
.source "RecurrenceRuleConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RulePartLabel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum BYDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum BYMONTH:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum BYMONTHDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum BYSETPOS:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum BYWEEKNO:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum BYYEARDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum COUNT:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum FREQ:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum INTERVAL:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum UNTIL:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

.field public static final enum WKST:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 29
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/4 v1, 0x0

    const-string v2, "FREQ"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->FREQ:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 30
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/4 v2, 0x1

    const-string v3, "UNTIL"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->UNTIL:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 31
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/4 v3, 0x2

    const-string v4, "COUNT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->COUNT:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 32
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/4 v4, 0x3

    const-string v5, "INTERVAL"

    invoke-direct {v0, v5, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->INTERVAL:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 33
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/4 v5, 0x4

    const-string v6, "BYDAY"

    invoke-direct {v0, v6, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 34
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/4 v6, 0x5

    const-string v7, "BYMONTHDAY"

    invoke-direct {v0, v7, v6}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYMONTHDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 35
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/4 v7, 0x6

    const-string v8, "BYYEARDAY"

    invoke-direct {v0, v8, v7}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYYEARDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 36
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/4 v8, 0x7

    const-string v9, "BYWEEKNO"

    invoke-direct {v0, v9, v8}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYWEEKNO:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 37
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/16 v9, 0x8

    const-string v10, "BYMONTH"

    invoke-direct {v0, v10, v9}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYMONTH:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 38
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/16 v10, 0x9

    const-string v11, "BYSETPOS"

    invoke-direct {v0, v11, v10}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYSETPOS:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 39
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/16 v11, 0xa

    const-string v12, "WKST"

    invoke-direct {v0, v12, v11}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->WKST:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    .line 28
    sget-object v12, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->FREQ:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->UNTIL:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->COUNT:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->INTERVAL:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYMONTHDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYYEARDAY:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYWEEKNO:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYMONTH:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->BYSETPOS:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->WKST:Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->$VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;
    .locals 1

    .line 28
    const-class v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->$VALUES:[Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    invoke-virtual {v0}, [Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/ical/rrule/RecurrenceRuleConverter$RulePartLabel;

    return-object v0
.end method
