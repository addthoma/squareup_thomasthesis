.class public interface abstract Lcom/squareup/shared/ical/rrule/RecurrenceRule;
.super Ljava/lang/Object;
.source "RecurrenceRule.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;,
        Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;,
        Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Ljava/util/Date;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract getByDay()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getByMonth()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getByMonthDay()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBySetPos()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getByWeekNo()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getByYearDay()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCount()Ljava/lang/Integer;
.end method

.method public abstract getDtStart()Ljava/util/Date;
.end method

.method public abstract getExDates()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFrequency()Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;
.end method

.method public abstract getInterval()I
.end method

.method public abstract getTimeZone()Ljava/util/TimeZone;
.end method

.method public abstract getUntil()Ljava/util/Date;
.end method

.method public abstract getWkst()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;
.end method
