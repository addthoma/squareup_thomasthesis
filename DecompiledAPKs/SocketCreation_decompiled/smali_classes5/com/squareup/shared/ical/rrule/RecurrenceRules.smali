.class public final Lcom/squareup/shared/ical/rrule/RecurrenceRules;
.super Ljava/lang/Object;
.source "RecurrenceRules.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static isWkstSignificantInRule(Lcom/squareup/shared/ical/rrule/RecurrenceRule;)Z
    .locals 4

    .line 20
    sget-object v0, Lcom/squareup/shared/ical/rrule/RecurrenceRules$1;->$SwitchMap$com$squareup$shared$ical$rrule$RecurrenceRule$Frequency:[I

    invoke-interface {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getFrequency()Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    return v1

    .line 26
    :cond_0
    invoke-interface {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByWeekNo()Ljava/util/Set;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 27
    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1

    .line 22
    :cond_2
    invoke-interface {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByDay()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 23
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    .line 24
    :goto_0
    invoke-interface {p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getInterval()I

    move-result p0

    if-le p0, v2, :cond_4

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    :cond_4
    return v1
.end method

.method public static recurrenceRuleWithInsignificantWkstRemoved(Lcom/squareup/shared/ical/rrule/RecurrenceRule;)Lcom/squareup/shared/ical/rrule/RecurrenceRule;
    .locals 16

    .line 37
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getWkst()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v0

    if-nez v0, :cond_0

    return-object p0

    .line 41
    :cond_0
    invoke-static/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRules;->isWkstSignificantInRule(Lcom/squareup/shared/ical/rrule/RecurrenceRule;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p0

    .line 46
    :cond_1
    new-instance v0, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;

    .line 47
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getDtStart()Ljava/util/Date;

    move-result-object v2

    .line 48
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    sget-object v4, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->MO:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    .line 50
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getExDates()Ljava/util/Set;

    move-result-object v5

    .line 51
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getFrequency()Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;

    move-result-object v6

    .line 52
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getInterval()I

    move-result v7

    .line 53
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getCount()Ljava/lang/Integer;

    move-result-object v8

    .line 54
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getUntil()Ljava/util/Date;

    move-result-object v9

    .line 55
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByMonth()Ljava/util/Set;

    move-result-object v10

    .line 56
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByWeekNo()Ljava/util/Set;

    move-result-object v11

    .line 57
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByYearDay()Ljava/util/Set;

    move-result-object v12

    .line 58
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByMonthDay()Ljava/util/Set;

    move-result-object v13

    .line 59
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getByDay()Ljava/util/Set;

    move-result-object v14

    .line 60
    invoke-interface/range {p0 .. p0}, Lcom/squareup/shared/ical/rrule/RecurrenceRule;->getBySetPos()Ljava/util/Set;

    move-result-object v15

    move-object v1, v0

    invoke-direct/range {v1 .. v15}, Lcom/squareup/shared/ical/rrule/RecurrenceRuleImpl;-><init>(Ljava/util/Date;Ljava/util/TimeZone;Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;Ljava/util/Set;Lcom/squareup/shared/ical/rrule/RecurrenceRule$Frequency;ILjava/lang/Integer;Ljava/util/Date;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-object v0
.end method
