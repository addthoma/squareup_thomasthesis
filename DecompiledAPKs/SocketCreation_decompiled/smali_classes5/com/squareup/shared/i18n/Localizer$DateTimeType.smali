.class public final enum Lcom/squareup/shared/i18n/Localizer$DateTimeType;
.super Ljava/lang/Enum;
.source "Localizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/i18n/Localizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DateTimeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/i18n/Localizer$DateTimeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/i18n/Localizer$DateTimeType;

.field public static final enum LONG_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

.field public static final enum LONG_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

.field public static final enum MEDIUM_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

.field public static final enum MEDIUM_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

.field public static final enum SHORT_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

.field public static final enum SHORT_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

.field public static final enum TIME:Lcom/squareup/shared/i18n/Localizer$DateTimeType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 28
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    const/4 v1, 0x0

    const-string v2, "SHORT_DATE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    .line 29
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    const/4 v2, 0x1

    const-string v3, "SHORT_DATE_NO_YEAR"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    .line 30
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    const/4 v3, 0x2

    const-string v4, "MEDIUM_DATE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->MEDIUM_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    .line 31
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    const/4 v4, 0x3

    const-string v5, "MEDIUM_DATE_NO_YEAR"

    invoke-direct {v0, v5, v4}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->MEDIUM_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    .line 32
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    const/4 v5, 0x4

    const-string v6, "LONG_DATE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->LONG_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    .line 33
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    const/4 v6, 0x5

    const-string v7, "LONG_DATE_NO_YEAR"

    invoke-direct {v0, v7, v6}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->LONG_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    .line 34
    new-instance v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    const/4 v7, 0x6

    const-string v8, "TIME"

    invoke-direct {v0, v8, v7}, Lcom/squareup/shared/i18n/Localizer$DateTimeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->TIME:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    .line 27
    sget-object v8, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->SHORT_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->MEDIUM_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->MEDIUM_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->LONG_DATE:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->LONG_DATE_NO_YEAR:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->TIME:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->$VALUES:[Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/i18n/Localizer$DateTimeType;
    .locals 1

    .line 27
    const-class v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/i18n/Localizer$DateTimeType;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->$VALUES:[Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-virtual {v0}, [Lcom/squareup/shared/i18n/Localizer$DateTimeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    return-object v0
.end method
