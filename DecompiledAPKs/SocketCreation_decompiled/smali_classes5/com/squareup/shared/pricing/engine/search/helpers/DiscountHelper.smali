.class public Lcom/squareup/shared/pricing/engine/search/helpers/DiscountHelper;
.super Ljava/lang/Object;
.source "DiscountHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDiscountValueForAmount(Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;J)J
    .locals 3

    .line 22
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getPercentage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 24
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getPercentage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Ljava/math/BigDecimal;

    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getPercentage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->scaleByPowerOfTen(I)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 26
    :cond_0
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    .line 29
    :goto_0
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    const/4 p2, 0x0

    sget-object v0, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    .line 30
    invoke-virtual {p1, p2, v0}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p1

    invoke-virtual {p1}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide p1

    .line 33
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getMaximumAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getMaximumAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v2, v0, p1

    if-gez v2, :cond_3

    .line 34
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getMaximumAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    goto :goto_1

    .line 36
    :cond_1
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 37
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;->getAmount()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 38
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    goto :goto_1

    :cond_2
    const-wide/16 p1, 0x0

    :cond_3
    :goto_1
    return-wide p1
.end method
