.class public Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;
.super Ljava/lang/Object;
.source "LineItemRuleNode.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/search/RuleNode;


# instance fields
.field private applicationsCount:J

.field private final applySet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

.field private final excludeSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

.field protected final matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

.field private final rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

.field private final sources:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Lcom/squareup/shared/pricing/engine/search/Source;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/JunctionSet;Lcom/squareup/shared/pricing/engine/search/JunctionSet;Lcom/squareup/shared/pricing/engine/search/JunctionSet;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Lcom/squareup/shared/pricing/engine/search/JunctionSet;",
            "Lcom/squareup/shared/pricing/engine/search/JunctionSet;",
            "Lcom/squareup/shared/pricing/engine/search/JunctionSet;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Lcom/squareup/shared/pricing/engine/search/Source;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 37
    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->applicationsCount:J

    if-eqz p2, :cond_2

    if-eqz p3, :cond_1

    if-nez p4, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot combine apply and exclude set"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 57
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 58
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    .line 59
    iput-object p3, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->excludeSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    .line 60
    iput-object p4, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->applySet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    .line 61
    iput-object p5, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->sources:Ljava/util/Map;

    return-void

    .line 50
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Match set cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private bestInflateFactor(Ljava/util/Map;)J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;)J"
        }
    .end annotation

    if-eqz p1, :cond_4

    .line 115
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 120
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 121
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/models/ClientServerIds;

    .line 122
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    .line 123
    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->sources:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Source;

    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-virtual {v2, v3}, Lcom/squareup/shared/pricing/engine/search/Source;->matchableQuantity(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x0

    .line 124
    sget-object v4, Ljava/math/RoundingMode;->FLOOR:Ljava/math/RoundingMode;

    invoke-virtual {v2, v1, v3, v4}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 129
    :cond_2
    iget-object p1, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMaxApplicationsPerAttachment()I

    move-result p1

    const/4 v1, -0x1

    if-eq p1, v1, :cond_3

    .line 132
    iget-object p1, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMaxApplicationsPerAttachment()I

    move-result p1

    int-to-long v1, p1

    iget-wide v3, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->applicationsCount:J

    sub-long/2addr v1, v3

    .line 133
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v3

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0

    .line 135
    :cond_3
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0

    :cond_4
    :goto_1
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method private buildApplicationWithApply()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
    .locals 12

    .line 244
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->applySet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    sget-object v4, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    sget-object v5, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MIN:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->exhaust(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v0

    .line 245
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    :cond_0
    const/4 v1, 0x0

    .line 248
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 249
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v10

    .line 250
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getOffsets()Ljava/util/Map;

    move-result-object v11

    .line 252
    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v4, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v6

    sget-object v7, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MIN:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v5, v10

    invoke-virtual/range {v3 .. v9}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v3

    .line 253
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    return-object v2

    .line 256
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 257
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getOffsets()Ljava/util/Map;

    move-result-object v2

    .line 258
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Candidate;

    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v1

    .line 260
    sget-object v3, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$$Lambda$2;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    invoke-static {v2, v11, v3}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->merge(Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object v11

    .line 265
    sget-object v2, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$$Lambda$3;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    invoke-static {v1, v10, v2}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->merge(Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object v9

    .line 267
    invoke-direct {p0, v9}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->bestInflateFactor(Ljava/util/Map;)J

    move-result-wide v7

    .line 269
    new-instance v1, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    iget-object v4, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 270
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide v5

    move-object v3, v1

    invoke-direct/range {v3 .. v11}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v1
.end method

.method private buildApplicationWithLeastExpensiveExclude()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
    .locals 12

    .line 141
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->excludeSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 142
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MIN:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v0

    .line 143
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    :cond_0
    const/4 v1, 0x0

    .line 146
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 147
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v11

    .line 148
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getOffsets()Ljava/util/Map;

    move-result-object v0

    .line 151
    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v4, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 152
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v6

    sget-object v7, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    sget-object v8, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MIN:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, v11

    .line 151
    invoke-virtual/range {v3 .. v10}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->exhaust(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v3

    .line 153
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    return-object v2

    .line 156
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 157
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v8

    .line 158
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getOffsets()Ljava/util/Map;

    move-result-object v2

    .line 161
    invoke-static {v8, v11}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->subtractFloor(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v9

    .line 162
    sget-object v3, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$$Lambda$0;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    invoke-static {v2, v0, v3}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->merge(Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object v10

    .line 164
    invoke-interface {v1, v9}, Lcom/squareup/shared/pricing/engine/search/Candidate;->filter(Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/Candidate;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide v4

    .line 166
    invoke-direct {p0, v8}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->bestInflateFactor(Ljava/util/Map;)J

    move-result-wide v6

    .line 167
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    iget-object v3, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-object v2, v0

    invoke-direct/range {v2 .. v10}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method

.method private buildApplicationWithMatch()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
    .locals 11

    .line 278
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    sget-object v4, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    sget-object v5, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MIN:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->exhaust(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v0

    .line 279
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 282
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 283
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v8

    .line 284
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getOffsets()Ljava/util/Map;

    move-result-object v9

    .line 286
    invoke-direct {p0, v8}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->bestInflateFactor(Ljava/util/Map;)J

    move-result-wide v5

    .line 288
    new-instance v10, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 289
    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide v3

    move-object v1, v10

    move-object v7, v8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v10
.end method

.method private buildApplicationWithMostExpensiveExclude()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
    .locals 22

    move-object/from16 v0, p0

    .line 178
    iget-object v1, v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->excludeSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v2, v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 179
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v4

    sget-object v5, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MAX:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v1

    .line 180
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    return-object v3

    :cond_0
    const/4 v2, 0x0

    .line 183
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 184
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v12

    .line 188
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getMinUnitPrice()J

    move-result-wide v4

    .line 193
    iget-object v1, v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->matchSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v6, v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 194
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v7

    sget-object v8, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    sget-object v9, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MIN:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object v4, v1

    move-object v5, v6

    move-object v6, v12

    .line 193
    invoke-virtual/range {v4 .. v11}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->exhaust(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v1

    .line 195
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    return-object v3

    .line 198
    :cond_1
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 199
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v3

    .line 203
    invoke-static {v3, v12}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->subtractFloor(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 208
    invoke-interface {v1, v3}, Lcom/squareup/shared/pricing/engine/search/Candidate;->filter(Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/Candidate;

    move-result-object v1

    .line 209
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getMaxUnitPrice()J

    move-result-wide v4

    .line 210
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getOffsets()Ljava/util/Map;

    move-result-object v11

    .line 218
    iget-object v6, v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->excludeSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    iget-object v7, v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v8

    sget-object v9, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->MIN:Lcom/squareup/shared/pricing/engine/search/Provider$Mode;

    .line 219
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    const/4 v12, 0x0

    move-object v4, v6

    move-object v5, v7

    move-object v6, v8

    move-object v7, v3

    move-object v8, v9

    move-object v9, v10

    move-object v10, v12

    .line 218
    invoke-virtual/range {v4 .. v10}, Lcom/squareup/shared/pricing/engine/search/JunctionSet;->satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object v4

    .line 220
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/search/Candidate;

    .line 221
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getApplications()Ljava/util/Map;

    move-result-object v4

    .line 222
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getOffsets()Ljava/util/Map;

    move-result-object v2

    .line 226
    invoke-static {v3, v4}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->add(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    .line 227
    sget-object v5, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$$Lambda$1;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;

    invoke-static {v11, v2, v5}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->merge(Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/util/MapUtils$Operation;)Ljava/util/Map;

    move-result-object v21

    .line 230
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide v15

    .line 231
    invoke-direct {v0, v4}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->bestInflateFactor(Ljava/util/Map;)J

    move-result-wide v17

    .line 233
    new-instance v1, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    iget-object v14, v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-object v13, v1

    move-object/from16 v19, v4

    move-object/from16 v20, v3

    invoke-direct/range {v13 .. v21}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;-><init>(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;JJLjava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v1
.end method


# virtual methods
.method public bestApplication()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;
    .locals 5

    .line 72
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-interface {v0}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMaxApplicationsPerAttachment()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->applicationsCount:J

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 73
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getMaxApplicationsPerAttachment()I

    move-result v2

    int-to-long v2, v2

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->excludeSet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    if-eqz v0, :cond_3

    .line 78
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode$1;->$SwitchMap$com$squareup$shared$pricing$engine$catalog$models$PricingRuleFacade$ExcludeStrategy:[I

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getExcludeStrategy()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 82
    invoke-direct {p0}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->buildApplicationWithMostExpensiveExclude()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    move-result-object v0

    return-object v0

    .line 84
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized ExcludeStrategy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 85
    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getExcludeStrategy()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_2
    invoke-direct {p0}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->buildApplicationWithLeastExpensiveExclude()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    move-result-object v0

    return-object v0

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->applySet:Lcom/squareup/shared/pricing/engine/search/JunctionSet;

    if-eqz v0, :cond_4

    .line 89
    invoke-direct {p0}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->buildApplicationWithApply()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    move-result-object v0

    return-object v0

    .line 92
    :cond_4
    invoke-direct {p0}, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->buildApplicationWithMatch()Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;

    move-result-object v0

    return-object v0
.end method

.method public commitMatch(Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;)V
    .locals 4

    .line 97
    iget-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->applicationsCount:J

    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ProspectiveRuleApplication;->getInflateFactor()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->applicationsCount:J

    return-void
.end method

.method public getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/LineItemRuleNode;->rule:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    return-object v0
.end method
