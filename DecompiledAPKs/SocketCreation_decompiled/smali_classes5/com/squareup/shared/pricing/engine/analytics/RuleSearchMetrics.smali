.class public Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;
.super Ljava/lang/Object;
.source "RuleSearchMetrics.java"


# instance fields
.field public final automatic:Z

.field public final countAmountDiscountRulesApplied:I

.field public final countCategoryRulesApplied:I

.field public final countComboRulesApplied:I

.field public final countComplexRulesApplied:I

.field public final countDecimalQuantityInCart:Ljava/math/BigDecimal;

.field public final countItemRulesApplied:I

.field public final countItemizationsInCart:I

.field public final countPercentDiscountRulesApplied:I

.field public final countQuantityInCart:I

.field public final countRecurringRulesApplied:I

.field public final countRulesApplied:I

.field public final countRulesLoaded:I

.field public final countTimeRestrictedRulesApplied:I

.field public final countVolumeRulesApplied:I

.field public final largestRivalSet:I

.field public final loaderDuration:J

.field public final passcodeUsed:Z

.field public final ruleEvaluations:J

.field public final searchDuration:J

.field public final totalAmountDiscounted:Lcom/squareup/shared/pricing/models/MonetaryAmount;

.field public final totalDuration:J


# direct methods
.method public constructor <init>(JJJIIJIILjava/math/BigDecimal;IIILcom/squareup/shared/pricing/models/MonetaryAmount;IIIIIIIZZ)V
    .locals 3

    move-object v0, p0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-wide v1, p1

    .line 145
    iput-wide v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->searchDuration:J

    move-wide v1, p3

    .line 146
    iput-wide v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->loaderDuration:J

    move-wide v1, p5

    .line 147
    iput-wide v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalDuration:J

    move v1, p7

    .line 148
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countRulesLoaded:I

    move v1, p8

    .line 149
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->largestRivalSet:I

    move-wide v1, p9

    .line 150
    iput-wide v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->ruleEvaluations:J

    move v1, p11

    .line 151
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countItemizationsInCart:I

    move v1, p12

    .line 152
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countQuantityInCart:I

    move-object/from16 v1, p13

    .line 153
    iput-object v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countDecimalQuantityInCart:Ljava/math/BigDecimal;

    move/from16 v1, p14

    .line 154
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countRulesApplied:I

    move/from16 v1, p15

    .line 155
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countPercentDiscountRulesApplied:I

    move/from16 v1, p16

    .line 156
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countAmountDiscountRulesApplied:I

    move-object/from16 v1, p17

    .line 157
    iput-object v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalAmountDiscounted:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move/from16 v1, p18

    .line 158
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countCategoryRulesApplied:I

    move/from16 v1, p19

    .line 159
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countItemRulesApplied:I

    move/from16 v1, p20

    .line 160
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countVolumeRulesApplied:I

    move/from16 v1, p21

    .line 161
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countComboRulesApplied:I

    move/from16 v1, p22

    .line 162
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countTimeRestrictedRulesApplied:I

    move/from16 v1, p23

    .line 163
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countRecurringRulesApplied:I

    move/from16 v1, p24

    .line 164
    iput v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->countComplexRulesApplied:I

    move/from16 v1, p25

    .line 165
    iput-boolean v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->automatic:Z

    move/from16 v1, p26

    .line 166
    iput-boolean v1, v0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->passcodeUsed:Z

    return-void
.end method


# virtual methods
.method public totalAmountDiscountedCents()I
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalAmountDiscounted:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalAmountDiscounted:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    return v0
.end method

.method public totalAmountDiscountedCurrencyCode()Ljava/lang/String;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalAmountDiscounted:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;->totalAmountDiscounted:Lcom/squareup/shared/pricing/models/MonetaryAmount;

    invoke-virtual {v0}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
