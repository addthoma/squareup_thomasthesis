.class public interface abstract Lcom/squareup/shared/pricing/engine/rules/RuleApplication;
.super Ljava/lang/Object;
.source "RuleApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;
    }
.end annotation


# virtual methods
.method public abstract getApplications()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
.end method
