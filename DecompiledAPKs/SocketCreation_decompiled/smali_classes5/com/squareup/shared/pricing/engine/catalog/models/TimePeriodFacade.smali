.class public interface abstract Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;
.super Ljava/lang/Object;
.source "TimePeriodFacade.java"


# virtual methods
.method public abstract activeAt(Ljava/util/Date;Ljava/util/TimeZone;)Z
.end method

.method public abstract getRRule()Ljava/lang/String;
.end method

.method public abstract recurrenceRuleValueKey()Ljava/lang/String;
.end method
