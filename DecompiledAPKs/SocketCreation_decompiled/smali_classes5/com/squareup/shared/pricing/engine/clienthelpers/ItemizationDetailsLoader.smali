.class public Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;
.super Ljava/lang/Object;
.source "ItemizationDetailsLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader$InvalidCartException;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private convertBackingType(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;
    .locals 3

    .line 197
    sget-object v0, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader$1;->$SwitchMap$com$squareup$protos$client$bills$Itemization$Configuration$BackingType:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 203
    sget-object p1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-object p1

    .line 205
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Unknown backing type %s"

    .line 206
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_1
    sget-object p1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->ITEMS_SERVICE_ITEM_VARIATION:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-object p1

    .line 199
    :cond_2
    sget-object p1, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;->CUSTOM_AMOUNT:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    return-object p1
.end method

.method private getItemIdOrThrow(Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/String;
    .locals 4

    .line 151
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    .line 152
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->item_variation:Lcom/squareup/api/items/ItemVariation;

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    .line 156
    iget-object v1, v0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 157
    :cond_0
    new-instance v1, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader$InvalidCartException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    aput-object p1, v2, v0

    const-string p1, "Conflict detected in top level item ID (%s) and configuration level item ID (%s)"

    .line 158
    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p0, p1}, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader$InvalidCartException;-><init>(Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;Ljava/lang/String;)V

    throw v1

    .line 162
    :cond_1
    :goto_0
    iget-object p1, v0, Lcom/squareup/api/items/Item;->id:Ljava/lang/String;

    return-object p1

    :cond_2
    if-eqz p1, :cond_3

    .line 164
    iget-object p1, p1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    return-object p1

    .line 166
    :cond_3
    new-instance p1, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader$InvalidCartException;

    const-string v0, "Itemization must have an associated item"

    invoke-direct {p1, p0, v0}, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader$InvalidCartException;-><init>(Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;Ljava/lang/String;)V

    throw p1
.end method

.method private getUnitPrice(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/shared/pricing/models/MonetaryAmount;
    .locals 5

    .line 173
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->modifier_option:Ljava/util/List;

    .line 178
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    .line 179
    iget-object v4, v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->modifier_option_money:Lcom/squareup/protos/common/Money;

    if-eqz v4, :cond_0

    .line 180
    iget-object v3, v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->modifier_option_money:Lcom/squareup/protos/common/Money;

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v1, v3

    goto :goto_0

    .line 184
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 187
    iget-object v3, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v3, :cond_2

    .line 188
    new-instance v0, Lcom/squareup/shared/pricing/models/MonetaryAmount;

    iget-object v3, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v3, v1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 189
    invoke-virtual {p1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object p1

    invoke-direct {v0, v3, v4, p1}, Lcom/squareup/shared/pricing/models/MonetaryAmount;-><init>(JLjava/util/Currency;)V

    :cond_2
    return-object v0
.end method


# virtual methods
.method public getDateFormatWithDefaultLocale()Ljava/text/SimpleDateFormat;
    .locals 2

    .line 41
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd\'T\'HH:mm:ssXXX"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public load(Lcom/squareup/protos/client/bills/Cart;Ljava/text/SimpleDateFormat;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Ljava/text/SimpleDateFormat;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-nez v1, :cond_0

    return-object v0

    .line 57
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Itemization;

    .line 59
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    sget-object v3, Lcom/squareup/protos/client/bills/Itemization;->DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 63
    :cond_1
    new-instance v2, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    invoke-direct {v2}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;-><init>()V

    .line 65
    iget-object v3, v1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    .line 67
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    invoke-direct {p0, v4}, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;->convertBackingType(Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->backingType(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$BackingType;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 70
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v5, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-ne v4, v5, :cond_5

    .line 71
    iget-object v4, v1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v4, :cond_4

    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v4, :cond_4

    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    if-eqz v4, :cond_4

    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    if-eqz v4, :cond_4

    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->item_variation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v4, :cond_4

    .line 81
    invoke-direct {p0, v1}, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;->getItemIdOrThrow(Lcom/squareup/protos/client/bills/Itemization;)Ljava/lang/String;

    move-result-object v4

    .line 82
    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->itemID(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 84
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->item_variation:Lcom/squareup/api/items/ItemVariation;

    iget-object v4, v4, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->variationID(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 87
    iget-object v4, v1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v4, :cond_2

    .line 88
    iget-object v4, v1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    iget-object v4, v4, Lcom/squareup/api/items/MenuCategory;->id:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->categoryID(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 91
    :cond_2
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    .line 93
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->fractional(Z)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    goto :goto_2

    .line 77
    :cond_4
    new-instance p1, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader$InvalidCartException;

    const-string p2, "Itemization backed by an Item Variation must have backing details supplied."

    invoke-direct {p1, p0, p2}, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader$InvalidCartException;-><init>(Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;Ljava/lang/String;)V

    throw p1

    .line 96
    :cond_5
    :goto_2
    invoke-direct {p0, v1}, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;->getUnitPrice(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v4

    .line 97
    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->unitPrice(Lcom/squareup/shared/pricing/models/MonetaryAmount;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 99
    new-instance v4, Ljava/math/BigDecimal;

    iget-object v5, v1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 101
    iget-object v4, v1, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v4, :cond_6

    .line 103
    new-instance v5, Lcom/squareup/shared/pricing/models/ClientServerIds;

    iget-object v8, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-direct {v5, v8, v4}, Lcom/squareup/shared/pricing/models/ClientServerIds;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v2, v5}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->clientServerIds(Lcom/squareup/shared/pricing/models/ClientServerIds;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 108
    :cond_6
    :try_start_0
    iget-object v4, v1, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, v4, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-virtual {p2, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->addedAt(Ljava/util/Date;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v4, :cond_a

    iget-object v4, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    if-eqz v4, :cond_a

    .line 121
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 122
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/bills/DiscountLineItem;

    if-eqz v5, :cond_7

    .line 123
    iget-object v6, v5, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_deleted:Ljava/lang/Boolean;

    sget-object v7, Lcom/squareup/protos/client/bills/Itemization;->DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

    .line 124
    invoke-static {v6, v7}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, v5, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    if-nez v6, :cond_8

    goto :goto_3

    .line 128
    :cond_8
    iget-object v6, v5, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    iget-object v6, v6, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    .line 129
    iget-object v5, v5, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_method:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    sget-object v7, Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;->PRICING_RULE:Lcom/squareup/protos/client/bills/DiscountLineItem$ApplicationMethod;

    if-ne v5, v7, :cond_7

    .line 130
    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 133
    :cond_9
    invoke-virtual {v2, v4}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->existingAutoDiscounts(Ljava/util/Set;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 137
    :cond_a
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 138
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz v1, :cond_b

    .line 139
    iget-object v4, v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    if-eqz v4, :cond_b

    .line 140
    new-instance v3, Ljava/util/HashSet;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    invoke-direct {v3, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 142
    :cond_b
    invoke-virtual {v2, v3}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;

    .line 144
    invoke-virtual {v2}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails$Builder;->build()Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :catch_0
    move-exception p1

    .line 111
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-array v0, v7, [Ljava/lang/Object;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v1, v1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    aput-object v1, v0, v6

    const-string v1, "cannot parse addedAt %s"

    .line 112
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :cond_c
    return-object v0
.end method
