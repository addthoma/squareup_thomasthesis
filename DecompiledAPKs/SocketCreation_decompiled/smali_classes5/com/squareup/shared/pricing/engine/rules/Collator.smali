.class public Lcom/squareup/shared/pricing/engine/rules/Collator;
.super Ljava/lang/Object;
.source "Collator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/pricing/engine/rules/Collator$Event;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static applicationsToEvents(Ljava/util/List;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/Collator$Event;",
            ">;>;"
        }
    .end annotation

    .line 158
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 159
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;

    .line 160
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v2

    .line 161
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;->getRule()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object v3

    .line 163
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication;->getApplications()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;

    .line 164
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->getTarget()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v5

    .line 165
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->getOffset()Ljava/math/BigDecimal;

    move-result-object v6

    .line 166
    invoke-virtual {v4}, Lcom/squareup/shared/pricing/engine/rules/RuleApplication$Target;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 168
    sget-object v7, Lcom/squareup/shared/pricing/engine/rules/Collator$$Lambda$0;->$instance:Lcom/squareup/shared/pricing/engine/util/MapUtils$Defer;

    invoke-static {v0, v5, v7}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->getOrDefault(Ljava/util/Map;Ljava/lang/Object;Lcom/squareup/shared/pricing/engine/util/MapUtils$Defer;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 170
    new-instance v7, Lcom/squareup/shared/pricing/engine/rules/Collator$$Lambda$1;

    invoke-direct {v7, v2, v3}, Lcom/squareup/shared/pricing/engine/rules/Collator$$Lambda$1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5, v6, v7}, Lcom/squareup/shared/pricing/engine/rules/Collator;->insertEvent(Ljava/util/List;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/rules/Collator$Event$Modifier;)V

    .line 171
    new-instance v6, Lcom/squareup/shared/pricing/engine/rules/Collator$$Lambda$2;

    invoke-direct {v6, v2, v3}, Lcom/squareup/shared/pricing/engine/rules/Collator$$Lambda$2;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5, v4, v6}, Lcom/squareup/shared/pricing/engine/rules/Collator;->insertEvent(Ljava/util/List;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/rules/Collator$Event$Modifier;)V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static collate(Ljava/util/List;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/RuleApplication;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;"
        }
    .end annotation

    .line 282
    invoke-static {p0}, Lcom/squareup/shared/pricing/engine/rules/Collator;->applicationsToEvents(Ljava/util/List;)Ljava/util/Map;

    move-result-object p0

    .line 283
    invoke-static {p0}, Lcom/squareup/shared/pricing/engine/rules/Collator;->eventsToBlocks(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private static eventsToBlocks(Ljava/util/Map;)Ljava/util/Map;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/Collator$Event;",
            ">;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;"
        }
    .end annotation

    .line 214
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 216
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 217
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/models/ClientServerIds;

    .line 218
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 220
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_3

    .line 224
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 225
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    .line 229
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;

    iget-object v4, v4, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->position:Ljava/math/BigDecimal;

    .line 231
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;

    iget-object v5, v5, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->position:Ljava/math/BigDecimal;

    .line 232
    new-instance v6, Ljava/util/HashMap;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;

    iget-object v2, v2, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->rulesIn:Ljava/util/Map;

    invoke-direct {v6, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 234
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    const/4 v7, 0x1

    move-object v8, v4

    move-object v4, v2

    const/4 v2, 0x1

    .line 236
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    if-ge v2, v9, :cond_0

    .line 237
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;

    .line 239
    iget-object v10, v9, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->position:Ljava/math/BigDecimal;

    invoke-virtual {v10, v5}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v10

    .line 240
    invoke-static {v6}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->nonzeroKeys(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v11

    .line 242
    invoke-interface {v11}, Ljava/util/Set;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 245
    invoke-virtual {v8, v10}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    move-object v8, v4

    goto :goto_1

    .line 247
    :cond_1
    invoke-interface {v11, v4}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 250
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v7

    .line 251
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;

    .line 252
    new-instance v12, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;

    invoke-virtual {v5}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;->getOffset()Ljava/math/BigDecimal;

    move-result-object v13

    .line 253
    invoke-virtual {v5}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-static {v11}, Lcom/squareup/shared/pricing/engine/rules/Collator;->idPairsToMap(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v10

    invoke-direct {v12, v13, v5, v10}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;-><init>(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/util/Map;)V

    .line 252
    invoke-interface {v3, v4, v12}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 258
    :cond_2
    new-instance v4, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;

    invoke-virtual {v5, v8}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-static {v11}, Lcom/squareup/shared/pricing/engine/rules/Collator;->idPairsToMap(Ljava/util/Set;)Ljava/util/Map;

    move-result-object v12

    invoke-direct {v4, v5, v10, v12}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;-><init>(Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/util/Map;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    :goto_1
    iget-object v5, v9, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->position:Ljava/math/BigDecimal;

    .line 264
    iget-object v4, v9, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->rulesIn:Ljava/util/Map;

    invoke-static {v6, v4, v7}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->incrementMap(Ljava/util/Map;Ljava/util/Map;I)V

    .line 265
    iget-object v4, v9, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->rulesOut:Ljava/util/Map;

    const/4 v9, -0x1

    invoke-static {v6, v4, v9}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->incrementMap(Ljava/util/Map;Ljava/util/Map;I)V

    add-int/lit8 v2, v2, 0x1

    move-object v4, v11

    goto :goto_0

    .line 221
    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Must have at least 2 events"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_4
    return-object v0
.end method

.method private static idPairsToMap(Ljava/util/Set;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 182
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 183
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;

    .line 184
    iget-object v2, v1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->ruleId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 187
    iget-object v2, v1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->ruleId:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$IdPair;->discountId:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 185
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "rule collisions not allowed when collating"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    return-object v0
.end method

.method private static insertEvent(Ljava/util/List;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/rules/Collator$Event$Modifier;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/Collator$Event;",
            ">;",
            "Ljava/math/BigDecimal;",
            "Lcom/squareup/shared/pricing/engine/rules/Collator$Event$Modifier;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 129
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 130
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;

    .line 131
    iget-object v2, v1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->position:Ljava/math/BigDecimal;

    invoke-virtual {v2, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-nez v2, :cond_0

    .line 133
    invoke-interface {p2, v1}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$Modifier;->modify(Lcom/squareup/shared/pricing/engine/rules/Collator$Event;)V

    return-void

    .line 136
    :cond_0
    iget-object v1, v1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->position:Ljava/math/BigDecimal;

    invoke-virtual {v1, p1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-lez v1, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_2
    :goto_1
    new-instance v1, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;

    invoke-direct {v1, p1}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;-><init>(Ljava/math/BigDecimal;)V

    .line 146
    invoke-interface {p0, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 147
    invoke-interface {p2, v1}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event$Modifier;->modify(Lcom/squareup/shared/pricing/engine/rules/Collator$Event;)V

    return-void
.end method

.method static final synthetic lambda$applicationsToEvents$0$Collator(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/pricing/engine/rules/Collator$Event;)V
    .locals 0

    .line 170
    invoke-virtual {p2, p0, p1}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->logIn(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static final synthetic lambda$applicationsToEvents$1$Collator(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/shared/pricing/engine/rules/Collator$Event;)V
    .locals 0

    .line 171
    invoke-virtual {p2, p0, p1}, Lcom/squareup/shared/pricing/engine/rules/Collator$Event;->logOut(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
