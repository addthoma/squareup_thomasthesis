.class public Lcom/squareup/shared/pricing/engine/search/SearchLoader;
.super Ljava/lang/Object;
.source "SearchLoader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private loadDiscounts(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Lcom/squareup/shared/pricing/engine/rules/RuleSet;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            ">;"
        }
    .end annotation

    .line 40
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 41
    invoke-interface {p2}, Lcom/squareup/shared/pricing/engine/rules/RuleSet;->getRules()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 42
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getDiscountId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, ""

    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 44
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    :cond_1
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-interface {p1, v0}, Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;->findDiscountsByIds(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    return-object p2
.end method

.method private loadPeriods(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Lcom/squareup/shared/pricing/engine/rules/RuleSet;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 32
    invoke-interface {p2}, Lcom/squareup/shared/pricing/engine/rules/RuleSet;->getRules()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;

    .line 33
    invoke-interface {v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getValidity()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 36
    :cond_0
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-interface {p1, v0}, Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;->findTimePeriodsByIds(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    return-object p2
.end method


# virtual methods
.method public load(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Lcom/squareup/shared/pricing/engine/rules/RuleSet;Ljava/util/Map;)Lcom/squareup/shared/pricing/engine/search/SearchBuilder;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;",
            "Lcom/squareup/shared/pricing/engine/rules/RuleSet;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/shared/pricing/engine/search/SearchBuilder;"
        }
    .end annotation

    .line 23
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;->readAllProductSets()Ljava/util/Map;

    move-result-object v1

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/pricing/engine/search/SearchLoader;->loadPeriods(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Lcom/squareup/shared/pricing/engine/rules/RuleSet;)Ljava/util/Map;

    move-result-object v2

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/pricing/engine/search/SearchLoader;->loadDiscounts(Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;Lcom/squareup/shared/pricing/engine/rules/RuleSet;)Ljava/util/Map;

    move-result-object v3

    .line 27
    new-instance p1, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;

    move-object v0, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/pricing/engine/search/SearchBuilder;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/rules/RuleSet;Ljava/util/Map;)V

    return-object p1
.end method
