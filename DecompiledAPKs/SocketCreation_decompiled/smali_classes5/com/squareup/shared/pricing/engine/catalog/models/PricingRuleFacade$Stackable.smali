.class public final enum Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;
.super Ljava/lang/Enum;
.source "PricingRuleFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Stackable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

.field public static final enum BASE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

.field public static final enum EXCLUSIVE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

.field public static final enum STACKABLE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 43
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    const/4 v1, 0x0

    const-string v2, "BASE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->BASE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    .line 44
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    const/4 v2, 0x1

    const-string v3, "STACKABLE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->STACKABLE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    .line 45
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    const/4 v3, 0x2

    const-string v4, "EXCLUSIVE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->EXCLUSIVE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    .line 42
    sget-object v4, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->BASE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->STACKABLE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->EXCLUSIVE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->$VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;
    .locals 1

    .line 42
    const-class v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->$VALUES:[Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    invoke-virtual {v0}, [Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    return-object v0
.end method
