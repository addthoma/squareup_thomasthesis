.class public Lcom/squareup/shared/pricing/engine/search/Source;
.super Ljava/lang/Object;
.source "Source.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/search/Provider;


# instance fields
.field private final discountValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final eligible:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

.field private matchedExclusive:Ljava/math/BigDecimal;

.field private final matchedQuantity:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field private matchedStackable:Ljava/math/BigDecimal;

.field private final parents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Provider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 36
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedQuantity:Ljava/util/Map;

    .line 37
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->discountValue:Ljava/util/Map;

    .line 38
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->eligible:Ljava/util/Map;

    .line 39
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->parents:Ljava/util/List;

    .line 40
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedExclusive:Ljava/math/BigDecimal;

    .line 41
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedStackable:Ljava/math/BigDecimal;

    return-void
.end method

.method private getCandidates(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/math/BigDecimal;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p4

    .line 88
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 89
    iget-object v3, v0, Lcom/squareup/shared/pricing/engine/search/Source;->eligible:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/squareup/shared/pricing/engine/search/Source;->eligible:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    goto/16 :goto_0

    .line 93
    :cond_0
    iget-object v3, v0, Lcom/squareup/shared/pricing/engine/search/Source;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v3}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getClientServerIds()Lcom/squareup/shared/pricing/models/ClientServerIds;

    move-result-object v3

    .line 100
    sget-object v4, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    move-object/from16 v5, p3

    invoke-static {v5, v3, v4}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->fetch(Ljava/util/Map;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 104
    invoke-virtual {p0, p1}, Lcom/squareup/shared/pricing/engine/search/Source;->matchableQuantity(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Ljava/math/BigDecimal;

    move-result-object v13

    .line 109
    invoke-virtual {p0, p1}, Lcom/squareup/shared/pricing/engine/search/Source;->matchableQuantity(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    sget-object v6, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->max(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    if-eqz v1, :cond_1

    .line 114
    invoke-virtual {v5, v1}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    .line 120
    :cond_1
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    move-object/from16 v6, p2

    invoke-static {v6, v3, v1}, Lcom/squareup/shared/pricing/engine/util/MapUtils;->fetch(Ljava/util/Map;Ljava/lang/Object;Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 125
    invoke-virtual {v1, v4}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->max(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v7

    .line 129
    invoke-virtual {v5, v7}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 131
    iget-object v3, v0, Lcom/squareup/shared/pricing/engine/search/Source;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    invoke-virtual {v3}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getUnitPrice()Lcom/squareup/shared/pricing/models/MonetaryAmount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/pricing/models/MonetaryAmount;->getAmount()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    if-eqz p6, :cond_2

    .line 132
    invoke-virtual/range {p6 .. p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v8, v3, v5

    if-gez v8, :cond_2

    .line 133
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    :cond_2
    if-eqz p7, :cond_3

    .line 135
    invoke-virtual/range {p7 .. p7}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v8, v3, v5

    if-lez v8, :cond_3

    .line 136
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 139
    :cond_3
    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v7, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    if-lez v3, :cond_4

    .line 140
    new-instance v3, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;

    iget-object v6, v0, Lcom/squareup/shared/pricing/engine/search/Source;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 143
    invoke-direct {p0, p1}, Lcom/squareup/shared/pricing/engine/search/Source;->offset(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Ljava/math/BigDecimal;

    move-result-object v8

    iget-object v4, v0, Lcom/squareup/shared/pricing/engine/search/Source;->discountValue:Ljava/util/Map;

    .line 145
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v12, 0x1

    move-object v5, v3

    move-object v9, v13

    invoke-direct/range {v5 .. v12}, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;-><init>(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;JZ)V

    .line 140
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_4
    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    if-lez v3, :cond_5

    .line 150
    new-instance v3, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;

    iget-object v6, v0, Lcom/squareup/shared/pricing/engine/search/Source;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 153
    invoke-direct {p0, p1}, Lcom/squareup/shared/pricing/engine/search/Source;->offset(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Ljava/math/BigDecimal;

    move-result-object v8

    iget-object v4, v0, Lcom/squareup/shared/pricing/engine/search/Source;->discountValue:Ljava/util/Map;

    .line 155
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v12, 0x0

    move-object v5, v3

    move-object v7, v1

    move-object v9, v13

    invoke-direct/range {v5 .. v12}, Lcom/squareup/shared/pricing/engine/search/SourceCandidate;-><init>(Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;Ljava/math/BigDecimal;Ljava/math/BigDecimal;Ljava/math/BigDecimal;JZ)V

    .line 150
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_0
    return-object v2
.end method

.method private offset(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Ljava/math/BigDecimal;
    .locals 2

    .line 50
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getStackable()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->EXCLUSIVE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    if-ne v0, v1, :cond_0

    .line 51
    iget-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedStackable:Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedExclusive:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    goto :goto_0

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedQuantity:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedExclusive:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public addParent(Lcom/squareup/shared/pricing/engine/search/Provider;)V
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->parents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearParents()V
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->parents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public commitMatch(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/math/BigDecimal;)V
    .locals 4

    .line 216
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedQuantity:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedQuantity:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v2, p2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getStackable()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    move-result-object p1

    sget-object v0, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->EXCLUSIVE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    if-ne p1, v0, :cond_0

    .line 218
    iget-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedExclusive:Ljava/math/BigDecimal;

    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedExclusive:Ljava/math/BigDecimal;

    goto :goto_0

    .line 221
    :cond_0
    iget-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedStackable:Ljava/math/BigDecimal;

    invoke-virtual {p1, p2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedStackable:Ljava/math/BigDecimal;

    :goto_0
    return-void
.end method

.method public exhaust(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/math/BigDecimal;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;"
        }
    .end annotation

    .line 170
    invoke-direct/range {p0 .. p7}, Lcom/squareup/shared/pricing/engine/search/Source;->getCandidates(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Provider;",
            ">;"
        }
    .end annotation

    .line 187
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getParents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Provider;",
            ">;"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->parents:Ljava/util/List;

    return-object v0
.end method

.method public matchableQuantity(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Ljava/math/BigDecimal;
    .locals 2

    .line 72
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getStackable()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->EXCLUSIVE:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    if-ne v0, v1, :cond_0

    .line 75
    iget-object p1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 73
    invoke-virtual {p1}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getQuantity()Ljava/math/BigDecimal;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedStackable:Ljava/math/BigDecimal;

    .line 74
    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedExclusive:Ljava/math/BigDecimal;

    .line 75
    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    goto :goto_0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->itemizationDetails:Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;

    .line 76
    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/search/ItemizationDetails;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedQuantity:Ljava/util/Map;

    .line 77
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedExclusive:Ljava/math/BigDecimal;

    .line 78
    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public registerRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;JZ)V
    .locals 3

    .line 206
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->matchedQuantity:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/search/Source;->discountValue:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    iget-object p2, p0, Lcom/squareup/shared/pricing/engine/search/Source;->eligible:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-interface {p2, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public satisfy(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/math/BigDecimal;",
            ">;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    .line 183
    invoke-direct/range {v0 .. v7}, Lcom/squareup/shared/pricing/engine/search/Source;->getCandidates(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Ljava/util/Map;Ljava/util/Map;Ljava/math/BigDecimal;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Ljava/lang/Long;Ljava/lang/Long;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
