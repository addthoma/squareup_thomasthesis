.class public Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;
.super Ljava/lang/Object;
.source "CatalogLocalWrapper.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/CatalogFacade$Local;


# instance fields
.field private catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/Catalog$Local;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    return-void
.end method


# virtual methods
.method public findActivePricingRules(Ljava/util/TimeZone;Ljava/util/Date;Ljava/util/Date;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    const-class v1, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;

    .line 39
    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;

    .line 44
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;->activeRuleIdsForRange(Ljava/util/TimeZone;Ljava/util/Date;Ljava/util/Date;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 45
    iget-object p1, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    const-class p2, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    .line 46
    invoke-interface {p1, p2, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 48
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 50
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    .line 51
    new-instance v0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;

    invoke-direct {v0, p3}, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;-><init>(Lcom/squareup/shared/catalog/models/CatalogPricingRule;)V

    .line 55
    invoke-virtual {v0}, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->getApplicationMode()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ApplicationMode;

    move-result-object p3

    sget-object v1, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ApplicationMode;->AUTOMATIC:Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ApplicationMode;

    if-ne p3, v1, :cond_0

    .line 56
    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object p2
.end method

.method public findActivePricingRules(Ljava/util/TimeZone;Ljava/util/Date;Ljava/util/Date;Ljava/util/Set;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation

    .line 68
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public findDiscountsByIds(Ljava/util/Set;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/DiscountFacade;",
            ">;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    const-class v1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 86
    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 89
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 90
    new-instance v2, Lcom/squareup/shared/pricing/engine/catalog/client/DiscountWrapper;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-direct {v2, v3}, Lcom/squareup/shared/pricing/engine/catalog/client/DiscountWrapper;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    .line 91
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 92
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public findPricingRulesByIds(Ljava/util/Set;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    const-class v1, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    .line 100
    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 103
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 104
    new-instance v2, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-direct {v2, v3}, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;-><init>(Lcom/squareup/shared/catalog/models/CatalogPricingRule;)V

    .line 105
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 106
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public findProductSetsByIds(Ljava/util/Set;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    const-class v1, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 114
    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 117
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 118
    new-instance v2, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    invoke-direct {v2, v3}, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;-><init>(Lcom/squareup/shared/catalog/models/CatalogProductSet;)V

    .line 119
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 120
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public findTimePeriodsByIds(Ljava/util/Set;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/TimePeriodFacade;",
            ">;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    const-class v1, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    .line 128
    invoke-interface {v0, v1, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p1

    .line 130
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 131
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 132
    new-instance v2, Lcom/squareup/shared/pricing/engine/catalog/client/TimePeriodWrapper;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    invoke-direct {v2, v3}, Lcom/squareup/shared/pricing/engine/catalog/client/TimePeriodWrapper;-><init>(Lcom/squareup/shared/catalog/models/CatalogTimePeriod;)V

    .line 133
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 134
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public readAllProductSets()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/CatalogLocalWrapper;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllProductSets()Ljava/util/List;

    move-result-object v0

    .line 74
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 75
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    .line 76
    new-instance v3, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;

    invoke-direct {v3, v2}, Lcom/squareup/shared/pricing/engine/catalog/client/ProductSetWrapper;-><init>(Lcom/squareup/shared/catalog/models/CatalogProductSet;)V

    .line 77
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogProductSet;->getId()Ljava/lang/String;

    move-result-object v2

    .line 78
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method
