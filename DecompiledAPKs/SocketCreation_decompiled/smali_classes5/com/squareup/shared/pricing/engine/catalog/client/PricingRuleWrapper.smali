.class public Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;
.super Ljava/lang/Object;
.source "PricingRuleWrapper.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;


# instance fields
.field private rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogPricingRule;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    return-void
.end method


# virtual methods
.method public getApplicationMode()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ApplicationMode;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getApplicationMode()Lcom/squareup/api/items/PricingRule$ApplicationMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/items/PricingRule$ApplicationMode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ApplicationMode;->valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ApplicationMode;

    move-result-object v0

    return-object v0
.end method

.method public getApplyOrMatchProductSetId()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getApplyOrMatchProductSetId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApplyProductSetId()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getApplyProductSetId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDiscountId()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getDiscountId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDiscountTargetScope()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getDiscountTargetScope()Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;->valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$DiscountTargetScope;

    move-result-object v0

    return-object v0
.end method

.method public getExcludeProductSetId()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getExcludeProductSetId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExcludeStrategy()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getExcludeStrategy()Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;->valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$ExcludeStrategy;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMatchProductSetId()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getMatchProductSetId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxApplicationsPerAttachment()I
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getMaxApplicationsPerAttachment()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStackable()Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getStackable()Lcom/squareup/api/items/PricingRule$Stackable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/items/PricingRule$Stackable;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;->valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade$Stackable;

    move-result-object v0

    return-object v0
.end method

.method public getValidFrom()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidFrom()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValidUntil()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidUntil()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValidity()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/catalog/client/PricingRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogPricingRule;->getValidity()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
