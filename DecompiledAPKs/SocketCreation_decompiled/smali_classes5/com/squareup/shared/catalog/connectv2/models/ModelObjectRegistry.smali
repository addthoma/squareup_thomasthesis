.class public interface abstract Lcom/squareup/shared/catalog/connectv2/models/ModelObjectRegistry;
.super Ljava/lang/Object;
.source "ModelObjectRegistry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObject;",
        "T::",
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract keyFromRaw(JLjava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey<",
            "*>;"
        }
    .end annotation
.end method

.method public abstract parse([B)Lcom/squareup/shared/catalog/connectv2/models/ModelObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ObjectT:TM;>([B)TObjectT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract typeFromModelClass(Ljava/lang/Class;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ObjectT:TM;>(",
            "Ljava/lang/Class<",
            "TObjectT;>;)",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;"
        }
    .end annotation
.end method

.method public abstract typeFromRaw(J)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)TT;"
        }
    .end annotation
.end method
