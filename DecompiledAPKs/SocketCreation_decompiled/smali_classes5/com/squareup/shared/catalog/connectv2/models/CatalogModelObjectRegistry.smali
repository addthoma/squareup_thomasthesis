.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;
.super Ljava/lang/Object;
.source "CatalogModelObjectRegistry.java"

# interfaces
.implements Lcom/squareup/shared/catalog/connectv2/models/ModelObjectRegistry;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectRegistry<",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;",
        ">;"
    }
.end annotation


# static fields
.field public static INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public keyFromRaw(JLjava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectRegistry$$CC;->keyFromRaw$$dflt$$(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectRegistry;JLjava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    move-result-object p1

    return-object p1
.end method

.method public objectFromProtoObject(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
    .locals 3

    .line 31
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->getValue()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->typeFromRaw(J)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    move-result-object v0

    .line 33
    sget-object v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry$1;->$SwitchMap$com$squareup$shared$catalog$connectv2$models$CatalogConnectV2ObjectType:[I

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 57
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Do not support type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 55
    :pswitch_0
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Tax;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Tax;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 53
    :pswitch_1
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogResource;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 51
    :pswitch_2
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 49
    :pswitch_3
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ModifierList;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ModifierList;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 47
    :pswitch_4
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 45
    :pswitch_5
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 43
    :pswitch_6
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 41
    :pswitch_7
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 39
    :pswitch_8
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 37
    :pswitch_9
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Discount;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Discount;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    .line 35
    :pswitch_a
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Category;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Category;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public parse([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">([B)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 11
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->objectFromProtoObject(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic parse([B)Lcom/squareup/shared/catalog/connectv2/models/ModelObject;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 6
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->parse([B)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p1

    return-object p1
.end method

.method public typeFromModelClass(Ljava/lang/Class;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;"
        }
    .end annotation

    .line 18
    invoke-static {}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->values()[Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 19
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->getObjectClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 23
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Do not support class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public typeFromRaw(J)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;
    .locals 0

    .line 27
    invoke-static {p1, p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;->fromStableIdentifier(J)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic typeFromRaw(J)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;
    .locals 0

    .line 6
    invoke-virtual {p0, p1, p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->typeFromRaw(J)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ObjectType;

    move-result-object p1

    return-object p1
.end method
