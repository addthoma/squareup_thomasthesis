.class public final Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ModifierList;
.super Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
.source "CatalogConnectV2ModifierList.java"


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 1

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    .line 16
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    const-string v0, "Modifier list data"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method
