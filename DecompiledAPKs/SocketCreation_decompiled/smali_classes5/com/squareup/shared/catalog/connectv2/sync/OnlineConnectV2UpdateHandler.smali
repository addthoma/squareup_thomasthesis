.class public Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;
.super Ljava/lang/Object;
.source "OnlineConnectV2UpdateHandler.java"


# instance fields
.field private final catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

.field private final catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

.field private final endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

.field private final fileThread:Ljava/util/concurrent/Executor;

.field private final httpThread:Ljava/util/concurrent/Executor;

.field private final mainThread:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/CatalogStoreProvider;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    .line 45
    iput-object p2, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    .line 46
    iput-object p3, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    .line 47
    iput-object p4, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->mainThread:Ljava/util/concurrent/Executor;

    .line 48
    iput-object p5, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->fileThread:Ljava/util/concurrent/Executor;

    .line 49
    iput-object p6, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->httpThread:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private handleResponseErrors(Ljava/util/List;Lcom/squareup/shared/catalog/sync/SyncCallback;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "*>;)Z"
        }
    .end annotation

    .line 134
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 138
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/resources/Error;

    .line 140
    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Error;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 142
    :cond_1
    new-instance p1, Lcom/squareup/shared/catalog/sync/SyncError;

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v1, v0}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithError(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncError;)V

    const/4 p1, 0x1

    return p1
.end method

.method private handleSyncErrorAndException(Lcom/squareup/shared/catalog/sync/SyncResult;Lcom/squareup/shared/catalog/sync/SyncCallback;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "*>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "*>;)Z"
        }
    .end annotation

    .line 114
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->mainThread:Ljava/util/concurrent/Executor;

    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithError(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncError;)V

    return v1

    .line 121
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;
    :try_end_0
    .catch Lcom/squareup/shared/catalog/CatalogException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x0

    return p1

    :catch_0
    move-exception p1

    .line 123
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return v1
.end method


# virtual methods
.method final synthetic lambda$upsert$0$OnlineConnectV2UpdateHandler(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->catalogLocal:Lcom/squareup/shared/catalog/Catalog$Local;

    invoke-interface {v0, p1}, Lcom/squareup/shared/catalog/Catalog$Local;->writeConnectV2Object(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    :catchall_0
    move-exception p1

    .line 97
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method final synthetic lambda$upsert$1$OnlineConnectV2UpdateHandler(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V
    .locals 1

    .line 66
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;-><init>()V

    .line 67
    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;->getBackingObject()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->object(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    move-result-object p1

    .line 71
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->endpoint:Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;->upsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    .line 74
    invoke-direct {p0, p1, p3}, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->handleSyncErrorAndException(Lcom/squareup/shared/catalog/sync/SyncResult;Lcom/squareup/shared/catalog/sync/SyncCallback;)Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 79
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;

    .line 82
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-direct {p0, p2, p3}, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->handleResponseErrors(Ljava/util/List;Lcom/squareup/shared/catalog/sync/SyncCallback;)Z

    move-result p2

    if-eqz p2, :cond_1

    return-void

    .line 86
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    const-string/jumbo p2, "updated object"

    invoke-static {p1, p2}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 87
    sget-object p2, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->INSTANCE:Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;

    .line 88
    invoke-virtual {p2, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogModelObjectRegistry;->objectFromProtoObject(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;

    move-result-object p1

    if-eqz p4, :cond_2

    .line 92
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->isCloseEnqueued()Z

    move-result p2

    if-nez p2, :cond_2

    .line 93
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->fileThread:Ljava/util/concurrent/Executor;

    new-instance p4, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler$$Lambda$1;

    invoke-direct {p4, p0, p1, p3}, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-interface {p2, p4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 103
    :cond_2
    iget-object p2, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-static {p2, p3, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    :goto_0
    return-void
.end method

.method public upsert(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            "Z",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "idempotency key"

    .line 62
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "CatalogConnectV2Object"

    .line 63
    invoke-static {p2, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->httpThread:Ljava/util/concurrent/Executor;

    new-instance v7, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler$$Lambda$0;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    invoke-interface {v0, v7}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
