.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;
.super Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
.source "CatalogConnectV2Item.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 1

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    .line 21
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    const-string v0, "Item data"

    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getAllItemOptionIds()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 30
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->item_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;

    .line 31
    iget-object v2, v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;->item_option_id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getAllVariations()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 38
    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    iget-object v1, v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 39
    new-instance v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;

    invoke-direct {v3, v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    .line 40
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Item;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
