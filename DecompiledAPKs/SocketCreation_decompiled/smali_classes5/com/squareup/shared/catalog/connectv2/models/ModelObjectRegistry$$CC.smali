.class public abstract synthetic Lcom/squareup/shared/catalog/connectv2/models/ModelObjectRegistry$$CC;
.super Ljava/lang/Object;


# direct methods
.method public static keyFromRaw$$dflt$$(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectRegistry;JLjava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;
    .locals 0

    .line 19
    invoke-interface {p0, p1, p2}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectRegistry;->typeFromRaw(J)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;

    move-result-object p0

    .line 20
    invoke-static {p0, p3}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->create(Lcom/squareup/shared/catalog/connectv2/models/ModelObjectType;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    move-result-object p0

    return-object p0
.end method
