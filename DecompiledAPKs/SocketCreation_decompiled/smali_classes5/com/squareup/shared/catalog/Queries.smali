.class public Lcom/squareup/shared/catalog/Queries;
.super Ljava/lang/Object;
.source "Queries.java"


# static fields
.field private static final ITEM_TYPES:Ljava/lang/String; = "%item_types%"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs buildParamsWithItemTypes(Ljava/util/List;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;[",
            "Ljava/lang/String;",
            ")[",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 57
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    array-length v2, p1

    :goto_0
    add-int/2addr v0, v2

    new-array v0, v0, [Ljava/lang/String;

    .line 59
    array-length v2, p1

    const/4 v3, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v4, p1, v1

    add-int/lit8 v5, v3, 0x1

    .line 60
    aput-object v4, v0, v3

    add-int/lit8 v1, v1, 0x1

    move v3, v5

    goto :goto_1

    .line 62
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/Item$Type;

    add-int/lit8 v1, v3, 0x1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/api/items/Item$Type;->getValue()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v3

    move v3, v1

    goto :goto_2

    :cond_2
    return-object v0
.end method

.method public static fixedLengthOrdinal(I)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 32
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const-string p0, "%08d"

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static groupPlaceholder(I)Ljava/lang/String;
    .locals 3

    .line 41
    new-array p0, p0, [Ljava/lang/String;

    const-string v0, "?"

    .line 42
    invoke-static {p0, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 43
    array-length v0, p0

    const-string v1, ","

    const/4 v2, 0x0

    invoke-static {p0, v1, v2, v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->join([Ljava/lang/Object;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static intFromCountCursor(Lcom/squareup/shared/sql/SQLCursor;)I
    .locals 2

    .line 21
    :try_start_0
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 22
    invoke-interface {p0, v1}, Lcom/squareup/shared/sql/SQLCursor;->getInt(I)I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return v0

    :cond_0
    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return v1

    :catchall_0
    move-exception v0

    invoke-interface {p0}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw v0
.end method

.method public static varargs rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 48
    invoke-static {p2, p3}, Lcom/squareup/shared/catalog/Queries;->buildParamsWithItemTypes(Ljava/util/List;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p3

    .line 49
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-static {p2}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, "%item_types%"

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1, p3}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p0

    return-object p0
.end method
