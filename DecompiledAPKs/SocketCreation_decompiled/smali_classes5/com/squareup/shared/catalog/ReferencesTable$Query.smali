.class public final enum Lcom/squareup/shared/catalog/ReferencesTable$Query;
.super Ljava/lang/Enum;
.source "ReferencesTable.java"

# interfaces
.implements Lcom/squareup/shared/catalog/HasQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/ReferencesTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Query"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/ReferencesTable$Query;",
        ">;",
        "Lcom/squareup/shared/catalog/HasQuery;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum COUNT_OBJECTS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum COUNT_REFERRERS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum CREATE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum CREATE_INDEX_REFERENT_ID_REFERRER_TYPE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum DELETE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum FIND_ALL_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum FIND_ALL_REFERRER_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum FIND_FIRST_REFERENT_ID:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum FIND_REFERRERS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum RESOLVE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum RESOLVE_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum RESOLVE_MEMBERSHIPS_WITH_POSSIBLE_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum RESOLVE_OUTER:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum RESOLVE_RELATED_IDS_WITH_POSSIBLE_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum RESOLVE_WITH_ITEM_TYPES:Lcom/squareup/shared/catalog/ReferencesTable$Query;

.field public static final enum WRITE:Lcom/squareup/shared/catalog/ReferencesTable$Query;


# instance fields
.field private final query:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 53
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "CREATE TABLE {table} ({referrer_type} INTEGER, {referrer_id} TEXT, {referent_type} INTEGER,  {referent_id} TEXT, PRIMARY KEY ({referrer_id}, {referent_type}))"

    .line 54
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v2, "table"

    const-string v3, "references_table"

    .line 57
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v4, "referrer_type"

    .line 58
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v5, "referrer_id"

    .line 59
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v6, "referent_type"

    .line 60
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v7, "referent_id"

    .line 61
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 63
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x0

    const-string v9, "CREATE"

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->CREATE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 65
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "CREATE INDEX {idx} ON {references_table} ({referent_id}, {referrer_type})"

    .line 66
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v9, "idx"

    const-string v10, "referent_id_referrer_type"

    .line 67
    invoke-virtual {v1, v9, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 68
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 69
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 70
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 72
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x1

    const-string v10, "CREATE_INDEX_REFERENT_ID_REFERRER_TYPE"

    invoke-direct {v0, v10, v9, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->CREATE_INDEX_REFERENT_ID_REFERRER_TYPE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 74
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT COUNT(right.{referent_id}) FROM {references_table} AS left, {references_table} AS right, {objects_table} as objects WHERE left.{referent_id} = ? AND left.{referrer_type} = ? AND left.{referent_type} = ? AND left.{referrer_id} = right.{referrer_id} AND right.{referent_type} = ? AND right.{referent_id} = objects.{object_id} AND (objects.{item_type} IS NULL OR objects.{item_type} IN (%item_types%))"

    .line 75
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 85
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 86
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 87
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 88
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 89
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v10, "objects"

    const-string v11, "objects_table"

    .line 90
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v12, "object_id"

    .line 91
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v13, "item_type"

    .line 92
    invoke-virtual {v1, v13, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 94
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v14, 0x2

    const-string v15, "COUNT_OBJECTS"

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->COUNT_OBJECTS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 96
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT COUNT(*) FROM {references_table} WHERE {referent_id} = ? AND {referrer_type} = ? AND {referent_type} = ?"

    .line 97
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 102
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 103
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 104
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 105
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 106
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 107
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v15, "COUNT_REFERRERS"

    const/4 v14, 0x3

    invoke-direct {v0, v15, v14, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->COUNT_REFERRERS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 109
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "DELETE from {table} WHERE {referrer_id} = ?"

    .line 110
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 111
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 112
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 114
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v14, "DELETE"

    const/4 v15, 0x4

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->DELETE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 116
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT {referent_id} FROM {references_table} WHERE {referrer_id} = ? AND {referrer_type} = ? AND {referent_type} = ? LIMIT 1"

    .line 117
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 122
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 123
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 124
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 125
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 126
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 127
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 128
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v14, "FIND_FIRST_REFERENT_ID"

    const/4 v15, 0x5

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_FIRST_REFERENT_ID:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 130
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT {referent_type}, {referent_id} FROM {references_table} WHERE {referrer_id} = ? AND {referrer_type} = ? AND {referent_type} IN (%referent_types%)"

    .line 131
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 136
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 137
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 138
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 139
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 140
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 141
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 142
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v14, "FIND_ALL_REFERENT_IDS"

    const/4 v15, 0x6

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_ALL_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 144
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT {referrer_type}, {referrer_id} FROM {references_table} WHERE {referent_id} = ? AND {referent_type} = ? AND {referrer_type} IN (%referrer_types%)"

    .line 145
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 150
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 151
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 152
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 153
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 154
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 155
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 156
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v14, "FIND_ALL_REFERRER_IDS"

    const/4 v15, 0x7

    invoke-direct {v0, v14, v15, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_ALL_REFERRER_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 158
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT objects.{object_blob} FROM {references_table} AS ref, {objects_table} AS objects WHERE ref.{referent_id} IN (%referent_in%) AND ref.{referrer_type} = ? AND ref.{referent_type} = ? AND objects.{objects_id} = ref.{referrer_id} ORDER BY objects.{sort_text} COLLATE NOCASE"

    .line 159
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 167
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 168
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v14, "object_blob"

    .line 169
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 170
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 171
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v15, "objects_id"

    .line 172
    invoke-virtual {v1, v15, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 173
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 174
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v15, "sort_text"

    .line 175
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 176
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 177
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v9, "FIND_REFERRERS"

    const/16 v8, 0x8

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_REFERRERS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 179
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT objects.{object_blob} FROM {references_table} AS left, {references_table} AS right, {objects_table} AS objects WHERE left.{referent_id} = ? AND left.{referrer_type} = ? AND left.{referent_type} = ? AND left.{referrer_id} = right.{referrer_id} AND right.{referent_type} = ? AND objects.{object_id} = right.{referent_id} GROUP BY left.{referrer_id} ORDER BY objects.{sort_text} COLLATE NOCASE"

    .line 180
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 192
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 193
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 194
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 195
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 196
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 197
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 198
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 199
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 200
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 201
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 202
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "RESOLVE"

    const/16 v9, 0x9

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 204
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT right.{referent_id} FROM {references_table} AS left, {references_table} AS right WHERE left.{referent_id} = ? AND left.{referrer_type} = right.{referrer_type} AND left.{referent_type} = ? AND left.{referrer_id} = right.{referrer_id} AND right.{referent_type} = ? GROUP BY left.{referrer_id} COLLATE NOCASE"

    .line 205
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 214
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 215
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 216
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 217
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 218
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 219
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 220
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "RESOLVE_IDS"

    const/16 v9, 0xa

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 222
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT objects.{object_blob} FROM {references_table} AS left, {references_table} AS right, {objects_table} AS objects WHERE left.{referent_id} = ? AND left.{referrer_type} = ? AND left.{referrer_id} = right.{referrer_id} AND right.{referent_id} IN (%referent_ids%) AND objects.{object_id} = left.{referrer_id} GROUP BY left.{referrer_id} COLLATE NOCASE"

    .line 223
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 233
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 234
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 235
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 236
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 237
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 238
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 239
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 240
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 241
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "RESOLVE_MEMBERSHIPS_WITH_POSSIBLE_REFERENT_IDS"

    const/16 v9, 0xb

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_MEMBERSHIPS_WITH_POSSIBLE_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 243
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT right.{referent_id} FROM {references_table} AS left, {references_table} AS right WHERE left.{referent_id} = ? AND left.{referrer_type} = right.{referrer_type} AND left.{referent_type} = ? AND left.{referrer_id} = right.{referrer_id} AND right.{referent_type} = ? AND right.{referent_id} IN (%referent_ids%) GROUP BY left.{referrer_id} COLLATE NOCASE"

    .line 244
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 254
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 255
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 256
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 257
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 258
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 259
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 260
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "RESOLVE_RELATED_IDS_WITH_POSSIBLE_REFERENT_IDS"

    const/16 v9, 0xc

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_RELATED_IDS_WITH_POSSIBLE_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 262
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT objects.{object_blob} FROM {references_table} AS left, {references_table} AS right, {objects_table} AS objects WHERE left.{referent_id} = ? AND left.{referrer_type} = ? AND left.{referent_type} = ? AND left.{referrer_id} = right.{referrer_id} AND right.{referent_type} = ? AND objects.{object_id} = right.{referent_id} AND (objects.{item_type} IS NULL OR objects.{item_type} IN (%item_types%)) GROUP BY left.{referrer_id} ORDER BY objects.{sort_text} COLLATE NOCASE"

    .line 263
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 276
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 277
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 278
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 279
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 280
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 281
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 282
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 283
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 284
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 285
    invoke-virtual {v1, v13, v13}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 286
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 287
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "RESOLVE_WITH_ITEM_TYPES"

    const/16 v9, 0xd

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_WITH_ITEM_TYPES:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 289
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "SELECT objects.{object_blob}, relations.referrer, relations.middle FROM {objects_table} AS objects LEFT OUTER JOIN( SELECT left.{referrer_id} AS referrer, right.{referent_id} as referent, middle_objects.{object_blob} as middle FROM {references_table} AS left, {references_table} as right, {objects_table} as target_objects, {objects_table} as middle_objects WHERE left.{referent_id} = ? AND left.{referrer_type} = ? AND left.{referent_type} = ? AND left.{referrer_id} = right.{referrer_id} AND right.{referent_type} = ? AND target_objects.{object_type} = ? AND target_objects.{object_id} = right.{referent_id} AND middle_objects.{object_id} = right.{referrer_id} GROUP BY left.{referrer_id} ) AS relations ON objects.{object_id} = relations.referent WHERE objects.{object_type} = ? ORDER BY objects.{sort_text} COLLATE NOCASE"

    .line 290
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 310
    invoke-virtual {v1, v3, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 311
    invoke-virtual {v1, v11, v10}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 312
    invoke-virtual {v1, v14, v14}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 313
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 314
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 315
    invoke-virtual {v1, v12, v12}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    const-string v8, "object_type"

    const-string v9, "object_type"

    .line 316
    invoke-virtual {v1, v8, v9}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 317
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 318
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 319
    invoke-virtual {v1, v15, v15}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 320
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 321
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v8, "RESOLVE_OUTER"

    const/16 v9, 0xe

    invoke-direct {v0, v8, v9, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_OUTER:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 323
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const-string v1, "INSERT OR REPLACE INTO {table} ({referrer_type}, {referrer_id}, {referent_type}, {referent_id}) VALUES (?, ?, ?, ?)"

    .line 324
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 326
    invoke-virtual {v1, v2, v3}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 327
    invoke-virtual {v1, v4, v4}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 328
    invoke-virtual {v1, v5, v5}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 329
    invoke-virtual {v1, v6, v6}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 330
    invoke-virtual {v1, v7, v7}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object v1

    .line 331
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 332
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "WRITE"

    const/16 v3, 0xf

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/ReferencesTable$Query;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->WRITE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/shared/catalog/ReferencesTable$Query;

    .line 52
    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->CREATE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->CREATE_INDEX_REFERENT_ID_REFERRER_TYPE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->COUNT_OBJECTS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->COUNT_REFERRERS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->DELETE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_FIRST_REFERENT_ID:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_ALL_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_ALL_REFERRER_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_REFERRERS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_MEMBERSHIPS_WITH_POSSIBLE_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_RELATED_IDS_WITH_POSSIBLE_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_WITH_ITEM_TYPES:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_OUTER:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/ReferencesTable$Query;->WRITE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/ReferencesTable$Query;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 336
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 337
    iput-object p3, p0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->query:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/ReferencesTable$Query;
    .locals 1

    .line 52
    const-class v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/ReferencesTable$Query;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/ReferencesTable$Query;
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->$VALUES:[Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/ReferencesTable$Query;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/ReferencesTable$Query;

    return-object v0
.end method


# virtual methods
.method public getQuery()Ljava/lang/String;
    .locals 1

    .line 341
    iget-object v0, p0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->query:Ljava/lang/String;

    return-object v0
.end method
