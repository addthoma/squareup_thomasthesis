.class public final Lcom/squareup/shared/catalog/models/CatalogSurcharge;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogSurcharge.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Surcharge;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getCalculationPhase()Lcom/squareup/api/items/CalculationPhase;
    .locals 2

    .line 35
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurcharge;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    sget-object v1, Lcom/squareup/api/items/Surcharge;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/CalculationPhase;

    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurcharge;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurcharge;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 19
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurcharge;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPercentage()Ljava/lang/String;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurcharge;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->percentage:Ljava/lang/String;

    return-object v0
.end method

.method public getSurchargeType()Lcom/squareup/api/items/Surcharge$Type;
    .locals 2

    .line 27
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurcharge;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->type:Lcom/squareup/api/items/Surcharge$Type;

    sget-object v1, Lcom/squareup/api/items/Surcharge;->DEFAULT_TYPE:Lcom/squareup/api/items/Surcharge$Type;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge$Type;

    return-object v0
.end method

.method public getTaxable()Ljava/lang/Boolean;
    .locals 2

    .line 31
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurcharge;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->taxable:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Surcharge;->DEFAULT_TAXABLE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public getautoGratuity()Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogSurcharge;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Surcharge;

    iget-object v0, v0, Lcom/squareup/api/items/Surcharge;->auto_gratuity:Lcom/squareup/api/items/Surcharge$AutoGratuitySettings;

    return-object v0
.end method
