.class public final Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
.super Ljava/lang/Object;
.source "CatalogItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private dirty:Z

.field private final item:Lcom/squareup/api/items/Item$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>(Lcom/squareup/api/items/Item$Type;)V
    .locals 1

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 146
    new-instance v0, Lcom/squareup/api/items/Item$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Item$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->type(Lcom/squareup/api/items/Item$Type;)Lcom/squareup/api/items/Item$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/Item$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    const/4 p1, 0x1

    .line 147
    iput-boolean p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    return-void
.end method

.method private constructor <init>(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lcom/squareup/api/items/Item$Builder;Z)V
    .locals 0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 158
    iput-object p2, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    .line 159
    iput-boolean p3, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItem;)V
    .locals 0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 152
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    invoke-virtual {p1}, Lcom/squareup/api/items/Item;->newBuilder()Lcom/squareup/api/items/Item$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    const/4 p1, 0x0

    .line 153
    iput-boolean p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    return-void
.end method

.method public static fromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 3

    .line 167
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->dirtyFlag([B)Z

    move-result v0

    .line 168
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObject;->fromByteArrayWithDirtyFlag([B)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p0

    .line 169
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    if-nez v1, :cond_0

    new-instance v1, Lcom/squareup/api/items/Item$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/Item$Builder;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item:Lcom/squareup/api/items/Item;

    invoke-virtual {v1}, Lcom/squareup/api/items/Item;->newBuilder()Lcom/squareup/api/items/Item$Builder;

    move-result-object v1

    .line 170
    :goto_0
    new-instance v2, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-direct {v2, p0, v1, v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;-><init>(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lcom/squareup/api/items/Item$Builder;Z)V

    return-object v2
.end method


# virtual methods
.method public addOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    .line 223
    new-instance v0, Lcom/squareup/api/items/ItemOptionForItem;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/api/items/ItemOptionForItem;-><init>(Ljava/lang/String;)V

    .line 224
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object p1, p1, Lcom/squareup/api/items/Item$Builder;->item_options:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/shared/catalog/models/CatalogItem;
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/Item$Builder;->build()Lcom/squareup/api/items/Item;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item(Lcom/squareup/api/items/Item;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 308
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItem;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public clearImageId()Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 2

    const/4 v0, 0x1

    .line 240
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 241
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Item$Builder;->image(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public getAbbreviation()Ljava/lang/String;
    .locals 2

    .line 252
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->abbreviation:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->color:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->description:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getItemType()Lcom/squareup/api/items/Item$Type;
    .locals 2

    .line 271
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item;->DEFAULT_TYPE:Lcom/squareup/api/items/Item$Type;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item$Type;

    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isEcomAvailable()Z
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->ecom_available:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Item;->DEFAULT_ECOM_AVAILABLE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public removeAllOptions()Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/Item$Builder;->item_options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object p0
.end method

.method public removeOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    .line 229
    new-instance v0, Lcom/squareup/api/items/ItemOptionForItem;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/api/items/ItemOptionForItem;-><init>(Ljava/lang/String;)V

    .line 230
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    iget-object p1, p1, Lcom/squareup/api/items/Item$Builder;->item_options:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public setAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 256
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 257
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->abbreviation(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public setCategoryId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 2

    const/4 v0, 0x1

    .line 281
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    if-eqz p1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/api/items/Item$Builder;->menu_category:Lcom/squareup/api/sync/ObjectId;

    goto :goto_0

    .line 285
    :cond_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/squareup/api/items/Item$Builder;->menu_category:Lcom/squareup/api/sync/ObjectId;

    :goto_0
    return-object p0
.end method

.method public setColor(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 275
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 276
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->color(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 207
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 208
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->description(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public setEcomAvailable(Z)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 301
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 302
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/api/items/Item$Builder;->ecom_available:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 180
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 181
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public setImageId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 2

    const/4 v0, 0x1

    .line 246
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 247
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->image(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public setIsAvailableForPickup(Z)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 197
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 198
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->available_for_pickup(Ljava/lang/Boolean;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public setMerchantCatalogObjectReference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 217
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 218
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Item$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;

    return-object p0
.end method

.method public setSkipsModifierScreen(Z)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 291
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    .line 292
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->item:Lcom/squareup/api/items/Item$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/api/items/Item$Builder;->skips_modifier_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public toByteArray()[B
    .locals 2

    .line 163
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->dirty:Z

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogObject;->toByteArrayWithDirtyPrefix(Lcom/squareup/api/sync/ObjectWrapper;Z)[B

    move-result-object v0

    return-object v0
.end method
