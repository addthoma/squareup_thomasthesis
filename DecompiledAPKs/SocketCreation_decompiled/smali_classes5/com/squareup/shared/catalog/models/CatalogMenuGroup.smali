.class public final Lcom/squareup/shared/catalog/models/CatalogMenuGroup;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogMenuGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogMenuGroup$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Tag;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getColor()Ljava/lang/String;
    .locals 2

    .line 31
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroup;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Tag;

    iget-object v0, v0, Lcom/squareup/api/items/Tag;->color:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 19
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroup;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Tag;

    iget-object v0, v0, Lcom/squareup/api/items/Tag;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTagMemberships()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .line 27
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroup;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Tag;

    iget-object v0, v0, Lcom/squareup/api/items/Tag;->tag_membership:Ljava/util/List;

    return-object v0
.end method

.method public getTagType()Lcom/squareup/api/items/Tag$Type;
    .locals 2

    .line 23
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogMenuGroup;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Tag;

    iget-object v0, v0, Lcom/squareup/api/items/Tag;->type:Lcom/squareup/api/items/Tag$Type;

    sget-object v1, Lcom/squareup/api/items/Tag;->DEFAULT_TYPE:Lcom/squareup/api/items/Tag$Type;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Tag$Type;

    return-object v0
.end method
