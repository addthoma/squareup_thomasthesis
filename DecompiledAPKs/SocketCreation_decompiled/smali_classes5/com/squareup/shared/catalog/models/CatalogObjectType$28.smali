.class final enum Lcom/squareup/shared/catalog/models/CatalogObjectType$28;
.super Lcom/squareup/shared/catalog/models/CatalogObjectType;
.source "CatalogObjectType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogObjectType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4010
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 336
    invoke-direct/range {v0 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;Lcom/squareup/shared/catalog/models/CatalogObjectType$1;)V

    return-void
.end method


# virtual methods
.method messageObject(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/wire/Message;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ")TT;"
        }
    .end annotation

    .line 338
    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper;->configuration:Lcom/squareup/api/items/Configuration;

    return-object p1
.end method

.method setMessage(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lcom/squareup/wire/Message;)V
    .locals 0

    .line 342
    check-cast p2, Lcom/squareup/api/items/Configuration;

    invoke-virtual {p1, p2}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->configuration(Lcom/squareup/api/items/Configuration;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    return-void
.end method
