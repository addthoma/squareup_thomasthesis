.class public abstract enum Lcom/squareup/shared/catalog/models/CatalogObjectType;
.super Ljava/lang/Enum;
.source "CatalogObjectType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum DINING_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum FAVORITES_LIST_POSITION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum FLOOR_PLAN:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum FLOOR_PLAN_TILE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM_FEE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM_MODIFIER_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum MENU:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum MENU_GROUP_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum PLACEHOLDER:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum PRICING_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum SURCHARGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum TAX:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum TAX_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum TICKET_TEMPLATE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum TILE_APPEARANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum TIME_PERIOD:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field public static final enum VOID_REASON:Lcom/squareup/shared/catalog/models/CatalogObjectType;


# instance fields
.field final catalogObjectClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field final catalogObjectConstructor:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field final objectType:Lcom/squareup/api/items/Type;

.field final protoClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 58
    new-instance v6, Lcom/squareup/shared/catalog/models/CatalogObjectType$1;

    const-class v3, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    sget-object v4, Lcom/squareup/api/items/Type;->DISCOUNT:Lcom/squareup/api/items/Type;

    const-class v5, Lcom/squareup/api/items/Discount;

    const-string v1, "DISCOUNT"

    const/4 v2, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/catalog/models/CatalogObjectType$1;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v6, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 68
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$2;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogFavoritesListPosition;

    sget-object v11, Lcom/squareup/api/items/Type;->FAVORITES_LIST_POSITION:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/FavoritesListPosition;

    const-string v8, "FAVORITES_LIST_POSITION"

    const/4 v9, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$2;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->FAVORITES_LIST_POSITION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 79
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$3;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogFloorPlan;

    sget-object v5, Lcom/squareup/api/items/Type;->FLOOR_PLAN:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/FloorPlan;

    const-string v2, "FLOOR_PLAN"

    const/4 v3, 0x2

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$3;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->FLOOR_PLAN:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 89
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$4;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogFloorPlanTile;

    sget-object v11, Lcom/squareup/api/items/Type;->FLOOR_PLAN_TILE:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/FloorPlanTile;

    const-string v8, "FLOOR_PLAN_TILE"

    const/4 v9, 0x3

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$4;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->FLOOR_PLAN_TILE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 99
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$5;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogItem;

    sget-object v5, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/Item;

    const-string v2, "ITEM"

    const/4 v3, 0x4

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$5;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 109
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$6;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    sget-object v11, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/MenuCategory;

    const-string v8, "ITEM_CATEGORY"

    const/4 v9, 0x5

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$6;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 119
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$7;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    sget-object v5, Lcom/squareup/api/items/Type;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/ItemFeeMembership;

    const-string v2, "ITEM_FEE_MEMBERSHIP"

    const/4 v3, 0x6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$7;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 130
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$8;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogItemImage;

    sget-object v11, Lcom/squareup/api/items/Type;->ITEM_IMAGE:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/ItemImage;

    const-string v8, "ITEM_IMAGE"

    const/4 v9, 0x7

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$8;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 140
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$9;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    sget-object v5, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/ItemModifierList;

    const-string v2, "ITEM_MODIFIER_LIST"

    const/16 v3, 0x8

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$9;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 151
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$10;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    sget-object v11, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_OPTION:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/ItemModifierOption;

    const-string v8, "ITEM_MODIFIER_OPTION"

    const/16 v9, 0x9

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$10;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 162
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$11;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    sget-object v5, Lcom/squareup/api/items/Type;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/ItemItemModifierListMembership;

    const-string v2, "ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP"

    const/16 v3, 0xa

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$11;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 173
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$12;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    sget-object v11, Lcom/squareup/api/items/Type;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/ItemPageMembership;

    const-string v8, "ITEM_PAGE_MEMBERSHIP"

    const/16 v9, 0xb

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$12;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 184
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$13;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    sget-object v5, Lcom/squareup/api/items/Type;->ITEM_VARIATION:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/ItemVariation;

    const-string v2, "ITEM_VARIATION"

    const/16 v3, 0xc

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$13;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 194
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$14;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogPage;

    sget-object v11, Lcom/squareup/api/items/Type;->PAGE:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/Page;

    const-string v8, "PAGE"

    const/16 v9, 0xd

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$14;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 204
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$15;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;

    sget-object v5, Lcom/squareup/api/items/Type;->PLACEHOLDER:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/Placeholder;

    const-string v2, "PLACEHOLDER"

    const/16 v3, 0xe

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$15;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PLACEHOLDER:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 214
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$16;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogDiningOption;

    sget-object v11, Lcom/squareup/api/items/Type;->DINING_OPTION:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/DiningOption;

    const-string v8, "DINING_OPTION"

    const/16 v9, 0xf

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$16;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DINING_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 224
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$17;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogMenu;

    sget-object v5, Lcom/squareup/api/items/Type;->MENU:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/Menu;

    const-string v2, "MENU"

    const/16 v3, 0x10

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$17;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 234
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$18;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogMenuGroup;

    sget-object v11, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/Tag;

    const-string v8, "MENU_GROUP"

    const/16 v9, 0x11

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$18;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 244
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$19;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogMenuGroupMembership;

    sget-object v5, Lcom/squareup/api/items/Type;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/MenuGroupMembership;

    const-string v2, "MENU_GROUP_MEMBERSHIP"

    const/16 v3, 0x12

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$19;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 255
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$20;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogPricingRule;

    sget-object v11, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/PricingRule;

    const-string v8, "PRICING_RULE"

    const/16 v9, 0x13

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$20;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRICING_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 265
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$21;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogProductSet;

    sget-object v5, Lcom/squareup/api/items/Type;->PRODUCT_SET:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/ProductSet;

    const-string v2, "PRODUCT_SET"

    const/16 v3, 0x14

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$21;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 275
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$22;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogSurcharge;

    sget-object v11, Lcom/squareup/api/items/Type;->SURCHARGE:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/Surcharge;

    const-string v8, "SURCHARGE"

    const/16 v9, 0x15

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$22;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->SURCHARGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 285
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$23;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;

    sget-object v5, Lcom/squareup/api/items/Type;->SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/SurchargeFeeMembership;

    const-string v2, "SURCHARGE_FEE_MEMBERSHIP"

    const/16 v3, 0x16

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$23;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 296
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$24;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogTax;

    sget-object v11, Lcom/squareup/api/items/Type;->FEE:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/Fee;

    const-string v8, "TAX"

    const/16 v9, 0x17

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$24;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 306
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$25;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogTaxRule;

    sget-object v5, Lcom/squareup/api/items/Type;->TAX_RULE:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/TaxRule;

    const-string v2, "TAX_RULE"

    const/16 v3, 0x18

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$25;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 316
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$26;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogTicketGroup;

    sget-object v11, Lcom/squareup/api/items/Type;->TICKET_GROUP:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/TicketGroup;

    const-string v8, "TICKET_GROUP"

    const/16 v9, 0x19

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$26;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 326
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$27;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    sget-object v5, Lcom/squareup/api/items/Type;->TICKET_TEMPLATE:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/TicketTemplate;

    const-string v2, "TICKET_TEMPLATE"

    const/16 v3, 0x1a

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$27;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_TEMPLATE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 336
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$28;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogConfiguration;

    sget-object v11, Lcom/squareup/api/items/Type;->CONFIGURATION:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/Configuration;

    const-string v8, "TILE_APPEARANCE"

    const/16 v9, 0x1b

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$28;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TILE_APPEARANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 346
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$29;

    const-class v4, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    sget-object v5, Lcom/squareup/api/items/Type;->TIME_PERIOD:Lcom/squareup/api/items/Type;

    const-class v6, Lcom/squareup/api/items/TimePeriod;

    const-string v2, "TIME_PERIOD"

    const/16 v3, 0x1c

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType$29;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TIME_PERIOD:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 356
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$30;

    const-class v10, Lcom/squareup/shared/catalog/models/CatalogVoidReason;

    sget-object v11, Lcom/squareup/api/items/Type;->VOID_REASON:Lcom/squareup/api/items/Type;

    const-class v12, Lcom/squareup/api/items/VoidReason;

    const-string v8, "VOID_REASON"

    const/16 v9, 0x1d

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/shared/catalog/models/CatalogObjectType$30;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->VOID_REASON:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 56
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->FAVORITES_LIST_POSITION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->FLOOR_PLAN:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->FLOOR_PLAN_TILE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PLACEHOLDER:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DINING_OPTION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRICING_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->SURCHARGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_TEMPLATE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TILE_APPEARANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TIME_PERIOD:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->VOID_REASON:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->$VALUES:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;",
            "Lcom/squareup/api/items/Type;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;)V"
        }
    .end annotation

    .line 372
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const-string p1, "catalogObjectClass"

    .line 373
    invoke-static {p3, p1}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Class;

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->catalogObjectClass:Ljava/lang/Class;

    .line 374
    iput-object p4, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->objectType:Lcom/squareup/api/items/Type;

    .line 375
    iput-object p5, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->protoClass:Ljava/lang/Class;

    .line 376
    invoke-direct {p0, p3}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->getObjectWrapperConstructor(Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->catalogObjectConstructor:Ljava/lang/reflect/Constructor;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;Lcom/squareup/shared/catalog/models/CatalogObjectType$1;)V
    .locals 0

    .line 57
    invoke-direct/range {p0 .. p5}, Lcom/squareup/shared/catalog/models/CatalogObjectType;-><init>(Ljava/lang/String;ILjava/lang/Class;Lcom/squareup/api/items/Type;Ljava/lang/Class;)V

    return-void
.end method

.method private getObjectWrapperConstructor(Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Ljava/lang/reflect/Constructor<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    const/4 v2, 0x0

    .line 383
    const-class v3, Lcom/squareup/api/sync/ObjectWrapper;

    aput-object v3, v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 384
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    .line 386
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected a constructor with one ObjectWrapper parameter on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 56
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->$VALUES:[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/models/CatalogObjectType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object v0
.end method


# virtual methods
.method public createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 457
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    return-object v0
.end method

.method createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;
    .locals 1

    if-nez p1, :cond_0

    .line 465
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object p1

    .line 466
    :cond_0
    new-instance v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    return-object p1
.end method

.method public doesProtoHaveMerchantCatalogToken()Z
    .locals 2

    .line 427
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$31;->$SwitchMap$com$squareup$shared$catalog$models$CatalogObjectType:[I

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getCatalogItemClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;"
        }
    .end annotation

    .line 497
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->catalogObjectClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getExtension()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation

    .line 501
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->protoClass:Ljava/lang/Class;

    return-object v0
.end method

.method public getProtoObjectType()Lcom/squareup/api/items/Type;
    .locals 1

    .line 484
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->objectType:Lcom/squareup/api/items/Type;

    return-object v0
.end method

.method public getProtoTypeValue()I
    .locals 1

    .line 489
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->objectType:Lcom/squareup/api/items/Type;

    invoke-virtual {v0}, Lcom/squareup/api/items/Type;->getValue()I

    move-result v0

    return v0
.end method

.method matches(Lcom/squareup/shared/catalog/models/CatalogObject;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;)Z"
        }
    .end annotation

    .line 493
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->catalogObjectClass:Ljava/lang/Class;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method abstract messageObject(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/wire/Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ")TT;"
        }
    .end annotation
.end method

.method public newObjectFromMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/String;",
            "Lcom/squareup/wire/Message;",
            ")TT;"
        }
    .end annotation

    .line 402
    invoke-virtual {p0, p1, p2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapObjectMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ")TT;"
        }
    .end annotation

    .line 395
    :try_start_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->catalogObjectConstructor:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 397
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method abstract setMessage(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lcom/squareup/wire/Message;)V
.end method

.method public wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;
    .locals 2

    .line 449
    new-instance v0, Lcom/squareup/api/sync/ObjectType$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectType$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->objectType:Lcom/squareup/api/items/Type;

    .line 450
    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectType$Builder;->type(Lcom/squareup/api/items/Type;)Lcom/squareup/api/sync/ObjectType$Builder;

    move-result-object v0

    .line 451
    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectType$Builder;->build()Lcom/squareup/api/sync/ObjectType;

    move-result-object v0

    .line 452
    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/api/sync/ObjectId$Builder;->type(Lcom/squareup/api/sync/ObjectType;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    return-object p1
.end method

.method public wrapObjectMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/api/sync/ObjectWrapper;
    .locals 2

    .line 471
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->protoClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    new-instance v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/ObjectWrapper$Builder;-><init>()V

    .line 478
    invoke-virtual {p0, v0, p2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->setMessage(Lcom/squareup/api/sync/ObjectWrapper$Builder;Lcom/squareup/wire/Message;)V

    .line 479
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    return-object p1

    .line 472
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Wrong message "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " for extension message type "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->protoClass:Ljava/lang/Class;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
