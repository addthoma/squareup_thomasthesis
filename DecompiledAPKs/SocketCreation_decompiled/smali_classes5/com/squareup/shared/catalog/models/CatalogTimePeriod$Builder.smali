.class public final Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;
.super Ljava/lang/Object;
.source "CatalogTimePeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogTimePeriod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TIME_PERIOD:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 263
    new-instance v0, Lcom/squareup/api/items/TimePeriod$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TimePeriod$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TimePeriod$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogTimePeriod;)V
    .locals 0

    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 268
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period:Lcom/squareup/api/items/TimePeriod;

    invoke-virtual {p1}, Lcom/squareup/api/items/TimePeriod;->newBuilder()Lcom/squareup/api/items/TimePeriod$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogTimePeriod;
    .locals 2

    .line 315
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/TimePeriod$Builder;->build()Lcom/squareup/api/items/TimePeriod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->time_period(Lcom/squareup/api/items/TimePeriod;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 316
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogTimePeriod;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public getDuration()Ljava/lang/String;
    .locals 2

    .line 304
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TimePeriod$Builder;->duration:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TimePeriod$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRRule()Ljava/lang/String;
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TimePeriod$Builder;->rrule:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getStartsAt()Ljava/lang/String;
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TimePeriod$Builder;->starts_at:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public setDuration(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TimePeriod$Builder;->duration(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;

    return-object p0
.end method

.method public setIdForTest(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;
    .locals 2

    .line 308
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 309
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 310
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TimePeriod$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TimePeriod$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;

    return-object p0
.end method

.method public setRRule(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TimePeriod$Builder;->rrule(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;

    return-object p0
.end method

.method public setStartsAt(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;
    .locals 1

    .line 277
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTimePeriod$Builder;->timePeriod:Lcom/squareup/api/items/TimePeriod$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TimePeriod$Builder;->starts_at(Ljava/lang/String;)Lcom/squareup/api/items/TimePeriod$Builder;

    return-object p0
.end method
