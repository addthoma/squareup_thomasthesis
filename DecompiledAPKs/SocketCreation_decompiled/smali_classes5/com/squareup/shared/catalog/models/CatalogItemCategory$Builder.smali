.class public final Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
.super Ljava/lang/Object;
.source "CatalogItemCategory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItemCategory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final category:Lcom/squareup/api/items/MenuCategory$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 60
    new-instance v0, Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/MenuCategory$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/MenuCategory$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemCategory;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 65
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {p1}, Lcom/squareup/api/items/MenuCategory;->newBuilder()Lcom/squareup/api/items/MenuCategory$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogItemCategory;
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/MenuCategory$Builder;->build()Lcom/squareup/api/items/MenuCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->menu_category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 113
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public getAbbreviation()Ljava/lang/String;
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory$Builder;->abbreviation:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory$Builder;->color:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory$Builder;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/MenuCategory$Builder;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/MenuCategory;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public setAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/MenuCategory$Builder;->abbreviation(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;

    return-object p0
.end method

.method public setColor(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/MenuCategory$Builder;->color(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;

    return-object p0
.end method

.method public setIdForTest(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/ObjectId;

    .line 106
    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId;->newBuilder()Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    .line 107
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/MenuCategory$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/MenuCategory$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/MenuCategory$Builder;

    return-object p0
.end method

.method public setOrdinal(I)Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemCategory$Builder;->category:Lcom/squareup/api/items/MenuCategory$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/MenuCategory$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/MenuCategory$Builder;

    return-object p0
.end method
