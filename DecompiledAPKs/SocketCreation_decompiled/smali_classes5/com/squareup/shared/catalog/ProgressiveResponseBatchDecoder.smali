.class public final Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;
.super Ljava/lang/Object;
.source "ProgressiveResponseBatchDecoder.java"


# instance fields
.field private final builder:Lcom/squareup/api/rpc/ResponseBatch$Builder;

.field private final in:Ljava/io/InputStream;

.field private final protoToken:J

.field private final reader:Lcom/squareup/wire/ProtoReader;

.field private responseBatchWithoutResponses:Lcom/squareup/api/rpc/ResponseBatch;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "InputStream"

    .line 26
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->in:Ljava/io/InputStream;

    .line 27
    new-instance v0, Lcom/squareup/wire/ProtoReader;

    invoke-static {p1}, Lokio/Okio;->source(Ljava/io/InputStream;)Lokio/Source;

    move-result-object p1

    invoke-static {p1}, Lokio/Okio;->buffer(Lokio/Source;)Lokio/BufferedSource;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/wire/ProtoReader;-><init>(Lokio/BufferedSource;)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->reader:Lcom/squareup/wire/ProtoReader;

    .line 28
    iget-object p1, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->reader:Lcom/squareup/wire/ProtoReader;

    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->protoToken:J

    .line 29
    new-instance p1, Lcom/squareup/api/rpc/ResponseBatch$Builder;

    invoke-direct {p1}, Lcom/squareup/api/rpc/ResponseBatch$Builder;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->builder:Lcom/squareup/api/rpc/ResponseBatch$Builder;

    return-void
.end method


# virtual methods
.method public cleanUp()V
    .locals 1

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public getResponseBatchWithoutResponses()Lcom/squareup/api/rpc/ResponseBatch;
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->responseBatchWithoutResponses:Lcom/squareup/api/rpc/ResponseBatch;

    if-eqz v0, :cond_0

    return-object v0

    .line 67
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The data stream hasn\'t been fully decoded yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextResponse()Lcom/squareup/api/rpc/Response;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 40
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->reader:Lcom/squareup/wire/ProtoReader;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->reader:Lcom/squareup/wire/ProtoReader;

    invoke-virtual {v1}, Lcom/squareup/wire/ProtoReader;->peekFieldEncoding()Lcom/squareup/wire/FieldEncoding;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/squareup/wire/FieldEncoding;->rawProtoAdapter()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->reader:Lcom/squareup/wire/ProtoReader;

    invoke-virtual {v2, v3}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v2

    .line 50
    iget-object v3, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->builder:Lcom/squareup/api/rpc/ResponseBatch$Builder;

    invoke-virtual {v3, v0, v1, v2}, Lcom/squareup/api/rpc/ResponseBatch$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->builder:Lcom/squareup/api/rpc/ResponseBatch$Builder;

    sget-object v1, Lcom/squareup/api/rpc/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->reader:Lcom/squareup/wire/ProtoReader;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/rpc/Error;

    invoke-virtual {v0, v1}, Lcom/squareup/api/rpc/ResponseBatch$Builder;->error(Lcom/squareup/api/rpc/Error;)Lcom/squareup/api/rpc/ResponseBatch$Builder;

    goto :goto_0

    .line 43
    :cond_1
    sget-object v0, Lcom/squareup/api/rpc/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->reader:Lcom/squareup/wire/ProtoReader;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/rpc/Response;

    return-object v0

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->reader:Lcom/squareup/wire/ProtoReader;

    iget-wide v1, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->protoToken:J

    invoke-virtual {v0, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessage(J)V

    .line 55
    iget-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->builder:Lcom/squareup/api/rpc/ResponseBatch$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/rpc/ResponseBatch$Builder;->build()Lcom/squareup/api/rpc/ResponseBatch;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->responseBatchWithoutResponses:Lcom/squareup/api/rpc/ResponseBatch;

    const/4 v0, 0x0

    return-object v0
.end method
