.class public Lcom/squareup/shared/catalog/sync/SyncTasks;
.super Ljava/lang/Object;
.source "SyncTasks.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/sync/SyncTasks$SyncFailure;
    }
.end annotation


# static fields
.field private static final EXPLODE_ON_EXCEPTION:Lcom/squareup/shared/catalog/sync/SyncCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncTasks$$Lambda$1;->$instance:Lcom/squareup/shared/catalog/sync/SyncCallback;

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncTasks;->EXPLODE_ON_EXCEPTION:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 24
    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncTasks;->EXPLODE_ON_EXCEPTION:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-object v0
.end method

.method static handleRpcError(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/api/rpc/Error;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSync;",
            "Lcom/squareup/api/rpc/Error;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p1, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 33
    iget-object v1, p1, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    const-string v2, "]: "

    if-eqz v1, :cond_1

    .line 34
    iget-object p0, p1, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 35
    sget-object p1, Lcom/squareup/api/rpc/Error$ServerErrorCode;->INTERNAL_SERVER_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    if-ne p0, p1, :cond_0

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 36
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Catalog Server Error ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 37
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-direct {v0, p1, p0}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p0

    return-object p0

    .line 39
    :cond_1
    iget-object p1, p1, Lcom/squareup/api/rpc/Error;->sync_error_code:Lcom/squareup/api/sync/SyncErrorCode;

    sget-object v1, Lcom/squareup/api/sync/SyncErrorCode;->SESSION_EXPIRED:Lcom/squareup/api/sync/SyncErrorCode;

    invoke-static {p1, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/sync/SyncErrorCode;

    .line 40
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Catalog Sync Error ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 41
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncTasks$1;->$SwitchMap$com$squareup$api$sync$SyncErrorCode:[I

    invoke-virtual {p1}, Lcom/squareup/api/sync/SyncErrorCode;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_3

    const/4 p0, 0x2

    if-eq p1, p0, :cond_2

    .line 59
    new-instance p0, Lcom/squareup/shared/catalog/sync/SyncError;

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-direct {p0, p1, v0}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p0

    return-object p0

    .line 55
    :cond_2
    new-instance p0, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/String;)V

    const-string p1, "Catalog received an UNAUTHORIZED response"

    .line 56
    invoke-static {p0, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p0

    return-object p0

    .line 45
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->requestNewSessionId()V

    .line 46
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p0

    return-object p0
.end method

.method static final synthetic lambda$syncComplete$0$SyncTasks(Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 0

    .line 70
    invoke-interface {p0, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method public static syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;)V"
        }
    .end annotation

    .line 70
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncTasks$$Lambda$0;

    invoke-direct {v0, p1, p2}, Lcom/squareup/shared/catalog/sync/SyncTasks$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    invoke-interface {p0, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static syncFailWithError(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncError;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/sync/SyncError;",
            ")V"
        }
    .end annotation

    .line 88
    invoke-static {p2}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method public static syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 98
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncTasks$SyncFailure;

    invoke-direct {v0, p2}, Lcom/squareup/shared/catalog/sync/SyncTasks$SyncFailure;-><init>(Ljava/lang/Throwable;)V

    invoke-static {p0, p1, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method public static syncSucceed(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 78
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
