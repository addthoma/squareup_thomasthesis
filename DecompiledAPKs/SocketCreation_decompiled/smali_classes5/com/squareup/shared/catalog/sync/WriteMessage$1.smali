.class Lcom/squareup/shared/catalog/sync/WriteMessage$1;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "WriteMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/WriteMessage;->onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/WriteMessage;

.field final synthetic val$response:Lcom/squareup/api/rpc/Response;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/WriteMessage;Lcom/squareup/api/rpc/Response;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/WriteMessage$1;->this$0:Lcom/squareup/shared/catalog/sync/WriteMessage;

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/WriteMessage$1;->val$response:Lcom/squareup/api/rpc/Response;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/WriteMessage$1;->val$response:Lcom/squareup/api/rpc/Response;

    iget-object v1, v1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    iget-object v1, v1, Lcom/squareup/api/rpc/Error;->description:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const-string v1, "Employee is not authorized to edit item catalog"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->resetAndThrow(Ljava/lang/String;Ljava/lang/Exception;)V

    const/4 p1, 0x0

    return-object p1
.end method
