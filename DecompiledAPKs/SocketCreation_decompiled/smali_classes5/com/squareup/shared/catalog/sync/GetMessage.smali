.class public final Lcom/squareup/shared/catalog/sync/GetMessage;
.super Lcom/squareup/shared/catalog/sync/CatalogMessage;
.source "GetMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;,
        Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;,
        Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;,
        Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

.field private final catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field private final clock:Lcom/squareup/shared/catalog/logging/Clock;

.field private final connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

.field private final initialSync:Z

.field private final lastConnectV2ServerVersion:J

.field private final progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

.field private receivedCogsObjectsCount:I

.field private requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

.field private status:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

.field private final syncedConnectV2ObjectTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field private final threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

.field private totalCogsObjectCount:I

.field private final updateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

.field private final useMultipleBatchUpdate:Z

.field private version:J


# direct methods
.method constructor <init>(Lcom/squareup/api/rpc/Request;Lcom/squareup/shared/catalog/sync/CatalogSync;JLjava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/Request;",
            "Lcom/squareup/shared/catalog/sync/CatalogSync;",
            "J",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;",
            "Lcom/squareup/shared/catalog/logging/CatalogAnalytics;",
            "Lcom/squareup/shared/catalog/logging/Clock;",
            "Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;",
            "Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;",
            "Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;",
            ")V"
        }
    .end annotation

    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogMessage;-><init>(Lcom/squareup/api/rpc/Request;)V

    .line 71
    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->READY:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->status:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    .line 90
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    .line 91
    iput-wide p3, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->lastConnectV2ServerVersion:J

    .line 92
    iput-object p5, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->syncedConnectV2ObjectTypes:Ljava/util/List;

    .line 93
    iput-object p6, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    .line 94
    iput-object p7, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

    .line 95
    iput-object p8, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    .line 96
    iput-object p9, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    .line 97
    iput-object p10, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    .line 98
    iput-object p11, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->updateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    .line 99
    iget-object p2, p1, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    iget-object p2, p2, Lcom/squareup/api/sync/GetRequest;->max_batch_size:Ljava/lang/Long;

    sget-object p3, Lcom/squareup/api/sync/GetRequest;->DEFAULT_MAX_BATCH_SIZE:Ljava/lang/Long;

    .line 100
    invoke-static {p2, p3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    const/4 p4, 0x1

    const/4 p5, 0x0

    const-wide/16 p6, 0x0

    cmp-long p8, p2, p6

    if-eqz p8, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    iput-boolean p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->useMultipleBatchUpdate:Z

    .line 101
    iget-object p1, p1, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    iget-object p1, p1, Lcom/squareup/api/sync/GetRequest;->applied_server_version:Ljava/lang/Long;

    sget-object p2, Lcom/squareup/api/sync/GetRequest;->DEFAULT_APPLIED_SERVER_VERSION:Ljava/lang/Long;

    .line 102
    invoke-static {p1, p2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    cmp-long p3, p1, p6

    if-nez p3, :cond_1

    goto :goto_1

    :cond_1
    const/4 p4, 0x0

    :goto_1
    iput-boolean p4, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->initialSync:Z

    return-void
.end method

.method static synthetic access$1200(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/sync/GetMessage;->handleModifiedObjectSets(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/squareup/shared/catalog/sync/GetMessage;I)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage;->incrementReceivedCogsObjectCount(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->status:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;)Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->status:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/shared/catalog/sync/GetMessage;)J
    .locals 2

    .line 53
    iget-wide v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->version:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/squareup/shared/catalog/sync/GetMessage;)Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/shared/catalog/sync/GetMessage;)Ljava/util/List;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->syncedConnectV2ObjectTypes:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/shared/catalog/sync/GetMessage;)Z
    .locals 0

    .line 53
    iget-boolean p0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->useMultipleBatchUpdate:Z

    return p0
.end method

.method private handleModifiedObjectSets(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 250
    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->access$400(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)Ljava/util/List;

    move-result-object v3

    .line 251
    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->access$500(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)Ljava/util/List;

    move-result-object v2

    .line 252
    invoke-static {p2}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->access$400(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)Ljava/util/List;

    move-result-object v5

    .line 253
    invoke-static {p2}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;->access$500(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;)Ljava/util/List;

    move-result-object v4

    .line 255
    iget-boolean p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->useMultipleBatchUpdate:Z

    if-nez p1, :cond_0

    iget p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->totalCogsObjectCount:I

    if-lez p1, :cond_0

    .line 256
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->updateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    invoke-interface {p1, v3, v2, v5, v4}, Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;->dispatchObjectsToBeChanged(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 264
    :cond_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide p1

    .line 265
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 266
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    add-int/2addr v1, v6

    .line 267
    iget-object v6, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    invoke-virtual {v6, v0, v1}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->increaseObjectsCounts(II)V

    .line 269
    iget-object v6, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    new-instance v7, Lcom/squareup/shared/catalog/sync/GetMessage$2;

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/shared/catalog/sync/GetMessage$2;-><init>(Lcom/squareup/shared/catalog/sync/GetMessage;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    new-instance v0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/sync/GetMessage;JLcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-virtual {v6, v7, v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method private incrementReceivedCogsObjectCount(I)V
    .locals 1

    .line 358
    iget v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->receivedCogsObjectsCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->receivedCogsObjectsCount:I

    return-void
.end method

.method private logEventsIfSuccessful(Z)V
    .locals 12

    .line 329
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 331
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    if-nez p1, :cond_0

    .line 337
    iput-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    return-void

    .line 341
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->getTotalObjectsCount()I

    move-result p1

    .line 342
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v2

    .line 343
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->getTotalDurationMs(J)J

    move-result-wide v8

    .line 344
    new-instance v0, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;

    iget-boolean v5, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->initialSync:Z

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    .line 345
    invoke-static {v2}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->access$900(Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;)I

    move-result v6

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    .line 346
    invoke-static {v2}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->access$1000(Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;)I

    move-result v7

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    invoke-static {v2}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->access$1100(Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;)J

    move-result-wide v10

    move-object v4, v0

    invoke-direct/range {v4 .. v11}, Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;-><init>(ZIIJJ)V

    .line 347
    iget-boolean v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->initialSync:Z

    if-nez v2, :cond_1

    const/16 v2, 0x3e8

    if-lt p1, v2, :cond_1

    .line 348
    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

    invoke-interface {v2, v0}, Lcom/squareup/shared/catalog/logging/CatalogAnalytics;->onIncrementalSync(Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;)V

    :cond_1
    const/16 v2, 0x1388

    if-lt p1, v2, :cond_2

    .line 352
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogAnalytics;->onLargeSync(Lcom/squareup/shared/catalog/logging/CatalogSyncMetrics;)V

    .line 354
    :cond_2
    iput-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    return-void

    .line 332
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempt to log timing event before request sent"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private modifiedObjectsFromCogsObjectWrappers(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;"
        }
    .end annotation

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 213
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 215
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 216
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const-wide/16 v3, 0x0

    move-wide v5, v3

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/api/sync/ObjectWrapper;

    .line 219
    invoke-static {v7}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->isKnown(Lcom/squareup/api/sync/ObjectWrapper;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 220
    iget-object v7, v7, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    const-string v8, "<null ID>"

    if-eqz v7, :cond_0

    .line 223
    iget-object v7, v7, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    if-nez v7, :cond_0

    const-string v8, "<null type>"

    .line 225
    :cond_0
    invoke-interface {v2, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    goto :goto_0

    .line 230
    :cond_1
    invoke-static {v7}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v8

    .line 231
    iget-object v7, v7, Lcom/squareup/api/sync/ObjectWrapper;->deleted:Ljava/lang/Boolean;

    sget-object v9, Lcom/squareup/api/sync/ObjectWrapper;->DEFAULT_DELETED:Ljava/lang/Boolean;

    invoke-static {v7, v9}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 232
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234
    :cond_2
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    cmp-long p1, v5, v3

    if-lez p1, :cond_4

    .line 238
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result p1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 239
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x1

    new-array v6, p1, [Ljava/lang/String;

    .line 240
    invoke-interface {v2, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const-string v6, ", "

    invoke-static {v2, v6, v5, p1}, Lcom/squareup/shared/catalog/utils/StringUtils;->join([Ljava/lang/Object;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v4

    const-string p1, "Catalog: Skipped %d objects, variously typed %s"

    .line 239
    invoke-static {p1, v3}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 242
    :cond_4
    new-instance p1, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    const/4 v2, 0x0

    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/sync/GetMessage$1;)V

    return-object p1
.end method

.method private updateCatalogSyncProgress()V
    .locals 2

    .line 362
    iget v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->totalCogsObjectCount:I

    if-eqz v0, :cond_0

    .line 363
    iget v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->receivedCogsObjectsCount:I

    mul-int/lit8 v1, v1, 0x64

    div-int/2addr v1, v0

    .line 364
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;->onNext(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method final synthetic lambda$handleModifiedObjectSets$1$GetMessage(JLcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 3

    .line 318
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/GetMessage;->updateCatalogSyncProgress()V

    .line 319
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v1}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v1

    sub-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->increaseDatabaseDuration(J)V

    .line 320
    iget-boolean p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->useMultipleBatchUpdate:Z

    if-nez p1, :cond_1

    .line 322
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->status:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    sget-object p2, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->COMPLETE:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage;->logEventsIfSuccessful(Z)V

    .line 324
    :cond_1
    invoke-interface {p3, p4}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method final synthetic lambda$onComplete$0$GetMessage(JZLcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 3

    .line 204
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v1}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v1

    sub-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;->increaseDatabaseDuration(J)V

    if-eqz p3, :cond_0

    .line 205
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->status:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    sget-object p2, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->COMPLETE:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage;->logEventsIfSuccessful(Z)V

    .line 206
    invoke-interface {p4, p5}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method

.method public onComplete(ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 172
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;->onNext(I)V

    .line 174
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->useMultipleBatchUpdate:Z

    if-nez v0, :cond_1

    .line 175
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v3

    .line 182
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    new-instance v7, Lcom/squareup/shared/catalog/sync/GetMessage$1;

    invoke-direct {v7, p0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage$1;-><init>(Lcom/squareup/shared/catalog/sync/GetMessage;Z)V

    new-instance v8, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;

    move-object v1, v8

    move-object v2, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/sync/GetMessage;JZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-virtual {v0, v7, v8}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/Response;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 113
    iget-object v0, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-object p1, p1, Lcom/squareup/api/rpc/Response;->error:Lcom/squareup/api/rpc/Error;

    invoke-static {v0, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->handleRpcError(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/api/rpc/Error;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    .line 115
    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    .line 119
    :cond_0
    iget-object p1, p1, Lcom/squareup/api/rpc/Response;->get_response:Lcom/squareup/api/sync/GetResponse;

    .line 120
    iget-object v0, p1, Lcom/squareup/api/sync/GetResponse;->current_server_version:Ljava/lang/Long;

    sget-object v1, Lcom/squareup/api/sync/GetResponse;->DEFAULT_CURRENT_SERVER_VERSION:Ljava/lang/Long;

    .line 121
    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v0, 0x0

    cmp-long v2, v4, v0

    if-eqz v2, :cond_6

    .line 130
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->status:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    sget-object v1, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->MESSAGE_SENT:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 131
    iput-wide v4, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->version:J

    .line 132
    iget-object v0, p1, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->totalCogsObjectCount:I

    goto :goto_0

    .line 133
    :cond_1
    iget-wide v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->version:J

    cmp-long v3, v0, v4

    if-nez v3, :cond_5

    .line 135
    iget v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->totalCogsObjectCount:I

    iget-object v1, p1, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 139
    :goto_0
    iget-object p1, p1, Lcom/squareup/api/sync/GetResponse;->objects:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 142
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage;->modifiedObjectsFromCogsObjectWrappers(Ljava/util/List;)Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    move-result-object v0

    .line 144
    iget v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->receivedCogsObjectsCount:I

    .line 145
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v1, v3

    iget v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->totalCogsObjectCount:I

    if-ne v1, v3, :cond_2

    const/4 v2, 0x1

    :cond_2
    const/4 v1, 0x0

    if-eqz v2, :cond_3

    .line 149
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    iget-wide v2, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->lastConnectV2ServerVersion:J

    iget-object v6, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->syncedConnectV2ObjectTypes:Ljava/util/List;

    new-instance v7, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;

    invoke-direct {v7, p0, v0, p2, v1}, Lcom/squareup/shared/catalog/sync/GetMessage$ConnectV2ResponseHandler;-><init>(Lcom/squareup/shared/catalog/sync/GetMessage;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/GetMessage$1;)V

    move-object v1, p1

    invoke-virtual/range {v1 .. v7}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->sync(JJLjava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler$SearchCatalogObjectResponseHandler;)V

    goto :goto_1

    .line 156
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/GetMessage;->incrementReceivedCogsObjectCount(I)V

    .line 157
    new-instance p1, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;

    invoke-direct {p1, v1, v1, v1}, Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/sync/GetMessage$1;)V

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/shared/catalog/sync/GetMessage;->handleModifiedObjectSets(Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/GetMessage$ModifiedObjects;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    :goto_1
    return-void

    .line 136
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "All GetResponses should have the same total object count"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 134
    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "All GetResponses must have the same version."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 127
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Catalog server version should not be 0."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startRequest()Lcom/squareup/api/rpc/Request;
    .locals 4

    .line 106
    sget-object v0, Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;->MESSAGE_SENT:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->status:Lcom/squareup/shared/catalog/sync/GetMessage$MessageStatus;

    .line 107
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;->onNext(I)V

    .line 108
    new-instance v0, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    invoke-interface {v1}, Lcom/squareup/shared/catalog/logging/Clock;->getUptimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;-><init>(JLcom/squareup/shared/catalog/CatalogThreadsEnforcer;)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage;->requestStats:Lcom/squareup/shared/catalog/sync/GetMessage$RequestStats;

    .line 109
    invoke-super {p0}, Lcom/squareup/shared/catalog/sync/CatalogMessage;->startRequest()Lcom/squareup/api/rpc/Request;

    move-result-object v0

    return-object v0
.end method
