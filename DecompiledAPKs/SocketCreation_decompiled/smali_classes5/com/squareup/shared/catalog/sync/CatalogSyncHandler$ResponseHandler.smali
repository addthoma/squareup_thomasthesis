.class final Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;
.super Ljava/lang/Object;
.source "CatalogSyncHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ResponseHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$LocalSyncCallback;
    }
.end annotation


# instance fields
.field private final batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

.field private final callback:Lcom/squareup/shared/catalog/sync/SyncCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final decoder:Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

.field private final fileThread:Ljava/util/concurrent/Executor;

.field private final httpThread:Ljava/util/concurrent/Executor;

.field private final mainThread:Ljava/util/concurrent/Executor;

.field private responseCount:I


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    .line 117
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    .line 118
    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->fileThread:Ljava/util/concurrent/Executor;

    .line 119
    iput-object p4, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->httpThread:Ljava/util/concurrent/Executor;

    .line 120
    iput-object p5, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->decoder:Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    .line 121
    iput-object p6, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$1;)V
    .locals 0

    .line 103
    invoke-direct/range {p0 .. p6}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)I
    .locals 0

    .line 103
    iget p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->responseCount:I

    return p0
.end method

.method static synthetic access$1100(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;
    .locals 0

    .line 103
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 103
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->fileThread:Ljava/util/concurrent/Executor;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 103
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Lcom/squareup/shared/catalog/sync/SyncCallback;
    .locals 0

    .line 103
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;
    .locals 0

    .line 103
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->decoder:Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 103
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->httpThread:Ljava/util/concurrent/Executor;

    return-object p0
.end method


# virtual methods
.method final synthetic lambda$run$0$CatalogSyncHandler$ResponseHandler()V
    .locals 4

    .line 131
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    iget v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->responseCount:I

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->access$700(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;IZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method final synthetic lambda$run$1$CatalogSyncHandler$ResponseHandler(Ljava/lang/Throwable;)V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method final synthetic lambda$run$2$CatalogSyncHandler$ResponseHandler(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V
    .locals 3

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    new-instance v1, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$1;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V

    invoke-static {v0, p1, v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->access$1200(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 162
    iget-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->httpThread:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->decoder:Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$4;->get$Lambda(Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 163
    iget-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    iget v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->responseCount:I

    const/4 v1, 0x0

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v2

    invoke-static {p2, v0, v1, v2}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->access$700(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;IZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    .line 164
    iget-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->fileThread:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$5;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$5;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;Ljava/lang/Throwable;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method final synthetic lambda$run$3$CatalogSyncHandler$ResponseHandler(Ljava/lang/Throwable;)V
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method final synthetic lambda$run$4$CatalogSyncHandler$ResponseHandler()V
    .locals 4

    .line 187
    :try_start_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->batch:Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    iget v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->responseCount:I

    const/4 v2, 0x1

    new-instance v3, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2;

    invoke-direct {v3, p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$2;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->access$700(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;IZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    .line 195
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->fileThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$3;

    invoke-direct {v2, p0, v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$3;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;Ljava/lang/Throwable;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public run()V
    .locals 5

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->decoder:Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->nextResponse()Lcom/squareup/api/rpc/Response;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    .line 141
    iget v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->responseCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->responseCount:I

    .line 144
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$1;

    invoke-direct {v2, p0, v0, p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->decoder:Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->getResponseBatchWithoutResponses()Lcom/squareup/api/rpc/ResponseBatch;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->decoder:Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->cleanUp()V

    .line 174
    iget-object v1, v0, Lcom/squareup/api/rpc/ResponseBatch;->error:Lcom/squareup/api/rpc/Error;

    if-eqz v1, :cond_2

    .line 175
    iget v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->responseCount:I

    if-gtz v1, :cond_1

    .line 178
    new-instance v1, Lcom/squareup/shared/catalog/sync/SyncError;

    iget-object v2, v0, Lcom/squareup/api/rpc/ResponseBatch;->error:Lcom/squareup/api/rpc/Error;

    .line 179
    invoke-static {v2}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->fromRpcError(Lcom/squareup/api/rpc/Error;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/api/rpc/ResponseBatch;->error:Lcom/squareup/api/rpc/Error;

    invoke-virtual {v0}, Lcom/squareup/api/rpc/Error;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-static {v0, v2, v1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithError(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncError;)V

    return-void

    .line 176
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The ResponseBatch contains both error and responses."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$2;

    invoke-direct {v1, p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$2;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v0

    .line 130
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->decoder:Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/ProgressiveResponseBatchDecoder;->cleanUp()V

    .line 131
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$0;

    invoke-direct {v2, p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 133
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;->callback:Lcom/squareup/shared/catalog/sync/SyncCallback;

    new-instance v3, Lcom/squareup/shared/catalog/sync/SyncError;

    sget-object v4, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithError(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncError;)V

    return-void
.end method
