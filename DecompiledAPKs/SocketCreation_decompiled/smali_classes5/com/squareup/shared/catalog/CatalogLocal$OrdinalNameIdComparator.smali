.class abstract Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
.super Ljava/lang/Object;
.source "CatalogLocal.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/CatalogLocal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "OrdinalNameIdComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/shared/catalog/models/CatalogObject;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "TT;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/shared/catalog/models/CatalogObject;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .line 764
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;->getOrdinal(Lcom/squareup/shared/catalog/models/CatalogObject;)I

    move-result v0

    .line 765
    invoke-virtual {p0, p2}, Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;->getOrdinal(Lcom/squareup/shared/catalog/models/CatalogObject;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 767
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/shared/catalog/utils/ObjectUtils;->compare(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    return p1

    .line 769
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;->getName(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;

    move-result-object v0

    .line 770
    invoke-virtual {p0, p2}, Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;->getName(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;

    move-result-object v1

    .line 771
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 775
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 761
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast p2, Lcom/squareup/shared/catalog/models/CatalogObject;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;->compare(Lcom/squareup/shared/catalog/models/CatalogObject;Lcom/squareup/shared/catalog/models/CatalogObject;)I

    move-result p1

    return p1
.end method

.method abstract getName(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method

.method abstract getOrdinal(Lcom/squareup/shared/catalog/models/CatalogObject;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation
.end method
