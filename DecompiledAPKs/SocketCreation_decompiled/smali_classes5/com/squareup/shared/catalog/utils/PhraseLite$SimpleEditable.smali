.class Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;
.super Ljava/lang/Object;
.source "PhraseLite.java"

# interfaces
.implements Ljava/lang/CharSequence;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/PhraseLite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimpleEditable"
.end annotation


# instance fields
.field private text:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0

    .line 337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/CharSequence;Lcom/squareup/shared/catalog/utils/PhraseLite$1;)V
    .locals 0

    .line 334
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;-><init>(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public charAt(I)C
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    return p1
.end method

.method public length()I
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public replace(IILjava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;
    .locals 3

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-lez p1, :cond_0

    .line 344
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    :cond_0
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 347
    iget-object p1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-ge p2, p1, :cond_1

    .line 348
    iget-object p1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    return-object p0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;->text:Ljava/lang/String;

    return-object v0
.end method
