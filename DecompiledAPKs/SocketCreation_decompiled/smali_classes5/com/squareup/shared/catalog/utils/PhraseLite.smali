.class public Lcom/squareup/shared/catalog/utils/PhraseLite;
.super Ljava/lang/Object;
.source "PhraseLite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;,
        Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;,
        Lcom/squareup/shared/catalog/utils/PhraseLite$LeftCurlyBracketToken;,
        Lcom/squareup/shared/catalog/utils/PhraseLite$TextToken;,
        Lcom/squareup/shared/catalog/utils/PhraseLite$Token;
    }
.end annotation


# static fields
.field private static final EOF:I


# instance fields
.field private curChar:C

.field private curCharIndex:I

.field private formatted:Ljava/lang/CharSequence;

.field private head:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

.field private final keys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final keysToValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final pattern:Ljava/lang/CharSequence;


# direct methods
.method private constructor <init>(Ljava/lang/CharSequence;)V
    .locals 2

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keys:Ljava/util/Set;

    .line 57
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keysToValues:Ljava/util/Map;

    .line 151
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    :cond_0
    iput-char v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curChar:C

    .line 153
    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->pattern:Ljava/lang/CharSequence;

    const/4 p1, 0x0

    .line 159
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->token(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 161
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->head:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->head:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    goto :goto_0

    :cond_2
    return-void
.end method

.method private consume()V
    .locals 2

    .line 241
    iget v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curCharIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curCharIndex:I

    .line 242
    iget v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curCharIndex:I

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->pattern:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->pattern:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curCharIndex:I

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    :goto_0
    iput-char v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curChar:C

    return-void
.end method

.method private createEditable(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;
    .locals 2

    .line 147
    new-instance v0, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;-><init>(Ljava/lang/CharSequence;Lcom/squareup/shared/catalog/utils/PhraseLite$1;)V

    return-object v0
.end method

.method public static from(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;
    .locals 1

    .line 79
    new-instance v0, Lcom/squareup/shared/catalog/utils/PhraseLite;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/utils/PhraseLite;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private key(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;
    .locals 3

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    invoke-direct {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->consume()V

    .line 193
    :goto_0
    iget-char v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curChar:C

    const/16 v2, 0x61

    if-lt v1, v2, :cond_0

    const/16 v2, 0x7a

    if-le v1, v2, :cond_1

    :cond_0
    iget-char v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curChar:C

    const/16 v2, 0x5f

    if-ne v1, v2, :cond_2

    .line 194
    :cond_1
    iget-char v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 195
    invoke-direct {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->consume()V

    goto :goto_0

    :cond_2
    const/16 v2, 0x7d

    if-ne v1, v2, :cond_4

    .line 202
    invoke-direct {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->consume()V

    .line 205
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_3

    .line 209
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keys:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 211
    new-instance v1, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;

    invoke-direct {v1, p1, v0}, Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;-><init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;Ljava/lang/String;)V

    return-object v1

    .line 206
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Empty key: {}"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 200
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Missing closing brace: }"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private leftCurlyBracket(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$LeftCurlyBracketToken;
    .locals 1

    .line 226
    invoke-direct {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->consume()V

    .line 227
    invoke-direct {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->consume()V

    .line 228
    new-instance v0, Lcom/squareup/shared/catalog/utils/PhraseLite$LeftCurlyBracketToken;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite$LeftCurlyBracketToken;-><init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)V

    return-object v0
.end method

.method private lookahead()C
    .locals 2

    .line 233
    iget v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curCharIndex:I

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->pattern:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->pattern:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curCharIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private text(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$TextToken;
    .locals 3

    .line 216
    iget v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curCharIndex:I

    .line 218
    :goto_0
    iget-char v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curChar:C

    const/16 v2, 0x7b

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_0

    .line 219
    invoke-direct {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->consume()V

    goto :goto_0

    .line 221
    :cond_0
    new-instance v1, Lcom/squareup/shared/catalog/utils/PhraseLite$TextToken;

    iget v2, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curCharIndex:I

    sub-int/2addr v2, v0

    invoke-direct {v1, p1, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite$TextToken;-><init>(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;I)V

    return-object v1
.end method

.method private token(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$Token;
    .locals 3

    .line 168
    iget-char v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->curChar:C

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/16 v1, 0x7b

    if-ne v0, v1, :cond_3

    .line 172
    invoke-direct {p0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->lookahead()C

    move-result v0

    if-ne v0, v1, :cond_1

    .line 174
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->leftCurlyBracket(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$LeftCurlyBracketToken;

    move-result-object p1

    return-object p1

    :cond_1
    const/16 v1, 0x61

    if-lt v0, v1, :cond_2

    const/16 v1, 0x7a

    if-gt v0, v1, :cond_2

    .line 176
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->key(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$KeyToken;

    move-result-object p1

    return-object p1

    .line 178
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected character \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "\'; expected key."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 182
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/utils/PhraseLite;->text(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$TextToken;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public format()Ljava/lang/CharSequence;
    .locals 4

    .line 120
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->formatted:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keysToValues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keys:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->pattern:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/utils/PhraseLite;->createEditable(Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->head:Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    :goto_0
    if-eqz v1, :cond_0

    .line 130
    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keysToValues:Ljava/util/Map;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->expand(Lcom/squareup/shared/catalog/utils/PhraseLite$SimpleEditable;Ljava/util/Map;)V

    .line 129
    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/PhraseLite$Token;->access$000(Lcom/squareup/shared/catalog/utils/PhraseLite$Token;)Lcom/squareup/shared/catalog/utils/PhraseLite$Token;

    move-result-object v1

    goto :goto_0

    .line 133
    :cond_0
    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->formatted:Ljava/lang/CharSequence;

    goto :goto_1

    .line 122
    :cond_1
    new-instance v0, Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keys:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keysToValues:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 124
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing keys: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->formatted:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public put(Ljava/lang/String;I)Lcom/squareup/shared/catalog/utils/PhraseLite;
    .locals 0

    .line 110
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/shared/catalog/utils/PhraseLite;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/shared/catalog/utils/PhraseLite;
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keys:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 96
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->keysToValues:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    .line 99
    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->formatted:Ljava/lang/CharSequence;

    return-object p0

    .line 94
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Null value for \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 91
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/PhraseLite;->pattern:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
