.class public Lcom/squareup/shared/catalog/android/RealSQLDatabase;
.super Ljava/lang/Object;
.source "RealSQLDatabase.java"

# interfaces
.implements Lcom/squareup/shared/sql/SQLDatabase;


# instance fields
.field private final db:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "db"

    .line 14
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/database/sqlite/SQLiteDatabase;

    iput-object p1, p0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method


# virtual methods
.method public beginTransaction()V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    return-void
.end method

.method public compileStatement(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatement;
    .locals 2

    .line 30
    new-instance v0, Lcom/squareup/shared/catalog/android/RealSQLStatement;

    iget-object v1, p0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/android/RealSQLStatement;-><init>(Landroid/database/sqlite/SQLiteStatement;)V

    return-object v0
.end method

.method public endTransaction()V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void
.end method

.method public execSQL(Ljava/lang/String;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    .line 38
    new-instance v0, Lcom/squareup/shared/catalog/android/RealSQLCursor;

    iget-object v1, p0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/android/RealSQLCursor;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method

.method public setTransactionSuccessful()V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    return-void
.end method
