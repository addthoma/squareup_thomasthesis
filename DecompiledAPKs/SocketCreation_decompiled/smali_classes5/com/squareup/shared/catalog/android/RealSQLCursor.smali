.class Lcom/squareup/shared/catalog/android/RealSQLCursor;
.super Landroid/database/CursorWrapper;
.source "RealSQLCursor.java"

# interfaces
.implements Lcom/squareup/shared/sql/SQLCursor;


# direct methods
.method constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public varargs mergeWithCursors([Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 3

    .line 16
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Landroid/database/Cursor;

    const/4 v1, 0x0

    .line 17
    aput-object p0, v0, v1

    .line 18
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    add-int/lit8 v2, v1, 0x1

    .line 19
    aget-object v1, p1, v1

    check-cast v1, Landroid/database/Cursor;

    aput-object v1, v0, v2

    move v1, v2

    goto :goto_0

    .line 21
    :cond_0
    new-instance p1, Lcom/squareup/shared/catalog/android/RealSQLCursor;

    new-instance v1, Landroid/database/MergeCursor;

    invoke-direct {v1, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    invoke-direct {p1, v1}, Lcom/squareup/shared/catalog/android/RealSQLCursor;-><init>(Landroid/database/Cursor;)V

    return-object p1
.end method
