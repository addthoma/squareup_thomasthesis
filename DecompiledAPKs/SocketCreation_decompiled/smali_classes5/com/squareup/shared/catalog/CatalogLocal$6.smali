.class Lcom/squareup/shared/catalog/CatalogLocal$6;
.super Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;
.source "CatalogLocal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/CatalogLocal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator<",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/squareup/shared/catalog/CatalogLocal$OrdinalNameIdComparator;-><init>()V

    return-void
.end method


# virtual methods
.method getName(Lcom/squareup/shared/catalog/models/CatalogDiscount;)Ljava/lang/String;
    .locals 0

    .line 144
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method bridge synthetic getName(Lcom/squareup/shared/catalog/models/CatalogObject;)Ljava/lang/String;
    .locals 0

    .line 138
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal$6;->getName(Lcom/squareup/shared/catalog/models/CatalogDiscount;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getOrdinal(Lcom/squareup/shared/catalog/models/CatalogDiscount;)I
    .locals 0

    .line 140
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/LanguageUtils;->collationSectionIndex(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method bridge synthetic getOrdinal(Lcom/squareup/shared/catalog/models/CatalogObject;)I
    .locals 0

    .line 138
    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/CatalogLocal$6;->getOrdinal(Lcom/squareup/shared/catalog/models/CatalogDiscount;)I

    move-result p1

    return p1
.end method
