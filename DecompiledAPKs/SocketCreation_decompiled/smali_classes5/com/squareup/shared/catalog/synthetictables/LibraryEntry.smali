.class public final Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
.super Ljava/lang/Object;
.source "LibraryEntry.java"


# instance fields
.field private final abbreviation:Ljava/lang/String;

.field private final categoryId:Ljava/lang/String;

.field private final color:Ljava/lang/String;

.field private final defaultVariationId:Ljava/lang/String;

.field private final discountType:Lcom/squareup/api/items/Discount$DiscountType;

.field private final duration:J

.field private final imageId:Ljava/lang/String;

.field private final imageUrl:Ljava/lang/String;

.field private final itemType:Lcom/squareup/api/items/Item$Type;

.field private final measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

.field private final name:Ljava/lang/String;

.field private final namingMethod:Lcom/squareup/api/items/TicketGroup$NamingMethod;

.field private final objectId:Ljava/lang/String;

.field private final ordinal:I

.field private final percentage:Ljava/lang/String;

.field private final price:Lcom/squareup/protos/common/Money;

.field private final type:Lcom/squareup/shared/catalog/models/CatalogObjectType;

.field private final variations:I


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Item$Type;ILjava/lang/String;Lcom/squareup/api/items/TicketGroup$NamingMethod;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 3

    move-object v0, p0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 42
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->type:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-object v1, p2

    .line 43
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->objectId:Ljava/lang/String;

    move-object v1, p3

    .line 44
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->name:Ljava/lang/String;

    move-object v1, p4

    .line 45
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->imageId:Ljava/lang/String;

    move-object v1, p5

    .line 46
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->imageUrl:Ljava/lang/String;

    move-object v1, p6

    .line 47
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->price:Lcom/squareup/protos/common/Money;

    move-object v1, p7

    .line 48
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->percentage:Ljava/lang/String;

    move v1, p8

    .line 49
    iput v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->variations:I

    move-wide v1, p9

    .line 50
    iput-wide v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->duration:J

    move-object v1, p11

    .line 51
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->defaultVariationId:Ljava/lang/String;

    move-object v1, p12

    .line 52
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->abbreviation:Ljava/lang/String;

    move-object/from16 v1, p13

    .line 53
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->color:Ljava/lang/String;

    move-object/from16 v1, p14

    .line 54
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    move-object/from16 v1, p15

    .line 55
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->itemType:Lcom/squareup/api/items/Item$Type;

    move/from16 v1, p16

    .line 56
    iput v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->ordinal:I

    move-object/from16 v1, p17

    .line 57
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->categoryId:Ljava/lang/String;

    move-object/from16 v1, p18

    .line 58
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->namingMethod:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    move-object/from16 v1, p19

    .line 59
    iput-object v1, v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-void
.end method

.method public static forCategory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 21

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    .line 64
    new-instance v20, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-object/from16 v0, v20

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;-><init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Item$Type;ILjava/lang/String;Lcom/squareup/api/items/TicketGroup$NamingMethod;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-object v20
.end method

.method public static forDiscount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 21

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, p2

    move-object/from16 v6, p3

    move-object/from16 v13, p4

    move-object/from16 v14, p5

    .line 70
    new-instance v20, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-object/from16 v0, v20

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;-><init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Item$Type;ILjava/lang/String;Lcom/squareup/api/items/TicketGroup$NamingMethod;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-object v20
.end method

.method public static forItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 21

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v8, p5

    move-wide/from16 v9, p6

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v15, p11

    move-object/from16 v17, p12

    move-object/from16 v19, p13

    .line 77
    new-instance v20, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-object/from16 v0, v20

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v7, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v18, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;-><init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Item$Type;ILjava/lang/String;Lcom/squareup/api/items/TicketGroup$NamingMethod;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-object v20
.end method

.method public static forModifierList(Ljava/lang/String;Ljava/lang/String;II)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 21

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v8, p2

    move/from16 v16, p3

    .line 84
    new-instance v20, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-object/from16 v0, v20

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;-><init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Item$Type;ILjava/lang/String;Lcom/squareup/api/items/TicketGroup$NamingMethod;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-object v20
.end method

.method public static forTicketGroup(Ljava/lang/String;Ljava/lang/String;IILcom/squareup/api/items/TicketGroup$NamingMethod;)Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;
    .locals 21

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v8, p2

    move/from16 v16, p3

    move-object/from16 v18, p4

    .line 90
    new-instance v20, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-object/from16 v0, v20

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;-><init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$DiscountType;Lcom/squareup/api/items/Item$Type;ILjava/lang/String;Lcom/squareup/api/items/TicketGroup$NamingMethod;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-object v20
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 198
    instance-of v0, p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->objectId:Ljava/lang/String;

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getAbbreviation()Ljava/lang/String;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->abbreviation:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->abbreviation:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getCatalogMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    return-object v0
.end method

.method public getCategoryId()Ljava/lang/String;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->categoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->color:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultVariationId()Ljava/lang/String;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->defaultVariationId:Ljava/lang/String;

    return-object v0
.end method

.method public getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->discountType:Lcom/squareup/api/items/Discount$DiscountType;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .line 207
    iget-wide v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->duration:J

    return-wide v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->imageId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->imageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getItemType()Lcom/squareup/api/items/Item$Type;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->itemType:Lcom/squareup/api/items/Item$Type;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->namingMethod:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    return-object v0
.end method

.method public getObjectId()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->objectId:Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 1

    .line 173
    iget v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->ordinal:I

    return v0
.end method

.method public getPercentage()Ljava/lang/String;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->percentage:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->price:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getTicketTemplateCount()I
    .locals 1

    .line 160
    iget v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->variations:I

    return v0
.end method

.method public getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->type:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object v0
.end method

.method public getVariations()I
    .locals 1

    .line 155
    iget v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->variations:I

    return v0
.end method

.method public hasCatalogMeasurementUnit()Z
    .locals 1

    .line 189
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getCatalogMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->objectId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isGiftCard()Z
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->itemType:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
