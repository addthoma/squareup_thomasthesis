.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;
.super Ljava/lang/Object;
.source "ItemVariationInventoryAlertConfig.java"


# instance fields
.field public final inventoryAlertThreshold:J

.field public final isInventoryAlertEnabled:Z

.field public final isInventoryTracked:Z

.field public final isVariationDeleted:Z

.field public final itemId:Ljava/lang/String;

.field public final updateServerTimestamp:J

.field public final variationId:Ljava/lang/String;

.field public final variationToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZJZJ)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;->variationId:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;->variationToken:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;->itemId:Ljava/lang/String;

    .line 22
    iput-boolean p4, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;->isInventoryTracked:Z

    .line 23
    iput-boolean p5, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;->isInventoryAlertEnabled:Z

    .line 24
    iput-wide p6, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;->inventoryAlertThreshold:J

    .line 25
    iput-boolean p8, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;->isVariationDeleted:Z

    .line 26
    iput-wide p9, p0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationInventoryAlertConfig;->updateServerTimestamp:J

    return-void
.end method
