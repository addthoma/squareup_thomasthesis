.class public Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;
.super Ljava/lang/Object;
.source "PricingRuleLookupTableReader.java"

# interfaces
.implements Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;
    }
.end annotation


# instance fields
.field private helper:Lcom/squareup/shared/sql/DatabaseHelper;

.field private final sourceTables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTable;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;->sourceTables:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public activeRuleIdsForRange(Ljava/util/TimeZone;Ljava/util/Date;Ljava/util/Date;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TimeZone;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    invoke-interface {v0}, Lcom/squareup/shared/sql/DatabaseHelper;->getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;

    move-result-object v0

    .line 66
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {p1, v3, v4}, Ljava/util/TimeZone;->getOffset(J)I

    move-result p2

    int-to-long v3, p2

    add-long/2addr v1, v3

    .line 67
    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide p2

    invoke-virtual {p1, p2, p3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result p1

    int-to-long p1, p1

    add-long/2addr v3, p1

    const-wide/16 p1, 0x0

    cmp-long p3, v3, p1

    if-gez p3, :cond_0

    const-wide v3, 0x7fffffffffffffffL

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/String;

    const/4 p2, 0x0

    .line 71
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, p2

    const/4 p2, 0x1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p3

    aput-object p3, p1, p2

    .line 72
    sget-object p2, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->SEARCH_PRICING_RULES_BY_ACTIVE_DATES:Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p2, p1}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 74
    :try_start_0
    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    const-string p3, "pricing_rule_id"

    .line 75
    invoke-interface {p1, p3}, Lcom/squareup/shared/sql/SQLCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p3

    .line 76
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    invoke-interface {p1, p3}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :cond_1
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p2

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p2
.end method

.method public onRegistered(Lcom/squareup/shared/sql/DatabaseHelper;)V
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;->helper:Lcom/squareup/shared/sql/DatabaseHelper;

    return-void
.end method

.method public sourceTables()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTable;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/shared/catalog/synthetictables/PricingRuleLookupTableReader;->sourceTables:Ljava/util/List;

    return-object v0
.end method
