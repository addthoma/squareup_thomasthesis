.class final Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$1;
.super Ljava/lang/Object;
.source "SignOutRunner.kt"

# interfaces
.implements Lcom/squareup/tickets/TicketsCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/signout/SignOutRunner;->attemptShowSignoutDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tickets/TicketsCallback<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/tickets/TicketsResult;",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/signout/SignOutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/signout/SignOutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$1;->this$0:Lcom/squareup/signout/SignOutRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/tickets/TicketsResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$1;->this$0:Lcom/squareup/signout/SignOutRunner;

    invoke-static {v0}, Lcom/squareup/signout/SignOutRunner;->access$getUnsyncedOpenTicketsSpinner$p(Lcom/squareup/signout/SignOutRunner;)Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;->showSpinner(Z)V

    .line 125
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    .line 126
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->compare(II)I

    move-result v0

    if-lez v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$1;->this$0:Lcom/squareup/signout/SignOutRunner;

    .line 128
    invoke-static {v0}, Lcom/squareup/signout/SignOutRunner;->access$getRes$p(Lcom/squareup/signout/SignOutRunner;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/opentickets/R$string;->open_tickets_syncing:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez p1, :cond_0

    .line 129
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v2, p0, Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$1;->this$0:Lcom/squareup/signout/SignOutRunner;

    invoke-static {v2}, Lcom/squareup/signout/SignOutRunner;->access$getRes$p(Lcom/squareup/signout/SignOutRunner;)Lcom/squareup/util/Res;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/tickets/UnsyncedTicketConfirmations;->buildMessage(ILcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "buildMessage(unsyncedTickets!!, res)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-static {v0, v1, p1}, Lcom/squareup/signout/SignOutRunner;->access$showSignOutDialog(Lcom/squareup/signout/SignOutRunner;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_1
    iget-object p1, p0, Lcom/squareup/signout/SignOutRunner$attemptShowSignoutDialog$1;->this$0:Lcom/squareup/signout/SignOutRunner;

    invoke-static {p1}, Lcom/squareup/signout/SignOutRunner;->access$showSignOutDialog(Lcom/squareup/signout/SignOutRunner;)V

    :goto_0
    return-void
.end method
