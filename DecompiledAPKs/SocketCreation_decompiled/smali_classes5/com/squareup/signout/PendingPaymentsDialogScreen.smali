.class public Lcom/squareup/signout/PendingPaymentsDialogScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "PendingPaymentsDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/signout/PendingPaymentsDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signout/PendingPaymentsDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/signout/PendingPaymentsDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final pendingPaymentsMessage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/signout/-$$Lambda$PendingPaymentsDialogScreen$8Dhg2_7ZeBy0S0DGg4m0V8GNOkQ;->INSTANCE:Lcom/squareup/signout/-$$Lambda$PendingPaymentsDialogScreen$8Dhg2_7ZeBy0S0DGg4m0V8GNOkQ;

    .line 55
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/signout/PendingPaymentsDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/signout/PendingPaymentsDialogScreen;->pendingPaymentsMessage:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/signout/PendingPaymentsDialogScreen;
    .locals 1

    .line 56
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 57
    new-instance v0, Lcom/squareup/signout/PendingPaymentsDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/signout/PendingPaymentsDialogScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 51
    iget-object p2, p0, Lcom/squareup/signout/PendingPaymentsDialogScreen;->pendingPaymentsMessage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
