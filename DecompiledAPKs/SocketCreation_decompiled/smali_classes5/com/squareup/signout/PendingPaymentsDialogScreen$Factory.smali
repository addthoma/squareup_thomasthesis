.class public Lcom/squareup/signout/PendingPaymentsDialogScreen$Factory;
.super Ljava/lang/Object;
.source "PendingPaymentsDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signout/PendingPaymentsDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/activity/ActivityAppletStarter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 43
    invoke-interface {p0}, Lcom/squareup/ui/activity/ActivityAppletStarter;->activateActivityApplet()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 32
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/signout/PendingPaymentsDialogScreen;

    .line 33
    iget-object v0, v0, Lcom/squareup/signout/PendingPaymentsDialogScreen;->pendingPaymentsMessage:Ljava/lang/String;

    .line 35
    const-class v1, Lcom/squareup/ui/activity/ActivityAppletStarter$ParentComponent;

    .line 36
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/ActivityAppletStarter$ParentComponent;

    .line 37
    invoke-interface {v1}, Lcom/squareup/ui/activity/ActivityAppletStarter$ParentComponent;->activityAppletStarter()Lcom/squareup/ui/activity/ActivityAppletStarter;

    move-result-object v1

    .line 39
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/signout/R$string;->cannot_sign_out:I

    .line 40
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/signout/R$string;->payments:I

    new-instance v2, Lcom/squareup/signout/-$$Lambda$PendingPaymentsDialogScreen$Factory$OUz7KXdU9qd0okAL6WYrNFtb9Aw;

    invoke-direct {v2, v1}, Lcom/squareup/signout/-$$Lambda$PendingPaymentsDialogScreen$Factory$OUz7KXdU9qd0okAL6WYrNFtb9Aw;-><init>(Lcom/squareup/ui/activity/ActivityAppletStarter;)V

    .line 42
    invoke-virtual {p1, v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    .line 44
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 45
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 39
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
