.class public final enum Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;
.super Ljava/lang/Enum;
.source "TutorialState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tutorialv2/TutorialState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TooltipAlignment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u0004J\u0006\u0010\u0005\u001a\u00020\u0000j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;",
        "",
        "(Ljava/lang/String;I)V",
        "checkingOrder",
        "",
        "rtl",
        "TOOLTIP_ALIGN_LEFT",
        "TOOLTIP_ALIGN_RIGHT",
        "TOOLTIP_ALIGN_CENTER",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

.field public static final enum TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

.field public static final enum TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

.field public static final enum TOOLTIP_ALIGN_RIGHT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    new-instance v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    const/4 v2, 0x0

    const-string v3, "TOOLTIP_ALIGN_LEFT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    const/4 v2, 0x1

    const-string v3, "TOOLTIP_ALIGN_RIGHT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_RIGHT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    const/4 v2, 0x2

    const-string v3, "TOOLTIP_ALIGN_CENTER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->$VALUES:[Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;
    .locals 1

    const-class v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;
    .locals 1

    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->$VALUES:[Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    invoke-virtual {v0}, [Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    return-object v0
.end method


# virtual methods
.method public final checkingOrder()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;",
            ">;"
        }
    .end annotation

    .line 121
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const-string v1, "Arrays.asList(\n         \u2026TIP_ALIGN_RIGHT\n        )"

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v0, v5, :cond_2

    if-eq v0, v4, :cond_1

    if-eq v0, v3, :cond_0

    new-array v0, v3, [Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 135
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v2

    .line 136
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v5

    .line 137
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_RIGHT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v4

    .line 135
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "Arrays.asList(TOOLTIP_AL\u2026TIP_ALIGN_RIGHT\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-array v0, v3, [Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 131
    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v3, v0, v2

    .line 132
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v2, v0, v5

    .line 133
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_RIGHT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v2, v0, v4

    .line 130
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_1
    new-array v0, v3, [Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 127
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_RIGHT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v5

    .line 128
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v1, v0, v4

    .line 126
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "Arrays.asList(\n         \u2026LTIP_ALIGN_LEFT\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_2
    new-array v0, v3, [Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 123
    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v3, v0, v2

    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v2, v0, v5

    .line 124
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_RIGHT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    aput-object v2, v0, v4

    .line 122
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final rtl()Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;
    .locals 2

    .line 143
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    :cond_0
    return-object p0

    .line 145
    :cond_1
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_LEFT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    return-object v0

    .line 144
    :cond_2
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_RIGHT:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    return-object v0
.end method
