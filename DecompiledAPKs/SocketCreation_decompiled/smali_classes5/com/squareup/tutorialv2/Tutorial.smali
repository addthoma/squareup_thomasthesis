.class public interface abstract Lcom/squareup/tutorialv2/Tutorial;
.super Ljava/lang/Object;
.source "Tutorial.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/Tutorial$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fJ\u0008\u0010\u0002\u001a\u00020\u0003H&J\u001a\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH&J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/Tutorial;",
        "Lmortar/Scoped;",
        "onExitRequested",
        "",
        "onTutorialEvent",
        "name",
        "",
        "value",
        "",
        "onTutorialPendingActionEvent",
        "pendingAction",
        "Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "tutorialState",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "Companion",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tutorialv2/Tutorial$Companion;

.field public static final REPLACING:Ljava/lang/String; = "Tutorial being replaced by one of a higher priority"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/tutorialv2/Tutorial$Companion;->$$INSTANCE:Lcom/squareup/tutorialv2/Tutorial$Companion;

    sput-object v0, Lcom/squareup/tutorialv2/Tutorial;->Companion:Lcom/squareup/tutorialv2/Tutorial$Companion;

    return-void
.end method


# virtual methods
.method public abstract onExitRequested()V
.end method

.method public abstract onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V
.end method

.method public abstract onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
.end method

.method public abstract tutorialState()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end method
