.class public final Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;
.super Ljava/lang/Object;
.source "RealTutorialCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tutorialv2/RealTutorialCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final coreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;->coreProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)Lcom/squareup/tutorialv2/RealTutorialCoordinator;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;-><init>(Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tutorialv2/RealTutorialCoordinator;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;->coreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    invoke-static {v0, v1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;->newInstance(Lcom/squareup/tutorialv2/TutorialCore;Lio/reactivex/Scheduler;)Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/RealTutorialCoordinator_Factory;->get()Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    move-result-object v0

    return-object v0
.end method
