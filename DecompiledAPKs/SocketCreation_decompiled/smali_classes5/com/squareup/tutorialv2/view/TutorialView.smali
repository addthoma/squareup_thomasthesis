.class public interface abstract Lcom/squareup/tutorialv2/view/TutorialView;
.super Ljava/lang/Object;
.source "TutorialView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;
    }
.end annotation


# virtual methods
.method public abstract getResources()Landroid/content/res/Resources;
.end method

.method public abstract hide()V
.end method

.method public abstract hidePrimaryButton()V
.end method

.method public abstract hideSecondaryButton()V
.end method

.method public abstract setCloseButtonColor(I)V
.end method

.method public abstract setContentText(I)V
.end method

.method public abstract setContentText(Ljava/lang/CharSequence;)V
.end method

.method public abstract setOnInteractionListener(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V
.end method

.method public abstract setPrimaryButton(ILandroid/view/View$OnClickListener;)V
.end method

.method public abstract setSecondaryButton(ILandroid/view/View$OnClickListener;)V
.end method

.method public abstract setStepsText(Ljava/lang/CharSequence;)V
.end method

.method public abstract setViewBackgroundColor(I)V
.end method

.method public abstract show(Landroid/view/View;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;)V
.end method
