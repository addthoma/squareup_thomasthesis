.class final Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$1;
.super Ljava/lang/Object;
.source "RealTutorialCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tutorialv2/RealTutorialCoordinator;->onTutorialState(Lcom/squareup/tutorialv2/TutorialState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/tutorialv2/TutorialState;

.field final synthetic this$0:Lcom/squareup/tutorialv2/RealTutorialCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/tutorialv2/RealTutorialCoordinator;Lcom/squareup/tutorialv2/TutorialState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$1;->this$0:Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    iput-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$1;->$state:Lcom/squareup/tutorialv2/TutorialState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 67
    iget-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$1;->this$0:Lcom/squareup/tutorialv2/RealTutorialCoordinator;

    invoke-static {p1}, Lcom/squareup/tutorialv2/RealTutorialCoordinator;->access$getCore$p(Lcom/squareup/tutorialv2/RealTutorialCoordinator;)Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCoordinator$onTutorialState$1;->$state:Lcom/squareup/tutorialv2/TutorialState;

    iget-object v0, v0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonEvent:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method
