.class public final Lcom/squareup/tour/TourPreference;
.super Ljava/lang/Object;
.source "TourPreference.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTourPreference.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TourPreference.kt\ncom/squareup/tour/TourPreference\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,51:1\n1642#2,2:52\n1360#2:54\n1429#2,3:55\n*E\n*S KotlinDebug\n*F\n+ 1 TourPreference.kt\ncom/squareup/tour/TourPreference\n*L\n26#1,2:52\n43#1:54\n43#1,3:55\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B3\u0008\u0007\u0012\u0014\u0008\u0001\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003\u0012\u0014\u0008\u0001\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00040\u0007\u00a2\u0006\u0002\u0010\tJ\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0004J\u0014\u0010\u000b\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0004R\u001a\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00040\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/tour/TourPreference;",
        "",
        "educationTourSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "Lcom/squareup/tour/Education;",
        "verticalTourPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "(Lcom/squareup/settings/LocalSetting;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "getSeenTourPageNames",
        "setSeenTourPages",
        "",
        "seenTourPages",
        "Lcom/squareup/tour/SerializableHasTourPages;",
        "pos-tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final educationTourSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Set<",
            "Lcom/squareup/tour/Education;",
            ">;>;"
        }
    .end annotation
.end field

.field private final verticalTourPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .param p1    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/tour/TourEducationItemsSeen;
        .end annotation
    .end param
    .param p2    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/tour/TourEducationItemsSeenVertical;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Set<",
            "Lcom/squareup/tour/Education;",
            ">;>;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "educationTourSetting"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verticalTourPreference"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tour/TourPreference;->educationTourSetting:Lcom/squareup/settings/LocalSetting;

    iput-object p2, p0, Lcom/squareup/tour/TourPreference;->verticalTourPreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method


# virtual methods
.method public final getSeenTourPageNames()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/tour/TourPreference;->educationTourSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {}, Lkotlin/collections/SetsKt;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    const-string v1, "seenEducationTourPages"

    .line 42
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 54
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 55
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 56
    check-cast v2, Lcom/squareup/tour/Education;

    .line 43
    invoke-virtual {v2}, Lcom/squareup/tour/Education;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 57
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 44
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/squareup/tour/TourPreference;->verticalTourPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    if-eqz v1, :cond_1

    const-string v2, "it"

    .line 47
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-object v0
.end method

.method public final setSeenTourPages(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/tour/SerializableHasTourPages;",
            ">;)V"
        }
    .end annotation

    const-string v0, "seenTourPages"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 24
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 25
    iget-object v2, p0, Lcom/squareup/tour/TourPreference;->educationTourSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {}, Lkotlin/collections/SetsKt;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 26
    check-cast p1, Ljava/lang/Iterable;

    .line 52
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tour/SerializableHasTourPages;

    .line 27
    invoke-virtual {v2}, Lcom/squareup/tour/SerializableHasTourPages;->getHasTourPages()Lcom/squareup/tour/Tour$HasTourPages;

    move-result-object v3

    instance-of v3, v3, Lcom/squareup/tour/Education;

    if-eqz v3, :cond_0

    .line 28
    invoke-virtual {v2}, Lcom/squareup/tour/SerializableHasTourPages;->getHasTourPages()Lcom/squareup/tour/Tour$HasTourPages;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {v2}, Lcom/squareup/tour/SerializableHasTourPages;->getUniqueName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 33
    :cond_1
    iget-object p1, p0, Lcom/squareup/tour/TourPreference;->verticalTourPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p1, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/tour/TourPreference;->educationTourSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method
