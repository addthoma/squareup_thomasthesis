.class public abstract Lcom/squareup/tour/TourModule;
.super Ljava/lang/Object;
.source "TourModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindsTour(Lcom/squareup/tour/RealTour;)Lcom/squareup/tour/Tour;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
