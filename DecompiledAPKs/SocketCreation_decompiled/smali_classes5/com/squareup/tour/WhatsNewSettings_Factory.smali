.class public final Lcom/squareup/tour/WhatsNewSettings_Factory;
.super Ljava/lang/Object;
.source "WhatsNewSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tour/WhatsNewSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final tourPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/TourPreference;",
            ">;"
        }
    .end annotation
.end field

.field private final tourProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewTourProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final whatsNewBadgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewBadge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewTourProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/TourPreference;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewSettings_Factory;->whatsNewBadgeProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/tour/WhatsNewSettings_Factory;->tourProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/tour/WhatsNewSettings_Factory;->tourPreferenceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tour/WhatsNewSettings_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewTourProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/TourPreference;",
            ">;)",
            "Lcom/squareup/tour/WhatsNewSettings_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/tour/WhatsNewSettings_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tour/WhatsNewSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/tour/WhatsNewTourProvider;Lcom/squareup/tour/TourPreference;)Lcom/squareup/tour/WhatsNewSettings;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/tour/WhatsNewSettings;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tour/WhatsNewSettings;-><init>(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/tour/WhatsNewTourProvider;Lcom/squareup/tour/TourPreference;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tour/WhatsNewSettings;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewSettings_Factory;->whatsNewBadgeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tour/WhatsNewBadge;

    iget-object v1, p0, Lcom/squareup/tour/WhatsNewSettings_Factory;->tourProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tour/WhatsNewTourProvider;

    iget-object v2, p0, Lcom/squareup/tour/WhatsNewSettings_Factory;->tourPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tour/TourPreference;

    invoke-static {v0, v1, v2}, Lcom/squareup/tour/WhatsNewSettings_Factory;->newInstance(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/tour/WhatsNewTourProvider;Lcom/squareup/tour/TourPreference;)Lcom/squareup/tour/WhatsNewSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewSettings_Factory;->get()Lcom/squareup/tour/WhatsNewSettings;

    move-result-object v0

    return-object v0
.end method
