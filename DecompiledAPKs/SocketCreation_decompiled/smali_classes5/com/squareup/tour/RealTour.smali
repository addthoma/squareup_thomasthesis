.class public Lcom/squareup/tour/RealTour;
.super Ljava/lang/Object;
.source "RealTour.java"

# interfaces
.implements Lcom/squareup/tour/Tour;


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/tour/RealTour;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public showAllWhatsNewScreen()V
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/squareup/tour/RealTour;->flow:Lflow/Flow;

    invoke-static {}, Lcom/squareup/tour/WhatsNewTourScreen;->showAll()Lcom/squareup/tour/WhatsNewTourScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
