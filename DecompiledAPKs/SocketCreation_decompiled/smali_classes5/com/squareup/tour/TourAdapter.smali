.class public final Lcom/squareup/tour/TourAdapter;
.super Landroidx/viewpager/widget/PagerAdapter;
.source "TourAdapter.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final flush:Z

.field private final isTablet:Z

.field public final pages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tour/TourPage;",
            ">;"
        }
    .end annotation
.end field

.field private final tourImageBottomMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/tour/TourAdapter;-><init>(Landroid/content/Context;ZZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 1

    .line 32
    invoke-direct {p0}, Landroidx/viewpager/widget/PagerAdapter;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    .line 33
    iput-object p1, p0, Lcom/squareup/tour/TourAdapter;->context:Landroid/content/Context;

    .line 34
    iput-boolean p2, p0, Lcom/squareup/tour/TourAdapter;->isTablet:Z

    .line 35
    iput-boolean p3, p0, Lcom/squareup/tour/TourAdapter;->flush:Z

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/tour/TourAdapter;->tourImageBottomMargin:I

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .line 84
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .line 54
    iget-object v0, p0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/tour/TourPage;

    .line 56
    iget-object v0, p0, Lcom/squareup/tour/TourAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/tour/TourAdapter;->flush:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/tour/common/R$layout;->flush_tour_page:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/tour/common/R$layout;->tour_page:I

    :goto_0
    const/4 v2, 0x0

    .line 57
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 59
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 61
    sget p1, Lcom/squareup/tour/common/R$id;->tour_title:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 62
    sget v1, Lcom/squareup/tour/common/R$id;->tour_subtitle:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    .line 63
    sget v2, Lcom/squareup/tour/common/R$id;->tour_image:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 65
    iget-boolean v3, p0, Lcom/squareup/tour/TourAdapter;->flush:Z

    if-nez v3, :cond_2

    .line 67
    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 68
    iget-boolean v4, p2, Lcom/squareup/tour/TourPage;->imageGravityBottom:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/squareup/tour/TourAdapter;->isTablet:Z

    if-nez v4, :cond_1

    const/16 v4, 0x51

    .line 69
    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_1

    .line 71
    :cond_1
    iget v4, p0, Lcom/squareup/tour/TourAdapter;->tourImageBottomMargin:I

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 75
    :cond_2
    :goto_1
    iget v3, p2, Lcom/squareup/tour/TourPage;->imageResId:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    iget v2, p2, Lcom/squareup/tour/TourPage;->titleResId:I

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 78
    iget p1, p2, Lcom/squareup/tour/TourPage;->subtitleResId:I

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public updatePages(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;)V"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 43
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tour/Tour$HasTourPages;

    .line 44
    iget-object v1, p0, Lcom/squareup/tour/TourAdapter;->pages:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/squareup/tour/Tour$HasTourPages;->addPages(Ljava/util/List;)V

    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/tour/TourAdapter;->notifyDataSetChanged()V

    return-void
.end method
