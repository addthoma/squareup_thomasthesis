.class final Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "TransferReportsDetailWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transferreports/TransferReportsDetailWorkflow;->render(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;Lcom/squareup/transferreports/TransferReportsDetailState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $state:Lcom/squareup/transferreports/TransferReportsDetailState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/transferreports/TransferReportsDetailState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$4;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$4;->$state:Lcom/squareup/transferreports/TransferReportsDetailState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$4;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 105
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$4;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    new-instance v1, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;

    iget-object v2, p0, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$render$4;->$state:Lcom/squareup/transferreports/TransferReportsDetailState;

    check-cast v2, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingFeeDetailDialog;

    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsDetailState$ShowingFeeDetailDialog;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsDetailWorkflow$Action$ShowTransferReportsDetail;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
