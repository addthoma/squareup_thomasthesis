.class public final Lcom/squareup/transferreports/TransferReportsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "TransferReportsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transferreports/TransferReportsWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/transferreports/TransferReportsProps;",
        "Lcom/squareup/transferreports/TransferReportsState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferReportsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferReportsWorkflow.kt\ncom/squareup/transferreports/TransferReportsWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,180:1\n32#2,12:181\n149#3,5:193\n149#3,5:198\n149#3,5:203\n85#4:208\n85#4:211\n240#5:209\n240#5:212\n276#6:210\n276#6:213\n*E\n*S KotlinDebug\n*F\n+ 1 TransferReportsWorkflow.kt\ncom/squareup/transferreports/TransferReportsWorkflow\n*L\n52#1,12:181\n68#1,5:193\n76#1,5:198\n80#1,5:203\n99#1:208\n105#1:211\n99#1:209\n105#1:212\n99#1:210\n105#1:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u00010B/\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u001e\u0010\u0015\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u0017j\u0002`\u001a0\u0016H\u0002J\u001a\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u00022\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J&\u0010\u001f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u0017j\u0002`\u001a0\u00162\u0006\u0010 \u001a\u00020\u0018H\u0002JN\u0010!\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020\u00032\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040$H\u0016J \u0010%\u001a\u00020&2\u0016\u0010\'\u001a\u0012\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00190\u0017j\u0002`\u001aH\u0002J\u0010\u0010(\u001a\u00020\u001e2\u0006\u0010\"\u001a\u00020\u0003H\u0016J&\u0010)\u001a\u00020*2\u000c\u0010+\u001a\u0008\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020/2\u0006\u0010 \u001a\u00020\u0018H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/transferreports/TransferReportsProps;",
        "Lcom/squareup/transferreports/TransferReportsState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "transferReportsLoader",
        "Lcom/squareup/transferreports/TransferReportsLoader;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "instantTransferRunner",
        "Lcom/squareup/instantdeposit/InstantDepositRunner;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "detailWorkflow",
        "Lcom/squareup/transferreports/TransferReportsDetailWorkflow;",
        "(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)V",
        "getTransferReportsWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lkotlin/Pair;",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
        "Lcom/squareup/transferreports/Snapshots;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "loadMoreWorker",
        "reportsSnapshot",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showTransferReports",
        "Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;",
        "snapshots",
        "snapshotState",
        "transferReportsScreen",
        "Lcom/squareup/transferreports/TransferReportsScreen;",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/transferreports/TransferReportsWorkflow$Action;",
        "showCurrentBalanceAndActiveSales",
        "",
        "Action",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final detailWorkflow:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final instantTransferRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

.field private final transferReportsLoader:Lcom/squareup/transferreports/TransferReportsLoader;


# direct methods
.method public constructor <init>(Lcom/squareup/transferreports/TransferReportsLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/transferreports/TransferReportsDetailWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transferReportsLoader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instantTransferRunner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "detailWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->transferReportsLoader:Lcom/squareup/transferreports/TransferReportsLoader;

    iput-object p2, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->instantTransferRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p5, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->detailWorkflow:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    return-void
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/transferreports/TransferReportsWorkflow;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$showTransferReports(Lcom/squareup/transferreports/TransferReportsWorkflow;Lkotlin/Pair;)Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/transferreports/TransferReportsWorkflow;->showTransferReports(Lkotlin/Pair;)Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;

    move-result-object p0

    return-object p0
.end method

.method private final getTransferReportsWorker()Lcom/squareup/workflow/Worker;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Worker<",
            "Lkotlin/Pair<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;>;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->transferReportsLoader:Lcom/squareup/transferreports/TransferReportsLoader;

    invoke-interface {v0}, Lcom/squareup/transferreports/TransferReportsLoader;->getTransferReports()Lio/reactivex/Single;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->instantTransferRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "instantTransferRunner.snapshot().firstOrError()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/SingleSource;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/RxKotlinKt;->zipWith(Lio/reactivex/Single;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object v0

    .line 208
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$getTransferReportsWorker$$inlined$asWorker$1;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/squareup/transferreports/TransferReportsWorkflow$getTransferReportsWorker$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 209
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 210
    const-class v1, Lkotlin/Pair;

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v4, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v2, Lcom/squareup/workflow/Worker;

    return-object v2
.end method

.method private final loadMoreWorker(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/workflow/Worker;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lkotlin/Pair<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;>;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->transferReportsLoader:Lcom/squareup/transferreports/TransferReportsLoader;

    invoke-interface {v0, p1}, Lcom/squareup/transferreports/TransferReportsLoader;->loadMore(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lio/reactivex/Single;

    move-result-object p1

    .line 104
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->instantTransferRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositRunner;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "instantTransferRunner.snapshot().firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/SingleSource;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/RxKotlinKt;->zipWith(Lio/reactivex/Single;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    .line 211
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/transferreports/TransferReportsWorkflow$loadMoreWorker$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/transferreports/TransferReportsWorkflow$loadMoreWorker$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 212
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 213
    const-class v0, Lkotlin/Pair;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final showTransferReports(Lkotlin/Pair;)Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;)",
            "Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;"
        }
    .end annotation

    .line 109
    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 110
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->loading()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->couldNotLoadBalance()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 113
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;->allowPartialDeposit()Z

    move-result p1

    goto :goto_1

    .line 111
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STORED_BALANCE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    .line 115
    :goto_1
    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;

    if-nez p1, :cond_2

    .line 117
    invoke-virtual {v0}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;->hasActiveSalesReport()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    .line 115
    :goto_2
    invoke-direct {v1, v0, p1}, Lcom/squareup/transferreports/TransferReportsWorkflow$Action$ShowTransferReports;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Z)V

    return-object v1
.end method

.method private final transferReportsScreen(Lcom/squareup/workflow/Sink;ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsScreen;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/transferreports/TransferReportsWorkflow$Action;",
            ">;Z",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            ")",
            "Lcom/squareup/transferreports/TransferReportsScreen;"
        }
    .end annotation

    .line 126
    new-instance v0, Lcom/squareup/transferreports/TransferReportsScreen;

    new-instance v10, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    .line 127
    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DEPOSITS_REPORT_DETAIL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    .line 130
    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 131
    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$2;

    invoke-direct {v1, p1, p3}, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$2;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 132
    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$3;

    invoke-direct {v1, p0}, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$3;-><init>(Lcom/squareup/transferreports/TransferReportsWorkflow;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 133
    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;

    invoke-direct {v1, p3, p1, p2}, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$4;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lcom/squareup/workflow/Sink;Z)V

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function3;

    .line 144
    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$5;

    invoke-direct {v1, p1}, Lcom/squareup/transferreports/TransferReportsWorkflow$transferReportsScreen$5;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, v1

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v1, v10

    move v3, p2

    move-object v4, p3

    .line 126
    invoke-direct/range {v1 .. v9}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;-><init>(ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)V

    invoke-direct {v0, v10}, Lcom/squareup/transferreports/TransferReportsScreen;-><init>(Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;)V

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/transferreports/TransferReportsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/transferreports/TransferReportsState;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    .line 181
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_3

    .line 186
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string v3, "Parcel.obtain()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 188
    array-length v3, p2

    invoke-virtual {v2, p2, v1, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 189
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 190
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 192
    :goto_2
    check-cast p2, Lcom/squareup/transferreports/TransferReportsState;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 53
    :cond_4
    sget-object p2, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsSummaryProps;->INSTANCE:Lcom/squareup/transferreports/TransferReportsProps$TransferReportsSummaryProps;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    sget-object p1, Lcom/squareup/transferreports/TransferReportsState$LoadingTransferReports;->INSTANCE:Lcom/squareup/transferreports/TransferReportsState$LoadingTransferReports;

    move-object p2, p1

    check-cast p2, Lcom/squareup/transferreports/TransferReportsState;

    goto :goto_3

    .line 54
    :cond_5
    instance-of p2, p1, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    if-eqz p2, :cond_6

    new-instance p2, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;

    check-cast p1, Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    const/4 v2, 0x2

    invoke-direct {p2, p1, v1, v2, v0}, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;-><init>(Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/transferreports/TransferReportsState;

    :goto_3
    return-object p2

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/transferreports/TransferReportsProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/transferreports/TransferReportsWorkflow;->initialState(Lcom/squareup/transferreports/TransferReportsProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/transferreports/TransferReportsState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/transferreports/TransferReportsProps;

    check-cast p2, Lcom/squareup/transferreports/TransferReportsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/transferreports/TransferReportsWorkflow;->render(Lcom/squareup/transferreports/TransferReportsProps;Lcom/squareup/transferreports/TransferReportsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/transferreports/TransferReportsProps;Lcom/squareup/transferreports/TransferReportsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/transferreports/TransferReportsProps;",
            "Lcom/squareup/transferreports/TransferReportsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/transferreports/TransferReportsState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "props"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    move-object/from16 v10, p3

    invoke-static {v10, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget-object v3, Lcom/squareup/transferreports/TransferReportsState$LoadingTransferReports;->INSTANCE:Lcom/squareup/transferreports/TransferReportsState$LoadingTransferReports;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    const-string v11, ""

    if-eqz v3, :cond_0

    .line 63
    invoke-direct/range {p0 .. p0}, Lcom/squareup/transferreports/TransferReportsWorkflow;->getTransferReportsWorker()Lcom/squareup/workflow/Worker;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$render$1;

    move-object v2, v0

    check-cast v2, Lcom/squareup/transferreports/TransferReportsWorkflow;

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsWorkflow$render$1;-><init>(Lcom/squareup/transferreports/TransferReportsWorkflow;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object/from16 v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 65
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    const/4 v2, 0x0

    .line 67
    new-instance v3, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    sget-object v13, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0xffe

    const/16 v26, 0x0

    move-object v12, v3

    invoke-direct/range {v12 .. v26}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 64
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/transferreports/TransferReportsWorkflow;->transferReportsScreen(Lcom/squareup/workflow/Sink;ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsScreen;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 194
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 195
    const-class v3, Lcom/squareup/transferreports/TransferReportsScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v11}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 196
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 194
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 69
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 72
    :cond_0
    instance-of v3, v2, Lcom/squareup/transferreports/TransferReportsState$ShowingTransferReports;

    if-eqz v3, :cond_1

    .line 73
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    .line 74
    check-cast v2, Lcom/squareup/transferreports/TransferReportsState$ShowingTransferReports;

    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsState$ShowingTransferReports;->getShowCurrentBalanceAndActiveSales()Z

    move-result v3

    .line 75
    invoke-virtual {v2}, Lcom/squareup/transferreports/TransferReportsState$ShowingTransferReports;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v2

    .line 72
    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/transferreports/TransferReportsWorkflow;->transferReportsScreen(Lcom/squareup/workflow/Sink;ZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/transferreports/TransferReportsScreen;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 199
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 200
    const-class v3, Lcom/squareup/transferreports/TransferReportsScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v11}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 201
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 199
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 76
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    .line 78
    :cond_1
    instance-of v3, v2, Lcom/squareup/transferreports/TransferReportsState$LoadingMoreTransferReports;

    if-eqz v3, :cond_2

    .line 79
    move-object v1, v2

    check-cast v1, Lcom/squareup/transferreports/TransferReportsState$LoadingMoreTransferReports;

    invoke-virtual {v1}, Lcom/squareup/transferreports/TransferReportsState$LoadingMoreTransferReports;->getReportsSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/transferreports/TransferReportsWorkflow;->loadMoreWorker(Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;)Lcom/squareup/workflow/Worker;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v1, Lcom/squareup/transferreports/TransferReportsWorkflow$render$2;

    move-object v2, v0

    check-cast v2, Lcom/squareup/transferreports/TransferReportsWorkflow;

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsWorkflow$render$2;-><init>(Lcom/squareup/transferreports/TransferReportsWorkflow;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object/from16 v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 80
    new-instance v1, Lcom/squareup/transferreports/TransferReportsScreen;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/transferreports/TransferReportsScreen;-><init>(Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 204
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 205
    const-class v3, Lcom/squareup/transferreports/TransferReportsScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v11}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 206
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 204
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 81
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    .line 84
    :cond_2
    instance-of v3, v2, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/squareup/transferreports/TransferReportsWorkflow;->detailWorkflow:Lcom/squareup/transferreports/TransferReportsDetailWorkflow;

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Workflow;

    move-object v3, v2

    check-cast v3, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;

    invoke-virtual {v3}, Lcom/squareup/transferreports/TransferReportsState$DelegatingToDetailWorkflow;->getDetailProps()Lcom/squareup/transferreports/TransferReportsProps$TransferReportsDetailProps;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v3, Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;

    invoke-direct {v3, v1, v2}, Lcom/squareup/transferreports/TransferReportsWorkflow$render$3;-><init>(Lcom/squareup/transferreports/TransferReportsProps;Lcom/squareup/transferreports/TransferReportsState;)V

    move-object v8, v3

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v1, 0x0

    move-object/from16 v4, p3

    move-object v10, v1

    invoke-static/range {v4 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    :goto_0
    return-object v1

    :cond_3
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/transferreports/TransferReportsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/transferreports/TransferReportsState;

    invoke-virtual {p0, p1}, Lcom/squareup/transferreports/TransferReportsWorkflow;->snapshotState(Lcom/squareup/transferreports/TransferReportsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
