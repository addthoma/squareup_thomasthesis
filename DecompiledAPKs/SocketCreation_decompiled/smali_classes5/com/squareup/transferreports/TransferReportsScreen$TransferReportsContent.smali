.class public final Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;
.super Ljava/lang/Object;
.source "TransferReportsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/TransferReportsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TransferReportsContent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u001a\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B{\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\"\u0010\u000c\u001a\u001e\u0012\u0004\u0012\u00020\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u000f\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\t0\r\u0012\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\u0012J\t\u0010\u001f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0006H\u00c6\u0003J\u000f\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J\u000f\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J\u000f\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J%\u0010%\u001a\u001e\u0012\u0004\u0012\u00020\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u000f\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\t0\rH\u00c6\u0003J\u000f\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J\u008d\u0001\u0010\'\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082$\u0008\u0002\u0010\u000c\u001a\u001e\u0012\u0004\u0012\u00020\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u000f\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\t0\r2\u000e\u0008\u0002\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0001J\u0013\u0010(\u001a\u00020\u00032\u0008\u0010)\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010*\u001a\u00020+H\u00d6\u0001J\t\u0010,\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R-\u0010\u000c\u001a\u001e\u0012\u0004\u0012\u00020\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u000f\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\t0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0017\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0016R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0016R\u0017\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0014\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;",
        "",
        "detailViewEnabled",
        "",
        "showCurrentBalanceAndActiveSales",
        "reportSnapshot",
        "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "onBack",
        "Lkotlin/Function0;",
        "",
        "onLoadMore",
        "onHelp",
        "onDetail",
        "Lkotlin/Function3;",
        "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
        "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
        "",
        "onReload",
        "(ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)V",
        "getDetailViewEnabled",
        "()Z",
        "getOnBack",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnDetail",
        "()Lkotlin/jvm/functions/Function3;",
        "getOnHelp",
        "getOnLoadMore",
        "getOnReload",
        "getReportSnapshot",
        "()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
        "getShowCurrentBalanceAndActiveSales",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "transfer-reports_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final detailViewEnabled:Z

.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onDetail:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onHelp:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onLoadMore:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onReload:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

.field private final showCurrentBalanceAndActiveSales:Z


# direct methods
.method public constructor <init>(ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "-",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "reportSnapshot"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLoadMore"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onHelp"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDetail"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onReload"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->detailViewEnabled:Z

    iput-boolean p2, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->showCurrentBalanceAndActiveSales:Z

    iput-object p3, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iput-object p4, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onBack:Lkotlin/jvm/functions/Function0;

    iput-object p5, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    iput-object p6, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onHelp:Lkotlin/jvm/functions/Function0;

    iput-object p7, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onDetail:Lkotlin/jvm/functions/Function3;

    iput-object p8, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onReload:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public synthetic constructor <init>(ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 25

    and-int/lit8 v0, p9, 0x4

    if-eqz v0, :cond_0

    .line 14
    new-instance v0, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    .line 15
    sget-object v2, Lcom/squareup/transferreports/TransferReportsLoader$State;->LOADING:Lcom/squareup/transferreports/TransferReportsLoader$State;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xffe

    const/4 v15, 0x0

    move-object v1, v0

    .line 14
    invoke-direct/range {v1 .. v15}, Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;-><init>(Lcom/squareup/transferreports/TransferReportsLoader$State;Lcom/squareup/protos/client/settlements/PendingSettlementsResponse;Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;Ljava/util/List;Lcom/squareup/transferreports/TransferReportsLoader$State;Ljava/util/List;ILcom/squareup/transferreports/TransferReportsLoader$DepositType;Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v19, v0

    goto :goto_0

    :cond_0
    move-object/from16 v19, p3

    :goto_0
    move-object/from16 v16, p0

    move/from16 v17, p1

    move/from16 v18, p2

    move-object/from16 v20, p4

    move-object/from16 v21, p5

    move-object/from16 v22, p6

    move-object/from16 v23, p7

    move-object/from16 v24, p8

    invoke-direct/range {v16 .. v24}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;-><init>(ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->detailViewEnabled:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->showCurrentBalanceAndActiveSales:Z

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onBack:Lkotlin/jvm/functions/Function0;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onHelp:Lkotlin/jvm/functions/Function0;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onDetail:Lkotlin/jvm/functions/Function3;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onReload:Lkotlin/jvm/functions/Function0;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move p1, v2

    move p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->copy(ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->detailViewEnabled:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->showCurrentBalanceAndActiveSales:Z

    return v0
.end method

.method public final component3()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    return-object v0
.end method

.method public final component4()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onHelp:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function3<",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onDetail:Lkotlin/jvm/functions/Function3;

    return-object v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onReload:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "-",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;"
        }
    .end annotation

    const-string v0, "reportSnapshot"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLoadMore"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onHelp"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDetail"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onReload"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    move-object v1, v0

    move v2, p1

    move v3, p2

    invoke-direct/range {v1 .. v9}, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;-><init>(ZZLcom/squareup/transferreports/TransferReportsLoader$Snapshot;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->detailViewEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->detailViewEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->showCurrentBalanceAndActiveSales:Z

    iget-boolean v1, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->showCurrentBalanceAndActiveSales:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onBack:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onHelp:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onHelp:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onDetail:Lkotlin/jvm/functions/Function3;

    iget-object v1, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onDetail:Lkotlin/jvm/functions/Function3;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onReload:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onReload:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDetailViewEnabled()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->detailViewEnabled:Z

    return v0
.end method

.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnDetail()Lkotlin/jvm/functions/Function3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function3<",
            "Lcom/squareup/transferreports/TransferReportsLoader$DepositType;",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onDetail:Lkotlin/jvm/functions/Function3;

    return-object v0
.end method

.method public final getOnHelp()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onHelp:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnLoadMore()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnReload()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onReload:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getReportSnapshot()Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    return-object v0
.end method

.method public final getShowCurrentBalanceAndActiveSales()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->showCurrentBalanceAndActiveSales:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->detailViewEnabled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->showCurrentBalanceAndActiveSales:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onBack:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onHelp:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onDetail:Lkotlin/jvm/functions/Function3;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_6
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onReload:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransferReportsContent(detailViewEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->detailViewEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showCurrentBalanceAndActiveSales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->showCurrentBalanceAndActiveSales:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", reportSnapshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->reportSnapshot:Lcom/squareup/transferreports/TransferReportsLoader$Snapshot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onLoadMore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onLoadMore:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onHelp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onHelp:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onDetail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onDetail:Lkotlin/jvm/functions/Function3;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onReload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transferreports/TransferReportsScreen$TransferReportsContent;->onReload:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
