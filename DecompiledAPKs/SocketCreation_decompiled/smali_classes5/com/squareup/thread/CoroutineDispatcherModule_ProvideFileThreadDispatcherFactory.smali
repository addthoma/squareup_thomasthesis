.class public final Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;
.super Ljava/lang/Object;
.source "CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final dispatchersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/CoroutineFileDispatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/CoroutineFileDispatcher;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;->dispatchersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/CoroutineFileDispatcher;",
            ">;)",
            "Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;

    invoke-direct {v0, p0}, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFileThreadDispatcher(Lcom/squareup/thread/CoroutineFileDispatcher;)Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/thread/CoroutineDispatcherModule;->provideFileThreadDispatcher(Lcom/squareup/thread/CoroutineFileDispatcher;)Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lkotlinx/coroutines/CoroutineDispatcher;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;->get()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public get()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;->dispatchersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/CoroutineFileDispatcher;

    invoke-static {v0}, Lcom/squareup/thread/CoroutineDispatcherModule_ProvideFileThreadDispatcherFactory;->provideFileThreadDispatcher(Lcom/squareup/thread/CoroutineFileDispatcher;)Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method
