.class public abstract Lcom/squareup/thread/CoroutineDispatcherModule;
.super Ljava/lang/Object;
.source "CoroutineDispatcherModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/CoroutineDispatcherModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\'\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H!J\u0012\u0010\u0007\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H!J\u0012\u0010\u0008\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H!J\u0012\u0010\t\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H!\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/thread/CoroutineDispatcherModule;",
        "",
        "()V",
        "bindComputationContext",
        "Lkotlin/coroutines/CoroutineContext;",
        "dispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "bindMainContext",
        "bindMainImmediateContext",
        "bindRpcContext",
        "Companion",
        "impl-coroutines-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/thread/CoroutineDispatcherModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/thread/CoroutineDispatcherModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/thread/CoroutineDispatcherModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/thread/CoroutineDispatcherModule;->Companion:Lcom/squareup/thread/CoroutineDispatcherModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideComputationDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Computation;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/thread/CoroutineDispatcherModule;->Companion:Lcom/squareup/thread/CoroutineDispatcherModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/thread/CoroutineDispatcherModule$Companion;->provideComputationDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public static final provideFileThreadDispatcher(Lcom/squareup/thread/CoroutineFileDispatcher;)Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/thread/CoroutineDispatcherModule;->Companion:Lcom/squareup/thread/CoroutineDispatcherModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/thread/CoroutineDispatcherModule$Companion;->provideFileThreadDispatcher(Lcom/squareup/thread/CoroutineFileDispatcher;)Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p0

    return-object p0
.end method

.method public static final provideMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/thread/CoroutineDispatcherModule;->Companion:Lcom/squareup/thread/CoroutineDispatcherModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/thread/CoroutineDispatcherModule$Companion;->provideMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public static final provideMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Main$Immediate;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/thread/CoroutineDispatcherModule;->Companion:Lcom/squareup/thread/CoroutineDispatcherModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/thread/CoroutineDispatcherModule$Companion;->provideMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public static final provideRpcDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Rpc;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/thread/CoroutineDispatcherModule;->Companion:Lcom/squareup/thread/CoroutineDispatcherModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/thread/CoroutineDispatcherModule$Companion;->provideRpcDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract bindComputationContext(Lkotlinx/coroutines/CoroutineDispatcher;)Lkotlin/coroutines/CoroutineContext;
    .param p1    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/thread/Computation;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindMainContext(Lkotlinx/coroutines/CoroutineDispatcher;)Lkotlin/coroutines/CoroutineContext;
    .param p1    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindMainImmediateContext(Lkotlinx/coroutines/CoroutineDispatcher;)Lkotlin/coroutines/CoroutineContext;
    .param p1    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/thread/Main$Immediate;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRpcContext(Lkotlinx/coroutines/CoroutineDispatcher;)Lkotlin/coroutines/CoroutineContext;
    .param p1    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/thread/Rpc;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
