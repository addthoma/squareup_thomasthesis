.class public final Lcom/squareup/settingsapplet/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settingsapplet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final active_bank_account_uppercase:I = 0x7f120052

.field public static final add_bank_account_message_title:I = 0x7f12007b

.field public static final add_money:I = 0x7f120084

.field public static final add_money_hint:I = 0x7f120089

.field public static final automatic_deposits_enabled_body:I = 0x7f12010f

.field public static final automatic_deposits_enabled_title:I = 0x7f120110

.field public static final awaiting_debit_authorization:I = 0x7f120117

.field public static final awaiting_email_confirmation:I = 0x7f120118

.field public static final bank_name_and_account_type:I = 0x7f120161

.field public static final barcode_scanners_help_message:I = 0x7f120166

.field public static final barcode_scanners_none_found:I = 0x7f120167

.field public static final barcode_scanners_settings_label:I = 0x7f120168

.field public static final barcode_scanners_uppercase_available:I = 0x7f120169

.field public static final business_address_subheading:I = 0x7f1201b6

.field public static final cancel_bank_verification:I = 0x7f120282

.field public static final cancel_verification:I = 0x7f12029d

.field public static final cancel_verification_message:I = 0x7f12029f

.field public static final cancel_verification_title:I = 0x7f1202a0

.field public static final cash_drawers_help_message:I = 0x7f120374

.field public static final cash_drawers_none_found:I = 0x7f120375

.field public static final cash_drawers_settings_label:I = 0x7f120376

.field public static final cash_drawers_test_open:I = 0x7f120377

.field public static final cash_drawers_uppercase_available:I = 0x7f120378

.field public static final cash_management_off:I = 0x7f120385

.field public static final cash_management_on:I = 0x7f120386

.field public static final cash_management_unclosed_drawer_message:I = 0x7f120388

.field public static final cash_management_unclosed_drawer_title:I = 0x7f120389

.field public static final change_account:I = 0x7f1203ec

.field public static final clear_photo:I = 0x7f120425

.field public static final conditional_taxes_help_text_for_taxes_settings:I = 0x7f120474

.field public static final continue_verification:I = 0x7f1204d1

.field public static final create_ticket_group:I = 0x7f1205eb

.field public static final crm_customer_management_off:I = 0x7f120684

.field public static final crm_customer_management_on:I = 0x7f120685

.field public static final crm_customer_management_settings_header_label:I = 0x7f120686

.field public static final crm_email_collection_settings_header_label:I = 0x7f1206b4

.field public static final crm_email_collection_settings_screen_dialog_confirmation:I = 0x7f1206b5

.field public static final crm_email_collection_settings_screen_dialog_decline:I = 0x7f1206b6

.field public static final crm_email_collection_settings_screen_dialog_message:I = 0x7f1206b7

.field public static final crm_email_collection_settings_screen_dialog_title:I = 0x7f1206b8

.field public static final crm_email_collection_settings_screen_hint:I = 0x7f1206b9

.field public static final customer_checkout_settings_label:I = 0x7f12079f

.field public static final debit_card:I = 0x7f1207e0

.field public static final deposit_options_hint:I = 0x7f1207f2

.field public static final deposit_options_hint_jp:I = 0x7f1207f3

.field public static final deposit_schedule_arrival_day_and_time:I = 0x7f1207f7

.field public static final deposit_schedule_automatic:I = 0x7f1207f8

.field public static final deposit_schedule_automatic_description:I = 0x7f1207f9

.field public static final deposit_schedule_manual:I = 0x7f1207fb

.field public static final deposit_schedule_manual_description:I = 0x7f1207fc

.field public static final deposit_schedule_title:I = 0x7f1207fd

.field public static final deposit_speed_one_to_two_bus_days:I = 0x7f120801

.field public static final deposit_speed_same_day_fee:I = 0x7f120804

.field public static final device_no_name:I = 0x7f12083c

.field public static final edit_ticket_group:I = 0x7f12096d

.field public static final email_resent_body:I = 0x7f1209a2

.field public static final email_resent_title:I = 0x7f1209a3

.field public static final employee_management_enabled_toggle:I = 0x7f120a10

.field public static final employee_management_enabled_toggle_description:I = 0x7f120a11

.field public static final employee_management_enabled_toggle_description_dashboard:I = 0x7f120a12

.field public static final employee_management_enabled_toggle_short:I = 0x7f120a13

.field public static final employee_management_guest_mode_toggle:I = 0x7f120a15

.field public static final employee_management_guest_mode_toggle_description:I = 0x7f120a16

.field public static final employee_management_guest_mode_toggle_short:I = 0x7f120a17

.field public static final employee_management_passcode_description:I = 0x7f120a21

.field public static final employee_management_passcode_description_link_text:I = 0x7f120a22

.field public static final employee_management_passcode_toggle_always:I = 0x7f120a23

.field public static final employee_management_passcode_toggle_never:I = 0x7f120a24

.field public static final employee_management_passcode_toggle_restricted:I = 0x7f120a25

.field public static final employee_management_passcode_uppercase_heading:I = 0x7f120a26

.field public static final employee_management_timeout_option_toggle_1m:I = 0x7f120a2d

.field public static final employee_management_timeout_option_toggle_30s:I = 0x7f120a2e

.field public static final employee_management_timeout_option_toggle_5m:I = 0x7f120a2f

.field public static final employee_management_timeout_option_toggle_never:I = 0x7f120a30

.field public static final employee_management_title:I = 0x7f120a31

.field public static final employee_management_track_time_toggle:I = 0x7f120a32

.field public static final employee_management_track_time_toggle_description:I = 0x7f120a33

.field public static final employee_management_track_time_toggle_description_old:I = 0x7f120a34

.field public static final employee_management_track_time_toggle_short:I = 0x7f120a35

.field public static final employee_management_transaction_lock_mode_toggle:I = 0x7f120a36

.field public static final estimated_completion_date:I = 0x7f120a8f

.field public static final expected_verified_date:I = 0x7f120a95

.field public static final giftcards_settings_egift_enable_in_pos:I = 0x7f120b2c

.field public static final giftcards_settings_egiftcard_custom_policy_hint:I = 0x7f120b2d

.field public static final giftcards_settings_egiftcard_custom_policy_uppercase:I = 0x7f120b2e

.field public static final giftcards_settings_egiftcard_designs:I = 0x7f120b2f

.field public static final giftcards_settings_egiftcard_designs_add:I = 0x7f120b30

.field public static final giftcards_settings_egiftcard_designs_add_action_button:I = 0x7f120b31

.field public static final giftcards_settings_egiftcard_designs_amount_plural:I = 0x7f120b32

.field public static final giftcards_settings_egiftcard_designs_amount_singular:I = 0x7f120b33

.field public static final giftcards_settings_egiftcard_designs_crop_photo_action:I = 0x7f120b34

.field public static final giftcards_settings_egiftcard_designs_crop_photo_error:I = 0x7f120b35

.field public static final giftcards_settings_egiftcard_designs_crop_photo_title:I = 0x7f120b36

.field public static final giftcards_settings_egiftcard_designs_upload_custom:I = 0x7f120b37

.field public static final giftcards_settings_egiftcard_designs_upload_custom_failed:I = 0x7f120b38

.field public static final giftcards_settings_egiftcard_designs_upload_custom_not_available:I = 0x7f120b39

.field public static final giftcards_settings_egiftcard_designs_upload_custom_success:I = 0x7f120b3a

.field public static final giftcards_settings_egiftcard_min_max_load_uppercase:I = 0x7f120b3b

.field public static final giftcards_settings_egiftcard_section_title_uppercase:I = 0x7f120b3c

.field public static final giftcards_settings_load_error:I = 0x7f120b3d

.field public static final giftcards_settings_plastic_description:I = 0x7f120b3e

.field public static final giftcards_settings_plastic_description_link_text:I = 0x7f120b3f

.field public static final giftcards_settings_save_error:I = 0x7f120b40

.field public static final giftcards_settings_title:I = 0x7f120b41

.field public static final instant_deposits_card_failed:I = 0x7f120c16

.field public static final instant_deposits_deposit_schedule_close_of_day:I = 0x7f120c1d

.field public static final instant_deposits_deposit_schedule_hint:I = 0x7f120c1e

.field public static final instant_deposits_deposit_schedule_hint_without_same_day_deposit:I = 0x7f120c1f

.field public static final instant_deposits_link_expired:I = 0x7f120c26

.field public static final instant_deposits_pending_verification:I = 0x7f120c2b

.field public static final instant_transfer_terms:I = 0x7f120c3c

.field public static final instant_transfer_terms_url:I = 0x7f120c3d

.field public static final instant_transfers_description:I = 0x7f120c3e

.field public static final instant_transfers_description_uk:I = 0x7f120c3f

.field public static final item_appeareance_dialog_cancel:I = 0x7f120dc7

.field public static final item_appeareance_dialog_confirm_message:I = 0x7f120dc8

.field public static final item_appeareance_dialog_confirm_title:I = 0x7f120dc9

.field public static final item_appeareance_dialog_yes:I = 0x7f120dca

.field public static final item_appeareance_option_text:I = 0x7f120dcd

.field public static final item_appeareance_settings_label:I = 0x7f120dce

.field public static final item_editing_delete_from_location_tax:I = 0x7f120e07

.field public static final learn_more_about_deposits:I = 0x7f120eb6

.field public static final linked_bank_account_uppercase:I = 0x7f120eda

.field public static final load_deposits_error_title:I = 0x7f120ee6

.field public static final loyalty_settings_enable_loyalty:I = 0x7f120f78

.field public static final loyalty_settings_enable_loyalty_subtitle:I = 0x7f120f79

.field public static final loyalty_settings_front_of_transaction_subtitle:I = 0x7f120f7a

.field public static final loyalty_settings_front_of_transaction_title:I = 0x7f120f7b

.field public static final loyalty_settings_off:I = 0x7f120f7c

.field public static final loyalty_settings_on:I = 0x7f120f7d

.field public static final loyalty_settings_show_nonqualifying:I = 0x7f120f7e

.field public static final loyalty_settings_show_nonqualifying_short:I = 0x7f120f7f

.field public static final loyalty_settings_show_nonqualifying_subtitle:I = 0x7f120f80

.field public static final loyalty_settings_timeout_30:I = 0x7f120f81

.field public static final loyalty_settings_timeout_60:I = 0x7f120f82

.field public static final loyalty_settings_timeout_90:I = 0x7f120f83

.field public static final loyalty_settings_timeout_subtitle:I = 0x7f120f84

.field public static final loyalty_settings_timeout_title:I = 0x7f120f85

.field public static final loyalty_settings_title:I = 0x7f120f86

.field public static final manual_deposits_enabled_body_with_id:I = 0x7f120f90

.field public static final manual_deposits_enabled_body_without_id:I = 0x7f120f91

.field public static final manual_deposits_enabled_title:I = 0x7f120f92

.field public static final merchant_profile_add_logo:I = 0x7f120f97

.field public static final merchant_profile_add_photo:I = 0x7f120f98

.field public static final merchant_profile_business_address_dialog_button:I = 0x7f120f99

.field public static final merchant_profile_business_address_dialog_message:I = 0x7f120f9a

.field public static final merchant_profile_business_address_dialog_mobile_business:I = 0x7f120f9b

.field public static final merchant_profile_business_address_dialog_no_address:I = 0x7f120f9c

.field public static final merchant_profile_business_address_dialog_title:I = 0x7f120f9d

.field public static final merchant_profile_business_address_title:I = 0x7f120f9e

.field public static final merchant_profile_business_address_warning_no_po_box_allowed_message:I = 0x7f120f9f

.field public static final merchant_profile_business_address_warning_no_po_box_allowed_title:I = 0x7f120fa0

.field public static final merchant_profile_business_description_hint:I = 0x7f120fa1

.field public static final merchant_profile_business_name_hint:I = 0x7f120fa2

.field public static final merchant_profile_change_logo:I = 0x7f120fa3

.field public static final merchant_profile_change_logo_message:I = 0x7f120fa4

.field public static final merchant_profile_confirm_business_address_dialog_button_negative:I = 0x7f120fa5

.field public static final merchant_profile_confirm_business_address_dialog_button_positive:I = 0x7f120fa6

.field public static final merchant_profile_confirm_business_address_dialog_message_address:I = 0x7f120fa7

.field public static final merchant_profile_confirm_business_address_dialog_message_mobile:I = 0x7f120fa8

.field public static final merchant_profile_confirm_business_address_dialog_title:I = 0x7f120fa9

.field public static final merchant_profile_contact_email:I = 0x7f120faa

.field public static final merchant_profile_contact_email_selected:I = 0x7f120fab

.field public static final merchant_profile_contact_facebook:I = 0x7f120fac

.field public static final merchant_profile_contact_facebook_selected:I = 0x7f120fad

.field public static final merchant_profile_contact_phone:I = 0x7f120fae

.field public static final merchant_profile_contact_phone_selected:I = 0x7f120faf

.field public static final merchant_profile_contact_twitter:I = 0x7f120fb0

.field public static final merchant_profile_contact_twitter_selected:I = 0x7f120fb1

.field public static final merchant_profile_contact_website:I = 0x7f120fb2

.field public static final merchant_profile_contact_website_selected:I = 0x7f120fb3

.field public static final merchant_profile_featured_image_error_small:I = 0x7f120fb4

.field public static final merchant_profile_featured_image_error_small_title:I = 0x7f120fb5

.field public static final merchant_profile_featured_image_error_wrong_type:I = 0x7f120fb6

.field public static final merchant_profile_featured_image_error_wrong_type_title:I = 0x7f120fb7

.field public static final merchant_profile_featured_image_hint:I = 0x7f120fb8

.field public static final merchant_profile_load_failed:I = 0x7f120fb9

.field public static final merchant_profile_mobile_business_help_text:I = 0x7f120fba

.field public static final merchant_profile_mobile_business_switch_short:I = 0x7f120fbb

.field public static final merchant_profile_photo_on_receipt_switch:I = 0x7f120fbc

.field public static final merchant_profile_photo_on_receipt_switch_short:I = 0x7f120fbd

.field public static final merchant_profile_row_value:I = 0x7f120fbe

.field public static final merchant_profile_saving_image:I = 0x7f120fbf

.field public static final merchant_profile_title:I = 0x7f120fc3

.field public static final missing_address_fields_message:I = 0x7f120fee

.field public static final missing_address_fields_title:I = 0x7f120fef

.field public static final never_collect_signature_help_url:I = 0x7f121062

.field public static final no_linked_bank_account_error_message:I = 0x7f12108a

.field public static final not_activated_account_error_message:I = 0x7f1210a8

.field public static final offline_mode_enable_hint:I = 0x7f1210cf

.field public static final offline_mode_enabled_off:I = 0x7f1210d0

.field public static final offline_mode_enabled_on:I = 0x7f1210d1

.field public static final offline_mode_for_more_information:I = 0x7f1210d2

.field public static final offline_mode_no_limit:I = 0x7f1210d5

.field public static final offline_mode_transaction_limit_hint:I = 0x7f1210d7

.field public static final offline_mode_transaction_limit_hint_no_tips:I = 0x7f1210d8

.field public static final online_bank_linking:I = 0x7f121129

.field public static final online_checkout_settings_title:I = 0x7f121159

.field public static final open_tickets_off:I = 0x7f1211a1

.field public static final open_tickets_on:I = 0x7f1211a2

.field public static final open_tickets_toggle_hint:I = 0x7f1211bb

.field public static final orderhub_alerts_explanation:I = 0x7f121230

.field public static final orderhub_alerts_frequency_1:I = 0x7f121231

.field public static final orderhub_alerts_frequency_2:I = 0x7f121232

.field public static final orderhub_alerts_frequency_3:I = 0x7f121233

.field public static final orderhub_alerts_frequency_4:I = 0x7f121234

.field public static final orderhub_alerts_frequency_header_uppercase:I = 0x7f121235

.field public static final orderhub_alerts_settings_section_label:I = 0x7f121236

.field public static final orderhub_allow_alerts:I = 0x7f121237

.field public static final orderhub_printing_explanation:I = 0x7f1212d6

.field public static final orderhub_printing_new_orders:I = 0x7f1212d7

.field public static final orderhub_printing_settings_section_label:I = 0x7f1212d8

.field public static final orderhub_quick_actions_explanation:I = 0x7f1212da

.field public static final orderhub_quick_actions_settings_section_label:I = 0x7f1212dd

.field public static final orderhub_quick_actions_use_in_order_manager:I = 0x7f1212de

.field public static final paper_signature_always_print_receipt:I = 0x7f12131e

.field public static final paper_signature_always_skip_signature_warning_dialog_content:I = 0x7f12131f

.field public static final paper_signature_always_skip_signature_warning_dialog_title:I = 0x7f121320

.field public static final paper_signature_quick_tip_hint:I = 0x7f121326

.field public static final paper_signature_sign_on_device_hint:I = 0x7f121331

.field public static final paper_signature_sign_on_device_hint_au:I = 0x7f121332

.field public static final paper_signature_sign_on_printed_receipt_description:I = 0x7f121335

.field public static final paper_signature_sign_on_printed_receipts_warning_dialog_content:I = 0x7f121337

.field public static final paper_signature_sign_on_printed_receipts_warning_dialog_title:I = 0x7f121338

.field public static final paper_signature_sign_on_printed_receipts_without_printer_dialog_content:I = 0x7f121339

.field public static final paper_signature_sign_on_printed_receipts_without_printer_dialog_title:I = 0x7f12133a

.field public static final paper_signature_skip_signature_warning_dialog_content:I = 0x7f12133b

.field public static final paper_signature_skip_signature_warning_dialog_title:I = 0x7f12133c

.field public static final passcode_settings_after_each_sale_check:I = 0x7f12135f

.field public static final passcode_settings_after_logout_check:I = 0x7f121360

.field public static final passcode_settings_after_logout_description:I = 0x7f121361

.field public static final passcode_settings_after_timeout:I = 0x7f121362

.field public static final passcode_settings_after_timeout_1_minute:I = 0x7f121363

.field public static final passcode_settings_after_timeout_30_seconds:I = 0x7f121364

.field public static final passcode_settings_after_timeout_5_minutes:I = 0x7f121365

.field public static final passcode_settings_after_timeout_never:I = 0x7f121366

.field public static final passcode_settings_back_out_of_sale_check:I = 0x7f121367

.field public static final passcode_settings_confirm_passcode:I = 0x7f121368

.field public static final passcode_settings_create_owner_passcode_description:I = 0x7f121369

.field public static final passcode_settings_create_owner_passcode_success_description:I = 0x7f12136a

.field public static final passcode_settings_create_owner_passcode_title:I = 0x7f12136b

.field public static final passcode_settings_create_passcode_progress_title:I = 0x7f12136c

.field public static final passcode_settings_create_team_passcode:I = 0x7f12136d

.field public static final passcode_settings_create_team_passcode_description:I = 0x7f12136e

.field public static final passcode_settings_create_team_passcode_dialog_message:I = 0x7f12136f

.field public static final passcode_settings_create_team_passcode_dialog_negative_button_text:I = 0x7f121370

.field public static final passcode_settings_create_team_passcode_dialog_positive_button_text:I = 0x7f121371

.field public static final passcode_settings_create_team_passcode_dialog_title:I = 0x7f121372

.field public static final passcode_settings_create_team_passcode_success_continue_button:I = 0x7f121373

.field public static final passcode_settings_create_team_passcode_success_description:I = 0x7f121374

.field public static final passcode_settings_create_team_passcode_title:I = 0x7f121375

.field public static final passcode_settings_edit_team_passcode_title:I = 0x7f121376

.field public static final passcode_settings_enable_switch:I = 0x7f121377

.field public static final passcode_settings_enable_switch_description:I = 0x7f121378

.field public static final passcode_settings_enter_new_passcode:I = 0x7f121379

.field public static final passcode_settings_enter_passcode:I = 0x7f12137a

.field public static final passcode_settings_error:I = 0x7f12137b

.field public static final passcode_settings_incomplete_passcode:I = 0x7f12137c

.field public static final passcode_settings_not_available_dialog_button_text:I = 0x7f12137d

.field public static final passcode_settings_not_available_dialog_message:I = 0x7f12137e

.field public static final passcode_settings_passcode_in_use:I = 0x7f12137f

.field public static final passcode_settings_passcodes_do_not_match_error:I = 0x7f121380

.field public static final passcode_settings_section_title:I = 0x7f121381

.field public static final passcode_settings_share_team_passcode_dialog_button_text:I = 0x7f121382

.field public static final passcode_settings_share_team_passcode_dialog_message:I = 0x7f121383

.field public static final passcode_settings_share_team_passcode_dialog_title:I = 0x7f121384

.field public static final passcode_settings_team_passcode:I = 0x7f121385

.field public static final passcode_settings_team_passcode_description:I = 0x7f121386

.field public static final passcode_settings_team_passcode_switch:I = 0x7f121387

.field public static final passcode_settings_team_permissions:I = 0x7f121388

.field public static final passcode_settings_team_permissions_edit_link:I = 0x7f121389

.field public static final passcode_settings_team_permissions_enable_link:I = 0x7f12138a

.field public static final passcode_settings_timeout_description:I = 0x7f12138b

.field public static final passcode_settings_timeout_save_button:I = 0x7f12138c

.field public static final passcode_settings_timeout_title:I = 0x7f12138d

.field public static final payment_types_active:I = 0x7f12140c

.field public static final payment_types_settings_instructions:I = 0x7f12140f

.field public static final payment_types_settings_label_card:I = 0x7f121410

.field public static final payment_types_settings_label_card_on_file:I = 0x7f121411

.field public static final payment_types_settings_label_cash:I = 0x7f121412

.field public static final payment_types_settings_label_emoney:I = 0x7f121413

.field public static final payment_types_settings_label_gift_card:I = 0x7f121414

.field public static final payment_types_settings_label_installments:I = 0x7f121415

.field public static final payment_types_settings_label_invoice:I = 0x7f121416

.field public static final payment_types_settings_label_online_checkout:I = 0x7f121417

.field public static final payment_types_settings_label_record_card_payment:I = 0x7f121418

.field public static final payment_types_skip_itemized_cart_hint:I = 0x7f12141b

.field public static final payment_types_skip_itemzied_cart:I = 0x7f12141c

.field public static final payment_types_skip_payment_type_selection:I = 0x7f12141d

.field public static final payment_types_skip_payment_type_selection_hint:I = 0x7f12141e

.field public static final payment_types_title:I = 0x7f12141f

.field public static final payroll_upsell_description:I = 0x7f121422

.field public static final payroll_upsell_headline:I = 0x7f121423

.field public static final payroll_upsell_link:I = 0x7f121424

.field public static final payroll_upsell_title:I = 0x7f121425

.field public static final pending_bank_account_uppercase:I = 0x7f121429

.field public static final peripheral_configured_many:I = 0x7f121433

.field public static final peripheral_configured_one:I = 0x7f121434

.field public static final peripheral_connected_many:I = 0x7f121435

.field public static final peripheral_connected_one:I = 0x7f121436

.field public static final predefined_ticket_group_many_tickets:I = 0x7f121467

.field public static final predefined_ticket_group_one_ticket:I = 0x7f121468

.field public static final predefined_tickets_change_ticket_naming:I = 0x7f12146b

.field public static final predefined_tickets_change_ticket_naming_message:I = 0x7f12146c

.field public static final predefined_tickets_disabled_dialog_confirmation:I = 0x7f121475

.field public static final predefined_tickets_disabled_dialog_message:I = 0x7f121476

.field public static final predefined_tickets_disabled_dialog_title:I = 0x7f121477

.field public static final predefined_tickets_opt_in_hint:I = 0x7f121485

.field public static final predefined_tickets_opt_in_message:I = 0x7f121486

.field public static final predefined_tickets_opt_in_title:I = 0x7f121487

.field public static final predefined_tickets_template_name_hint:I = 0x7f12148c

.field public static final predefined_tickets_ticket_group_limit_reached:I = 0x7f12148d

.field public static final predefined_tickets_ticket_group_limit_reached_message:I = 0x7f12148e

.field public static final predefined_tickets_ticket_group_name_hint:I = 0x7f12148f

.field public static final predefined_tickets_ticket_template_limit_message:I = 0x7f121490

.field public static final predefined_tickets_ticket_template_limit_reached:I = 0x7f121491

.field public static final predefined_tickets_ticket_template_limit_reached_message:I = 0x7f121492

.field public static final print_allow_printing:I = 0x7f1214a0

.field public static final print_allow_printing_hint:I = 0x7f1214a1

.field public static final print_order_tickets_confirm_naming_change:I = 0x7f1214a7

.field public static final print_receipts_value:I = 0x7f1214ad

.field public static final print_receipts_value_open_tickets:I = 0x7f1214ae

.field public static final print_receipts_value_reports:I = 0x7f1214af

.field public static final print_receipts_value_reports_open_tickets:I = 0x7f1214b0

.field public static final print_uncategorized_items:I = 0x7f1214b5

.field public static final printer_settings_label:I = 0x7f1214bc

.field public static final printer_station_create_printer_station:I = 0x7f1214bd

.field public static final printer_station_edit_printer_station:I = 0x7f1214be

.field public static final printer_station_no_options_selected:I = 0x7f1214bf

.field public static final printer_station_no_printer_selected:I = 0x7f1214c0

.field public static final printer_stations_cannot_sign_on_printed_receipt_content:I = 0x7f1214c1

.field public static final printer_stations_cannot_sign_on_printed_receipt_title:I = 0x7f1214c2

.field public static final printer_stations_change_settings:I = 0x7f1214c3

.field public static final printer_stations_confirm_change_settings:I = 0x7f1214c4

.field public static final printer_stations_disabled_uppercase:I = 0x7f1214c5

.field public static final printer_stations_edit_printer_stations:I = 0x7f1214c6

.field public static final printer_stations_enabled_uppercase:I = 0x7f1214c7

.field public static final printer_stations_list_title_phrase:I = 0x7f1214c8

.field public static final printer_stations_name_hint:I = 0x7f1214c9

.field public static final printer_stations_name_required_error_message:I = 0x7f1214ca

.field public static final printer_stations_no_hardware_printer:I = 0x7f1214cc

.field public static final printer_stations_no_hardware_printer_selected:I = 0x7f1214cd

.field public static final printer_stations_no_hardware_printers_available_text:I = 0x7f1214ce

.field public static final printer_stations_no_hardware_printers_available_title:I = 0x7f1214cf

.field public static final printer_stations_select_hardware_printer:I = 0x7f1214d9

.field public static final printer_stations_test_print_job_name:I = 0x7f1214dc

.field public static final printer_stations_test_print_ticket_name:I = 0x7f1214dd

.field public static final printer_test_print:I = 0x7f1214de

.field public static final printer_test_print_item_example:I = 0x7f1214df

.field public static final printer_test_print_printer_disconnected:I = 0x7f1214e0

.field public static final printer_test_print_uppercase_dining_option_for_here:I = 0x7f1214e1

.field public static final printer_title:I = 0x7f1214e2

.field public static final quick_amounts_status_auto:I = 0x7f121515

.field public static final quick_amounts_status_off:I = 0x7f121516

.field public static final quick_amounts_status_set:I = 0x7f121517

.field public static final scales_settings_title:I = 0x7f12177b

.field public static final shared_settings_empty_state_dashboard_link:I = 0x7f1217e0

.field public static final shared_settings_empty_state_dashboard_link_text:I = 0x7f1217e1

.field public static final shared_settings_synced:I = 0x7f1217e4

.field public static final shared_settings_title:I = 0x7f1217e5

.field public static final sign_skip_under_amount:I = 0x7f1217fa

.field public static final sign_under_amount_hint:I = 0x7f1217fb

.field public static final signature_always_collect:I = 0x7f1217fc

.field public static final signature_and_receipt_title:I = 0x7f1217fd

.field public static final signature_collect_over_amount:I = 0x7f1217ff

.field public static final signature_never_collect:I = 0x7f121802

.field public static final signature_never_collect_hint:I = 0x7f121803

.field public static final signature_optional_off_short:I = 0x7f121804

.field public static final signature_optional_on:I = 0x7f121805

.field public static final signature_optional_on_short:I = 0x7f121806

.field public static final signature_title:I = 0x7f121808

.field public static final skip_receipt_screen_helper_text:I = 0x7f121816

.field public static final skip_receipt_screen_url:I = 0x7f121817

.field public static final skip_receipt_screen_warning_content:I = 0x7f121818

.field public static final skip_receipt_screen_warning_title:I = 0x7f121819

.field public static final square_reader_magstripe_fullname:I = 0x7f12189c

.field public static final swipe_chip_cards_enable_hint:I = 0x7f1218e3

.field public static final swipe_chip_cards_popup_for_more_information:I = 0x7f1218e5

.field public static final swipe_chip_cards_popup_liability_warning_text:I = 0x7f1218e7

.field public static final swipe_chip_cards_title:I = 0x7f1218e8

.field public static final swipe_chip_cards_toggle_off:I = 0x7f1218e9

.field public static final swipe_chip_cards_toggle_on:I = 0x7f1218ea

.field public static final tax_applicable_items:I = 0x7f121920

.field public static final tax_applicable_items_count_all:I = 0x7f121921

.field public static final tax_applicable_items_count_none:I = 0x7f121922

.field public static final tax_applicable_items_count_one:I = 0x7f121923

.field public static final tax_applicable_items_count_some:I = 0x7f121924

.field public static final tax_applicable_items_custom:I = 0x7f121925

.field public static final tax_applicable_items_select_all:I = 0x7f121926

.field public static final tax_applicable_items_select_none:I = 0x7f121927

.field public static final tax_applicable_services:I = 0x7f121928

.field public static final tax_applicable_services_count_all:I = 0x7f121929

.field public static final tax_applicable_services_count_none:I = 0x7f12192a

.field public static final tax_applicable_services_count_one:I = 0x7f12192b

.field public static final tax_applicable_services_count_some:I = 0x7f12192c

.field public static final tax_count_one:I = 0x7f121934

.field public static final tax_count_two_or_more:I = 0x7f121935

.field public static final tax_count_zero:I = 0x7f121936

.field public static final tax_create_tax:I = 0x7f121937

.field public static final tax_delete_button_confirm_stage_text:I = 0x7f121938

.field public static final tax_edit:I = 0x7f121939

.field public static final tax_error_missing_name_message:I = 0x7f12193a

.field public static final tax_error_missing_name_title:I = 0x7f12193b

.field public static final tax_error_missing_rate_message:I = 0x7f12193c

.field public static final tax_error_missing_rate_title:I = 0x7f12193d

.field public static final tax_inclusion_helper:I = 0x7f121943

.field public static final tax_item_pricing:I = 0x7f121946

.field public static final tax_off:I = 0x7f12194b

.field public static final tax_percentage_dialog_title:I = 0x7f12194c

.field public static final tax_taxes:I = 0x7f12194e

.field public static final time_tracking_settings_enable_switch_description:I = 0x7f12197e

.field public static final time_tracking_settings_section_title:I = 0x7f12197f

.field public static final tip_off:I = 0x7f1219be

.field public static final tip_set_percentages:I = 0x7f1219c6

.field public static final tip_set_percentages_short:I = 0x7f1219c7

.field public static final tip_smart_amounts:I = 0x7f1219c8

.field public static final tip_smart_amounts_short:I = 0x7f1219c9

.field public static final titlecase_deposits:I = 0x7f1219db

.field public static final titlecase_settings:I = 0x7f1219e2

.field public static final track_time_upsell_description:I = 0x7f1219f2

.field public static final track_time_upsell_headline:I = 0x7f1219f3

.field public static final track_time_upsell_link:I = 0x7f1219f4

.field public static final track_time_upsell_title:I = 0x7f1219f5

.field public static final uppercase_header_business_address:I = 0x7f121b33

.field public static final uppercase_payment_types_settings_disabled:I = 0x7f121b5e

.field public static final uppercase_payment_types_settings_primary:I = 0x7f121b5f

.field public static final uppercase_payment_types_settings_secondary:I = 0x7f121b60

.field public static final verification_canceled:I = 0x7f121b97

.field public static final verification_hint:I = 0x7f121b98

.field public static final verification_hint_ca_au:I = 0x7f121b99

.field public static final verification_hint_jp:I = 0x7f121b9a

.field public static final verification_in_progress:I = 0x7f121b9b


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
