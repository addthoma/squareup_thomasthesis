.class public abstract Lcom/squareup/statusbar/event/SwitcherApplet;
.super Ljava/lang/Object;
.source "SwitcherApplet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/statusbar/event/SwitcherApplet$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSwitcherApplet.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SwitcherApplet.kt\ncom/squareup/statusbar/event/SwitcherApplet\n*L\n1#1,48:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0006\u0010\u000e\u001a\u00020\u000fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/statusbar/event/SwitcherApplet;",
        "",
        "title",
        "",
        "intentScreenExtra",
        "(Ljava/lang/String;Ljava/lang/String;)V",
        "getIntentScreenExtra",
        "()Ljava/lang/String;",
        "getTitle",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toBundle",
        "Landroid/os/Bundle;",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/statusbar/event/SwitcherApplet$Companion;

.field public static final INTENT_SCREEN_EXTRA_KEY:Ljava/lang/String; = "INTENT_SCREEN_EXTRA"

.field public static final TITLE_KEY:Ljava/lang/String; = "TITLE"


# instance fields
.field private final intentScreenExtra:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/statusbar/event/SwitcherApplet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/statusbar/event/SwitcherApplet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/statusbar/event/SwitcherApplet;->Companion:Lcom/squareup/statusbar/event/SwitcherApplet$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intentScreenExtra"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->intentScreenExtra:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 18
    move-object v0, p0

    check-cast v0, Lcom/squareup/statusbar/event/SwitcherApplet;

    const/4 v1, 0x1

    if-ne v0, p1, :cond_0

    return v1

    .line 19
    :cond_0
    instance-of v0, p1, Lcom/squareup/statusbar/event/SwitcherApplet;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->title:Ljava/lang/String;

    check-cast p1, Lcom/squareup/statusbar/event/SwitcherApplet;

    iget-object v3, p1, Lcom/squareup/statusbar/event/SwitcherApplet;->title:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    return v2

    .line 22
    :cond_2
    iget-object v0, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->intentScreenExtra:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/statusbar/event/SwitcherApplet;->intentScreenExtra:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_3

    return v2

    :cond_3
    return v1
.end method

.method public final getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->intentScreenExtra:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->title:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 29
    iget-object v1, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->intentScreenExtra:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toBundle()Landroid/os/Bundle;
    .locals 3

    .line 12
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 13
    iget-object v1, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->title:Ljava/lang/String;

    const-string v2, "TITLE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    iget-object v1, p0, Lcom/squareup/statusbar/event/SwitcherApplet;->intentScreenExtra:Ljava/lang/String;

    const-string v2, "INTENT_SCREEN_EXTRA"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
