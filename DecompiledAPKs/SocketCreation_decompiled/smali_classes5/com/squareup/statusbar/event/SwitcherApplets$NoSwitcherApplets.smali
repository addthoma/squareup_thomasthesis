.class public final Lcom/squareup/statusbar/event/SwitcherApplets$NoSwitcherApplets;
.super Ljava/lang/Object;
.source "SwitcherApplets.kt"

# interfaces
.implements Lcom/squareup/statusbar/event/SwitcherApplets;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/statusbar/event/SwitcherApplets;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoSwitcherApplets"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/statusbar/event/SwitcherApplets$NoSwitcherApplets;",
        "Lcom/squareup/statusbar/event/SwitcherApplets;",
        "()V",
        "applets",
        "",
        "Lcom/squareup/statusbar/event/SwitcherApplet;",
        "getApplets",
        "()Ljava/util/List;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/statusbar/event/SwitcherApplets$NoSwitcherApplets;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/statusbar/event/SwitcherApplets$NoSwitcherApplets;

    invoke-direct {v0}, Lcom/squareup/statusbar/event/SwitcherApplets$NoSwitcherApplets;-><init>()V

    sput-object v0, Lcom/squareup/statusbar/event/SwitcherApplets$NoSwitcherApplets;->INSTANCE:Lcom/squareup/statusbar/event/SwitcherApplets$NoSwitcherApplets;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/statusbar/event/SwitcherApplet;",
            ">;"
        }
    .end annotation

    .line 11
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
