.class public final Lcom/squareup/splitticket/RealPricingEngineServiceWrapper;
.super Ljava/lang/Object;
.source "RealPricingEngineServiceWrapper.kt"

# interfaces
.implements Lcom/squareup/splitticket/PricingEngineServiceWrapper;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/splitticket/RealPricingEngineServiceWrapper;",
        "Lcom/squareup/splitticket/PricingEngineServiceWrapper;",
        "pricingEngineService",
        "Lcom/squareup/prices/PricingEngineService;",
        "(Lcom/squareup/prices/PricingEngineService;)V",
        "rulesForOrder",
        "Lrx/Single;",
        "Lcom/squareup/prices/PricingEngineServiceResult;",
        "order",
        "Lcom/squareup/payment/Order;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final pricingEngineService:Lcom/squareup/prices/PricingEngineService;


# direct methods
.method public constructor <init>(Lcom/squareup/prices/PricingEngineService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pricingEngineService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/splitticket/RealPricingEngineServiceWrapper;->pricingEngineService:Lcom/squareup/prices/PricingEngineService;

    return-void
.end method


# virtual methods
.method public rulesForOrder(Lcom/squareup/payment/Order;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/prices/PricingEngineServiceResult;",
            ">;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lcom/squareup/splitticket/RealPricingEngineServiceWrapper;->pricingEngineService:Lcom/squareup/prices/PricingEngineService;

    invoke-virtual {v0, p1}, Lcom/squareup/prices/PricingEngineService;->rulesForOrder(Lcom/squareup/payment/Order;)Lrx/Single;

    move-result-object p1

    const-string v0, "pricingEngineService.rulesForOrder(order)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
