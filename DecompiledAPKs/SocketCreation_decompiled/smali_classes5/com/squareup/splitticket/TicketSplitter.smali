.class public interface abstract Lcom/squareup/splitticket/TicketSplitter;
.super Ljava/lang/Object;
.source "TicketSplitter.kt"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u001e\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0015\u001a\u00020\u0004H&J\u0008\u0010\u0016\u001a\u00020\u000cH&J\u0008\u0010\u0017\u001a\u00020\u000cH&J\u0010\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u0008H&J\u0008\u0010\u001a\u001a\u00020\u001bH&J\u0010\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0019\u001a\u00020\u0008H&J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0019\u001a\u00020\u0008H&J\u0008\u0010\u001f\u001a\u00020\u000cH&J\u0018\u0010 \u001a\u00020\u001e2\u0006\u0010\u0019\u001a\u00020\u00082\u0006\u0010!\u001a\u00020\u001eH&J\u0010\u0010\"\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u0008H&J,\u0010#\u001a\u00020\u001e2\u0006\u0010\u0019\u001a\u00020\u00082\u0006\u0010$\u001a\u00020\u00082\u0008\u0010%\u001a\u0004\u0018\u00010\u00082\u0008\u0010&\u001a\u0004\u0018\u00010\'H&R\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\u000cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R\u0018\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0012\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/splitticket/TicketSplitter;",
        "Lmortar/bundler/Bundler;",
        "onAsyncStateChange",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/splitticket/SplitState;",
        "getOnAsyncStateChange",
        "()Lio/reactivex/Observable;",
        "parentTicketBaseName",
        "",
        "getParentTicketBaseName",
        "()Ljava/lang/String;",
        "selectedEntriesCountAcrossAllTickets",
        "",
        "getSelectedEntriesCountAcrossAllTickets",
        "()I",
        "splitStates",
        "",
        "getSplitStates",
        "()Ljava/util/Collection;",
        "splitStatesByViewIndex",
        "getSplitStatesByViewIndex",
        "createNewSplitTicket",
        "getSavedTicketsCount",
        "getSplitTicketsCount",
        "moveSelectedItemsTo",
        "ticketId",
        "printAllTickets",
        "",
        "printOneTicket",
        "removeOneTicket",
        "",
        "saveAllTickets",
        "saveOneTicket",
        "removeStateAfterSave",
        "splitStateForTicket",
        "updateTicket",
        "name",
        "note",
        "predefinedTicket",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract createNewSplitTicket()Lcom/squareup/splitticket/SplitState;
.end method

.method public abstract getOnAsyncStateChange()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/splitticket/SplitState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getParentTicketBaseName()Ljava/lang/String;
.end method

.method public abstract getSavedTicketsCount()I
.end method

.method public abstract getSelectedEntriesCountAcrossAllTickets()I
.end method

.method public abstract getSplitStates()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/splitticket/SplitState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSplitStatesByViewIndex()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/splitticket/SplitState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSplitTicketsCount()I
.end method

.method public abstract moveSelectedItemsTo(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;
.end method

.method public abstract printAllTickets()V
.end method

.method public abstract printOneTicket(Ljava/lang/String;)V
.end method

.method public abstract removeOneTicket(Ljava/lang/String;)Z
.end method

.method public abstract saveAllTickets()I
.end method

.method public abstract saveOneTicket(Ljava/lang/String;Z)Z
.end method

.method public abstract splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;
.end method

.method public abstract updateTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Z
.end method
