.class public final enum Lcom/squareup/sdk/pos/transaction/Tender$Type;
.super Ljava/lang/Enum;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/pos/transaction/Tender$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/pos/transaction/Tender$Type;

.field public static final enum CARD:Lcom/squareup/sdk/pos/transaction/Tender$Type;

.field public static final enum CASH:Lcom/squareup/sdk/pos/transaction/Tender$Type;

.field public static final enum OTHER:Lcom/squareup/sdk/pos/transaction/Tender$Type;

.field public static final enum SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/transaction/Tender$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 79
    new-instance v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;

    const/4 v1, 0x0

    const-string v2, "CARD"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sdk/pos/transaction/Tender$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;->CARD:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;

    const/4 v2, 0x1

    const-string v3, "CASH"

    invoke-direct {v0, v3, v2}, Lcom/squareup/sdk/pos/transaction/Tender$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;->CASH:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;

    const/4 v3, 0x2

    const-string v4, "SQUARE_GIFT_CARD"

    invoke-direct {v0, v4, v3}, Lcom/squareup/sdk/pos/transaction/Tender$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;->SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    new-instance v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;

    const/4 v4, 0x3

    const-string v5, "OTHER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/sdk/pos/transaction/Tender$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;->OTHER:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/sdk/pos/transaction/Tender$Type;

    .line 78
    sget-object v5, Lcom/squareup/sdk/pos/transaction/Tender$Type;->CARD:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Tender$Type;->CASH:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Tender$Type;->SQUARE_GIFT_CARD:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/sdk/pos/transaction/Tender$Type;->OTHER:Lcom/squareup/sdk/pos/transaction/Tender$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;->$VALUES:[Lcom/squareup/sdk/pos/transaction/Tender$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Tender$Type;
    .locals 1

    .line 78
    const-class v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/pos/transaction/Tender$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/pos/transaction/Tender$Type;
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;->$VALUES:[Lcom/squareup/sdk/pos/transaction/Tender$Type;

    invoke-virtual {v0}, [Lcom/squareup/sdk/pos/transaction/Tender$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/pos/transaction/Tender$Type;

    return-object v0
.end method
