.class public final Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "$AutoValue_Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter<",
        "Lcom/squareup/sdk/pos/transaction/Tender;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardDetailsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/TenderCardDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDetailsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/TenderCashDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final clientIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final createdAtAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/DateTime;",
            ">;"
        }
    .end annotation
.end field

.field private final customerIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final serverIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tipMoneyAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final totalMoneyAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final typeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Tender$Type;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1

    .line 29
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 30
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->clientIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 31
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->serverIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    const-class v0, Lcom/squareup/sdk/pos/transaction/DateTime;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->createdAtAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    const-class v0, Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->totalMoneyAdapter:Lcom/google/gson/TypeAdapter;

    .line 34
    const-class v0, Lcom/squareup/sdk/pos/transaction/Money;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->tipMoneyAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->customerIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    const-class v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    .line 37
    const-class v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->cardDetailsAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    const-class v0, Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->cashDetailsAdapter:Lcom/google/gson/TypeAdapter;

    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/squareup/sdk/pos/transaction/Tender;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 69
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    return-object v2

    .line 73
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    move-object v4, v2

    move-object v5, v4

    move-object v6, v5

    move-object v7, v6

    move-object v8, v7

    move-object v9, v8

    move-object v10, v9

    move-object v11, v10

    move-object v12, v11

    .line 83
    :goto_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 84
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v2, :cond_1

    .line 86
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    .line 89
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    const-string v2, "cardDetails"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x7

    goto :goto_1

    :sswitch_1
    const-string v2, "serverId"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "clientId"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_3
    const-string v2, "createdAt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_4
    const-string/jumbo v2, "type"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x6

    goto :goto_1

    :sswitch_5
    const-string/jumbo v2, "totalMoney"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_6
    const-string v2, "cashDetails"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v1, 0x8

    goto :goto_1

    :sswitch_7
    const-string v2, "customerId"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_8
    const-string/jumbo v2, "tipMoney"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x4

    :cond_2
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 127
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 123
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->cashDetailsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    move-object v12, v0

    goto/16 :goto_0

    .line 119
    :pswitch_1
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->cardDetailsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    move-object v11, v0

    goto/16 :goto_0

    .line 115
    :pswitch_2
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sdk/pos/transaction/Tender$Type;

    move-object v10, v0

    goto/16 :goto_0

    .line 111
    :pswitch_3
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->customerIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v9, v0

    goto/16 :goto_0

    .line 107
    :pswitch_4
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->tipMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sdk/pos/transaction/Money;

    move-object v8, v0

    goto/16 :goto_0

    .line 103
    :pswitch_5
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->totalMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sdk/pos/transaction/Money;

    move-object v7, v0

    goto/16 :goto_0

    .line 99
    :pswitch_6
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->createdAtAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sdk/pos/transaction/DateTime;

    move-object v6, v0

    goto/16 :goto_0

    .line 95
    :pswitch_7
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->serverIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    goto/16 :goto_0

    .line 91
    :pswitch_8
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->clientIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    goto/16 :goto_0

    .line 131
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 132
    new-instance p1, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;

    move-object v3, p1

    invoke-direct/range {v3 .. v12}, Lcom/squareup/sdk/pos/transaction/AutoValue_Tender;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/Tender$Type;Lcom/squareup/sdk/pos/transaction/TenderCardDetails;Lcom/squareup/sdk/pos/transaction/TenderCashDetails;)V

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x77f9671b -> :sswitch_8
        -0x5e3ef667 -> :sswitch_7
        -0x4d716071 -> :sswitch_6
        -0x2b0b4024 -> :sswitch_5
        0x368f3a -> :sswitch_4
        0x23aa6d3b -> :sswitch_3
        0x36253646 -> :sswitch_2
        0x523373be -> :sswitch_1
        0x7ec57d12 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/squareup/sdk/pos/transaction/Tender;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/squareup/sdk/pos/transaction/Tender;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p2, :cond_0

    .line 43
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    return-void

    .line 46
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    const-string v0, "clientId"

    .line 47
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 48
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->clientIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->clientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string v0, "serverId"

    .line 49
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 50
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->serverIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->serverId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string v0, "createdAt"

    .line 51
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 52
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->createdAtAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string/jumbo v0, "totalMoney"

    .line 53
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 54
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->totalMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->totalMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string/jumbo v0, "tipMoney"

    .line 55
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 56
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->tipMoneyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->tipMoney()Lcom/squareup/sdk/pos/transaction/Money;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string v0, "customerId"

    .line 57
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 58
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->customerIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->customerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string/jumbo v0, "type"

    .line 59
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 60
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->type()Lcom/squareup/sdk/pos/transaction/Tender$Type;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string v0, "cardDetails"

    .line 61
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 62
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->cardDetailsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->cardDetails()Lcom/squareup/sdk/pos/transaction/TenderCardDetails;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    const-string v0, "cashDetails"

    .line 63
    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 64
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->cashDetailsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/squareup/sdk/pos/transaction/Tender;->cashDetails()Lcom/squareup/sdk/pos/transaction/TenderCashDetails;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 65
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    return-void
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19
    check-cast p2, Lcom/squareup/sdk/pos/transaction/Tender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Tender$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/squareup/sdk/pos/transaction/Tender;)V

    return-void
.end method
