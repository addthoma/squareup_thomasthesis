.class final Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCashDetails$Builder;
.super Lcom/squareup/sdk/pos/transaction/TenderCashDetails$Builder;
.source "$$AutoValue_TenderCashDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCashDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private buyerTenderedMoney:Lcom/squareup/sdk/pos/transaction/Money;

.field private changeBackMoney:Lcom/squareup/sdk/pos/transaction/Money;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 66
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/TenderCashDetails$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/pos/transaction/TenderCashDetails;
    .locals 4

    .line 87
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCashDetails$Builder;->buyerTenderedMoney:Lcom/squareup/sdk/pos/transaction/Money;

    const-string v1, ""

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " buyerTenderedMoney"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCashDetails$Builder;->changeBackMoney:Lcom/squareup/sdk/pos/transaction/Money;

    if-nez v0, :cond_1

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " changeBackMoney"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 93
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails;

    iget-object v1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCashDetails$Builder;->buyerTenderedMoney:Lcom/squareup/sdk/pos/transaction/Money;

    iget-object v2, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCashDetails$Builder;->changeBackMoney:Lcom/squareup/sdk/pos/transaction/Money;

    invoke-direct {v0, v1, v2}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCashDetails;-><init>(Lcom/squareup/sdk/pos/transaction/Money;Lcom/squareup/sdk/pos/transaction/Money;)V

    return-object v0

    .line 94
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public buyerTenderedMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/TenderCashDetails$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 73
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCashDetails$Builder;->buyerTenderedMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object p0

    .line 71
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null buyerTenderedMoney"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public changeBackMoney(Lcom/squareup/sdk/pos/transaction/Money;)Lcom/squareup/sdk/pos/transaction/TenderCashDetails$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 81
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_TenderCashDetails$Builder;->changeBackMoney:Lcom/squareup/sdk/pos/transaction/Money;

    return-object p0

    .line 79
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null changeBackMoney"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
