.class public final Lcom/squareup/sdk/reader/checkout/TipSettings;
.super Ljava/lang/Object;
.source "TipSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;
    }
.end annotation


# instance fields
.field private final showCustomTipField:Z

.field private final showSeparateTipScreen:Z

.field private final tipPercentages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;)V
    .locals 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showCustomTipField:Z

    .line 30
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showSeparateTipScreen:Z

    .line 31
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->tipPercentages:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;Lcom/squareup/sdk/reader/checkout/TipSettings$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/TipSettings;-><init>(Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;)V

    return-void
.end method

.method public static newBuilder()Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/TipSettings$1;)V

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;
    .locals 2

    .line 53
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/TipSettings$1;)V

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showCustomTipField:Z

    .line 54
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->showCustomTipField(Z)Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showSeparateTipScreen:Z

    .line 55
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->showSeparateTipScreen(Z)Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->tipPercentages:Ljava/util/List;

    .line 56
    invoke-virtual {v0, v1}, Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;->tipPercentages(Ljava/util/List;)Lcom/squareup/sdk/reader/checkout/TipSettings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_5

    .line 61
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 63
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/checkout/TipSettings;

    .line 65
    iget-boolean v2, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showCustomTipField:Z

    iget-boolean v3, p1, Lcom/squareup/sdk/reader/checkout/TipSettings;->showCustomTipField:Z

    if-eq v2, v3, :cond_2

    return v1

    .line 66
    :cond_2
    iget-boolean v2, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showSeparateTipScreen:Z

    iget-boolean v3, p1, Lcom/squareup/sdk/reader/checkout/TipSettings;->showSeparateTipScreen:Z

    if-eq v2, v3, :cond_3

    return v1

    .line 67
    :cond_3
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->tipPercentages:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/TipSettings;->tipPercentages:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    return v1

    :cond_4
    return v0

    :cond_5
    :goto_0
    return v1
.end method

.method public getShowCustomTipField()Z
    .locals 1

    .line 36
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showCustomTipField:Z

    return v0
.end method

.method public getShowSeparateTipScreen()Z
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showSeparateTipScreen:Z

    return v0
.end method

.method public getTipPercentages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->tipPercentages:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 73
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showCustomTipField:Z

    mul-int/lit8 v0, v0, 0x1f

    .line 74
    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showSeparateTipScreen:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 75
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->tipPercentages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TipSettings{showCustomTipField="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showCustomTipField:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showSeparateTipScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->showSeparateTipScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", tipPercentages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TipSettings;->tipPercentages:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
