.class public final Lcom/squareup/sdk/reader/checkout/Tender$Builder;
.super Ljava/lang/Object;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

.field private cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

.field private createdAt:Ljava/util/Date;

.field private tenderId:Ljava/lang/String;

.field private tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private type:Lcom/squareup/sdk/reader/checkout/Tender$Type;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/Tender$Type;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)V
    .locals 0

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 236
    iput-object p2, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    .line 237
    iput-object p3, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tenderId:Ljava/lang/String;

    .line 238
    iput-object p4, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    .line 239
    iput-object p5, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    .line 240
    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    iput-object p2, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->createdAt:Ljava/util/Date;

    .line 241
    new-instance p2, Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p1

    const-wide/16 p3, 0x0

    invoke-direct {p2, p3, p4, p1}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    iput-object p2, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/Tender$Type;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;Lcom/squareup/sdk/reader/checkout/Tender$1;)V
    .locals 0

    .line 224
    invoke-direct/range {p0 .. p5}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/Tender$Type;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Tender;)V
    .locals 6

    .line 245
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender;->access$900(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender;->access$1000(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/Tender$Type;

    move-result-object v2

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender;->access$1100(Lcom/squareup/sdk/reader/checkout/Tender;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender;->access$1200(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    move-result-object v4

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender;->access$1300(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/Tender$Type;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)V

    .line 246
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender;->access$1400(Lcom/squareup/sdk/reader/checkout/Tender;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->createdAt:Ljava/util/Date;

    .line 247
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/Tender;->access$1500(Lcom/squareup/sdk/reader/checkout/Tender;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Tender;Lcom/squareup/sdk/reader/checkout/Tender$1;)V
    .locals 0

    .line 224
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/Tender$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Tender;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Ljava/lang/String;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tenderId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Ljava/util/Date;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->createdAt:Ljava/util/Date;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/Tender$Type;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/sdk/reader/checkout/Tender$Builder;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails;
    .locals 0

    .line 224
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/reader/checkout/Tender;
    .locals 2

    .line 302
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Tender;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/Tender;-><init>(Lcom/squareup/sdk/reader/checkout/Tender$Builder;Lcom/squareup/sdk/reader/checkout/Tender$1;)V

    return-object v0
.end method

.method public cardTender(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 1

    const-string v0, "tenderId"

    .line 260
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tenderId:Ljava/lang/String;

    const-string p1, "cardDetails"

    .line 261
    invoke-static {p2, p1}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    .line 262
    sget-object p1, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CARD:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    const/4 p1, 0x0

    .line 263
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    return-object p0
.end method

.method public cashTender(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 1

    const-string v0, "cashDetails"

    .line 269
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    const/4 p1, 0x0

    .line 270
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tenderId:Ljava/lang/String;

    .line 271
    sget-object v0, Lcom/squareup/sdk/reader/checkout/Tender$Type;->CARD:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    .line 272
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    return-object p0
.end method

.method public createdAt(Ljava/util/Date;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 1

    const-string v0, "createdAt"

    .line 287
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Date;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->createdAt:Ljava/util/Date;

    return-object p0
.end method

.method public otherTender()Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 2

    const/4 v0, 0x0

    .line 278
    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tenderId:Ljava/lang/String;

    .line 279
    sget-object v1, Lcom/squareup/sdk/reader/checkout/Tender$Type;->OTHER:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    iput-object v1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->type:Lcom/squareup/sdk/reader/checkout/Tender$Type;

    .line 280
    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cardDetails:Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    .line 281
    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->cashDetails:Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    return-object p0
.end method

.method public tipMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 1

    const-string/jumbo v0, "tipMoney"

    .line 293
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->tipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public totalMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Tender$Builder;
    .locals 1

    const-string/jumbo v0, "totalMoney"

    .line 252
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 253
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Tender$Builder;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method
