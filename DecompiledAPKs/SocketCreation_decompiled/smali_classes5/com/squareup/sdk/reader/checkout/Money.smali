.class public final Lcom/squareup/sdk/reader/checkout/Money;
.super Ljava/lang/Object;
.source "Money.java"


# instance fields
.field private final amount:J

.field private final currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;


# direct methods
.method public constructor <init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-wide p1, p0, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    const-string p1, "currencyCode"

    .line 27
    invoke-static {p3, p1}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-void
.end method

.method private addExact(JJ)J
    .locals 3

    add-long v0, p1, p3

    xor-long/2addr p1, v0

    xor-long/2addr p3, v0

    and-long/2addr p1, p3

    const-wide/16 p3, 0x0

    cmp-long v2, p1, p3

    if-ltz v2, :cond_0

    return-wide v0

    .line 119
    :cond_0
    new-instance p1, Ljava/lang/ArithmeticException;

    const-string p2, "long overflow"

    invoke-direct {p1, p2}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private checkCurrency(Lcom/squareup/sdk/reader/checkout/Money;)V
    .locals 3

    .line 103
    iget-object v0, p1, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    if-ne v0, v1, :cond_0

    return-void

    .line 104
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot add "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " money to a "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " money"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private plus(J)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ArithmeticException;
        }
    .end annotation

    .line 110
    iget-wide v0, p0, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/squareup/sdk/reader/checkout/Money;->addExact(JJ)J

    move-result-wide p1

    .line 111
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Money;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 126
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 128
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    .line 130
    iget-wide v2, p0, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    iget-wide v4, p1, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    cmp-long v6, v2, v4

    if-eqz v6, :cond_2

    return v1

    .line 131
    :cond_2
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    if-eq v2, p1, :cond_3

    return v1

    :cond_3
    return v0

    :cond_4
    :goto_0
    return v1
.end method

.method public format()Ljava/lang/String;
    .locals 1

    .line 88
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/sdk/reader/checkout/Money;->format(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public format(Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    const-string v0, "locale"

    .line 98
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 99
    invoke-static {p0, p1}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->format(Lcom/squareup/sdk/reader/checkout/Money;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAmount()J
    .locals 2

    .line 35
    iget-wide v0, p0, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    return-wide v0
.end method

.method public getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .line 137
    iget-wide v0, p0, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 138
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->hashCode()I

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method

.method public minus(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/ArithmeticException;
        }
    .end annotation

    const-string v0, "amountMoneyToSubtract"

    .line 79
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/Money;->checkCurrency(Lcom/squareup/sdk/reader/checkout/Money;)V

    .line 81
    iget-wide v0, p1, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    neg-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/sdk/reader/checkout/Money;->plus(J)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    return-object p1
.end method

.method public plus(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/ArithmeticException;
        }
    .end annotation

    const-string v0, "amountMoneyToAdd"

    .line 59
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/Money;->checkCurrency(Lcom/squareup/sdk/reader/checkout/Money;)V

    .line 61
    iget-wide v0, p1, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    invoke-direct {p0, v0, v1}, Lcom/squareup/sdk/reader/checkout/Money;->plus(J)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Money{amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/sdk/reader/checkout/Money;->amount:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", currencyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/Money;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
