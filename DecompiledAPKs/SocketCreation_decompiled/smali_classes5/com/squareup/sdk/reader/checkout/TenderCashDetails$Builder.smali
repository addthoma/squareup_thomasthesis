.class public final Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;
.super Ljava/lang/Object;
.source "TenderCashDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/TenderCashDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Money;)V
    .locals 3

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 100
    new-instance v0, Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p1

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/sdk/reader/checkout/Money;-><init>(JLcom/squareup/sdk/reader/checkout/CurrencyCode;)V

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/TenderCashDetails$1;)V
    .locals 0

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)V
    .locals 1

    .line 104
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->access$400(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;)V

    .line 105
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;->access$500(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;Lcom/squareup/sdk/reader/checkout/TenderCashDetails$1;)V
    .locals 0

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/TenderCashDetails;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 94
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/sdk/reader/checkout/TenderCashDetails;
    .locals 2

    .line 128
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/TenderCashDetails;-><init>(Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;Lcom/squareup/sdk/reader/checkout/TenderCashDetails$1;)V

    return-object v0
.end method

.method public buyerTenderedMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;
    .locals 1

    const-string v0, "buyerTenderedMoney"

    .line 110
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->buyerTenderedMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public changeBackMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;
    .locals 1

    const-string v0, "changeBackMoney"

    .line 116
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCashDetails$Builder;->changeBackMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method
