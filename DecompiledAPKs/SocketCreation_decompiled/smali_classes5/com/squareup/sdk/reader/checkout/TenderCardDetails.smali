.class public final Lcom/squareup/sdk/reader/checkout/TenderCardDetails;
.super Ljava/lang/Object;
.source "TenderCardDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;,
        Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;
    }
.end annotation


# instance fields
.field private final card:Lcom/squareup/sdk/reader/checkout/Card;

.field private final cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

.field private final entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;)V
    .locals 1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->access$100(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;)Lcom/squareup/sdk/reader/checkout/Card;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->card:Lcom/squareup/sdk/reader/checkout/Card;

    .line 48
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    .line 49
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;)Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;-><init>(Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/Card;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->card:Lcom/squareup/sdk/reader/checkout/Card;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;)Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    return-object p0
.end method

.method public static newBuilder(Lcom/squareup/sdk/reader/checkout/Card;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;)Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;
    .locals 2

    const-string v0, "card"

    .line 37
    invoke-static {p0, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "entryMethod"

    .line 38
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 39
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Card;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$1;)V

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/TenderCardDetails;Lcom/squareup/sdk/reader/checkout/TenderCardDetails$1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 88
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;

    .line 90
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->card:Lcom/squareup/sdk/reader/checkout/Card;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->card:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/Card;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 91
    :cond_2
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    if-eq v2, v3, :cond_3

    return v1

    .line 92
    :cond_3
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    if-eqz v2, :cond_4

    invoke-virtual {v2, p1}, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_5

    :goto_0
    return v1

    :cond_5
    return v0

    :cond_6
    :goto_1
    return v1
.end method

.method public getCard()Lcom/squareup/sdk/reader/checkout/Card;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->card:Lcom/squareup/sdk/reader/checkout/Card;

    return-object v0
.end method

.method public getCardReceiptDetails()Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    return-object v0
.end method

.method public getEntryMethod()Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->card:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Card;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 102
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 103
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TenderCardDetails{card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->card:Lcom/squareup/sdk/reader/checkout/Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", entryMethod="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->entryMethod:Lcom/squareup/sdk/reader/checkout/TenderCardDetails$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cardReceiptDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/TenderCardDetails;->cardReceiptDetails:Lcom/squareup/sdk/reader/checkout/CardReceiptDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
