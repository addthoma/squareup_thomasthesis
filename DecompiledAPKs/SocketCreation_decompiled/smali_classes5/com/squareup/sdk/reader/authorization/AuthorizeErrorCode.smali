.class public final enum Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;
.super Ljava/lang/Enum;
.source "AuthorizeErrorCode.java"

# interfaces
.implements Lcom/squareup/sdk/reader/core/ErrorCode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;",
        ">;",
        "Lcom/squareup/sdk/reader/core/ErrorCode;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

.field public static final enum NO_NETWORK:Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

.field public static final enum USAGE_ERROR:Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 15
    new-instance v0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    const/4 v1, 0x0

    const-string v2, "NO_NETWORK"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;->NO_NETWORK:Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    .line 22
    new-instance v0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    const/4 v2, 0x1

    const-string v3, "USAGE_ERROR"

    invoke-direct {v0, v3, v2}, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    .line 10
    sget-object v3, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;->NO_NETWORK:Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;->$VALUES:[Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;->$VALUES:[Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    invoke-virtual {v0}, [Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    return-object v0
.end method


# virtual methods
.method public isUsageError()Z
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;->USAGE_ERROR:Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
