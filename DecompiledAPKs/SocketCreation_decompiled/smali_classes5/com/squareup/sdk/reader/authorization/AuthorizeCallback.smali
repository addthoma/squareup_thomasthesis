.class public interface abstract Lcom/squareup/sdk/reader/authorization/AuthorizeCallback;
.super Ljava/lang/Object;
.source "AuthorizeCallback.java"

# interfaces
.implements Lcom/squareup/sdk/reader/core/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/sdk/reader/core/Callback<",
        "Lcom/squareup/sdk/reader/core/Result<",
        "Lcom/squareup/sdk/reader/authorization/Location;",
        "Lcom/squareup/sdk/reader/core/ResultError<",
        "Lcom/squareup/sdk/reader/authorization/AuthorizeErrorCode;",
        ">;>;>;"
    }
.end annotation
