.class public final synthetic Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 8

    invoke-static {}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->values()[Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->values()[Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ALIPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CASH_APP:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EFTPOS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/4 v4, 0x7

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->FELICA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0x8

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->INTERAC:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0x9

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->JCB:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0xa

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->MASTERCARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0xb

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0xc

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0xd

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0xe

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->VISA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0xf

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EBT:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ordinal()I

    move-result v1

    const/16 v4, 0x10

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/bills/Tender$Type;->values()[Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->CARD:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Type;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/sdk/reader/ReaderSdkConversionUtils$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Type;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
