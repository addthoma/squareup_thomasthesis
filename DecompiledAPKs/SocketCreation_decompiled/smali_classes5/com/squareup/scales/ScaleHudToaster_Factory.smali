.class public final Lcom/squareup/scales/ScaleHudToaster_Factory;
.super Ljava/lang/Object;
.source "ScaleHudToaster_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/scales/ScaleHudToaster;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scaleToastDelayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final scaleTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScaleTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScaleTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->scaleTrackerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->resProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->scaleToastDelayProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/scales/ScaleHudToaster_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScaleTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/scales/ScaleHudToaster_Factory;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/squareup/scales/ScaleHudToaster_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/scales/ScaleHudToaster_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/scales/ScaleTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/thread/executor/MainThread;Z)Lcom/squareup/scales/ScaleHudToaster;
    .locals 8

    .line 58
    new-instance v7, Lcom/squareup/scales/ScaleHudToaster;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/scales/ScaleHudToaster;-><init>(Lcom/squareup/scales/ScaleTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/thread/executor/MainThread;Z)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/scales/ScaleHudToaster;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->scaleTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/scales/ScaleTracker;

    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/scales/ScaleHudToaster_Factory;->scaleToastDelayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-static/range {v1 .. v6}, Lcom/squareup/scales/ScaleHudToaster_Factory;->newInstance(Lcom/squareup/scales/ScaleTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/thread/executor/MainThread;Z)Lcom/squareup/scales/ScaleHudToaster;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/scales/ScaleHudToaster_Factory;->get()Lcom/squareup/scales/ScaleHudToaster;

    move-result-object v0

    return-object v0
.end method
