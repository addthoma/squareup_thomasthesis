.class public final enum Lcom/squareup/scales/analytics/ScaleEventValue;
.super Ljava/lang/Enum;
.source "ScalesHardwareAnalytics.kt"

# interfaces
.implements Lcom/squareup/analytics/NamedEvent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/scales/analytics/ScaleEventValue;",
        ">;",
        "Lcom/squareup/analytics/NamedEvent;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0008\u001a\u00020\u0004H\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/scales/analytics/ScaleEventValue;",
        "",
        "Lcom/squareup/analytics/NamedEvent;",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue",
        "()Ljava/lang/String;",
        "getName",
        "CONNECTED",
        "DISCONNECTED",
        "READ_FAIL",
        "READ_SUCCESS",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/scales/analytics/ScaleEventValue;

.field public static final enum CONNECTED:Lcom/squareup/scales/analytics/ScaleEventValue;

.field public static final enum DISCONNECTED:Lcom/squareup/scales/analytics/ScaleEventValue;

.field public static final enum READ_FAIL:Lcom/squareup/scales/analytics/ScaleEventValue;

.field public static final enum READ_SUCCESS:Lcom/squareup/scales/analytics/ScaleEventValue;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/scales/analytics/ScaleEventValue;

    new-instance v1, Lcom/squareup/scales/analytics/ScaleEventValue;

    const/4 v2, 0x0

    const-string v3, "CONNECTED"

    const-string v4, "Scale: Connected"

    .line 111
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/scales/analytics/ScaleEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/scales/analytics/ScaleEventValue;->CONNECTED:Lcom/squareup/scales/analytics/ScaleEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/scales/analytics/ScaleEventValue;

    const/4 v2, 0x1

    const-string v3, "DISCONNECTED"

    const-string v4, "Scale: Disconnected"

    .line 112
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/scales/analytics/ScaleEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/scales/analytics/ScaleEventValue;->DISCONNECTED:Lcom/squareup/scales/analytics/ScaleEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/scales/analytics/ScaleEventValue;

    const/4 v2, 0x2

    const-string v3, "READ_FAIL"

    const-string v4, "Scale: Read Fail"

    .line 113
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/scales/analytics/ScaleEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/scales/analytics/ScaleEventValue;->READ_FAIL:Lcom/squareup/scales/analytics/ScaleEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/scales/analytics/ScaleEventValue;

    const/4 v2, 0x3

    const-string v3, "READ_SUCCESS"

    const-string v4, "Scale: Read Success"

    .line 114
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/scales/analytics/ScaleEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/scales/analytics/ScaleEventValue;->READ_SUCCESS:Lcom/squareup/scales/analytics/ScaleEventValue;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/scales/analytics/ScaleEventValue;->$VALUES:[Lcom/squareup/scales/analytics/ScaleEventValue;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/scales/analytics/ScaleEventValue;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/scales/analytics/ScaleEventValue;
    .locals 1

    const-class v0, Lcom/squareup/scales/analytics/ScaleEventValue;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/scales/analytics/ScaleEventValue;

    return-object p0
.end method

.method public static values()[Lcom/squareup/scales/analytics/ScaleEventValue;
    .locals 1

    sget-object v0, Lcom/squareup/scales/analytics/ScaleEventValue;->$VALUES:[Lcom/squareup/scales/analytics/ScaleEventValue;

    invoke-virtual {v0}, [Lcom/squareup/scales/analytics/ScaleEventValue;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/scales/analytics/ScaleEventValue;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/scales/analytics/ScaleEventValue;->value:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/scales/analytics/ScaleEventValue;->value:Ljava/lang/String;

    return-object v0
.end method
