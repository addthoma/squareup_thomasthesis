.class public final Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;
.super Ljava/lang/Object;
.source "ScalesHardwareAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logScaleConnected",
        "",
        "scale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "logScaleDisconnected",
        "logScaleReadingEvent",
        "event",
        "Lcom/squareup/scales/ScaleEvent;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public final logScaleConnected(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 3

    const-string v0, "scale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 29
    new-instance v1, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleConnectionLogEvent;

    .line 30
    sget-object v2, Lcom/squareup/scales/analytics/ScaleEventValue;->CONNECTED:Lcom/squareup/scales/analytics/ScaleEventValue;

    .line 29
    invoke-direct {v1, v2, p1}, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleConnectionLogEvent;-><init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 28
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public final logScaleDisconnected(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 3

    const-string v0, "scale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 39
    new-instance v1, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleConnectionLogEvent;

    .line 40
    sget-object v2, Lcom/squareup/scales/analytics/ScaleEventValue;->DISCONNECTED:Lcom/squareup/scales/analytics/ScaleEventValue;

    .line 39
    invoke-direct {v1, v2, p1}, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleConnectionLogEvent;-><init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 38
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public final logScaleReadingEvent(Lcom/squareup/scales/ScaleEvent;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    instance-of v0, p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 52
    new-instance v1, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleStableReadingLogEvent;

    .line 53
    sget-object v2, Lcom/squareup/scales/analytics/ScaleEventValue;->READ_SUCCESS:Lcom/squareup/scales/analytics/ScaleEventValue;

    .line 54
    check-cast p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    .line 52
    invoke-direct {v1, v2, p1}, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleStableReadingLogEvent;-><init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleEvent$StableReadingEvent;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 51
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 57
    :cond_0
    instance-of v0, p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 58
    new-instance v1, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleErrorReadingLogEvent;

    .line 59
    sget-object v2, Lcom/squareup/scales/analytics/ScaleEventValue;->READ_FAIL:Lcom/squareup/scales/analytics/ScaleEventValue;

    .line 60
    check-cast p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    .line 58
    invoke-direct {v1, v2, p1}, Lcom/squareup/scales/analytics/ScaleLogEvent$ScaleErrorReadingLogEvent;-><init>(Lcom/squareup/scales/analytics/ScaleEventValue;Lcom/squareup/scales/ScaleEvent$NoReadingEvent;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 57
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 63
    :cond_1
    instance-of p1, p1, Lcom/squareup/scales/ScaleEvent$UnstableReadingEvent;

    if-eqz p1, :cond_2

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
