.class public final Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;
.super Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;
.source "UsbScaleInterpreter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ValidStatus"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008 \n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001Bc\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010\u0010J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\rH\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u000fH\u00c6\u0003J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\u0003H\u00c6\u0003J\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\t\u0010(\u001a\u00020\u0003H\u00c6\u0003J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\u0003H\u00c6\u0003J\u0010\u0010+\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0016J\u0082\u0001\u0010,\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u00032\u0008\u0008\u0002\u0010\n\u001a\u00020\u00032\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r2\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00c6\u0001\u00a2\u0006\u0002\u0010-J\u0013\u0010.\u001a\u00020\u00032\u0008\u0010/\u001a\u0004\u0018\u000100H\u00d6\u0003J\t\u00101\u001a\u000202H\u00d6\u0001J\t\u00103\u001a\u000204H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0012R\u0015\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0017\u001a\u0004\u0008\u0015\u0010\u0016R\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0012R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0012R\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0012\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;",
        "Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;",
        "eeproomError",
        "",
        "ramError",
        "atZero",
        "stable",
        "faultyCalibration",
        "romError",
        "overCapacity",
        "underCapacity",
        "initialZeroError",
        "netOrGross",
        "Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;",
        "range",
        "Lcom/squareup/scales/UsbScaleInterpreter$RangeType;",
        "(ZZZZZZZZLjava/lang/Boolean;Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;Lcom/squareup/scales/UsbScaleInterpreter$RangeType;)V",
        "getAtZero",
        "()Z",
        "getEeproomError",
        "getFaultyCalibration",
        "getInitialZeroError",
        "()Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        "getNetOrGross",
        "()Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;",
        "getOverCapacity",
        "getRamError",
        "getRange",
        "()Lcom/squareup/scales/UsbScaleInterpreter$RangeType;",
        "getRomError",
        "getStable",
        "getUnderCapacity",
        "component1",
        "component10",
        "component11",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "(ZZZZZZZZLjava/lang/Boolean;Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;Lcom/squareup/scales/UsbScaleInterpreter$RangeType;)Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final atZero:Z

.field private final eeproomError:Z

.field private final faultyCalibration:Z

.field private final initialZeroError:Ljava/lang/Boolean;

.field private final netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

.field private final overCapacity:Z

.field private final ramError:Z

.field private final range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

.field private final romError:Z

.field private final stable:Z

.field private final underCapacity:Z


# direct methods
.method public constructor <init>(ZZZZZZZZLjava/lang/Boolean;Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;Lcom/squareup/scales/UsbScaleInterpreter$RangeType;)V
    .locals 1

    const/4 v0, 0x0

    .line 107
    invoke-direct {p0, v0}, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->eeproomError:Z

    iput-boolean p2, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->ramError:Z

    iput-boolean p3, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->atZero:Z

    iput-boolean p4, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->stable:Z

    iput-boolean p5, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->faultyCalibration:Z

    iput-boolean p6, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->romError:Z

    iput-boolean p7, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->overCapacity:Z

    iput-boolean p8, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->underCapacity:Z

    iput-object p9, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->initialZeroError:Ljava/lang/Boolean;

    iput-object p10, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    iput-object p11, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;ZZZZZZZZLjava/lang/Boolean;Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;Lcom/squareup/scales/UsbScaleInterpreter$RangeType;ILjava/lang/Object;)Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;
    .locals 12

    move-object v0, p0

    move/from16 v1, p12

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->eeproomError:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->ramError:Z

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->atZero:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->stable:Z

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->faultyCalibration:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->romError:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->overCapacity:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-boolean v9, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->underCapacity:Z

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->initialZeroError:Ljava/lang/Boolean;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    goto :goto_a

    :cond_a
    move-object/from16 v1, p11

    :goto_a
    move p1, v2

    move p2, v3

    move p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v1

    invoke-virtual/range {p0 .. p11}, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->copy(ZZZZZZZZLjava/lang/Boolean;Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;Lcom/squareup/scales/UsbScaleInterpreter$RangeType;)Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->eeproomError:Z

    return v0
.end method

.method public final component10()Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    return-object v0
.end method

.method public final component11()Lcom/squareup/scales/UsbScaleInterpreter$RangeType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->ramError:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->atZero:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->stable:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->faultyCalibration:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->romError:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->overCapacity:Z

    return v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->underCapacity:Z

    return v0
.end method

.method public final component9()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->initialZeroError:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final copy(ZZZZZZZZLjava/lang/Boolean;Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;Lcom/squareup/scales/UsbScaleInterpreter$RangeType;)Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;
    .locals 13

    new-instance v12, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;

    move-object v0, v12

    move v1, p1

    move v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v11}, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;-><init>(ZZZZZZZZLjava/lang/Boolean;Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;Lcom/squareup/scales/UsbScaleInterpreter$RangeType;)V

    return-object v12
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->eeproomError:Z

    iget-boolean v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->eeproomError:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->ramError:Z

    iget-boolean v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->ramError:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->atZero:Z

    iget-boolean v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->atZero:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->stable:Z

    iget-boolean v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->stable:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->faultyCalibration:Z

    iget-boolean v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->faultyCalibration:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->romError:Z

    iget-boolean v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->romError:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->overCapacity:Z

    iget-boolean v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->overCapacity:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->underCapacity:Z

    iget-boolean v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->underCapacity:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->initialZeroError:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->initialZeroError:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    iget-object v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    iget-object p1, p1, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAtZero()Z
    .locals 1

    .line 98
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->atZero:Z

    return v0
.end method

.method public final getEeproomError()Z
    .locals 1

    .line 96
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->eeproomError:Z

    return v0
.end method

.method public final getFaultyCalibration()Z
    .locals 1

    .line 100
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->faultyCalibration:Z

    return v0
.end method

.method public final getInitialZeroError()Ljava/lang/Boolean;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->initialZeroError:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final getNetOrGross()Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    return-object v0
.end method

.method public final getOverCapacity()Z
    .locals 1

    .line 102
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->overCapacity:Z

    return v0
.end method

.method public final getRamError()Z
    .locals 1

    .line 97
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->ramError:Z

    return v0
.end method

.method public final getRange()Lcom/squareup/scales/UsbScaleInterpreter$RangeType;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    return-object v0
.end method

.method public final getRomError()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->romError:Z

    return v0
.end method

.method public final getStable()Z
    .locals 1

    .line 99
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->stable:Z

    return v0
.end method

.method public final getUnderCapacity()Z
    .locals 1

    .line 103
    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->underCapacity:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->eeproomError:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->ramError:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->atZero:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->stable:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->faultyCalibration:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->romError:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->overCapacity:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->underCapacity:Z

    if-eqz v2, :cond_7

    goto :goto_0

    :cond_7
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->initialZeroError:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_9
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ValidStatus(eeproomError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->eeproomError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", ramError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->ramError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", atZero="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->atZero:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", stable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->stable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", faultyCalibration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->faultyCalibration:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", romError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->romError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", overCapacity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->overCapacity:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", underCapacity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->underCapacity:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", initialZeroError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->initialZeroError:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", netOrGross="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->netOrGross:Lcom/squareup/scales/UsbScaleInterpreter$NetOrGross;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$ScaleStatus$ValidStatus;->range:Lcom/squareup/scales/UsbScaleInterpreter$RangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
