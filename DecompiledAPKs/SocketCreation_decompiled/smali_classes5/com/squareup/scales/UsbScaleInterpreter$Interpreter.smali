.class public abstract Lcom/squareup/scales/UsbScaleInterpreter$Interpreter;
.super Ljava/lang/Object;
.source "UsbScaleInterpreter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/UsbScaleInterpreter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Interpreter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;,
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightResponse;,
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;,
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellOverMaximumWeightResponse;,
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnderMinimumWeightResponse;,
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnstableReadingResponse;,
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightReadingErrorResponse;,
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellUnitErrorResponse;,
        Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellDecodingResponseError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\t\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0001\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/scales/UsbScaleInterpreter$Interpreter;",
        "",
        "()V",
        "BrecknellDecodingResponseError",
        "BrecknellNormalWeightResponse",
        "BrecknellOverMaximumWeightResponse",
        "BrecknellUnderMinimumWeightResponse",
        "BrecknellUnitErrorResponse",
        "BrecknellUnstableReadingResponse",
        "BrecknellWeightReadingErrorResponse",
        "BrecknellWeightResponse",
        "WeightResponse",
        "Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$WeightResponse;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter;-><init>()V

    return-void
.end method
