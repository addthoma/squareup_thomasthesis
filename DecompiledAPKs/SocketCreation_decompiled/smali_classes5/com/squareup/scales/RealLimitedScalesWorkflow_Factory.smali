.class public final Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealLimitedScalesWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/scales/RealLimitedScalesWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/NoConnectedScalesWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ShowingConnectedScalesWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/NoConnectedScalesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ShowingConnectedScalesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/NoConnectedScalesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ShowingConnectedScalesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;",
            ">;)",
            "Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/scales/NoConnectedScalesWorkflow;Lcom/squareup/scales/ShowingConnectedScalesWorkflow;Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;)Lcom/squareup/scales/RealLimitedScalesWorkflow;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/scales/RealLimitedScalesWorkflow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/scales/RealLimitedScalesWorkflow;-><init>(Lcom/squareup/scales/NoConnectedScalesWorkflow;Lcom/squareup/scales/ShowingConnectedScalesWorkflow;Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/scales/RealLimitedScalesWorkflow;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/scales/NoConnectedScalesWorkflow;

    iget-object v1, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/scales/ShowingConnectedScalesWorkflow;

    iget-object v2, p0, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;

    invoke-static {v0, v1, v2}, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;->newInstance(Lcom/squareup/scales/NoConnectedScalesWorkflow;Lcom/squareup/scales/ShowingConnectedScalesWorkflow;Lcom/squareup/connectedscalesdata/ConnectedScalesRepository;)Lcom/squareup/scales/RealLimitedScalesWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/scales/RealLimitedScalesWorkflow_Factory;->get()Lcom/squareup/scales/RealLimitedScalesWorkflow;

    move-result-object v0

    return-object v0
.end method
