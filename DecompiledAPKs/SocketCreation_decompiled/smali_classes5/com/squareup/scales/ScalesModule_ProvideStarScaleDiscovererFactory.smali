.class public final Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;
.super Ljava/lang/Object;
.source "ScalesModule_ProvideStarScaleDiscovererFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/scales/StarScaleDiscoverer;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/scales/ScalesModule;

.field private final realScaleTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/RealScaleTracker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/scales/ScalesModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/scales/ScalesModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/RealScaleTracker;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->module:Lcom/squareup/scales/ScalesModule;

    .line 35
    iput-object p2, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->realScaleTrackerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/scales/ScalesModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/scales/ScalesModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/RealScaleTracker;",
            ">;)",
            "Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;-><init>(Lcom/squareup/scales/ScalesModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static provideStarScaleDiscoverer(Lcom/squareup/scales/ScalesModule;Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/RealScaleTracker;)Lcom/squareup/scales/StarScaleDiscoverer;
    .locals 0

    .line 56
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/scales/ScalesModule;->provideStarScaleDiscoverer(Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/RealScaleTracker;)Lcom/squareup/scales/StarScaleDiscoverer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/scales/StarScaleDiscoverer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/scales/StarScaleDiscoverer;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->module:Lcom/squareup/scales/ScalesModule;

    iget-object v1, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    iget-object v3, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v4, p0, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->realScaleTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/scales/RealScaleTracker;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->provideStarScaleDiscoverer(Lcom/squareup/scales/ScalesModule;Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/RealScaleTracker;)Lcom/squareup/scales/StarScaleDiscoverer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/scales/ScalesModule_ProvideStarScaleDiscovererFactory;->get()Lcom/squareup/scales/StarScaleDiscoverer;

    move-result-object v0

    return-object v0
.end method
