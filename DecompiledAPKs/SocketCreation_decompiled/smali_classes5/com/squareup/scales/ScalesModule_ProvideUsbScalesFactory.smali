.class public final Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;
.super Ljava/lang/Object;
.source "ScalesModule_ProvideUsbScalesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/scales/SerialUsbScaleDiscoverer;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final managerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/scales/ScalesModule;

.field private final realScaleTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/RealScaleTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final usbDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;"
        }
    .end annotation
.end field

.field private final usbScaleInterpreterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/UsbScaleInterpreter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/scales/ScalesModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/scales/ScalesModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/RealScaleTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/UsbScaleInterpreter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->module:Lcom/squareup/scales/ScalesModule;

    .line 45
    iput-object p2, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->usbDiscovererProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->managerProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->realScaleTrackerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->usbScaleInterpreterProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p8, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/scales/ScalesModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/scales/ScalesModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/RealScaleTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/UsbScaleInterpreter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;",
            ">;)",
            "Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;"
        }
    .end annotation

    .line 65
    new-instance v9, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;-><init>(Lcom/squareup/scales/ScalesModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static provideUsbScales(Lcom/squareup/scales/ScalesModule;Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)Lcom/squareup/scales/SerialUsbScaleDiscoverer;
    .locals 0

    .line 72
    invoke-virtual/range {p0 .. p7}, Lcom/squareup/scales/ScalesModule;->provideUsbScales(Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)Lcom/squareup/scales/SerialUsbScaleDiscoverer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/scales/SerialUsbScaleDiscoverer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/scales/SerialUsbScaleDiscoverer;
    .locals 8

    .line 56
    iget-object v0, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->module:Lcom/squareup/scales/ScalesModule;

    iget-object v1, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->usbDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/usb/UsbDiscoverer;

    iget-object v2, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->managerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/hardware/usb/UsbManager;

    iget-object v3, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->realScaleTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/scales/RealScaleTracker;

    iget-object v4, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->usbScaleInterpreterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/scales/UsbScaleInterpreter;

    iget-object v5, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/thread/executor/MainThread;

    iget-object v6, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v7, p0, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;

    invoke-static/range {v0 .. v7}, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->provideUsbScales(Lcom/squareup/scales/ScalesModule;Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/scales/UsbScaleInterpreter;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;Lcom/squareup/scales/analytics/ScalesHardwareAnalytics;)Lcom/squareup/scales/SerialUsbScaleDiscoverer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/scales/ScalesModule_ProvideUsbScalesFactory;->get()Lcom/squareup/scales/SerialUsbScaleDiscoverer;

    move-result-object v0

    return-object v0
.end method
