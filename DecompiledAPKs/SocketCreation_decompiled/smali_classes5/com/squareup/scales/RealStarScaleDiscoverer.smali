.class public final Lcom/squareup/scales/RealStarScaleDiscoverer;
.super Ljava/lang/Object;
.source "StarScaleDiscoverer.kt"

# interfaces
.implements Lcom/squareup/scales/StarScaleDiscoverer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStarScaleDiscoverer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StarScaleDiscoverer.kt\ncom/squareup/scales/RealStarScaleDiscoverer\n*L\n1#1,75:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000O\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0011\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u0013\u001a\u00020\u0014H\u0002J\u0012\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u0016H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0012\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/scales/RealStarScaleDiscoverer;",
        "Lcom/squareup/scales/StarScaleDiscoverer;",
        "application",
        "Landroid/app/Application;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "realScaleTracker",
        "Lcom/squareup/scales/RealScaleTracker;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/settings/server/Features;)V",
        "hardwareScalesList",
        "",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "starDeviceManager",
        "Lcom/starmicronics/starmgsio/StarDeviceManager;",
        "starDeviceManagerCallback",
        "com/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1",
        "Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;",
        "getInterfaceType",
        "Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MANUFACTURER:Lcom/squareup/scales/ScaleTracker$Manufacturer;


# instance fields
.field private final application:Landroid/app/Application;

.field private final features:Lcom/squareup/settings/server/Features;

.field private hardwareScalesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

.field private starDeviceManager:Lcom/starmicronics/starmgsio/StarDeviceManager;

.field private final starDeviceManagerCallback:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/scales/RealStarScaleDiscoverer;->Companion:Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;

    .line 34
    sget-object v0, Lcom/squareup/scales/ScaleTracker$Manufacturer;->STARMICRONICS:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    sput-object v0, Lcom/squareup/scales/RealStarScaleDiscoverer;->MANUFACTURER:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/scales/RealScaleTracker;Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "realScaleTracker"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-object p3, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

    iput-object p4, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->features:Lcom/squareup/settings/server/Features;

    .line 37
    new-instance p1, Lcom/starmicronics/starmgsio/StarDeviceManager;

    iget-object p2, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->application:Landroid/app/Application;

    check-cast p2, Landroid/content/Context;

    invoke-direct {p0}, Lcom/squareup/scales/RealStarScaleDiscoverer;->getInterfaceType()Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lcom/starmicronics/starmgsio/StarDeviceManager;-><init>(Landroid/content/Context;Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;)V

    iput-object p1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->starDeviceManager:Lcom/starmicronics/starmgsio/StarDeviceManager;

    .line 38
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->hardwareScalesList:Ljava/util/List;

    .line 48
    new-instance p1, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;

    invoke-direct {p1, p0}, Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;-><init>(Lcom/squareup/scales/RealStarScaleDiscoverer;)V

    iput-object p1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->starDeviceManagerCallback:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/scales/RealStarScaleDiscoverer;->Companion:Lcom/squareup/scales/RealStarScaleDiscoverer$Companion;

    return-object v0
.end method

.method public static final synthetic access$getApplication$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Landroid/app/Application;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->application:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic access$getHardwareScalesList$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Ljava/util/List;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->hardwareScalesList:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getMANUFACTURER$cp()Lcom/squareup/scales/ScaleTracker$Manufacturer;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/scales/RealStarScaleDiscoverer;->MANUFACTURER:Lcom/squareup/scales/ScaleTracker$Manufacturer;

    return-object v0
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method public static final synthetic access$getRealScaleTracker$p(Lcom/squareup/scales/RealStarScaleDiscoverer;)Lcom/squareup/scales/RealScaleTracker;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->realScaleTracker:Lcom/squareup/scales/RealScaleTracker;

    return-object p0
.end method

.method public static final synthetic access$setHardwareScalesList$p(Lcom/squareup/scales/RealStarScaleDiscoverer;Ljava/util/List;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->hardwareScalesList:Ljava/util/List;

    return-void
.end method

.method private final getInterfaceType()Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BLE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;->All:Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;

    goto :goto_0

    .line 73
    :cond_0
    sget-object v0, Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;->USB:Lcom/starmicronics/starmgsio/StarDeviceManager$InterfaceType;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 41
    iget-object p1, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->starDeviceManager:Lcom/starmicronics/starmgsio/StarDeviceManager;

    iget-object v0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->starDeviceManagerCallback:Lcom/squareup/scales/RealStarScaleDiscoverer$starDeviceManagerCallback$1;

    check-cast v0, Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;

    invoke-virtual {p1, v0}, Lcom/starmicronics/starmgsio/StarDeviceManager;->scanForScales(Lcom/starmicronics/starmgsio/StarDeviceManagerCallback;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/scales/RealStarScaleDiscoverer;->starDeviceManager:Lcom/starmicronics/starmgsio/StarDeviceManager;

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/StarDeviceManager;->stopScan()V

    return-void
.end method
