.class final Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$3;
.super Lkotlin/jvm/internal/Lambda;
.source "ShowingConnectedScalesLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;->invoke(Lcom/squareup/cycler/Recycler$Config;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lkotlin/Unit;",
        ">;",
        "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$FooterRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nShowingConnectedScalesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ShowingConnectedScalesLayoutRunner.kt\ncom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$3\n+ 2 BlueprintUiModel.kt\ncom/squareup/blueprint/mosaic/BlueprintUiModelKt\n+ 3 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt\n+ 4 BlueprintDsl.kt\ncom/squareup/blueprint/BlueprintDslKt$center$1\n*L\n1#1,222:1\n17#2,11:223\n26#2:234\n22#2,3:235\n28#2:265\n125#3,5:238\n113#3,9:243\n119#3:252\n120#3,11:254\n113#4:253\n*E\n*S KotlinDebug\n*F\n+ 1 ShowingConnectedScalesLayoutRunner.kt\ncom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$3\n*L\n164#1,11:223\n164#1:234\n164#1,3:235\n164#1:265\n164#1,5:238\n164#1,9:243\n164#1:252\n164#1,11:254\n164#1:253\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "<anonymous parameter 0>",
        "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$FooterRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;


# direct methods
.method constructor <init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$3;->this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/mosaic/core/UiModelContext;

    check-cast p2, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$FooterRow;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$3;->invoke(Lcom/squareup/mosaic/core/UiModelContext;Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$FooterRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/mosaic/core/UiModelContext;Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$FooterRow;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row$FooterRow;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "$receiver"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "<anonymous parameter 0>"

    move-object/from16 v2, p2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    new-instance v1, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    .line 235
    invoke-interface/range {p1 .. p1}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v1

    .line 227
    invoke-direct/range {v2 .. v8}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;-><init>(Ljava/lang/Object;ZZLcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 165
    move-object v2, v1

    check-cast v2, Lcom/squareup/blueprint/BlueprintContext;

    const/4 v3, 0x0

    .line 238
    invoke-static {v3}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Lcom/squareup/resources/DimenModel;

    .line 239
    invoke-static {v3}, Lcom/squareup/resources/DimenModelsKt;->getDp(I)Lcom/squareup/resources/FixedDimen;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Lcom/squareup/resources/DimenModel;

    .line 242
    new-instance v3, Lcom/squareup/blueprint/InsetBlock;

    invoke-interface {v2}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xf8

    const/4 v15, 0x0

    move-object v5, v3

    invoke-direct/range {v5 .. v15}, Lcom/squareup/blueprint/InsetBlock;-><init>(Ljava/lang/Object;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 166
    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    sget v4, Lcom/squareup/connectedscales/impl/R$dimen;->scales_list_footer_vertical_padding:I

    invoke-static {v1, v4}, Lcom/squareup/mosaic/core/UiModelKt;->dimen(Lcom/squareup/mosaic/core/UiModel;I)Lcom/squareup/resources/DimenModel;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/blueprint/InsetBlock;->setTopInset(Lcom/squareup/resources/DimenModel;)V

    .line 167
    move-object v4, v3

    check-cast v4, Lcom/squareup/blueprint/BlueprintContext;

    .line 244
    sget-object v7, Lcom/squareup/blueprint/CenterBlock$Type;->BOTH:Lcom/squareup/blueprint/CenterBlock$Type;

    .line 252
    invoke-interface {v4}, Lcom/squareup/blueprint/BlueprintContext;->createParams()Ljava/lang/Object;

    move-result-object v6

    .line 248
    new-instance v11, Lcom/squareup/blueprint/CenterBlock;

    const/4 v8, 0x0

    const/4 v9, 0x4

    move-object v5, v11

    invoke-direct/range {v5 .. v10}, Lcom/squareup/blueprint/CenterBlock;-><init>(Ljava/lang/Object;Lcom/squareup/blueprint/CenterBlock$Type;Lcom/squareup/blueprint/Block;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v5, p0

    .line 168
    iget-object v6, v5, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$3;->this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;

    iget-object v6, v6, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;->this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;

    move-object v7, v11

    check-cast v7, Lcom/squareup/blueprint/BlueprintContext;

    invoke-static {v6, v7}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->access$scalesFooter(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Lcom/squareup/blueprint/BlueprintContext;)V

    .line 169
    check-cast v11, Lcom/squareup/blueprint/Block;

    .line 247
    invoke-interface {v4, v11}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 170
    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 242
    invoke-interface {v2, v3}, Lcom/squareup/blueprint/BlueprintContext;->add(Lcom/squareup/blueprint/Block;)V

    .line 233
    invoke-interface {v0, v1}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
