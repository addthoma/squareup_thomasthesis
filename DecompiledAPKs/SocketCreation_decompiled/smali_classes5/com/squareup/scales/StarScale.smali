.class public final Lcom/squareup/scales/StarScale;
.super Ljava/lang/Object;
.source "StarScale.kt"

# interfaces
.implements Lcom/squareup/scales/Scale;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStarScale.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StarScale.kt\ncom/squareup/scales/StarScale\n*L\n1#1,133:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000M\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001d\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0000\u00a2\u0006\u0002\u0008\u001bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000eX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000eX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0010R\u0014\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/scales/StarScale;",
        "Lcom/squareup/scales/Scale;",
        "application",
        "Landroid/app/Application;",
        "(Landroid/app/Application;)V",
        "scale",
        "Lcom/starmicronics/starmgsio/Scale;",
        "scaleCallback",
        "com/squareup/scales/StarScale$scaleCallback$1",
        "Lcom/squareup/scales/StarScale$scaleCallback$1;",
        "scaleEventRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/scales/ScaleEvent;",
        "scaleEvents",
        "Lio/reactivex/Observable;",
        "getScaleEvents",
        "()Lio/reactivex/Observable;",
        "unitOfMeasurement",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "getUnitOfMeasurement",
        "unitOfMeasurementRelay",
        "startConnection",
        "",
        "identifier",
        "",
        "interfaceType",
        "Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;",
        "startConnection$impl_release",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private scale:Lcom/starmicronics/starmgsio/Scale;

.field private final scaleCallback:Lcom/squareup/scales/StarScale$scaleCallback$1;

.field private final scaleEventRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/scales/ScaleEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final scaleEvents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/ScaleEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final unitOfMeasurement:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/UnitOfMeasurement;",
            ">;"
        }
    .end annotation
.end field

.field private final unitOfMeasurementRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/scales/UnitOfMeasurement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 2

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/scales/StarScale;->application:Landroid/app/Application;

    .line 26
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/scales/StarScale;->scaleEventRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 27
    iget-object p1, p0, Lcom/squareup/scales/StarScale;->scaleEventRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hide()Lio/reactivex/Observable;

    move-result-object p1

    const-string v1, "scaleEventRelay.hide()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/scales/StarScale;->scaleEvents:Lio/reactivex/Observable;

    .line 29
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/scales/StarScale;->unitOfMeasurementRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 30
    iget-object p1, p0, Lcom/squareup/scales/StarScale;->unitOfMeasurementRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 31
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lio/reactivex/Observable;->hide()Lio/reactivex/Observable;

    move-result-object p1

    const-string/jumbo v0, "unitOfMeasurementRelay\n \u2026ilChanged()\n      .hide()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/scales/StarScale;->unitOfMeasurement:Lio/reactivex/Observable;

    .line 61
    new-instance p1, Lcom/squareup/scales/StarScale$scaleCallback$1;

    invoke-direct {p1, p0}, Lcom/squareup/scales/StarScale$scaleCallback$1;-><init>(Lcom/squareup/scales/StarScale;)V

    iput-object p1, p0, Lcom/squareup/scales/StarScale;->scaleCallback:Lcom/squareup/scales/StarScale$scaleCallback$1;

    return-void
.end method

.method public static final synthetic access$getScale$p(Lcom/squareup/scales/StarScale;)Lcom/starmicronics/starmgsio/Scale;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/scales/StarScale;->scale:Lcom/starmicronics/starmgsio/Scale;

    return-object p0
.end method

.method public static final synthetic access$getScaleEventRelay$p(Lcom/squareup/scales/StarScale;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/scales/StarScale;->scaleEventRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getUnitOfMeasurementRelay$p(Lcom/squareup/scales/StarScale;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/scales/StarScale;->unitOfMeasurementRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$setScale$p(Lcom/squareup/scales/StarScale;Lcom/starmicronics/starmgsio/Scale;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/squareup/scales/StarScale;->scale:Lcom/starmicronics/starmgsio/Scale;

    return-void
.end method


# virtual methods
.method public getScaleEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/ScaleEvent;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/scales/StarScale;->scaleEvents:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getUnitOfMeasurement()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/scales/UnitOfMeasurement;",
            ">;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/scales/StarScale;->unitOfMeasurement:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final startConnection$impl_release(Ljava/lang/String;Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;)V
    .locals 3

    const-string v0, "identifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interfaceType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/scales/StarScale;->scale:Lcom/starmicronics/starmgsio/Scale;

    if-eqz v0, :cond_0

    return-void

    .line 40
    :cond_0
    new-instance v0, Lcom/starmicronics/starmgsio/StarDeviceManager;

    iget-object v1, p0, Lcom/squareup/scales/StarScale;->application:Landroid/app/Application;

    check-cast v1, Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/starmicronics/starmgsio/StarDeviceManager;-><init>(Landroid/content/Context;)V

    .line 43
    sget-object v1, Lcom/squareup/scales/StarScale$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x1

    const-string v2, "ConnectionInfo.Builder()\u2026ier)\n            .build()"

    if-eq p2, v1, :cond_2

    const/4 v1, 0x2

    if-eq p2, v1, :cond_1

    .line 51
    new-instance p2, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    invoke-direct {p2}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;-><init>()V

    .line 52
    invoke-virtual {p2, p1}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->setBleInfo(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->build()Lcom/starmicronics/starmgsio/ConnectionInfo;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_1
    new-instance p2, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    invoke-direct {p2}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;-><init>()V

    .line 48
    invoke-virtual {p2, p1}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->setUsbInfo(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    move-result-object p1

    const/16 p2, 0x4b0

    .line 49
    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->setBaudRate(I)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->build()Lcom/starmicronics/starmgsio/ConnectionInfo;

    move-result-object p1

    const-string p2, "ConnectionInfo.Builder()\u2026200)\n            .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_2
    new-instance p2, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    invoke-direct {p2}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;-><init>()V

    .line 45
    invoke-virtual {p2, p1}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->setBleInfo(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->build()Lcom/starmicronics/starmgsio/ConnectionInfo;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    :goto_0
    invoke-virtual {v0, p1}, Lcom/starmicronics/starmgsio/StarDeviceManager;->createScale(Lcom/starmicronics/starmgsio/ConnectionInfo;)Lcom/starmicronics/starmgsio/Scale;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 58
    iget-object p2, p0, Lcom/squareup/scales/StarScale;->scaleCallback:Lcom/squareup/scales/StarScale$scaleCallback$1;

    check-cast p2, Lcom/starmicronics/starmgsio/ScaleCallback;

    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/Scale;->connect(Lcom/starmicronics/starmgsio/ScaleCallback;)V

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    :goto_1
    iput-object p1, p0, Lcom/squareup/scales/StarScale;->scale:Lcom/starmicronics/starmgsio/Scale;

    return-void
.end method
