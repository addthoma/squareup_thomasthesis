.class public Lcom/squareup/settings/server/OnboardingSettings;
.super Ljava/lang/Object;
.source "OnboardingSettings.java"


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/settings/server/OnboardingSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 19
    iput-object p2, p0, Lcom/squareup/settings/server/OnboardingSettings;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public acceptsCards()Z
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->payment_cards:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public hasInAppBankLinking()Z
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_IN_APP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isEligibleForSquareCardPayments()Z
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->eligible_for_square_card_processing:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public shouldActivateOnTheWeb()Z
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->activation_url:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/settings/server/UserSettings;

    iget-object v1, p0, Lcom/squareup/settings/server/OnboardingSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-direct {v0, v1}, Lcom/squareup/settings/server/UserSettings;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 62
    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public showBankLinkingAfterActivation()Z
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINK_POST_SIGNUP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_BANK:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public showInAppActivationPostSignup()Z
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions;->can_onboard:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public showReferralAfterActivation()Z
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/server/OnboardingSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_REFERRAL:Lcom/squareup/settings/server/Features$Feature;

    .line 29
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
