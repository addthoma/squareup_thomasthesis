.class public Lcom/squareup/settings/server/MerchantRegisterSettings;
.super Ljava/lang/Object;
.source "MerchantRegisterSettings.java"


# instance fields
.field private final statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;


# direct methods
.method constructor <init>(Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/settings/server/MerchantRegisterSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-void
.end method


# virtual methods
.method public getGuestPermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/settings/server/MerchantRegisterSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    return-object v0

    .line 29
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLegacyGuestModeExpiration()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/settings/server/MerchantRegisterSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTeamPasscode()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/settings/server/MerchantRegisterSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTeamRoleToken()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/settings/server/MerchantRegisterSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public showShareTeamPasscodeWarning()Z
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/settings/server/MerchantRegisterSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_0

    .line 62
    iget-object v1, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 64
    iget-object v0, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public useTeamPermissions()Z
    .locals 2

    .line 15
    iget-object v0, p0, Lcom/squareup/settings/server/MerchantRegisterSettings;->statusResponse:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    if-eqz v0, :cond_0

    .line 17
    iget-object v1, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 18
    iget-object v0, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
