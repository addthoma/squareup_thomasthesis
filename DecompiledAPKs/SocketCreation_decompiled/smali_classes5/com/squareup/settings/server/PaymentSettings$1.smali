.class synthetic Lcom/squareup/settings/server/PaymentSettings$1;
.super Ljava/lang/Object;
.source "PaymentSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/PaymentSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$server$account$protos$FlagsAndPermissions$RoundingType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 184
    invoke-static {}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->values()[Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/settings/server/PaymentSettings$1;->$SwitchMap$com$squareup$server$account$protos$FlagsAndPermissions$RoundingType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/settings/server/PaymentSettings$1;->$SwitchMap$com$squareup$server$account$protos$FlagsAndPermissions$RoundingType:[I

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->BANKERS:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/settings/server/PaymentSettings$1;->$SwitchMap$com$squareup$server$account$protos$FlagsAndPermissions$RoundingType:[I

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->TRUNCATE:Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RoundingType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
