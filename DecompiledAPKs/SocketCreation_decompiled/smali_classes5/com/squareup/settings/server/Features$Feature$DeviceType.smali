.class public enum Lcom/squareup/settings/server/Features$Feature$DeviceType;
.super Ljava/lang/Enum;
.source "Features.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/Features$Feature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4009
    name = "DeviceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/settings/server/Features$Feature$DeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/settings/server/Features$Feature$DeviceType;

.field public static final enum BOTH:Lcom/squareup/settings/server/Features$Feature$DeviceType;

.field public static final enum MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

.field public static final enum TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 759
    new-instance v0, Lcom/squareup/settings/server/Features$Feature$DeviceType$1;

    const/4 v1, 0x0

    const-string v2, "TABLET_ONLY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/settings/server/Features$Feature$DeviceType$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    .line 764
    new-instance v0, Lcom/squareup/settings/server/Features$Feature$DeviceType$2;

    const/4 v2, 0x1

    const-string v3, "MOBILE_ONLY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/settings/server/Features$Feature$DeviceType$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    .line 769
    new-instance v0, Lcom/squareup/settings/server/Features$Feature$DeviceType;

    const/4 v3, 0x2

    const-string v4, "BOTH"

    invoke-direct {v0, v4, v3}, Lcom/squareup/settings/server/Features$Feature$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/settings/server/Features$Feature$DeviceType;->BOTH:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/settings/server/Features$Feature$DeviceType;

    .line 758
    sget-object v4, Lcom/squareup/settings/server/Features$Feature$DeviceType;->TABLET_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/settings/server/Features$Feature$DeviceType;->MOBILE_ONLY:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/Features$Feature$DeviceType;->BOTH:Lcom/squareup/settings/server/Features$Feature$DeviceType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/settings/server/Features$Feature$DeviceType;->$VALUES:[Lcom/squareup/settings/server/Features$Feature$DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 758
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/squareup/settings/server/Features$1;)V
    .locals 0

    .line 758
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/server/Features$Feature$DeviceType;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/settings/server/Features$Feature$DeviceType;
    .locals 1

    .line 758
    const-class v0, Lcom/squareup/settings/server/Features$Feature$DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/server/Features$Feature$DeviceType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/settings/server/Features$Feature$DeviceType;
    .locals 1

    .line 758
    sget-object v0, Lcom/squareup/settings/server/Features$Feature$DeviceType;->$VALUES:[Lcom/squareup/settings/server/Features$Feature$DeviceType;

    invoke-virtual {v0}, [Lcom/squareup/settings/server/Features$Feature$DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/settings/server/Features$Feature$DeviceType;

    return-object v0
.end method


# virtual methods
.method public applies(Z)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
