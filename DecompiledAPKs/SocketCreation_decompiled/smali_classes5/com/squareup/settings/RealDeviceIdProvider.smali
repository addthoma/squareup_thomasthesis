.class public Lcom/squareup/settings/RealDeviceIdProvider;
.super Ljava/lang/Object;
.source "RealDeviceIdProvider.java"

# interfaces
.implements Lcom/squareup/settings/DeviceIdProvider;


# instance fields
.field private final application:Landroid/app/Application;

.field private volatile deviceId:Ljava/lang/String;

.field private final deviceIdStorage:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final systemProperties:Lcom/squareup/util/SystemProperties;

.field private final telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method constructor <init>(Landroid/app/Application;Landroid/telephony/TelephonyManager;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/SystemProperties;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Landroid/telephony/TelephonyManager;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/util/SystemProperties;",
            ")V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/settings/RealDeviceIdProvider;->application:Landroid/app/Application;

    .line 40
    iput-object p2, p0, Lcom/squareup/settings/RealDeviceIdProvider;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 41
    iput-object p3, p0, Lcom/squareup/settings/RealDeviceIdProvider;->installationIdProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceIdStorage:Lcom/squareup/settings/LocalSetting;

    .line 43
    iput-object p5, p0, Lcom/squareup/settings/RealDeviceIdProvider;->systemProperties:Lcom/squareup/util/SystemProperties;

    return-void
.end method


# virtual methods
.method public getDeviceId()Ljava/lang/String;
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 55
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->systemProperties:Lcom/squareup/util/SystemProperties;

    invoke-virtual {v0}, Lcom/squareup/util/SystemProperties;->getX2SerialNumber()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 57
    iput-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceId:Ljava/lang/String;

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->PHONE:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/settings/RealDeviceIdProvider;->application:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 64
    iput-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 72
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 73
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceIdStorage:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 76
    iput-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceId:Ljava/lang/String;

    goto :goto_1

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->installationIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 80
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    return-object v0

    :cond_3
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "blank installationId"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 87
    :cond_4
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceIdStorage:Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 90
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/squareup/settings/RealDeviceIdProvider;->deviceId:Ljava/lang/String;

    return-object v0
.end method
