.class public Lcom/squareup/settings/DeviceSettingsSettingsInitializer;
.super Ljava/lang/Object;
.source "DeviceSettingsSettingsInitializer.java"


# instance fields
.field private final deviceInitialized:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final executor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;->deviceInitialized:Lcom/squareup/settings/LocalSetting;

    .line 17
    iput-object p2, p0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;->executor:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/settings/-$$Lambda$DeviceSettingsSettingsInitializer$U53ze389t_noFL5pH_pm9UWId4Q;

    invoke-direct {v1, p0}, Lcom/squareup/settings/-$$Lambda$DeviceSettingsSettingsInitializer$U53ze389t_noFL5pH_pm9UWId4Q;-><init>(Lcom/squareup/settings/DeviceSettingsSettingsInitializer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$initialize$0$DeviceSettingsSettingsInitializer()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "[Settings] Warming up device settings"

    .line 27
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    iget-object v1, p0, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;->deviceInitialized:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Settings] Finished warming up device settings"

    .line 31
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
