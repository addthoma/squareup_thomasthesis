.class public final Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;
.super Ljava/lang/Object;
.source "UserDirectoryModule_ProvideUserDataDirectoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private final dataDirectoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;->dataDirectoryProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;->userIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUserDataDirectory(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 0

    .line 39
    invoke-static {p0, p1}, Lcom/squareup/settings/UserDirectoryModule;->provideUserDataDirectory(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/io/File;

    return-object p0
.end method


# virtual methods
.method public get()Ljava/io/File;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;->dataDirectoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;->userIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;->provideUserDataDirectory(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/settings/UserDirectoryModule_ProvideUserDataDirectoryFactory;->get()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
