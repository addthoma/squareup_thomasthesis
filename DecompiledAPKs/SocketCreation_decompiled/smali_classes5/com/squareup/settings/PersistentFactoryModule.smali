.class public abstract Lcom/squareup/settings/PersistentFactoryModule;
.super Ljava/lang/Object;
.source "PersistentFactoryModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract providePersistentFactory(Lcom/squareup/persistent/DefaultPersistentFactory;)Lcom/squareup/persistent/PersistentFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
