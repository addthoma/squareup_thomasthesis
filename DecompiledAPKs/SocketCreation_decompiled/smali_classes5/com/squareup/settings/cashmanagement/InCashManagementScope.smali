.class public abstract Lcom/squareup/settings/cashmanagement/InCashManagementScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InCashManagementScope.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0003R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/settings/cashmanagement/InCashManagementScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "cashScope",
        "Lcom/squareup/settings/cashmanagement/CashManagementScope;",
        "(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V",
        "getParentKey",
        "cash-management_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cashScope:Lcom/squareup/settings/cashmanagement/CashManagementScope;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V
    .locals 1

    const-string v0, "cashScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/settings/cashmanagement/InCashManagementScope;->cashScope:Lcom/squareup/settings/cashmanagement/CashManagementScope;

    return-void
.end method


# virtual methods
.method public final getParentKey()Lcom/squareup/settings/cashmanagement/CashManagementScope;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/settings/cashmanagement/InCashManagementScope;->cashScope:Lcom/squareup/settings/cashmanagement/CashManagementScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/settings/cashmanagement/InCashManagementScope;->getParentKey()Lcom/squareup/settings/cashmanagement/CashManagementScope;

    move-result-object v0

    return-object v0
.end method
