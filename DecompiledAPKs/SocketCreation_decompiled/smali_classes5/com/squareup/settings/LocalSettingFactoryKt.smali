.class public final Lcom/squareup/settings/LocalSettingFactoryKt;
.super Ljava/lang/Object;
.source "LocalSettingFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLocalSettingFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LocalSettingFactory.kt\ncom/squareup/settings/LocalSettingFactoryKt\n*L\n1#1,21:1\n20#1:22\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u001a8\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u0003*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u0001H\u0002H\u0086\u0008\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "create",
        "Lcom/squareup/settings/LocalSetting;",
        "T",
        "",
        "Lcom/squareup/settings/LocalSettingFactory;",
        "key",
        "",
        "defaultValue",
        "(Lcom/squareup/settings/LocalSettingFactory;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/LocalSetting;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic create(Lcom/squareup/settings/LocalSettingFactory;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/settings/LocalSettingFactory;",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/squareup/settings/LocalSetting<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$create"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    const-string v1, "T"

    .line 20
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-interface {p0, v0, p1, p2}, Lcom/squareup/settings/LocalSettingFactory;->create(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic create$default(Lcom/squareup/settings/LocalSettingFactory;Ljava/lang/String;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    const-string p3, "$this$create"

    .line 19
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "key"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p3, 0x4

    const-string p4, "T"

    .line 22
    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p3, Ljava/lang/Object;

    invoke-interface {p0, p3, p1, p2}, Lcom/squareup/settings/LocalSettingFactory;->create(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    return-object p0
.end method
