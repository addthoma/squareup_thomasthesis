.class public final Lcom/squareup/singlesignon/SingleSignOn$DefaultImpls;
.super Ljava/lang/Object;
.source "SingleSignOn.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/singlesignon/SingleSignOn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static clearSessionToken(Lcom/squareup/singlesignon/SingleSignOn;)V
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/singlesignon/ClearSessionReason$ClearCache;->INSTANCE:Lcom/squareup/singlesignon/ClearSessionReason$ClearCache;

    check-cast v0, Lcom/squareup/singlesignon/ClearSessionReason;

    invoke-interface {p0, v0}, Lcom/squareup/singlesignon/SingleSignOn;->clearSessionToken(Lcom/squareup/singlesignon/ClearSessionReason;)V

    return-void
.end method

.method public static synthetic clearSessionToken$default(Lcom/squareup/singlesignon/SingleSignOn;Lcom/squareup/singlesignon/ClearSessionReason;ILjava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 35
    sget-object p1, Lcom/squareup/singlesignon/ClearSessionReason$ClearCache;->INSTANCE:Lcom/squareup/singlesignon/ClearSessionReason$ClearCache;

    check-cast p1, Lcom/squareup/singlesignon/ClearSessionReason;

    :cond_0
    invoke-interface {p0, p1}, Lcom/squareup/singlesignon/SingleSignOn;->clearSessionToken(Lcom/squareup/singlesignon/ClearSessionReason;)V

    return-void

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: clearSessionToken"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
