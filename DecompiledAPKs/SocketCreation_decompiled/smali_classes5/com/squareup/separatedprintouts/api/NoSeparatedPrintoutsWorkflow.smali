.class public final Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "NoSeparatedPrintoutsWorkflow.kt"

# interfaces
.implements Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u00012\u00020\tB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\nJF\u0010\u000b\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u000c\u001a\u00020\u00022\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00030\u000eH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "separated-printouts_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflow;->render(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "This workflow should never be launched!"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
