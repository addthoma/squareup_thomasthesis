.class public final Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsViewFactoryFactory;
.super Ljava/lang/Object;
.source "NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsViewFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsViewFactoryFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsViewFactoryFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsViewFactoryFactory$InstanceHolder;->access$000()Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsViewFactoryFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSeparatedPrintoutsViewFactory()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;->provideSeparatedPrintoutsViewFactory()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsViewFactoryFactory;->provideSeparatedPrintoutsViewFactory()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule_ProvideSeparatedPrintoutsViewFactoryFactory;->get()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

    move-result-object v0

    return-object v0
.end method
