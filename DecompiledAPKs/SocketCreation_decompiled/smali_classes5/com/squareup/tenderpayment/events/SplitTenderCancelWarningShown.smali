.class public Lcom/squareup/tenderpayment/events/SplitTenderCancelWarningShown;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "SplitTenderCancelWarningShown.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 10
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SPLIT_TENDER_WARNING_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
