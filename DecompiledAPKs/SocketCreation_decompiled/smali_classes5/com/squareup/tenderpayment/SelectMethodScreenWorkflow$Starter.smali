.class public interface abstract Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;
.super Ljava/lang/Object;
.source "SelectMethodScreenWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Starter"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$Starter;",
        "",
        "start",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;",
        "startArgs",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract start(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;
.end method

.method public abstract start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;
.end method
