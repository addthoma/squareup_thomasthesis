.class public final Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;
.super Ljava/lang/Object;
.source "SelectMethodScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TenderViewData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000bR\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
        "",
        "enabled",
        "",
        "title",
        "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
        "action",
        "Lcom/squareup/fsm/SideEffect;",
        "creditCardOnFileTender",
        "quickCashTender",
        "giftCardsOnFileTender",
        "(ZLcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/fsm/SideEffect;ZZZ)V",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final action:Lcom/squareup/fsm/SideEffect;

.field public final creditCardOnFileTender:Z

.field public final enabled:Z

.field public final giftCardsOnFileTender:Z

.field public final quickCashTender:Z

.field public final title:Lcom/squareup/tenderpayment/SelectMethod$TextData;


# direct methods
.method public constructor <init>(ZLcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/fsm/SideEffect;ZZZ)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->enabled:Z

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->title:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->action:Lcom/squareup/fsm/SideEffect;

    iput-boolean p4, p0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->creditCardOnFileTender:Z

    iput-boolean p5, p0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->quickCashTender:Z

    iput-boolean p6, p0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->giftCardsOnFileTender:Z

    return-void
.end method
