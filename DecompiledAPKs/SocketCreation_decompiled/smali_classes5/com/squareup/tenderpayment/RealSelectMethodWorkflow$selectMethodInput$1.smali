.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;-><init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->invoke(Lcom/squareup/tenderpayment/SelectMethod$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/tenderpayment/SelectMethod$Event;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 803
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onBackPressed()V

    goto/16 :goto_0

    .line 804
    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$CashSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CashSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onCashSelected()V

    goto/16 :goto_0

    .line 805
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$CardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CardSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onCardSelected()V

    goto/16 :goto_0

    .line 806
    :cond_2
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$GiftCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$GiftCardSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onGiftCardSelected()V

    goto/16 :goto_0

    .line 807
    :cond_3
    instance-of v0, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 808
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;->getTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;->getTenderName()Ljava/lang/String;

    move-result-object p1

    .line 807
    invoke-virtual {v0, v1, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onOtherTenderSelected(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 810
    :cond_4
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$ThirdPartyCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ThirdPartyCardSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onThirdPartyCardSelected()V

    goto/16 :goto_0

    .line 811
    :cond_5
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$InvoiceSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$InvoiceSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onInvoiceSelected()V

    goto/16 :goto_0

    .line 812
    :cond_6
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onCardOnFileSelected()V

    goto/16 :goto_0

    .line 813
    :cond_7
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$AddGiftCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$AddGiftCardSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onAddGiftCardSelected()V

    goto/16 :goto_0

    .line 814
    :cond_8
    instance-of v0, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 815
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->getInstrumentToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->getCardNameAndNumber()Ljava/lang/String;

    move-result-object p1

    .line 814
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onConfirmChargeCardOnFile(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 817
    :cond_9
    instance-of v0, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$QuickCashTenderReceived;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 818
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$QuickCashTenderReceived;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$QuickCashTenderReceived;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 817
    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onQuickCashTenderReceived(Lcom/squareup/protos/common/Money;)V

    goto :goto_0

    .line 820
    :cond_a
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$SecondaryTendersSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$SecondaryTendersSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onSecondaryTendersSelected()V

    goto :goto_0

    .line 821
    :cond_b
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$SplitTender;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$SplitTender;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onSplitTender()V

    goto :goto_0

    .line 822
    :cond_c
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$ReenableContactlessClicked;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ReenableContactlessClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onReenableContactlessClicked()V

    goto :goto_0

    .line 823
    :cond_d
    instance-of v0, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$RecordFullyCompedPayment;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    .line 824
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$RecordFullyCompedPayment;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$RecordFullyCompedPayment;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 823
    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onRecordFullyCompedPayment(Lcom/squareup/protos/common/Money;)V

    goto :goto_0

    .line 826
    :cond_e
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$ContactlessSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ContactlessSelected;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onContactlessSelected()V

    goto :goto_0

    .line 827
    :cond_f
    instance-of v0, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$TenderOptionSelection;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$TenderOptionSelection;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$TenderOptionSelection;->getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onTenderOptionSelected(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V

    :cond_10
    :goto_0
    return-void
.end method
