.class public Lcom/squareup/tenderpayment/CashOptionsView;
.super Landroid/widget/LinearLayout;
.source "CashOptionsView.java"


# instance fields
.field private final buttonHorizontalPadding:I

.field private final buttonLeftMargin:I

.field private cashOptionCustom:Landroid/widget/TextView;

.field private final onCashAmountSelected:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final onCustomCashOptionClicked:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final textSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, v0}, Lcom/squareup/tenderpayment/CashOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/tenderpayment/CashOptionsView;->onCashAmountSelected:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 57
    sget p2, Lcom/squareup/tenderworkflow/R$layout;->payment_cash_options_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/tenderpayment/CashOptionsView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 58
    invoke-direct {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->bindViews()V

    .line 60
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/noho/R$dimen;->noho_text_size_body:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->textSize:F

    .line 62
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/noho/R$dimen;->noho_row_gap_size:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonLeftMargin:I

    .line 63
    iget p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonLeftMargin:I

    iput p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonHorizontalPadding:I

    .line 64
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p2, -0x2

    const/4 v0, -0x1

    invoke-direct {p1, p2, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 65
    iget p2, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonLeftMargin:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 66
    iget-object p2, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    iget p2, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonHorizontalPadding:I

    invoke-virtual {p1, p2, v0, v0, v0}, Landroid/widget/TextView;->setPaddingRelative(IIII)V

    .line 68
    iget-object p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    iget p2, p0, Lcom/squareup/tenderpayment/CashOptionsView;->textSize:F

    invoke-virtual {p1, v0, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 70
    iget-object p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->onCustomCashOptionClicked:Lio/reactivex/Observable;

    return-void
.end method

.method private addCashOption(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;Z)V"
        }
    .end annotation

    .line 149
    new-instance v0, Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x11

    .line 150
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 151
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$style;->TextAppearance_Marin_Medium_Blue:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 153
    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_clear_ultra_light_gray_pressed:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    .line 155
    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 157
    iget p2, p0, Lcom/squareup/tenderpayment/CashOptionsView;->textSize:F

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    .line 158
    new-instance p2, Lcom/squareup/tenderpayment/-$$Lambda$CashOptionsView$15NXrupAIH2ixYn5QHKVIdE61Z0;

    invoke-direct {p2, p0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$CashOptionsView$15NXrupAIH2ixYn5QHKVIdE61Z0;-><init>(Lcom/squareup/tenderpayment/CashOptionsView;Lcom/squareup/protos/common/Money;)V

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p3, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 161
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    .line 164
    :cond_0
    iget p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonHorizontalPadding:I

    invoke-virtual {v0, p1, v1, p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setPaddingRelative(IIII)V

    .line 165
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p2, -0x2

    const/4 p3, -0x1

    invoke-direct {p1, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 167
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getChildCount()I

    move-result p2

    const/4 p3, 0x1

    if-lt p2, p3, :cond_1

    .line 171
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getChildCount()I

    move-result p2

    sub-int/2addr p2, p3

    .line 172
    iget p3, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonLeftMargin:I

    invoke-virtual {p1, p3, v1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 174
    invoke-virtual {p0, v0, p2, p1}, Lcom/squareup/tenderpayment/CashOptionsView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    .line 168
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-class p3, Lcom/squareup/tenderpayment/CashOptionsView;

    .line 169
    invoke-virtual {p3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " should always have at least 1 child view."

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private bindViews()V
    .locals 1

    .line 178
    sget v0, Lcom/squareup/tenderworkflow/R$id;->cash_option_custom:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    return-void
.end method

.method private clearCashOptions()V
    .locals 2

    .line 141
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/squareup/tenderpayment/CashOptionsView;->removeViews(II)V

    return-void
.end method

.method private removeCashOptionsToFit(ILjava/util/List;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 117
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    float-to-int v1, v1

    .line 119
    iget v2, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonHorizontalPadding:I

    mul-int/lit8 v4, v2, 0x2

    sub-int/2addr p1, v1

    sub-int/2addr p1, v2

    .line 122
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 124
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/common/Money;

    .line 126
    invoke-interface {p3, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v5, v5

    .line 125
    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_1

    :cond_0
    add-int/2addr v2, v4

    .line 129
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 130
    iget v5, p0, Lcom/squareup/tenderpayment/CashOptionsView;->buttonLeftMargin:I

    add-int/2addr v2, v5

    mul-int v1, v1, v2

    if-ge v1, p1, :cond_1

    return-void

    .line 135
    :cond_1
    invoke-static {p4, p2}, Lcom/squareup/money/QuickCashCalculator;->removeQuickCashOption(Lcom/squareup/protos/common/Money;Ljava/util/List;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private updateQuickCashOptions(ILjava/util/List;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter;",
            "Lcom/squareup/protos/common/Money;",
            "Z)V"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 104
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/squareup/tenderpayment/CashOptionsView;->removeCashOptionsToFit(ILjava/util/List;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    .line 106
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/common/Money;

    .line 107
    invoke-direct {p0, p2, p3, p5}, Lcom/squareup/tenderpayment/CashOptionsView;->addCashOption(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$addCashOption$0$CashOptionsView(Lcom/squareup/protos/common/Money;Landroid/view/View;)V
    .locals 0

    .line 158
    iget-object p2, p0, Lcom/squareup/tenderpayment/CashOptionsView;->onCashAmountSelected:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onCashAmountSelected()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/tenderpayment/CashOptionsView;->onCashAmountSelected:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public onCustomCashOptionClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/tenderpayment/CashOptionsView;->onCustomCashOptionClicked:Lio/reactivex/Observable;

    return-object v0
.end method

.method public setCashOptions(ILjava/util/List;Lcom/squareup/text/Formatter;ZLcom/squareup/protos/common/Money;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;Z",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->clearCashOptions()V

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move v5, p4

    .line 84
    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/CashOptionsView;->updateQuickCashOptions(ILjava/util/List;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;Z)V

    if-eqz p4, :cond_0

    .line 87
    iget-object p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->getContext()Landroid/content/Context;

    move-result-object p2

    sget p3, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-static {p2, p3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    .line 87
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 89
    iget-object p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 92
    :cond_0
    invoke-static {p5}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 93
    iget-object p1, p0, Lcom/squareup/tenderpayment/CashOptionsView;->cashOptionCustom:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    return-void
.end method
