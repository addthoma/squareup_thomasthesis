.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SeparateTenderAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;,
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;,
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;,
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderHeaderViewHolder;,
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;,
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;,
        Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSeparateTenderAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SeparateTenderAdapter.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderAdapter\n*L\n1#1,462:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0008\u0008\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0007&\'()*+,BI\u0012\u001c\u0010\u0003\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u00080\u0004\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0017H\u0016J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0017H\u0016J\u0018\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0017H\u0016J\u0010\u0010!\u001a\u00020\u001b2\u0006\u0010\"\u001a\u00020\u0002H\u0016J\u0010\u0010#\u001a\u00020\u001b2\u0006\u0010\"\u001a\u00020\u0002H\u0016J\u0014\u0010$\u001a\u00020\u001b2\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150%R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0003\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u00080\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "moneyLocaleHelper",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)V",
        "tenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "getItemCount",
        "",
        "getItemViewType",
        "position",
        "onBindViewHolder",
        "",
        "viewHolder",
        "onCreateViewHolder",
        "viewGroup",
        "Landroid/view/ViewGroup;",
        "itemViewType",
        "onViewAttachedToWindow",
        "holder",
        "onViewDetachedFromWindow",
        "setTenders",
        "",
        "BindableScreenData",
        "CompletedTenderHeaderViewHolder",
        "CompletedTenderViewHolder",
        "EditTenderRowViewHolder",
        "EvenSplitsTenderRowViewHolder",
        "HelpBannerRowViewHolder",
        "ViewHolder",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iput-object p4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 62
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->tenders:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getMoneyLocaleHelper$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/money/MoneyLocaleHelper;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;)Lcom/squareup/util/Res;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public getItemViewType(I)I
    .locals 3

    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz p1, :cond_2

    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_0

    if-eq p1, v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :cond_3
    :goto_0
    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->onBindViewHolder(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;I)V
    .locals 2

    const-string/jumbo v0, "viewHolder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;

    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->screens:Lio/reactivex/Observable;

    invoke-interface {p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$BindableScreenData;->bind(Lio/reactivex/Observable;)V

    goto :goto_0

    .line 98
    :cond_0
    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;

    if-eqz v0, :cond_1

    add-int/lit8 p2, p2, -0x4

    .line 102
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;

    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->screens:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->tenders:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/payment/tender/BaseTender;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;->bind(Lio/reactivex/Observable;Lcom/squareup/payment/tender/BaseTender;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 55
    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;
    .locals 3

    const-string/jumbo v0, "viewGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p2, :cond_4

    const/4 v2, 0x1

    if-eq p2, v2, :cond_3

    const/4 v2, 0x2

    if-eq p2, v2, :cond_2

    const/4 v2, 0x3

    if-eq p2, v2, :cond_1

    const/4 v2, 0x4

    if-ne p2, v2, :cond_0

    .line 83
    sget p2, Lcom/squareup/tenderworkflow/R$layout;->split_tender_completed_payment_row:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 84
    new-instance p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;

    move-object v0, p0

    check-cast v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-direct {p2, v0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderViewHolder;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    goto :goto_0

    .line 86
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "View holder of type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " is not supported."

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 86
    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 80
    :cond_1
    sget p2, Lcom/squareup/tenderworkflow/R$layout;->split_tender_completed_payments_header:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 81
    new-instance p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderHeaderViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$CompletedTenderHeaderViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    goto :goto_0

    .line 77
    :cond_2
    sget p2, Lcom/squareup/tenderworkflow/R$layout;->split_tender_even_split_row:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 78
    new-instance p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;

    move-object v0, p0

    check-cast v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-direct {p2, v0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EvenSplitsTenderRowViewHolder;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    goto :goto_0

    .line 74
    :cond_3
    sget p2, Lcom/squareup/tenderworkflow/R$layout;->split_tender_custom_amount_row:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 75
    new-instance p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    move-object v0, p0

    check-cast v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-direct {p2, v0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    goto :goto_0

    .line 71
    :cond_4
    sget p2, Lcom/squareup/tenderworkflow/R$layout;->split_tender_help_banner_row:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 72
    new-instance p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;

    move-object v0, p0

    check-cast v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;

    invoke-direct {p2, v0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    :goto_0
    return-object p2
.end method

.method public bridge synthetic onViewAttachedToWindow(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->onViewAttachedToWindow(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;)V

    return-void
.end method

.method public onViewAttachedToWindow(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    move-object v0, p1

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    invoke-super {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->onViewAttachedToWindow(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    .line 109
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;->onAttach()V

    return-void
.end method

.method public bridge synthetic onViewDetachedFromWindow(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->onViewDetachedFromWindow(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;)V

    return-void
.end method

.method public onViewDetachedFromWindow(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    move-object v0, p1

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    invoke-super {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->onViewDetachedFromWindow(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    .line 114
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$ViewHolder;->onDetach()V

    return-void
.end method

.method public final setTenders(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "tenders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    new-instance v0, Lcom/squareup/tenderpayment/BaseTenderDiff;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->tenders:Ljava/util/List;

    invoke-direct {v0, v1, p1}, Lcom/squareup/tenderpayment/BaseTenderDiff;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 141
    check-cast v0, Landroidx/recyclerview/widget/DiffUtil$Callback;

    invoke-static {v0}, Landroidx/recyclerview/widget/DiffUtil;->calculateDiff(Landroidx/recyclerview/widget/DiffUtil$Callback;)Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    move-result-object v0

    const-string v1, "DiffUtil.calculateDiff(diffCallback)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    new-instance v1, Ljava/util/ArrayList;

    move-object v2, p1

    check-cast v2, Ljava/util/Collection;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast v1, Ljava/util/List;

    iput-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;->tenders:Ljava/util/List;

    .line 144
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$setTenders$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter;Ljava/util/List;)V

    check-cast v1, Landroidx/recyclerview/widget/ListUpdateCallback;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroidx/recyclerview/widget/ListUpdateCallback;)V

    return-void
.end method
