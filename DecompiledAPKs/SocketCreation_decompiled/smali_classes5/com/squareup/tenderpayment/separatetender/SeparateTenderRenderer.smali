.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderRenderer;
.super Ljava/lang/Object;
.source "SeparateTenderRenderer.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Renderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Renderer<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\tJH\u0010\n\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u000b\u001a\u00020\u00022\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderRenderer;",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderRenderer;->render(Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getScreen()Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    move-result-object p3

    .line 30
    instance-of v0, p3, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;

    if-eqz v0, :cond_0

    sget-object p3, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$Splitting;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$Splitting;

    check-cast p3, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    :goto_0
    move-object v1, p3

    goto :goto_1

    .line 31
    :cond_0
    instance-of v0, p3, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomEvenSplit;

    if-eqz v0, :cond_1

    sget-object p3, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$EvenSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$EvenSplit;

    check-cast p3, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    goto :goto_0

    .line 32
    :cond_1
    instance-of p3, p3, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$ConfirmCancelSeparate;

    if-eqz p3, :cond_6

    sget-object p3, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$ConfirmingSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$ConfirmingSplit;

    check-cast p3, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    goto :goto_0

    .line 35
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object p3

    iget-object p3, p3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long p3, v2, v4

    if-lez p3, :cond_2

    const/4 p3, 0x1

    const/4 v8, 0x1

    goto :goto_2

    :cond_2
    const/4 p3, 0x0

    const/4 v8, 0x0

    .line 38
    :goto_2
    instance-of p3, v1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$Splitting;

    if-eqz p3, :cond_3

    .line 39
    new-instance p3, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    .line 41
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 42
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 43
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getBillAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 44
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplitAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 45
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplits()J

    move-result-wide v6

    .line 47
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getCompletedTenders()Ljava/util/List;

    move-result-object v9

    move-object v0, p3

    .line 39
    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V

    .line 49
    invoke-static {p3, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreenKt;->createSeparateTenderScreen(Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 51
    :cond_3
    instance-of p3, v1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$EvenSplit;

    if-eqz p3, :cond_4

    .line 52
    new-instance p3, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    .line 54
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 55
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 56
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getBillAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 57
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplitAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 58
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplits()J

    move-result-wide v6

    .line 60
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getCompletedTenders()Ljava/util/List;

    move-result-object v9

    move-object v0, p3

    .line 52
    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V

    .line 62
    invoke-static {p3, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreenKt;->createSeparateTenderCustomEvenSplitScreen(Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 64
    :cond_4
    instance-of p1, v1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$ConfirmingSplit;

    if-eqz p1, :cond_5

    .line 65
    invoke-static {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreenKt;->createSeparateTenderConfirmCancelScreen(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 68
    :cond_5
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unknown state "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 32
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
