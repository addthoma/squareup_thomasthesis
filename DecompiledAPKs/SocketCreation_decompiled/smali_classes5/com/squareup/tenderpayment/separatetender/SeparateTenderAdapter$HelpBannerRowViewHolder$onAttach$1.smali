.class final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder$onAttach$1;
.super Ljava/lang/Object;
.source "SeparateTenderAdapter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->onAttach()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000120\u0010\u0002\u001a,\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\u0004\u0018\u0001`\u00060\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;)V"
        }
    .end annotation

    .line 216
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder$onAttach$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;

    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->getBillAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->getAmountRemainingAfterAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;->access$updateAmountSubtitle(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 205
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$HelpBannerRowViewHolder$onAttach$1;->accept(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method
