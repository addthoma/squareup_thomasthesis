.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;
.super Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;
.source "SeparateTenderResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EvenSplitSelected"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000f\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u001a\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "cancelledTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "numberOfSplits",
        "",
        "(Ljava/util/List;J)V",
        "getCancelledTenders",
        "()Ljava/util/List;",
        "getNumberOfSplits",
        "()J",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cancelledTenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final numberOfSplits:J


# direct methods
.method public constructor <init>(Ljava/util/List;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;J)V"
        }
    .end annotation

    const-string v0, "cancelledTenders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->cancelledTenders:Ljava/util/List;

    iput-wide p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->numberOfSplits:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;Ljava/util/List;JILjava/lang/Object;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->getCancelledTenders()Ljava/util/List;

    move-result-object p1

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget-wide p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->numberOfSplits:J

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->copy(Ljava/util/List;J)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->getCancelledTenders()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->numberOfSplits:J

    return-wide v0
.end method

.method public final copy(Ljava/util/List;J)Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;J)",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;"
        }
    .end annotation

    const-string v0, "cancelledTenders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;-><init>(Ljava/util/List;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->getCancelledTenders()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->getCancelledTenders()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->numberOfSplits:J

    iget-wide v2, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->numberOfSplits:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getCancelledTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->cancelledTenders:Ljava/util/List;

    return-object v0
.end method

.method public final getNumberOfSplits()J
    .locals 2

    .line 20
    iget-wide v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->numberOfSplits:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->getCancelledTenders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->numberOfSplits:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EvenSplitSelected(cancelledTenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->getCancelledTenders()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numberOfSplits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult$EvenSplitSelected;->numberOfSplits:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
