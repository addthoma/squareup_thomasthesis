.class public final Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SeparateCustomEvenCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSeparateCustomEvenCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SeparateCustomEvenCoordinator.kt\ncom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,150:1\n1103#2,7:151\n*E\n*S KotlinDebug\n*F\n+ 1 SeparateCustomEvenCoordinator.kt\ncom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator\n*L\n93#1,7:151\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0001,B9\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0015H\u0016J\u0016\u0010#\u001a\u00020!2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00060%H\u0002J\u0008\u0010&\u001a\u00020\'H\u0002J\u0008\u0010(\u001a\u00020!H\u0002J\u0018\u0010)\u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00152\u0006\u0010*\u001a\u00020\u0005H\u0002J\u001e\u0010+\u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00152\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00060%H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\'\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001f\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "amountEntered",
        "",
        "getAmountEntered",
        "()J",
        "continueButton",
        "Landroid/view/View;",
        "editText",
        "Lcom/squareup/noho/NohoEditText;",
        "getMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "quantityScrubber",
        "Lcom/squareup/text/QuantityScrubber;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getScreens",
        "()Lio/reactivex/Observable;",
        "attach",
        "",
        "view",
        "completeSplitTender",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "isValidAmountEntered",
        "",
        "setupEditText",
        "updateData",
        "data",
        "updateWorkflow",
        "Factory",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private continueButton:Landroid/view/View;

.field private editText:Lcom/squareup/noho/NohoEditText;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final quantityScrubber:Lcom/squareup/text/QuantityScrubber;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->res:Lcom/squareup/util/Res;

    .line 39
    new-instance p1, Lcom/squareup/text/QuantityScrubber;

    const-wide/16 p2, 0x1

    invoke-direct {p1, p2, p3}, Lcom/squareup/text/QuantityScrubber;-><init>(J)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->quantityScrubber:Lcom/squareup/text/QuantityScrubber;

    return-void
.end method

.method public static final synthetic access$completeSplitTender(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->completeSplitTender(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$getContinueButton$p(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)Landroid/view/View;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->continueButton:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "continueButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getEditText$p(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)Lcom/squareup/noho/NohoEditText;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->editText:Lcom/squareup/noho/NohoEditText;

    if-nez p0, :cond_0

    const-string v0, "editText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$isValidAmountEntered(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)Z
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->isValidAmountEntered()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setContinueButton$p(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Landroid/view/View;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->continueButton:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$setEditText$p(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Lcom/squareup/noho/NohoEditText;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->editText:Lcom/squareup/noho/NohoEditText;

    return-void
.end method

.method public static final synthetic access$updateData(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Landroid/view/View;Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->updateData(Landroid/view/View;Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;)V

    return-void
.end method

.method public static final synthetic access$updateWorkflow(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->updateWorkflow(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final completeSplitTender(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;)V"
        }
    .end annotation

    .line 137
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$EvenSplitSelected;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->getAmountEntered()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$EvenSplitSelected;-><init>(J)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final getAmountEntered()J
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->editText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string v1, "editText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/text/StringsKt;->toLongOrNull(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method private final isValidAmountEntered()Z
    .locals 5

    .line 133
    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->getAmountEntered()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final setupEditText()V
    .locals 5

    .line 110
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->editText:Lcom/squareup/noho/NohoEditText;

    const-string v1, "editText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 112
    :cond_0
    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/tenderworkflow/R$string;->split_tender_amount_done:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x6

    .line 111
    invoke-virtual {v0, v2, v3}, Lcom/squareup/noho/NohoEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 114
    new-instance v2, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v3, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->quantityScrubber:Lcom/squareup/text/QuantityScrubber;

    check-cast v3, Lcom/squareup/text/Scrubber;

    iget-object v4, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->editText:Lcom/squareup/noho/NohoEditText;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v4, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v2, v3, v4}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 115
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 120
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$2;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$$inlined$with$lambda$2;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;)V

    check-cast v1, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 126
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$1$3;

    invoke-direct {v1, v0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$setupEditText$1$3;-><init>(Lcom/squareup/noho/NohoEditText;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private final updateData(Landroid/view/View;Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;)V
    .locals 3

    .line 67
    sget v0, Lcom/squareup/tenderworkflow/R$string;->split_tender_equal_split_title:I

    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 68
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->getBillAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string/jumbo v1, "total"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 73
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 72
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->quantityScrubber:Lcom/squareup/text/QuantityScrubber;

    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;->getMaxSplits()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/text/QuantityScrubber;->setMaxValue(J)V

    return-void
.end method

.method private final updateWorkflow(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;)V"
        }
    .end annotation

    .line 81
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$1;

    invoke-direct {v0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v0, "actionBar"

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 87
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$2;

    invoke-direct {v1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->continueButton:Landroid/view/View;

    if-nez p1, :cond_2

    const-string v0, "continueButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 151
    :cond_2
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->editText:Lcom/squareup/noho/NohoEditText;

    if-nez p1, :cond_3

    const-string v0, "editText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$updateWorkflow$4;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    .line 45
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "(view.findById(\n        \u2026 ActionBarView).presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 48
    sget v0, Lcom/squareup/tenderworkflow/R$id;->split_tender_custom_split:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->editText:Lcom/squareup/noho/NohoEditText;

    .line 49
    sget v0, Lcom/squareup/tenderworkflow/R$id;->split_tender_confirm_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->continueButton:Landroid/view/View;

    .line 51
    invoke-direct {p0}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->setupEditText()V

    .line 53
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$attach$1;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$attach$2;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.map { it.data }\n\u2026 updateData(view, data) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$attach$3;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$attach$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$attach$4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$attach$4;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.map { it.workflo\u2026orkflow(view, workflow) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void

    .line 45
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.marin.widgets.ActionBarView"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public final getScreens()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent;",
            ">;>;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator;->screens:Lio/reactivex/Observable;

    return-object v0
.end method
