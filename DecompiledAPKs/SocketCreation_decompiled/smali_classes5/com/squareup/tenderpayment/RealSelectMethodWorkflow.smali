.class public final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$Starter;,
        Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;,
        Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;,
        Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;,
        Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSelectMethodWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSelectMethodWorkflow.kt\ncom/squareup/tenderpayment/RealSelectMethodWorkflow\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,1643:1\n180#2:1644\n37#3,7:1645\n250#4,2:1652\n*E\n*S KotlinDebug\n*F\n+ 1 RealSelectMethodWorkflow.kt\ncom/squareup/tenderpayment/RealSelectMethodWorkflow\n*L\n663#1:1644\n663#1,7:1645\n1556#1,2:1652\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ac\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0010\u0018\u00002\u00020\u0001:\n\u008b\u0002\u008c\u0002\u008d\u0002\u008e\u0002\u008f\u0002B\u0087\u0002\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020+\u0012\u0006\u0010,\u001a\u00020-\u0012\u0008\u0008\u0001\u0010.\u001a\u00020/\u0012\u0006\u00100\u001a\u000201\u0012\u0014\u0008\u0001\u00102\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002050403\u0012\u0006\u00106\u001a\u000207\u0012\u0006\u00108\u001a\u000209\u0012\u0006\u0010:\u001a\u00020;\u0012\u0006\u0010<\u001a\u00020=\u0012\u0006\u0010>\u001a\u00020?\u00a2\u0006\u0002\u0010@J\t\u0010\u008c\u0001\u001a\u00020`H\u0016J\t\u0010\u008d\u0001\u001a\u00020`H\u0002J\t\u0010\u008e\u0001\u001a\u00020`H\u0002J\t\u0010\u008f\u0001\u001a\u00020`H\u0002J\u001b\u0010\u0090\u0001\u001a\u00020`2\u0007\u0010\u0091\u0001\u001a\u00020V2\u0007\u0010\u0092\u0001\u001a\u000205H\u0007J\t\u0010\u0093\u0001\u001a\u00020`H\u0002J\u0017\u0010\u0094\u0001\u001a\u0010\u0012\u000c\u0012\n\u0012\u0005\u0012\u00030\u0096\u00010\u0095\u00010sH\u0002J\t\u0010\u0097\u0001\u001a\u00020`H\u0002JF\u0010\u0098\u0001\u001a*\u0012&\u0012$\u0012\u0004\u0012\u00020|\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030}j\u0002`~0{j\u0008\u0012\u0004\u0012\u00020|`\u007f0s2\u0013\u0010\u0099\u0001\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Sj\u0002`TH\u0002J\t\u0010\u009a\u0001\u001a\u00020`H\u0002J\t\u0010\u009b\u0001\u001a\u00020`H\u0007J\t\u0010\u009c\u0001\u001a\u00020`H\u0002J\t\u0010\u009d\u0001\u001a\u00020`H\u0002J\u0013\u0010\u009e\u0001\u001a\u00020`2\u0008\u0010\u009f\u0001\u001a\u00030\u00a0\u0001H\u0002J\u0018\u0010\u00a1\u0001\u001a\u00020B2\r\u0010\u00a2\u0001\u001a\u0008\u0012\u0004\u0012\u00020B0FH\u0002J\u0014\u0010\u00a3\u0001\u001a\u00030\u00a4\u00012\u0008\u0010\u00a5\u0001\u001a\u00030\u00a6\u0001H\u0002J\t\u0010\u00a7\u0001\u001a\u00020VH\u0002J\t\u0010\u00a8\u0001\u001a\u000205H\u0002J\t\u0010\u00a9\u0001\u001a\u00020BH\u0002J\t\u0010\u00aa\u0001\u001a\u00020`H\u0007J\t\u0010\u00ab\u0001\u001a\u00020`H\u0007J\t\u0010\u00ac\u0001\u001a\u00020`H\u0007J\t\u0010\u00ad\u0001\u001a\u00020`H\u0007J\t\u0010\u00ae\u0001\u001a\u00020`H\u0007J\u0013\u0010\u00af\u0001\u001a\u00020`2\u0008\u0010\u00b0\u0001\u001a\u00030\u00b1\u0001H\u0007J\t\u0010\u00b2\u0001\u001a\u00020`H\u0007J$\u0010\u00b3\u0001\u001a\u00020`2\u0007\u0010\u0091\u0001\u001a\u00020V2\u0007\u0010\u0092\u0001\u001a\u0002052\u0007\u0010\u00b4\u0001\u001a\u000205H\u0007J\t\u0010\u00b5\u0001\u001a\u00020`H\u0007J\t\u0010\u00b6\u0001\u001a\u00020`H\u0007J\t\u0010\u00b7\u0001\u001a\u00020`H\u0007J\t\u0010\u00b8\u0001\u001a\u00020`H\u0007J\u0013\u0010\u00b9\u0001\u001a\u00020`2\u0008\u0010\u00ba\u0001\u001a\u00030\u00bb\u0001H\u0002J\t\u0010\u00bc\u0001\u001a\u00020`H\u0007J\t\u0010\u00bd\u0001\u001a\u00020`H\u0007J\u001c\u0010\u00be\u0001\u001a\u00020`2\u0008\u0010\u00a5\u0001\u001a\u00030\u00a6\u00012\u0007\u0010\u00bf\u0001\u001a\u000205H\u0007J\u0012\u0010\u00c0\u0001\u001a\u00020`2\u0007\u0010\u0091\u0001\u001a\u00020VH\u0007J\u0012\u0010\u00c1\u0001\u001a\u00020`2\u0007\u0010\u00ba\u0001\u001a\u00020iH\u0002J\u0012\u0010\u00c2\u0001\u001a\u00020`2\u0007\u0010\u00c3\u0001\u001a\u00020VH\u0007J\t\u0010\u00c4\u0001\u001a\u00020`H\u0007J\t\u0010\u00c5\u0001\u001a\u00020`H\u0007J\t\u0010\u00c6\u0001\u001a\u00020`H\u0007J\t\u0010\u00c7\u0001\u001a\u00020`H\u0007J\u0013\u0010\u00c8\u0001\u001a\u00020`2\u0008\u0010\u00c9\u0001\u001a\u00030\u00ca\u0001H\u0007J\u0013\u0010\u00cb\u0001\u001a\u00020`2\u0008\u0010\u00ba\u0001\u001a\u00030\u00cc\u0001H\u0007J\t\u0010\u00cd\u0001\u001a\u00020`H\u0007J2\u0010\u00ce\u0001\u001a\u001a\u0012\u0004\u0012\u00020|\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030}j\u0002`~0{2\u000f\u0010\u00cf\u0001\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030}H\u0002J\t\u0010\u00d0\u0001\u001a\u00020`H\u0002J\t\u0010\u00d1\u0001\u001a\u00020`H\u0002J\u001f\u0010\u00d2\u0001\u001a\u00020B2\t\u0008\u0002\u0010\u00d3\u0001\u001a\u00020B2\t\u0008\u0002\u0010\u00d4\u0001\u001a\u00020BH\u0002J\u0012\u0010\u00d5\u0001\u001a\u00020`2\u0007\u0010\u0091\u0001\u001a\u00020VH\u0002J\u0012\u0010\u00d6\u0001\u001a\u00020`2\u0007\u0010\u0091\u0001\u001a\u00020VH\u0007J\u0012\u0010\u00d7\u0001\u001a\u00020`2\u0007\u0010j\u001a\u00030\u00d8\u0001H\u0002J\u0013\u0010\u00d9\u0001\u001a\u00020`2\u0008\u0010\u00da\u0001\u001a\u00030\u00db\u0001H\u0002J\u0013\u0010\u00dc\u0001\u001a\u00020`2\u0008\u0010\u00dd\u0001\u001a\u00030\u00de\u0001H\u0002J\u0013\u0010\u00df\u0001\u001a\u00020`2\u0008\u0010\u00dd\u0001\u001a\u00030\u00de\u0001H\u0002J\u0013\u0010\u00e0\u0001\u001a\u00020`2\u0008\u0010\u00dd\u0001\u001a\u00030\u00de\u0001H\u0002J\t\u0010\u00e1\u0001\u001a\u00020`H\u0002J\u0013\u0010\u00e2\u0001\u001a\u00020`2\u0008\u0010\u00ba\u0001\u001a\u00030\u00e3\u0001H\u0002J\t\u0010\u00e4\u0001\u001a\u00020`H\u0002J\u0013\u0010\u00e5\u0001\u001a\u00020`2\u0008\u0010\u00ba\u0001\u001a\u00030\u00e6\u0001H\u0016J\u0013\u0010\u00e7\u0001\u001a\u00020`2\u0008\u0010\u00e8\u0001\u001a\u00030\u00e9\u0001H\u0002J\t\u0010\u00ea\u0001\u001a\u00020`H\u0002J\t\u0010\u00eb\u0001\u001a\u00020BH\u0002J\t\u0010\u00ec\u0001\u001a\u00020BH\u0002J\t\u0010\u00ed\u0001\u001a\u00020BH\u0002J\u0013\u0010\u00ee\u0001\u001a\u00020`2\u0008\u0010\u00ba\u0001\u001a\u00030\u00ef\u0001H\u0002J\t\u0010\u00f0\u0001\u001a\u00020`H\u0002J\t\u0010\u00f1\u0001\u001a\u00020`H\u0002J,\u0010\u00f2\u0001\u001a\u00020B2\u0007\u0010\u00d3\u0001\u001a\u00020B2\u0007\u0010\u00d4\u0001\u001a\u00020B2\u0007\u0010\u00f3\u0001\u001a\u00020B2\u0006\u0010A\u001a\u00020BH\u0002J\t\u0010\u00f4\u0001\u001a\u00020`H\u0002J\n\u0010\u00f5\u0001\u001a\u00030\u00f6\u0001H\u0002J\n\u0010\u00f7\u0001\u001a\u00030\u00f8\u0001H\u0002J9\u0010\u00f9\u0001\u001a\u00020`2\u0008\u0010\u00fa\u0001\u001a\u00030\u0089\u00012\u0013\u0010\u00fb\u0001\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030Sj\u0002`T2\u0007\u0010\u00fc\u0001\u001a\u00020B2\u0006\u0010w\u001a\u00020xH\u0002J\u0012\u0010\u00f9\u0001\u001a\u00020`2\u0007\u0010\u00fd\u0001\u001a\u00020xH\u0002J\u0013\u0010\u00fe\u0001\u001a\u00020`2\u0008\u0010\u00ff\u0001\u001a\u00030\u0080\u0002H\u0002J5\u0010\u0081\u0002\u001a\u00030\u0080\u00022)\u0010\u0082\u0002\u001a$\u0012\u0004\u0012\u00020|\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030}j\u0002`~0{j\u0008\u0012\u0004\u0012\u00020|`\u007fH\u0002J\t\u0010\u0083\u0002\u001a\u00020`H\u0002J\u0011\u0010\u0084\u0002\u001a\u00020`2\u0006\u0010X\u001a\u00020QH\u0002J\t\u0010\u0085\u0002\u001a\u00020`H\u0002J\t\u0010\u0086\u0002\u001a\u00020`H\u0002JF\u0010\u0086\u0002\u001a\u00020t2\u0007\u0010\u0087\u0002\u001a\u00020\\2\u0007\u0010\u0088\u0002\u001a\u00020V2\u0006\u0010X\u001a\u00020Q2\u0008\u0010\u0089\u0002\u001a\u00030\u0083\u00012\u000f\u0010\u008a\u0002\u001a\n\u0012\u0005\u0012\u00030\u0096\u00010\u0095\u00012\u0006\u0010A\u001a\u00020BH\u0002R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020BX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020DX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010E\u001a\u0008\u0012\u0004\u0012\u00020G0F8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008H\u0010IR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010J\u001a&\u0012\u000c\u0012\n M*\u0004\u0018\u00010L0L M*\u0012\u0012\u000c\u0012\n M*\u0004\u0018\u00010L0L\u0018\u00010K0KX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010N\u001a&\u0012\u000c\u0012\n M*\u0004\u0018\u00010O0O M*\u0012\u0012\u000c\u0012\n M*\u0004\u0018\u00010O0O\u0018\u00010K0KX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010P\u001a\u0004\u0018\u00010QX\u0082\u000e\u00a2\u0006\u0002\n\u0000Rf\u0010R\u001aZ\u0012&\u0012$\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003 M*\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010Sj\u0004\u0018\u0001`T0Sj\u0002`T M*,\u0012&\u0012$\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003 M*\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010Sj\u0004\u0018\u0001`T0Sj\u0002`T\u0018\u00010K0KX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010U\u001a\u00020VX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010W\u001a&\u0012\u000c\u0012\n M*\u0004\u0018\u00010V0V M*\u0012\u0012\u000c\u0012\n M*\u0004\u0018\u00010V0V\u0018\u00010K0KX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010X\u001a&\u0012\u000c\u0012\n M*\u0004\u0018\u00010Q0Q M*\u0012\u0012\u000c\u0012\n M*\u0004\u0018\u00010Q0Q\u0018\u00010Y0YX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010Z\u001a\u00020DX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u000207X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010[\u001a&\u0012\u000c\u0012\n M*\u0004\u0018\u00010\\0\\ M*\u0012\u0012\u000c\u0012\n M*\u0004\u0018\u00010\\0\\\u0018\u00010K0KX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010]\u001a\u000205X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010^\u001a&\u0012\u000c\u0012\n M*\u0004\u0018\u00010`0` M*\u0012\u0012\u000c\u0012\n M*\u0004\u0018\u00010`0`\u0018\u00010_0_X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010a\u001a\u0002058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008b\u0010cR\u000e\u00100\u001a\u000201X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010d\u001a\u00020eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010f\u001a\u000e\u0012\u0004\u0012\u00020h\u0012\u0004\u0012\u00020i0gX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010j\u001a\u0008\u0012\u0004\u0012\u00020l0k8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008m\u0010nR\u000e\u0010>\u001a\u00020?X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020;X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010o\u001a\u0008\u0012\u0004\u0012\u00020q0pX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010r\u001a\u0008\u0012\u0004\u0012\u00020t0sX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020=X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u00102\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002050403X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010u\u001a\u00020BX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010v\u001a\u0004\u0018\u00010VX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010w\u001a\u00020xX\u0082.\u00a2\u0006\u0002\n\u0000RD\u0010y\u001a0\u0012,\u0012*\u0012&\u0012$\u0012\u0004\u0012\u00020|\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030}j\u0002`~0{j\u0008\u0012\u0004\u0012\u00020|`\u007f0z0s8VX\u0096\u0004\u00a2\u0006\u0008\u001a\u0006\u0008\u0080\u0001\u0010\u0081\u0001R\u000e\u00108\u001a\u000209X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R7\u0010\u0082\u0001\u001a*\u0012\u000e\u0012\u000c M*\u0005\u0018\u00010\u0083\u00010\u0083\u0001 M*\u0014\u0012\u000e\u0012\u000c M*\u0005\u0018\u00010\u0083\u00010\u0083\u0001\u0018\u00010K0KX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0084\u0001\u001a\u00030\u0085\u0001X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0086\u0001\u001a\u0004\u0018\u00010lX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0087\u0001\u001a\n\u0012\u0005\u0012\u00030\u0089\u00010\u0088\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u008a\u0001\u001a\u00030\u008b\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0090\u0002"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;",
        "cardReaderHubUtils",
        "Lcom/squareup/cardreader/CardReaderHubUtils;",
        "completer",
        "Lcom/squareup/tenderpayment/TenderCompleter;",
        "device",
        "Lcom/squareup/util/Device;",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "paymentEventHandler",
        "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "offlineModeMonitor",
        "Lcom/squareup/payment/OfflineModeMonitor;",
        "x2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "gateKeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "bus",
        "Lcom/squareup/badbus/BadBus;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "dippedCardTracker",
        "Lcom/squareup/cardreader/DippedCardTracker;",
        "changeHudToaster",
        "Lcom/squareup/tenderpayment/ChangeHudToaster;",
        "cardReaderOracle",
        "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "checkoutInformationEventLogger",
        "Lcom/squareup/log/CheckoutInformationEventLogger;",
        "touchEventMonitor",
        "Lcom/squareup/ui/TouchEventMonitor;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "mainScheduler",
        "Lrx/Scheduler;",
        "pauseAndResumeRegistrar",
        "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
        "showModalList",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "",
        "manualCardEntryScreenDataHelper",
        "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
        "swipeValidator",
        "Lcom/squareup/swipe/SwipeValidator;",
        "selectMethodBuyerCheckoutEnabled",
        "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
        "selectMethodScreensFactory",
        "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
        "screenDataRenderer",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
        "(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)V",
        "buyerCheckoutEnabled",
        "",
        "buyerCheckoutEnabledDisposable",
        "Lio/reactivex/disposables/Disposable;",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "getCompletedTenders",
        "()Ljava/util/List;",
        "confirmChargeCardOnFileScreenData",
        "Lrx/subjects/BehaviorSubject;",
        "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;",
        "kotlin.jvm.PlatformType",
        "confirmCollectCashScreenData",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
        "currentInternetState",
        "Lcom/squareup/connectivity/InternetState;",
        "currentScreenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "customCashValue",
        "Lcom/squareup/protos/common/Money;",
        "displayedAmountDue",
        "internetState",
        "Lio/reactivex/Observable;",
        "internetStateDisposable",
        "nfcEnabled",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
        "nonLoggedInEmployeeToken",
        "onWorkflowEnd",
        "Lrx/subjects/PublishSubject;",
        "",
        "orderIdentifier",
        "getOrderIdentifier",
        "()Ljava/lang/String;",
        "paymentEventSubscription",
        "Lrx/subscriptions/CompositeSubscription;",
        "readerStateEngine",
        "Lcom/squareup/fsm/FiniteStateEngine;",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;",
        "result",
        "Lrx/Single;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "getResult",
        "()Lrx/Single;",
        "selectMethodInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "selectMethodScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "showSecondaryMethods",
        "splitTenderAmount",
        "startArgs",
        "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
        "state",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "getState",
        "()Lrx/Observable;",
        "toastData",
        "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
        "touchEventSubscription",
        "Lrx/Subscription;",
        "workflowResult",
        "workflowStateEngine",
        "Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;",
        "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;",
        "x2Disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "abandon",
        "branConnected",
        "buyerSelectedPaymentMethod",
        "cancelBillPayment",
        "chargeCardOnFile",
        "tenderedAmount",
        "instrumentToken",
        "checkCancelBillPaymentPermissions",
        "connectedReaderCapabilities",
        "Ljava/util/EnumSet;",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        "createCardOnFileInstrumentTender",
        "createScreen",
        "screenKey",
        "disableReaderPayments",
        "doNotChargeCardOnFile",
        "enableReaderPayments",
        "end",
        "exitWithReaderIssue",
        "issue",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
        "falseThenTrueFilter",
        "values",
        "getOtherTenderType",
        "Lcom/squareup/server/account/protos/OtherTenderType;",
        "tender",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "getTenderAmount",
        "getUserToken",
        "hasTendered",
        "onAddGiftCardSelected",
        "onBackPressed",
        "onCardOnFileSelected",
        "onCardSelected",
        "onCashSelected",
        "onCompleteTenderResult",
        "completeTenderResult",
        "Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;",
        "onConfirmCancelSplitTenderTransaction",
        "onConfirmChargeCardOnFile",
        "cardNameAndNumber",
        "onContactlessSelected",
        "onDismissCancelSplitTenderTransaction",
        "onDismissCollectCash",
        "onDismissed",
        "onEvent",
        "event",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;",
        "onGiftCardSelected",
        "onInvoiceSelected",
        "onOtherTenderSelected",
        "tenderName",
        "onQuickCashTenderReceived",
        "onReaderEvent",
        "onRecordFullyCompedPayment",
        "amount",
        "onReenableContactlessClicked",
        "onSecondaryTendersSelected",
        "onSplitTender",
        "onSplitTenderWarningDismissed",
        "onTenderOptionSelected",
        "tenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "onTenderReceived",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;",
        "onThirdPartyCardSelected",
        "onlyCard",
        "screen",
        "payCashCustomSelected",
        "payGiftCardOnFile",
        "preAuthTipRequiredAndTipEnabled",
        "preAuthTipRequired",
        "tipEnabled",
        "proceedWithQuickCashPayment",
        "processCashPayment",
        "processContactlessPayment",
        "Lcom/squareup/ui/main/SmartPaymentResult;",
        "processEmvDip",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "processSingleTenderSwipe",
        "successfulSwipe",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "processSplitTenderSwipe",
        "processSwipe",
        "promptForPayment",
        "recordCardPayment",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;",
        "reenableReaderPayments",
        "sendEvent",
        "",
        "setToastDataForCardError",
        "failureReason",
        "Lcom/squareup/ui/main/errors/PaymentError;",
        "setupPayCardOnFileResult",
        "shouldCheckPaymentReadySmartReaderIsAvailable",
        "shouldEnableNfcField",
        "showAccidentalCashModal",
        "showConfirmChargeCardOnFileDialog",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;",
        "showConfirmSplitTenderCancel",
        "showContactlessReenabledHud",
        "showContactlessRow",
        "areContactlessReadersReadyForPayments",
        "showSplitTenderWarning",
        "splitTenderLabel",
        "Lcom/squareup/tenderpayment/SelectMethod$TextData;",
        "splitTenderState",
        "Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;",
        "start",
        "initialState",
        "initialScreenKey",
        "initialShowSecondaryValue",
        "startArg",
        "startFromSnapshot",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "takeSnapshot",
        "posContainerScreen",
        "tearDown",
        "updateInternetState",
        "updateNfcState",
        "updateSelectMethodScreen",
        "nfcState",
        "displayedAmount",
        "toast",
        "readerCapabilities",
        "NfcState",
        "ReaderEvent",
        "ReaderState",
        "Starter",
        "State",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private buyerCheckoutEnabled:Z

.field private buyerCheckoutEnabledDisposable:Lio/reactivex/disposables/Disposable;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final completer:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final confirmChargeCardOnFileScreenData:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final confirmCollectCashScreenData:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private currentInternetState:Lcom/squareup/connectivity/InternetState;

.field private final currentScreenKey:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;>;"
        }
    .end annotation
.end field

.field private final customCashValue:Lcom/squareup/protos/common/Money;

.field private final device:Lcom/squareup/util/Device;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final displayedAmountDue:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final gateKeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private internetStateDisposable:Lio/reactivex/disposables/Disposable;

.field private final mainScheduler:Lrx/Scheduler;

.field private final manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

.field private final nfcEnabled:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final nonLoggedInEmployeeToken:Ljava/lang/String;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final onWorkflowEnd:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private final paymentEventHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

.field private final paymentEventSubscription:Lrx/subscriptions/CompositeSubscription;

.field private final readerStateEngine:Lcom/squareup/fsm/FiniteStateEngine;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/fsm/FiniteStateEngine<",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final screenDataRenderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;

.field private final selectMethodBuyerCheckoutEnabled:Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;

.field private final selectMethodInput:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation
.end field

.field private selectMethodScreenData:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final showModalList:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private showSecondaryMethods:Z

.field private splitTenderAmount:Lcom/squareup/protos/common/Money;

.field private startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

.field private final swipeValidator:Lcom/squareup/swipe/SwipeValidator;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final toastData:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
            ">;"
        }
    .end annotation
.end field

.field private final touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

.field private touchEventSubscription:Lrx/Subscription;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

.field private final workflowStateEngine:Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents<",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;",
            ">;"
        }
    .end annotation
.end field

.field private final x2Disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/util/Device;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/Transaction;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/tenderpayment/ChangeHudToaster;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/tutorialv2/TutorialCore;Lrx/Scheduler;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;Lcom/squareup/swipe/SwipeValidator;Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Lcom/squareup/tenderpayment/SelectMethodScreensFactory;Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;)V
    .locals 16
    .param p23    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p25    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/tenderpayment/CollectCashModalShown;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/ui/NfcProcessor;",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/payment/tender/TenderFactory;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            "Lcom/squareup/tenderpayment/ChangeHudToaster;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/ui/TouchEventMonitor;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lrx/Scheduler;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            "Lcom/squareup/swipe/SwipeValidator;",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
            "Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v15, p16

    move-object/from16 v0, p17

    const-string v0, "cardReaderHubUtils"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completer"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nfcProcessor"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentEventHandler"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    move-object/from16 v9, p10

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "offlineModeMonitor"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "x2SellerScreenRunner"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "gateKeeper"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bus"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dippedCardTracker"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeHudToaster"

    move-object/from16 v9, p17

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderOracle"

    move-object/from16 v9, p18

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    move-object/from16 v9, p19

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutInformationEventLogger"

    move-object/from16 v9, p20

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "touchEventMonitor"

    move-object/from16 v9, p21

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialCore"

    move-object/from16 v9, p22

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    move-object/from16 v9, p23

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pauseAndResumeRegistrar"

    move-object/from16 v9, p24

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showModalList"

    move-object/from16 v9, p25

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manualCardEntryScreenDataHelper"

    move-object/from16 v9, p26

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipeValidator"

    move-object/from16 v9, p27

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectMethodBuyerCheckoutEnabled"

    move-object/from16 v9, p28

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectMethodScreensFactory"

    move-object/from16 v9, p29

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenDataRenderer"

    move-object/from16 v9, p30

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v9, p17

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    iput-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    iput-object v3, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->device:Lcom/squareup/util/Device;

    iput-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iput-object v5, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iput-object v6, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v7, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    iput-object v8, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object/from16 v1, p9

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v10, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    iput-object v11, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object v12, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->gateKeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object v13, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->bus:Lcom/squareup/badbus/BadBus;

    iput-object v14, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object v15, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    iput-object v9, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    move-object/from16 v1, p18

    move-object/from16 v2, p19

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iput-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p20

    move-object/from16 v2, p21

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    iput-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    move-object/from16 v1, p22

    move-object/from16 v2, p23

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->mainScheduler:Lrx/Scheduler;

    move-object/from16 v1, p24

    move-object/from16 v2, p25

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iput-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showModalList:Lcom/f2prateek/rx/preferences2/Preference;

    move-object/from16 v1, p26

    move-object/from16 v2, p27

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    iput-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    move-object/from16 v1, p28

    move-object/from16 v2, p29

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodBuyerCheckoutEnabled:Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;

    iput-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    move-object/from16 v1, p30

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->screenDataRenderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;

    .line 280
    invoke-interface/range {p10 .. p10}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->internetState:Lio/reactivex/Observable;

    .line 281
    new-instance v1, Lcom/squareup/protos/common/Money;

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/common/Money;->DEFAULT_CURRENCY_CODE:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v1, v2, v3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->customCashValue:Lcom/squareup/protos/common/Money;

    const-string v1, "nonLoggedInEmployeeToken"

    .line 282
    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nonLoggedInEmployeeToken:Ljava/lang/String;

    .line 289
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    .line 291
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->displayedAmountDue:Lrx/subjects/BehaviorSubject;

    .line 292
    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->DISABLED:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcEnabled:Lrx/subjects/BehaviorSubject;

    .line 294
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmChargeCardOnFileScreenData:Lrx/subjects/BehaviorSubject;

    .line 296
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmCollectCashScreenData:Lrx/subjects/BehaviorSubject;

    .line 298
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v2, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    .line 299
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onWorkflowEnd:Lrx/subjects/PublishSubject;

    .line 316
    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventSubscription:Lrx/subscriptions/CompositeSubscription;

    .line 317
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2Disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 364
    new-instance v1, Lcom/squareup/fsm/FiniteStateEngine;

    const/4 v2, 0x5

    new-array v3, v2, [Lcom/squareup/fsm/Rule;

    .line 365
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->UNINITIALIZED:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    sget-object v6, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    invoke-static {v4, v5, v6}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    .line 366
    new-instance v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$1;

    invoke-direct {v5, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v5, Lcom/squareup/fsm/Guard;

    invoke-virtual {v4, v5}, Lcom/squareup/fsm/Rule;->onlyIf(Lcom/squareup/fsm/Guard;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 368
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    new-instance v6, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$2;

    invoke-direct {v6, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$2;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v6, Lcom/squareup/fsm/SideEffect;

    invoke-static {v4, v6}, Lcom/squareup/fsm/Rule;->onEntry(Ljava/lang/Object;Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    const/4 v6, 0x1

    aput-object v4, v3, v6

    .line 372
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    sget-object v7, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_OFF:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    sget-object v8, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->OFF:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    invoke-static {v4, v7, v8}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    const/4 v7, 0x2

    aput-object v4, v3, v7

    .line 374
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->OFF:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    new-instance v8, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$3;

    invoke-direct {v8, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$3;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v8, Lcom/squareup/fsm/SideEffect;

    invoke-static {v4, v8}, Lcom/squareup/fsm/Rule;->onEntry(Ljava/lang/Object;Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    const/4 v8, 0x3

    aput-object v4, v3, v8

    .line 378
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->OFF:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    sget-object v9, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    sget-object v10, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    invoke-static {v4, v9, v10}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    const/4 v9, 0x4

    aput-object v4, v3, v9

    .line 364
    invoke-direct {v1, v3}, Lcom/squareup/fsm/FiniteStateEngine;-><init>([Lcom/squareup/fsm/Rule;)V

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->readerStateEngine:Lcom/squareup/fsm/FiniteStateEngine;

    .line 381
    new-instance v1, Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;

    const/16 v3, 0x29

    new-array v3, v3, [Lcom/squareup/fsm/Rule;

    .line 382
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    new-instance v10, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$4;

    invoke-direct {v10, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$4;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v10, Lcom/squareup/fsm/SideEffect;

    invoke-static {v4, v10}, Lcom/squareup/fsm/Rule;->onEntry(Ljava/lang/Object;Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    aput-object v4, v3, v5

    .line 383
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    new-instance v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$5;

    move-object v10, v0

    check-cast v10, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {v5, v10}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$5;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    new-instance v11, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$sam$com_squareup_fsm_SideEffect$0;

    invoke-direct {v11, v5}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$sam$com_squareup_fsm_SideEffect$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v11, Lcom/squareup/fsm/SideEffect;

    invoke-static {v4, v11}, Lcom/squareup/fsm/Rule;->onEntry(Ljava/lang/Object;Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    aput-object v4, v3, v6

    .line 385
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowCashDialog;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowCashDialog;

    sget-object v11, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SHOWING_CASH_DIALOG:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v4, v5, v11}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    .line 386
    new-instance v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;

    invoke-direct {v5, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$6;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v5, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v4, v5}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    aput-object v4, v3, v7

    .line 393
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SHOWING_CASH_DIALOG:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CashDialogDismiss;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CashDialogDismiss;

    sget-object v11, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v4, v5, v11}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    .line 394
    new-instance v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$7;

    invoke-direct {v5, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$7;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v5, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v4, v5}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    aput-object v4, v3, v8

    .line 400
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowSecondary;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowSecondary;

    sget-object v8, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v4, v5, v8}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    .line 401
    new-instance v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$8;

    invoke-direct {v5, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$8;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v5, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v4, v5}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    aput-object v4, v3, v9

    .line 406
    sget-object v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v5, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayContactless;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayContactless;

    sget-object v8, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v4, v5, v8}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    .line 407
    new-instance v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$9;

    invoke-direct {v5, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$9;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v5, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v4, v5}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v4

    aput-object v4, v3, v2

    .line 415
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ReenableContactless;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ReenableContactless;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 416
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$10;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$10;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/4 v4, 0x6

    aput-object v2, v3, v4

    .line 419
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$BackPressed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$BackPressed;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 420
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$11;

    invoke-direct {v4, v10}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$11;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    new-instance v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$sam$com_squareup_fsm_Guard$0;

    invoke-direct {v5, v4}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$sam$com_squareup_fsm_Guard$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v5, Lcom/squareup/fsm/Guard;

    invoke-virtual {v2, v5}, Lcom/squareup/fsm/Rule;->onlyIf(Lcom/squareup/fsm/Guard;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 421
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$12;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$12;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/4 v4, 0x7

    aput-object v2, v3, v4

    .line 426
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SoloSellerCashReceived;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SoloSellerCashReceived;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 428
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$13;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$13;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    .line 427
    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x8

    aput-object v2, v3, v4

    .line 432
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$BackPressed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$BackPressed;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 433
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$14;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$14;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/Guard;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->onlyIf(Lcom/squareup/fsm/Guard;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x9

    aput-object v2, v3, v4

    .line 435
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$15;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$15;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-static {v2, v4}, Lcom/squareup/fsm/Rule;->onEntry(Ljava/lang/Object;Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0xa

    aput-object v2, v3, v4

    .line 442
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillConfirmed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillConfirmed;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 443
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$16;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$16;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0xb

    aput-object v2, v3, v4

    .line 444
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillDismiss;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillDismiss;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0xc

    aput-object v2, v3, v4

    .line 446
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillCheckPermissions;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillCheckPermissions;

    .line 447
    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL_CHECKING_PERMISSIONS:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    .line 445
    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0xd

    aput-object v2, v3, v4

    .line 451
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL_CHECKING_PERMISSIONS:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    .line 452
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$17;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$17;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    .line 450
    invoke-static {v2, v4}, Lcom/squareup/fsm/Rule;->onEntry(Ljava/lang/Object;Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0xe

    aput-object v2, v3, v4

    .line 453
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL_CHECKING_PERMISSIONS:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillConfirmed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillConfirmed;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 454
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$18;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$18;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0xf

    aput-object v2, v3, v4

    .line 455
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CANCELLING_BILL_CHECKING_PERMISSIONS:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillDismiss;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillDismiss;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x10

    aput-object v2, v3, v4

    .line 457
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowSplitTenderWarning;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowSplitTenderWarning;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SPLIT_TENDER_WARNING:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 458
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$19;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$19;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x11

    aput-object v2, v3, v4

    .line 459
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SPLIT_TENDER_WARNING:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SplitTenderWarningAcknowledged;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SplitTenderWarningAcknowledged;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x12

    aput-object v2, v3, v4

    .line 461
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$FinishWithSplitTender;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$FinishWithSplitTender;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 462
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$20;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$20;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x13

    aput-object v2, v3, v4

    .line 466
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCash;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCash;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 467
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$21;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$21;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x14

    aput-object v2, v3, v4

    .line 468
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SHOWING_CASH_DIALOG:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCash;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCash;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 469
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$22;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$22;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x15

    aput-object v2, v3, v4

    .line 470
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCard;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCard;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 471
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$23;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$23;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x16

    aput-object v2, v3, v4

    .line 475
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayGiftCard;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayGiftCard;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 476
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$24;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$24;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x17

    aput-object v2, v3, v4

    .line 477
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$AddGiftCard;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$AddGiftCard;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 478
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$25;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$25;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x18

    aput-object v2, v3, v4

    .line 479
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CreateInvoice;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CreateInvoice;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 480
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$26;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$26;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x19

    aput-object v2, v3, v4

    .line 481
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCardOnFile;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCardOnFile;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 482
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$27;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$27;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffect;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffect;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x1a

    aput-object v2, v3, v4

    .line 483
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 484
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$28;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$28;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffectForEvent;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffectForEvent;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x1b

    aput-object v2, v3, v4

    .line 485
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderOptionSelected;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x1c

    aput-object v2, v3, v4

    .line 486
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessSwipe;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x1d

    aput-object v2, v3, v4

    .line 487
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessSplitTenderSwipe;

    .line 488
    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    .line 487
    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x1e

    aput-object v2, v3, v4

    .line 489
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessEmvDip;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x1f

    aput-object v2, v3, v4

    .line 490
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessContactless;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x20

    aput-object v2, v3, v4

    .line 491
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ExitWithReaderIssue;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x21

    aput-object v2, v3, v4

    .line 496
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->PROCESSING_TENDER:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 497
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$29;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$29;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffectForEvent;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffectForEvent;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x22

    aput-object v2, v3, v4

    .line 499
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SHOWING_CASH_DIALOG:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 500
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$30;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$30;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffectForEvent;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffectForEvent;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x23

    aput-object v2, v3, v4

    .line 502
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x24

    aput-object v2, v3, v4

    .line 505
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;

    .line 506
    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CONFIRMING_CHARGE_CARD_ON_FILE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    .line 504
    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 509
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$31;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$31;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffectForEvent;

    .line 508
    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffectForEvent;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x25

    aput-object v2, v3, v4

    .line 513
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CONFIRMING_CHARGE_CARD_ON_FILE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$DoNotChargeCardOnFile;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$DoNotChargeCardOnFile;

    .line 514
    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    .line 512
    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x26

    aput-object v2, v3, v4

    .line 518
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->CONFIRMING_CHARGE_CARD_ON_FILE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    const-class v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->PROCESSING_TENDER:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    .line 517
    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    .line 520
    new-instance v4, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$32;

    invoke-direct {v4, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$32;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v4, Lcom/squareup/fsm/SideEffectForEvent;

    invoke-virtual {v2, v4}, Lcom/squareup/fsm/Rule;->doAction(Lcom/squareup/fsm/SideEffectForEvent;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x27

    aput-object v2, v3, v4

    .line 522
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->PROCESSING_TENDER:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    sget-object v4, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderProcessed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderProcessed;

    sget-object v5, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->DONE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-static {v2, v4, v5}, Lcom/squareup/fsm/Rule;->transition(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/fsm/Rule;

    move-result-object v2

    const/16 v4, 0x28

    aput-object v2, v3, v4

    .line 381
    invoke-direct {v1, v3}, Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;-><init>([Lcom/squareup/fsm/Rule;)V

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowStateEngine:Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;

    .line 525
    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventSubscription:Lrx/subscriptions/CompositeSubscription;

    iget-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v2}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v2

    check-cast v2, Lio/reactivex/ObservableSource;

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v2, v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;

    invoke-direct {v3, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$33;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v3, Lrx/functions/Action1;

    invoke-virtual {v2, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v2

    const-string/jumbo v3, "toV1Observable(paymentEv\u2026lureReason)\n      }\n    }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/squareup/util/rx/RxKt;->plusAssign(Lrx/subscriptions/CompositeSubscription;Lrx/Subscription;)V

    .line 537
    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2Disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 538
    iget-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->onPaymentMethodSelected()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$34;

    invoke-direct {v3, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$34;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 537
    invoke-virtual {v1, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 543
    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2Disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->branReady()Lio/reactivex/Observable;

    move-result-object v2

    .line 544
    invoke-virtual {v2, v7, v6}, Lio/reactivex/Observable;->buffer(II)Lio/reactivex/Observable;

    move-result-object v2

    .line 545
    new-instance v3, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$35;

    invoke-direct {v3, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$35;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v3, Lio/reactivex/functions/Predicate;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    .line 546
    new-instance v3, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$36;

    invoke-direct {v3, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$36;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 543
    invoke-virtual {v1, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 801
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;

    invoke-direct {v1, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$selectMethodInput$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v1}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method

.method public static final synthetic access$branConnected(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->branConnected()V

    return-void
.end method

.method public static final synthetic access$buyerSelectedPaymentMethod(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->buyerSelectedPaymentMethod()V

    return-void
.end method

.method public static final synthetic access$cancelBillPayment(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->cancelBillPayment()V

    return-void
.end method

.method public static final synthetic access$checkCancelBillPaymentPermissions(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->checkCancelBillPaymentPermissions()V

    return-void
.end method

.method public static final synthetic access$createScreen(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->createScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$disableReaderPayments(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->disableReaderPayments()V

    return-void
.end method

.method public static final synthetic access$enableReaderPayments(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->enableReaderPayments()V

    return-void
.end method

.method public static final synthetic access$end(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->end()V

    return-void
.end method

.method public static final synthetic access$exitWithReaderIssue(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->exitWithReaderIssue(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method public static final synthetic access$falseThenTrueFilter(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Ljava/util/List;)Z
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->falseThenTrueFilter(Ljava/util/List;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getBuyerCheckoutEnabled$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Z
    .locals 0

    .line 229
    iget-boolean p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->buyerCheckoutEnabled:Z

    return p0
.end method

.method public static final synthetic access$getCheckoutInformationEventLogger$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/log/CheckoutInformationEventLogger;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    return-object p0
.end method

.method public static final synthetic access$getCompletedTenders$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Ljava/util/List;
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getCompletedTenders()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCurrentScreenKey$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lrx/subjects/BehaviorSubject;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getManualCardEntryScreenDataHelper$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->manualCardEntryScreenDataHelper:Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    return-object p0
.end method

.method public static final synthetic access$getSelectMethodInput$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object p0
.end method

.method public static final synthetic access$getSelectMethodScreensFactory$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tenderpayment/SelectMethodScreensFactory;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreensFactory:Lcom/squareup/tenderpayment/SelectMethodScreensFactory;

    return-object p0
.end method

.method public static final synthetic access$getShowModalList$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showModalList:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$getShowSecondaryMethods$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Z
    .locals 0

    .line 229
    iget-boolean p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showSecondaryMethods:Z

    return p0
.end method

.method public static final synthetic access$getStartArgs$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;
    .locals 1

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-nez p0, :cond_0

    const-string v0, "startArgs"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getTenderInEdit$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/payment/TenderInEdit;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method public static final synthetic access$getTutorialCore$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tutorialv2/TutorialCore;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-object p0
.end method

.method public static final synthetic access$getUserToken(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Ljava/lang/String;
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getUserToken()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getWorkflowResult$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Lcom/squareup/tenderpayment/TenderPaymentResult;
    .locals 0

    .line 229
    iget-object p0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-object p0
.end method

.method public static final synthetic access$hasTendered(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Z
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->hasTendered()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$onEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public static final synthetic access$onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    return-void
.end method

.method public static final synthetic access$onlyCard(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onlyCard(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$payCashCustomSelected(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->payCashCustomSelected()V

    return-void
.end method

.method public static final synthetic access$payGiftCardOnFile(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->payGiftCardOnFile()V

    return-void
.end method

.method public static final synthetic access$processContactlessPayment(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->processContactlessPayment(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method public static final synthetic access$processEmvDip(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->processEmvDip(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public static final synthetic access$processSwipe(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->processSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public static final synthetic access$recordCardPayment(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->recordCardPayment(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;)V

    return-void
.end method

.method public static final synthetic access$reenableReaderPayments(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->reenableReaderPayments()V

    return-void
.end method

.method public static final synthetic access$setBuyerCheckoutEnabled$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Z)V
    .locals 0

    .line 229
    iput-boolean p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->buyerCheckoutEnabled:Z

    return-void
.end method

.method public static final synthetic access$setShowSecondaryMethods$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Z)V
    .locals 0

    .line 229
    iput-boolean p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showSecondaryMethods:Z

    return-void
.end method

.method public static final synthetic access$setStartArgs$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    return-void
.end method

.method public static final synthetic access$setToastDataForCardError(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/ui/main/errors/PaymentError;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->setToastDataForCardError(Lcom/squareup/ui/main/errors/PaymentError;)V

    return-void
.end method

.method public static final synthetic access$setWorkflowResult$p(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/TenderPaymentResult;)V
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void
.end method

.method public static final synthetic access$setupPayCardOnFileResult(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->setupPayCardOnFileResult()V

    return-void
.end method

.method public static final synthetic access$shouldEnableNfcField(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Z
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->shouldEnableNfcField()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$showConfirmChargeCardOnFileDialog(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showConfirmChargeCardOnFileDialog(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;)V

    return-void
.end method

.method public static final synthetic access$showConfirmSplitTenderCancel(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showConfirmSplitTenderCancel()V

    return-void
.end method

.method public static final synthetic access$showSplitTenderWarning(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showSplitTenderWarning()V

    return-void
.end method

.method public static final synthetic access$start(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->start(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    return-void
.end method

.method public static final synthetic access$startFromSnapshot(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/workflow/Snapshot;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startFromSnapshot(Lcom/squareup/workflow/Snapshot;)V

    return-void
.end method

.method public static final synthetic access$takeSnapshot(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Ljava/util/Map;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->takeSnapshot(Ljava/util/Map;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateInternetState(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/connectivity/InternetState;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->updateInternetState(Lcom/squareup/connectivity/InternetState;)V

    return-void
.end method

.method public static final synthetic access$updateNfcState(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->updateNfcState()V

    return-void
.end method

.method public static final synthetic access$updateSelectMethodScreen(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;Lcom/squareup/connectivity/InternetState;Lcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
    .locals 0

    .line 229
    invoke-direct/range {p0 .. p6}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->updateSelectMethodScreen(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;Lcom/squareup/connectivity/InternetState;Lcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateSelectMethodScreen(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->updateSelectMethodScreen()V

    return-void
.end method

.method private final branConnected()V
    .locals 1

    .line 1610
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 1611
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->end()V

    return-void
.end method

.method private final buyerSelectedPaymentMethod()V
    .locals 1

    .line 1605
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowBuyerPip;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowBuyerPip;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 1606
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->end()V

    return-void
.end method

.method private final cancelBillPayment()V
    .locals 2

    .line 1128
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderCompleter;->cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1129
    :cond_0
    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 1132
    :goto_0
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 1131
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledApiPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledApiPayment;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 1130
    :cond_2
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledInvoicePayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledInvoicePayment;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 1129
    :goto_1
    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void
.end method

.method private final checkCancelBillPaymentPermissions()V
    .locals 3

    .line 1137
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->gateKeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 1138
    sget-object v1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$checkCancelBillPaymentPermissions$1;

    invoke-direct {v2, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$checkCancelBillPaymentPermissions$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v2, Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 1137
    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method private final connectedReaderCapabilities()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;>;"
        }
    .end annotation

    .line 716
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-nez v0, :cond_0

    const-string v1, "startArgs"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getReaderPaymentsEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 717
    const-class v0, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.just(EnumSet.\u2026apabilities::class.java))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 718
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 719
    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    .line 720
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$connectedReaderCapabilities$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$connectedReaderCapabilities$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    .line 721
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->asCapabilities(Lcom/squareup/settings/server/Features;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 722
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$connectedReaderCapabilities$2;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$connectedReaderCapabilities$2;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "cardReaderOracle\n       \u2026derCapabilities\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private final createCardOnFileInstrumentTender()V
    .locals 2

    .line 1534
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1535
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1536
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 1540
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/tender/TenderFactory;->createInstrument()Lcom/squareup/payment/tender/InstrumentTender$Builder;

    move-result-object v0

    const-string v1, "tender"

    .line 1541
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 1542
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 1544
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1545
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/payment/TenderInEdit;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    :cond_1
    return-void
.end method

.method private final createScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 1300
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->showScreen()V

    .line 1303
    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->isPrimaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    const-string v1, "selectMethodScreenData"

    if-eqz v0, :cond_1

    .line 1304
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreenData:Lrx/Observable;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1305
    :cond_0
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$1;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const-string v0, "selectMethodScreenData\n \u2026          )\n            }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1316
    :cond_1
    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->isSecondaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreenData:Lrx/Observable;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1317
    :cond_2
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$2;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$2;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Action1;

    invoke-virtual {p1, v0}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    .line 1318
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$3;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$3;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const-string v0, "selectMethodScreenData\n \u2026            )\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1328
    :cond_3
    sget-object v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "combineLatest(\n         \u2026      )\n        )\n      }"

    if-eqz v0, :cond_5

    .line 1329
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmChargeCardOnFileScreenData:Lrx/subjects/BehaviorSubject;

    check-cast p1, Lrx/Observable;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreenData:Lrx/Observable;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1330
    :cond_4
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    .line 1328
    invoke-static {p1, v0, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    .line 1331
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$4;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$4;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1351
    :cond_5
    sget-object v0, Lcom/squareup/tenderpayment/SplitTenderWarningDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "selectMethodScreenData.m\u2026      )\n        )\n      }"

    if-eqz v0, :cond_7

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreenData:Lrx/Observable;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$5;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1363
    :cond_7
    sget-object v0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreenData:Lrx/Observable;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$6;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$6;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1381
    :cond_9
    sget-object v0, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1382
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmCollectCashScreenData:Lrx/subjects/BehaviorSubject;

    check-cast p1, Lrx/Observable;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreenData:Lrx/Observable;

    if-nez v0, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 1383
    :cond_a
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    .line 1381
    invoke-static {p1, v0, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    .line 1384
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$createScreen$7;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    .line 1405
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Unknown key %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final disableReaderPayments()V
    .locals 1

    .line 1446
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->cancelPaymentAndDestroy()V

    return-void
.end method

.method private final enableReaderPayments()V
    .locals 2

    .line 1449
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->shouldEnableNfcField()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1451
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$enableReaderPayments$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$enableReaderPayments$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithNfcFieldOn(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    goto :goto_0

    .line 1469
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithoutNfc()V

    :goto_0
    return-void
.end method

.method private final end()V
    .locals 3

    .line 1244
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    .line 1247
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cardOptionShouldBeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Leaving SelectMethodScreen, card amount in range"

    goto :goto_1

    :cond_1
    const-string v0, "Leaving SelectMethodScreen"

    .line 1252
    :goto_1
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-interface {v1, v0, v2}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1256
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->onCompleted()V

    .line 1257
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tearDown()V

    .line 1259
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 1260
    instance-of v1, v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;

    if-eqz v1, :cond_2

    goto :goto_2

    .line 1261
    :cond_2
    instance-of v1, v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDip;

    if-eqz v1, :cond_3

    goto :goto_2

    .line 1262
    :cond_3
    instance-of v0, v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;

    if-eqz v0, :cond_4

    .line 1265
    :goto_2
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->destroy()V

    goto :goto_3

    .line 1268
    :cond_4
    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_OFF:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    :goto_3
    return-void

    .line 1244
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must set result before ending."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final exitWithReaderIssue(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V
    .locals 1

    .line 628
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;-><init>(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 629
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ExitWithReaderIssue;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ExitWithReaderIssue;-><init>(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method private final falseThenTrueFilter(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .line 1602
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    :goto_0
    return v2
.end method

.method private final getCompletedTenders()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation

    .line 348
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getCapturedTenders()Ljava/util/List;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillP\u2026         .capturedTenders"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 351
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    const-string v1, "emptyList()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private final getOrderIdentifier()Ljava/lang/String;
    .locals 2

    .line 345
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->orderIdentifier()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "transaction.order.orderIdentifier()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getOtherTenderType(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 5

    .line 1551
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->LEGACY_OTHER:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Cannot return OtherTenderType for "

    if-eqz v0, :cond_4

    .line 1555
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    const-string v2, "settings.paymentSettings"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getOtherTenderOptions()Ljava/util/List;

    move-result-object v0

    const-string v2, "settings.paymentSettings.otherTenderOptions"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 1652
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 1556
    iget-object v3, v3, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    iget-object v4, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    .line 1653
    :goto_1
    check-cast v2, Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v2, :cond_3

    return-object v2

    .line 1557
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 1552
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1551
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final getTenderAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 356
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireBillP\u2026ment().remainingAmountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string/jumbo v1, "transaction.amountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private final getUserToken()Ljava/lang/String;
    .locals 2

    .line 568
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nonLoggedInEmployeeToken:Ljava/lang/String;

    goto :goto_0

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "transaction.currentEmployeeToken"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private final hasTendered()Z
    .locals 1

    .line 1561
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasTendered()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 1093
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowStateEngine:Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;

    invoke-virtual {v1}, Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;->peekStateToSave()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 1094
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "currentState: %s, onEvent: %s"

    .line 1092
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1096
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowStateEngine:Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;

    invoke-virtual {v0, p1}, Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;->onEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 1100
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "onReaderEvent: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1101
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->readerStateEngine:Lcom/squareup/fsm/FiniteStateEngine;

    invoke-virtual {v0, p1}, Lcom/squareup/fsm/FiniteStateEngine;->onEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final onlyCard(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 1297
    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v1, 0x1

    new-array v1, v1, [Lkotlin/Pair;

    new-instance v2, Lkotlin/Pair;

    sget-object v3, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-direct {v2, v3, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p1, 0x0

    aput-object v2, v1, p1

    invoke-virtual {v0, v1}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final payCashCustomSelected()V
    .locals 3

    .line 1410
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1412
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1413
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    .line 1415
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 1416
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v2, v0, v0}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 1418
    :cond_2
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCashCustomAmount;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PayCashCustomAmount;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void
.end method

.method private final payGiftCardOnFile()V
    .locals 1

    .line 1509
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1510
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->createCardOnFileInstrumentTender()V

    .line 1513
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1514
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCardOnFileWithCurrentCustomer;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCardOnFileWithCurrentCustomer;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 1516
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCard;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCard;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 1513
    :goto_0
    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void
.end method

.method private final preAuthTipRequiredAndTipEnabled(ZZ)Z
    .locals 0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic preAuthTipRequiredAndTipEnabled$default(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;ZZILjava/lang/Object;)Z
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    .line 1594
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object p4, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 1595
    iget-object p2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result p2

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->preAuthTipRequiredAndTipEnabled(ZZ)Z

    move-result p0

    return p0
.end method

.method private final proceedWithQuickCashPayment(Lcom/squareup/protos/common/Money;)V
    .locals 3

    .line 1016
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1017
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void

    .line 1024
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1025
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    .line 1027
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->displayedAmountDue:Lrx/subjects/BehaviorSubject;

    const-string v1, "displayedAmountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 1028
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v2, v0, v0}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v1, v0}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 1029
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    .line 1031
    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_SPLIT_TENDER:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    if-ne p1, v0, :cond_2

    .line 1033
    sget-object p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SoloSellerCashReceived;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SoloSellerCashReceived;

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    goto :goto_0

    :cond_2
    const-string v0, "completeTenderResult"

    .line 1035
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onCompleteTenderResult(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)V

    .line 1037
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->end()V

    :goto_0
    return-void
.end method

.method private final processContactlessPayment(Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 1

    .line 591
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;-><init>(Lcom/squareup/ui/main/SmartPaymentResult;)V

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 592
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessContactless;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessContactless;-><init>(Lcom/squareup/ui/main/SmartPaymentResult;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method private final processEmvDip(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 576
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    const-string v1, "tenderInEdit.requireSmartCardTender()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 582
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDip;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDip;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 584
    :cond_0
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 587
    :goto_0
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessEmvDip;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessEmvDip;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method private final processSingleTenderSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    .line 610
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;-><init>(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 611
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessSwipe;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessSwipe;-><init>(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method private final processSplitTenderSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 2

    .line 615
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 617
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/squareup/tenderpayment/TenderCompleter;->payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 619
    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 621
    :goto_0
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 620
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 619
    :goto_1
    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 624
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessSplitTenderSwipe;

    const-string v1, "cResult"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ProcessSplitTenderSwipe;-><init>(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method private final processSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    .line 596
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->swipeValidator:Lcom/squareup/swipe/SwipeValidator;

    invoke-virtual {v0, p1}, Lcom/squareup/swipe/SwipeValidator;->swipeHasEnoughData(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 597
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->processSplitTenderSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    goto :goto_0

    .line 600
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->processSingleTenderSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    goto :goto_0

    .line 603
    :cond_1
    sget-object p1, Lcom/squareup/ui/main/errors/SwipeStraight;->INSTANCE:Lcom/squareup/ui/main/errors/SwipeStraight;

    check-cast p1, Lcom/squareup/ui/main/errors/PaymentError;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->setToastDataForCardError(Lcom/squareup/ui/main/errors/PaymentError;)V

    :goto_0
    return-void
.end method

.method private final promptForPayment()V
    .locals 2

    .line 1229
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1230
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1231
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->promptForPayment(Lcom/squareup/payment/tender/BaseTender$Builder;)Z

    goto :goto_0

    .line 1233
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->promptForPayment()Z

    :cond_1
    :goto_0
    return-void
.end method

.method private final recordCardPayment(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;)V
    .locals 3

    .line 1150
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1151
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    .line 1155
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;->getOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object p1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/payment/tender/TenderFactory;->createOther(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    .line 1156
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    check-cast p1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v0, p1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 1158
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$SplitTenderRecordCardPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$SplitTenderRecordCardPayment;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_1

    .line 1160
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;->getOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v0, "event.otherTenderType.tender_type!!"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->fromValue(I)Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object p1

    if-eqz p1, :cond_4

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 1162
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPaymentDebitCredit;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPaymentDebitCredit;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 1161
    :cond_3
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPayment;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 1160
    :goto_0
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    :goto_1
    return-void

    .line 1163
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Tender is not a recordable tender or has not been implemented yet."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final reenableReaderPayments()V
    .locals 1

    .line 1441
    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_OFF:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    .line 1442
    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    .line 1443
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showContactlessReenabledHud()V

    return-void
.end method

.method private final setToastDataForCardError(Lcom/squareup/ui/main/errors/PaymentError;)V
    .locals 2

    .line 551
    instance-of v0, p1, Lcom/squareup/ui/main/errors/PaymentOutOfRange;

    if-eqz v0, :cond_0

    .line 552
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->PAYMENT_OUT_OF_RANGE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 554
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/main/errors/PaymentOutOfRangeGiftCard;

    if-eqz v0, :cond_1

    .line 555
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->PAYMENT_OUT_OF_RANGE_GIFT_CARD:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 557
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/main/errors/SwipeStraight;

    if-eqz v0, :cond_2

    .line 558
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->SWIPE_STRAIGHT:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 560
    :cond_2
    instance-of p1, p1, Lcom/squareup/ui/main/errors/TryAgain;

    if-eqz p1, :cond_3

    .line 561
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->TRY_AGAIN:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 564
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final setupPayCardOnFileResult()V
    .locals 1

    .line 1521
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1522
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCardOnFileWithCurrentCustomer;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PayCardOnFileWithCurrentCustomer;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 1524
    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$SelectCustomerAndPayCardOnFile;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$SelectCustomerAndPayCardOnFile;

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 1521
    :goto_0
    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 1528
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1529
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->createCardOnFileInstrumentTender()V

    :cond_1
    return-void
.end method

.method private final shouldCheckPaymentReadySmartReaderIsAvailable()Z
    .locals 2

    .line 1599
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_WITHOUT_MATCHING_TMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final shouldEnableNfcField()Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 1588
    invoke-static {p0, v0, v0, v1, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->preAuthTipRequiredAndTipEnabled$default(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;ZZILjava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentInternetState:Lcom/squareup/connectivity/InternetState;

    sget-object v2, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v1}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->buyerCheckoutEnabled:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private final showAccidentalCashModal()Z
    .locals 3

    .line 1003
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getUserToken()Ljava/lang/String;

    move-result-object v0

    .line 1004
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->SHOW_ACCIDENTAL_CASH_MODAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showModalList:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final showConfirmChargeCardOnFileDialog(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;)V
    .locals 5

    .line 1105
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmChargeCardOnFileScreenData:Lrx/subjects/BehaviorSubject;

    .line 1106
    new-instance v1, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;

    .line 1107
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 1108
    iget-object v3, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getCustomerDisplayNameOrBlank()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "transaction.customerDisplayNameOrBlank"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;->getCardNameAndNumber$tender_payment_release()Ljava/lang/String;

    move-result-object v4

    .line 1109
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;->getInstrumentToken$tender_payment_release()Ljava/lang/String;

    move-result-object p1

    .line 1106
    invoke-direct {v1, v2, v3, v4, p1}, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 1112
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final showConfirmSplitTenderCancel()V
    .locals 2

    .line 1116
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/SplitTenderCancelWarningShown;

    invoke-direct {v1}, Lcom/squareup/tenderpayment/events/SplitTenderCancelWarningShown;-><init>()V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1117
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final showContactlessReenabledHud()V
    .locals 3

    .line 1123
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v2, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->CONTACTLESS_REENABLED:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 1124
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v2, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final showContactlessRow(ZZZZ)Z
    .locals 0

    .line 1572
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->preAuthTipRequiredAndTipEnabled(ZZ)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final showSplitTenderWarning()V
    .locals 2

    .line 1120
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/tenderpayment/SplitTenderWarningDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final splitTenderLabel()Lcom/squareup/tenderpayment/SelectMethod$TextData;
    .locals 7

    .line 1492
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1493
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-nez v0, :cond_0

    const-string v1, "startArgs"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getHasEditedSplit()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/squareup/tenderworkflow/R$string;->split_tender_edit_split:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/squareup/tenderworkflow/R$string;->split_tender_again:I

    goto :goto_0

    .line 1495
    :cond_2
    sget v0, Lcom/squareup/tenderworkflow/R$string;->split_tender:I

    :goto_0
    move v2, v0

    .line 1498
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$TextData;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1502
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$splitTenderLabel$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$splitTenderLabel$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    move-object v5, v1

    check-cast v5, Lcom/squareup/fsm/SideEffect;

    const/4 v6, 0x0

    move-object v1, v0

    .line 1498
    invoke-direct/range {v1 .. v6}, Lcom/squareup/tenderpayment/SelectMethod$TextData;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/fsm/SideEffect;Ljava/lang/String;)V

    return-object v0
.end method

.method private final splitTenderState()Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;
    .locals 4

    .line 323
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 324
    new-instance v1, Lcom/squareup/protos/common/Money;

    sget-object v2, Lcom/squareup/protos/common/Money;->DEFAULT_AMOUNT:Ljava/lang/Long;

    iget-object v3, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v1, v2, v3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 327
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    const-string v3, "startArgs"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getSplitTenderEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->DISABLED:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    goto :goto_0

    .line 328
    :cond_1
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->HIDDEN:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    goto :goto_0

    .line 329
    :cond_2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->HIDDEN:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    goto :goto_0

    .line 330
    :cond_3
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-nez v0, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getSplitTenderEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->NORMAL:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    goto :goto_0

    .line 331
    :cond_5
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->HIDDEN:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    :goto_0
    return-object v0
.end method

.method private final start(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;Lcom/squareup/workflow/legacy/Screen$Key;ZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;Z",
            "Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;",
            ")V"
        }
    .end annotation

    .line 736
    iput-object p4, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    .line 739
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcEnabled:Lrx/subjects/BehaviorSubject;

    move-object v1, v0

    check-cast v1, Lrx/Observable;

    .line 740
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->displayedAmountDue:Lrx/subjects/BehaviorSubject;

    move-object v2, v0

    check-cast v2, Lrx/Observable;

    .line 741
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->internetState:Lio/reactivex/Observable;

    const-string v8, "internetState"

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/ObservableSource;

    sget-object v3, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v3}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v3

    .line 742
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    move-object v4, v0

    check-cast v4, Lrx/Observable;

    .line 743
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->connectedReaderCapabilities()Lrx/Observable;

    move-result-object v5

    .line 744
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodBuyerCheckoutEnabled:Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;

    invoke-interface {v0}, Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;->isEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v6

    .line 745
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$1;

    move-object v7, p0

    check-cast v7, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {v0, v7}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function6;

    new-instance v7, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$sam$rx_functions_Func6$0;

    invoke-direct {v7, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$sam$rx_functions_Func6$0;-><init>(Lkotlin/jvm/functions/Function6;)V

    check-cast v7, Lrx/functions/Func6;

    .line 738
    invoke-static/range {v1 .. v7}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;)Lrx/Observable;

    move-result-object v0

    .line 746
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n        n\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodScreenData:Lrx/Observable;

    .line 748
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    .line 749
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$2;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$2;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "features.featureEnabled(\u2026koutEnabled = enabled!! }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->buyerCheckoutEnabledDisposable:Lio/reactivex/disposables/Disposable;

    .line 751
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v0}, Lcom/squareup/log/CheckoutInformationEventLogger;->startCheckoutIfHasNot()V

    .line 752
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->touchEventMonitor:Lcom/squareup/ui/TouchEventMonitor;

    .line 753
    invoke-interface {v0}, Lcom/squareup/ui/TouchEventMonitor;->observeTouchEvents()Lrx/Observable;

    move-result-object v0

    .line 754
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$3;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$3;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string/jumbo v1, "touchEventMonitor\n      \u2026mationEventLogger.tap() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->touchEventSubscription:Lrx/Subscription;

    .line 758
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 759
    invoke-virtual {p4}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p4

    iput-object p4, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->splitTenderAmount:Lcom/squareup/protos/common/Money;

    .line 762
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->promptForPayment()V

    .line 765
    :cond_0
    iget-object p4, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->readerStateEngine:Lcom/squareup/fsm/FiniteStateEngine;

    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->UNINITIALIZED:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    invoke-virtual {p4, v0}, Lcom/squareup/fsm/FiniteStateEngine;->startFromState(Ljava/lang/Object;)V

    .line 766
    iget-object p4, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowStateEngine:Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;

    invoke-virtual {p4, p1}, Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;->startFromState(Ljava/lang/Object;)V

    .line 767
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 768
    iput-boolean p3, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showSecondaryMethods:Z

    .line 770
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 771
    iget-object p2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->internetState:Lio/reactivex/Observable;

    invoke-static {p2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p3, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {p3}, Lcom/squareup/payment/OfflineModeMonitor;->offlineMode()Lio/reactivex/Observable;

    move-result-object p3

    const-string p4, "offlineModeMonitor.offlineMode()"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 770
    invoke-virtual {p1, p2, p3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 773
    new-instance p2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$4;

    invoke-direct {p2, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$4;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string p2, "Observables.combineLates\u2026ateInternetState(state) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->internetStateDisposable:Lio/reactivex/disposables/Disposable;

    .line 775
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventSubscription:Lrx/subscriptions/CompositeSubscription;

    const/4 p2, 0x2

    new-array p2, p2, [Lrx/Subscription;

    const/4 p3, 0x0

    .line 776
    iget-object p4, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->bus:Lcom/squareup/badbus/BadBus;

    const-class v0, Lcom/squareup/autocapture/PerformAutoCapture;

    invoke-virtual {p4, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p4

    check-cast p4, Lio/reactivex/ObservableSource;

    sget-object v0, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {p4, v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p4

    .line 777
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$5;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$5;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Action1;

    invoke-virtual {p4, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p4

    aput-object p4, p2, p3

    const/4 p3, 0x1

    .line 783
    iget-object p4, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {p4}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->isRunningState()Lio/reactivex/Observable;

    move-result-object p4

    const-string v0, "pauseAndResumeRegistrar.isRunningState"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p4, Lio/reactivex/ObservableSource;

    sget-object v0, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {p4, v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p4

    .line 784
    new-instance v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$6;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$start$6;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v0, Lrx/functions/Action1;

    invoke-virtual {p4, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p4

    aput-object p4, p2, p3

    .line 775
    invoke-virtual {p1, p2}, Lrx/subscriptions/CompositeSubscription;->addAll([Lrx/Subscription;)V

    .line 798
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    invoke-interface {p1}, Lcom/squareup/tenderpayment/ChangeHudToaster;->toastIfAvailable()V

    return-void
.end method

.method private final start(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 3

    .line 681
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getRestartSelectMethodData()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;->getShowSecondaryMethods()Z

    move-result v0

    .line 684
    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    if-eqz v0, :cond_0

    .line 685
    sget-object v2, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 683
    :goto_0
    invoke-direct {p0, v1, v2, v0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->start(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;Lcom/squareup/workflow/legacy/Screen$Key;ZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    return-void
.end method

.method private final startFromSnapshot(Lcom/squareup/workflow/Snapshot;)V
    .locals 5

    .line 663
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 1644
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 664
    invoke-static {}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->values()[Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    move-result-object v0

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    .line 665
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 666
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v2

    .line 667
    new-instance v3, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {v3, v1, v2}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p1

    .line 1645
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v2, "Parcel.obtain()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1646
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 1647
    array-length v2, p1

    const/4 v4, 0x0

    invoke-virtual {v1, p1, v4, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 1648
    invoke-virtual {v1, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 1649
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1650
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 669
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    .line 675
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getRestartSelectMethodData()Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;->getShowSecondaryMethods()Z

    move-result v1

    .line 672
    invoke-direct {p0, v0, v3, v1, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->start(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;Lcom/squareup/workflow/legacy/Screen$Key;ZLcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    return-void
.end method

.method private final takeSnapshot(Ljava/util/Map;)Lcom/squareup/workflow/Snapshot;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    .line 633
    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    invoke-static {p1}, Lcom/squareup/container/LayeredScreensKt;->top(Ljava/util/Map;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    iput-object p1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 635
    new-instance p1, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {p1}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowStateEngine:Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;

    invoke-virtual {v1}, Lcom/squareup/fsm/FiniteStateEngineWithUntypedEvents;->peekStateToSave()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->ordinal()I

    move-result v1

    iput v1, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 641
    iget-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v2, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 642
    iget-boolean v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showSecondaryMethods:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    :goto_0
    iput-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 643
    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->SELECT_METHOD:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$State;->ordinal()I

    move-result v1

    iput v1, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 646
    :cond_1
    new-instance v1, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    .line 647
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    const-string v3, "startArgs"

    if-nez v2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getTenderOptions()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object v4

    .line 648
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-nez v2, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getReaderPaymentsEnabled()Z

    move-result v5

    .line 649
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-nez v2, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v2}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getSplitTenderEnabled()Z

    move-result v6

    .line 650
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v7

    .line 651
    new-instance v8, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showSecondaryMethods:Z

    invoke-direct {v8, v2}, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;-><init>(Z)V

    .line 652
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-nez v2, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;->getHasEditedSplit()Z

    move-result v9

    move-object v2, v1

    move-object v3, v4

    move v4, v5

    move v5, v6

    move-object v6, v7

    move-object v7, v8

    move v8, v9

    .line 646
    invoke-direct/range {v2 .. v8}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;ZZLcom/squareup/protos/common/Money;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;Z)V

    .line 655
    sget-object v2, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v3, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$takeSnapshot$1;

    invoke-direct {v3, p1, v0, v1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$takeSnapshot$1;-><init>(Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2, v3}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method private final tearDown()V
    .locals 2

    .line 1275
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    sget-object v1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowBuyerPip;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowBuyerPip;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1276
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 1279
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    sget-object v1, Lcom/squareup/tenderpayment/TenderPaymentResult$QuickCashReceived;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$QuickCashReceived;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1280
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/ChangeHudToaster;->clear()V

    goto :goto_0

    .line 1282
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/ChangeHudToaster;->toastIfAvailable()V

    .line 1285
    :goto_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->displayedAmountDue:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->onCompleted()V

    .line 1286
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcEnabled:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->onCompleted()V

    .line 1287
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmChargeCardOnFileScreenData:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->onCompleted()V

    .line 1288
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmCollectCashScreenData:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->onCompleted()V

    .line 1289
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2Disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    .line 1290
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->internetStateDisposable:Lio/reactivex/disposables/Disposable;

    if-nez v0, :cond_2

    const-string v1, "internetStateDisposable"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 1291
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->buyerCheckoutEnabledDisposable:Lio/reactivex/disposables/Disposable;

    if-nez v0, :cond_3

    const-string v1, "buyerCheckoutEnabledDisposable"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 1292
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventSubscription:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    .line 1293
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->touchEventSubscription:Lrx/Subscription;

    if-nez v0, :cond_4

    const-string/jumbo v1, "touchEventSubscription"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 1294
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onWorkflowEnd:Lrx/subjects/PublishSubject;

    invoke-virtual {v0}, Lrx/subjects/PublishSubject;->onCompleted()V

    return-void
.end method

.method private final updateInternetState(Lcom/squareup/connectivity/InternetState;)V
    .locals 0

    .line 878
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentInternetState:Lcom/squareup/connectivity/InternetState;

    .line 881
    sget-object p1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_OFF:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    .line 882
    sget-object p1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    return-void
.end method

.method private final updateNfcState()V
    .locals 2

    .line 1474
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1475
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcEnabled:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void

    .line 1478
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/NfcProcessor;->areContactlessReadersReadyForPayments()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1479
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->shouldEnableNfcField()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1480
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcEnabled:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE_WITHOUT_TAP:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 1481
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->hasPaymentStartedOnContactlessReader()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1482
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcEnabled:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->AVAILABLE:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 1484
    :cond_2
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcEnabled:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->TIMED_OUT:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 1487
    :cond_3
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcEnabled:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;->DISABLED:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private final updateSelectMethodScreen(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;Lcom/squareup/connectivity/InternetState;Lcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/connectivity/InternetState;",
            "Lcom/squareup/tenderpayment/SelectMethod$ToastData;",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;Z)",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 838
    iget-object v1, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->screenDataRenderer:Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;

    .line 839
    iget-object v2, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->startArgs:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    if-nez v2, :cond_0

    const-string v3, "startArgs"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 840
    :cond_0
    iget-object v3, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->selectMethodInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 843
    sget-object v4, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    const/4 v5, 0x0

    move-object/from16 v7, p3

    if-ne v7, v4, :cond_1

    const/4 v7, 0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    .line 847
    :goto_0
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v10

    const-string/jumbo v4, "transaction.amountDue"

    invoke-static {v10, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 849
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTransactionMaximum()J

    move-result-wide v8

    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v4}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v4

    const-string v11, "settings.userSettings"

    invoke-static {v4, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v4

    const-string v12, "settings.userSettings.currency"

    invoke-static {v4, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 848
    invoke-static {v8, v9, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v13

    .line 852
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v8

    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v4}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v4

    invoke-static {v4, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v4

    invoke-static {v4, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 851
    invoke-static {v8, v9, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v12

    .line 854
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v14

    .line 855
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getNumSplitsTotal()J

    move-result-wide v8

    const-wide/16 v15, 0x1

    cmp-long v4, v8, v15

    if-lez v4, :cond_2

    const/4 v15, 0x1

    goto :goto_1

    :cond_2
    const/4 v15, 0x0

    .line 857
    :goto_1
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    .line 858
    iget-object v8, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v8}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v8

    .line 859
    iget-object v9, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v9}, Lcom/squareup/ui/NfcProcessor;->areContactlessReadersReadyForPayments()Z

    move-result v9

    move/from16 v6, p6

    .line 856
    invoke-direct {v0, v4, v8, v9, v6}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showContactlessRow(ZZZZ)Z

    move-result v31

    .line 862
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getCustomerDisplayNameOrBlank()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v8, "transaction.customerDisplayNameOrBlank"

    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_3

    const/16 v32, 0x1

    goto :goto_2

    :cond_3
    const/16 v32, 0x0

    .line 863
    :goto_2
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v17

    .line 864
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getCompletedTenders()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    add-int/2addr v4, v5

    int-to-long v4, v4

    move-wide/from16 v18, v4

    .line 865
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getNumSplitsTotal()J

    move-result-wide v20

    .line 866
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->splitTenderState()Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    move-result-object v22

    .line 867
    invoke-direct/range {p0 .. p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->splitTenderLabel()Lcom/squareup/tenderpayment/SelectMethod$TextData;

    move-result-object v23

    .line 868
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->cardOption()Lcom/squareup/payment/CardOptionEnabled;

    move-result-object v4

    move-object/from16 v24, v4

    const-string/jumbo v5, "transaction.cardOption()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 869
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v4}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v25

    .line 870
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v4}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v4

    invoke-static {v4, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v4

    move-object/from16 v26, v4

    const-string v5, "settings.userSettings.countryCode"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 871
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {v4}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v27

    .line 872
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getCustomerGiftCardInstrumentDetails()Ljava/util/List;

    move-result-object v28

    .line 873
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->getCustomerNonGiftCardInstrumentDetails()Ljava/util/List;

    move-result-object v29

    .line 874
    iget-object v4, v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v4}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v30

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move v6, v7

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    move-object v11, v13

    move v13, v14

    move v14, v15

    move/from16 v15, v31

    move/from16 v16, v32

    .line 838
    invoke-virtual/range {v1 .. v30}, Lcom/squareup/tenderpayment/SelectMethodWorkflowScreenDataRenderer;->createSelectMethodScreenData(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$NfcState;Lcom/squareup/protos/common/Money;ZLcom/squareup/tenderpayment/SelectMethod$ToastData;Ljava/util/EnumSet;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZZZJJLcom/squareup/tenderpayment/SelectMethod$SplitTenderState;Lcom/squareup/tenderpayment/SelectMethod$TextData;Lcom/squareup/payment/CardOptionEnabled;ZLcom/squareup/CountryCode;ZLjava/util/List;Ljava/util/List;Z)Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    move-result-object v1

    return-object v1
.end method

.method private final updateSelectMethodScreen()V
    .locals 2

    .line 1426
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->displayedAmountDue:Lrx/subjects/BehaviorSubject;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 1429
    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;->TURN_ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onReaderEvent(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderEvent;)V

    .line 1432
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->promptForPayment()V

    .line 1433
    iget-boolean v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showSecondaryMethods:Z

    if-eqz v0, :cond_0

    .line 1434
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 1436
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public abandon()V
    .locals 2

    .line 701
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Select Method Workflow has successfully emitted a result and is completing."

    .line 702
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 706
    :cond_0
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tearDown()V

    .line 710
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->readerStateEngine:Lcom/squareup/fsm/FiniteStateEngine;

    invoke-virtual {v0}, Lcom/squareup/fsm/FiniteStateEngine;->peekStateToSave()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    sget-object v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;->ON:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$ReaderState;

    if-ne v0, v1, :cond_1

    .line 711
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->paymentEventHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->destroy()V

    :cond_1
    return-void
.end method

.method public final chargeCardOnFile(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "tenderedAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrumentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1082
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final doNotChargeCardOnFile()V
    .locals 1

    .line 1076
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$DoNotChargeCardOnFile;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$DoNotChargeCardOnFile;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public getResult()Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 342
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->last()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$result$1;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$result$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->toSingle()Lrx/Single;

    move-result-object v0

    const-string v1, "currentScreenKey.last().\u2026rkflowResult }.toSingle()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getState()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    .line 336
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentScreenKey:Lrx/subjects/BehaviorSubject;

    .line 337
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$state$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$state$1;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$sam$rx_functions_Func1$0;

    invoke-direct {v2, v1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/subjects/BehaviorSubject;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 338
    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onWorkflowEnd:Lrx/subjects/PublishSubject;

    check-cast v1, Lrx/Observable;

    invoke-virtual {v0, v1}, Lrx/Observable;->takeUntil(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    .line 339
    new-instance v1, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$state$2;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$state$2;-><init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "currentScreenKey\n       \u2026k, takeSnapshot(stack)) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onAddGiftCardSelected()V
    .locals 2

    .line 962
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 963
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$AddGiftCard;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$AddGiftCard;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .line 892
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$BackPressed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$BackPressed;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onCardOnFileSelected()V
    .locals 3

    .line 956
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 957
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/CardOnFileTapped;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/events/CardOnFileTapped;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 958
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCardOnFile;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCardOnFile;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onCardSelected()V
    .locals 3

    .line 911
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/CnpTappedEvent;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/events/CnpTappedEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 912
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCard;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCard;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onCashSelected()V
    .locals 3

    .line 895
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 896
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/CustomCashTappedEvent;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/events/CustomCashTappedEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 898
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showAccidentalCashModal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 899
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmCollectCashScreenData:Lrx/subjects/BehaviorSubject;

    .line 900
    new-instance v1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;

    .line 901
    iget-object v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->customCashValue:Lcom/squareup/protos/common/Money;

    .line 900
    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;-><init>(Lcom/squareup/protos/common/Money;)V

    .line 899
    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 904
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowCashDialog;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowCashDialog;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    goto :goto_0

    .line 906
    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCash;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCash;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    :goto_0
    return-void
.end method

.method public final onCompleteTenderResult(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)V
    .locals 1

    const-string v0, "completeTenderResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1199
    sget-object v0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    .line 1221
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void

    .line 1215
    :cond_0
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void

    .line 1211
    :cond_1
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void

    .line 1209
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cash/Other tenders should not be authorized."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 1205
    :cond_3
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void

    .line 1201
    :cond_4
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    return-void
.end method

.method public final onConfirmCancelSplitTenderTransaction()V
    .locals 1

    .line 1070
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillCheckPermissions;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillCheckPermissions;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onConfirmChargeCardOnFile(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "tenderedAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instrumentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardNameAndNumber"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 979
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnConfirmCardOnFileChargedEvent;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    .line 978
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onContactlessSelected()V
    .locals 3

    .line 1061
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v0}, Lcom/squareup/cardreader/DippedCardTracker;->mustReinsertDippedCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1062
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v2, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->REMOVE_CHIP_CARD:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 1063
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->toastData:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    sget-object v2, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->NONE:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/SelectMethod$ToastData;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 1065
    :cond_0
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayContactless;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayContactless;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    :goto_0
    return-void
.end method

.method public final onDismissCancelSplitTenderTransaction()V
    .locals 1

    .line 1072
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillDismiss;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CancelBillDismiss;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onDismissCollectCash()V
    .locals 1

    .line 1074
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CashDialogDismiss;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CashDialogDismiss;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onDismissed()V
    .locals 1

    .line 1088
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SellerCashReceivedPaymentDismissed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SellerCashReceivedPaymentDismissed;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onGiftCardSelected()V
    .locals 3

    .line 916
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 917
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/GiftCardTappedEvent;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/events/GiftCardTappedEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 918
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayGiftCard;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayGiftCard;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onInvoiceSelected()V
    .locals 3

    .line 949
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 950
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/InvoiceTappedEvent;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/events/InvoiceTappedEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 951
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Payment Type: Invoice"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 952
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CreateInvoice;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$CreateInvoice;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onOtherTenderSelected(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)V
    .locals 3

    const-string v0, "tender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 925
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 926
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/OtherTappedEvent;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p2, v2}, Lcom/squareup/tenderpayment/events/OtherTappedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 927
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOtherTenderType(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v0

    .line 929
    iget-object v1, v0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string/jumbo v2, "type.accepts_tips!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 930
    new-instance p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;

    invoke-direct {p1, v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;-><init>(Lcom/squareup/server/account/protos/OtherTenderType;)V

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void

    .line 934
    :cond_1
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;

    .line 937
    new-instance v1, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;

    iget-boolean v2, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showSecondaryMethods:Z

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;-><init>(Z)V

    .line 934
    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/tenderpayment/TenderPaymentResult$PayOther;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;Lcom/squareup/tenderpayment/RestartSelectMethodWorkflowData;)V

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 939
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    sget-object v1, Lcom/squareup/comms/protos/common/TenderType;->OTHER:Lcom/squareup/comms/protos/common/TenderType;

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z

    .line 940
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onQuickCashTenderReceived(Lcom/squareup/protos/common/Money;)V
    .locals 3

    const-string/jumbo v0, "tenderedAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 984
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/QuickCashTappedEvent;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/tenderpayment/events/QuickCashTappedEvent;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 990
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->showAccidentalCashModal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 991
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->confirmCollectCashScreenData:Lrx/subjects/BehaviorSubject;

    .line 992
    new-instance v1, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;-><init>(Lcom/squareup/protos/common/Money;)V

    .line 991
    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 996
    sget-object p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowCashDialog;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowCashDialog;

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    goto :goto_0

    .line 998
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->proceedWithQuickCashPayment(Lcom/squareup/protos/common/Money;)V

    :goto_0
    return-void
.end method

.method public final onRecordFullyCompedPayment(Lcom/squareup/protos/common/Money;)V
    .locals 3

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 888
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/HeaderButtonTapped;

    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/tenderpayment/events/HeaderButtonTapped;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 889
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnCashTenderReceivedEvent;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onReenableContactlessClicked()V
    .locals 1

    .line 1058
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ReenableContactless;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ReenableContactless;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onSecondaryTendersSelected()V
    .locals 2

    .line 1042
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/MorePaymentOptionsTappedEvent;

    invoke-direct {v1}, Lcom/squareup/tenderpayment/events/MorePaymentOptionsTappedEvent;-><init>()V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1043
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowSecondary;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowSecondary;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onSplitTender()V
    .locals 2

    .line 1047
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/tenderpayment/events/SplitTenderTappedEvent;

    invoke-direct {v1}, Lcom/squareup/tenderpayment/events/SplitTenderTappedEvent;-><init>()V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1048
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 1049
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->currentInternetState:Lcom/squareup/connectivity/InternetState;

    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1050
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowSplitTenderWarning;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$ShowSplitTenderWarning;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    goto :goto_0

    .line 1053
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    sget-object v1, Lcom/squareup/comms/protos/common/TenderType;->SPLIT:Lcom/squareup/comms/protos/common/TenderType;

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z

    .line 1054
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$FinishWithSplitTender;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$FinishWithSplitTender;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    :goto_0
    return-void
.end method

.method public final onSplitTenderWarningDismissed()V
    .locals 1

    .line 1085
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SplitTenderWarningAcknowledged;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$SplitTenderWarningAcknowledged;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onTenderOptionSelected(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V
    .locals 1

    const-string/jumbo v0, "tenderOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 969
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$TenderOptionSelected;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$TenderOptionSelected;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    iput-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->workflowResult:Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 970
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderOptionSelected;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderOptionSelected;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final onTenderReceived(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1171
    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    .line 1173
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;->getTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    goto :goto_0

    .line 1188
    :cond_1
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->payCashTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    const-string p1, "completer.payCashTender(event.tenderedAmount)"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1175
    :cond_2
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;

    .line 1177
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderReceivedEvent$OnChargeCardOnFileEvent;->getInstrumentToken$tender_payment_release()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->getInstrumentDetails(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    move-result-object p1

    .line 1179
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1180
    invoke-direct {p0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->createCardOnFileInstrumentTender()V

    .line 1181
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender$Builder;

    move-result-object v0

    const-string v1, "tenderInEdit.requireInstrumentTender()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->setInstrument(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)V

    .line 1185
    :cond_3
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->completer:Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->chargeCardOnFile(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    const-string p1, "completer.chargeCardOnFile(selectedInstrument)"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1192
    :goto_0
    invoke-virtual {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onCompleteTenderResult(Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;)V

    .line 1193
    sget-object p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderProcessed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderProcessed;

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    .line 1195
    iget-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->changeHudToaster:Lcom/squareup/tenderpayment/ChangeHudToaster;

    invoke-interface {p1}, Lcom/squareup/tenderpayment/ChangeHudToaster;->toastIfAvailable()V

    return-void
.end method

.method public final onThirdPartyCardSelected()V
    .locals 3

    .line 944
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 945
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    const-string v2, "settings.paymentSettings"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getThirdPartyOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v1

    const-string v2, "settings.paymentSettings.thirdPartyOtherTenderType"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnRecordPaymentEvent;-><init>(Lcom/squareup/server/account/protos/OtherTenderType;)V

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    return-void
.end method

.method public final processCashPayment(Lcom/squareup/protos/common/Money;)V
    .locals 2

    const-string/jumbo v0, "tenderedAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1008
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    iget-object v1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->customCashValue:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1009
    sget-object p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCash;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayCash;

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->onEvent(Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;)V

    goto :goto_0

    .line 1011
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->proceedWithQuickCashPayment(Lcom/squareup/protos/common/Money;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic sendEvent(Ljava/lang/Object;)V
    .locals 0

    .line 229
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->sendEvent(Ljava/lang/Void;)V

    return-void
.end method

.method public sendEvent(Ljava/lang/Void;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public toCompletable()Lrx/Completable;
    .locals 1

    .line 229
    invoke-static {p0}, Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$DefaultImpls;->toCompletable(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow;)Lrx/Completable;

    move-result-object v0

    return-object v0
.end method
