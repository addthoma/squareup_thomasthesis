.class public final Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;
.super Ljava/lang/Object;
.source "SynchronousLocalPaymentPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final sessionlessPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;->sessionlessPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;",
            ">;)",
            "Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;)Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;",
            ")",
            "Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;-><init>(Ldagger/Lazy;Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;->sessionlessPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;

    invoke-static {v0, v1}, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;)Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter_Factory;->get()Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;

    move-result-object v0

    return-object v0
.end method
