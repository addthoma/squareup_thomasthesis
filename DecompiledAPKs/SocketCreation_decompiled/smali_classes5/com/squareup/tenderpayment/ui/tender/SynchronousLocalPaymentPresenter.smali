.class public Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;
.super Ljava/lang/Object;
.source "SynchronousLocalPaymentPresenter.java"


# instance fields
.field private final lazyTenderCompleter:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionlessPresenter:Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;


# direct methods
.method constructor <init>(Ldagger/Lazy;Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->lazyTenderCompleter:Ldagger/Lazy;

    .line 18
    iput-object p2, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->sessionlessPresenter:Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;

    return-void
.end method


# virtual methods
.method public startSynchronousCashPayment(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->lazyTenderCompleter:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->sessionlessPresenter:Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;

    .line 28
    invoke-virtual {v1, p1, p2}, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->startSynchronousCashPayment(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    .line 27
    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public startSynchronousOtherPayment(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->lazyTenderCompleter:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->sessionlessPresenter:Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;

    .line 44
    invoke-virtual {v1, p1, p2, p3}, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->startSynchronousOtherPayment(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    .line 43
    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    return-object p1
.end method

.method public startSynchronousZeroAmountPayment()Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->lazyTenderCompleter:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, p0, Lcom/squareup/tenderpayment/ui/tender/SynchronousLocalPaymentPresenter;->sessionlessPresenter:Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;

    .line 33
    invoke-virtual {v1}, Lcom/squareup/tenderpayment/ui/tender/SessionlessSynchronousLocalPaymentPresenter;->startSynchronousZeroAmountPayment()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderCompleter;->completeTenderAndAuthorize(Z)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    return-object v0
.end method
