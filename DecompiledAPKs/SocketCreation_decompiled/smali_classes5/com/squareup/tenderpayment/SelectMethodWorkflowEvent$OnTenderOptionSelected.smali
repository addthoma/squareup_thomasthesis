.class public final Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderOptionSelected;
.super Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;
.source "SelectMethodWorkflowEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnTenderOptionSelected"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderOptionSelected;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;",
        "tenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V",
        "getTenderOption",
        "()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tenderOption:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V
    .locals 1

    const-string/jumbo v0, "tenderOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderOptionSelected;->tenderOption:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    return-void
.end method


# virtual methods
.method public final getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$OnTenderOptionSelected;->tenderOption:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    return-object v0
.end method
