.class final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->render(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;->invoke(Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 483
    sget-object v1, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$DoNotChargeCardOnFile;->INSTANCE:Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$DoNotChargeCardOnFile;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 484
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$confirmChargeCardOnFileDialogDoNotChargeAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 486
    :cond_0
    instance-of p1, p1, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event$ChargeCardOnFile;

    if-eqz p1, :cond_1

    .line 487
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$10;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$confirmChargeCardOnFileDialogChargeAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 481
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void

    .line 487
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
