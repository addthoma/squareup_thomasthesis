.class public Lcom/squareup/text/PercentageScrubber;
.super Ljava/lang/Object;
.source "PercentageScrubber.java"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final precision:I


# direct methods
.method public constructor <init>(ILjavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/squareup/text/PercentageScrubber;->precision:I

    .line 20
    iput-object p2, p0, Lcom/squareup/text/PercentageScrubber;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    if-eqz p1, :cond_b

    .line 24
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/PercentageScrubber;->localeProvider:Ljavax/inject/Provider;

    .line 28
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/DecimalFormatSymbols;->getInstance(Ljava/util/Locale;)Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    .line 31
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    array-length v2, p1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v3, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x1

    :goto_0
    const/16 v8, 0x64

    const/16 v9, 0x30

    if-ge v3, v2, :cond_6

    aget-char v10, p1, v3

    if-lt v10, v9, :cond_3

    const/16 v9, 0x39

    if-gt v10, v9, :cond_3

    if-eqz v7, :cond_2

    if-ne v6, v5, :cond_1

    const/4 v6, 0x0

    :cond_1
    mul-int/lit8 v6, v6, 0xa

    add-int/lit8 v10, v10, -0x30

    add-int/2addr v6, v10

    .line 46
    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_1

    .line 47
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    iget v9, p0, Lcom/squareup/text/PercentageScrubber;->precision:I

    if-ge v8, v9, :cond_5

    .line 49
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    if-eqz v7, :cond_5

    .line 51
    iget v8, p0, Lcom/squareup/text/PercentageScrubber;->precision:I

    if-lez v8, :cond_5

    const/16 v8, 0x2e

    if-eq v10, v8, :cond_4

    if-ne v10, v0, :cond_5

    :cond_4
    const/4 v7, 0x0

    :cond_5
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 59
    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    if-ne v6, v5, :cond_7

    if-nez v7, :cond_7

    .line 61
    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_7
    if-eq v6, v5, :cond_8

    .line 63
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_8
    :goto_2
    if-eq v6, v8, :cond_a

    if-nez v7, :cond_9

    .line 67
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 69
    :cond_9
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 71
    :cond_a
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_b
    :goto_3
    const-string p1, ""

    return-object p1
.end method
