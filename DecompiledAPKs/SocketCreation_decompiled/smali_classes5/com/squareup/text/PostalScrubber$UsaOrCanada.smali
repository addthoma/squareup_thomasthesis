.class final Lcom/squareup/text/PostalScrubber$UsaOrCanada;
.super Lcom/squareup/text/PostalScrubber;
.source "PostalScrubber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/PostalScrubber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UsaOrCanada"
.end annotation


# static fields
.field private static final CANADA:Lcom/squareup/text/PostalScrubber;

.field private static final USA:Lcom/squareup/text/PostalScrubber;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 161
    new-instance v0, Lcom/squareup/text/PostalScrubber$Canada;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/text/PostalScrubber$Canada;-><init>(Lcom/squareup/text/PostalScrubber$1;)V

    sput-object v0, Lcom/squareup/text/PostalScrubber$UsaOrCanada;->CANADA:Lcom/squareup/text/PostalScrubber;

    .line 162
    new-instance v0, Lcom/squareup/text/PostalScrubber$Usa;

    invoke-direct {v0, v1}, Lcom/squareup/text/PostalScrubber$Usa;-><init>(Lcom/squareup/text/PostalScrubber$1;)V

    sput-object v0, Lcom/squareup/text/PostalScrubber$UsaOrCanada;->USA:Lcom/squareup/text/PostalScrubber;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 160
    invoke-direct {p0}, Lcom/squareup/text/PostalScrubber;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/text/PostalScrubber$1;)V
    .locals 0

    .line 160
    invoke-direct {p0}, Lcom/squareup/text/PostalScrubber$UsaOrCanada;-><init>()V

    return-void
.end method


# virtual methods
.method protected isValidFormattedPostalCode(Ljava/lang/String;)Z
    .locals 1

    .line 179
    sget-object v0, Lcom/squareup/text/PostalScrubber$UsaOrCanada;->CANADA:Lcom/squareup/text/PostalScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/PostalScrubber;->isValidFormattedPostalCode(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/text/PostalScrubber$UsaOrCanada;->USA:Lcom/squareup/text/PostalScrubber;

    .line 180
    invoke-virtual {v0, p1}, Lcom/squareup/text/PostalScrubber;->isValidFormattedPostalCode(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method protected parsePostal(Ljava/lang/String;)Lcom/squareup/text/PostalScrubber$Result;
    .locals 6

    .line 168
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-char v4, v0, v3

    .line 169
    invoke-static {v4}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 170
    sget-object v0, Lcom/squareup/text/PostalScrubber$UsaOrCanada;->CANADA:Lcom/squareup/text/PostalScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/PostalScrubber;->parsePostal(Ljava/lang/String;)Lcom/squareup/text/PostalScrubber$Result;

    move-result-object p1

    return-object p1

    .line 171
    :cond_0
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 172
    sget-object v0, Lcom/squareup/text/PostalScrubber$UsaOrCanada;->USA:Lcom/squareup/text/PostalScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/PostalScrubber;->parsePostal(Ljava/lang/String;)Lcom/squareup/text/PostalScrubber$Result;

    move-result-object p1

    return-object p1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 175
    :cond_2
    new-instance p1, Lcom/squareup/text/PostalScrubber$Result;

    const-string v0, ""

    invoke-direct {p1, v2, v0}, Lcom/squareup/text/PostalScrubber$Result;-><init>(ZLjava/lang/String;)V

    return-object p1
.end method
