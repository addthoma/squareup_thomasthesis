.class public Lcom/squareup/text/FacebookScrubber;
.super Ljava/lang/Object;
.source "FacebookScrubber.java"

# interfaces
.implements Lcom/squareup/text/SelectableTextScrubber;


# static fields
.field private static final FACEBOOK_PATTERN:Ljava/util/regex/Pattern;

.field private static final FACEBOOK_URL:Ljava/lang/String; = "facebook.com/"

.field private static final NOT_WORD_CHAR_OR_DOT:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "(https?://)?(www\\.)?facebook\\.com/(.{0,50})?"

    const/4 v1, 0x2

    .line 13
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/FacebookScrubber;->FACEBOOK_PATTERN:Ljava/util/regex/Pattern;

    const-string v0, "[^\\w.]"

    .line 14
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/FacebookScrubber;->NOT_WORD_CHAR_OR_DOT:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public scrub(Lcom/squareup/text/SelectableTextScrubber$SelectableText;Lcom/squareup/text/SelectableTextScrubber$SelectableText;)Lcom/squareup/text/SelectableTextScrubber$SelectableText;
    .locals 5

    .line 18
    iget-object v0, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    .line 19
    iget-object v1, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    .line 21
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object p1

    .line 25
    :cond_0
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    const/16 v3, 0xd

    const-string v4, "facebook.com/"

    if-eqz v2, :cond_2

    .line 27
    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-object p2

    .line 29
    :cond_1
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p1, v4, v3, v3}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object p1

    .line 32
    :cond_2
    sget-object p2, Lcom/squareup/text/FacebookScrubber;->FACEBOOK_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p2

    .line 33
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 34
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x3

    invoke-virtual {p2, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p2

    sget-object v1, Lcom/squareup/text/FacebookScrubber;->NOT_WORD_CHAR_OR_DOT:Ljava/util/regex/Pattern;

    invoke-static {p2, v1}, Lcom/squareup/util/Strings;->removePattern(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object p1

    .line 38
    :cond_3
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 39
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 40
    new-instance p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p2, p1, v0, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object p2

    .line 43
    :cond_4
    sget-object p2, Lcom/squareup/text/FacebookScrubber;->FACEBOOK_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/regex/Matcher;->matches()Z

    move-result p2

    if-eqz p2, :cond_5

    return-object p1

    .line 49
    :cond_5
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p1, v4, v3, v3}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object p1
.end method
