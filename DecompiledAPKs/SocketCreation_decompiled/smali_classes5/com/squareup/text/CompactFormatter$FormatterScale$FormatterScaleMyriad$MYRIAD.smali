.class public final Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;
.super Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;
.source "CompactFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MYRIAD"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;",
        "Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;",
        "()V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 108
    new-instance v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;

    invoke-direct {v0}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;-><init>()V

    sput-object v0, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;->INSTANCE:Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad$MYRIAD;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x2710

    .line 108
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    const-string v1, "BigDecimal.valueOf(10_000)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/text/CompactFormatter$FormatterScale$FormatterScaleMyriad;-><init>(Ljava/math/BigDecimal;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
