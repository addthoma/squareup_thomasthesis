.class public Lcom/squareup/text/LineHeightSpan;
.super Landroid/text/style/ReplacementSpan;
.source "LineHeightSpan.java"


# instance fields
.field private final lineHeight:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 22
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 23
    iput p1, p0, Lcom/squareup/text/LineHeightSpan;->lineHeight:I

    return-void
.end method

.method public static getLineHeightCharSequence(Landroid/content/res/Resources;I)Ljava/lang/CharSequence;
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/text/LineHeightSpan;

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    invoke-direct {v0, p0}, Lcom/squareup/text/LineHeightSpan;-><init>(I)V

    const-string p0, "."

    invoke-static {p0, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 0

    return-void
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 0

    const/4 p1, 0x0

    if-eqz p5, :cond_0

    .line 32
    iput p1, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 33
    iget p2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput p2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 34
    iget p2, p0, Lcom/squareup/text/LineHeightSpan;->lineHeight:I

    iput p2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 35
    iget p2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iput p2, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    :cond_0
    return p1
.end method
