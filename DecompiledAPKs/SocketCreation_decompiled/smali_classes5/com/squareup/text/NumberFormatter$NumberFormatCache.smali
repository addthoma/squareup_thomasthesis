.class final Lcom/squareup/text/NumberFormatter$NumberFormatCache;
.super Ljava/lang/Object;
.source "NumberFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/NumberFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NumberFormatCache"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/text/NumberFormatter$NumberFormatCache;",
        "",
        "locale",
        "Ljava/util/Locale;",
        "numberFormat",
        "Ljava/text/NumberFormat;",
        "(Ljava/util/Locale;Ljava/text/NumberFormat;)V",
        "getLocale",
        "()Ljava/util/Locale;",
        "getNumberFormat",
        "()Ljava/text/NumberFormat;",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final locale:Ljava/util/Locale;

.field private final numberFormat:Ljava/text/NumberFormat;


# direct methods
.method public constructor <init>(Ljava/util/Locale;Ljava/text/NumberFormat;)V
    .locals 1

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "numberFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/NumberFormatter$NumberFormatCache;->locale:Ljava/util/Locale;

    iput-object p2, p0, Lcom/squareup/text/NumberFormatter$NumberFormatCache;->numberFormat:Ljava/text/NumberFormat;

    return-void
.end method


# virtual methods
.method public final getLocale()Ljava/util/Locale;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/text/NumberFormatter$NumberFormatCache;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public final getNumberFormat()Ljava/text/NumberFormat;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/text/NumberFormatter$NumberFormatCache;->numberFormat:Ljava/text/NumberFormat;

    return-object v0
.end method
