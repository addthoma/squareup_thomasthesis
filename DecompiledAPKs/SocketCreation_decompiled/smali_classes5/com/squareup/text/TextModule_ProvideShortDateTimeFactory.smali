.class public final Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;
.super Ljava/lang/Object;
.source "TextModule_ProvideShortDateTimeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/text/DateFormat;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;->localeProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideShortDateTime(Ljava/util/Locale;Lcom/squareup/util/Res;)Ljava/text/DateFormat;
    .locals 0

    .line 41
    invoke-static {p0, p1}, Lcom/squareup/text/TextModule;->provideShortDateTime(Ljava/util/Locale;Lcom/squareup/util/Res;)Ljava/text/DateFormat;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/text/DateFormat;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;->get()Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/text/DateFormat;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iget-object v1, p0, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/text/TextModule_ProvideShortDateTimeFactory;->provideShortDateTime(Ljava/util/Locale;Lcom/squareup/util/Res;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method
