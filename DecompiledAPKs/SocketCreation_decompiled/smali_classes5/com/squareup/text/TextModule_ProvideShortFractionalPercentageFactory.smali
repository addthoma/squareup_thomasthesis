.class public final Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;
.super Ljava/lang/Object;
.source "TextModule_ProvideShortFractionalPercentageFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/util/Percentage;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;

    invoke-direct {v0, p0}, Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideShortFractionalPercentage(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-static {p0}, Lcom/squareup/text/TextModule;->provideShortFractionalPercentage(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/text/Formatter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;->provideShortFractionalPercentage(Ljavax/inject/Provider;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/text/TextModule_ProvideShortFractionalPercentageFactory;->get()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method
