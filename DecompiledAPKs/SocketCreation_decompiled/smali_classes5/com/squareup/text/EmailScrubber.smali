.class public Lcom/squareup/text/EmailScrubber;
.super Ljava/lang/Object;
.source "EmailScrubber.java"

# interfaces
.implements Lcom/squareup/text/InsertingScrubber;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static watcher(Lcom/squareup/text/HasSelectableText;)Landroid/text/TextWatcher;
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    new-instance v1, Lcom/squareup/text/EmailScrubber;

    invoke-direct {v1}, Lcom/squareup/text/EmailScrubber;-><init>()V

    invoke-direct {v0, v1, p0}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    return-object v0
.end method


# virtual methods
.method public scrub(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 14
    invoke-static {p2}, Lcom/squareup/text/Emails;->isValidOrPartial(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ne v1, v2, :cond_0

    return-object p2

    :cond_0
    if-eqz v0, :cond_1

    move-object p1, p2

    :cond_1
    return-object p1
.end method
