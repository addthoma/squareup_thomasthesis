.class public final Lcom/squareup/textappearance/SquareTextAppearance$Companion;
.super Ljava/lang/Object;
.source "SquareTextAppearance.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/textappearance/SquareTextAppearance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareTextAppearance.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareTextAppearance.kt\ncom/squareup/textappearance/SquareTextAppearance$Companion\n+ 2 StyledAttributes.kt\ncom/squareup/ui/internal/styles/StyledAttributesKt\n*L\n1#1,107:1\n38#2,6:108\n*E\n*S KotlinDebug\n*F\n+ 1 SquareTextAppearance.kt\ncom/squareup/textappearance/SquareTextAppearance$Companion\n*L\n57#1,6:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0008\u0008\u0001\u0010\t\u001a\u00020\nJ\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/textappearance/SquareTextAppearance$Companion;",
        "",
        "()V",
        "loadFromStyle",
        "Lcom/squareup/textappearance/SquareTextAppearance;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "appearanceId",
        "",
        "textappearance_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final loadFromStyle(Landroid/content/Context;I)Lcom/squareup/textappearance/SquareTextAppearance;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    move-object v0, p0

    check-cast v0, Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;->loadFromStyle(Landroid/content/Context;Landroid/util/AttributeSet;I)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p1

    return-object p1
.end method

.method public final loadFromStyle(Landroid/content/Context;Landroid/util/AttributeSet;I)Lcom/squareup/textappearance/SquareTextAppearance;
    .locals 11

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    sget-object v0, Lcom/squareup/textappearance/R$styleable;->ExtraTextAppearance:[I

    const-string v1, "R.styleable.ExtraTextAppearance"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 108
    invoke-virtual {p1, p2, v0, v1, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string p3, "a"

    .line 110
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    sget p3, Lcom/squareup/textappearance/R$styleable;->ExtraTextAppearance_sqFontSelection:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    if-eqz p3, :cond_0

    .line 64
    new-instance v0, Lcom/squareup/textappearance/FontSelection;

    invoke-direct {v0, p1, p3}, Lcom/squareup/textappearance/FontSelection;-><init>(Landroid/content/Context;I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v3, v0

    .line 67
    sget p1, Lcom/squareup/textappearance/R$styleable;->ExtraTextAppearance_android_textStyle:I

    invoke-virtual {p2, p1, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    .line 73
    sget p3, Lcom/squareup/textappearance/R$styleable;->ExtraTextAppearance_android_textSize:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    .line 76
    sget v0, Lcom/squareup/textappearance/R$styleable;->ExtraTextAppearance_android_fontFamily:I

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    if-eqz v9, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 79
    :goto_1
    new-instance v10, Lcom/squareup/textappearance/SquareTextAppearance;

    if-eqz v0, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    move v4, p1

    :goto_2
    int-to-float v5, p3

    .line 83
    sget p1, Lcom/squareup/textappearance/R$styleable;->ExtraTextAppearance_android_textColor:I

    invoke-virtual {p2, p1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    .line 84
    sget p1, Lcom/squareup/textappearance/R$styleable;->ExtraTextAppearance_android_textAllCaps:I

    invoke-virtual {p2, p1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    .line 85
    sget p1, Lcom/squareup/textappearance/R$styleable;->ExtraTextAppearance_android_letterSpacing:I

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v8

    move-object v2, v10

    .line 79
    invoke-direct/range {v2 .. v9}, Lcom/squareup/textappearance/SquareTextAppearance;-><init>(Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-object v10

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method
