.class public final Lcom/squareup/textappearance/SquareTextAppearanceKt;
.super Ljava/lang/Object;
.source "SquareTextAppearance.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareTextAppearance.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareTextAppearance.kt\ncom/squareup/textappearance/SquareTextAppearanceKt\n*L\n1#1,107:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "setTextAppearance",
        "",
        "Landroid/widget/TextView;",
        "textAppearance",
        "Lcom/squareup/textappearance/SquareTextAppearance;",
        "textappearance_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setTextAppearance(Landroid/widget/TextView;Lcom/squareup/textappearance/SquareTextAppearance;)V
    .locals 2

    const-string v0, "$this$setTextAppearance"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "textAppearance"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/textappearance/SquareTextAppearance;->getTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 95
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextSize()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 96
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextSize()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 99
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v0

    instance-of v0, v0, Landroidx/appcompat/text/AllCapsTransformationMethod;

    .line 100
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextAllCaps()Z

    move-result v1

    if-eq v0, v1, :cond_3

    .line 102
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextAllCaps()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroidx/appcompat/text/AllCapsTransformationMethod;

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/text/AllCapsTransformationMethod;-><init>(Landroid/content/Context;)V

    check-cast v0, Landroid/text/method/TransformationMethod;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 104
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getLetterSpacing()F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setLetterSpacing(F)V

    .line 105
    invoke-virtual {p1}, Lcom/squareup/textappearance/SquareTextAppearance;->getTextColor()Landroid/content/res/ColorStateList;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_4
    return-void
.end method
