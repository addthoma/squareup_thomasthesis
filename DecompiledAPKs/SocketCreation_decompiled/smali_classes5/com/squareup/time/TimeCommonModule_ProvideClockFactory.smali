.class public final Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;
.super Ljava/lang/Object;
.source "TimeCommonModule_ProvideClockFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lorg/threeten/bp/Clock;",
        ">;"
    }
.end annotation


# instance fields
.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;->currentTimeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;)",
            "Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;

    invoke-direct {v0, p0}, Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideClock(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/Clock;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/time/TimeCommonModule;->INSTANCE:Lcom/squareup/time/TimeCommonModule;

    invoke-virtual {v0, p0}, Lcom/squareup/time/TimeCommonModule;->provideClock(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/Clock;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/threeten/bp/Clock;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;->get()Lorg/threeten/bp/Clock;

    move-result-object v0

    return-object v0
.end method

.method public get()Lorg/threeten/bp/Clock;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/time/CurrentTime;

    invoke-static {v0}, Lcom/squareup/time/TimeCommonModule_ProvideClockFactory;->provideClock(Lcom/squareup/time/CurrentTime;)Lorg/threeten/bp/Clock;

    move-result-object v0

    return-object v0
.end method
