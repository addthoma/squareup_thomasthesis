.class public final Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;
.super Ljava/lang/Object;
.source "TimeCommonModule_ProvideZonedDateTimeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lorg/threeten/bp/ZonedDateTime;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lorg/threeten/bp/Clock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lorg/threeten/bp/Clock;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lorg/threeten/bp/Clock;",
            ">;)",
            "Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;

    invoke-direct {v0, p0}, Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideZonedDateTime(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/time/TimeCommonModule;->INSTANCE:Lcom/squareup/time/TimeCommonModule;

    invoke-virtual {v0, p0}, Lcom/squareup/time/TimeCommonModule;->provideZonedDateTime(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/threeten/bp/ZonedDateTime;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;->get()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method

.method public get()Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/Clock;

    invoke-static {v0}, Lcom/squareup/time/TimeCommonModule_ProvideZonedDateTimeFactory;->provideZonedDateTime(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0
.end method
