.class public final Lcom/squareup/time/RealTimeZoneFormatter;
.super Ljava/lang/Object;
.source "RealTimeZoneFormatter.kt"

# interfaces
.implements Lcom/squareup/time/TimeZoneFormatter;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTimeZoneFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTimeZoneFormatter.kt\ncom/squareup/time/RealTimeZoneFormatter\n*L\n1#1,61:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0010\u0010\u000f\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/time/RealTimeZoneFormatter;",
        "Lcom/squareup/time/TimeZoneFormatter;",
        "availableTimeZones",
        "Lcom/squareup/time/AvailableTimeZones;",
        "resources",
        "Landroid/content/res/Resources;",
        "(Lcom/squareup/time/AvailableTimeZones;Landroid/content/res/Resources;)V",
        "getDisplayName",
        "",
        "descriptor",
        "Lcom/squareup/time/TimeZoneDescriptor;",
        "timeZone",
        "Lorg/threeten/bp/ZoneId;",
        "getOffset",
        "",
        "getOffsetString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final availableTimeZones:Lcom/squareup/time/AvailableTimeZones;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/squareup/time/AvailableTimeZones;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "availableTimeZones"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/time/RealTimeZoneFormatter;->availableTimeZones:Lcom/squareup/time/AvailableTimeZones;

    iput-object p2, p0, Lcom/squareup/time/RealTimeZoneFormatter;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method private final getOffset(Lorg/threeten/bp/ZoneId;)I
    .locals 1

    .line 57
    invoke-virtual {p1}, Lorg/threeten/bp/ZoneId;->getRules()Lorg/threeten/bp/zone/ZoneRules;

    move-result-object p1

    invoke-static {}, Lorg/threeten/bp/Instant;->now()Lorg/threeten/bp/Instant;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/zone/ZoneRules;->getOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object p1

    const-string/jumbo v0, "timeZone.rules.getOffset(Instant.now())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/threeten/bp/ZoneOffset;->getTotalSeconds()I

    move-result p1

    return p1
.end method


# virtual methods
.method public getDisplayName(Lcom/squareup/time/TimeZoneDescriptor;)Ljava/lang/String;
    .locals 3

    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p1}, Lcom/squareup/time/TimeZoneDescriptor;->getTimeZone()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 26
    invoke-virtual {p1}, Lcom/squareup/time/TimeZoneDescriptor;->getNameId()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 27
    sget-object p1, Lorg/threeten/bp/format/TextStyle;->FULL_STANDALONE:Lorg/threeten/bp/format/TextStyle;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lorg/threeten/bp/ZoneId;->getDisplayName(Lorg/threeten/bp/format/TextStyle;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "zoneId.getDisplayName(FU\u2026ONE, Locale.getDefault())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 30
    :cond_0
    invoke-virtual {v0}, Lorg/threeten/bp/ZoneId;->getRules()Lorg/threeten/bp/zone/ZoneRules;

    move-result-object v0

    invoke-static {}, Lorg/threeten/bp/Instant;->now()Lorg/threeten/bp/Instant;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/zone/ZoneRules;->isDaylightSavings(Lorg/threeten/bp/Instant;)Z

    move-result v0

    .line 31
    iget-object v1, p0, Lcom/squareup/time/RealTimeZoneFormatter;->resources:Landroid/content/res/Resources;

    if-eqz v0, :cond_1

    .line 32
    sget v0, Lcom/squareup/time/R$string;->time_zone_daylight:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/squareup/time/R$string;->time_zone_standard:I

    .line 31
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(\n   \u2026.time_zone_standard\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v1, p0, Lcom/squareup/time/RealTimeZoneFormatter;->resources:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/squareup/time/TimeZoneDescriptor;->getNameId()I

    move-result p1

    invoke-static {v1, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 36
    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "in_daylight"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getDisplayName(Lorg/threeten/bp/ZoneId;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "timeZone"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/time/RealTimeZoneFormatter;->availableTimeZones:Lcom/squareup/time/AvailableTimeZones;

    invoke-virtual {v0, p1}, Lcom/squareup/time/AvailableTimeZones;->descriptor(Lorg/threeten/bp/ZoneId;)Lcom/squareup/time/TimeZoneDescriptor;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/time/RealTimeZoneFormatter;->getDisplayName(Lcom/squareup/time/TimeZoneDescriptor;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getOffsetString(Lcom/squareup/time/TimeZoneDescriptor;)Ljava/lang/String;
    .locals 5

    const-string v0, "descriptor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/squareup/time/TimeZoneDescriptor;->getTimeZone()Lorg/threeten/bp/ZoneId;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/time/RealTimeZoneFormatter;->getOffset(Lorg/threeten/bp/ZoneId;)I

    move-result p1

    .line 45
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 46
    div-int/lit8 v0, v0, 0x3c

    rem-int/lit8 v1, v0, 0x3c

    .line 47
    div-int/lit8 v0, v0, 0x3c

    .line 49
    iget-object v2, p0, Lcom/squareup/time/RealTimeZoneFormatter;->resources:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/time/R$string;->time_zone_offset:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    if-ltz p1, :cond_0

    const-string p1, "+"

    goto :goto_0

    :cond_0
    const-string p1, "-"

    .line 50
    :goto_0
    check-cast p1, Ljava/lang/CharSequence;

    const-string v3, "sign"

    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 51
    sget-object v2, Lkotlin/jvm/internal/StringCompanionObject;->INSTANCE:Lkotlin/jvm/internal/StringCompanionObject;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Locale.US"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    array-length v0, v3

    invoke-static {v3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%d:%02d"

    invoke-static {v2, v1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(locale, format, *args)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "offset"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
