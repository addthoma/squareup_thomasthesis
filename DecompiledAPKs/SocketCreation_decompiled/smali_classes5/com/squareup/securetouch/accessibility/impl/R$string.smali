.class public final Lcom/squareup/securetouch/accessibility/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accessible_keypad_clear_digits:I = 0x7f12002e

.field public static final accessible_keypad_error_too_few_digits:I = 0x7f12002f

.field public static final accessible_keypad_error_too_many_digits:I = 0x7f120030

.field public static final accessible_keypad_first_try_instructions:I = 0x7f120031

.field public static final accessible_keypad_first_try_title:I = 0x7f120032

.field public static final accessible_keypad_last_try_title:I = 0x7f120033

.field public static final accessible_keypad_multiple_digits_entered:I = 0x7f120034

.field public static final accessible_keypad_retry_title:I = 0x7f120035

.field public static final accessible_keypad_single_digit_entered:I = 0x7f120036

.field public static final digit_0:I = 0x7f12086b

.field public static final digit_1:I = 0x7f12086c

.field public static final digit_2:I = 0x7f12086d

.field public static final digit_3:I = 0x7f12086e

.field public static final digit_4:I = 0x7f12086f

.field public static final digit_5:I = 0x7f120870

.field public static final digit_6:I = 0x7f120871

.field public static final digit_7:I = 0x7f120872

.field public static final digit_8:I = 0x7f120873

.field public static final digit_9:I = 0x7f120874


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
