.class public final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;
.super Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KeypadCenter"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0018\u0010\u0016\u001a\u00020\u0017*\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;",
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;",
        "center",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "keypadType",
        "Lcom/squareup/securetouch/AccessibilityKeypadType;",
        "(Lcom/squareup/securetouch/SecureTouchPoint;Lcom/squareup/securetouch/AccessibilityKeypadType;)V",
        "getCenter",
        "()Lcom/squareup/securetouch/SecureTouchPoint;",
        "getKeypadType",
        "()Lcom/squareup/securetouch/AccessibilityKeypadType;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final center:Lcom/squareup/securetouch/SecureTouchPoint;

.field private final keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchPoint;Lcom/squareup/securetouch/AccessibilityKeypadType;)V
    .locals 1

    const-string v0, "center"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keypadType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 284
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;Lcom/squareup/securetouch/SecureTouchPoint;Lcom/squareup/securetouch/AccessibilityKeypadType;ILjava/lang/Object;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->copy(Lcom/squareup/securetouch/SecureTouchPoint;Lcom/squareup/securetouch/AccessibilityKeypadType;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    sget-object v1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/securetouch/AccessibilityKeypadType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 292
    new-instance v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;

    .line 293
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    .line 294
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    invoke-virtual {v2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getDigitsEntered()I

    move-result v2

    .line 295
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    invoke-virtual {v3}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v3

    .line 292
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;ILcom/squareup/securetouch/SecureTouchPinEntryState;)V

    check-cast v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 287
    :cond_1
    new-instance v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitZero;

    .line 288
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    .line 289
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    invoke-virtual {v2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getDigitsEntered()I

    move-result v2

    .line 290
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    invoke-virtual {v3}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v3

    .line 287
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitZero;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;ILcom/squareup/securetouch/SecureTouchPinEntryState;)V

    check-cast v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;

    .line 286
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    return-object v0
.end method

.method public final component2()Lcom/squareup/securetouch/AccessibilityKeypadType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    return-object v0
.end method

.method public final copy(Lcom/squareup/securetouch/SecureTouchPoint;Lcom/squareup/securetouch/AccessibilityKeypadType;)Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;
    .locals 1

    const-string v0, "center"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keypadType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;

    invoke-direct {v0, p1, p2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;Lcom/squareup/securetouch/AccessibilityKeypadType;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    iget-object v1, p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    iget-object p1, p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCenter()Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    return-object v0
.end method

.method public final getKeypadType()Lcom/squareup/securetouch/AccessibilityKeypadType;
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeypadCenter(center="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", keypadType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$KeypadCenter;->keypadType:Lcom/squareup/securetouch/AccessibilityKeypadType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
