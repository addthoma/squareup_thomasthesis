.class public final Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;
.super Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;
.source "SecureTouchAccessibilityPinEntryState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowingDigitsOneThroughNine"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;",
        "center",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "digitsEntered",
        "",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "(Lcom/squareup/securetouch/SecureTouchPoint;ILcom/squareup/securetouch/SecureTouchPinEntryState;)V",
        "getCenter",
        "()Lcom/squareup/securetouch/SecureTouchPoint;",
        "getDigitsEntered",
        "()I",
        "getPinEntryState",
        "()Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final center:Lcom/squareup/securetouch/SecureTouchPoint;

.field private final digitsEntered:I

.field private final pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchPoint;ILcom/squareup/securetouch/SecureTouchPinEntryState;)V
    .locals 1

    const-string v0, "center"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinEntryState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    iput p2, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->digitsEntered:I

    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;Lcom/squareup/securetouch/SecureTouchPoint;ILcom/squareup/securetouch/SecureTouchPinEntryState;ILjava/lang/Object;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getDigitsEntered()I

    move-result p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p3

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->copy(Lcom/squareup/securetouch/SecureTouchPoint;ILcom/squareup/securetouch/SecureTouchPinEntryState;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getDigitsEntered()I

    move-result v0

    return v0
.end method

.method public final component3()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/securetouch/SecureTouchPoint;ILcom/squareup/securetouch/SecureTouchPinEntryState;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;
    .locals 1

    const-string v0, "center"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinEntryState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;ILcom/squareup/securetouch/SecureTouchPinEntryState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    iget-object v1, p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getDigitsEntered()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getDigitsEntered()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCenter()Lcom/squareup/securetouch/SecureTouchPoint;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    return-object v0
.end method

.method public getDigitsEntered()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->digitsEntered:I

    return v0
.end method

.method public getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getDigitsEntered()I

    move-result v2

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowingDigitsOneThroughNine(center="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->center:Lcom/squareup/securetouch/SecureTouchPoint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", digitsEntered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getDigitsEntered()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", pinEntryState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState$ShowingDigitsOneThroughNine;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
