.class final Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;
.super Ljava/lang/Object;
.source "SecureTouchCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/SecureTouchCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/workflow/legacy/Screen;",
        "+",
        "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012(\u0010\u0002\u001a$\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u0012\u0004\u0012\u00020\u00080\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/SecureTouchScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;

    invoke-direct {v0}, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;->INSTANCE:Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 43
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/SecureTouchCoordinator$attach$6;->test(Lkotlin/Pair;)Z

    move-result p1

    return p1
.end method

.method public final test(Lkotlin/Pair;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/workflow/legacy/Screen;",
            "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 116
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/securetouch/SecureTouchScreen;

    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchScreen;->getScreenData()Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->getWaitingForKeypadLayout()Z

    move-result p1

    return p1
.end method
