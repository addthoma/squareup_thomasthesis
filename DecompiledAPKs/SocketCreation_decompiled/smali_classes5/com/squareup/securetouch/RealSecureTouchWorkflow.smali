.class public final Lcom/squareup/securetouch/RealSecureTouchWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSecureTouchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/securetouch/SecureTouchInput;",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSecureTouchWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSecureTouchWorkflow.kt\ncom/squareup/securetouch/RealSecureTouchWorkflow\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,386:1\n335#1,4:387\n337#1:391\n339#1,2:397\n335#1,4:399\n337#1:403\n339#1,2:409\n335#1,4:411\n337#1:415\n339#1,2:421\n335#1,4:427\n337#1:431\n339#1,2:437\n335#1,4:439\n337#1:443\n339#1,2:449\n19#2:392\n19#2:404\n19#2:416\n19#2:432\n19#2:444\n19#2:481\n41#3:393\n56#3,2:394\n41#3:405\n56#3,2:406\n41#3:417\n56#3,2:418\n41#3:423\n56#3,2:424\n41#3:433\n56#3,2:434\n41#3:445\n56#3,2:446\n41#3:482\n56#3,2:483\n276#4:396\n276#4:408\n276#4:420\n276#4:426\n276#4:436\n276#4:448\n276#4:485\n149#5,5:451\n149#5,5:456\n149#5,5:461\n149#5,5:466\n149#5,5:471\n149#5,5:476\n*E\n*S KotlinDebug\n*F\n+ 1 RealSecureTouchWorkflow.kt\ncom/squareup/securetouch/RealSecureTouchWorkflow\n*L\n108#1,4:387\n108#1:391\n108#1,2:397\n112#1,4:399\n112#1:403\n112#1,2:409\n118#1,4:411\n118#1:415\n118#1,2:421\n154#1,4:427\n154#1:431\n154#1,2:437\n160#1,4:439\n160#1:443\n160#1,2:449\n108#1:392\n112#1:404\n118#1:416\n154#1:432\n160#1:444\n337#1:481\n108#1:393\n108#1,2:394\n112#1:405\n112#1,2:406\n118#1:417\n118#1,2:418\n138#1:423\n138#1,2:424\n154#1:433\n154#1,2:434\n160#1:445\n160#1,2:446\n338#1:482\n338#1,2:483\n108#1:396\n112#1:408\n118#1:420\n138#1:426\n154#1:436\n160#1:448\n338#1:485\n210#1,5:451\n228#1,5:456\n279#1,5:461\n295#1,5:466\n313#1,5:471\n328#1,5:476\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001BM\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0002\u0010\u001bJ:\u0010\"\u001a\u0018\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020$0\u0007j\u0008\u0012\u0004\u0012\u00020#`%2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\'2\u0006\u0010(\u001a\u00020)H\u0002JF\u0010*\u001a$\u0012\u0004\u0012\u00020+\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020+`\t2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\'2\u0006\u0010(\u001a\u00020,H\u0002J\u001a\u0010-\u001a\u00020\u00032\u0006\u0010.\u001a\u00020\u00022\u0008\u0010/\u001a\u0004\u0018\u000100H\u0016JN\u00101\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010.\u001a\u00020\u00022\u0006\u00102\u001a\u00020\u00032\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\'H\u0016J:\u00103\u001a\u0018\u0012\u0004\u0012\u000204\u0012\u0004\u0012\u00020$0\u0007j\u0008\u0012\u0004\u0012\u000204`%2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\'2\u0006\u0010(\u001a\u000205H\u0002J\u0010\u00106\u001a\u0002002\u0006\u00102\u001a\u00020\u0003H\u0016J&\u00107\u001a\u0018\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020$0\u0007j\u0008\u0012\u0004\u0012\u00020#`%2\u0006\u0010(\u001a\u00020\u0003H\u0002J:\u00108\u001a\u0018\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020$0\u0007j\u0008\u0012\u0004\u0012\u00020#`%2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\'2\u0006\u0010(\u001a\u000209H\u0002J\u0014\u0010:\u001a\u00020\u0003*\u0002092\u0006\u0010;\u001a\u00020<H\u0002J\u000c\u0010=\u001a\u00020\u0003*\u000209H\u0002JV\u0010>\u001a\u00020?\"\n\u0008\u0000\u0010@\u0018\u0001*\u00020A*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\'2/\u0008\u0004\u0010B\u001a)\u0012\u0013\u0012\u0011H@\u00a2\u0006\u000c\u0008D\u0012\u0008\u0008E\u0012\u0004\u0008\u0008(;\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040F0CH\u0082\u0008J\u000c\u0010G\u001a\u00020H*\u00020\u0002H\u0002JN\u0010I\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t\"\u0008\u0008\u0000\u0010@*\u00020J*\u0018\u0012\u0004\u0012\u0002H@\u0012\u0004\u0012\u00020$0\u0007j\u0008\u0012\u0004\u0012\u0002H@`%H\u0002JP\u0010K\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t*$\u0012\u0004\u0012\u00020+\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020+`\tH\u0002R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001c\u001a\n \u001e*\u0004\u0018\u00010\u001d0\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006L"
    }
    d2 = {
        "Lcom/squareup/securetouch/RealSecureTouchWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/securetouch/SecureTouchInput;",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "cardreader",
        "Lcom/squareup/securetouch/SecureTouchFeature;",
        "secureTouchAccessibilityWorkflow",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "accessibilitySettings",
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "restoreAccessibilitySettingsLifecycleWorker",
        "Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;",
        "headsetConnectionListener",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
        "(Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/accessibility/AccessibilitySettings;Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V",
        "forceTalkBackOn",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "headphoneConnectionState",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
        "awaitingKeypadScreen",
        "Lcom/squareup/securetouch/SecureTouchScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "currentState",
        "Lcom/squareup/securetouch/AwaitingKeypad;",
        "enableAccessibleKeypadDialog",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "secureTouchModeSelectionScreen",
        "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;",
        "Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;",
        "snapshotState",
        "spinnerScreen",
        "usingKeypadScreen",
        "Lcom/squareup/securetouch/UsingKeypad;",
        "afterTouchOn",
        "event",
        "Lcom/squareup/securetouch/SecureTouchKeyPressEvent;",
        "onKeypadAccessibilityPressed",
        "onReaderOutput",
        "",
        "T",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "handler",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "Lcom/squareup/workflow/WorkflowAction;",
        "shouldBeInSecureTouchAccessibilityMode",
        "",
        "toOnlyPosScreen",
        "",
        "toPosLayering",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

.field private final cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final forceTalkBackOn:Lio/reactivex/Completable;

.field private final headphoneConnectionState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final restoreAccessibilitySettingsLifecycleWorker:Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;

.field private final secureTouchAccessibilityWorkflow:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/accessibility/AccessibilitySettings;Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/securetouch/SecureTouchFeature;",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/accessibility/AccessibilitySettings;",
            "Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardreader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "secureTouchAccessibilityWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilitySettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restoreAccessibilitySettingsLifecycleWorker"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "headsetConnectionListener"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    iput-object p2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->secureTouchAccessibilityWorkflow:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;

    iput-object p3, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p5, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    iput-object p7, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->restoreAccessibilitySettingsLifecycleWorker:Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;

    .line 64
    invoke-interface {p8}, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;->onHeadsetStateChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->headphoneConnectionState:Lio/reactivex/Observable;

    .line 65
    iget-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    invoke-virtual {p1}, Lcom/squareup/accessibility/AccessibilitySettings;->getTalkBackState()Lio/reactivex/Observable;

    move-result-object p1

    .line 66
    sget-object p2, Lcom/squareup/securetouch/RealSecureTouchWorkflow$forceTalkBackOn$1;->INSTANCE:Lcom/squareup/securetouch/RealSecureTouchWorkflow$forceTalkBackOn$1;

    check-cast p2, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 67
    new-instance p2, Lcom/squareup/securetouch/RealSecureTouchWorkflow$forceTalkBackOn$2;

    invoke-direct {p2, p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$forceTalkBackOn$2;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->flatMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->forceTalkBackOn:Lio/reactivex/Completable;

    return-void
.end method

.method public static final synthetic access$afterTouchOn(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/UsingKeypad;Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)Lcom/squareup/securetouch/SecureTouchState;
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->afterTouchOn(Lcom/squareup/securetouch/UsingKeypad;Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)Lcom/squareup/securetouch/SecureTouchState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAccessibilitySettings$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/accessibility/AccessibilitySettings;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    return-object p0
.end method

.method public static final synthetic access$getCardreader$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->cardreader:Lcom/squareup/securetouch/SecureTouchFeature;

    return-object p0
.end method

.method private final afterTouchOn(Lcom/squareup/securetouch/UsingKeypad;Lcom/squareup/securetouch/SecureTouchKeyPressEvent;)Lcom/squareup/securetouch/SecureTouchState;
    .locals 3

    .line 344
    sget-object v0, Lcom/squareup/securetouch/KeypadDeletePressed;->INSTANCE:Lcom/squareup/securetouch/KeypadDeletePressed;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/securetouch/UsingKeypad;->decrement()Lcom/squareup/securetouch/UsingKeypad;

    move-result-object p1

    check-cast p1, Lcom/squareup/securetouch/SecureTouchState;

    goto :goto_0

    .line 345
    :cond_0
    sget-object v0, Lcom/squareup/securetouch/KeypadDigitPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadDigitPressed;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/securetouch/UsingKeypad;->increment()Lcom/squareup/securetouch/UsingKeypad;

    move-result-object p1

    check-cast p1, Lcom/squareup/securetouch/SecureTouchState;

    goto :goto_0

    .line 346
    :cond_1
    instance-of v0, p2, Lcom/squareup/securetouch/SecureTouchUserCancelled;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 347
    new-instance p1, Lcom/squareup/securetouch/DisablingKeypad;

    sget-object p2, Lcom/squareup/securetouch/RealSecureTouchWorkflow$afterTouchOn$1;->INSTANCE:Lcom/squareup/securetouch/RealSecureTouchWorkflow$afterTouchOn$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p2, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/securetouch/DisablingKeypad;-><init>(Lcom/squareup/workflow/WorkflowAction;)V

    check-cast p1, Lcom/squareup/securetouch/SecureTouchState;

    goto :goto_0

    .line 349
    :cond_2
    sget-object v0, Lcom/squareup/securetouch/SecureTouchUserDone;->INSTANCE:Lcom/squareup/securetouch/SecureTouchUserDone;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 350
    new-instance p1, Lcom/squareup/securetouch/DisablingKeypad;

    sget-object p2, Lcom/squareup/securetouch/RealSecureTouchWorkflow$afterTouchOn$2;->INSTANCE:Lcom/squareup/securetouch/RealSecureTouchWorkflow$afterTouchOn$2;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, p2, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/securetouch/DisablingKeypad;-><init>(Lcom/squareup/workflow/WorkflowAction;)V

    check-cast p1, Lcom/squareup/securetouch/SecureTouchState;

    goto :goto_0

    .line 352
    :cond_3
    sget-object v0, Lcom/squareup/securetouch/KeypadAccessibilityPressed;->INSTANCE:Lcom/squareup/securetouch/KeypadAccessibilityPressed;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->onKeypadAccessibilityPressed(Lcom/squareup/securetouch/UsingKeypad;)Lcom/squareup/securetouch/SecureTouchState;

    move-result-object p1

    goto :goto_0

    .line 353
    :cond_4
    instance-of v0, p2, Lcom/squareup/securetouch/KeypadInvalidAction;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/squareup/securetouch/SecureTouchState;

    :goto_0
    return-object p1

    .line 354
    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No transition for "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method private final awaitingKeypadScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/AwaitingKeypad;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;",
            "Lcom/squareup/securetouch/AwaitingKeypad;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 196
    new-instance v0, Lcom/squareup/securetouch/SecureTouchScreen;

    .line 197
    sget-object v1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->Companion:Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;

    move-object v2, p2

    check-cast v2, Lcom/squareup/securetouch/SecureTouchState;

    invoke-virtual {v1, v2}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;->fromSecureTouchState(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    move-result-object v1

    .line 198
    new-instance v2, Lcom/squareup/securetouch/RealSecureTouchWorkflow$awaitingKeypadScreen$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$awaitingKeypadScreen$1;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/AwaitingKeypad;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 205
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$awaitingKeypadScreen$2;

    invoke-direct {p1, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$awaitingKeypadScreen$2;-><init>(Lcom/squareup/securetouch/AwaitingKeypad;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    .line 196
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/securetouch/SecureTouchScreen;-><init>(Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 452
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 453
    const-class p2, Lcom/squareup/securetouch/SecureTouchScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 454
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 452
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final enableAccessibleKeypadDialog(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;)Ljava/util/Map;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;",
            "Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p2

    .line 286
    sget-object v1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 287
    new-instance v2, Lcom/squareup/securetouch/SecureTouchScreen;

    .line 288
    new-instance v14, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xff

    const/4 v13, 0x0

    move-object v3, v14

    invoke-direct/range {v3 .. v13}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;-><init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 289
    new-instance v3, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$1;

    invoke-direct {v3, v0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$1;-><init>(Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 290
    new-instance v4, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$2;

    invoke-direct {v4, v0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$2;-><init>(Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 287
    invoke-direct {v2, v14, v3, v4}, Lcom/squareup/securetouch/SecureTouchScreen;-><init>(Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 467
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 468
    const-class v4, Lcom/squareup/securetouch/SecureTouchScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    const-string v5, ""

    invoke-static {v4, v5}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 469
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 467
    invoke-direct {v3, v4, v2, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 296
    new-instance v2, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;

    .line 297
    new-instance v4, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;

    move-object v6, p0

    move-object/from16 v7, p1

    invoke-direct {v4, p0, v7, v0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$enableAccessibleKeypadDialog$3;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 296
    invoke-direct {v2, v4}, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 472
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 473
    const-class v4, Lcom/squareup/securetouch/EnableAccessibleKeypadDialogScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v5}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 474
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 472
    invoke-direct {v0, v4, v2, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 286
    invoke-virtual {v1, v3, v0}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private final onKeypadAccessibilityPressed(Lcom/squareup/securetouch/UsingKeypad;)Lcom/squareup/securetouch/SecureTouchState;
    .locals 4

    .line 359
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 360
    new-instance v0, Lcom/squareup/securetouch/DisablingKeypad;

    .line 361
    new-instance v3, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/UsingKeypad;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v3, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 360
    invoke-direct {v0, p1}, Lcom/squareup/securetouch/DisablingKeypad;-><init>(Lcom/squareup/workflow/WorkflowAction;)V

    check-cast v0, Lcom/squareup/securetouch/SecureTouchState;

    goto :goto_0

    .line 370
    :cond_0
    new-instance v0, Lcom/squareup/securetouch/DisablingKeypad;

    .line 371
    new-instance v3, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$2;

    invoke-direct {v3, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$2;-><init>(Lcom/squareup/securetouch/UsingKeypad;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v2, v3, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 370
    invoke-direct {v0, p1}, Lcom/squareup/securetouch/DisablingKeypad;-><init>(Lcom/squareup/workflow/WorkflowAction;)V

    check-cast v0, Lcom/squareup/securetouch/SecureTouchState;

    :goto_0
    return-object v0
.end method

.method private final synthetic onReaderOutput(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "+",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;>;)V"
        }
    .end annotation

    .line 336
    invoke-static {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "T"

    const/4 v2, 0x4

    .line 481
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v2, "ofType(T::class.java)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 482
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v2, "this.toFlowable(BUFFER)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_0

    .line 484
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    const/4 v2, 0x6

    .line 485
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const/4 v1, 0x0

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 339
    new-instance v0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onReaderOutput$1;

    invoke-direct {v0, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onReaderOutput$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p1

    .line 335
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    .line 484
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final secureTouchModeSelectionScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;)Lcom/squareup/workflow/legacy/Screen;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;",
            "Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 235
    new-instance v0, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;

    .line 236
    invoke-virtual {p2}, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->getUseHighContrastMode()Z

    move-result v1

    .line 237
    iget-object v2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "moneyFormatter.format(transaction.amountDue)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    new-instance v3, Lcom/squareup/securetouch/RealSecureTouchWorkflow$secureTouchModeSelectionScreen$1;

    invoke-direct {v3, p0, p1, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$secureTouchModeSelectionScreen$1;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 235
    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;-><init>(Ljava/lang/CharSequence;ZLkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 462
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 463
    const-class p2, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 464
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 462
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final shouldBeInSecureTouchAccessibilityMode(Lcom/squareup/securetouch/SecureTouchInput;)Z
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 189
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchInput;->getSecureTouchMode()Lcom/squareup/securetouch/SecureTouchMode;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/securetouch/SecureTouchMode$AccessibilityMode;

    return p1
.end method

.method private final spinnerScreen(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/workflow/legacy/Screen;
    .locals 4

    .line 320
    new-instance v0, Lcom/squareup/securetouch/SecureTouchScreen;

    .line 321
    sget-object v1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->Companion:Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;->fromSecureTouchState(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    move-result-object v1

    .line 322
    new-instance v2, Lcom/squareup/securetouch/RealSecureTouchWorkflow$spinnerScreen$1;

    invoke-direct {v2, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$spinnerScreen$1;-><init>(Lcom/squareup/securetouch/SecureTouchState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 323
    new-instance v3, Lcom/squareup/securetouch/RealSecureTouchWorkflow$spinnerScreen$2;

    invoke-direct {v3, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$spinnerScreen$2;-><init>(Lcom/squareup/securetouch/SecureTouchState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 320
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/securetouch/SecureTouchScreen;-><init>(Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 477
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 478
    const-class v1, Lcom/squareup/securetouch/SecureTouchScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 479
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 477
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final toOnlyPosScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Screen;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 379
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->toPosLayering(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final toPosLayering(Ljava/util/Map;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 383
    sget-object v0, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    sget-object v1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final usingKeypadScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/UsingKeypad;)Lcom/squareup/workflow/legacy/Screen;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;",
            "Lcom/squareup/securetouch/UsingKeypad;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 217
    new-instance v0, Lcom/squareup/securetouch/SecureTouchScreen;

    .line 218
    sget-object v1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->Companion:Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;

    move-object v2, p2

    check-cast v2, Lcom/squareup/securetouch/SecureTouchState;

    invoke-virtual {v1, v2}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;->fromSecureTouchState(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    move-result-object v1

    .line 219
    new-instance v2, Lcom/squareup/securetouch/RealSecureTouchWorkflow$usingKeypadScreen$1;

    invoke-direct {v2, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$usingKeypadScreen$1;-><init>(Lcom/squareup/securetouch/UsingKeypad;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 221
    new-instance v3, Lcom/squareup/securetouch/RealSecureTouchWorkflow$usingKeypadScreen$2;

    invoke-direct {v3, p0, p1, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$usingKeypadScreen$2;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/UsingKeypad;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 217
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/securetouch/SecureTouchScreen;-><init>(Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 457
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 458
    const-class p2, Lcom/squareup/securetouch/SecureTouchScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 459
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 457
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/securetouch/SecureTouchInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/securetouch/SecureTouchState;
    .locals 1

    const-string p2, "input"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->shouldBeInSecureTouchAccessibilityMode(Lcom/squareup/securetouch/SecureTouchInput;)Z

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 78
    new-instance p1, Lcom/squareup/securetouch/LaunchSecureTouchAccessibilityFlow;

    invoke-direct {p1, v0}, Lcom/squareup/securetouch/LaunchSecureTouchAccessibilityFlow;-><init>(Z)V

    check-cast p1, Lcom/squareup/securetouch/SecureTouchState;

    goto :goto_0

    .line 80
    :cond_0
    new-instance p2, Lcom/squareup/securetouch/AwaitingKeypad;

    .line 81
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchInput;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    .line 80
    invoke-direct {p2, p1, v0}, Lcom/squareup/securetouch/AwaitingKeypad;-><init>(Lcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    move-object p1, p2

    check-cast p1, Lcom/squareup/securetouch/SecureTouchState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/securetouch/SecureTouchInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->initialState(Lcom/squareup/securetouch/SecureTouchInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/securetouch/SecureTouchState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/securetouch/SecureTouchInput;

    check-cast p2, Lcom/squareup/securetouch/SecureTouchState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->render(Lcom/squareup/securetouch/SecureTouchInput;Lcom/squareup/securetouch/SecureTouchState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/securetouch/SecureTouchInput;Lcom/squareup/securetouch/SecureTouchState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/securetouch/SecureTouchInput;",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    instance-of v0, p2, Lcom/squareup/securetouch/AwaitingKeypad;

    if-eqz v0, :cond_0

    .line 97
    check-cast p2, Lcom/squareup/securetouch/AwaitingKeypad;

    invoke-direct {p0, p3, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->awaitingKeypadScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/AwaitingKeypad;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->toOnlyPosScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 101
    :cond_0
    instance-of v0, p2, Lcom/squareup/securetouch/UsingKeypad;

    const-string v1, "ofType(T::class.java)"

    const-string v2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    const-string/jumbo v3, "this.toFlowable(BUFFER)"

    if-eqz v0, :cond_4

    .line 388
    invoke-static {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object p1

    .line 392
    const-class v0, Lcom/squareup/securetouch/SecureTouchDisabled;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 393
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_3

    .line 395
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 396
    const-class v0, Lcom/squareup/securetouch/SecureTouchDisabled;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v6, v4

    check-cast v6, Lcom/squareup/workflow/Worker;

    const/4 v7, 0x0

    .line 397
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$1;

    invoke-direct {p1, p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$1;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)V

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v5, p3

    .line 387
    invoke-static/range {v5 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 400
    invoke-static {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object p1

    .line 404
    const-class v0, Lcom/squareup/securetouch/SecureTouchFailure;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 405
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_2

    .line 407
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 408
    const-class v0, Lcom/squareup/securetouch/SecureTouchFailure;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v4, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v4, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v6, v4

    check-cast v6, Lcom/squareup/workflow/Worker;

    const/4 v7, 0x0

    .line 409
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$2;

    invoke-direct {p1, p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$2;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)V

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v5, p3

    .line 399
    invoke-static/range {v5 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 412
    invoke-static {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object p1

    .line 416
    const-class v0, Lcom/squareup/securetouch/SecureTouchKeyPressEvent;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 417
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_1

    .line 419
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 420
    const-class v0, Lcom/squareup/securetouch/SecureTouchKeyPressEvent;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 421
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;

    invoke-direct {p1, p0, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$3;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/SecureTouchState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 411
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 125
    check-cast p2, Lcom/squareup/securetouch/UsingKeypad;

    invoke-direct {p0, p3, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->usingKeypadScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/UsingKeypad;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->toOnlyPosScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 419
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 407
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 395
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 128
    :cond_4
    instance-of v0, p2, Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;

    if-eqz v0, :cond_5

    .line 129
    check-cast p2, Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;

    invoke-direct {p0, p3, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->enableAccessibleKeypadDialog(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/ShowingEnableAccessibleKeypadDialog;)Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->toPosLayering(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 132
    :cond_5
    instance-of v0, p2, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    if-eqz v0, :cond_7

    .line 133
    iget-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->forceTalkBackOn:Lio/reactivex/Completable;

    const-string v0, "forceTalkBackOn"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/workflow/rx2/RxWorkersKt;->asWorker(Lio/reactivex/Completable;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p3, p1, v1, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 138
    iget-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->headphoneConnectionState:Lio/reactivex/Observable;

    .line 423
    sget-object v4, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v4}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_6

    .line 425
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 426
    const-class v2, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 138
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4;

    invoke-direct {p1, p0, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$4;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/SecureTouchState;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->restoreAccessibilitySettingsLifecycleWorker:Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;

    check-cast p2, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    invoke-virtual {p2}, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->getSetTalkBackEnabledOnFinish()Z

    move-result v2

    invoke-interface {p1, v2}, Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;->create(Z)Lcom/squareup/workflow/LifecycleWorker;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/Worker;

    .line 146
    invoke-static {p3, p1, v1, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 150
    invoke-direct {p0, p3, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->secureTouchModeSelectionScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->toOnlyPosScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 425
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 153
    :cond_7
    instance-of v0, p2, Lcom/squareup/securetouch/DisablingKeypad;

    if-eqz v0, :cond_9

    .line 428
    invoke-static {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object p1

    .line 432
    const-class v0, Lcom/squareup/securetouch/SecureTouchDisabled;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 433
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_8

    .line 435
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 436
    const-class v0, Lcom/squareup/securetouch/SecureTouchDisabled;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 437
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$4;

    invoke-direct {p1, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$4;-><init>(Lcom/squareup/securetouch/SecureTouchState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 427
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 156
    invoke-direct {p0, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->spinnerScreen(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->toOnlyPosScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 435
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 159
    :cond_9
    instance-of v0, p2, Lcom/squareup/securetouch/EnablingKeypad;

    if-eqz v0, :cond_b

    .line 440
    invoke-static {p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object p1

    .line 444
    const-class v0, Lcom/squareup/securetouch/SecureTouchEnabled;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 445
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_a

    .line 447
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 448
    const-class v0, Lcom/squareup/securetouch/SecureTouchEnabled;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 449
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$5;

    invoke-direct {p1, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$5;-><init>(Lcom/squareup/securetouch/SecureTouchState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 439
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 162
    invoke-direct {p0, p2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->spinnerScreen(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->toOnlyPosScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 447
    :cond_a
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 165
    :cond_b
    instance-of v0, p2, Lcom/squareup/securetouch/LaunchSecureTouchAccessibilityFlow;

    if-eqz v0, :cond_c

    .line 166
    iget-object v0, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->secureTouchAccessibilityWorkflow:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 167
    new-instance v3, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;

    .line 168
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchInput;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object p1

    .line 169
    check-cast p2, Lcom/squareup/securetouch/LaunchSecureTouchAccessibilityFlow;

    invoke-virtual {p2}, Lcom/squareup/securetouch/LaunchSecureTouchAccessibilityFlow;->getLaunchedFromDialog()Z

    move-result p2

    .line 167
    invoke-direct {v3, p1, p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;-><init>(Lcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    const/4 v4, 0x0

    .line 171
    new-instance p1, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$7;

    invoke-direct {p1, p0}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$screen$7;-><init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 165
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    const/4 p2, 0x0

    .line 176
    invoke-static {p1, p2}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 165
    :cond_c
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/securetouch/SecureTouchState;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->snapshotState(Lcom/squareup/securetouch/SecureTouchState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
