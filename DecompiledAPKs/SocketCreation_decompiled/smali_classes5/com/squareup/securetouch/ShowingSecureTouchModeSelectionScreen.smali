.class public final Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;
.super Lcom/squareup/securetouch/SecureTouchState;
.source "SecureTouchState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000b\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "setTalkBackEnabledOnFinish",
        "",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "useHighContrastMode",
        "(ZLcom/squareup/securetouch/SecureTouchPinEntryState;Z)V",
        "getPinEntryState",
        "()Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "getSetTalkBackEnabledOnFinish",
        "()Z",
        "setSetTalkBackEnabledOnFinish",
        "(Z)V",
        "getUseHighContrastMode",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

.field private setTalkBackEnabledOnFinish:Z

.field private final useHighContrastMode:Z


# direct methods
.method public constructor <init>(ZLcom/squareup/securetouch/SecureTouchPinEntryState;Z)V
    .locals 1

    const-string v0, "pinEntryState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 65
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/SecureTouchState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    iput-object p2, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    iput-boolean p3, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->useHighContrastMode:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/squareup/securetouch/SecureTouchPinEntryState;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    .line 63
    sget-object p2, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    check-cast p2, Lcom/squareup/securetouch/SecureTouchPinEntryState;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;-><init>(ZLcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;ZLcom/squareup/securetouch/SecureTouchPinEntryState;ZILjava/lang/Object;)Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->useHighContrastMode:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->copy(ZLcom/squareup/securetouch/SecureTouchPinEntryState;Z)Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    return v0
.end method

.method public final component2()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->useHighContrastMode:Z

    return v0
.end method

.method public final copy(ZLcom/squareup/securetouch/SecureTouchPinEntryState;Z)Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;
    .locals 1

    const-string v0, "pinEntryState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;-><init>(ZLcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    iget-boolean v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    iget-boolean v1, p1, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    iget-object v1, p1, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->useHighContrastMode:Z

    iget-boolean p1, p1, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->useHighContrastMode:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public final getSetTalkBackEnabledOnFinish()Z
    .locals 1

    .line 62
    iget-boolean v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    return v0
.end method

.method public final getUseHighContrastMode()Z
    .locals 1

    .line 64
    iget-boolean v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->useHighContrastMode:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->useHighContrastMode:Z

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final setSetTalkBackEnabledOnFinish(Z)V
    .locals 0

    .line 62
    iput-boolean p1, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowingSecureTouchModeSelectionScreen(setTalkBackEnabledOnFinish="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->setTalkBackEnabledOnFinish:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", pinEntryState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", useHighContrastMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;->useHighContrastMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
