.class public final Lcom/squareup/securetouch/NoTouchReporting;
.super Ljava/lang/Object;
.source "TouchReporting.kt"

# interfaces
.implements Lcom/squareup/securetouch/TouchReporting;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTouchReporting.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TouchReporting.kt\ncom/squareup/securetouch/NoTouchReporting\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,61:1\n151#2,2:62\n*E\n*S KotlinDebug\n*F\n+ 1 TouchReporting.kt\ncom/squareup/securetouch/NoTouchReporting\n*L\n60#1,2:62\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000f\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\u0096\u0001J\u000f\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0004H\u0096\u0001\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/securetouch/NoTouchReporting;",
        "Lcom/squareup/securetouch/TouchReporting;",
        "()V",
        "disableTouchReporting",
        "Lio/reactivex/Single;",
        "Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingDisabled;",
        "enableTouchReporting",
        "Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingEnabled;",
        "secure-touch-reporting_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/securetouch/NoTouchReporting;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/securetouch/TouchReporting;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 60
    new-instance v0, Lcom/squareup/securetouch/NoTouchReporting;

    invoke-direct {v0}, Lcom/squareup/securetouch/NoTouchReporting;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/NoTouchReporting;->INSTANCE:Lcom/squareup/securetouch/NoTouchReporting;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 62
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 63
    const-class v2, Lcom/squareup/securetouch/TouchReporting;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/securetouch/TouchReporting;

    iput-object v0, p0, Lcom/squareup/securetouch/NoTouchReporting;->$$delegate_0:Lcom/squareup/securetouch/TouchReporting;

    return-void
.end method


# virtual methods
.method public disableTouchReporting()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingDisabled;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/securetouch/NoTouchReporting;->$$delegate_0:Lcom/squareup/securetouch/TouchReporting;

    invoke-interface {v0}, Lcom/squareup/securetouch/TouchReporting;->disableTouchReporting()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public enableTouchReporting()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/securetouch/TouchReportingEvent$TouchReportingEnabled;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/securetouch/NoTouchReporting;->$$delegate_0:Lcom/squareup/securetouch/TouchReporting;

    invoke-interface {v0}, Lcom/squareup/securetouch/TouchReporting;->enableTouchReporting()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
