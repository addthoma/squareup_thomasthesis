.class public final Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SecureTouchModeSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSecureTouchModeSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SecureTouchModeSelectionCoordinator.kt\ncom/squareup/securetouch/SecureTouchModeSelectionCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,98:1\n1103#2,7:99\n1103#2,7:106\n1103#2,7:113\n1103#2,7:120\n*E\n*S KotlinDebug\n*F\n+ 1 SecureTouchModeSelectionCoordinator.kt\ncom/squareup/securetouch/SecureTouchModeSelectionCoordinator\n*L\n71#1,7:99\n74#1,7:106\n77#1,7:113\n78#1,7:120\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u001dB;\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u0005H\u0002J\u000c\u0010\u001a\u001a\u00020\u001b*\u00020\u0017H\u0002J\u000c\u0010\u001c\u001a\u00020\u0015*\u00020\u0017H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "accessibilitySettings",
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "mainThread",
        "Lio/reactivex/Scheduler;",
        "(Lio/reactivex/Observable;Lcom/squareup/accessibility/AccessibilitySettings;Lio/reactivex/Scheduler;)V",
        "dismissButton",
        "Landroid/widget/Button;",
        "displayAmount",
        "Lcom/squareup/marketfont/MarketTextView;",
        "enableHighContrastModeButton",
        "enablePinAccessibleModeButton",
        "modeSelectionTitle",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "updateView",
        "screen",
        "announceTitleAndAmountOnLoop",
        "Lio/reactivex/disposables/Disposable;",
        "bind",
        "Factory",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

.field private dismissButton:Landroid/widget/Button;

.field private displayAmount:Lcom/squareup/marketfont/MarketTextView;

.field private enableHighContrastModeButton:Landroid/widget/Button;

.field private enablePinAccessibleModeButton:Landroid/widget/Button;

.field private final mainThread:Lio/reactivex/Scheduler;

.field private modeSelectionTitle:Lcom/squareup/marketfont/MarketTextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/accessibility/AccessibilitySettings;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/accessibility/AccessibilitySettings;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilitySettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->mainThread:Lio/reactivex/Scheduler;

    return-void
.end method

.method public static final synthetic access$getDisplayAmount$p(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;)Lcom/squareup/marketfont/MarketTextView;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->displayAmount:Lcom/squareup/marketfont/MarketTextView;

    if-nez p0, :cond_0

    const-string v0, "displayAmount"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getModeSelectionTitle$p(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;)Lcom/squareup/marketfont/MarketTextView;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->modeSelectionTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez p0, :cond_0

    const-string v0, "modeSelectionTitle"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setDisplayAmount$p(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;Lcom/squareup/marketfont/MarketTextView;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->displayAmount:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method public static final synthetic access$setModeSelectionTitle$p(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;Lcom/squareup/marketfont/MarketTextView;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->modeSelectionTitle:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->updateView(Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;)V

    return-void
.end method

.method private final announceTitleAndAmountOnLoop(Landroid/view/View;)Lio/reactivex/disposables/Disposable;
    .locals 6

    .line 86
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 87
    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    invoke-virtual {v1}, Lcom/squareup/accessibility/AccessibilitySettings;->getTalkBackState()Lio/reactivex/Observable;

    move-result-object v1

    .line 88
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->mainThread:Lio/reactivex/Scheduler;

    const-wide/16 v4, 0xa

    invoke-static {v4, v5, v2, v3}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    const-wide/16 v3, 0x0

    .line 89
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 88
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "Observable.interval(ANNO\u2026\n            0L\n        )"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 92
    sget-object v1, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$1;->INSTANCE:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 93
    new-instance v1, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;-><init>(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "combineLatest(\n        a\u2026yAmount.text}\")\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final bind(Landroid/view/View;)V
    .locals 1

    .line 59
    sget v0, Lcom/squareup/securetouch/R$id;->display_amount:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->displayAmount:Lcom/squareup/marketfont/MarketTextView;

    .line 60
    sget v0, Lcom/squareup/securetouch/R$id;->keypad_mode_selection_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->modeSelectionTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 61
    sget v0, Lcom/squareup/securetouch/R$id;->high_contrast_keypad_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->enableHighContrastModeButton:Landroid/widget/Button;

    .line 62
    sget v0, Lcom/squareup/securetouch/R$id;->accessible_keypad_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->enablePinAccessibleModeButton:Landroid/widget/Button;

    .line 63
    sget v0, Lcom/squareup/securetouch/R$id;->dismiss_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->dismissButton:Landroid/widget/Button;

    return-void
.end method

.method private final updateView(Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;)V
    .locals 3

    .line 67
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->displayAmount:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_0

    const-string v1, "displayAmount"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->getAmount()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;->getUsingHighContrastMode()Z

    move-result v0

    const-string v1, "enableHighContrastModeButton"

    if-eqz v0, :cond_3

    .line 70
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->enableHighContrastModeButton:Landroid/widget/Button;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/securetouch/R$string;->keypad_mode_standard_pin_button_text:I

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 71
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->enableHighContrastModeButton:Landroid/widget/Button;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    .line 99
    new-instance v1, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$updateView$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$updateView$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 73
    :cond_3
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->enableHighContrastModeButton:Landroid/widget/Button;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v2, Lcom/squareup/securetouch/R$string;->keypad_mode_high_contrast_button_text:I

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 74
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->enableHighContrastModeButton:Landroid/widget/Button;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    .line 106
    new-instance v1, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$updateView$$inlined$onClickDebounced$2;

    invoke-direct {v1, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$updateView$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    :goto_0
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->enablePinAccessibleModeButton:Landroid/widget/Button;

    if-nez v0, :cond_6

    const-string v1, "enablePinAccessibleModeButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    .line 113
    new-instance v1, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$updateView$$inlined$onClickDebounced$3;

    invoke-direct {v1, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$updateView$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->dismissButton:Landroid/widget/Button;

    if-nez v0, :cond_7

    const-string v1, "dismissButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/view/View;

    .line 120
    new-instance v1, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$updateView$$inlined$onClickDebounced$4;

    invoke-direct {v1, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$updateView$$inlined$onClickDebounced$4;-><init>(Lcom/squareup/securetouch/SecureTouchModeSelectionScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->bind(Landroid/view/View;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$attach$1;-><init>(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->announceTitleAndAmountOnLoop(Landroid/view/View;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 55
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
