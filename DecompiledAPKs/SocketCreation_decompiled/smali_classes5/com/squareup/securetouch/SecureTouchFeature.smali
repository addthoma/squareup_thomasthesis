.class public interface abstract Lcom/squareup/securetouch/SecureTouchFeature;
.super Ljava/lang/Object;
.source "SecureTouchFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003H&J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0004H&J\u0008\u0010\n\u001a\u00020\u0008H&J\u0008\u0010\u000b\u001a\u00020\u0008H&J\u0010\u0010\u000c\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0006H&J\u0010\u0010\r\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u000fH&J\u0010\u0010\u0010\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u000fH&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchFeature;",
        "",
        "eventsForReader",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
        "eventsFromReader",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "onSecureTouchApplicationEvent",
        "",
        "event",
        "onSecureTouchDisabled",
        "onSecureTouchEnabled",
        "onSecureTouchFeatureEvent",
        "onUnexpectedReleaseEvent",
        "secureTouchPoint",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "onUnexpectedTouchEvent",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract eventsForReader()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract eventsFromReader()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
.end method

.method public abstract onSecureTouchDisabled()V
.end method

.method public abstract onSecureTouchEnabled()V
.end method

.method public abstract onSecureTouchFeatureEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
.end method

.method public abstract onUnexpectedReleaseEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V
.end method

.method public abstract onUnexpectedTouchEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V
.end method
