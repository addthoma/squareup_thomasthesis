.class public final Lcom/squareup/securetouch/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accessibility_button:I = 0x7f0a0102

.field public static final accessible_keypad_button:I = 0x7f0a0124

.field public static final below_first_row:I = 0x7f0a0223

.field public static final below_first_row_divider:I = 0x7f0a0224

.field public static final below_second_row:I = 0x7f0a0225

.field public static final below_second_row_divider:I = 0x7f0a0226

.field public static final below_third_row:I = 0x7f0a0227

.field public static final below_third_row_divider:I = 0x7f0a0228

.field public static final bottom_horizontal_divider:I = 0x7f0a0243

.field public static final cancel_button:I = 0x7f0a0299

.field public static final digit:I = 0x7f0a05b7

.field public static final dismiss_button:I = 0x7f0a05d6

.field public static final display_amount:I = 0x7f0a05d9

.field public static final done_button:I = 0x7f0a05e5

.field public static final empty_space_10:I = 0x7f0a06f4

.field public static final high_contrast_keypad_button:I = 0x7f0a07d9

.field public static final keypad_0:I = 0x7f0a08fa

.field public static final keypad_1:I = 0x7f0a08fb

.field public static final keypad_2:I = 0x7f0a08fc

.field public static final keypad_3:I = 0x7f0a08fd

.field public static final keypad_4:I = 0x7f0a08fe

.field public static final keypad_5:I = 0x7f0a08ff

.field public static final keypad_6:I = 0x7f0a0900

.field public static final keypad_7:I = 0x7f0a0901

.field public static final keypad_8:I = 0x7f0a0902

.field public static final keypad_9:I = 0x7f0a0903

.field public static final keypad_delete:I = 0x7f0a0904

.field public static final keypad_keys:I = 0x7f0a0907

.field public static final keypad_mode_selection_title:I = 0x7f0a0909

.field public static final keypad_screen_view:I = 0x7f0a090a

.field public static final keypad_title:I = 0x7f0a090b

.field public static final keypad_view_container:I = 0x7f0a090c

.field public static final one_third:I = 0x7f0a0aa0

.field public static final one_third_vertical_divider:I = 0x7f0a0aa1

.field public static final star_group:I = 0x7f0a0f28

.field public static final subtext:I = 0x7f0a0f48

.field public static final top_horizontal_divider:I = 0x7f0a1050

.field public static final two_thirds:I = 0x7f0a10aa

.field public static final two_thirds_vertical_divider:I = 0x7f0a10ab


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
