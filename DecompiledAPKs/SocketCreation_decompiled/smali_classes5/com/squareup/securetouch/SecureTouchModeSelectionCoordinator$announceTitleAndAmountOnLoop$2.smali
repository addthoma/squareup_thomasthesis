.class final Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;
.super Ljava/lang/Object;
.source "SecureTouchModeSelectionCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->announceTitleAndAmountOnLoop(Landroid/view/View;)Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Ljava/lang/Boolean;",
        "+",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\t\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000126\u0010\u0002\u001a2\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lkotlin/Pair;",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_announceTitleAndAmountOnLoop:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;->this$0:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;->$this_announceTitleAndAmountOnLoop:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 28
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 94
    iget-object p1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;->$this_announceTitleAndAmountOnLoop:Landroid/view/View;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;->this$0:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;

    invoke-static {v1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->access$getModeSelectionTitle$p(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator$announceTitleAndAmountOnLoop$2;->this$0:Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;

    invoke-static {v1}, Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;->access$getDisplayAmount$p(Lcom/squareup/securetouch/SecureTouchModeSelectionCoordinator;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    return-void
.end method
