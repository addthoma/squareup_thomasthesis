.class public final Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;
.super Ljava/lang/Object;
.source "SecureTouchWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/securetouch/SecureTouchWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureTouchWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoSecureTouchWorkflowRunner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0004H\u0016J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u0004H\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;",
        "Lcom/squareup/securetouch/SecureTouchWorkflowRunner;",
        "()V",
        "onResult",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "onUpdateScreens",
        "",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "registerServices",
        "",
        "scopeBuilder",
        "Lmortar/MortarScope$Builder;",
        "secure-touch-workflow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;

    invoke-direct {v0}, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;->INSTANCE:Lcom/squareup/securetouch/SecureTouchWorkflowRunner$NoSecureTouchWorkflowRunner;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;"
        }
    .end annotation

    .line 15
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.never()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onUpdateScreens()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;>;"
        }
    .end annotation

    .line 14
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.never()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public registerServices(Lmortar/MortarScope$Builder;)V
    .locals 1

    const-string v0, "scopeBuilder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
