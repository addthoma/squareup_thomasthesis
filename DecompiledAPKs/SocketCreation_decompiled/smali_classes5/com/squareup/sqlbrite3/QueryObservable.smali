.class public final Lcom/squareup/sqlbrite3/QueryObservable;
.super Lio/reactivex/Observable;
.source "QueryObservable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/Observable<",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        ">;"
    }
.end annotation


# static fields
.field static final QUERY_OBSERVABLE:Lio/reactivex/functions/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Function<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;",
            "Lcom/squareup/sqlbrite3/QueryObservable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final upstream:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/sqlbrite3/QueryObservable$1;

    invoke-direct {v0}, Lcom/squareup/sqlbrite3/QueryObservable$1;-><init>()V

    sput-object v0, Lcom/squareup/sqlbrite3/QueryObservable;->QUERY_OBSERVABLE:Lio/reactivex/functions/Function;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/sqlbrite3/QueryObservable;->upstream:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public final mapToList(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    .line 132
    invoke-static {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->mapToList(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryObservable;->lift(Lio/reactivex/ObservableOperator;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final mapToOne(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 55
    invoke-static {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->mapToOne(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryObservable;->lift(Lio/reactivex/ObservableOperator;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final mapToOneOrDefault(Lio/reactivex/functions/Function;Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;TT;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 81
    invoke-static {p1, p2}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->mapToOneOrDefault(Lio/reactivex/functions/Function;Ljava/lang/Object;)Lio/reactivex/ObservableOperator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryObservable;->lift(Lio/reactivex/ObservableOperator;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final mapToOptional(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/functions/Function<",
            "Landroid/database/Cursor;",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    .line 106
    invoke-static {p1}, Lcom/squareup/sqlbrite3/SqlBrite$Query;->mapToOptional(Lio/reactivex/functions/Function;)Lio/reactivex/ObservableOperator;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/QueryObservable;->lift(Lio/reactivex/ObservableOperator;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer<",
            "-",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;)V"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/sqlbrite3/QueryObservable;->upstream:Lio/reactivex/Observable;

    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/Observer;)V

    return-void
.end method
