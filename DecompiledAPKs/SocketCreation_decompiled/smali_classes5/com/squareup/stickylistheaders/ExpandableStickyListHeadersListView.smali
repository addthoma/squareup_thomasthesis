.class public Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;
.super Lcom/squareup/stickylistheaders/StickyListHeadersListView;
.source "ExpandableStickyListHeadersListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;
    }
.end annotation


# static fields
.field public static final ANIMATION_COLLAPSE:I = 0x1

.field public static final ANIMATION_EXPAND:I


# instance fields
.field mDefaultAnimExecutor:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;

.field mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance p1, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$1;

    invoke-direct {p1, p0}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$1;-><init>(Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;)V

    iput-object p1, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mDefaultAnimExecutor:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance p1, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$1;

    invoke-direct {p1, p0}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$1;-><init>(Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;)V

    iput-object p1, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mDefaultAnimExecutor:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    new-instance p1, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$1;

    invoke-direct {p1, p0}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$1;-><init>(Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;)V

    iput-object p1, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mDefaultAnimExecutor:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;

    return-void
.end method

.method private animateView(Landroid/view/View;I)V
    .locals 1

    if-nez p2, :cond_0

    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    if-ne v0, p2, :cond_1

    .line 117
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mDefaultAnimExecutor:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;

    if-eqz v0, :cond_2

    .line 121
    invoke-interface {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;->executeAnim(Landroid/view/View;I)V

    :cond_2
    return-void
.end method


# virtual methods
.method public collapse(J)V
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->isHeaderCollapsed(J)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->collapse(J)V

    .line 89
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->getItemViewsByHeaderId(J)Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 93
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    const/4 v0, 0x1

    .line 94
    invoke-direct {p0, p2, v0}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->animateView(Landroid/view/View;I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public expand(J)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->isHeaderCollapsed(J)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->expand(J)V

    .line 74
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->getItemViewsByHeaderId(J)Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    .line 78
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    const/4 v0, 0x0

    .line 79
    invoke-direct {p0, p2, v0}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->animateView(Landroid/view/View;I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public findItemIdByView(Landroid/view/View;)J
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->findItemIdByView(Landroid/view/View;)J

    move-result-wide v0

    return-wide v0
.end method

.method public findViewByItemId(J)Landroid/view/View;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->findViewByItemId(J)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getAdapter()Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    return-object v0
.end method

.method public bridge synthetic getAdapter()Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->getAdapter()Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    move-result-object v0

    return-object v0
.end method

.method public isHeaderCollapsed(J)Z
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;->isHeaderCollapsed(J)Z

    move-result p1

    return p1
.end method

.method public setAdapter(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V
    .locals 1

    .line 56
    new-instance v0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-direct {v0, p1}, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    iput-object v0, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    .line 57
    iget-object p1, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mExpandableStickyListHeadersAdapter:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersAdapter;

    invoke-super {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setAdapter(Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;)V

    return-void
.end method

.method public setAnimExecutor(Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView;->mDefaultAnimExecutor:Lcom/squareup/stickylistheaders/ExpandableStickyListHeadersListView$IAnimationExecutor;

    return-void
.end method
