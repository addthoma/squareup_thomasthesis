.class public Lcom/squareup/squarewave/wav/WavHelper;
.super Ljava/lang/Object;
.source "WavHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fillInWavLength(Ljava/io/File;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 55
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v0, p0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-wide/16 v1, 0x4

    .line 56
    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    add-int/lit8 p1, p1, 0x24

    and-int/lit16 p0, p1, 0xff

    .line 58
    invoke-virtual {v0, p0}, Ljava/io/RandomAccessFile;->write(I)V

    ushr-int/lit8 p0, p1, 0x8

    and-int/lit16 p0, p0, 0xff

    .line 59
    invoke-virtual {v0, p0}, Ljava/io/RandomAccessFile;->write(I)V

    ushr-int/lit8 p0, p1, 0x10

    and-int/lit16 p0, p0, 0xff

    .line 60
    invoke-virtual {v0, p0}, Ljava/io/RandomAccessFile;->write(I)V

    ushr-int/lit8 p0, p1, 0x18

    and-int/lit16 p0, p0, 0xff

    .line 61
    invoke-virtual {v0, p0}, Ljava/io/RandomAccessFile;->write(I)V

    .line 62
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    return-void
.end method

.method public static saveAsWav(Ljava/io/File;I[S)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 14
    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 18
    :try_start_0
    array-length p0, p2

    shl-int/lit8 p0, p0, 0x1

    .line 20
    invoke-static {v0, p0, p1}, Lcom/squareup/squarewave/wav/WavHelper;->writeWavHeader(Ljava/io/OutputStream;II)V

    .line 22
    array-length p0, p2

    const/4 p1, 0x0

    :goto_0
    if-ge p1, p0, :cond_0

    aget-short v1, p2, p1

    .line 23
    invoke-static {v0, v1}, Lcom/squareup/squarewave/wav/WavHelper;->writeShort(Ljava/io/OutputStream;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 27
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    return-void

    :catchall_0
    move-exception p0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 30
    :catch_1
    throw p0
.end method

.method public static writeInt(Ljava/io/OutputStream;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    and-int/lit16 v0, p1, 0xff

    .line 80
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    ushr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    .line 81
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    ushr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    .line 82
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    ushr-int/lit8 p1, p1, 0x18

    and-int/lit16 p1, p1, 0xff

    .line 83
    invoke-virtual {p0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public static writeShort(Ljava/io/OutputStream;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    and-int/lit16 v0, p1, 0xff

    .line 74
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    ushr-int/lit8 p1, p1, 0x8

    and-int/lit16 p1, p1, 0xff

    .line 75
    invoke-virtual {p0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public static writeShorts(Ljava/io/OutputStream;[S)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 67
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-short v2, p1, v1

    .line 68
    invoke-static {p0, v2}, Lcom/squareup/squarewave/wav/WavHelper;->writeShort(Ljava/io/OutputStream;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static writeWavHeader(Ljava/io/OutputStream;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "UTF-8"

    const-string v1, "RIFF"

    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    add-int/lit8 v1, p1, 0x24

    .line 39
    invoke-static {p0, v1}, Lcom/squareup/squarewave/wav/WavHelper;->writeInt(Ljava/io/OutputStream;I)V

    const-string v1, "WAVE"

    .line 41
    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    const-string v1, "fmt "

    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write([B)V

    const/16 v1, 0x10

    .line 43
    invoke-static {p0, v1}, Lcom/squareup/squarewave/wav/WavHelper;->writeInt(Ljava/io/OutputStream;I)V

    const/4 v2, 0x1

    .line 44
    invoke-static {p0, v2}, Lcom/squareup/squarewave/wav/WavHelper;->writeShort(Ljava/io/OutputStream;I)V

    .line 45
    invoke-static {p0, v2}, Lcom/squareup/squarewave/wav/WavHelper;->writeShort(Ljava/io/OutputStream;I)V

    .line 46
    invoke-static {p0, p2}, Lcom/squareup/squarewave/wav/WavHelper;->writeInt(Ljava/io/OutputStream;I)V

    const/4 v2, 0x2

    mul-int/lit8 p2, p2, 0x2

    .line 47
    invoke-static {p0, p2}, Lcom/squareup/squarewave/wav/WavHelper;->writeInt(Ljava/io/OutputStream;I)V

    .line 48
    invoke-static {p0, v2}, Lcom/squareup/squarewave/wav/WavHelper;->writeShort(Ljava/io/OutputStream;I)V

    .line 49
    invoke-static {p0, v1}, Lcom/squareup/squarewave/wav/WavHelper;->writeShort(Ljava/io/OutputStream;I)V

    const-string p2, "data"

    .line 50
    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p2

    invoke-virtual {p0, p2}, Ljava/io/OutputStream;->write([B)V

    .line 51
    invoke-static {p0, p1}, Lcom/squareup/squarewave/wav/WavHelper;->writeInt(Ljava/io/OutputStream;I)V

    return-void
.end method
