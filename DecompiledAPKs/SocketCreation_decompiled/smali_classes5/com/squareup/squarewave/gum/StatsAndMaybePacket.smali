.class public Lcom/squareup/squarewave/gum/StatsAndMaybePacket;
.super Ljava/lang/Object;
.source "StatsAndMaybePacket.java"


# instance fields
.field public final packet:Lcom/squareup/squarewave/gum/CardDataPacket;

.field public final stats:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gum/CardDataPacket;Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;->packet:Lcom/squareup/squarewave/gum/CardDataPacket;

    .line 14
    iput-object p2, p0, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;->stats:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    return-void
.end method


# virtual methods
.method public isSuccessfulDemod()Z
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;->stats:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
