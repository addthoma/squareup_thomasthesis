.class public final Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;
.super Ljava/lang/Object;
.source "M1PacketCardDataPlainText.java"


# static fields
.field private static final M1_PACKET_COUNTER_LENGTH:I = 0x6

.field private static final M1_PACKET_ENTROPY_LENGTH:I = 0x6

.field private static final M1_PACKET_SWIPE_SPEED_LENGTH:I = 0x3


# instance fields
.field public final bitPeriodBWD:S

.field public final bitPeriodBit140:S

.field public final bitPeriodEndOfSwipe:S

.field public final counter:J

.field public final entropy:J

.field public final swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;


# direct methods
.method constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 3

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    .line 24
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    new-array v1, v0, [B

    .line 26
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 27
    invoke-static {v1}, Lcom/squareup/squarewave/gum/Util;->unsignedBytesToLongLittleEndian([B)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->counter:J

    .line 29
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    new-array v1, v0, [B

    .line 31
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 32
    invoke-static {v1}, Lcom/squareup/squarewave/gum/Util;->unsignedBytesToLongLittleEndian([B)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->entropy:J

    const/4 v1, 0x1

    .line 34
    invoke-static {p1, v1}, Lcom/squareup/squarewave/gum/Buffers;->next(Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    if-eqz v2, :cond_2

    if-eq v2, v1, :cond_1

    const/4 v1, 0x2

    if-ne v2, v1, :cond_0

    .line 43
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->UNKNOWN_DIRECTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    goto :goto_0

    .line 46
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown swipe direction digit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 40
    :cond_1
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->BACKWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    goto :goto_0

    .line 37
    :cond_2
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->FORWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    iput-object v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 49
    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/squarewave/gum/Buffers;->next(Ljava/nio/ByteBuffer;I)Ljava/nio/ByteBuffer;

    .line 50
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Ljava/nio/ShortBuffer;->get()S

    move-result v0

    iput-short v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodBWD:S

    .line 52
    invoke-virtual {p1}, Ljava/nio/ShortBuffer;->get()S

    move-result v0

    iput-short v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodBit140:S

    .line 53
    invoke-virtual {p1}, Ljava/nio/ShortBuffer;->get()S

    move-result p1

    iput-short p1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodEndOfSwipe:S

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "{"

    .line 59
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "counter: "

    .line 60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->counter:J

    .line 61
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", entropy: "

    .line 62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->entropy:J

    .line 63
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", swipeDirection: "

    .line 64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->swipeDirection:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 65
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", bitPeriodBWD: "

    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-short v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodBWD:S

    .line 67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bitPeriodBit140: "

    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-short v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodBit140:S

    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bitPeriodEndOfSwipe: "

    .line 70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-short v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;->bitPeriodEndOfSwipe:S

    .line 71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
