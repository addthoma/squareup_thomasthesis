.class public Lcom/squareup/squarewave/gum/EventBuilder;
.super Ljava/lang/Object;
.source "EventBuilder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildCarrierDetectInfo(JIZJJIIIIZLcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;I)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;-><init>()V

    .line 27
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_sample:Ljava/lang/Long;

    .line 28
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->num_samples:Ljava/lang/Integer;

    .line 29
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->is_early_packet:Ljava/lang/Boolean;

    .line 30
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_runtime_in_us:Ljava/lang/Long;

    .line 31
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_time_in_us:Ljava/lang/Long;

    .line 32
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_threshold:Ljava/lang/Integer;

    .line 33
    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_avg:Ljava/lang/Integer;

    .line 34
    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_threshold:Ljava/lang/Integer;

    .line 35
    invoke-static {p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_avg:Ljava/lang/Integer;

    .line 36
    invoke-static {p12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->allow_restart:Ljava/lang/Boolean;

    move-object v1, p13

    .line 37
    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    move-object/from16 v1, p14

    .line 38
    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 39
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->in_packet_runtime_us:Ljava/lang/Integer;

    .line 40
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    move-result-object v0

    return-object v0
.end method

.method public static buildClassifyInfo(IIIIIIIIIIFFFFIF)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;-><init>()V

    .line 50
    invoke-static {p0}, Lcom/squareup/squarewave/gum/Mapping;->lcrLinkTypeToLogClassifyInfoLinkType(I)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    .line 51
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    .line 52
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    .line 53
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    .line 54
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->ffts_runtime:Ljava/lang/Integer;

    .line 55
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->analyze_ffts_runtime:Ljava/lang/Integer;

    .line 56
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    .line 57
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    .line 58
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->find_peaks_runtime:Ljava/lang/Integer;

    .line 59
    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    .line 60
    invoke-static {p10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->o1_score:Ljava/lang/Float;

    .line 61
    invoke-static {p11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_fast_score:Ljava/lang/Float;

    .line 62
    invoke-static {p12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_slow_score:Ljava/lang/Float;

    .line 63
    invoke-static {p13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_score:Ljava/lang/Float;

    .line 64
    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_count:Ljava/lang/Integer;

    .line 65
    invoke-static/range {p15 .. p15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_variability:Ljava/lang/Float;

    .line 66
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    move-result-object v0

    return-object v0
.end method

.method public static buildSqLinkDemodInfo(IIZIIIIIIIZFIIIIIFFFFFII)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;
    .locals 3

    .line 77
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;-><init>()V

    .line 78
    invoke-static {p0}, Lcom/squareup/squarewave/gum/Mapping;->demodResultToLogDemodResult(I)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v1, -0x1

    move v2, p1

    if-eq v2, v1, :cond_0

    .line 80
    invoke-static {p1}, Lcom/squareup/squarewave/gum/Mapping;->statusOrdinalToLast4Status(I)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    .line 82
    :cond_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->is_fast:Ljava/lang/Boolean;

    .line 83
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->low_pass_filter_runtime:Ljava/lang/Integer;

    .line 84
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->find_preamble_freq_runtime:Ljava/lang/Integer;

    .line 85
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->first_find_sync_runtime:Ljava/lang/Integer;

    .line 86
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->rest_find_sync_runtime:Ljava/lang/Integer;

    .line 87
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->demodulate_packets_runtime:Ljava/lang/Integer;

    .line 88
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_runtime:Ljava/lang/Integer;

    .line 89
    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->viterbi_runtime:Ljava/lang/Integer;

    .line 90
    invoke-static {p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->inverted:Ljava/lang/Boolean;

    .line 91
    invoke-static {p11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->preamble_freq:Ljava/lang/Float;

    .line 92
    invoke-static {p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index0:Ljava/lang/Integer;

    .line 93
    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index1:Ljava/lang/Integer;

    .line 94
    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index2:Ljava/lang/Integer;

    .line 95
    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index3:Ljava/lang/Integer;

    .line 96
    invoke-static/range {p16 .. p16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index4:Ljava/lang/Integer;

    .line 97
    invoke-static/range {p17 .. p17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency0:Ljava/lang/Float;

    .line 98
    invoke-static/range {p18 .. p18}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency1:Ljava/lang/Float;

    .line 99
    invoke-static/range {p19 .. p19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency2:Ljava/lang/Float;

    .line 100
    invoke-static/range {p20 .. p20}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency3:Ljava/lang/Float;

    .line 101
    invoke-static/range {p21 .. p21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency4:Ljava/lang/Float;

    .line 102
    invoke-static/range {p22 .. p22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_bit_errors:Ljava/lang/Integer;

    .line 103
    invoke-static/range {p23 .. p23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_num_bits:Ljava/lang/Integer;

    .line 104
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    move-result-object v0

    return-object v0
.end method

.method public static quickTest()V
    .locals 2

    .line 17
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "it worked"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
