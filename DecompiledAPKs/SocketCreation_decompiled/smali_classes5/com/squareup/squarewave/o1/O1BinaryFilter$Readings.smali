.class final Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;
.super Ljava/lang/Object;
.source "O1BinaryFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/o1/O1BinaryFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Readings"
.end annotation


# instance fields
.field private firstHalf:S

.field private firstSampleIndex:I

.field private highToLowLevel:I

.field private lowToHighLevel:I

.field private final readings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/squarewave/gen2/Reading;",
            ">;"
        }
    .end annotation
.end field

.field private waitingForSecondHalf:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->readings:Ljava/util/List;

    const/4 v0, 0x0

    .line 301
    iput-boolean v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->waitingForSecondHalf:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/o1/O1BinaryFilter$1;)V
    .locals 0

    .line 296
    invoke-direct {p0}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;-><init>()V

    return-void
.end method

.method private getLevel(SS)I
    .locals 0

    if-ge p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 330
    :goto_0
    iget-object p2, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->readings:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-nez p2, :cond_1

    xor-int/lit8 p2, p1, 0x1

    .line 332
    iput p2, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->lowToHighLevel:I

    .line 333
    iput p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->highToLowLevel:I

    :cond_1
    if-eqz p1, :cond_2

    .line 335
    iget p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->lowToHighLevel:I

    goto :goto_1

    :cond_2
    iget p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->highToLowLevel:I

    :goto_1
    return p1
.end method


# virtual methods
.method public addHalfCycleAverage(SII)V
    .locals 2

    .line 309
    iget-boolean v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->waitingForSecondHalf:Z

    if-eqz v0, :cond_0

    .line 310
    iget-object p2, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->readings:Ljava/util/List;

    new-instance v0, Lcom/squareup/squarewave/gen2/Reading;

    iget-short v1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->firstHalf:S

    invoke-direct {p0, v1, p1}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->getLevel(SS)I

    move-result p1

    iget v1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->firstSampleIndex:I

    invoke-direct {v0, p1, v1, p3}, Lcom/squareup/squarewave/gen2/Reading;-><init>(III)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x0

    .line 311
    iput-boolean p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->waitingForSecondHalf:Z

    goto :goto_0

    .line 313
    :cond_0
    iput-short p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->firstHalf:S

    .line 314
    iput p2, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->firstSampleIndex:I

    const/4 p1, 0x1

    .line 315
    iput-boolean p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->waitingForSecondHalf:Z

    :goto_0
    return-void
.end method

.method public clear()V
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->readings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public getArray()[Lcom/squareup/squarewave/gen2/Reading;
    .locals 2

    .line 340
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->readings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/squarewave/gen2/Reading;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/squarewave/gen2/Reading;

    return-object v0
.end method
