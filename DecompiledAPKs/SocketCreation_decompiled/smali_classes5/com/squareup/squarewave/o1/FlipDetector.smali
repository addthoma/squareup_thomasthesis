.class Lcom/squareup/squarewave/o1/FlipDetector;
.super Ljava/lang/Object;
.source "FlipDetector.java"


# instance fields
.field private final swipe:Lcom/squareup/squarewave/o1/O1Swipe;

.field private final threshold:Lcom/squareup/squarewave/o1/Threshold;


# direct methods
.method constructor <init>(Lcom/squareup/squarewave/o1/O1Swipe;Lcom/squareup/squarewave/o1/Threshold;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/squarewave/o1/FlipDetector;->swipe:Lcom/squareup/squarewave/o1/O1Swipe;

    .line 21
    iput-object p2, p0, Lcom/squareup/squarewave/o1/FlipDetector;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    return-void
.end method

.method private isLocalMaximum(I)Z
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/squareup/squarewave/o1/FlipDetector;->swipe:Lcom/squareup/squarewave/o1/O1Swipe;

    invoke-virtual {v0}, Lcom/squareup/squarewave/o1/O1Swipe;->getDerivative()[S

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 40
    array-length v2, v0

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    if-lt p1, v2, :cond_0

    goto :goto_1

    .line 42
    :cond_0
    aget-short v2, v0, p1

    add-int/lit8 v4, p1, 0x1

    .line 44
    aget-short v4, v0, v4

    if-gt v2, v4, :cond_1

    return v1

    :cond_1
    sub-int/2addr p1, v3

    .line 48
    aget-short v4, v0, p1

    :goto_0
    if-ne v4, v2, :cond_2

    if-lez p1, :cond_2

    add-int/lit8 p1, p1, -0x1

    .line 51
    aget-short v4, v0, p1

    goto :goto_0

    :cond_2
    if-le v2, v4, :cond_3

    const/4 v1, 0x1

    :cond_3
    :goto_1
    return v1
.end method

.method private isOverThreshold(I)Z
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/squarewave/o1/FlipDetector;->swipe:Lcom/squareup/squarewave/o1/O1Swipe;

    invoke-virtual {v0}, Lcom/squareup/squarewave/o1/O1Swipe;->getDerivative()[S

    move-result-object v0

    aget-short v0, v0, p1

    iget-object v1, p0, Lcom/squareup/squarewave/o1/FlipDetector;->threshold:Lcom/squareup/squarewave/o1/Threshold;

    invoke-interface {v1, p1}, Lcom/squareup/squarewave/o1/Threshold;->getValue(I)S

    move-result p1

    if-le v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public flipAt(I)Z
    .locals 1

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/o1/FlipDetector;->isLocalMaximum(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/squarewave/o1/FlipDetector;->isOverThreshold(I)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public flipsBetween(II)[I
    .locals 4

    sub-int v0, p2, p1

    .line 65
    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge p1, p2, :cond_1

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/squarewave/o1/FlipDetector;->flipAt(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 69
    aput p1, v0, v2

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 73
    :cond_1
    new-array p1, v2, [I

    .line 74
    invoke-static {v0, v1, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1
.end method
