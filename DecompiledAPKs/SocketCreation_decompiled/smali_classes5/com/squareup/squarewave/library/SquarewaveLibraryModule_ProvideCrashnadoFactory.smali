.class public final Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;
.super Ljava/lang/Object;
.source "SquarewaveLibraryModule_ProvideCrashnadoFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crashnado/Crashnado;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;->module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    return-void
.end method

.method public static create(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)V

    return-object v0
.end method

.method public static provideCrashnado(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/crashnado/Crashnado;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule;->provideCrashnado()Lcom/squareup/crashnado/Crashnado;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/crashnado/Crashnado;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/crashnado/Crashnado;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;->module:Lcom/squareup/squarewave/library/SquarewaveLibraryModule;

    invoke-static {v0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;->provideCrashnado(Lcom/squareup/squarewave/library/SquarewaveLibraryModule;)Lcom/squareup/crashnado/Crashnado;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/SquarewaveLibraryModule_ProvideCrashnadoFactory;->get()Lcom/squareup/crashnado/Crashnado;

    move-result-object v0

    return-object v0
.end method
