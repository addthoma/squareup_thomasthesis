.class public Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
.super Ljava/lang/Object;
.source "Gen2Swipe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squarewave/gen2/Gen2Swipe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

.field private peaks:[Lcom/squareup/squarewave/gen2/Peak;

.field private volatile peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

.field private readings:[Lcom/squareup/squarewave/gen2/Reading;

.field private signal:Lcom/squareup/squarewave/Signal;

.field private streaks:[Lcom/squareup/squarewave/gen2/Streak;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/Signal;)V
    .locals 0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/squarewave/gen2/Gen2Swipe;)V
    .locals 1

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    invoke-static {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->access$100(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/Signal;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    .line 144
    invoke-static {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->access$200(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/Peak;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    .line 145
    invoke-static {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->access$300(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/Peak;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    .line 146
    invoke-static {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->access$400(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/Reading;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    .line 147
    invoke-static {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->access$500(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/CharacterReading;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    .line 148
    invoke-static {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->access$600(Lcom/squareup/squarewave/gen2/Gen2Swipe;)[Lcom/squareup/squarewave/gen2/Streak;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->streaks:[Lcom/squareup/squarewave/gen2/Streak;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/squarewave/gen2/Gen2Swipe;Lcom/squareup/squarewave/gen2/Gen2Swipe$1;)V
    .locals 0

    .line 129
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;-><init>(Lcom/squareup/squarewave/gen2/Gen2Swipe;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/squarewave/gen2/Gen2Swipe;
    .locals 9

    .line 153
    new-instance v8, Lcom/squareup/squarewave/gen2/Gen2Swipe;

    iget-object v1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    iget-object v2, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    iget-object v3, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    iget-object v4, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    iget-object v5, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    iget-object v6, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->streaks:[Lcom/squareup/squarewave/gen2/Streak;

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/squarewave/gen2/Gen2Swipe;-><init>(Lcom/squareup/squarewave/Signal;[Lcom/squareup/squarewave/gen2/Peak;[Lcom/squareup/squarewave/gen2/Peak;[Lcom/squareup/squarewave/gen2/Reading;[Lcom/squareup/squarewave/gen2/CharacterReading;[Lcom/squareup/squarewave/gen2/Streak;Lcom/squareup/squarewave/gen2/Gen2Swipe$1;)V

    return-object v8
.end method

.method public characterReadings([Lcom/squareup/squarewave/gen2/CharacterReading;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    return-object p0
.end method

.method public peaks(Ljava/util/List;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/squarewave/gen2/Peak;",
            ">;)",
            "Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;"
        }
    .end annotation

    .line 195
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/squareup/squarewave/gen2/Peak;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/squarewave/gen2/Peak;

    invoke-virtual {p0, p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaks([Lcom/squareup/squarewave/gen2/Peak;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    return-object p1
.end method

.method public peaks([Lcom/squareup/squarewave/gen2/Peak;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    const/4 p1, 0x0

    .line 184
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    .line 185
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    .line 186
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    return-object p0
.end method

.method public readings([Lcom/squareup/squarewave/gen2/Reading;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    const/4 p1, 0x0

    .line 221
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    return-object p0
.end method

.method public signal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    const/4 p1, 0x0

    .line 164
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaks:[Lcom/squareup/squarewave/gen2/Peak;

    .line 165
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->peaksBySampleIndex:[Lcom/squareup/squarewave/gen2/Peak;

    .line 166
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->readings:[Lcom/squareup/squarewave/gen2/Reading;

    .line 167
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->characterReadings:[Lcom/squareup/squarewave/gen2/CharacterReading;

    return-object p0
.end method

.method public streaks(Ljava/util/List;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/squarewave/gen2/Streak;",
            ">;)",
            "Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;"
        }
    .end annotation

    .line 212
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/squareup/squarewave/gen2/Streak;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/squarewave/gen2/Streak;

    invoke-virtual {p0, p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->streaks([Lcom/squareup/squarewave/gen2/Streak;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    return-object p1
.end method

.method public streaks([Lcom/squareup/squarewave/gen2/Streak;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->streaks:[Lcom/squareup/squarewave/gen2/Streak;

    return-object p0
.end method

.method public triggersError(Z)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->triggersError()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->buildUpon()Lcom/squareup/squarewave/Signal$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/squarewave/Signal$Builder;->triggersError(Z)Lcom/squareup/squarewave/Signal$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/squarewave/Signal$Builder;->build()Lcom/squareup/squarewave/Signal;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal:Lcom/squareup/squarewave/Signal;

    :cond_0
    return-object p0
.end method
