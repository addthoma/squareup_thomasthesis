.class public Lcom/squareup/squarewave/gen2/Gen2DecoderFilter;
.super Ljava/lang/Object;
.source "Gen2DecoderFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# instance fields
.field private nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/squarewave/gen2/Gen2DecoderFilter;-><init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2DecoderFilter;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    return-void
.end method

.method private tryDecode(Lcom/squareup/squarewave/gen2/Gen2Swipe;Lcom/squareup/squarewave/gen2/CardDataStart;[Lcom/squareup/squarewave/gen2/Reading;Ljava/util/List;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/squarewave/gen2/Gen2Swipe;",
            "Lcom/squareup/squarewave/gen2/CardDataStart;",
            "[",
            "Lcom/squareup/squarewave/gen2/Reading;",
            "Ljava/util/List<",
            "Lcom/squareup/squarewave/gen2/CharacterReading;",
            ">;)",
            "Lcom/squareup/squarewave/gen2/Gen2DemodResult;"
        }
    .end annotation

    .line 81
    invoke-virtual {p2}, Lcom/squareup/squarewave/gen2/CardDataStart;->getDecoder()Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;

    move-result-object v0

    invoke-interface {v0, p3, p2, p4, p1}, Lcom/squareup/squarewave/gen2/Gen2ReadingsDecoder;->decode([Lcom/squareup/squarewave/gen2/Reading;Lcom/squareup/squarewave/gen2/CardDataStart;Ljava/util/List;Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 9

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 30
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->readings()[Lcom/squareup/squarewave/gen2/Reading;

    move-result-object v1

    .line 35
    invoke-static {v1}, Lcom/squareup/squarewave/gen2/CardDataStart;->find([Lcom/squareup/squarewave/gen2/Reading;)[Lcom/squareup/squarewave/gen2/CardDataStart;

    move-result-object v2

    .line 39
    array-length v3, v2

    if-nez v3, :cond_0

    .line 40
    new-instance v1, Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;->START_SENTINEL_NOT_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;

    invoke-direct {v1, v2, v3}, Lcom/squareup/squarewave/gen2/Gen2DemodResult;-><init>(Lcom/squareup/squarewave/decode/DemodResult;Lcom/squareup/protos/logging/events/swipe_experience/Gen2DemodInfo$DemodResult;)V

    move-object v7, v1

    goto :goto_2

    .line 43
    :cond_0
    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v6, v4

    move-object v7, v6

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_4

    aget-object v7, v2, v4

    .line 44
    invoke-virtual {v7}, Lcom/squareup/squarewave/gen2/CardDataStart;->isForward()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 45
    invoke-direct {p0, p1, v7, v1, v0}, Lcom/squareup/squarewave/gen2/Gen2DecoderFilter;->tryDecode(Lcom/squareup/squarewave/gen2/Gen2Swipe;Lcom/squareup/squarewave/gen2/CardDataStart;[Lcom/squareup/squarewave/gen2/Reading;Ljava/util/List;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v7

    goto :goto_1

    :cond_1
    if-nez v6, :cond_2

    .line 48
    array-length v6, v1

    new-array v6, v6, [Lcom/squareup/squarewave/gen2/Reading;

    .line 49
    array-length v8, v1

    invoke-static {v1, v5, v6, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 50
    invoke-static {v6}, Lcom/squareup/util/Arrays;->reverse([Ljava/lang/Object;)V

    .line 52
    :cond_2
    invoke-direct {p0, p1, v7, v6, v0}, Lcom/squareup/squarewave/gen2/Gen2DecoderFilter;->tryDecode(Lcom/squareup/squarewave/gen2/Gen2Swipe;Lcom/squareup/squarewave/gen2/CardDataStart;[Lcom/squareup/squarewave/gen2/Reading;Ljava/util/List;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v7

    .line 54
    :goto_1
    iget-object v8, v7, Lcom/squareup/squarewave/gen2/Gen2DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    invoke-virtual {v8}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v8

    if-eqz v8, :cond_3

    goto :goto_2

    .line 55
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 60
    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/squareup/squarewave/gen2/Gen2DecoderFilter;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    if-eqz v1, :cond_5

    .line 61
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->buildUpon()Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    .line 63
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/squareup/squarewave/gen2/CharacterReading;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/squareup/squarewave/gen2/CharacterReading;

    .line 62
    invoke-virtual {p1, v2}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->characterReadings([Lcom/squareup/squarewave/gen2/CharacterReading;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    .line 64
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->build()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object p1

    .line 61
    invoke-interface {v1, p1}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    .line 68
    :cond_5
    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object v7
.end method
