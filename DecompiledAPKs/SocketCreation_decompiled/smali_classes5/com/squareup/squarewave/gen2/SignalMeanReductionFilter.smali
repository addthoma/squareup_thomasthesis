.class public Lcom/squareup/squarewave/gen2/SignalMeanReductionFilter;
.super Ljava/lang/Object;
.source "SignalMeanReductionFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# instance fields
.field private final nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/SignalMeanReductionFilter;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    return-void
.end method


# virtual methods
.method public hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 12

    .line 21
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->samples()[S

    move-result-object v0

    .line 22
    array-length v1, v0

    .line 23
    new-array v2, v1, [S

    .line 27
    array-length v3, v0

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    move-wide v6, v5

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_0

    aget-short v8, v0, v5

    int-to-long v8, v8

    add-long/2addr v6, v8

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    int-to-long v8, v1

    .line 30
    div-long/2addr v6, v8

    .line 33
    :goto_1
    array-length v1, v0

    if-ge v4, v1, :cond_3

    .line 35
    aget-short v1, v0, v4

    int-to-long v8, v1

    sub-long/2addr v8, v6

    const-wide/16 v10, 0x7fff

    cmp-long v1, v8, v10

    if-lez v1, :cond_1

    const/16 v1, 0x7fff

    .line 38
    aput-short v1, v2, v4

    goto :goto_2

    :cond_1
    const-wide/16 v10, -0x8000

    cmp-long v1, v8, v10

    if-gez v1, :cond_2

    const/16 v1, -0x8000

    .line 40
    aput-short v1, v2, v4

    goto :goto_2

    :cond_2
    long-to-int v1, v8

    int-to-short v1, v1

    .line 42
    aput-short v1, v2, v4

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 47
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->getSignal()Lcom/squareup/squarewave/Signal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal;->buildUpon()Lcom/squareup/squarewave/Signal$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/squareup/squarewave/Signal$Builder;->samples([S)Lcom/squareup/squarewave/Signal$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/squarewave/Signal$Builder;->build()Lcom/squareup/squarewave/Signal;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/squareup/squarewave/gen2/SignalMeanReductionFilter;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->buildUpon()Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->signal(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->build()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object p1

    return-object p1
.end method
