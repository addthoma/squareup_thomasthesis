.class final Lcom/squareup/tmn/TmnTimings$event$1;
.super Ljava/lang/Object;
.source "TmnTimings.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $timeNanos:J

.field final synthetic $what:Lcom/squareup/tmn/What;

.field final synthetic this$0:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/TmnTimings;Lcom/squareup/tmn/What;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings$event$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    iput-object p2, p0, Lcom/squareup/tmn/TmnTimings$event$1;->$what:Lcom/squareup/tmn/What;

    iput-wide p3, p0, Lcom/squareup/tmn/TmnTimings$event$1;->$timeNanos:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .line 136
    new-instance v0, Lcom/squareup/tmn/Event;

    iget-object v1, p0, Lcom/squareup/tmn/TmnTimings$event$1;->$what:Lcom/squareup/tmn/What;

    iget-wide v2, p0, Lcom/squareup/tmn/TmnTimings$event$1;->$timeNanos:J

    iget-object v4, p0, Lcom/squareup/tmn/TmnTimings$event$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v4}, Lcom/squareup/tmn/TmnTimings;->access$getCurrentPhase$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/tmn/Phase;

    move-result-object v4

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/squareup/tmn/Event;-><init>(Lcom/squareup/tmn/What;Lcom/squareup/tmn/Phase;J)V

    .line 137
    iget-object v1, p0, Lcom/squareup/tmn/TmnTimings$event$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v1}, Lcom/squareup/tmn/TmnTimings;->access$getVerbose$p(Lcom/squareup/tmn/TmnTimings;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  Tmn Event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/squareup/tmn/TmnTimings$event$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v1}, Lcom/squareup/tmn/TmnTimings;->access$getEvents$p(Lcom/squareup/tmn/TmnTimings;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method
