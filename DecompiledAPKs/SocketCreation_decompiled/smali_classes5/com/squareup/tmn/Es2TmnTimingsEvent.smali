.class public final Lcom/squareup/tmn/Es2TmnTimingsEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "TmnTimings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u000c\n\u0002\u0010\u000e\n\u0002\u0008\'\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0080\u0008\u0018\u00002\u00020\u0001B\u0087\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\u0006\u0012\u0006\u0010\t\u001a\u00020\u0006\u0012\u0006\u0010\n\u001a\u00020\u0006\u0012\u0006\u0010\u000b\u001a\u00020\u0006\u0012\u0006\u0010\u000c\u001a\u00020\u0006\u0012\u0006\u0010\r\u001a\u00020\u0006\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0006\u0012\u0006\u0010\u0010\u001a\u00020\u0006\u0012\u0006\u0010\u0011\u001a\u00020\u0003\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u0010\u0015J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\u0006H\u00c6\u0003J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\t\u0010,\u001a\u00020\u0006H\u00c6\u0003J\t\u0010-\u001a\u00020\u0006H\u00c6\u0003J\t\u0010.\u001a\u00020\u0003H\u00c6\u0003J\t\u0010/\u001a\u00020\u0013H\u00c6\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\u0013H\u00c6\u0003J\t\u00101\u001a\u00020\u0003H\u00c6\u0003J\t\u00102\u001a\u00020\u0006H\u00c6\u0003J\t\u00103\u001a\u00020\u0006H\u00c6\u0003J\t\u00104\u001a\u00020\u0006H\u00c6\u0003J\t\u00105\u001a\u00020\u0006H\u00c6\u0003J\t\u00106\u001a\u00020\u0006H\u00c6\u0003J\t\u00107\u001a\u00020\u0006H\u00c6\u0003J\t\u00108\u001a\u00020\u0006H\u00c6\u0003J\u00ab\u0001\u00109\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00062\u0008\u0008\u0002\u0010\t\u001a\u00020\u00062\u0008\u0008\u0002\u0010\n\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00062\u0008\u0008\u0002\u0010\r\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00132\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0013H\u00c6\u0001J\u0013\u0010:\u001a\u00020;2\u0008\u0010<\u001a\u0004\u0018\u00010=H\u00d6\u0003J\t\u0010>\u001a\u00020\u0003H\u00d6\u0001J\t\u0010?\u001a\u00020\u0013H\u00d6\u0001R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0019R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001cR\u0011\u0010\u0008\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001cR\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001cR\u0011\u0010\n\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001cR\u0011\u0010\u000b\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u001cR\u0011\u0010\u000c\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u001cR\u0011\u0010\r\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\u001cR\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\u0019R\u0011\u0010\u000f\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u001cR\u0011\u0010\u0010\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u001cR\u0011\u0010\u0011\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010\u0019R\u0011\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u0017\u00a8\u0006@"
    }
    d2 = {
        "Lcom/squareup/tmn/Es2TmnTimingsEvent;",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "emoney_transaction_metrics_ble_connection_interval_millis",
        "",
        "emoney_transaction_metrics_ble_mtu",
        "emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis",
        "",
        "emoney_transaction_metrics_tap_to_beep_ble_read_max_millis",
        "emoney_transaction_metrics_tap_to_beep_ble_read_millis",
        "emoney_transaction_metrics_tap_to_beep_ble_write_millis",
        "emoney_transaction_metrics_tap_to_beep_client_millis",
        "emoney_transaction_metrics_tap_to_beep_lcr_millis",
        "emoney_transaction_metrics_tap_to_beep_millis",
        "emoney_transaction_metrics_tap_to_beep_server_millis",
        "emoney_transaction_metrics_tap_to_beep_server_request_count",
        "emoney_transaction_metrics_total_ble_write_millis",
        "emoney_transaction_metrics_total_server_millis",
        "emoney_transaction_metrics_total_server_request_count",
        "emoney_transaction_metrics_transaction_id",
        "",
        "connection_network_type",
        "(IIJJJJJJJJIJJILjava/lang/String;Ljava/lang/String;)V",
        "getConnection_network_type",
        "()Ljava/lang/String;",
        "getEmoney_transaction_metrics_ble_connection_interval_millis",
        "()I",
        "getEmoney_transaction_metrics_ble_mtu",
        "getEmoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis",
        "()J",
        "getEmoney_transaction_metrics_tap_to_beep_ble_read_max_millis",
        "getEmoney_transaction_metrics_tap_to_beep_ble_read_millis",
        "getEmoney_transaction_metrics_tap_to_beep_ble_write_millis",
        "getEmoney_transaction_metrics_tap_to_beep_client_millis",
        "getEmoney_transaction_metrics_tap_to_beep_lcr_millis",
        "getEmoney_transaction_metrics_tap_to_beep_millis",
        "getEmoney_transaction_metrics_tap_to_beep_server_millis",
        "getEmoney_transaction_metrics_tap_to_beep_server_request_count",
        "getEmoney_transaction_metrics_total_ble_write_millis",
        "getEmoney_transaction_metrics_total_server_millis",
        "getEmoney_transaction_metrics_total_server_request_count",
        "getEmoney_transaction_metrics_transaction_id",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "tmn-timings_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connection_network_type:Ljava/lang/String;

.field private final emoney_transaction_metrics_ble_connection_interval_millis:I

.field private final emoney_transaction_metrics_ble_mtu:I

.field private final emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

.field private final emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

.field private final emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

.field private final emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

.field private final emoney_transaction_metrics_tap_to_beep_client_millis:J

.field private final emoney_transaction_metrics_tap_to_beep_lcr_millis:J

.field private final emoney_transaction_metrics_tap_to_beep_millis:J

.field private final emoney_transaction_metrics_tap_to_beep_server_millis:J

.field private final emoney_transaction_metrics_tap_to_beep_server_request_count:I

.field private final emoney_transaction_metrics_total_ble_write_millis:J

.field private final emoney_transaction_metrics_total_server_millis:J

.field private final emoney_transaction_metrics_total_server_request_count:I

.field private final emoney_transaction_metrics_transaction_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIJJJJJJJJIJJILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    move-object v0, p0

    move-object/from16 v1, p25

    const-string v2, "emoney_transaction_metrics_transaction_id"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "emoney_transaction_metrics"

    .line 371
    invoke-direct {p0, v2}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    move v2, p1

    iput v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_connection_interval_millis:I

    move v2, p2

    iput v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_mtu:I

    move-wide v2, p3

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

    move-wide v2, p5

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

    move-wide v2, p7

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

    move-wide v2, p9

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

    move-wide v2, p11

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_client_millis:J

    move-wide/from16 v2, p13

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_lcr_millis:J

    move-wide/from16 v2, p15

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_millis:J

    move-wide/from16 v2, p17

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_millis:J

    move/from16 v2, p19

    iput v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_request_count:I

    move-wide/from16 v2, p20

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_ble_write_millis:J

    move-wide/from16 v2, p22

    iput-wide v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_millis:J

    move/from16 v2, p24

    iput v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_request_count:I

    iput-object v1, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_transaction_id:Ljava/lang/String;

    move-object/from16 v1, p26

    iput-object v1, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->connection_network_type:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tmn/Es2TmnTimingsEvent;IIJJJJJJJJIJJILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/tmn/Es2TmnTimingsEvent;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p27

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_connection_interval_millis:I

    goto :goto_0

    :cond_0
    move/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_mtu:I

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-wide v4, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

    goto :goto_2

    :cond_2
    move-wide/from16 v4, p3

    :goto_2
    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_3

    iget-wide v6, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

    goto :goto_3

    :cond_3
    move-wide/from16 v6, p5

    :goto_3
    and-int/lit8 v8, v1, 0x10

    if-eqz v8, :cond_4

    iget-wide v8, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

    goto :goto_4

    :cond_4
    move-wide/from16 v8, p7

    :goto_4
    and-int/lit8 v10, v1, 0x20

    if-eqz v10, :cond_5

    iget-wide v10, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

    goto :goto_5

    :cond_5
    move-wide/from16 v10, p9

    :goto_5
    and-int/lit8 v12, v1, 0x40

    if-eqz v12, :cond_6

    iget-wide v12, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_client_millis:J

    goto :goto_6

    :cond_6
    move-wide/from16 v12, p11

    :goto_6
    and-int/lit16 v14, v1, 0x80

    if-eqz v14, :cond_7

    iget-wide v14, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_lcr_millis:J

    goto :goto_7

    :cond_7
    move-wide/from16 v14, p13

    :goto_7
    move-wide/from16 p13, v14

    and-int/lit16 v14, v1, 0x100

    if-eqz v14, :cond_8

    iget-wide v14, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_millis:J

    goto :goto_8

    :cond_8
    move-wide/from16 v14, p15

    :goto_8
    move-wide/from16 p15, v14

    and-int/lit16 v14, v1, 0x200

    if-eqz v14, :cond_9

    iget-wide v14, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_millis:J

    goto :goto_9

    :cond_9
    move-wide/from16 v14, p17

    :goto_9
    move-wide/from16 p17, v14

    and-int/lit16 v14, v1, 0x400

    if-eqz v14, :cond_a

    iget v14, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_request_count:I

    goto :goto_a

    :cond_a
    move/from16 v14, p19

    :goto_a
    and-int/lit16 v15, v1, 0x800

    move/from16 p19, v14

    if-eqz v15, :cond_b

    iget-wide v14, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_ble_write_millis:J

    goto :goto_b

    :cond_b
    move-wide/from16 v14, p20

    :goto_b
    move-wide/from16 p20, v14

    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-wide v14, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_millis:J

    goto :goto_c

    :cond_c
    move-wide/from16 v14, p22

    :goto_c
    move-wide/from16 p22, v14

    and-int/lit16 v14, v1, 0x2000

    if-eqz v14, :cond_d

    iget v14, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_request_count:I

    goto :goto_d

    :cond_d
    move/from16 v14, p24

    :goto_d
    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget-object v15, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_transaction_id:Ljava/lang/String;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p25

    :goto_e
    const v16, 0x8000

    and-int v1, v1, v16

    if-eqz v1, :cond_f

    iget-object v1, v0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->connection_network_type:Ljava/lang/String;

    goto :goto_f

    :cond_f
    move-object/from16 v1, p26

    :goto_f
    move/from16 p1, v2

    move/from16 p2, v3

    move-wide/from16 p3, v4

    move-wide/from16 p5, v6

    move-wide/from16 p7, v8

    move-wide/from16 p9, v10

    move-wide/from16 p11, v12

    move/from16 p24, v14

    move-object/from16 p25, v15

    move-object/from16 p26, v1

    invoke-virtual/range {p0 .. p26}, Lcom/squareup/tmn/Es2TmnTimingsEvent;->copy(IIJJJJJJJJIJJILjava/lang/String;Ljava/lang/String;)Lcom/squareup/tmn/Es2TmnTimingsEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_connection_interval_millis:I

    return v0
.end method

.method public final component10()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_millis:J

    return-wide v0
.end method

.method public final component11()I
    .locals 1

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_request_count:I

    return v0
.end method

.method public final component12()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_ble_write_millis:J

    return-wide v0
.end method

.method public final component13()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_millis:J

    return-wide v0
.end method

.method public final component14()I
    .locals 1

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_request_count:I

    return v0
.end method

.method public final component15()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_transaction_id:Ljava/lang/String;

    return-object v0
.end method

.method public final component16()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->connection_network_type:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_mtu:I

    return v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

    return-wide v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

    return-wide v0
.end method

.method public final component5()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

    return-wide v0
.end method

.method public final component6()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

    return-wide v0
.end method

.method public final component7()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_client_millis:J

    return-wide v0
.end method

.method public final component8()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_lcr_millis:J

    return-wide v0
.end method

.method public final component9()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_millis:J

    return-wide v0
.end method

.method public final copy(IIJJJJJJJJIJJILjava/lang/String;Ljava/lang/String;)Lcom/squareup/tmn/Es2TmnTimingsEvent;
    .locals 28

    move/from16 v1, p1

    move/from16 v2, p2

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    move-wide/from16 v11, p11

    move-wide/from16 v13, p13

    move-wide/from16 v15, p15

    move-wide/from16 v17, p17

    move/from16 v19, p19

    move-wide/from16 v20, p20

    move-wide/from16 v22, p22

    move/from16 v24, p24

    move-object/from16 v25, p25

    move-object/from16 v26, p26

    const-string v0, "emoney_transaction_metrics_transaction_id"

    move-object/from16 v1, p25

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v27, Lcom/squareup/tmn/Es2TmnTimingsEvent;

    move-object/from16 v0, v27

    move/from16 v1, p1

    invoke-direct/range {v0 .. v26}, Lcom/squareup/tmn/Es2TmnTimingsEvent;-><init>(IIJJJJJJJJIJJILjava/lang/String;Ljava/lang/String;)V

    return-object v27
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_connection_interval_millis:I

    iget v1, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_connection_interval_millis:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_mtu:I

    iget v1, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_mtu:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_client_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_client_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_lcr_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_lcr_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_request_count:I

    iget v1, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_request_count:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_ble_write_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_ble_write_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_millis:J

    iget-wide v2, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_millis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_request_count:I

    iget v1, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_request_count:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_transaction_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_transaction_id:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->connection_network_type:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/tmn/Es2TmnTimingsEvent;->connection_network_type:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConnection_network_type()Ljava/lang/String;
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->connection_network_type:Ljava/lang/String;

    return-object v0
.end method

.method public final getEmoney_transaction_metrics_ble_connection_interval_millis()I
    .locals 1

    .line 355
    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_connection_interval_millis:I

    return v0
.end method

.method public final getEmoney_transaction_metrics_ble_mtu()I
    .locals 1

    .line 356
    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_mtu:I

    return v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis()J
    .locals 2

    .line 357
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_ble_read_max_millis()J
    .locals 2

    .line 358
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_ble_read_millis()J
    .locals 2

    .line 359
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_ble_write_millis()J
    .locals 2

    .line 360
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_client_millis()J
    .locals 2

    .line 361
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_client_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_lcr_millis()J
    .locals 2

    .line 362
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_lcr_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_millis()J
    .locals 2

    .line 363
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_server_millis()J
    .locals 2

    .line 364
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_tap_to_beep_server_request_count()I
    .locals 1

    .line 365
    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_request_count:I

    return v0
.end method

.method public final getEmoney_transaction_metrics_total_ble_write_millis()J
    .locals 2

    .line 366
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_ble_write_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_total_server_millis()J
    .locals 2

    .line 367
    iget-wide v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_millis:J

    return-wide v0
.end method

.method public final getEmoney_transaction_metrics_total_server_request_count()I
    .locals 1

    .line 368
    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_request_count:I

    return v0
.end method

.method public final getEmoney_transaction_metrics_transaction_id()Ljava/lang/String;
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_transaction_id:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_connection_interval_millis:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_mtu:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_client_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_lcr_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_request_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_ble_write_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_millis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_request_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_transaction_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->connection_network_type:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Es2TmnTimingsEvent(emoney_transaction_metrics_ble_connection_interval_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_connection_interval_millis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_ble_mtu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_ble_mtu:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_pos_stack_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_ble_read_max_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_max_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_ble_read_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_read_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_ble_write_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_ble_write_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_client_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_client_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_lcr_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_lcr_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_server_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_tap_to_beep_server_request_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_tap_to_beep_server_request_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_total_ble_write_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_ble_write_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_total_server_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_millis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_total_server_request_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_total_server_request_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", emoney_transaction_metrics_transaction_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->emoney_transaction_metrics_transaction_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", connection_network_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/Es2TmnTimingsEvent;->connection_network_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
