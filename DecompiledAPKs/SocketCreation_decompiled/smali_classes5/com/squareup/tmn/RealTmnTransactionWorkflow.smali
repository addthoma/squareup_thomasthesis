.class public final Lcom/squareup/tmn/RealTmnTransactionWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Lcom/squareup/tmn/TmnTransactionWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;,
        Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;,
        Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;,
        Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;,
        Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/tmn/TmnTransactionInput;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "Lkotlin/Unit;",
        ">;",
        "Lcom/squareup/tmn/TmnTransactionWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTmnTransactionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTmnTransactionWorkflow.kt\ncom/squareup/tmn/RealTmnTransactionWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n*L\n1#1,786:1\n41#2:787\n56#2,2:788\n85#2:791\n85#2:794\n41#2:797\n56#2,2:798\n41#2:801\n56#2,2:802\n276#3:790\n276#3:793\n276#3:796\n276#3:800\n276#3:804\n240#4:792\n240#4:795\n*E\n*S KotlinDebug\n*F\n+ 1 RealTmnTransactionWorkflow.kt\ncom/squareup/tmn/RealTmnTransactionWorkflow\n*L\n119#1:787\n119#1,2:788\n483#1:791\n528#1:794\n597#1:797\n597#1,2:798\n639#1:801\n639#1,2:802\n119#1:790\n483#1:793\n528#1:796\n597#1:800\n639#1:804\n483#1:792\n528#1:795\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0089\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007*\u0001 \u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00012\u00020\u0006:\u0005ijklmBQ\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0008\u0008\u0001\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019J\u0016\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#2\u0006\u0010%\u001a\u00020&H\u0002J0\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040(2\u0006\u0010)\u001a\u00020\u00032\u0006\u0010*\u001a\u00020+2\n\u0008\u0002\u0010,\u001a\u0004\u0018\u00010\u0004H\u0002J\u0008\u0010-\u001a\u00020.H\u0002J\u0018\u0010/\u001a\u00020.2\u0006\u00100\u001a\u00020\u00032\u0006\u00101\u001a\u000202H\u0002J0\u00103\u001a\u0008\u0012\u0004\u0012\u0002040#2\u0006\u0010%\u001a\u00020&2\u0006\u00105\u001a\u0002062\u0006\u00107\u001a\u0002082\u0008\u00109\u001a\u0004\u0018\u00010:H\u0002J \u0010;\u001a\u00020<2\u0008\u0010=\u001a\u0004\u0018\u00010>2\u000c\u0010?\u001a\u0008\u0012\u0004\u0012\u00020A0@H\u0002J\u000e\u0010B\u001a\u0008\u0012\u0004\u0012\u0002020#H\u0002J\u001c\u0010C\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040(2\u0006\u0010D\u001a\u00020EH\u0002J$\u0010F\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040(2\u0006\u0010D\u001a\u00020G2\u0006\u0010H\u001a\u000204H\u0002J$\u0010I\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040(2\u0006\u0010D\u001a\u00020G2\u0006\u0010J\u001a\u00020KH\u0002J$\u0010L\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040(2\u0006\u0010M\u001a\u00020A2\u0006\u0010D\u001a\u00020NH\u0002J\u001a\u0010O\u001a\u00020\u00032\u0006\u0010P\u001a\u00020\u00022\u0008\u0010Q\u001a\u0004\u0018\u00010RH\u0016J\u0012\u0010S\u001a\u00020\u001b2\u0008\u0010T\u001a\u0004\u0018\u00010UH\u0002J \u0010V\u001a\u00020\u00032\u0006\u0010W\u001a\u00020\u00022\u0006\u0010X\u001a\u00020\u00022\u0006\u0010D\u001a\u00020\u0003H\u0016J,\u0010Y\u001a\u00020\u00052\u0006\u0010P\u001a\u00020\u00022\u0006\u0010D\u001a\u00020\u00032\u0012\u0010Z\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040[H\u0016J:\u0010\\\u001a\u00020\u00052\u0012\u0010Z\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040[2\u0006\u0010%\u001a\u00020&2\u0008\u0008\u0002\u0010]\u001a\u00020.2\n\u0008\u0002\u0010^\u001a\u0004\u0018\u00010.H\u0002J.\u0010_\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040(2\u0006\u0010]\u001a\u00020.2\u0008\u0010`\u001a\u0004\u0018\u00010.2\u0006\u0010a\u001a\u00020\u0004H\u0002J.\u0010b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020A0c0#2\u0008\u0010`\u001a\u0004\u0018\u00010.2\u0006\u0010d\u001a\u00020:2\u0006\u0010]\u001a\u00020.H\u0002J(\u0010e\u001a\u0008\u0012\u0004\u0012\u00020\u001b0#2\u0008\u0010`\u001a\u0004\u0018\u00010.2\u0006\u0010f\u001a\u00020g2\u0006\u0010]\u001a\u00020.H\u0002J\u0010\u0010h\u001a\u00020R2\u0006\u0010D\u001a\u00020\u0003H\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001cR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0010\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010!R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006n"
    }
    d2 = {
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/tmn/TmnTransactionInput;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "",
        "Lcom/squareup/tmn/TmnTransactionWorkflow;",
        "felicaService",
        "Lcom/squareup/server/felica/FelicaService;",
        "cardReaderHelper",
        "Lcom/squareup/tmn/CardReaderHelper;",
        "tmnAudioPlayer",
        "Lcom/squareup/tmn/audio/TmnAudioPlayer;",
        "tmnObservablesHelper",
        "Lcom/squareup/tmn/TmnObservablesHelper;",
        "analytics",
        "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
        "miryoWorkerDelayer",
        "Lcom/squareup/tmn/MiryoWorkerDelayer;",
        "timings",
        "Lcom/squareup/tmn/TmnTimings;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/server/felica/FelicaService;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/audio/TmnAudioPlayer;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;Lcom/squareup/tmn/MiryoWorkerDelayer;Lcom/squareup/tmn/TmnTimings;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;)V",
        "enableFelicaCertEnv",
        "",
        "Ljava/lang/Boolean;",
        "getMiryoWorkerDelayer",
        "()Lcom/squareup/tmn/MiryoWorkerDelayer;",
        "teardownWorker",
        "com/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;",
        "audioWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
        "brandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "enterStateAndLog",
        "Lcom/squareup/workflow/WorkflowAction;",
        "newState",
        "tmnEventValue",
        "Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;",
        "emittingOutput",
        "generateTransactionId",
        "",
        "getIllegalStateMessage",
        "currentState",
        "currentEvent",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "getPaymentStarterWorker",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;",
        "transactionType",
        "Lcom/squareup/tmn/TmnTransactionType;",
        "amountAuthorized",
        "",
        "miryoData",
        "",
        "getProxyMessageResponseFailure",
        "Lcom/squareup/tmn/TmnTransactionOutput$Failed;",
        "afterWriteNotifyData",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;",
        "receiver",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
        "getTmnWorker",
        "handleNetworkErrorTransactionStatusToServer",
        "state",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;",
        "handlePaymentStartSuccessful",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;",
        "paymentStarterOutput",
        "handleReceivedInitialTmnDataFromLcr",
        "onTmnDataToTmnEvent",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;",
        "handleSuccessSendProxyMessageToServer",
        "proxyMessageResponse",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "isSuccess",
        "tmnTransactionResult",
        "Lcom/squareup/cardreader/lcr/TmnTransactionResult;",
        "onPropsChanged",
        "old",
        "new",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "runAndLogAudioWorker",
        "transactionId",
        "connectionId",
        "sendFailureTransactionStatusToServer",
        "connId",
        "tmnResult",
        "sendProxyMessageToServer",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "tmnData",
        "sendTransactionStatusToServer",
        "status",
        "Lcom/squareup/protos/client/felica/Status;",
        "snapshotState",
        "ActiveCardReaderDisconnectedWorker",
        "AfterWriteNotifyData",
        "PaymentInfo",
        "PaymentStarterOutput",
        "TmnTransactionState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

.field private final cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

.field private final enableFelicaCertEnv:Ljava/lang/Boolean;

.field private final felicaService:Lcom/squareup/server/felica/FelicaService;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final miryoWorkerDelayer:Lcom/squareup/tmn/MiryoWorkerDelayer;

.field private final teardownWorker:Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;

.field private final timings:Lcom/squareup/tmn/TmnTimings;

.field private final tmnAudioPlayer:Lcom/squareup/tmn/audio/TmnAudioPlayer;

.field private final tmnObservablesHelper:Lcom/squareup/tmn/TmnObservablesHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/server/felica/FelicaService;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/audio/TmnAudioPlayer;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;Lcom/squareup/tmn/MiryoWorkerDelayer;Lcom/squareup/tmn/TmnTimings;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p8    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "felicaService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tmnAudioPlayer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tmnObservablesHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "miryoWorkerDelayer"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timings"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->felicaService:Lcom/squareup/server/felica/FelicaService;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->tmnAudioPlayer:Lcom/squareup/tmn/audio/TmnAudioPlayer;

    iput-object p4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->tmnObservablesHelper:Lcom/squareup/tmn/TmnObservablesHelper;

    iput-object p5, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    iput-object p6, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->miryoWorkerDelayer:Lcom/squareup/tmn/MiryoWorkerDelayer;

    iput-object p7, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->timings:Lcom/squareup/tmn/TmnTimings;

    iput-object p8, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->mainScheduler:Lio/reactivex/Scheduler;

    .line 109
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_FELICA_CERTIFICATION_ENVIRONMENT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p9, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enableFelicaCertEnv:Ljava/lang/Boolean;

    .line 124
    new-instance p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;

    invoke-direct {p1, p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)V

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->teardownWorker:Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;

    return-void
.end method

.method public static final synthetic access$enterStateAndLog(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$generateTransactionId(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Ljava/lang/String;
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->generateTransactionId()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    return-object p0
.end method

.method public static final synthetic access$getCardReaderHelper$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/CardReaderHelper;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

    return-object p0
.end method

.method public static final synthetic access$getEnableFelicaCertEnv$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Ljava/lang/Boolean;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enableFelicaCertEnv:Ljava/lang/Boolean;

    return-object p0
.end method

.method public static final synthetic access$getFelicaService$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/server/felica/FelicaService;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->felicaService:Lcom/squareup/server/felica/FelicaService;

    return-object p0
.end method

.method public static final synthetic access$getIllegalStateMessage(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/dipper/events/TmnEvent;)Ljava/lang/String;
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getIllegalStateMessage(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/dipper/events/TmnEvent;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getMainScheduler$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lio/reactivex/Scheduler;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getProxyMessageResponseFailure(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/tmn/TmnTransactionOutput$Failed;
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getProxyMessageResponseFailure(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTimings$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTimings;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->timings:Lcom/squareup/tmn/TmnTimings;

    return-object p0
.end method

.method public static final synthetic access$getTmnAudioPlayer$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/audio/TmnAudioPlayer;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->tmnAudioPlayer:Lcom/squareup/tmn/audio/TmnAudioPlayer;

    return-object p0
.end method

.method public static final synthetic access$handleNetworkErrorTransactionStatusToServer(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->handleNetworkErrorTransactionStatusToServer(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handlePaymentStartSuccessful(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->handlePaymentStartSuccessful(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleReceivedInitialTmnDataFromLcr(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->handleReceivedInitialTmnDataFromLcr(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleSuccessSendProxyMessageToServer(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/protos/client/felica/ProxyMessageResponse;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->handleSuccessSendProxyMessageToServer(Lcom/squareup/protos/client/felica/ProxyMessageResponse;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$isSuccess(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Z
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->isSuccess(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$sendFailureTransactionStatusToServer(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->sendFailureTransactionStatusToServer(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final audioWorker(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
            ">;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->tmnObservablesHelper:Lcom/squareup/tmn/TmnObservablesHelper;

    invoke-interface {v0}, Lcom/squareup/tmn/TmnObservablesHelper;->getAudioRequestObservable()Lio/reactivex/Observable;

    move-result-object v0

    .line 116
    new-instance v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$audioWorker$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$audioWorker$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 117
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$audioWorker$2;

    invoke-direct {v0, p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$audioWorker$2;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 118
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$audioWorker$3;

    invoke-direct {v0, p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$audioWorker$3;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)V

    check-cast v0, Lio/reactivex/functions/Action;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->doOnDispose(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object p1

    const-string/jumbo v0, "tmnObservablesHelper.get\u2026nAudioPlayer.shutDown() }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 787
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string/jumbo v0, "this.toFlowable(BUFFER)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 789
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 790
    const-class v0, Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1

    .line 789
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final enterStateAndLog(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    .line 683
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TMN Transaction Workflow State Transition: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "Reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    invoke-virtual {p2}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 685
    invoke-static {p2, v0}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 686
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnStateTransitionEvent;

    invoke-direct {v1, p2}, Lcom/squareup/tmn/logging/TmnEvents$TmnStateTransitionEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 687
    sget-object p2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p2, p1, p3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method static synthetic enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 680
    check-cast p3, Lcom/squareup/tmn/TmnTransactionOutput;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final generateTransactionId()Ljava/lang/String;
    .locals 7

    .line 661
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    .line 662
    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v0, "UUID.randomUUID()\n        .toString()"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "-"

    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    .line 663
    invoke-static/range {v1 .. v6}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getIllegalStateMessage(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/dipper/events/TmnEvent;)Ljava/lang/String;
    .locals 2

    .line 672
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received unsupported event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " in state: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getPaymentStarterWorker(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionType;J[B)Lcom/squareup/workflow/Worker;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
            "Lcom/squareup/tmn/TmnTransactionType;",
            "J[B)",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;",
            ">;"
        }
    .end annotation

    .line 620
    new-instance v7, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$getPaymentStarterWorker$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionType;J[B)V

    check-cast v7, Ljava/util/concurrent/Callable;

    invoke-static {v7}, Lio/reactivex/Observable;->defer(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "Observable.defer {\n     \u2026er, transactionId))\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 801
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string/jumbo p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 803
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 804
    const-class p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    return-object p3

    .line 803
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final getProxyMessageResponseFailure(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/tmn/TmnTransactionOutput$Failed;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
            ">;)",
            "Lcom/squareup/tmn/TmnTransactionOutput$Failed;"
        }
    .end annotation

    .line 534
    instance-of v0, p2, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 536
    check-cast p2, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/felica/ProxyMessageResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/felica/ProxyMessageResponse;->error:Lcom/squareup/protos/client/felica/Error;

    if-eqz v0, :cond_0

    .line 537
    invoke-virtual {p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/felica/ProxyMessageResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/felica/ProxyMessageResponse;->error:Lcom/squareup/protos/client/felica/Error;

    invoke-virtual {p2}, Lcom/squareup/protos/client/felica/Error;->toString()Ljava/lang/String;

    move-result-object p2

    .line 538
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TMN: sendProxyMessageToServer failure non-null error - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 539
    new-instance v0, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    invoke-direct {v0, p1, p2}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    goto :goto_0

    .line 541
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/felica/ProxyMessageResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/felica/ProxyMessageResponse;->packet_data:Lokio/ByteString;

    if-nez p2, :cond_1

    new-array p2, v1, [Ljava/lang/Object;

    const-string v0, "TMN: sendProxyMessageToServer failure null packet data"

    .line 542
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 543
    new-instance v0, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    const-string p2, "Null packet data response"

    invoke-direct {v0, p1, p2}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-array p2, v1, [Ljava/lang/Object;

    const-string v0, "TMN: sendProxyMessageToServer failure unknown"

    .line 546
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 547
    new-instance v0, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    const-string p2, "Rejected server call"

    invoke-direct {v0, p1, p2}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    .line 535
    :goto_0
    check-cast v0, Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    goto :goto_1

    :cond_2
    new-array p2, v1, [Ljava/lang/Object;

    const-string v0, "TMN: sendProxyMessageToServer failure unknown - Network failure"

    .line 552
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 553
    new-instance p2, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    const-string v0, "Network failure"

    invoke-direct {p2, p1, v0}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    move-object v0, p2

    check-cast v0, Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    :goto_1
    return-object v0
.end method

.method private final getTmnWorker()Lcom/squareup/workflow/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/dipper/events/TmnEvent;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lio/reactivex/ObservableSource;

    .line 593
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->tmnObservablesHelper:Lcom/squareup/tmn/TmnObservablesHelper;

    invoke-interface {v1}, Lcom/squareup/tmn/TmnObservablesHelper;->getOnTmnDataToTmnObservable()Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 594
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->tmnObservablesHelper:Lcom/squareup/tmn/TmnObservablesHelper;

    invoke-interface {v1}, Lcom/squareup/tmn/TmnObservablesHelper;->getOnTmnCompletionObservable()Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 595
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->tmnObservablesHelper:Lcom/squareup/tmn/TmnObservablesHelper;

    invoke-interface {v1}, Lcom/squareup/tmn/TmnObservablesHelper;->getOnTmnWriteNotifyObservable()Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 592
    invoke-static {v0}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026eNotifyObservable()\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 797
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v1, "this.toFlowable(BUFFER)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_0

    .line 799
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 800
    const-class v1, Lcom/squareup/dipper/events/TmnEvent;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v2, Lcom/squareup/workflow/Worker;

    return-object v2

    .line 799
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final handleNetworkErrorTransactionStatusToServer(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    .line 387
    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;->getTmnTransactionOutput()Lcom/squareup/tmn/TmnTransactionOutput;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;->getTmnTransactionOutput()Lcom/squareup/tmn/TmnTransactionOutput;

    move-result-object v0

    check-cast v0, Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    invoke-virtual {v0}, Lcom/squareup/tmn/TmnTransactionOutput$Failed;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 390
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 391
    sget-object v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    new-instance v2, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    .line 392
    invoke-virtual {p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;->getTmnTransactionOutput()Lcom/squareup/tmn/TmnTransactionOutput;

    move-result-object p1

    check-cast p1, Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionOutput$Failed;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object p1

    const-string v3, "Error setting transaction status to failure after write notify"

    .line 391
    invoke-direct {v2, p1, v3}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    .line 390
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 397
    :cond_0
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 398
    sget-object v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    new-instance v1, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;

    const/4 v2, 0x0

    const-string v3, "Error setting transaction status"

    invoke-direct {v1, v2, v3}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$NetworkError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;)V

    .line 397
    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final handlePaymentStartSuccessful(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    .line 412
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getStoredPacketData()[B

    move-result-object v0

    if-eqz v0, :cond_1

    .line 414
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    .line 415
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getStoredPacketData()[B

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    .line 416
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, v0

    .line 414
    invoke-direct/range {v1 .. v8}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;-><init>([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v10, v0

    check-cast v10, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 417
    sget-object v11, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    move-object v9, p0

    .line 413
    invoke-static/range {v9 .. v14}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 421
    :cond_1
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;

    .line 422
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getPaymentInfo()Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentStarterOutput;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v0

    .line 421
    invoke-direct/range {v1 .. v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;Ljava/lang/String;Lcom/squareup/cardreader/CardReader;[BILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v9, v0

    check-cast v9, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 424
    sget-object v10, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->CARDREADER_START_SUCCESS:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object v8, p0

    .line 420
    invoke-static/range {v8 .. v13}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final handleReceivedInitialTmnDataFromLcr(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    .line 439
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 441
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    .line 442
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->getTmnData()[B

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, v0

    .line 441
    invoke-direct/range {v1 .. v8}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;-><init>([BLjava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v10, v0

    check-cast v10, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 443
    sget-object v11, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    move-object v9, p0

    .line 440
    invoke-static/range {v9 .. v14}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 447
    :cond_0
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;

    .line 448
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getPaymentInfo()Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->getTmnData()[B

    move-result-object v5

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v0

    .line 447
    invoke-direct/range {v1 .. v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;Ljava/lang/String;Lcom/squareup/cardreader/CardReader;[BILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v9, v0

    check-cast v9, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 449
    sget-object v10, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object v8, p0

    .line 446
    invoke-static/range {v8 .. v13}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final handleSuccessSendProxyMessageToServer(Lcom/squareup/protos/client/felica/ProxyMessageResponse;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;)Lcom/squareup/workflow/WorkflowAction;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    .line 561
    iget-object v0, p1, Lcom/squareup/protos/client/felica/ProxyMessageResponse;->packet_data:Lokio/ByteString;

    invoke-virtual {v0}, Lokio/ByteString;->toByteArray()[B

    move-result-object v0

    .line 563
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TMN: Sending server response to LCR\n encrypted bytes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    sget-object v2, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    array-length v3, v0

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lokio/ByteString$Companion;->of([B)Lokio/ByteString;

    move-result-object v2

    invoke-virtual {v2}, Lokio/ByteString;->hex()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 562
    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 567
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->analytics:Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    .line 568
    new-instance v11, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 569
    sget-object v2, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SENDING_READER_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v2}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getTransactionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getConnId()Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x34

    const/4 v10, 0x0

    move-object v2, v11

    .line 568
    invoke-direct/range {v2 .. v10}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v11, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 567
    invoke-virtual {v1, v11}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 573
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/tmn/CardReaderHelper;->sendTmnDataToCardReader$impl_release([B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 575
    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getTransactionId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getConnId()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;

    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/squareup/tmn/TmnTransactionOutput$Failed$CardReaderError;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V

    check-cast v1, Lcom/squareup/tmn/TmnTransactionOutput;

    .line 574
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->sendFailureTransactionStatusToServer(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 579
    :cond_0
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    .line 580
    iget-object p1, p1, Lcom/squareup/protos/client/felica/ProxyMessageResponse;->connection_id:Ljava/lang/String;

    const-string v1, "proxyMessageResponse.connection_id"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getTransactionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v2

    .line 581
    invoke-virtual {p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object p2

    .line 579
    invoke-direct {v0, p1, v1, v2, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/CardReader;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;)V

    move-object v4, v0

    check-cast v4, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 582
    sget-object v5, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->NETWORK_SUCCESS:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    .line 578
    invoke-static/range {v3 .. v8}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final isSuccess(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Z
    .locals 1

    .line 383
    sget-object v0, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->TMN_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final runAndLogAudioWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "-",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;",
            "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 699
    invoke-direct {p0, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->audioWorker(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    new-instance p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;

    invoke-direct {p2, p0, p3, p4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$runAndLogAudioWorker$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, p2

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic runAndLogAudioWorker$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    const-string p3, ""

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    const/4 p4, 0x0

    .line 697
    check-cast p4, Ljava/lang/String;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->runAndLogAudioWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final sendFailureTransactionStatusToServer(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "TMN: Something went wrong. Entering terminal state."

    .line 649
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 651
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;

    sget-object v1, Lcom/squareup/protos/client/felica/Status;->FAILURE:Lcom/squareup/protos/client/felica/Status;

    invoke-direct {v0, v1, p3, p1, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;-><init>(Lcom/squareup/protos/client/felica/Status;Lcom/squareup/tmn/TmnTransactionOutput;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    .line 652
    sget-object v4, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->NETWORK_FAILURE:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    .line 650
    invoke-static/range {v2 .. v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->enterStateAndLog$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final sendProxyMessageToServer(Ljava/lang/String;[BLjava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[B",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
            ">;>;"
        }
    .end annotation

    .line 489
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;[BLjava/lang/String;Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->defer(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.defer {\n    val t\u2026inScheduler\n        )\n  }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 794
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$$inlined$asWorker$1;

    const/4 p3, 0x0

    invoke-direct {p2, p1, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendProxyMessageToServer$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 795
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 796
    const-class p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    sget-object p3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v0, Lcom/squareup/protos/client/felica/ProxyMessageResponse;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    invoke-virtual {p3, v0}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object p3

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    return-object p3
.end method

.method private final sendTransactionStatusToServer(Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/felica/Status;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 458
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->defer(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.defer {\n    val r\u2026          }\n        }\n  }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 791
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$$inlined$asWorker$1;

    const/4 p3, 0x0

    invoke-direct {p2, p1, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 792
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 793
    sget-object p2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    return-object p3
.end method


# virtual methods
.method public final getMiryoWorkerDelayer()Lcom/squareup/tmn/MiryoWorkerDelayer;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->miryoWorkerDelayer:Lcom/squareup/tmn/MiryoWorkerDelayer;

    return-object v0
.end method

.method public initialState(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;
    .locals 8

    const-string p2, "input"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getAction()Lcom/squareup/tmn/Action;

    move-result-object p2

    .line 138
    instance-of v0, p2, Lcom/squareup/tmn/Action$StartPayment;

    if-eqz v0, :cond_0

    new-instance p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;

    .line 139
    new-instance v2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getAmountAuthorized()J

    move-result-wide v3

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;-><init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)V

    .line 140
    invoke-direct {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->generateTransactionId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, p2

    .line 138
    invoke-direct/range {v1 .. v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;Ljava/lang/String;Lcom/squareup/cardreader/CardReader;[BILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    goto :goto_0

    .line 142
    :cond_0
    instance-of p2, p2, Lcom/squareup/tmn/Action$NoOp;

    if-eqz p2, :cond_1

    sget-object p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    move-object p2, p1

    check-cast p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    :goto_0
    return-object p2

    .line 143
    :cond_1
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported initial input action "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getAction()Lcom/squareup/tmn/Action;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/tmn/TmnTransactionInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->initialState(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;
    .locals 7

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 357
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-object p3

    .line 361
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/tmn/TmnTransactionInput;->getAction()Lcom/squareup/tmn/Action;

    move-result-object p1

    .line 362
    instance-of v0, p1, Lcom/squareup/tmn/Action$NoOp;

    if-eqz v0, :cond_1

    return-object p3

    .line 363
    :cond_1
    instance-of v0, p1, Lcom/squareup/tmn/Action$StartPayment;

    if-eqz v0, :cond_3

    .line 364
    sget-object p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-nez p1, :cond_2

    .line 369
    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->cardReaderHelper:Lcom/squareup/tmn/CardReaderHelper;

    invoke-virtual {p1}, Lcom/squareup/tmn/CardReaderHelper;->resetCardReader$impl_release()V

    .line 370
    new-instance p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;

    .line 371
    new-instance v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    invoke-virtual {p2}, Lcom/squareup/tmn/TmnTransactionInput;->getTmnTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object p3

    invoke-virtual {p2}, Lcom/squareup/tmn/TmnTransactionInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/tmn/TmnTransactionInput;->getAmountAuthorized()J

    move-result-wide v2

    invoke-direct {v1, p3, v0, v2, v3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;-><init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)V

    .line 372
    invoke-direct {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->generateTransactionId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p1

    .line 370
    invoke-direct/range {v0 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;Ljava/lang/String;Lcom/squareup/cardreader/CardReader;[BILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    return-object p1

    .line 365
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 366
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Illegal state: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, " - Unable to start a payment in any state other than Idle."

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 365
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 375
    :cond_3
    instance-of p1, p1, Lcom/squareup/tmn/Action$CancelPayment;

    if-eqz p1, :cond_4

    sget-object p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    return-object p1

    .line 376
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unsupported action onPropsChanged "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/tmn/TmnTransactionInput;->getAction()Lcom/squareup/tmn/Action;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/tmn/TmnTransactionInput;

    check-cast p2, Lcom/squareup/tmn/TmnTransactionInput;

    check-cast p3, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->onPropsChanged(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/tmn/TmnTransactionInput;

    check-cast p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->render(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/workflow/RenderContext;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public render(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/workflow/RenderContext;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/TmnTransactionInput;",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "-",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->teardownWorker:Lcom/squareup/tmn/RealTmnTransactionWorkflow$teardownWorker$1;

    check-cast v0, Lcom/squareup/workflow/Worker;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p3, v0, v1, v2, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 156
    instance-of v0, p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$1;

    invoke-direct {v2, p0, v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v2}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object v0

    const-string v1, "Idle State: Reset Cardreader"

    invoke-static {p3, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 163
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->runAndLogAudioWorker$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 165
    :cond_0
    instance-of v0, p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;

    if-eqz v0, :cond_1

    .line 166
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v2

    move-object v7, p2

    check-cast v7, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;

    invoke-virtual {v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->runAndLogAudioWorker$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 168
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;

    invoke-virtual {v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/CardReader;)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/Worker;

    const/4 v2, 0x0

    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$2;

    invoke-direct {v0, p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$2;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 176
    invoke-direct {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getTmnWorker()Lcom/squareup/workflow/Worker;

    move-result-object v1

    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$3;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$3;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 201
    invoke-virtual {v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getPaymentInfo()Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v1

    invoke-virtual {v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getPaymentInfo()Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->getTransactionType()Lcom/squareup/tmn/TmnTransactionType;

    move-result-object v2

    .line 202
    invoke-virtual {v7}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForInitialTmnDataFromLcr;->getPaymentInfo()Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->getAmountAuthorized()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getMiryoData()[B

    move-result-object v5

    move-object v0, p0

    .line 200
    invoke-direct/range {v0 .. v5}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getPaymentStarterWorker(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Lcom/squareup/tmn/TmnTransactionType;J[B)Lcom/squareup/workflow/Worker;

    move-result-object v1

    const/4 v2, 0x0

    .line 204
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$4;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    .line 199
    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 221
    :cond_1
    instance-of v0, p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    if-eqz v0, :cond_2

    .line 222
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v0

    move-object v1, p2

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getTransactionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getConnId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p3, v0, v2, v3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->runAndLogAudioWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$WaitingForTmnDataFromLcr;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/CardReader;)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/Worker;

    const/4 v2, 0x0

    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$5;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$5;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 230
    invoke-direct {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->getTmnWorker()Lcom/squareup/workflow/Worker;

    move-result-object v1

    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$6;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 290
    :cond_2
    instance-of v0, p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    if-eqz v0, :cond_3

    .line 291
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v0

    move-object v6, p2

    check-cast v6, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    invoke-virtual {v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getTransactionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getConnId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p3, v0, v1, v2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->runAndLogAudioWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;

    invoke-virtual {v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/CardReader;)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/Worker;

    const/4 v2, 0x0

    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$7;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$7;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 300
    invoke-virtual {v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getConnId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getPacketData()[B

    move-result-object v1

    invoke-virtual {v6}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getTransactionId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->sendProxyMessageToServer(Ljava/lang/String;[BLjava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    const/4 v2, 0x0

    .line 301
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object v0, p3

    .line 299
    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_0

    .line 336
    :cond_3
    instance-of v0, p2, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;

    if-eqz v0, :cond_4

    .line 337
    invoke-virtual {p1}, Lcom/squareup/tmn/TmnTransactionInput;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v0

    move-object v1, p2

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;->getTransactionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;->getConnId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p3, v0, v2, v3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->runAndLogAudioWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;->getConnId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;->getStatus()Lcom/squareup/protos/client/felica/Status;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingTransactionStatusToServer;->getTransactionId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->sendTransactionStatusToServer(Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object v1

    const/4 v2, 0x0

    .line 340
    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$9;

    invoke-direct {v0, p0, p2}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$9;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    .line 338
    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public snapshotState(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 380
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->snapshotState(Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
