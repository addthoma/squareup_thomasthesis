.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;->render(Lcom/squareup/tmn/TmnTransactionInput;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/workflow/RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "+",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
        "Lcom/squareup/tmn/TmnTransactionOutput;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/felica/ProxyMessageResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;",
            "Lcom/squareup/tmn/TmnTransactionOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 303
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTimings$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v0

    sget-object v1, Lcom/squareup/tmn/What;->WORKFLOW_ON_SERVER_SUCCESS:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 305
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object v0

    .line 306
    new-instance v10, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 307
    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SENT_PACKET_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v1}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 308
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getConnId()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x30

    const/4 v9, 0x0

    move-object v1, v10

    .line 306
    invoke-direct/range {v1 .. v9}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v10, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 305
    invoke-virtual {v0, v10}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 311
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/felica/ProxyMessageResponse;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    invoke-static {v0, p1, v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$handleSuccessSendProxyMessageToServer(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/protos/client/felica/ProxyMessageResponse;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 313
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 315
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTmnAudioPlayer$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/audio/TmnAudioPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tmn/audio/TmnAudioPlayer;->stopAudio$impl_release()V

    .line 317
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getTimings$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/TmnTimings;

    move-result-object v0

    sget-object v1, Lcom/squareup/tmn/What;->WORKFLOW_ON_SERVER_FAILED:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 318
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getAnalytics$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    move-result-object v0

    .line 319
    new-instance v10, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;

    .line 320
    sget-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SENT_PACKET_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v1}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->getValue()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getTransactionId()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 321
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    invoke-virtual {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getConnId()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x30

    const/4 v9, 0x0

    move-object v1, v10

    .line 319
    invoke-direct/range {v1 .. v9}, Lcom/squareup/tmn/logging/TmnEvents$TmnCommunicationEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v10, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 318
    invoke-virtual {v0, v10}, Lcom/squareup/tmn/EmoneyAnalyticsLogger;->addEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 327
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    .line 328
    sget-object v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$Idle;

    check-cast v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    sget-object v2, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->NETWORK_FAILURE:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    iget-object v3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    .line 329
    iget-object v4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->$state:Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;

    check-cast v4, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;

    invoke-virtual {v4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState$SendingProxyMessageToServer;->getAfterWriteNotifyData()Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;

    move-result-object v4

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 328
    invoke-static {v3, v4, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getProxyMessageResponseFailure(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$AfterWriteNotifyData;Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/tmn/TmnTransactionOutput$Failed;

    move-result-object p1

    check-cast p1, Lcom/squareup/tmn/TmnTransactionOutput;

    .line 327
    invoke-static {v0, v1, v2, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$enterStateAndLog(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/tmn/RealTmnTransactionWorkflow$TmnTransactionState;Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;Lcom/squareup/tmn/TmnTransactionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$render$8;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
