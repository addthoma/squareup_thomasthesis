.class public final Lcom/squareup/tmn/audio/TmnAudioPlayer;
.super Ljava/lang/Object;
.source "TmnAudioPlayer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tmn/audio/TmnAudioPlayer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTmnAudioPlayer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TmnAudioPlayer.kt\ncom/squareup/tmn/audio/TmnAudioPlayer\n*L\n1#1,82:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0002J\u0015\u0010\t\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\u0008H\u0000\u00a2\u0006\u0002\u0008\u000bJ\u0015\u0010\u000c\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eH\u0000\u00a2\u0006\u0002\u0008\u000fJ\r\u0010\u0010\u001a\u00020\nH\u0000\u00a2\u0006\u0002\u0008\u0011J\r\u0010\u0012\u001a\u00020\nH\u0000\u00a2\u0006\u0002\u0008\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/tmn/audio/TmnAudioPlayer;",
        "",
        "tmnAudioPlayer",
        "Lcom/squareup/brandaudio/BrandAudioPlayer;",
        "(Lcom/squareup/brandaudio/BrandAudioPlayer;)V",
        "getSuccessSound",
        "",
        "brandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "initialize",
        "",
        "initialize$impl_release",
        "playTmnAudioMessage",
        "tmnAudioMessage",
        "Lcom/squareup/cardreader/lcr/CrsTmnAudio;",
        "playTmnAudioMessage$impl_release",
        "shutDown",
        "shutDown$impl_release",
        "stopAudio",
        "stopAudio$impl_release",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/tmn/audio/TmnAudioPlayer$Companion;

.field public static final ERROR:Ljava/lang/String; = "error.mp3"

.field public static final ID_SUCCESS:Ljava/lang/String; = "id_success.mp3"

.field public static final QP_SUCCESS:Ljava/lang/String; = "qp_success.mp3"

.field public static final RETOUCH_WITH_VOICE:Ljava/lang/String; = "retouch_with_voice.mp3"

.field public static final SUICA_SUCCESS:Ljava/lang/String; = "suica_success.mp3"

.field public static final SUICA_SUCCESS2:Ljava/lang/String; = "suica_success2.mp3"


# instance fields
.field private final tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tmn/audio/TmnAudioPlayer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->Companion:Lcom/squareup/tmn/audio/TmnAudioPlayer$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/brandaudio/BrandAudioPlayer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "tmnAudioPlayer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    return-void
.end method

.method private final getSuccessSound(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;)Ljava/lang/String;
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const-string p1, "suica_success.mp3"

    goto :goto_0

    :cond_1
    const-string p1, "id_success.mp3"

    goto :goto_0

    :cond_2
    const-string p1, "qp_success.mp3"

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final initialize$impl_release(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;)V
    .locals 2

    const-string v0, "brandId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/brandaudio/BrandAudioPlayer;->initialize(Z)V

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/tmn/audio/TmnAudioPlayer;->getSuccessSound(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 29
    iget-object v0, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    invoke-interface {v0, p1}, Lcom/squareup/brandaudio/BrandAudioPlayer;->prepareAudio(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final playTmnAudioMessage$impl_release(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V
    .locals 4

    const-string/jumbo v0, "tmnAudioMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    sget-object v0, Lcom/squareup/tmn/audio/TmnAudioPlayer$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 54
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const/4 v0, 0x1

    const-string v1, "retouch_with_voice.mp3"

    invoke-interface {p1, v1, v0}, Lcom/squareup/brandaudio/BrandAudioPlayer;->playBrandAudioMessage(Ljava/lang/String;Z)V

    goto :goto_0

    .line 51
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const-string v3, "error.mp3"

    invoke-static {p1, v3, v2, v1, v0}, Lcom/squareup/brandaudio/BrandAudioPlayer$DefaultImpls;->playBrandAudioMessage$default(Lcom/squareup/brandaudio/BrandAudioPlayer;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_0

    .line 48
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const-string v3, "suica_success2.mp3"

    invoke-static {p1, v3, v2, v1, v0}, Lcom/squareup/brandaudio/BrandAudioPlayer$DefaultImpls;->playBrandAudioMessage$default(Lcom/squareup/brandaudio/BrandAudioPlayer;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_0

    .line 45
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const-string v3, "suica_success.mp3"

    invoke-static {p1, v3, v2, v1, v0}, Lcom/squareup/brandaudio/BrandAudioPlayer$DefaultImpls;->playBrandAudioMessage$default(Lcom/squareup/brandaudio/BrandAudioPlayer;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_0

    .line 42
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const-string v3, "id_success.mp3"

    invoke-static {p1, v3, v2, v1, v0}, Lcom/squareup/brandaudio/BrandAudioPlayer$DefaultImpls;->playBrandAudioMessage$default(Lcom/squareup/brandaudio/BrandAudioPlayer;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_0

    .line 39
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    const-string v3, "qp_success.mp3"

    invoke-static {p1, v3, v2, v1, v0}, Lcom/squareup/brandaudio/BrandAudioPlayer$DefaultImpls;->playBrandAudioMessage$default(Lcom/squareup/brandaudio/BrandAudioPlayer;Ljava/lang/String;ZILjava/lang/Object;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final shutDown$impl_release()V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    invoke-interface {v0}, Lcom/squareup/brandaudio/BrandAudioPlayer;->shutDown()V

    return-void
.end method

.method public final stopAudio$impl_release()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/tmn/audio/TmnAudioPlayer;->tmnAudioPlayer:Lcom/squareup/brandaudio/BrandAudioPlayer;

    invoke-interface {v0}, Lcom/squareup/brandaudio/BrandAudioPlayer;->stopAudio()V

    return-void
.end method
