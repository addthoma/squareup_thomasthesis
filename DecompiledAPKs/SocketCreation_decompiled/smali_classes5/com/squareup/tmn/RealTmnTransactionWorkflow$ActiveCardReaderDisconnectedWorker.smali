.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "RealTmnTransactionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ActiveCardReaderDisconnectedWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Lcom/squareup/dipper/events/TmnEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0012\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0005J\u0014\u0010\u0006\u001a\u00020\u00072\n\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\tH\u0016J\u0010\u0010\n\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\u000bH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "currentCardReader",
        "Lcom/squareup/cardreader/CardReader;",
        "(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/CardReader;)V",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lorg/reactivestreams/Publisher;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentCardReader:Lcom/squareup/cardreader/CardReader;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Lcom/squareup/cardreader/CardReader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReader;",
            ")V"
        }
    .end annotation

    .line 599
    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    .line 601
    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;->currentCardReader:Lcom/squareup/cardreader/CardReader;

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 603
    instance-of v0, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;->currentCardReader:Lcom/squareup/cardreader/CardReader;

    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;

    iget-object p1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;->currentCardReader:Lcom/squareup/cardreader/CardReader;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public runPublisher()Lorg/reactivestreams/Publisher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/reactivestreams/Publisher<",
            "+",
            "Lcom/squareup/dipper/events/TmnEvent;",
            ">;"
        }
    .end annotation

    .line 607
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getCardReaderHelper$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/tmn/CardReaderHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;->currentCardReader:Lcom/squareup/cardreader/CardReader;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/CardReaderHelper;->activeCardReaderDisconnectedEvents$impl_release(Lcom/squareup/cardreader/CardReader;)Lio/reactivex/Observable;

    move-result-object v0

    .line 608
    new-instance v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker$runPublisher$1;

    invoke-direct {v1, p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker$runPublisher$1;-><init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow$ActiveCardReaderDisconnectedWorker;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 611
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string v1, "cardReaderHelper.activeC\u2026      .toFlowable(BUFFER)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
