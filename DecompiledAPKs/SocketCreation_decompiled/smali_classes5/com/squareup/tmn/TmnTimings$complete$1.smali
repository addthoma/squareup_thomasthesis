.class final Lcom/squareup/tmn/TmnTimings$complete$1;
.super Ljava/lang/Object;
.source "TmnTimings.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/TmnTimings;->complete(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTmnTimings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TmnTimings.kt\ncom/squareup/tmn/TmnTimings$complete$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,422:1\n1642#2,2:423\n1642#2,2:425\n*E\n*S KotlinDebug\n*F\n+ 1 TmnTimings.kt\ncom/squareup/tmn/TmnTimings$complete$1\n*L\n174#1,2:423\n264#1,2:425\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $allEvents:Ljava/util/List;

.field final synthetic $timeNanos:J

.field final synthetic $transactionId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/TmnTimings;Ljava/util/List;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    iput-object p2, p0, Lcom/squareup/tmn/TmnTimings$complete$1;->$allEvents:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/tmn/TmnTimings$complete$1;->$transactionId:Ljava/lang/String;

    iput-wide p4, p0, Lcom/squareup/tmn/TmnTimings$complete$1;->$timeNanos:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 44

    move-object/from16 v0, p0

    .line 150
    iget-object v1, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    const/4 v2, 0x0

    move-object v3, v2

    check-cast v3, Ljava/util/List;

    invoke-static {v1, v3}, Lcom/squareup/tmn/TmnTimings;->access$setEvents$p(Lcom/squareup/tmn/TmnTimings;Ljava/util/List;)V

    const/4 v1, 0x0

    new-array v3, v1, [Ljava/lang/Object;

    const-string v4, "complete:"

    .line 152
    invoke-static {v4, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    check-cast v2, Lcom/squareup/tmn/Event;

    .line 174
    iget-object v3, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->$allEvents:Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 423
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const-wide/16 v4, 0x0

    move-object v8, v2

    move-object v9, v8

    move-object/from16 v20, v9

    move-object/from16 v21, v20

    move-object/from16 v22, v21

    move-wide v6, v4

    move-wide v10, v6

    move-wide v12, v10

    move-wide v14, v12

    move-wide/from16 v16, v14

    move-wide/from16 v18, v16

    move-wide/from16 v39, v18

    const/16 v24, 0x0

    const/16 v41, 0x0

    move-wide/from16 v1, v39

    move-object/from16 v4, v22

    move-object v5, v4

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/squareup/tmn/Event;

    .line 183
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getWhat()Lcom/squareup/tmn/What;

    move-result-object v25

    sget-object v26, Lcom/squareup/tmn/TmnTimings$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual/range {v25 .. v25}, Lcom/squareup/tmn/What;->ordinal()I

    move-result v25

    aget v25, v26, v25

    packed-switch v25, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object/from16 v25, v3

    :cond_1
    move-object v0, v4

    move-wide/from16 v3, v39

    goto/16 :goto_2

    :pswitch_1
    if-eqz v8, :cond_0

    move-object/from16 v25, v3

    .line 255
    invoke-virtual {v8}, Lcom/squareup/tmn/Event;->getPhase()Lcom/squareup/tmn/Phase;

    move-result-object v3

    sget-object v0, Lcom/squareup/tmn/Phase;->AFTER_TAP:Lcom/squareup/tmn/Phase;

    if-ne v3, v0, :cond_1

    .line 256
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v26

    invoke-virtual {v8}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v28

    sub-long v26, v26, v28

    add-long v1, v1, v26

    goto/16 :goto_4

    :pswitch_2
    move-object/from16 v25, v3

    move-object/from16 v8, v23

    goto/16 :goto_4

    :pswitch_3
    move-object/from16 v25, v3

    .line 247
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getWhat()Lcom/squareup/tmn/What;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_FROM_LCR:Lcom/squareup/tmn/What;

    if-ne v0, v3, :cond_2

    move-object/from16 v9, v20

    :cond_2
    move-object/from16 v22, v23

    goto/16 :goto_4

    :pswitch_4
    move-object/from16 v25, v3

    .line 232
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getWhat()Lcom/squareup/tmn/What;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/What;->GATT_CALLBACK_ON_CHARACTERISTIC_CHANGED:Lcom/squareup/tmn/What;

    if-ne v0, v3, :cond_4

    if-nez v9, :cond_3

    move-object/from16 v9, v23

    move-object/from16 v21, v9

    goto/16 :goto_4

    .line 235
    :cond_3
    invoke-virtual {v9}, Lcom/squareup/tmn/Event;->getPhase()Lcom/squareup/tmn/Phase;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/Phase;->AFTER_TAP:Lcom/squareup/tmn/Phase;

    if-ne v0, v3, :cond_4

    .line 236
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v26

    invoke-virtual {v9}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v28

    sub-long v26, v26, v28

    add-long v10, v10, v26

    cmp-long v0, v26, v12

    if-lez v0, :cond_4

    move-wide/from16 v12, v26

    :cond_4
    move-object/from16 v21, v23

    goto/16 :goto_4

    :pswitch_5
    move-object/from16 v25, v3

    .line 212
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getWhat()Lcom/squareup/tmn/What;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/What;->BLE_BACKEND_VECTOR_TO_LCR:Lcom/squareup/tmn/What;

    if-ne v0, v3, :cond_6

    if-eqz v5, :cond_6

    .line 213
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v26

    invoke-virtual {v5}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v28

    sub-long v26, v26, v28

    add-long v6, v6, v26

    .line 216
    invoke-virtual {v5}, Lcom/squareup/tmn/Event;->getPhase()Lcom/squareup/tmn/Phase;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/Phase;->AFTER_TAP:Lcom/squareup/tmn/Phase;

    if-ne v0, v3, :cond_5

    add-long v14, v14, v26

    :cond_5
    move-object/from16 v5, v20

    :cond_6
    if-eqz v21, :cond_9

    .line 222
    invoke-virtual/range {v21 .. v21}, Lcom/squareup/tmn/Event;->getPhase()Lcom/squareup/tmn/Phase;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/Phase;->AFTER_TAP:Lcom/squareup/tmn/Phase;

    if-ne v0, v3, :cond_9

    .line 223
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v26

    invoke-virtual/range {v21 .. v21}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v28

    goto :goto_1

    :pswitch_6
    move-object/from16 v25, v3

    .line 199
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getWhat()Lcom/squareup/tmn/What;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/What;->GATT_WRITE_CHARACTERISTIC:Lcom/squareup/tmn/What;

    if-ne v0, v3, :cond_7

    if-nez v5, :cond_7

    move-object/from16 v5, v23

    :cond_7
    if-eqz v22, :cond_9

    .line 204
    invoke-virtual/range {v22 .. v22}, Lcom/squareup/tmn/Event;->getPhase()Lcom/squareup/tmn/Phase;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/Phase;->AFTER_TAP:Lcom/squareup/tmn/Phase;

    if-ne v0, v3, :cond_9

    .line 206
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v26

    invoke-virtual/range {v22 .. v22}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v28

    :goto_1
    sub-long v26, v26, v28

    add-long v18, v18, v26

    goto :goto_4

    :pswitch_7
    move-object/from16 v25, v3

    if-eqz v4, :cond_1

    .line 188
    invoke-virtual/range {v23 .. v23}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v26

    invoke-virtual {v4}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v28

    sub-long v26, v26, v28

    add-long v16, v16, v26

    add-int/lit8 v41, v41, 0x1

    .line 191
    invoke-virtual {v4}, Lcom/squareup/tmn/Event;->getPhase()Lcom/squareup/tmn/Phase;

    move-result-object v0

    sget-object v3, Lcom/squareup/tmn/Phase;->AFTER_TAP:Lcom/squareup/tmn/Phase;

    if-ne v0, v3, :cond_8

    move-object v0, v4

    move-wide/from16 v3, v39

    add-long v39, v3, v26

    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    :cond_8
    move-object v0, v4

    move-wide/from16 v3, v39

    goto :goto_3

    :pswitch_8
    move-object/from16 v25, v3

    move-wide/from16 v3, v39

    move-object/from16 v4, v23

    goto :goto_4

    :goto_2
    move-wide/from16 v39, v3

    :goto_3
    move-object v4, v0

    :cond_9
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v3, v25

    goto/16 :goto_0

    :cond_a
    move-wide/from16 v3, v39

    .line 263
    iget-object v5, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v5}, Lcom/squareup/tmn/TmnTimings;->access$getVerbose$p(Lcom/squareup/tmn/TmnTimings;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 264
    iget-object v5, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->$allEvents:Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 425
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/tmn/Event;

    .line 265
    new-instance v9, Lcom/squareup/tmn/TmnIndividualEvent;

    .line 266
    invoke-virtual {v8}, Lcom/squareup/tmn/Event;->getWhat()Lcom/squareup/tmn/What;

    move-result-object v26

    .line 267
    invoke-virtual {v8}, Lcom/squareup/tmn/Event;->getPhase()Lcom/squareup/tmn/Phase;

    move-result-object v27

    move-object/from16 v20, v5

    .line 268
    iget-object v5, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->$transactionId:Ljava/lang/String;

    .line 269
    invoke-virtual {v8}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v31

    move-wide/from16 v21, v12

    .line 270
    sget-object v12, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v33, v14

    invoke-virtual {v8}, Lcom/squareup/tmn/Event;->getStartedAtNanos()J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v29

    move-object/from16 v25, v9

    move-object/from16 v28, v5

    .line 265
    invoke-direct/range {v25 .. v32}, Lcom/squareup/tmn/TmnIndividualEvent;-><init>(Lcom/squareup/tmn/What;Lcom/squareup/tmn/Phase;Ljava/lang/String;JJ)V

    .line 272
    iget-object v5, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v5}, Lcom/squareup/tmn/TmnTimings;->access$getDateTimeFactory$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/util/DateTimeFactory;

    move-result-object v5

    invoke-virtual {v9}, Lcom/squareup/tmn/TmnIndividualEvent;->getTimestampMillis()J

    move-result-wide v12

    invoke-virtual {v5, v12, v13}, Lcom/squareup/util/DateTimeFactory;->forMillis(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v5

    iput-object v5, v9, Lcom/squareup/tmn/TmnIndividualEvent;->overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 273
    iget-object v5, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v5}, Lcom/squareup/tmn/TmnTimings;->access$getAnalytics$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/analytics/Analytics;

    move-result-object v5

    check-cast v9, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v5, v9}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    move-object/from16 v5, v20

    move-wide/from16 v12, v21

    move-wide/from16 v14, v33

    goto :goto_5

    :cond_b
    move-wide/from16 v21, v12

    move-wide/from16 v33, v14

    .line 277
    iget-wide v8, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->$timeNanos:J

    iget-object v5, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v5}, Lcom/squareup/tmn/TmnTimings;->access$getTapTimeNanos$p(Lcom/squareup/tmn/TmnTimings;)J

    move-result-wide v12

    sub-long/2addr v8, v12

    sub-long v13, v8, v3

    .line 280
    new-instance v5, Lcom/squareup/tmn/TmnTimingsEvent;

    move-wide/from16 v42, v21

    move-object v12, v5

    .line 281
    sget-object v15, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v20, v13

    move-wide/from16 v13, v16

    invoke-virtual {v15, v13, v14}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v13

    move-wide/from16 v31, v1

    move-wide/from16 v35, v10

    move-wide/from16 v29, v18

    move-wide/from16 v10, v20

    move-wide/from16 v1, v33

    .line 283
    sget-object v15, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v15, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v16

    .line 284
    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v18

    .line 285
    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v20

    .line 286
    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v22

    .line 288
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v25

    .line 289
    iget-object v1, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v1}, Lcom/squareup/tmn/TmnTimings;->access$getConnectionIntervalMillis$p(Lcom/squareup/tmn/TmnTimings;)I

    move-result v27

    .line 290
    iget-object v1, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v1}, Lcom/squareup/tmn/TmnTimings;->access$getMtu$p(Lcom/squareup/tmn/TmnTimings;)I

    move-result v28

    .line 291
    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v2, v29

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v29

    .line 294
    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v2, v31

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v31

    .line 295
    iget-object v1, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->$transactionId:Ljava/lang/String;

    move-object/from16 v33, v1

    .line 296
    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v10, v35

    invoke-virtual {v1, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v34

    .line 297
    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v2, v42

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v36

    .line 298
    iget-object v1, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v1}, Lcom/squareup/tmn/TmnTimings;->access$getTapConnectivity$p(Lcom/squareup/tmn/TmnTimings;)Ljava/lang/String;

    move-result-object v38

    move/from16 v15, v41

    .line 280
    invoke-direct/range {v12 .. v38}, Lcom/squareup/tmn/TmnTimingsEvent;-><init>(JIJJJJIJIIJJLjava/lang/String;JJLjava/lang/String;)V

    .line 300
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Logging TMN timings to ES: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 301
    iget-object v1, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v1}, Lcom/squareup/tmn/TmnTimings;->access$getAnalytics$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    move-object v2, v5

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 303
    new-instance v1, Lcom/squareup/tmn/Es2TmnTimingsEvent;

    move-object v6, v1

    .line 305
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getConnectionIntervalMilliseconds()I

    move-result v7

    .line 306
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getMTU()I

    move-result v8

    .line 308
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepPosBleStackTimeMilliseconds()J

    move-result-wide v9

    .line 310
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepBLEReadMaxMilliseconds()J

    move-result-wide v11

    .line 312
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepBLEReadTimeMilliseconds()J

    move-result-wide v13

    .line 314
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepBLEWriteMilliseconds()J

    move-result-wide v15

    .line 316
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepInferredClientMilliseconds()J

    move-result-wide v17

    .line 317
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepLcrTimeMilliseconds()J

    move-result-wide v19

    .line 318
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepMilliseconds()J

    move-result-wide v21

    .line 319
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepServerMilliseconds()J

    move-result-wide v23

    .line 321
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTapToBeepServerRequestCount()I

    move-result v25

    .line 322
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTotalBLEWriteTimeMilliseconds()J

    move-result-wide v26

    .line 323
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTotalServerMilliseconds()J

    move-result-wide v28

    .line 324
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTotalServerRequestCount()I

    move-result v30

    .line 325
    invoke-virtual {v5}, Lcom/squareup/tmn/TmnTimingsEvent;->getTransactionId()Ljava/lang/String;

    move-result-object v31

    .line 326
    iget-object v2, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v2}, Lcom/squareup/tmn/TmnTimings;->access$getTapConnectivity$p(Lcom/squareup/tmn/TmnTimings;)Ljava/lang/String;

    move-result-object v32

    .line 303
    invoke-direct/range {v6 .. v32}, Lcom/squareup/tmn/Es2TmnTimingsEvent;-><init>(IIJJJJJJJJIJJILjava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v2, v0, Lcom/squareup/tmn/TmnTimings$complete$1;->this$0:Lcom/squareup/tmn/TmnTimings;

    invoke-static {v2}, Lcom/squareup/tmn/TmnTimings;->access$getAnalytics$p(Lcom/squareup/tmn/TmnTimings;)Lcom/squareup/analytics/Analytics;

    move-result-object v2

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v2, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
