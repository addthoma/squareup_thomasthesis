.class public final enum Lcom/squareup/tmn/What;
.super Ljava/lang/Enum;
.source "TmnTimings.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tmn/What;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0016\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/tmn/What;",
        "",
        "(Ljava/lang/String;I)V",
        "GATT_CALLBACK_ON_CHARACTERISTIC_READ",
        "GATT_CALLBACK_ON_CHARACTERISTIC_CHANGED",
        "GATT_WRITE_CHARACTERISTIC",
        "GATT_READ_CHARACTERISTIC",
        "WORKFLOW_STARTING_PAYMENT_ON_READER",
        "WORKFLOW_ON_SERVER_SUCCESS",
        "WORKFLOW_ON_SERVER_FAILED",
        "WORKFLOW_SEND_TO_SERVER",
        "PAYMENT_FEATURE_TMN_BYTES_TO_READER",
        "PAYMENT_FEATURE_TMN_ACK_WRITE_NOTIFY",
        "PAYMENT_FEATURE_TMN_START",
        "PAYMENT_FEATURE_TMN_START_MIRYO",
        "PAYMENT_FEATURE_ON_TMN_AUTH_REQUEST",
        "PAYMENT_FEATURE_ON_TMN_DATA_TO_TMN",
        "PAYMENT_FEATURE_ON_TMN_WRITE_NOTIFY",
        "PAYMENT_FEATURE_ON_TMN_TRANSACTION_COMPLETE",
        "BLE_BACKEND_BYTES_TO_LCR",
        "BLE_BACKEND_VECTOR_TO_LCR",
        "BLE_BACKEND_BYTES_FROM_LCR",
        "BLE_BACKEND_BYTES_READ_ACK_VECTOR",
        "tmn-timings_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tmn/What;

.field public static final enum BLE_BACKEND_BYTES_FROM_LCR:Lcom/squareup/tmn/What;

.field public static final enum BLE_BACKEND_BYTES_READ_ACK_VECTOR:Lcom/squareup/tmn/What;

.field public static final enum BLE_BACKEND_BYTES_TO_LCR:Lcom/squareup/tmn/What;

.field public static final enum BLE_BACKEND_VECTOR_TO_LCR:Lcom/squareup/tmn/What;

.field public static final enum GATT_CALLBACK_ON_CHARACTERISTIC_CHANGED:Lcom/squareup/tmn/What;

.field public static final enum GATT_CALLBACK_ON_CHARACTERISTIC_READ:Lcom/squareup/tmn/What;

.field public static final enum GATT_READ_CHARACTERISTIC:Lcom/squareup/tmn/What;

.field public static final enum GATT_WRITE_CHARACTERISTIC:Lcom/squareup/tmn/What;

.field public static final enum PAYMENT_FEATURE_ON_TMN_AUTH_REQUEST:Lcom/squareup/tmn/What;

.field public static final enum PAYMENT_FEATURE_ON_TMN_DATA_TO_TMN:Lcom/squareup/tmn/What;

.field public static final enum PAYMENT_FEATURE_ON_TMN_TRANSACTION_COMPLETE:Lcom/squareup/tmn/What;

.field public static final enum PAYMENT_FEATURE_ON_TMN_WRITE_NOTIFY:Lcom/squareup/tmn/What;

.field public static final enum PAYMENT_FEATURE_TMN_ACK_WRITE_NOTIFY:Lcom/squareup/tmn/What;

.field public static final enum PAYMENT_FEATURE_TMN_BYTES_TO_READER:Lcom/squareup/tmn/What;

.field public static final enum PAYMENT_FEATURE_TMN_START:Lcom/squareup/tmn/What;

.field public static final enum PAYMENT_FEATURE_TMN_START_MIRYO:Lcom/squareup/tmn/What;

.field public static final enum WORKFLOW_ON_SERVER_FAILED:Lcom/squareup/tmn/What;

.field public static final enum WORKFLOW_ON_SERVER_SUCCESS:Lcom/squareup/tmn/What;

.field public static final enum WORKFLOW_SEND_TO_SERVER:Lcom/squareup/tmn/What;

.field public static final enum WORKFLOW_STARTING_PAYMENT_ON_READER:Lcom/squareup/tmn/What;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/squareup/tmn/What;

    new-instance v1, Lcom/squareup/tmn/What;

    const/4 v2, 0x0

    const-string v3, "GATT_CALLBACK_ON_CHARACTERISTIC_READ"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->GATT_CALLBACK_ON_CHARACTERISTIC_READ:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/4 v2, 0x1

    const-string v3, "GATT_CALLBACK_ON_CHARACTERISTIC_CHANGED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->GATT_CALLBACK_ON_CHARACTERISTIC_CHANGED:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/4 v2, 0x2

    const-string v3, "GATT_WRITE_CHARACTERISTIC"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->GATT_WRITE_CHARACTERISTIC:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/4 v2, 0x3

    const-string v3, "GATT_READ_CHARACTERISTIC"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->GATT_READ_CHARACTERISTIC:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/4 v2, 0x4

    const-string v3, "WORKFLOW_STARTING_PAYMENT_ON_READER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->WORKFLOW_STARTING_PAYMENT_ON_READER:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/4 v2, 0x5

    const-string v3, "WORKFLOW_ON_SERVER_SUCCESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->WORKFLOW_ON_SERVER_SUCCESS:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/4 v2, 0x6

    const-string v3, "WORKFLOW_ON_SERVER_FAILED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->WORKFLOW_ON_SERVER_FAILED:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/4 v2, 0x7

    const-string v3, "WORKFLOW_SEND_TO_SERVER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->WORKFLOW_SEND_TO_SERVER:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/16 v2, 0x8

    const-string v3, "PAYMENT_FEATURE_TMN_BYTES_TO_READER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_BYTES_TO_READER:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/16 v2, 0x9

    const-string v3, "PAYMENT_FEATURE_TMN_ACK_WRITE_NOTIFY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_ACK_WRITE_NOTIFY:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/16 v2, 0xa

    const-string v3, "PAYMENT_FEATURE_TMN_START"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_START:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/16 v2, 0xb

    const-string v3, "PAYMENT_FEATURE_TMN_START_MIRYO"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_START_MIRYO:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/16 v2, 0xc

    const-string v3, "PAYMENT_FEATURE_ON_TMN_AUTH_REQUEST"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_AUTH_REQUEST:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/16 v2, 0xd

    const-string v3, "PAYMENT_FEATURE_ON_TMN_DATA_TO_TMN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_DATA_TO_TMN:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const/16 v2, 0xe

    const-string v3, "PAYMENT_FEATURE_ON_TMN_WRITE_NOTIFY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_WRITE_NOTIFY:Lcom/squareup/tmn/What;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const-string v2, "PAYMENT_FEATURE_ON_TMN_TRANSACTION_COMPLETE"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_TRANSACTION_COMPLETE:Lcom/squareup/tmn/What;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const-string v2, "BLE_BACKEND_BYTES_TO_LCR"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_TO_LCR:Lcom/squareup/tmn/What;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const-string v2, "BLE_BACKEND_VECTOR_TO_LCR"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_VECTOR_TO_LCR:Lcom/squareup/tmn/What;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const-string v2, "BLE_BACKEND_BYTES_FROM_LCR"

    const/16 v3, 0x12

    invoke-direct {v1, v2, v3}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_FROM_LCR:Lcom/squareup/tmn/What;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/What;

    const-string v2, "BLE_BACKEND_BYTES_READ_ACK_VECTOR"

    const/16 v3, 0x13

    invoke-direct {v1, v2, v3}, Lcom/squareup/tmn/What;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tmn/What;->BLE_BACKEND_BYTES_READ_ACK_VECTOR:Lcom/squareup/tmn/What;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/tmn/What;->$VALUES:[Lcom/squareup/tmn/What;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 392
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tmn/What;
    .locals 1

    const-class v0, Lcom/squareup/tmn/What;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tmn/What;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tmn/What;
    .locals 1

    sget-object v0, Lcom/squareup/tmn/What;->$VALUES:[Lcom/squareup/tmn/What;

    invoke-virtual {v0}, [Lcom/squareup/tmn/What;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tmn/What;

    return-object v0
.end method
