.class public final Lcom/squareup/tmn/RealTmnObservablesHelper;
.super Ljava/lang/Object;
.source "RealTmnObservablesHelper.kt"

# interfaces
.implements Lcom/squareup/tmn/TmnObservablesHelper;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0016J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0006H\u0016J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0006H\u0016J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0006H\u0016J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0006H\u0016J\u000e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/tmn/RealTmnObservablesHelper;",
        "Lcom/squareup/tmn/TmnObservablesHelper;",
        "cardReaderListeners",
        "Lcom/squareup/cardreader/CardReaderListeners;",
        "(Lcom/squareup/cardreader/CardReaderListeners;)V",
        "getAudioRequestObservable",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
        "getDisplayRequestObservable",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "getOnTmnCompletionObservable",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;",
        "getOnTmnDataToTmnObservable",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;",
        "getOnTmnWriteNotifyObservable",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;",
        "getTmnAuthRequest",
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderListeners;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardReaderListeners"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnObservablesHelper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-void
.end method


# virtual methods
.method public getAudioRequestObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnObservablesHelper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->tmnEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 32
    const-class v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnAudioRequestEvent;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "cardReaderListeners.tmnE\u2026RequestEvent::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getDisplayRequestObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnObservablesHelper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->tmnEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 22
    const-class v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "cardReaderListeners.tmnE\u2026RequestEvent::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getOnTmnCompletionObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnObservablesHelper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->tmnEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    const-class v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnCompletionEvent;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "cardReaderListeners.tmnE\u2026pletionEvent::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getOnTmnDataToTmnObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnObservablesHelper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->tmnEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 37
    const-class v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "cardReaderListeners.tmnE\u2026taToTmnEvent::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getOnTmnWriteNotifyObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnObservablesHelper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->tmnEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    const-class v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "cardReaderListeners.tmnE\u2026nWriteNotify::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getTmnAuthRequest()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnObservablesHelper;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->tmnEvents()Lio/reactivex/Observable;

    move-result-object v0

    .line 27
    const-class v1, Lcom/squareup/dipper/events/TmnEvent$OnTmnAuthRequest;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "cardReaderListeners.tmnE\u2026nAuthRequest::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
