.class public final Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink$X2PosCreateAccountLink;
.super Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink;
.source "SquareDeviceDeepLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "X2PosCreateAccountLink"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink$X2PosCreateAccountLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink;",
        "()V",
        "deepLink",
        "Landroid/net/Uri;",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink$X2PosCreateAccountLink;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 130
    new-instance v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink$X2PosCreateAccountLink;

    invoke-direct {v0}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink$X2PosCreateAccountLink;-><init>()V

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink$X2PosCreateAccountLink;->INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink$X2PosCreateAccountLink;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 130
    invoke-direct {p0, v0}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public final deepLink()Landroid/net/Uri;
    .locals 2

    .line 131
    invoke-static {}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink;->access$Companion()Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSignInAppLink$Companion;

    const-string v0, "square-register://root/create-account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "Uri.parse(X2_POS_SIGN_CREATE_ACCOUNT_LINK)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
