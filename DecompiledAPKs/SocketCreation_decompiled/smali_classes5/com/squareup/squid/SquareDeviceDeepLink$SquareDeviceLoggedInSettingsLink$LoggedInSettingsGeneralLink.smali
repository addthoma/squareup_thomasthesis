.class public final Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;
.super Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;
.source "SquareDeviceDeepLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoggedInSettingsGeneralLink"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;",
        "()V",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;

    invoke-direct {v0}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;-><init>()V

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;->INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink$LoggedInSettingsGeneralLink;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "generalSettings"

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, v0, v1}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceLoggedInSettingsLink;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
