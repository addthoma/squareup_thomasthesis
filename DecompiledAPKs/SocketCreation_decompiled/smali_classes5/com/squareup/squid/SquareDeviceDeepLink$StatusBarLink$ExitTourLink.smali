.class public final Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;
.super Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;
.source "SquareDeviceDeepLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExitTourLink"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareDeviceDeepLink.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareDeviceDeepLink.kt\ncom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink\n*L\n1#1,139:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;",
        "()V",
        "deepLink",
        "Landroid/net/Uri;",
        "matches",
        "",
        "uri",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;

    invoke-direct {v0}, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;-><init>()V

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;->INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public final deepLink()Landroid/net/Uri;
    .locals 2

    .line 23
    invoke-static {}, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink;->access$Companion()Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$Companion;

    const-string v0, "square-register://statusbar/exitTour"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "Uri.parse(EXIT_TOUR_DEEP_LINK)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final matches(Landroid/net/Uri;)Z
    .locals 3

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/squareup/squid/SquareDeviceDeepLink$StatusBarLink$ExitTourLink;->deepLink()Landroid/net/Uri;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
