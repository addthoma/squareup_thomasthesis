.class public interface abstract Lcom/squareup/sku/DuplicateSkuResultController;
.super Ljava/lang/Object;
.source "DuplicateSkuResultController.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sku/DuplicateSkuResultController$NoDuplicateSkuResultController;,
        Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0002\u0012\u0013J\u0008\u0010\u0002\u001a\u00020\u0003H&J \u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tH&J\u0016\u0010\n\u001a\u00020\u00032\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cH&J\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00110\u00100\u000fH&J\u0014\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c0\u000fH&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/sku/DuplicateSkuResultController;",
        "",
        "onBackPressedOnDuplicateSkuResultScreen",
        "",
        "onVariationSelectedOnDuplicateSkuResultScreen",
        "itemId",
        "",
        "variationId",
        "type",
        "Lcom/squareup/api/items/Item$Type;",
        "showDuplicateSkuResult",
        "variationsWithDuplicateSku",
        "",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        "variationSelection",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;",
        "NoDuplicateSkuResultController",
        "VariationSelection",
        "sku_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onBackPressedOnDuplicateSkuResultScreen()V
.end method

.method public abstract onVariationSelectedOnDuplicateSkuResultScreen(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)V
.end method

.method public abstract showDuplicateSkuResult(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract variationSelection()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract variationsWithDuplicateSku()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;>;"
        }
    .end annotation
.end method
