.class public final enum Lcom/squareup/sqwidgets/time/AmPm;
.super Ljava/lang/Enum;
.source "AmPm.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/sqwidgets/time/AmPm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/sqwidgets/time/AmPm;

.field public static final enum AM:Lcom/squareup/sqwidgets/time/AmPm;

.field public static final enum PM:Lcom/squareup/sqwidgets/time/AmPm;


# instance fields
.field private final id:I

.field private final localizedName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 13
    new-instance v0, Lcom/squareup/sqwidgets/time/AmPm;

    const/4 v1, 0x0

    const-string v2, "AM"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/sqwidgets/time/AmPm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/sqwidgets/time/AmPm;->AM:Lcom/squareup/sqwidgets/time/AmPm;

    new-instance v0, Lcom/squareup/sqwidgets/time/AmPm;

    const/4 v2, 0x1

    const-string v3, "PM"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/sqwidgets/time/AmPm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/sqwidgets/time/AmPm;->PM:Lcom/squareup/sqwidgets/time/AmPm;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/sqwidgets/time/AmPm;

    .line 12
    sget-object v3, Lcom/squareup/sqwidgets/time/AmPm;->AM:Lcom/squareup/sqwidgets/time/AmPm;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/sqwidgets/time/AmPm;->PM:Lcom/squareup/sqwidgets/time/AmPm;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/sqwidgets/time/AmPm;->$VALUES:[Lcom/squareup/sqwidgets/time/AmPm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput p3, p0, Lcom/squareup/sqwidgets/time/AmPm;->id:I

    .line 62
    invoke-static {}, Ljava/text/DateFormatSymbols;->getInstance()Ljava/text/DateFormatSymbols;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object p1

    iget p2, p0, Lcom/squareup/sqwidgets/time/AmPm;->id:I

    aget-object p1, p1, p2

    iput-object p1, p0, Lcom/squareup/sqwidgets/time/AmPm;->localizedName:Ljava/lang/String;

    return-void
.end method

.method public static allDisplayNames()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 23
    invoke-static {}, Lcom/squareup/sqwidgets/time/AmPm;->values()[Lcom/squareup/sqwidgets/time/AmPm;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    .line 24
    invoke-virtual {v4}, Lcom/squareup/sqwidgets/time/AmPm;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static forDisplayName(Ljava/lang/String;)Lcom/squareup/sqwidgets/time/AmPm;
    .locals 5

    .line 36
    invoke-static {}, Lcom/squareup/sqwidgets/time/AmPm;->values()[Lcom/squareup/sqwidgets/time/AmPm;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 37
    iget-object v4, v3, Lcom/squareup/sqwidgets/time/AmPm;->localizedName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static forId(I)Lcom/squareup/sqwidgets/time/AmPm;
    .locals 5

    .line 51
    invoke-static {}, Lcom/squareup/sqwidgets/time/AmPm;->values()[Lcom/squareup/sqwidgets/time/AmPm;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 52
    invoke-virtual {v3}, Lcom/squareup/sqwidgets/time/AmPm;->getId()I

    move-result v4

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 56
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/sqwidgets/time/AmPm;
    .locals 1

    .line 12
    const-class v0, Lcom/squareup/sqwidgets/time/AmPm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/sqwidgets/time/AmPm;

    return-object p0
.end method

.method public static values()[Lcom/squareup/sqwidgets/time/AmPm;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/sqwidgets/time/AmPm;->$VALUES:[Lcom/squareup/sqwidgets/time/AmPm;

    invoke-virtual {v0}, [Lcom/squareup/sqwidgets/time/AmPm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/sqwidgets/time/AmPm;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/AmPm;->localizedName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 77
    iget v0, p0, Lcom/squareup/sqwidgets/time/AmPm;->id:I

    return v0
.end method
