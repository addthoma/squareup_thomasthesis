.class final Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$toClicksWithTimestamp$1;
.super Ljava/lang/Object;
.source "ClickSequenceTrigger.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->toClicksWithTimestamp(Lio/reactivex/Observable;Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $which:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;

.field final synthetic this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;


# direct methods
.method constructor <init>(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$toClicksWithTimestamp$1;->this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    iput-object p2, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$toClicksWithTimestamp$1;->$which:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Unit;)Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    new-instance p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;

    iget-object v0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$toClicksWithTimestamp$1;->this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    invoke-static {v0}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->access$getClock$p(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)Lcom/squareup/util/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$toClicksWithTimestamp$1;->$which:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;-><init>(JLcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;)V

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$toClicksWithTimestamp$1;->apply(Lkotlin/Unit;)Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;

    move-result-object p1

    return-object p1
.end method
