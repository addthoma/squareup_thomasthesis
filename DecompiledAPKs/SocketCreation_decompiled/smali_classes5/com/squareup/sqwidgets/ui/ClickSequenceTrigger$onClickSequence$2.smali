.class final Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;
.super Ljava/lang/Object;
.source "ClickSequenceTrigger.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->onClickSequence()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "TR;TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;",
        "state",
        "clickEvent",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;


# direct methods
.method constructor <init>(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;->this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;)Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;
    .locals 6

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clickEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClickSequenceTrigger timestamp: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;->getTimestamp()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " which: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;->getWhichClick()Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    .line 49
    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->getPreviousClickTimestamp()J

    move-result-wide v2

    iget-object v0, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;->this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    invoke-static {v0}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->access$getMaxTimeBetweenClicksMs$p(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)J

    move-result-wide v4

    .line 52
    invoke-virtual {p2, v2, v3, v4, v5}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;->isOlderThanThreshold(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    sget-object p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;->INSTANCE:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;

    check-cast p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;

    return-object p1

    .line 59
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;->getWhichClick()Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;

    move-result-object v0

    sget-object v2, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$WhichClickEvent;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    .line 78
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->getLeftClickCount()I

    move-result v0

    iget-object v3, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;->this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    invoke-static {v3}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->access$getNumberOfClicksToTrigger$p(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)I

    move-result v3

    if-eq v0, v3, :cond_1

    .line 79
    new-instance p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;

    const-wide/16 v2, 0x0

    invoke-direct {p1, v1, v1, v2, v3}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;-><init>(IIJ)V

    check-cast p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;

    goto :goto_1

    .line 85
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->getRightClickCount()I

    move-result v0

    add-int/2addr v0, v2

    .line 86
    iget-object v2, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;->this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    invoke-static {v2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->access$getNumberOfClicksToTrigger$p(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)I

    move-result v2

    if-ne v0, v2, :cond_2

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "ClickSequenceTrigger success!"

    .line 87
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    sget-object p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceSuccess;->INSTANCE:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceSuccess;

    check-cast p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;

    goto :goto_1

    .line 90
    :cond_2
    new-instance v1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;

    .line 91
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->getLeftClickCount()I

    move-result p1

    .line 93
    invoke-virtual {p2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;->getTimestamp()J

    move-result-wide v2

    .line 90
    invoke-direct {v1, p1, v0, v2, v3}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;-><init>(IIJ)V

    check-cast v1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;

    move-object p1, v1

    goto :goto_1

    .line 78
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 61
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->getLeftClickCount()I

    move-result v0

    add-int/2addr v0, v2

    .line 62
    iget-object v3, p0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;->this$0:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;

    invoke-static {v3}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;->access$getNumberOfClicksToTrigger$p(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger;)I

    move-result v3

    if-le v0, v3, :cond_5

    .line 63
    new-instance p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;

    .line 66
    invoke-virtual {p2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;->getTimestamp()J

    move-result-wide v3

    .line 63
    invoke-direct {p1, v2, v1, v3, v4}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;-><init>(IIJ)V

    goto :goto_0

    .line 69
    :cond_5
    new-instance v1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;

    .line 71
    invoke-virtual {p1}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;->getRightClickCount()I

    move-result p1

    .line 72
    invoke-virtual {p2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;->getTimestamp()J

    move-result-wide v2

    .line 69
    invoke-direct {v1, v0, p1, v2, v3}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceInProgress;-><init>(IIJ)V

    move-object p1, v1

    .line 62
    :goto_0
    check-cast p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;

    :goto_1
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;

    check-cast p2, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$onClickSequence$2;->apply(Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClicksWithTimestamp;)Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;

    move-result-object p1

    return-object p1
.end method
