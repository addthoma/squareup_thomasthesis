.class public Lcom/squareup/server/activation/ActivationResources;
.super Lcom/squareup/server/SimpleResponse;
.source "ActivationResources.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/activation/ActivationResources$RevenueEntry;,
        Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;,
        Lcom/squareup/server/activation/ActivationResources$BusinessCategory;
    }
.end annotation


# instance fields
.field private final business_categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$BusinessCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final estimated_revenue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$RevenueEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final shipping_message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    .line 17
    invoke-direct/range {v0 .. v6}, Lcom/squareup/server/activation/ActivationResources;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$BusinessCategory;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$RevenueEntry;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 23
    iput-object p4, p0, Lcom/squareup/server/activation/ActivationResources;->shipping_message:Ljava/lang/String;

    .line 24
    iput-object p5, p0, Lcom/squareup/server/activation/ActivationResources;->business_categories:Ljava/util/List;

    .line 25
    iput-object p6, p0, Lcom/squareup/server/activation/ActivationResources;->estimated_revenue:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getBusinessCategories()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$BusinessCategory;",
            ">;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationResources;->business_categories:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 31
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getEstimatedRevenueEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$RevenueEntry;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationResources;->estimated_revenue:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method
