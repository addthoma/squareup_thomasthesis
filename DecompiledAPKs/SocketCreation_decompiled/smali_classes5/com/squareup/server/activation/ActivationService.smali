.class public interface abstract Lcom/squareup/server/activation/ActivationService;
.super Ljava/lang/Object;
.source "ActivationService.java"


# virtual methods
.method public abstract activationUrl(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Query;
            value = "client_callback_url"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/activation/ActivationUrl;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/1.0/activation/activation-url"
    .end annotation
.end method

.method public abstract answers(Lcom/squareup/server/activation/AnswersBody;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Lcom/squareup/server/activation/AnswersBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/activation/AnswersBody;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/activation/answers"
    .end annotation
.end method

.method public abstract apply(Lcom/squareup/server/activation/ApplyBody;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Lcom/squareup/server/activation/ApplyBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/activation/ApplyBody;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/activation/apply"
    .end annotation
.end method

.method public abstract applyMoreInfo(Lcom/squareup/server/activation/ApplyMoreInfoBody;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Lcom/squareup/server/activation/ApplyMoreInfoBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/activation/ApplyMoreInfoBody;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/activation/update"
    .end annotation
.end method

.method public abstract createShippingAddress(Lcom/squareup/server/shipping/ShippingBody;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Lcom/squareup/server/shipping/ShippingBody;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/shipping/ShippingBody;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/shipping/UpdateAddressResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/shipping-address/create"
    .end annotation
.end method

.method public abstract merchantAnalytic(Lcom/squareup/protos/client/onboard/MerchantAnalyticRequest;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Lcom/squareup/protos/client/onboard/MerchantAnalyticRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/onboard/MerchantAnalyticRequest;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/onboard/merchant-analytic"
    .end annotation
.end method

.method public abstract resources()Lcom/squareup/server/SimpleStandardResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/activation/ActivationResources;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/1.0/activation/resources"
    .end annotation
.end method

.method public abstract retry(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/activation/retry"
    .end annotation
.end method

.method public abstract status()Lcom/squareup/server/SimpleStandardResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/activation/ActivationStatus;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/1.0/activation/status"
    .end annotation
.end method
