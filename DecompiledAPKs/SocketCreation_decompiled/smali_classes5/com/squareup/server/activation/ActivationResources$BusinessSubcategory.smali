.class public final Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;
.super Ljava/lang/Object;
.source "ActivationResources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/activation/ActivationResources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BusinessSubcategory"
.end annotation


# instance fields
.field public final description:Ljava/lang/String;

.field public final key:Ljava/lang/String;

.field public final skip_business_info:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;->key:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;->description:Ljava/lang/String;

    .line 80
    iput-boolean p3, p0, Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;->skip_business_info:Z

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;->description:Ljava/lang/String;

    return-object v0
.end method
