.class public interface abstract Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsService;
.super Ljava/lang/Object;
.source "TerminalCheckoutsService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsService$TerminalCheckoutsServiceResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u000cJ\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\"\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\t\u001a\u00020\n2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000bH\'\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsService;",
        "",
        "ackNextCheckout",
        "Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsService$TerminalCheckoutsServiceResponse;",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;",
        "request",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;",
        "cancelCheckout",
        "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;",
        "checkoutId",
        "",
        "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;",
        "TerminalCheckoutsServiceResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract ackNextCheckout(Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;)Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsService$TerminalCheckoutsServiceResponse;
    .param p1    # Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;",
            ")",
            "Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsService$TerminalCheckoutsServiceResponse<",
            "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json; charset=utf-8"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/terminals/checkouts/ack"
    .end annotation
.end method

.method public abstract cancelCheckout(Ljava/lang/String;Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;)Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsService$TerminalCheckoutsServiceResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "checkout_id"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;",
            ")",
            "Lcom/squareup/server/terminal/checkouts/TerminalCheckoutsService$TerminalCheckoutsServiceResponse<",
            "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/json; charset=utf-8"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/v2/terminals/checkouts/{checkout_id}/cancel"
    .end annotation
.end method
