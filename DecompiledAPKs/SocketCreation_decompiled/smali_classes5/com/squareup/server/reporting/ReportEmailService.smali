.class public interface abstract Lcom/squareup/server/reporting/ReportEmailService;
.super Ljava/lang/Object;
.source "ReportEmailService.java"


# virtual methods
.method public abstract emailReport(Lcom/squareup/server/reporting/ReportEmailBody;)Lcom/squareup/server/SimpleResponse;
    .param p1    # Lcom/squareup/server/reporting/ReportEmailBody;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/reports/email"
    .end annotation
.end method
