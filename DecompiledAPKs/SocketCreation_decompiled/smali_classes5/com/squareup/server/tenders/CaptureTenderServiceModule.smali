.class public final Lcom/squareup/server/tenders/CaptureTenderServiceModule;
.super Ljava/lang/Object;
.source "CaptureTenderServiceModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/server/tenders/CaptureTenderServiceModule;",
        "",
        "()V",
        "provideCaptureTendersService",
        "Lcom/squareup/server/tenders/CaptureTenderService;",
        "serviceCreator",
        "Lcom/squareup/api/ServiceCreator;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/server/tenders/CaptureTenderServiceModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/server/tenders/CaptureTenderServiceModule;

    invoke-direct {v0}, Lcom/squareup/server/tenders/CaptureTenderServiceModule;-><init>()V

    sput-object v0, Lcom/squareup/server/tenders/CaptureTenderServiceModule;->INSTANCE:Lcom/squareup/server/tenders/CaptureTenderServiceModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideCaptureTendersService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/tenders/CaptureTenderService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/Proto;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RealService;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "serviceCreator"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    const-class v0, Lcom/squareup/server/tenders/CaptureTenderService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    const-string v0, "serviceCreator.create(Ca\u2026enderService::class.java)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lcom/squareup/server/tenders/CaptureTenderService;

    return-object p0
.end method
