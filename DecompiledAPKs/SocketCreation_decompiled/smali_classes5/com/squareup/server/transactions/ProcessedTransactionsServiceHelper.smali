.class public final Lcom/squareup/server/transactions/ProcessedTransactionsServiceHelper;
.super Ljava/lang/Object;
.source "ProcessedTransactionsServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nProcessedTransactionsServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ProcessedTransactionsServiceHelper.kt\ncom/squareup/server/transactions/ProcessedTransactionsServiceHelper\n*L\n1#1,107:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u00062\u0006\u0010\t\u001a\u00020\nJB\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00080\u00070\u00062\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r2\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\r2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00102\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012J&\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/server/transactions/ProcessedTransactionsServiceHelper;",
        "",
        "service",
        "Lcom/squareup/server/transactions/ProcessedTransactionsService;",
        "(Lcom/squareup/server/transactions/ProcessedTransactionsService;)V",
        "getNextTransactionSummaries",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
        "nextPageRequest",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;",
        "getTransactionSummaries",
        "startTimeInclusive",
        "Lcom/squareup/protos/common/time/DateTime;",
        "endTimeExclusive",
        "queryString",
        "",
        "searchInstrument",
        "Lcom/squareup/protos/transactionsfe/SearchInstrument;",
        "validateInputs",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final service:Lcom/squareup/server/transactions/ProcessedTransactionsService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/transactions/ProcessedTransactionsService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/server/transactions/ProcessedTransactionsServiceHelper;->service:Lcom/squareup/server/transactions/ProcessedTransactionsService;

    return-void
.end method

.method public static synthetic getTransactionSummaries$default(Lcom/squareup/server/transactions/ProcessedTransactionsServiceHelper;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 37
    move-object p1, v0

    check-cast p1, Lcom/squareup/protos/common/time/DateTime;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 38
    move-object p2, v0

    check-cast p2, Lcom/squareup/protos/common/time/DateTime;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 39
    move-object p3, v0

    check-cast p3, Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 40
    move-object p4, v0

    check-cast p4, Lcom/squareup/protos/transactionsfe/SearchInstrument;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/server/transactions/ProcessedTransactionsServiceHelper;->getTransactionSummaries(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final validateInputs(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/transactionsfe/SearchInstrument;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_1

    .line 78
    iget-object v2, p3, Lcom/squareup/protos/transactionsfe/SearchInstrument;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    if-eqz v2, :cond_0

    iget-object p3, p3, Lcom/squareup/protos/transactionsfe/SearchInstrument;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-static {p3}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    :cond_1
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    return-void

    :cond_2
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 86
    iget-object p1, p1, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object p1, p2, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    const-string p2, "endTimeExclusive.instant_usec"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    cmp-long p3, v2, p1

    if-gez p3, :cond_3

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    .line 84
    :goto_1
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method


# virtual methods
.method public final getNextTransactionSummaries(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "nextPageRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/server/transactions/ProcessedTransactionsServiceHelper;->service:Lcom/squareup/server/transactions/ProcessedTransactionsService;

    invoke-interface {v0, p1}, Lcom/squareup/server/transactions/ProcessedTransactionsService;->getTransactionSummaries(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public final getTransactionSummaries(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/transactionsfe/SearchInstrument;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/transactionsfe/SearchInstrument;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
            ">;>;"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2, p4}, Lcom/squareup/server/transactions/ProcessedTransactionsServiceHelper;->validateInputs(Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/transactionsfe/SearchInstrument;)V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    move-object p1, v0

    goto :goto_0

    .line 49
    :cond_0
    new-instance v1, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;-><init>()V

    .line 50
    invoke-virtual {v1, p1}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->inclusive_start(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/common/time/DateTimeInterval$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->exclusive_end(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/common/time/DateTimeInterval$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->build()Lcom/squareup/protos/common/time/DateTimeInterval;

    move-result-object p1

    :goto_0
    if-nez p3, :cond_1

    if-nez p4, :cond_1

    goto :goto_1

    .line 56
    :cond_1
    new-instance p2, Lcom/squareup/protos/transactionsfe/Query$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/transactionsfe/Query$Builder;-><init>()V

    .line 57
    invoke-virtual {p2, p3}, Lcom/squareup/protos/transactionsfe/Query$Builder;->query_string(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/Query$Builder;

    move-result-object p2

    .line 58
    invoke-virtual {p2, p4}, Lcom/squareup/protos/transactionsfe/Query$Builder;->search_instrument(Lcom/squareup/protos/transactionsfe/SearchInstrument;)Lcom/squareup/protos/transactionsfe/Query$Builder;

    move-result-object p2

    .line 59
    invoke-virtual {p2}, Lcom/squareup/protos/transactionsfe/Query$Builder;->build()Lcom/squareup/protos/transactionsfe/Query;

    move-result-object v0

    .line 61
    :goto_1
    new-instance p2, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;-><init>()V

    .line 62
    invoke-virtual {p2, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    move-result-object p1

    .line 63
    invoke-virtual {p1, v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->query(Lcom/squareup/protos/transactionsfe/Query;)Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;

    move-result-object p1

    .line 64
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    move-result-object p1

    .line 66
    iget-object p2, p0, Lcom/squareup/server/transactions/ProcessedTransactionsServiceHelper;->service:Lcom/squareup/server/transactions/ProcessedTransactionsService;

    const-string p3, "request"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/transactions/ProcessedTransactionsService;->getTransactionSummaries(Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
