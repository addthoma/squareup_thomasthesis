.class final Lcom/squareup/server/messages/Message$Button$1;
.super Ljava/lang/Object;
.source "Message.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/messages/Message$Button;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/server/messages/Message$Button;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/server/messages/Message$Button;
    .locals 2

    .line 113
    new-instance v0, Lcom/squareup/server/messages/Message$Button;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/server/messages/Message$Button;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 111
    invoke-virtual {p0, p1}, Lcom/squareup/server/messages/Message$Button$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/server/messages/Message$Button;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/server/messages/Message$Button;
    .locals 0

    .line 117
    new-array p1, p1, [Lcom/squareup/server/messages/Message$Button;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 111
    invoke-virtual {p0, p1}, Lcom/squareup/server/messages/Message$Button$1;->newArray(I)[Lcom/squareup/server/messages/Message$Button;

    move-result-object p1

    return-object p1
.end method
