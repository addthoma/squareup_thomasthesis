.class public Lcom/squareup/server/payment/CreatePaymentResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "CreatePaymentResponse.java"


# instance fields
.field private final payment_id:Ljava/lang/String;

.field public final receipt_number:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 15
    iput-object p4, p0, Lcom/squareup/server/payment/CreatePaymentResponse;->payment_id:Ljava/lang/String;

    .line 16
    iput-object p5, p0, Lcom/squareup/server/payment/CreatePaymentResponse;->receipt_number:Ljava/lang/String;

    return-void
.end method

.method public static mock(Z)Lcom/squareup/server/payment/CreatePaymentResponse;
    .locals 7

    .line 20
    new-instance v6, Lcom/squareup/server/payment/CreatePaymentResponse;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "payment-id"

    const-string v5, "1234"

    move-object v0, v6

    move v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/payment/CreatePaymentResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v6
.end method


# virtual methods
.method public getPaymentId()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/server/payment/CreatePaymentResponse;->payment_id:Ljava/lang/String;

    return-object v0
.end method
