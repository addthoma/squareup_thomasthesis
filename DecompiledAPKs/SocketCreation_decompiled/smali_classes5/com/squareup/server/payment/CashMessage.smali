.class public Lcom/squareup/server/payment/CashMessage;
.super Lcom/squareup/server/payment/CreatePaymentMessage;
.source "CashMessage.java"


# instance fields
.field public final change_cents:J

.field public final tendered_cents:J


# direct methods
.method public constructor <init>(JJJJLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJJJ",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;)V"
        }
    .end annotation

    move-object v13, p0

    move-object v0, p0

    move-wide v1, p1

    move-wide/from16 v3, p7

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move-object/from16 v7, p11

    move-object/from16 v8, p12

    move-object/from16 v9, p13

    move-object/from16 v10, p14

    move/from16 v11, p15

    move-object/from16 v12, p16

    .line 21
    invoke-direct/range {v0 .. v12}, Lcom/squareup/server/payment/CreatePaymentMessage;-><init>(JJLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V

    move-wide/from16 v0, p3

    .line 23
    iput-wide v0, v13, Lcom/squareup/server/payment/CashMessage;->tendered_cents:J

    move-wide/from16 v0, p5

    .line 24
    iput-wide v0, v13, Lcom/squareup/server/payment/CashMessage;->change_cents:J

    return-void
.end method
