.class public Lcom/squareup/server/payment/Itemization;
.super Ljava/lang/Object;
.source "Itemization.java"


# instance fields
.field public final adjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field public final applied_modifier_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModifier;",
            ">;"
        }
    .end annotation
.end field

.field public final color:Ljava/lang/String;

.field public final created_at:Ljava/lang/String;

.field public final currency_code:Lcom/squareup/protos/common/CurrencyCode;

.field private final description:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final image_token:Ljava/lang/String;

.field private final image_url:Ljava/lang/String;

.field public final item_id:Ljava/lang/String;

.field private final item_name:Ljava/lang/String;

.field private final item_quantity:Lcom/squareup/quantity/DecimalQuantity;

.field public final item_variation_id:Ljava/lang/String;

.field public final item_variation_name:Ljava/lang/String;

.field private final item_version:Ljava/lang/String;

.field private final name:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final notes:Ljava/lang/String;

.field private final price_cents:J

.field private final price_money:Lcom/squareup/protos/common/Money;

.field private final quantity:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final quantity_precision:I

.field public final total_amount_cents:J

.field public final total_money:Lcom/squareup/protos/common/Money;

.field private final unit_abbreviation:Ljava/lang/String;

.field private final unit_name:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/math/BigDecimal;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/math/BigDecimal;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModifier;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move v1, p6

    move-object/from16 v2, p11

    move-object/from16 v3, p12

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v4, p1

    .line 161
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->item_id:Ljava/lang/String;

    const/4 v4, 0x0

    .line 162
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->name:Ljava/lang/String;

    move-object v5, p2

    .line 163
    iput-object v5, v0, Lcom/squareup/server/payment/Itemization;->item_name:Ljava/lang/String;

    move-object v5, p3

    .line 164
    iput-object v5, v0, Lcom/squareup/server/payment/Itemization;->notes:Ljava/lang/String;

    move-object/from16 v5, p16

    .line 165
    iput-object v5, v0, Lcom/squareup/server/payment/Itemization;->image_token:Ljava/lang/String;

    .line 166
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->description:Ljava/lang/String;

    .line 168
    iput v1, v0, Lcom/squareup/server/payment/Itemization;->quantity:I

    .line 169
    invoke-static {p7}, Lcom/squareup/quantity/DecimalQuantity;->of(Ljava/math/BigDecimal;)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object v4

    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->item_quantity:Lcom/squareup/quantity/DecimalQuantity;

    move v4, p8

    .line 170
    iput v4, v0, Lcom/squareup/server/payment/Itemization;->quantity_precision:I

    move-object v4, p9

    .line 171
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->unit_name:Ljava/lang/String;

    move-object/from16 v4, p10

    .line 172
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->unit_abbreviation:Ljava/lang/String;

    .line 173
    iput-object v2, v0, Lcom/squareup/server/payment/Itemization;->price_money:Lcom/squareup/protos/common/Money;

    .line 174
    iput-object v3, v0, Lcom/squareup/server/payment/Itemization;->total_money:Lcom/squareup/protos/common/Money;

    move-object v4, p4

    .line 175
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->item_variation_id:Ljava/lang/String;

    move-object v4, p5

    .line 176
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->item_variation_name:Ljava/lang/String;

    move-object/from16 v4, p13

    .line 177
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->adjustments:Ljava/util/List;

    move-object/from16 v4, p14

    .line 178
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->applied_modifier_options:Ljava/util/List;

    move-object/from16 v4, p15

    .line 180
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->color:Ljava/lang/String;

    move-object/from16 v4, p17

    .line 181
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->image_url:Ljava/lang/String;

    move-object/from16 v4, p18

    .line 182
    iput-object v4, v0, Lcom/squareup/server/payment/Itemization;->created_at:Ljava/lang/String;

    .line 184
    iget-object v4, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/squareup/server/payment/Itemization;->price_cents:J

    .line 185
    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iput-wide v3, v0, Lcom/squareup/server/payment/Itemization;->total_amount_cents:J

    .line 186
    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iput-object v2, v0, Lcom/squareup/server/payment/Itemization;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    move-object/from16 v2, p19

    .line 187
    iput-object v2, v0, Lcom/squareup/server/payment/Itemization;->item_version:Ljava/lang/String;

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    if-eqz p7, :cond_1

    :cond_0
    if-eq v1, v2, :cond_2

    if-nez p7, :cond_1

    goto :goto_0

    .line 191
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Exactly one of quantity and item_quantity must be set"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    return-void
.end method

.method public static forRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Lcom/squareup/server/payment/Itemization;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModifier;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/payment/Itemization;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v16, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    .line 139
    new-instance v20, Lcom/squareup/server/payment/Itemization;

    move-object/from16 v0, v20

    const/4 v6, -0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/squareup/server/payment/Itemization;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/math/BigDecimal;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v20
.end method

.method public static forTest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/payment/Itemization;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModifier;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/payment/Itemization;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v16, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v17, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    .line 150
    new-instance v20, Lcom/squareup/server/payment/Itemization;

    move-object/from16 v0, v20

    const/4 v6, -0x1

    invoke-direct/range {v0 .. v19}, Lcom/squareup/server/payment/Itemization;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/math/BigDecimal;ILjava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v20
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 257
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    .line 259
    :cond_1
    check-cast p1, Lcom/squareup/server/payment/Itemization;

    .line 261
    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->item_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->item_id:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 262
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getItemName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getItemName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 263
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getNotes()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getNotes()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 264
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getItemQuantity()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getItemQuantity()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/BigDecimals;->equalsIgnoringScale(Ljava/math/BigDecimal;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 265
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getQuantityPrecision()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getQuantityPrecision()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 266
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getUnitName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 267
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getPriceMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/Itemization;->getPriceMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->total_money:Lcom/squareup/protos/common/Money;

    .line 269
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->item_variation_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->item_variation_id:Ljava/lang/String;

    .line 270
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->item_variation_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->item_variation_name:Ljava/lang/String;

    .line 271
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->adjustments:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->adjustments:Ljava/util/List;

    .line 272
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->applied_modifier_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->applied_modifier_options:Ljava/util/List;

    .line 273
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->color:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->color:Ljava/lang/String;

    .line 274
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->image_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->image_url:Ljava/lang/String;

    .line 275
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->created_at:Ljava/lang/String;

    .line 276
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/squareup/server/payment/Itemization;->price_cents:J

    .line 277
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v3, p1, Lcom/squareup/server/payment/Itemization;->price_cents:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/squareup/server/payment/Itemization;->total_amount_cents:J

    .line 278
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v3, p1, Lcom/squareup/server/payment/Itemization;->total_amount_cents:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p1, Lcom/squareup/server/payment/Itemization;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 279
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->item_version:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/payment/Itemization;->item_version:Ljava/lang/String;

    .line 280
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .line 212
    iget-object v0, p0, Lcom/squareup/server/payment/Itemization;->image_url:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Urls;->validateImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemName()Ljava/lang/String;
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/squareup/server/payment/Itemization;->item_name:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/squareup/server/payment/Itemization;->item_quantity:Lcom/squareup/quantity/DecimalQuantity;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/squareup/server/payment/Itemization;->quantity:I

    .line 223
    invoke-static {v0}, Lcom/squareup/quantity/DecimalQuantity;->of(I)Lcom/squareup/quantity/DecimalQuantity;

    move-result-object v0

    .line 224
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/quantity/DecimalQuantity;->asBigDecimal()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getNotes()Ljava/lang/String;
    .locals 3

    .line 203
    iget-object v0, p0, Lcom/squareup/server/payment/Itemization;->notes:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->description:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPriceMoney()Lcom/squareup/protos/common/Money;
    .locals 5

    .line 248
    iget-wide v0, p0, Lcom/squareup/server/payment/Itemization;->price_cents:J

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->price_money:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 249
    iget-wide v0, p0, Lcom/squareup/server/payment/Itemization;->price_cents:J

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->price_money:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/payment/Itemization;->price_money:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getQuantityPrecision()I
    .locals 1

    .line 228
    iget v0, p0, Lcom/squareup/server/payment/Itemization;->quantity_precision:I

    return v0
.end method

.method public getUnitAbbreviation()Ljava/lang/String;
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/squareup/server/payment/Itemization;->unit_abbreviation:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getUnitName()Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/server/payment/Itemization;->unit_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    .line 286
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getItemName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 287
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getNotes()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 288
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getItemQuantity()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/BigDecimals;->normalizeScale(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 289
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getQuantityPrecision()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 290
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 291
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->price_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->item_variation_id:Ljava/lang/String;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->item_variation_name:Ljava/lang/String;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->adjustments:Ljava/util/List;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->color:Ljava/lang/String;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->image_url:Ljava/lang/String;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->created_at:Ljava/lang/String;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    iget-wide v1, p0, Lcom/squareup/server/payment/Itemization;->price_cents:J

    .line 300
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/16 v2, 0xe

    aput-object v1, v0, v2

    iget-wide v1, p0, Lcom/squareup/server/payment/Itemization;->total_amount_cents:J

    .line 301
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/16 v2, 0xf

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->item_version:Ljava/lang/String;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    .line 285
    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Itemization{item_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->item_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", item_name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getItemName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", notes=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getNotes()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", item_variation_id=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->item_variation_id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", item_variation_name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->item_variation_name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", quantity="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/squareup/server/payment/Itemization;->quantity:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", item_quantity="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->item_quantity:Lcom/squareup/quantity/DecimalQuantity;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", quantity_precision="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getQuantityPrecision()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", unit_name="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", unit_abbreviation="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    invoke-virtual {p0}, Lcom/squareup/server/payment/Itemization;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", price_money="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->price_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", total_money="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", adjustments="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->adjustments:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", applied_modifier_options="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->applied_modifier_options:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", color=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->color:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", image_token=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->image_token:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", image_url=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->image_url:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", created_at=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", item_version=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/Itemization;->item_version:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", price_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/server/payment/Itemization;->price_cents:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", total_amount_cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/server/payment/Itemization;->total_amount_cents:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", currency_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/Itemization;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
