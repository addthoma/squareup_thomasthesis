.class public Lcom/squareup/server/payment/ItemModifierMessage;
.super Ljava/lang/Object;
.source "ItemModifierMessage.java"


# instance fields
.field public final id:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final price_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/server/payment/ItemModifierMessage;->id:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/squareup/server/payment/ItemModifierMessage;->name:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/squareup/server/payment/ItemModifierMessage;->price_money:Lcom/squareup/protos/common/Money;

    return-void
.end method
