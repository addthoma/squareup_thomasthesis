.class public Lcom/squareup/server/payment/value/Adjustment;
.super Ljava/lang/Object;
.source "Adjustment.java"


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final applied:Lcom/squareup/protos/common/Money;

.field private final calculationPhase:I

.field private final childAdjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field private final couponToken:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

.field private final name:Ljava/lang/String;

.field private final percentage:Lcom/squareup/util/Percentage;

.field private final type:Ljava/lang/String;

.field private final typeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;ILcom/squareup/api/items/Fee$InclusionType;Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "I",
            "Lcom/squareup/api/items/Fee$InclusionType;",
            "Ljava/lang/String;",
            "Lcom/squareup/util/Percentage;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p4, p0, Lcom/squareup/server/payment/value/Adjustment;->applied:Lcom/squareup/protos/common/Money;

    .line 59
    iput-object p10, p0, Lcom/squareup/server/payment/value/Adjustment;->childAdjustments:Ljava/util/List;

    .line 60
    iput p5, p0, Lcom/squareup/server/payment/value/Adjustment;->calculationPhase:I

    .line 61
    iput-object p1, p0, Lcom/squareup/server/payment/value/Adjustment;->id:Ljava/lang/String;

    .line 62
    iput-object p6, p0, Lcom/squareup/server/payment/value/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    .line 63
    iput-object p7, p0, Lcom/squareup/server/payment/value/Adjustment;->name:Ljava/lang/String;

    .line 64
    iput-object p8, p0, Lcom/squareup/server/payment/value/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    .line 65
    iput-object p9, p0, Lcom/squareup/server/payment/value/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    .line 66
    iput-object p2, p0, Lcom/squareup/server/payment/value/Adjustment;->type:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/squareup/server/payment/value/Adjustment;->typeId:Ljava/lang/String;

    .line 68
    iput-object p11, p0, Lcom/squareup/server/payment/value/Adjustment;->couponToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 124
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    .line 126
    :cond_1
    check-cast p1, Lcom/squareup/server/payment/value/Adjustment;

    .line 128
    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->applied:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/payment/value/Adjustment;->applied:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/squareup/server/payment/value/Adjustment;->calculationPhase:I

    .line 129
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/squareup/server/payment/value/Adjustment;->calculationPhase:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/value/Adjustment;->id:Ljava/lang/String;

    .line 130
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 131
    invoke-virtual {p0}, Lcom/squareup/server/payment/value/Adjustment;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/server/payment/value/Adjustment;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/value/Adjustment;->name:Ljava/lang/String;

    .line 132
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    iget-object v3, p1, Lcom/squareup/server/payment/value/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    .line 133
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/payment/value/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    .line 134
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/value/Adjustment;->type:Ljava/lang/String;

    .line 135
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->typeId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/value/Adjustment;->typeId:Ljava/lang/String;

    .line 136
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->couponToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/payment/value/Adjustment;->couponToken:Ljava/lang/String;

    .line 137
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getApplied()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->applied:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getCalculationPhase()I
    .locals 1

    .line 106
    iget v0, p0, Lcom/squareup/server/payment/value/Adjustment;->calculationPhase:I

    return v0
.end method

.method public getChildAdjustments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/ItemizedAdjustment;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->childAdjustments:Ljava/util/List;

    return-object v0
.end method

.method public getCouponToken()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->couponToken:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v0, :cond_0

    return-object v0

    .line 77
    :cond_0
    sget-object v1, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPercentage()Lcom/squareup/util/Percentage;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    return-object v0
.end method

.method public getRawInclusionType()Lcom/squareup/api/items/Fee$InclusionType;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeId()Ljava/lang/String;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/server/payment/value/Adjustment;->typeId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    .line 142
    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->applied:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/server/payment/value/Adjustment;->calculationPhase:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->id:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->name:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->type:Ljava/lang/String;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->typeId:Ljava/lang/String;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->couponToken:Ljava/lang/String;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Adjustment{applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->applied:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/server/payment/value/Adjustment;->calculationPhase:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", inclusion_type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", percentage="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", type=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->type:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", type_id=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/server/payment/value/Adjustment;->typeId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", coupon_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/payment/value/Adjustment;->couponToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
