.class public final Lcom/squareup/server/SimpleStandardResponse;
.super Lcom/squareup/server/StandardResponse;
.source "SimpleStandardResponse.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/server/SimpleResponse;",
        ">",
        "Lcom/squareup/server/StandardResponse<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\u0010\u0006J\u0015\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/server/SimpleStandardResponse;",
        "T",
        "Lcom/squareup/server/SimpleResponse;",
        "Lcom/squareup/server/StandardResponse;",
        "factory",
        "Lcom/squareup/server/StandardResponse$Factory;",
        "(Lcom/squareup/server/StandardResponse$Factory;)V",
        "isSuccessful",
        "",
        "response",
        "(Lcom/squareup/server/SimpleResponse;)Z",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/server/StandardResponse$Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/StandardResponse$Factory<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1}, Lcom/squareup/server/StandardResponse;-><init>(Lcom/squareup/server/StandardResponse$Factory;)V

    return-void
.end method


# virtual methods
.method protected isSuccessful(Lcom/squareup/server/SimpleResponse;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->isSuccessful()Z

    move-result p1

    return p1
.end method

.method public bridge synthetic isSuccessful(Ljava/lang/Object;)Z
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/server/SimpleStandardResponse;->isSuccessful(Lcom/squareup/server/SimpleResponse;)Z

    move-result p1

    return p1
.end method
