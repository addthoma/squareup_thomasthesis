.class public final Lcom/squareup/server/account/protos/User$MerchantKey;
.super Lcom/squareup/wire/AndroidMessage;
.source "User.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MerchantKey"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/User$MerchantKey$ProtoAdapter_MerchantKey;,
        Lcom/squareup/server/account/protos/User$MerchantKey$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/User$MerchantKey;",
        "Lcom/squareup/server/account/protos/User$MerchantKey$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/User$MerchantKey;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/User$MerchantKey;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/User$MerchantKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/User$MerchantKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_MASTER_KEY_ID:Ljava/lang/Integer;

.field public static final DEFAULT_TIMESTAMP:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final master_key_id:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1231
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantKey$ProtoAdapter_MerchantKey;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$MerchantKey$ProtoAdapter_MerchantKey;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/User$MerchantKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1233
    sget-object v0, Lcom/squareup/server/account/protos/User$MerchantKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/User$MerchantKey;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 1237
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/User$MerchantKey;->DEFAULT_MASTER_KEY_ID:Ljava/lang/Integer;

    const-wide/16 v0, 0x0

    .line 1239
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/User$MerchantKey;->DEFAULT_TIMESTAMP:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 1

    .line 1275
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/User$MerchantKey;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 1280
    sget-object v0, Lcom/squareup/server/account/protos/User$MerchantKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1281
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    .line 1282
    iput-object p2, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    .line 1283
    iput-object p3, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/User$MerchantKey$Builder;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 1347
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantKey;->newBuilder()Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1299
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1300
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    .line 1301
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantKey;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantKey;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    .line 1302
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    .line 1303
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    .line 1304
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1309
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_3

    .line 1311
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantKey;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1312
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1313
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1314
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 1315
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/User$MerchantKey$Builder;
    .locals 2

    .line 1288
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;-><init>()V

    .line 1289
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->master_key_id:Ljava/lang/Integer;

    .line 1290
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->timestamp:Ljava/lang/Long;

    .line 1291
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->key:Ljava/lang/String;

    .line 1292
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantKey;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1230
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantKey;->newBuilder()Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/User$MerchantKey;)Lcom/squareup/server/account/protos/User$MerchantKey;
    .locals 2

    .line 1340
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey;->requireBuilder(Lcom/squareup/server/account/protos/User$MerchantKey$Builder;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->master_key_id(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v1

    .line 1341
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey;->requireBuilder(Lcom/squareup/server/account/protos/User$MerchantKey$Builder;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->timestamp(Ljava/lang/Long;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v1

    .line 1342
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey;->requireBuilder(Lcom/squareup/server/account/protos/User$MerchantKey$Builder;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->key(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 1343
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 1230
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$MerchantKey;->overlay(Lcom/squareup/server/account/protos/User$MerchantKey;)Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/User$MerchantKey;
    .locals 2

    .line 1332
    iget-object v0, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey;->requireBuilder(Lcom/squareup/server/account/protos/User$MerchantKey$Builder;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/User$MerchantKey;->DEFAULT_MASTER_KEY_ID:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->master_key_id(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v1

    .line 1333
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey;->requireBuilder(Lcom/squareup/server/account/protos/User$MerchantKey$Builder;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/User$MerchantKey;->DEFAULT_TIMESTAMP:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->timestamp(Ljava/lang/Long;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 1334
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 1230
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$MerchantKey;->populateDefaults()Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1323
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", master_key_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1324
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1325
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MerchantKey{"

    .line 1326
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
