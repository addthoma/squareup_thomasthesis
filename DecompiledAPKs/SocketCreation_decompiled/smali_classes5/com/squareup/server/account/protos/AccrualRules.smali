.class public final Lcom/squareup/server/account/protos/AccrualRules;
.super Lcom/squareup/wire/AndroidMessage;
.source "AccrualRules.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/AccrualRules$ProtoAdapter_AccrualRules;,
        Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;,
        Lcom/squareup/server/account/protos/AccrualRules$ItemRules;,
        Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;,
        Lcom/squareup/server/account/protos/AccrualRules$VisitRules;,
        Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;,
        Lcom/squareup/server/account/protos/AccrualRules$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules;",
        "Lcom/squareup/server/account/protos/AccrualRules$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/AccrualRules;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/AccrualRules;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/AccrualRules;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AccrualRules$AccrualRulesOptions#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AccrualRules$ItemRules#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AccrualRules$SpendRateRules#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AccrualRules$VisitRules#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$ProtoAdapter_AccrualRules;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$ProtoAdapter_AccrualRules;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 38
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;Lcom/squareup/server/account/protos/AccrualRules$VisitRules;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;Lcom/squareup/server/account/protos/AccrualRules$ItemRules;)V
    .locals 6

    .line 69
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/AccrualRules;-><init>(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;Lcom/squareup/server/account/protos/AccrualRules$VisitRules;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;Lcom/squareup/server/account/protos/AccrualRules$ItemRules;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;Lcom/squareup/server/account/protos/AccrualRules$VisitRules;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;Lcom/squareup/server/account/protos/AccrualRules$ItemRules;Lokio/ByteString;)V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 75
    invoke-static {p2, p3, p4}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p5

    const/4 v0, 0x1

    if-gt p5, v0, :cond_0

    .line 78
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    .line 79
    iput-object p2, p0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    .line 80
    iput-object p3, p0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    .line 81
    iput-object p4, p0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    return-void

    .line 76
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of visit_rules, spend_rate_rules, item_rules may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 164
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 98
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/AccrualRules;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 99
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    .line 104
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_4

    .line 111
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 116
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/AccrualRules$Builder;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$Builder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    .line 88
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    .line 89
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    .line 90
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/server/account/protos/AccrualRules;
    .locals 2

    .line 156
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    .line 157
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules(Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    .line 158
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules(Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    .line 159
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules(Lcom/squareup/server/account/protos/AccrualRules$ItemRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object p1, p0

    goto :goto_0

    .line 160
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules;->overlay(Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/AccrualRules;
    .locals 3

    .line 134
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object v0

    .line 136
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->accrual_rules_options(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    if-eqz v0, :cond_1

    .line 139
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    move-result-object v0

    .line 140
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->visit_rules(Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    if-eqz v0, :cond_2

    .line 143
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    move-result-object v0

    .line 144
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    if-eq v0, v2, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->spend_rate_rules(Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    if-eqz v0, :cond_3

    .line 147
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    move-result-object v0

    .line 148
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    if-eq v0, v2, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$Builder;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->item_rules(Lcom/squareup/server/account/protos/AccrualRules$ItemRules;)Lcom/squareup/server/account/protos/AccrualRules$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object v0, p0

    goto :goto_0

    .line 150
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    if-eqz v1, :cond_0

    const-string v1, ", accrual_rules_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    if-eqz v1, :cond_1

    const-string v1, ", visit_rules="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    if-eqz v1, :cond_2

    const-string v1, ", spend_rate_rules="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    if-eqz v1, :cond_3

    const-string v1, ", item_rules="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AccrualRules{"

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
