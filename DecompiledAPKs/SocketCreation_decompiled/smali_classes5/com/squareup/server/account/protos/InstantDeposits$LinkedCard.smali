.class public final Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
.super Lcom/squareup/wire/AndroidMessage;
.source "InstantDeposits.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LinkedCard"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$ProtoAdapter_LinkedCard;,
        Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;,
        Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;,
        Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CARD_STATUS:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.InstantDeposits$LinkedCard$CardDetails#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.InstantDeposits$LinkedCard$CardStatus#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 184
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$ProtoAdapter_LinkedCard;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$ProtoAdapter_LinkedCard;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 186
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 190
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->UNKNOWN:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->DEFAULT_CARD_STATUS:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;)V
    .locals 1

    .line 207
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;-><init>(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;Lokio/ByteString;)V
    .locals 1

    .line 212
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 213
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    .line 214
    iput-object p2, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 229
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 230
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    .line 231
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    iget-object v3, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    iget-object p1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    .line 233
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 238
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 240
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 243
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;
    .locals 2

    .line 219
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;-><init>()V

    .line 220
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    .line 221
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    iput-object v1, v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    .line 222
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
    .locals 2

    .line 270
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v1

    .line 271
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_status(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 272
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->overlay(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
    .locals 3

    .line 259
    iget-object v0, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object v0

    .line 261
    iget-object v2, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v1

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->requireBuilder(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->DEFAULT_CARD_STATUS:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_status(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 264
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 183
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->populateDefaults()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 251
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    if-eqz v1, :cond_0

    const-string v1, ", card_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    if-eqz v1, :cond_1

    const-string v1, ", card_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LinkedCard{"

    .line 253
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
