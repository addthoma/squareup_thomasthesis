.class public final Lcom/squareup/server/account/protos/FeeType$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FeeType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FeeType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FeeType;",
        "Lcom/squareup/server/account/protos/FeeType$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public calculation_phase:Ljava/lang/Integer;

.field public id:Ljava/lang/String;

.field public inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

.field public is_default:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 203
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FeeType;
    .locals 9

    .line 238
    new-instance v8, Lcom/squareup/server/account/protos/FeeType;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->calculation_phase:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->is_default:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->percentage:Ljava/lang/Double;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/FeeType;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Double;Lcom/squareup/api/items/Fee$InclusionType;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 190
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeType$Builder;->build()Lcom/squareup/server/account/protos/FeeType;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/FeeType$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->calculation_phase:Ljava/lang/Integer;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public inclusion_type(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/server/account/protos/FeeType$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    return-object p0
.end method

.method public is_default(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FeeType$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->is_default:Ljava/lang/Boolean;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/Double;)Lcom/squareup/server/account/protos/FeeType$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeType$Builder;->percentage:Ljava/lang/Double;

    return-object p0
.end method
