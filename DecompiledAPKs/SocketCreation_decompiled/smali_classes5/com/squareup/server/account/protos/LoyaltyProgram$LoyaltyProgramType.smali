.class public final enum Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;
.super Ljava/lang/Enum;
.source "LoyaltyProgram.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/LoyaltyProgram;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LoyaltyProgramType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType$ProtoAdapter_LoyaltyProgramType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum POINTS:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

.field public static final enum PROGRAM_TYPE_UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

.field public static final enum PUNCH_CARD:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 376
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    const/4 v1, 0x0

    const-string v2, "PROGRAM_TYPE_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->PROGRAM_TYPE_UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    .line 381
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    const/4 v2, 0x1

    const-string v3, "PUNCH_CARD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->PUNCH_CARD:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    .line 386
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    const/4 v3, 0x2

    const-string v4, "POINTS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->POINTS:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    .line 375
    sget-object v4, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->PROGRAM_TYPE_UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->PUNCH_CARD:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->POINTS:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->$VALUES:[Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    .line 388
    new-instance v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType$ProtoAdapter_LoyaltyProgramType;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType$ProtoAdapter_LoyaltyProgramType;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 392
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 393
    iput p3, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 403
    :cond_0
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->POINTS:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    return-object p0

    .line 402
    :cond_1
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->PUNCH_CARD:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    return-object p0

    .line 401
    :cond_2
    sget-object p0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->PROGRAM_TYPE_UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;
    .locals 1

    .line 375
    const-class v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;
    .locals 1

    .line 375
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->$VALUES:[Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    invoke-virtual {v0}, [Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 410
    iget v0, p0, Lcom/squareup/server/account/protos/LoyaltyProgram$LoyaltyProgramType;->value:I

    return v0
.end method
