.class final Lcom/squareup/server/account/protos/Tutorial$ProtoAdapter_Tutorial;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tutorial.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Tutorial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Tutorial"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/Tutorial;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 149
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/Tutorial;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/Tutorial;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 168
    new-instance v0, Lcom/squareup/server/account/protos/Tutorial$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Tutorial$Builder;-><init>()V

    .line 169
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 170
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 175
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 173
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_item_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    goto :goto_0

    .line 172
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_payment_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    goto :goto_0

    .line 179
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 180
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Tutorial$Builder;->build()Lcom/squareup/server/account/protos/Tutorial;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 147
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Tutorial$ProtoAdapter_Tutorial;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/Tutorial;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/Tutorial;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 161
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 162
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 163
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/Tutorial;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 147
    check-cast p2, Lcom/squareup/server/account/protos/Tutorial;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/Tutorial$ProtoAdapter_Tutorial;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/Tutorial;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/Tutorial;)I
    .locals 4

    .line 154
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 155
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Tutorial;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 147
    check-cast p1, Lcom/squareup/server/account/protos/Tutorial;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Tutorial$ProtoAdapter_Tutorial;->encodedSize(Lcom/squareup/server/account/protos/Tutorial;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/Tutorial;
    .locals 0

    .line 185
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Tutorial;->newBuilder()Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object p1

    .line 186
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 187
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->build()Lcom/squareup/server/account/protos/Tutorial;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 147
    check-cast p1, Lcom/squareup/server/account/protos/Tutorial;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Tutorial$ProtoAdapter_Tutorial;->redact(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/Tutorial;

    move-result-object p1

    return-object p1
.end method
