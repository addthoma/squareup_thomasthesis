.class public final Lcom/squareup/server/account/protos/ProcessingFees;
.super Lcom/squareup/wire/AndroidMessage;
.source "ProcessingFees.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/ProcessingFees$ProtoAdapter_ProcessingFees;,
        Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/ProcessingFees;",
        "Lcom/squareup/server/account/protos/ProcessingFees$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/ProcessingFees;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/ProcessingFees;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/ProcessingFees;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/ProcessingFees;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final cnp:Lcom/squareup/server/account/protos/ProcessingFee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ProcessingFee#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final cof:Lcom/squareup/server/account/protos/ProcessingFee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ProcessingFee#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final cp:Lcom/squareup/server/account/protos/ProcessingFee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ProcessingFee#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final interac:Lcom/squareup/server/account/protos/ProcessingFee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ProcessingFee#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ProcessingFee#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ProcessingFee#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/server/account/protos/ProcessingFees$ProtoAdapter_ProcessingFees;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ProcessingFees$ProtoAdapter_ProcessingFees;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/ProcessingFees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/ProcessingFees;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;)V
    .locals 8

    .line 79
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/ProcessingFees;-><init>(Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lcom/squareup/server/account/protos/ProcessingFee;Lokio/ByteString;)V
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/server/account/protos/ProcessingFees;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 86
    iput-object p1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 87
    iput-object p2, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 88
    iput-object p3, p0, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 89
    iput-object p4, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 90
    iput-object p5, p0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 91
    iput-object p6, p0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFees;->newBuilder()Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 110
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/ProcessingFees;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 111
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/ProcessingFees;

    .line 112
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFees;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFees;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v3, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v3, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v3, p1, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v3, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object v3, p1, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    iget-object p1, p1, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 118
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 123
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_6

    .line 125
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFees;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFee;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFee;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFee;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFee;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFee;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFee;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 132
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/ProcessingFees$Builder;
    .locals 2

    .line 96
    new-instance v0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 98
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 99
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 100
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 101
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 102
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFees;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFees;->newBuilder()Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/ProcessingFees;)Lcom/squareup/server/account/protos/ProcessingFees;
    .locals 2

    .line 182
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 183
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 184
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 185
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 186
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 187
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object p1, p0

    goto :goto_0

    .line 188
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/server/account/protos/ProcessingFees;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProcessingFees;->overlay(Lcom/squareup/server/account/protos/ProcessingFees;)Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/ProcessingFees;
    .locals 3

    .line 152
    iget-object v0, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFee;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 154
    iget-object v2, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cnp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_1

    .line 157
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFee;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 158
    iget-object v2, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_2

    .line 161
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFee;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 162
    iget-object v2, p0, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eq v0, v2, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->invoice_web_form(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_3

    .line 165
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFee;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 166
    iget-object v2, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eq v0, v2, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->cof(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_4

    .line 169
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFee;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 170
    iget-object v2, p0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eq v0, v2, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->interac(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    .line 172
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v0, :cond_5

    .line 173
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFee;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v0

    .line 174
    iget-object v2, p0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eq v0, v2, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ProcessingFees;->requireBuilder(Lcom/squareup/server/account/protos/ProcessingFees$Builder;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->jcb_cp(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFees$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object v0, p0

    goto :goto_0

    .line 176
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ProcessingFees$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ProcessingFees;->populateDefaults()Lcom/squareup/server/account/protos/ProcessingFees;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_0

    const-string v1, ", cnp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cnp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_1

    const-string v1, ", cp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_2

    const-string v1, ", invoice_web_form="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->invoice_web_form:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_3

    const-string v1, ", cof="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->cof:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_4

    const-string v1, ", interac="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->interac:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    if-eqz v1, :cond_5

    const-string v1, ", jcb_cp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ProcessingFees;->jcb_cp:Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ProcessingFees{"

    .line 146
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
