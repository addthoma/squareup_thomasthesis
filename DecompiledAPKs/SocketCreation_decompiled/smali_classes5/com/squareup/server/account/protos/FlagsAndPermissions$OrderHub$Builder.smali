.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_bazaar_orders:Ljava/lang/Boolean;

.field public automatic_action_max_age_seconds:Ljava/lang/Long;

.field public can_search_orders:Ljava/lang/Boolean;

.field public can_show_quick_actions_setting:Ljava/lang/Boolean;

.field public can_support_ecom_delivery_orders:Ljava/lang/Boolean;

.field public can_support_upcoming_orders:Ljava/lang/Boolean;

.field public default_search_range_millis:Ljava/lang/Long;

.field public throttle_order_search:Ljava/lang/Boolean;

.field public throttled_search_range_millis:Ljava/lang/Long;

.field public use_employee_permission:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18336
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_bazaar_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18355
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->allow_bazaar_orders:Ljava/lang/Boolean;

    return-object p0
.end method

.method public automatic_action_max_age_seconds(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18360
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->automatic_action_max_age_seconds:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;
    .locals 13

    .line 18391
    new-instance v12, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_upcoming_orders:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->use_employee_permission:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->allow_bazaar_orders:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->automatic_action_max_age_seconds:Ljava/lang/Long;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_search_orders:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttle_order_search:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttled_search_range_millis:Ljava/lang/Long;

    iget-object v9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->default_search_range_millis:Ljava/lang/Long;

    iget-object v10, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 18315
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub;

    move-result-object v0

    return-object v0
.end method

.method public can_search_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18365
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_search_orders:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_show_quick_actions_setting(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18350
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_show_quick_actions_setting:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_support_ecom_delivery_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18385
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_ecom_delivery_orders:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_support_upcoming_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18340
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->can_support_upcoming_orders:Ljava/lang/Boolean;

    return-object p0
.end method

.method public default_search_range_millis(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18380
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->default_search_range_millis:Ljava/lang/Long;

    return-object p0
.end method

.method public throttle_order_search(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18370
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttle_order_search:Ljava/lang/Boolean;

    return-object p0
.end method

.method public throttled_search_range_millis(Ljava/lang/Long;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18375
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->throttled_search_range_millis:Ljava/lang/Long;

    return-object p0
.end method

.method public use_employee_permission(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;
    .locals 0

    .line 18345
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$OrderHub$Builder;->use_employee_permission:Ljava/lang/Boolean;

    return-object p0
.end method
