.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loyalty"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$ProtoAdapter_Loyalty;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALPHANUMERIC_COUPONS:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SUPPORT_NEGATIVE_BALANCES:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_USE_CASH_INTEGRATION:Ljava/lang/Boolean;

.field public static final DEFAULT_DELETE_WILL_SEND_SMS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXPIRE_POINTS:Ljava/lang/Boolean;

.field public static final DEFAULT_LOYALTY_ENROLLMENT_WORKFLOW:Ljava/lang/Boolean;

.field public static final DEFAULT_LOYALTY_SMS:Ljava/lang/Boolean;

.field public static final DEFAULT_SEE_ENROLLMENT_TUTORIAL:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOULD_LOYALTY_HANDLE_RETURNS:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_LOYALTY:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_LOYALTY_VALUE_METRICS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_COUPON_ITEM_SELECTION:Ljava/lang/Boolean;

.field public static final DEFAULT_X2_FRONT_OF_TRANSACTION_CHECKIN:Ljava/lang/Boolean;

.field public static final DEFAULT_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Ljava/lang/Boolean;

.field public static final DEFAULT_X2_POST_TRANSACTION_DARK_THEME:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final alphanumeric_coupons:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final can_support_negative_balances:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final can_use_cash_integration:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final delete_will_send_sms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final expire_points:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final loyalty_enrollment_workflow:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x19
    .end annotation
.end field

.field public final loyalty_sms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final see_enrollment_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final should_loyalty_handle_returns:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final show_loyalty:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final show_loyalty_value_metrics:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final use_coupon_item_selection:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final x2_front_of_transaction_checkin:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x18
    .end annotation
.end field

.field public final x2_post_transaction_dark_theme:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4240
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$ProtoAdapter_Loyalty;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$ProtoAdapter_Loyalty;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4242
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 4246
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_ALPHANUMERIC_COUPONS:Ljava/lang/Boolean;

    .line 4248
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_CAN_USE_CASH_INTEGRATION:Ljava/lang/Boolean;

    .line 4250
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_SEE_ENROLLMENT_TUTORIAL:Ljava/lang/Boolean;

    .line 4252
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_EXPIRE_POINTS:Ljava/lang/Boolean;

    .line 4254
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_SHOW_LOYALTY:Ljava/lang/Boolean;

    .line 4256
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_LOYALTY_SMS:Ljava/lang/Boolean;

    .line 4258
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_X2_POST_TRANSACTION_DARK_THEME:Ljava/lang/Boolean;

    .line 4260
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_X2_FRONT_OF_TRANSACTION_CHECKIN:Ljava/lang/Boolean;

    .line 4262
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_USE_COUPON_ITEM_SELECTION:Ljava/lang/Boolean;

    .line 4264
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_DELETE_WILL_SEND_SMS:Ljava/lang/Boolean;

    .line 4266
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_SHOULD_LOYALTY_HANDLE_RETURNS:Ljava/lang/Boolean;

    .line 4268
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_CAN_SUPPORT_NEGATIVE_BALANCES:Ljava/lang/Boolean;

    .line 4270
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_SHOW_LOYALTY_VALUE_METRICS:Ljava/lang/Boolean;

    .line 4272
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Ljava/lang/Boolean;

    .line 4274
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_LOYALTY_ENROLLMENT_WORKFLOW:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 4439
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 4453
    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 4454
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    move-object v1, p2

    .line 4455
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    move-object v1, p3

    .line 4456
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    move-object v1, p4

    .line 4457
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    move-object v1, p5

    .line 4458
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    move-object v1, p6

    .line 4459
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    move-object v1, p7

    .line 4460
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    move-object v1, p8

    .line 4461
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    move-object v1, p9

    .line 4462
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    move-object v1, p10

    .line 4463
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    move-object v1, p11

    .line 4464
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    move-object v1, p12

    .line 4465
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    move-object/from16 v1, p13

    .line 4466
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    move-object/from16 v1, p14

    .line 4467
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    move-object/from16 v1, p15

    .line 4468
    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 4605
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4496
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4497
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    .line 4498
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    .line 4499
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    .line 4500
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    .line 4501
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    .line 4502
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    .line 4503
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    .line 4504
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    .line 4505
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    .line 4506
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    .line 4507
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    .line 4508
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    .line 4509
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    .line 4510
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    .line 4511
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    .line 4512
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    .line 4513
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4518
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_f

    .line 4520
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4521
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4522
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4523
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4524
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4525
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4526
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4527
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4528
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4529
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4530
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4531
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4532
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4533
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4534
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4535
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_e
    add-int/2addr v0, v2

    .line 4536
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_f
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 2

    .line 4473
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;-><init>()V

    .line 4474
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->alphanumeric_coupons:Ljava/lang/Boolean;

    .line 4475
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_use_cash_integration:Ljava/lang/Boolean;

    .line 4476
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->see_enrollment_tutorial:Ljava/lang/Boolean;

    .line 4477
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->expire_points:Ljava/lang/Boolean;

    .line 4478
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty:Ljava/lang/Boolean;

    .line 4479
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_sms:Ljava/lang/Boolean;

    .line 4480
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    .line 4481
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    .line 4482
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->use_coupon_item_selection:Ljava/lang/Boolean;

    .line 4483
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->delete_will_send_sms:Ljava/lang/Boolean;

    .line 4484
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    .line 4485
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_support_negative_balances:Ljava/lang/Boolean;

    .line 4486
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    .line 4487
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    .line 4488
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    .line 4489
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4239
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;
    .locals 2

    .line 4586
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->alphanumeric_coupons(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4587
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_use_cash_integration(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4588
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->see_enrollment_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4589
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->expire_points(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4590
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4591
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4592
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_post_transaction_dark_theme(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4593
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4594
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->use_coupon_item_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4595
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->delete_will_send_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4596
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->should_loyalty_handle_returns(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4597
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_support_negative_balances(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4598
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty_value_metrics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4599
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin_seller_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4600
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_enrollment_workflow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    :cond_e
    if-nez v1, :cond_f

    move-object p1, p0

    goto :goto_0

    .line 4601
    :cond_f
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 4239
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;
    .locals 2

    .line 4565
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_ALPHANUMERIC_COUPONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->alphanumeric_coupons(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4566
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_CAN_USE_CASH_INTEGRATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_use_cash_integration(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4567
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_SEE_ENROLLMENT_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->see_enrollment_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4568
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_EXPIRE_POINTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->expire_points(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4569
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_SHOW_LOYALTY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4570
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_LOYALTY_SMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4571
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_X2_POST_TRANSACTION_DARK_THEME:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_post_transaction_dark_theme(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4572
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_X2_FRONT_OF_TRANSACTION_CHECKIN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4573
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_USE_COUPON_ITEM_SELECTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->use_coupon_item_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4574
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_DELETE_WILL_SEND_SMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->delete_will_send_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4575
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_SHOULD_LOYALTY_HANDLE_RETURNS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->should_loyalty_handle_returns(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4576
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_CAN_SUPPORT_NEGATIVE_BALANCES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_support_negative_balances(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4577
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_SHOW_LOYALTY_VALUE_METRICS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty_value_metrics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4578
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin_seller_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    .line 4579
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->DEFAULT_LOYALTY_ENROLLMENT_WORKFLOW:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_enrollment_workflow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;

    move-result-object v1

    :cond_e
    if-nez v1, :cond_f

    move-object v0, p0

    goto :goto_0

    .line 4580
    :cond_f
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 4239
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4543
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4544
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", alphanumeric_coupons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->alphanumeric_coupons:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4545
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", can_use_cash_integration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_use_cash_integration:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4546
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", see_enrollment_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->see_enrollment_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4547
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", expire_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->expire_points:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4548
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", show_loyalty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4549
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", loyalty_sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4550
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", x2_post_transaction_dark_theme="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4551
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", x2_front_of_transaction_checkin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4552
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", use_coupon_item_selection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->use_coupon_item_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4553
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", delete_will_send_sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->delete_will_send_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4554
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", should_loyalty_handle_returns="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4555
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", can_support_negative_balances="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->can_support_negative_balances:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4556
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", show_loyalty_value_metrics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4557
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", x2_front_of_transaction_checkin_seller_override="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4558
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", loyalty_enrollment_workflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Loyalty{"

    .line 4559
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
