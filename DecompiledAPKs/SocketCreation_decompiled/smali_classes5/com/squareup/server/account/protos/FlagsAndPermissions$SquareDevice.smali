.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SquareDevice"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$ProtoAdapter_SquareDevice;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAN_CART_SCROLL_LOGGING:Ljava/lang/Boolean;

.field public static final DEFAULT_BRAN_DISPLAY_API_X2:Ljava/lang/Boolean;

.field public static final DEFAULT_BRAN_DISPLAY_CART_MONITOR_WORKFLOW_X2:Ljava/lang/Boolean;

.field public static final DEFAULT_BRAN_MULTIPLE_IMAGES:Ljava/lang/Boolean;

.field public static final DEFAULT_CHECK_PTS_COMPLIANCE:Ljava/lang/Boolean;

.field public static final DEFAULT_DIAGNOSTIC_DATA_REPORTER_2_FINGER_BUG_REPORTS:Ljava/lang/Boolean;

.field public static final DEFAULT_JUMBOTRON_SERVICE_KEY_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_PRINTER_DITHERING_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_SPE_FWUP_CRQ:Ljava/lang/Boolean;

.field public static final DEFAULT_SPE_FWUP_WITHOUT_MATCHING_TMS:Ljava/lang/Boolean;

.field public static final DEFAULT_SPE_TMS_LOGIN_CHECK:Ljava/lang/Boolean;

.field public static final DEFAULT_SPM:Ljava/lang/Boolean;

.field public static final DEFAULT_SPM_ES:Ljava/lang/Boolean;

.field public static final DEFAULT_STATUS_BAR_FULLSCREEN_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ACCESSIBLE_PIN_TUTORIAL:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_BUYER_DISPLAY_SETTINGS_V2_X2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_FILE_SIZE_ANALYTICS:Ljava/lang/Boolean;

.field public static final DEFAULT_X2_USE_PAYMENT_WORKFLOWS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final bran_cart_scroll_logging:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x19
    .end annotation
.end field

.field public final bran_display_api_x2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final bran_multiple_images:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final check_pts_compliance:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final jumbotron_service_key_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final printer_dithering_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final spe_fwup_crq:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final spe_fwup_without_matching_tms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final spe_tms_login_check:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final spm:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final spm_es:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final status_bar_fullscreen_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final use_accessible_pin_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final use_file_size_analytics:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x18
    .end annotation
.end field

.field public final x2_use_payment_workflows:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15140
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$ProtoAdapter_SquareDevice;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$ProtoAdapter_SquareDevice;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 15142
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 15146
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_PRINTER_DITHERING_T2:Ljava/lang/Boolean;

    .line 15148
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_STATUS_BAR_FULLSCREEN_T2:Ljava/lang/Boolean;

    .line 15150
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPE_FWUP_WITHOUT_MATCHING_TMS:Ljava/lang/Boolean;

    .line 15152
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_JUMBOTRON_SERVICE_KEY_T2:Ljava/lang/Boolean;

    .line 15154
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_BRAN_DISPLAY_API_X2:Ljava/lang/Boolean;

    .line 15156
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPE_FWUP_CRQ:Ljava/lang/Boolean;

    .line 15158
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_BRAN_DISPLAY_CART_MONITOR_WORKFLOW_X2:Ljava/lang/Boolean;

    .line 15160
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPM:Ljava/lang/Boolean;

    .line 15162
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_CHECK_PTS_COMPLIANCE:Ljava/lang/Boolean;

    .line 15164
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPM_ES:Ljava/lang/Boolean;

    .line 15166
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPE_TMS_LOGIN_CHECK:Ljava/lang/Boolean;

    .line 15168
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_BRAN_MULTIPLE_IMAGES:Ljava/lang/Boolean;

    .line 15170
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT:Ljava/lang/Boolean;

    .line 15172
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_USE_BUYER_DISPLAY_SETTINGS_V2_X2:Ljava/lang/Boolean;

    .line 15174
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_DIAGNOSTIC_DATA_REPORTER_2_FINGER_BUG_REPORTS:Ljava/lang/Boolean;

    .line 15176
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_USE_ACCESSIBLE_PIN_TUTORIAL:Ljava/lang/Boolean;

    .line 15178
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH:Ljava/lang/Boolean;

    .line 15180
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_X2_USE_PAYMENT_WORKFLOWS:Ljava/lang/Boolean;

    .line 15182
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_USE_FILE_SIZE_ANALYTICS:Ljava/lang/Boolean;

    .line 15184
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_BRAN_CART_SCROLL_LOGGING:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;Lokio/ByteString;)V
    .locals 1

    .line 15387
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 15388
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->printer_dithering_t2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    .line 15389
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    .line 15390
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    .line 15391
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    .line 15392
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_api_x2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    .line 15393
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_crq:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    .line 15394
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    .line 15395
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    .line 15396
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->check_pts_compliance:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    .line 15397
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm_es:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    .line 15398
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_tms_login_check:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    .line 15399
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_multiple_images:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    .line 15400
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    .line 15401
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    .line 15402
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    .line 15403
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    .line 15404
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    .line 15405
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->x2_use_payment_workflows:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    .line 15406
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_file_size_analytics:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    .line 15407
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 15574
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 15440
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 15441
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    .line 15442
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    .line 15443
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    .line 15444
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    .line 15445
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    .line 15446
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    .line 15447
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    .line 15448
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    .line 15449
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    .line 15450
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    .line 15451
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    .line 15452
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    .line 15453
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    .line 15454
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    .line 15455
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    .line 15456
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    .line 15457
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    .line 15458
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    .line 15459
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    .line 15460
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    .line 15461
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    .line 15462
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 15467
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_14

    .line 15469
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 15470
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15471
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15472
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15473
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15474
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15475
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15476
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15477
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15478
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15479
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15480
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15481
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15482
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15483
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15484
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15485
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15486
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15487
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15488
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15489
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_13
    add-int/2addr v0, v2

    .line 15490
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_14
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 2

    .line 15412
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;-><init>()V

    .line 15413
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->printer_dithering_t2:Ljava/lang/Boolean;

    .line 15414
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    .line 15415
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    .line 15416
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    .line 15417
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_api_x2:Ljava/lang/Boolean;

    .line 15418
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_crq:Ljava/lang/Boolean;

    .line 15419
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    .line 15420
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm:Ljava/lang/Boolean;

    .line 15421
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->check_pts_compliance:Ljava/lang/Boolean;

    .line 15422
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm_es:Ljava/lang/Boolean;

    .line 15423
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_tms_login_check:Ljava/lang/Boolean;

    .line 15424
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_multiple_images:Ljava/lang/Boolean;

    .line 15425
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    .line 15426
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    .line 15427
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    .line 15428
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    .line 15429
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    .line 15430
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->x2_use_payment_workflows:Ljava/lang/Boolean;

    .line 15431
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_file_size_analytics:Ljava/lang/Boolean;

    .line 15432
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    .line 15433
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 15139
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;
    .locals 2

    .line 15550
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->printer_dithering_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15551
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->status_bar_fullscreen_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15552
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_without_matching_tms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15553
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->jumbotron_service_key_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15554
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_api_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15555
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_crq(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15556
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_cart_monitor_workflow_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15557
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15558
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->check_pts_compliance(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15559
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm_es(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15560
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_tms_login_check(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15561
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_multiple_images(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15562
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_bran_payment_prompt_variations_experiment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15563
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_buyer_display_settings_v2_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15564
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->diagnostic_data_reporter_2_finger_bug_reports(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15565
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_accessible_pin_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15566
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->t2_buyer_checkout_defaults_to_us_english(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15567
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->x2_use_payment_workflows(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15568
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_file_size_analytics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15569
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_cart_scroll_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    :cond_13
    if-nez v1, :cond_14

    move-object p1, p0

    goto :goto_0

    .line 15570
    :cond_14
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 15139
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;
    .locals 2

    .line 15524
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_PRINTER_DITHERING_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->printer_dithering_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15525
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_STATUS_BAR_FULLSCREEN_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->status_bar_fullscreen_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15526
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPE_FWUP_WITHOUT_MATCHING_TMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_without_matching_tms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15527
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_JUMBOTRON_SERVICE_KEY_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->jumbotron_service_key_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15528
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_BRAN_DISPLAY_API_X2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_api_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15529
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPE_FWUP_CRQ:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_crq(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15530
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_BRAN_DISPLAY_CART_MONITOR_WORKFLOW_X2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_cart_monitor_workflow_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15531
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPM:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15532
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_CHECK_PTS_COMPLIANCE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->check_pts_compliance(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15533
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPM_ES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm_es(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15534
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_SPE_TMS_LOGIN_CHECK:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_tms_login_check(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15535
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_BRAN_MULTIPLE_IMAGES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_multiple_images(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15536
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_USE_BRAN_PAYMENT_PROMPT_VARIATIONS_EXPERIMENT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_bran_payment_prompt_variations_experiment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15537
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_USE_BUYER_DISPLAY_SETTINGS_V2_X2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_buyer_display_settings_v2_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15538
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_DIAGNOSTIC_DATA_REPORTER_2_FINGER_BUG_REPORTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->diagnostic_data_reporter_2_finger_bug_reports(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15539
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_USE_ACCESSIBLE_PIN_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_accessible_pin_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15540
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->t2_buyer_checkout_defaults_to_us_english(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15541
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_X2_USE_PAYMENT_WORKFLOWS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->x2_use_payment_workflows(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15542
    :cond_11
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    if-nez v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_USE_FILE_SIZE_ANALYTICS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_file_size_analytics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    .line 15543
    :cond_12
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->DEFAULT_BRAN_CART_SCROLL_LOGGING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_cart_scroll_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;

    move-result-object v1

    :cond_13
    if-nez v1, :cond_14

    move-object v0, p0

    goto :goto_0

    .line 15544
    :cond_14
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 15139
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 15497
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15498
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", printer_dithering_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->printer_dithering_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15499
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", status_bar_fullscreen_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15500
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", spe_fwup_without_matching_tms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15501
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", jumbotron_service_key_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15502
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", bran_display_api_x2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_api_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15503
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", spe_fwup_crq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_fwup_crq:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15504
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", bran_display_cart_monitor_workflow_x2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15505
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", spm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15506
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", check_pts_compliance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->check_pts_compliance:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15507
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", spm_es="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spm_es:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15508
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", spe_tms_login_check="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->spe_tms_login_check:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15509
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", bran_multiple_images="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_multiple_images:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15510
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", use_bran_payment_prompt_variations_experiment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15511
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", use_buyer_display_settings_v2_x2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15512
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", diagnostic_data_reporter_2_finger_bug_reports="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15513
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", use_accessible_pin_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15514
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", t2_buyer_checkout_defaults_to_us_english="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15515
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", x2_use_payment_workflows="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->x2_use_payment_workflows:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15516
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", use_file_size_analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->use_file_size_analytics:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 15517
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", bran_cart_scroll_logging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_13
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SquareDevice{"

    .line 15518
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
