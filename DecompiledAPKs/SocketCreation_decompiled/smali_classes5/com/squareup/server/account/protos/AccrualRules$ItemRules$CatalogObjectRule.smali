.class public final Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;
.super Lcom/squareup/wire/AndroidMessage;
.source "AccrualRules.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules$ItemRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CatalogObjectRule"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$ProtoAdapter_CatalogObjectRule;,
        Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_POINTS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.AccrualRules$CatalogObject#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final points:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 974
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$ProtoAdapter_CatalogObjectRule;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$ProtoAdapter_CatalogObjectRule;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 976
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->CREATOR:Landroid/os/Parcelable$Creator;

    const-wide/16 v0, 0x0

    .line 980
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->DEFAULT_POINTS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;)V
    .locals 1

    .line 1003
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;-><init>(Ljava/lang/Long;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;Lokio/ByteString;)V
    .locals 1

    .line 1008
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1009
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    .line 1010
    iput-object p2, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 1072
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1025
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1026
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    .line 1027
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    .line 1028
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    .line 1029
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1034
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 1036
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1037
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1038
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1039
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;
    .locals 2

    .line 1015
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;-><init>()V

    .line 1016
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->points:Ljava/lang/Long;

    .line 1017
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    iput-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    .line 1018
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 973
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;
    .locals 2

    .line 1066
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v1

    .line 1067
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->catalog_object(Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 1068
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 973
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->overlay(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;
    .locals 3

    .line 1055
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->DEFAULT_POINTS:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v1

    .line 1056
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    if-eqz v0, :cond_1

    .line 1057
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    move-result-object v0

    .line 1058
    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->requireBuilder(Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->catalog_object(Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 1060
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 973
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->populateDefaults()Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1046
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1047
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1048
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    if-eqz v1, :cond_1

    const-string v1, ", catalog_object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CatalogObjectRule{"

    .line 1049
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
