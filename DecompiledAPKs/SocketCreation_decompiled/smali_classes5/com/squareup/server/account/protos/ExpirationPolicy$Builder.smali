.class public final Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ExpirationPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ExpirationPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/ExpirationPolicy;",
        "Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

.field public type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 137
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/ExpirationPolicy;
    .locals 4

    .line 158
    new-instance v0, Lcom/squareup/server/account/protos/ExpirationPolicy;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    iget-object v2, p0, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/ExpirationPolicy;-><init>(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 132
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->build()Lcom/squareup/server/account/protos/ExpirationPolicy;

    move-result-object v0

    return-object v0
.end method

.method public expiration_period(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    return-object p0
.end method

.method public type(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    return-object p0
.end method
