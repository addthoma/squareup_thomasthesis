.class public final Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StoreAndForwardUserCredential.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;",
        "Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public signed_logged_in_user:Ljava/lang/String;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;
    .locals 4

    .line 143
    new-instance v0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    iget-object v1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->version:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->signed_logged_in_user:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;-><init>(Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->build()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    move-result-object v0

    return-object v0
.end method

.method public signed_logged_in_user(Ljava/lang/String;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->signed_logged_in_user:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/server/account/protos/StoreAndForwardUserCredential$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
