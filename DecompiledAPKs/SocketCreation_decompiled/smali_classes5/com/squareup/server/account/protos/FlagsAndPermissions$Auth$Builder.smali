.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_attempt_device_id_updates:Ljava/lang/Boolean;

.field public client_validate_session_on_start:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19666
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;
    .locals 4

    .line 19681
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;->client_validate_session_on_start:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;->client_attempt_device_id_updates:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 19661
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth;

    move-result-object v0

    return-object v0
.end method

.method public client_attempt_device_id_updates(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;
    .locals 0

    .line 19675
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;->client_attempt_device_id_updates:Ljava/lang/Boolean;

    return-object p0
.end method

.method public client_validate_session_on_start(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;
    .locals 0

    .line 19670
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Auth$Builder;->client_validate_session_on_start:Ljava/lang/Boolean;

    return-object p0
.end method
