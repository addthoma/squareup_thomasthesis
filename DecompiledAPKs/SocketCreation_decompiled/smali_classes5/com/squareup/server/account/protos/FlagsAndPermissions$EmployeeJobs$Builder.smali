.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_use_multiple_wages:Ljava/lang/Boolean;

.field public can_use_multiple_wages_beta:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15991
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;
    .locals 4

    .line 16006
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;->can_use_multiple_wages:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;->can_use_multiple_wages_beta:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 15986
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs;

    move-result-object v0

    return-object v0
.end method

.method public can_use_multiple_wages(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;
    .locals 0

    .line 15995
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;->can_use_multiple_wages:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_multiple_wages_beta(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;
    .locals 0

    .line 16000
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$EmployeeJobs$Builder;->can_use_multiple_wages_beta:Ljava/lang/Boolean;

    return-object p0
.end method
