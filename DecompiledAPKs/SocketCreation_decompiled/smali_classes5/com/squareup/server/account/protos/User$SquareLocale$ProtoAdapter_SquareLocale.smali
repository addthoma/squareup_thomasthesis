.class final Lcom/squareup/server/account/protos/User$SquareLocale$ProtoAdapter_SquareLocale;
.super Lcom/squareup/wire/ProtoAdapter;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User$SquareLocale;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SquareLocale"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/User$SquareLocale;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 650
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/User$SquareLocale;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/User$SquareLocale;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 671
    new-instance v0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;-><init>()V

    .line 672
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 673
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 686
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 679
    :cond_0
    :try_start_0
    iget-object v4, v0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->currency_codes:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/common/CurrencyCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 681
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 676
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    goto :goto_0

    .line 675
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code_guess(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    goto :goto_0

    .line 690
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 691
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->build()Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 648
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$SquareLocale$ProtoAdapter_SquareLocale;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/User$SquareLocale;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 663
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 664
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 665
    sget-object v0, Lcom/squareup/protos/common/CurrencyCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 666
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/User$SquareLocale;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 648
    check-cast p2, Lcom/squareup/server/account/protos/User$SquareLocale;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/User$SquareLocale$ProtoAdapter_SquareLocale;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/User$SquareLocale;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/User$SquareLocale;)I
    .locals 4

    .line 655
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    const/4 v3, 0x2

    .line 656
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 657
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 658
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$SquareLocale;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 648
    check-cast p1, Lcom/squareup/server/account/protos/User$SquareLocale;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$SquareLocale$ProtoAdapter_SquareLocale;->encodedSize(Lcom/squareup/server/account/protos/User$SquareLocale;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/User$SquareLocale;)Lcom/squareup/server/account/protos/User$SquareLocale;
    .locals 0

    .line 696
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$SquareLocale;->newBuilder()Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object p1

    .line 697
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 698
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->build()Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 648
    check-cast p1, Lcom/squareup/server/account/protos/User$SquareLocale;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$SquareLocale$ProtoAdapter_SquareLocale;->redact(Lcom/squareup/server/account/protos/User$SquareLocale;)Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object p1

    return-object p1
.end method
