.class public final Lcom/squareup/server/account/protos/RewardTier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RewardTier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/RewardTier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/RewardTier;",
        "Lcom/squareup/server/account/protos/RewardTier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public coupon_definition_token:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public points:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 156
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/RewardTier;
    .locals 5

    .line 185
    iget-object v0, p0, Lcom/squareup/server/account/protos/RewardTier$Builder;->points:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 188
    new-instance v1, Lcom/squareup/server/account/protos/RewardTier;

    iget-object v2, p0, Lcom/squareup/server/account/protos/RewardTier$Builder;->coupon_definition_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/server/account/protos/RewardTier$Builder;->name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/squareup/server/account/protos/RewardTier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v1

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    const-string v2, "points"

    aput-object v2, v1, v0

    .line 186
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 149
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/RewardTier$Builder;->build()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v0

    return-object v0
.end method

.method public coupon_definition_token(Ljava/lang/String;)Lcom/squareup/server/account/protos/RewardTier$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/server/account/protos/RewardTier$Builder;->coupon_definition_token:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/server/account/protos/RewardTier$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/server/account/protos/RewardTier$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public points(Ljava/lang/Long;)Lcom/squareup/server/account/protos/RewardTier$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/server/account/protos/RewardTier$Builder;->points:Ljava/lang/Long;

    return-object p0
.end method
