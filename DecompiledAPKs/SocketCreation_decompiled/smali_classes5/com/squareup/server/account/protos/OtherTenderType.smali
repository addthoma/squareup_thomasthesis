.class public final Lcom/squareup/server/account/protos/OtherTenderType;
.super Lcom/squareup/wire/AndroidMessage;
.source "OtherTenderType.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/OtherTenderType$ProtoAdapter_OtherTenderType;,
        Lcom/squareup/server/account/protos/OtherTenderType$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/OtherTenderType;",
        "Lcom/squareup/server/account/protos/OtherTenderType$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/OtherTenderType;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/OtherTenderType;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCEPTS_TIPS:Ljava/lang/Boolean;

.field public static final DEFAULT_TENDER_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDER_NOTE_PROMPT:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDER_OPENS_CASH_DRAWER:Ljava/lang/Boolean;

.field public static final DEFAULT_TENDER_TYPE:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final accepts_tips:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final tender_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final tender_note_prompt:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final tender_opens_cash_drawer:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final tender_type:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/server/account/protos/OtherTenderType$ProtoAdapter_OtherTenderType;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/OtherTenderType$ProtoAdapter_OtherTenderType;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 34
    sget-object v0, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/OtherTenderType;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 38
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/OtherTenderType;->DEFAULT_TENDER_TYPE:Ljava/lang/Integer;

    .line 44
    sput-object v1, Lcom/squareup/server/account/protos/OtherTenderType;->DEFAULT_TENDER_OPENS_CASH_DRAWER:Ljava/lang/Boolean;

    .line 46
    sput-object v1, Lcom/squareup/server/account/protos/OtherTenderType;->DEFAULT_ACCEPTS_TIPS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 7

    .line 89
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/server/account/protos/OtherTenderType;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/server/account/protos/OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 96
    iput-object p1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    .line 97
    iput-object p2, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    .line 98
    iput-object p3, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    .line 99
    iput-object p4, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    .line 100
    iput-object p5, p0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OtherTenderType;->newBuilder()Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 118
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/OtherTenderType;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 119
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/OtherTenderType;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OtherTenderType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/OtherTenderType;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    .line 124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    .line 125
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 130
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_5

    .line 132
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OtherTenderType;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 138
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/OtherTenderType$Builder;
    .locals 2

    .line 105
    new-instance v0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;-><init>()V

    .line 106
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_type:Ljava/lang/Integer;

    .line 107
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_name:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_note_prompt:Ljava/lang/String;

    .line 109
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    .line 110
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->accepts_tips:Ljava/lang/Boolean;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OtherTenderType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OtherTenderType;->newBuilder()Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 2

    .line 166
    iget-object v0, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OtherTenderType;->requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_type(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v1

    .line 167
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OtherTenderType;->requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_name(Ljava/lang/String;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v1

    .line 168
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OtherTenderType;->requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_note_prompt(Ljava/lang/String;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v1

    .line 169
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OtherTenderType;->requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_opens_cash_drawer(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v1

    .line 170
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OtherTenderType;->requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->accepts_tips(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v1

    :cond_4
    if-nez v1, :cond_5

    move-object p1, p0

    goto :goto_0

    .line 171
    :cond_5
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->build()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/server/account/protos/OtherTenderType;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/OtherTenderType;->overlay(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OtherTenderType;->requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/OtherTenderType;->DEFAULT_TENDER_TYPE:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_type(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v1

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OtherTenderType;->requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/OtherTenderType;->DEFAULT_TENDER_OPENS_CASH_DRAWER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_opens_cash_drawer(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v1

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/OtherTenderType;->requireBuilder(Lcom/squareup/server/account/protos/OtherTenderType$Builder;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/OtherTenderType;->DEFAULT_ACCEPTS_TIPS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->accepts_tips(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object v0, p0

    goto :goto_0

    .line 160
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->build()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OtherTenderType;->populateDefaults()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", tender_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", tender_note_prompt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_note_prompt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", tender_opens_cash_drawer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_opens_cash_drawer:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", accepts_tips="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OtherTenderType{"

    .line 151
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
