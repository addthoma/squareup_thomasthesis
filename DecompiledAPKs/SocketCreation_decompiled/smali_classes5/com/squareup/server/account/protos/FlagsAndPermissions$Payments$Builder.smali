.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public quick_amounts_v2:Ljava/lang/Boolean;

.field public request_business_address:Ljava/lang/Boolean;

.field public request_business_address_badge:Ljava/lang/Boolean;

.field public request_business_address_pobox_validation:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18617
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;
    .locals 7

    .line 18655
    new-instance v6, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_badge:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->quick_amounts_v2:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 18608
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments;

    move-result-object v0

    return-object v0
.end method

.method public quick_amounts_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;
    .locals 0

    .line 18649
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->quick_amounts_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public request_business_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;
    .locals 0

    .line 18624
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address:Ljava/lang/Boolean;

    return-object p0
.end method

.method public request_business_address_badge(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;
    .locals 0

    .line 18632
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_badge:Ljava/lang/Boolean;

    return-object p0
.end method

.method public request_business_address_pobox_validation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;
    .locals 0

    .line 18641
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Payments$Builder;->request_business_address_pobox_validation:Ljava/lang/Boolean;

    return-object p0
.end method
