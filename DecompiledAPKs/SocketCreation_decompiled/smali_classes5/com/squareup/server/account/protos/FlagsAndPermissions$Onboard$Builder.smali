.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ask_intent_questions:Ljava/lang/Boolean;

.field public bank_link:Ljava/lang/Boolean;

.field public choose_default_deposit_method:Ljava/lang/Boolean;

.field public offer_free_reader:Ljava/lang/Boolean;

.field public referral:Ljava/lang/Boolean;

.field public suppress_first_tutorial:Ljava/lang/Boolean;

.field public use_server_driven_flow:Ljava/lang/Boolean;

.field public validate_shipping_address:Ljava/lang/Boolean;

.field public x2_vertical_selection:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public ask_intent_questions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5131
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->ask_intent_questions:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bank_link(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5155
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->bank_link:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;
    .locals 12

    .line 5193
    new-instance v11, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->validate_shipping_address:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->ask_intent_questions:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->choose_default_deposit_method:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->offer_free_reader:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->bank_link:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->referral:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->suppress_first_tutorial:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->use_server_driven_flow:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->x2_vertical_selection:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5097
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard;

    move-result-object v0

    return-object v0
.end method

.method public choose_default_deposit_method(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5139
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->choose_default_deposit_method:Ljava/lang/Boolean;

    return-object p0
.end method

.method public offer_free_reader(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5147
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->offer_free_reader:Ljava/lang/Boolean;

    return-object p0
.end method

.method public referral(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5163
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->referral:Ljava/lang/Boolean;

    return-object p0
.end method

.method public suppress_first_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5171
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->suppress_first_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_server_driven_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5179
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->use_server_driven_flow:Ljava/lang/Boolean;

    return-object p0
.end method

.method public validate_shipping_address(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5123
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->validate_shipping_address:Ljava/lang/Boolean;

    return-object p0
.end method

.method public x2_vertical_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;
    .locals 0

    .line 5187
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Onboard$Builder;->x2_vertical_selection:Ljava/lang/Boolean;

    return-object p0
.end method
