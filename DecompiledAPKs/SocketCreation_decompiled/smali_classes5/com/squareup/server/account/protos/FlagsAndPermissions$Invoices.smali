.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Invoices"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$ProtoAdapter_Invoices;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_ARCHIVE:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOW_EDIT_FLOW_V2_IN_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOW_PDF_ATTACHMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_AUTOMATIC_PAYMENT_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_AUTOMATIC_REMINDERS_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_CREATION_FLOW_V2_WIDGETS:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_INVOICE_LINKING_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOM_SEND_REMINDER_MESSAGE_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_DEFAULT_MESSAGE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_DOWNLOAD_INVOICE:Ljava/lang/Boolean;

.field public static final DEFAULT_EDIT_ESTIMATE_FLOW_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_EDIT_FLOW_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_ESTIMATES_FILE_ATTACHMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_ESTIMATES_MOBILE_ALPHA:Ljava/lang/Boolean;

.field public static final DEFAULT_ESTIMATES_MOBILE_GA:Ljava/lang/Boolean;

.field public static final DEFAULT_ESTIMATES_PACKAGE_SUPPORT_MOBILE_READONLY:Ljava/lang/Boolean;

.field public static final DEFAULT_ESTIMATE_DEFAULTS_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_FILE_ATTACHMENT_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_FIRST_INVOICE_TUTORIAL_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_HOME_TAB:Ljava/lang/Boolean;

.field public static final DEFAULT_INVOICE_APP_BANNER:Ljava/lang/Boolean;

.field public static final DEFAULT_INVOICE_COF_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_INVOICE_EVENT_HISTORY_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_INVOICE_PREVIEW_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_INVOICE_SHARE_LINK_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_IPOS_ONBOARDING_MERCHANT_CUSTOMIZATION:Ljava/lang/Boolean;

.field public static final DEFAULT_IPOS_RATING_PROMPT:Ljava/lang/Boolean;

.field public static final DEFAULT_MOBILE_ANALYTICS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_NO_IDV_EXPERIMENT_ROLLOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_PARTIALLY_PAY_INVOICE:Ljava/lang/Boolean;

.field public static final DEFAULT_PAYMENT_REQUEST_EDITING:Ljava/lang/Boolean;

.field public static final DEFAULT_PAYMENT_SCHEDULE:Ljava/lang/Boolean;

.field public static final DEFAULT_PAYMENT_SCHEDULE_REMINDERS:Ljava/lang/Boolean;

.field public static final DEFAULT_RECORD_PAYMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_RECURRING_CANCEL_NEXT:Ljava/lang/Boolean;

.field public static final DEFAULT_RECURRING_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_REDUCE_STATE_FILTERS_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_SHIPPING_ADDRESS_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_TIMELINE_CTA_LINKING:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_INVOICE_DEFAULTS_MOBILE:Ljava/lang/Boolean;

.field public static final DEFAULT_VISIBLE_PUSH:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_archive:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1d
    .end annotation
.end field

.field public final allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x28
    .end annotation
.end field

.field public final allow_pdf_attachments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x26
    .end annotation
.end field

.field public final automatic_payment_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final automatic_reminders_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final creation_flow_v2_widgets:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x24
    .end annotation
.end field

.field public final custom_send_reminder_message_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final customer_invoice_linking_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final default_message_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final download_invoice:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1e
    .end annotation
.end field

.field public final edit_estimate_flow_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x27
    .end annotation
.end field

.field public final edit_flow_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1c
    .end annotation
.end field

.field public final estimate_defaults_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1b
    .end annotation
.end field

.field public final estimates_file_attachments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final estimates_mobile_alpha:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final estimates_mobile_ga:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final estimates_package_support_mobile_readonly:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x29
    .end annotation
.end field

.field public final file_attachment_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final first_invoice_tutorial_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final home_tab:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1f
    .end annotation
.end field

.field public final invoice_app_banner:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1a
    .end annotation
.end field

.field public final invoice_cof_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final invoice_event_history_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final invoice_preview_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final invoice_share_link_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final ipos_onboarding_merchant_customization:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final ipos_rating_prompt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x21
    .end annotation
.end field

.field public final mobile_analytics_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final no_idv_experiment_rollout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x23
    .end annotation
.end field

.field public final partially_pay_invoice:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x22
    .end annotation
.end field

.field public final payment_request_editing:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final payment_schedule:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x20
    .end annotation
.end field

.field public final payment_schedule_reminders:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x25
    .end annotation
.end field

.field public final record_payment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final recurring_cancel_next:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final recurring_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final reduce_state_filters_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final shipping_address_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final timeline_cta_linking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x18
    .end annotation
.end field

.field public final use_invoice_defaults_mobile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final visible_push:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x19
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7727
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$ProtoAdapter_Invoices;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$ProtoAdapter_Invoices;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7729
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 7733
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_PREVIEW_ANDROID:Ljava/lang/Boolean;

    .line 7735
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_SHARE_LINK_ANDROID:Ljava/lang/Boolean;

    .line 7737
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_DEFAULT_MESSAGE_ANDROID:Ljava/lang/Boolean;

    .line 7739
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_EVENT_HISTORY_ANDROID:Ljava/lang/Boolean;

    .line 7741
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_FIRST_INVOICE_TUTORIAL_ANDROID:Ljava/lang/Boolean;

    .line 7743
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_FILE_ATTACHMENT_ANDROID:Ljava/lang/Boolean;

    .line 7745
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_MOBILE_ANALYTICS_ANDROID:Ljava/lang/Boolean;

    .line 7747
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_RECURRING_MOBILE:Ljava/lang/Boolean;

    .line 7749
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_SHIPPING_ADDRESS_MOBILE:Ljava/lang/Boolean;

    .line 7751
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_CUSTOMER_INVOICE_LINKING_MOBILE:Ljava/lang/Boolean;

    .line 7753
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_CUSTOM_SEND_REMINDER_MESSAGE_MOBILE:Ljava/lang/Boolean;

    .line 7755
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_COF_MOBILE:Ljava/lang/Boolean;

    .line 7757
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_REDUCE_STATE_FILTERS_MOBILE:Ljava/lang/Boolean;

    .line 7759
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_AUTOMATIC_PAYMENT_MOBILE:Ljava/lang/Boolean;

    .line 7761
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_AUTOMATIC_REMINDERS_MOBILE:Ljava/lang/Boolean;

    .line 7763
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_RECURRING_CANCEL_NEXT:Ljava/lang/Boolean;

    .line 7765
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_IPOS_ONBOARDING_MERCHANT_CUSTOMIZATION:Ljava/lang/Boolean;

    .line 7767
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_USE_INVOICE_DEFAULTS_MOBILE:Ljava/lang/Boolean;

    .line 7769
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_PAYMENT_REQUEST_EDITING:Ljava/lang/Boolean;

    .line 7771
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_RECORD_PAYMENT:Ljava/lang/Boolean;

    .line 7773
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATES_MOBILE_ALPHA:Ljava/lang/Boolean;

    .line 7775
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATES_MOBILE_GA:Ljava/lang/Boolean;

    .line 7777
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATES_FILE_ATTACHMENTS:Ljava/lang/Boolean;

    .line 7779
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_TIMELINE_CTA_LINKING:Ljava/lang/Boolean;

    .line 7781
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_VISIBLE_PUSH:Ljava/lang/Boolean;

    .line 7783
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_APP_BANNER:Ljava/lang/Boolean;

    .line 7785
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATE_DEFAULTS_MOBILE:Ljava/lang/Boolean;

    .line 7787
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_EDIT_FLOW_V2:Ljava/lang/Boolean;

    .line 7789
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ALLOW_ARCHIVE:Ljava/lang/Boolean;

    .line 7791
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_DOWNLOAD_INVOICE:Ljava/lang/Boolean;

    .line 7793
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_HOME_TAB:Ljava/lang/Boolean;

    .line 7795
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_PAYMENT_SCHEDULE:Ljava/lang/Boolean;

    .line 7797
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_IPOS_RATING_PROMPT:Ljava/lang/Boolean;

    .line 7799
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_PARTIALLY_PAY_INVOICE:Ljava/lang/Boolean;

    .line 7801
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_NO_IDV_EXPERIMENT_ROLLOUT:Ljava/lang/Boolean;

    .line 7803
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_CREATION_FLOW_V2_WIDGETS:Ljava/lang/Boolean;

    .line 7805
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_PAYMENT_SCHEDULE_REMINDERS:Ljava/lang/Boolean;

    .line 7807
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ALLOW_PDF_ATTACHMENTS:Ljava/lang/Boolean;

    .line 7809
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_EDIT_ESTIMATE_FLOW_V2:Ljava/lang/Boolean;

    .line 7811
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ALLOW_EDIT_FLOW_V2_IN_CHECKOUT:Ljava/lang/Boolean;

    .line 7813
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATES_PACKAGE_SUPPORT_MOBILE_READONLY:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;Lokio/ByteString;)V
    .locals 1

    .line 8220
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 8221
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_preview_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    .line 8222
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_share_link_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    .line 8223
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->default_message_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    .line 8224
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_event_history_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    .line 8225
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    .line 8226
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->file_attachment_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    .line 8227
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->mobile_analytics_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    .line 8228
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    .line 8229
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->shipping_address_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    .line 8230
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    .line 8231
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    .line 8232
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_cof_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    .line 8233
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    .line 8234
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_payment_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    .line 8235
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_reminders_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    .line 8236
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_cancel_next:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    .line 8237
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    .line 8238
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    .line 8239
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_request_editing:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    .line 8240
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->record_payment:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    .line 8241
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_alpha:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    .line 8242
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_ga:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    .line 8243
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_file_attachments:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    .line 8244
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->timeline_cta_linking:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    .line 8245
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->visible_push:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    .line 8246
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_app_banner:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    .line 8247
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimate_defaults_mobile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    .line 8248
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_flow_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    .line 8249
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_archive:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    .line 8250
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->download_invoice:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    .line 8251
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->home_tab:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    .line 8252
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    .line 8253
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_rating_prompt:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    .line 8254
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->partially_pay_invoice:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    .line 8255
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    .line 8256
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    .line 8257
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule_reminders:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    .line 8258
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_pdf_attachments:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    .line 8259
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    .line 8260
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    .line 8261
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 8554
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8315
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8316
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    .line 8317
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    .line 8318
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    .line 8319
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    .line 8320
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    .line 8321
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    .line 8322
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    .line 8323
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    .line 8324
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    .line 8325
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    .line 8326
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    .line 8327
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    .line 8328
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    .line 8329
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    .line 8330
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    .line 8331
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    .line 8332
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    .line 8333
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    .line 8334
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    .line 8335
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    .line 8336
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    .line 8337
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    .line 8338
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    .line 8339
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    .line 8340
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    .line 8341
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    .line 8342
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    .line 8343
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    .line 8344
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    .line 8345
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    .line 8346
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    .line 8347
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    .line 8348
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    .line 8349
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    .line 8350
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    .line 8351
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    .line 8352
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    .line 8353
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    .line 8354
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    .line 8355
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    .line 8356
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    .line 8357
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    .line 8358
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8363
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_29

    .line 8365
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8366
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8367
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8368
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8369
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8370
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8371
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8372
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8373
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8374
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8375
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8376
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8377
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8378
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8379
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8380
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8381
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8382
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8383
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8384
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8385
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8386
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8387
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8388
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8389
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8390
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8391
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8392
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8393
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8394
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8395
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8396
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8397
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8398
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8399
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8400
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8401
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8402
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8403
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_25

    :cond_25
    const/4 v1, 0x0

    :goto_25
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8404
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_26

    :cond_26
    const/4 v1, 0x0

    :goto_26
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8405
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_27

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_27

    :cond_27
    const/4 v1, 0x0

    :goto_27
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8406
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_28
    add-int/2addr v0, v2

    .line 8407
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_29
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 2

    .line 8266
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;-><init>()V

    .line 8267
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_preview_android:Ljava/lang/Boolean;

    .line 8268
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_share_link_android:Ljava/lang/Boolean;

    .line 8269
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->default_message_android:Ljava/lang/Boolean;

    .line 8270
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_event_history_android:Ljava/lang/Boolean;

    .line 8271
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    .line 8272
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->file_attachment_android:Ljava/lang/Boolean;

    .line 8273
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->mobile_analytics_android:Ljava/lang/Boolean;

    .line 8274
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_mobile:Ljava/lang/Boolean;

    .line 8275
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->shipping_address_mobile:Ljava/lang/Boolean;

    .line 8276
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    .line 8277
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    .line 8278
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_cof_mobile:Ljava/lang/Boolean;

    .line 8279
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    .line 8280
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_payment_mobile:Ljava/lang/Boolean;

    .line 8281
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_reminders_mobile:Ljava/lang/Boolean;

    .line 8282
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_cancel_next:Ljava/lang/Boolean;

    .line 8283
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    .line 8284
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    .line 8285
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_request_editing:Ljava/lang/Boolean;

    .line 8286
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->record_payment:Ljava/lang/Boolean;

    .line 8287
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_alpha:Ljava/lang/Boolean;

    .line 8288
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_ga:Ljava/lang/Boolean;

    .line 8289
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_file_attachments:Ljava/lang/Boolean;

    .line 8290
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->timeline_cta_linking:Ljava/lang/Boolean;

    .line 8291
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->visible_push:Ljava/lang/Boolean;

    .line 8292
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_app_banner:Ljava/lang/Boolean;

    .line 8293
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimate_defaults_mobile:Ljava/lang/Boolean;

    .line 8294
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_flow_v2:Ljava/lang/Boolean;

    .line 8295
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_archive:Ljava/lang/Boolean;

    .line 8296
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->download_invoice:Ljava/lang/Boolean;

    .line 8297
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->home_tab:Ljava/lang/Boolean;

    .line 8298
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule:Ljava/lang/Boolean;

    .line 8299
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_rating_prompt:Ljava/lang/Boolean;

    .line 8300
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->partially_pay_invoice:Ljava/lang/Boolean;

    .line 8301
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    .line 8302
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    .line 8303
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule_reminders:Ljava/lang/Boolean;

    .line 8304
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_pdf_attachments:Ljava/lang/Boolean;

    .line 8305
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    .line 8306
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    .line 8307
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    .line 8308
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7726
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;
    .locals 2

    .line 8509
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_preview_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8510
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_share_link_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8511
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->default_message_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8512
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_event_history_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8513
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->first_invoice_tutorial_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8514
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->file_attachment_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8515
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->mobile_analytics_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8516
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8517
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->shipping_address_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8518
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->customer_invoice_linking_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8519
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->custom_send_reminder_message_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8520
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_cof_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8521
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->reduce_state_filters_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8522
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_payment_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8523
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_reminders_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8524
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_cancel_next(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8525
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_onboarding_merchant_customization(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8526
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->use_invoice_defaults_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8527
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_request_editing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8528
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->record_payment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8529
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_alpha(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8530
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    if-eqz v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_ga(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8531
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    if-eqz v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_file_attachments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8532
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    if-eqz v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->timeline_cta_linking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8533
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->visible_push(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8534
    :cond_18
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    if-eqz v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_app_banner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8535
    :cond_19
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimate_defaults_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8536
    :cond_1a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_flow_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8537
    :cond_1b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_archive(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8538
    :cond_1c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    if-eqz v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->download_invoice(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8539
    :cond_1d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->home_tab(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8540
    :cond_1e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    if-eqz v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8541
    :cond_1f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    if-eqz v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_rating_prompt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8542
    :cond_20
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    if-eqz v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->partially_pay_invoice(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8543
    :cond_21
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    if-eqz v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->no_idv_experiment_rollout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8544
    :cond_22
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    if-eqz v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->creation_flow_v2_widgets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8545
    :cond_23
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    if-eqz v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule_reminders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8546
    :cond_24
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    if-eqz v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_pdf_attachments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8547
    :cond_25
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_26

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_estimate_flow_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8548
    :cond_26
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_27

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_edit_flow_v2_in_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8549
    :cond_27
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    if-eqz v0, :cond_28

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_package_support_mobile_readonly(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    :cond_28
    if-nez v1, :cond_29

    move-object p1, p0

    goto :goto_0

    .line 8550
    :cond_29
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 7726
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;
    .locals 2

    .line 8462
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_PREVIEW_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_preview_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8463
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_SHARE_LINK_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_share_link_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8464
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_DEFAULT_MESSAGE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->default_message_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8465
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_EVENT_HISTORY_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_event_history_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8466
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_FIRST_INVOICE_TUTORIAL_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->first_invoice_tutorial_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8467
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_FILE_ATTACHMENT_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->file_attachment_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8468
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_MOBILE_ANALYTICS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->mobile_analytics_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8469
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_RECURRING_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8470
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_SHIPPING_ADDRESS_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->shipping_address_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8471
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_CUSTOMER_INVOICE_LINKING_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->customer_invoice_linking_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8472
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_CUSTOM_SEND_REMINDER_MESSAGE_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->custom_send_reminder_message_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8473
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_COF_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_cof_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8474
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_REDUCE_STATE_FILTERS_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->reduce_state_filters_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8475
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_AUTOMATIC_PAYMENT_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_payment_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8476
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_AUTOMATIC_REMINDERS_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_reminders_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8477
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_RECURRING_CANCEL_NEXT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_cancel_next(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8478
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_IPOS_ONBOARDING_MERCHANT_CUSTOMIZATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_onboarding_merchant_customization(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8479
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_USE_INVOICE_DEFAULTS_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->use_invoice_defaults_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8480
    :cond_11
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    if-nez v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_PAYMENT_REQUEST_EDITING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_request_editing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8481
    :cond_12
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_RECORD_PAYMENT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->record_payment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8482
    :cond_13
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    if-nez v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATES_MOBILE_ALPHA:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_alpha(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8483
    :cond_14
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    if-nez v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATES_MOBILE_GA:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_ga(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8484
    :cond_15
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    if-nez v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATES_FILE_ATTACHMENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_file_attachments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8485
    :cond_16
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    if-nez v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_TIMELINE_CTA_LINKING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->timeline_cta_linking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8486
    :cond_17
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    if-nez v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_VISIBLE_PUSH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->visible_push(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8487
    :cond_18
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    if-nez v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_INVOICE_APP_BANNER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_app_banner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8488
    :cond_19
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    if-nez v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATE_DEFAULTS_MOBILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimate_defaults_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8489
    :cond_1a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_EDIT_FLOW_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_flow_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8490
    :cond_1b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    if-nez v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ALLOW_ARCHIVE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_archive(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8491
    :cond_1c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    if-nez v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_DOWNLOAD_INVOICE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->download_invoice(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8492
    :cond_1d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    if-nez v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_HOME_TAB:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->home_tab(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8493
    :cond_1e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    if-nez v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_PAYMENT_SCHEDULE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8494
    :cond_1f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    if-nez v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_IPOS_RATING_PROMPT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_rating_prompt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8495
    :cond_20
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    if-nez v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_PARTIALLY_PAY_INVOICE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->partially_pay_invoice(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8496
    :cond_21
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    if-nez v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_NO_IDV_EXPERIMENT_ROLLOUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->no_idv_experiment_rollout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8497
    :cond_22
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    if-nez v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_CREATION_FLOW_V2_WIDGETS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->creation_flow_v2_widgets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8498
    :cond_23
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    if-nez v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_PAYMENT_SCHEDULE_REMINDERS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule_reminders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8499
    :cond_24
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    if-nez v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ALLOW_PDF_ATTACHMENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_pdf_attachments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8500
    :cond_25
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_26

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_EDIT_ESTIMATE_FLOW_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_estimate_flow_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8501
    :cond_26
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    if-nez v0, :cond_27

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ALLOW_EDIT_FLOW_V2_IN_CHECKOUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_edit_flow_v2_in_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    .line 8502
    :cond_27
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    if-nez v0, :cond_28

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->DEFAULT_ESTIMATES_PACKAGE_SUPPORT_MOBILE_READONLY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_package_support_mobile_readonly(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;

    move-result-object v1

    :cond_28
    if-nez v1, :cond_29

    move-object v0, p0

    goto :goto_0

    .line 8503
    :cond_29
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 7726
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8414
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8415
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", invoice_preview_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_preview_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8416
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", invoice_share_link_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_share_link_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8417
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", default_message_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->default_message_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8418
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", invoice_event_history_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_event_history_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8419
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", first_invoice_tutorial_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8420
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", file_attachment_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->file_attachment_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8421
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", mobile_analytics_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->mobile_analytics_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8422
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", recurring_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8423
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", shipping_address_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->shipping_address_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8424
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", customer_invoice_linking_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8425
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", custom_send_reminder_message_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8426
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", invoice_cof_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_cof_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8427
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", reduce_state_filters_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8428
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", automatic_payment_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_payment_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8429
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", automatic_reminders_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->automatic_reminders_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8430
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", recurring_cancel_next="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->recurring_cancel_next:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8431
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", ipos_onboarding_merchant_customization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8432
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", use_invoice_defaults_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8433
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", payment_request_editing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_request_editing:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8434
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", record_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->record_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8435
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", estimates_mobile_alpha="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_alpha:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8436
    :cond_14
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    const-string v1, ", estimates_mobile_ga="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_mobile_ga:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8437
    :cond_15
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    const-string v1, ", estimates_file_attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_file_attachments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8438
    :cond_16
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    const-string v1, ", timeline_cta_linking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->timeline_cta_linking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8439
    :cond_17
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    const-string v1, ", visible_push="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->visible_push:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8440
    :cond_18
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    const-string v1, ", invoice_app_banner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->invoice_app_banner:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8441
    :cond_19
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    const-string v1, ", estimate_defaults_mobile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimate_defaults_mobile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8442
    :cond_1a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    const-string v1, ", edit_flow_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_flow_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8443
    :cond_1b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    const-string v1, ", allow_archive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_archive:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8444
    :cond_1c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    const-string v1, ", download_invoice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->download_invoice:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8445
    :cond_1d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    const-string v1, ", home_tab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->home_tab:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8446
    :cond_1e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    const-string v1, ", payment_schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8447
    :cond_1f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    if-eqz v1, :cond_20

    const-string v1, ", ipos_rating_prompt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->ipos_rating_prompt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8448
    :cond_20
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    const-string v1, ", partially_pay_invoice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->partially_pay_invoice:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8449
    :cond_21
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    const-string v1, ", no_idv_experiment_rollout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8450
    :cond_22
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    const-string v1, ", creation_flow_v2_widgets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8451
    :cond_23
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    const-string v1, ", payment_schedule_reminders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->payment_schedule_reminders:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8452
    :cond_24
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    const-string v1, ", allow_pdf_attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_pdf_attachments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8453
    :cond_25
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    const-string v1, ", edit_estimate_flow_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8454
    :cond_26
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_27

    const-string v1, ", allow_edit_flow_v2_in_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8455
    :cond_27
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    const-string v1, ", estimates_package_support_mobile_readonly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_28
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Invoices{"

    .line 8456
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
