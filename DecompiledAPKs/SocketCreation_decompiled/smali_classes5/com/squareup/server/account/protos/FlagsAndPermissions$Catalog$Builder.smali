.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public display_modifier_instead_of_option:Ljava/lang/Boolean;

.field public duplicate_sku_items_applet:Ljava/lang/Boolean;

.field public enable_item_options:Ljava/lang/Boolean;

.field public enable_item_options_android:Ljava/lang/Boolean;

.field public item_options_edit:Ljava/lang/Boolean;

.field public item_options_global_edit:Ljava/lang/Boolean;

.field public limit_variations_per_item:Ljava/lang/Boolean;

.field public purge_catalog_after_signout:Ljava/lang/Boolean;

.field public should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

.field public x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;
    .locals 13

    .line 4166
    new-instance v12, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->purge_catalog_after_signout:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->limit_variations_per_item:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options_android:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_edit:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_global_edit:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4081
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog;

    move-result-object v0

    return-object v0
.end method

.method public display_modifier_instead_of_option(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4129
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->display_modifier_instead_of_option:Ljava/lang/Boolean;

    return-object p0
.end method

.method public duplicate_sku_items_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4123
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->duplicate_sku_items_applet:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_item_options(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4155
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_item_options_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4142
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->enable_item_options_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public item_options_edit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4150
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_edit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public item_options_global_edit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4160
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->item_options_global_edit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public limit_variations_per_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4134
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->limit_variations_per_item:Ljava/lang/Boolean;

    return-object p0
.end method

.method public purge_catalog_after_signout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4112
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->purge_catalog_after_signout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public should_disallow_itemsfe_inventory_api(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4107
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->should_disallow_itemsfe_inventory_api:Ljava/lang/Boolean;

    return-object p0
.end method

.method public x2_perform_background_sync_after_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;
    .locals 0

    .line 4118
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Catalog$Builder;->x2_perform_background_sync_after_transaction:Ljava/lang/Boolean;

    return-object p0
.end method
