.class public final Lcom/squareup/server/account/MerchantProfileResponse$Companion;
.super Ljava/lang/Object;
.source "MerchantProfileResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/MerchantProfileResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/server/account/MerchantProfileResponse$Companion;",
        "",
        "()V",
        "failure",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "services-data_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/server/account/MerchantProfileResponse$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final failure()Lcom/squareup/server/account/MerchantProfileResponse;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/server/account/MerchantProfileResponse;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3, v1}, Lcom/squareup/server/account/MerchantProfileResponse;-><init>(Lcom/squareup/server/account/MerchantProfileResponse$Entity;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
