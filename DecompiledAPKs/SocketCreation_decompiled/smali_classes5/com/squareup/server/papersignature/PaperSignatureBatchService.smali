.class public interface abstract Lcom/squareup/server/papersignature/PaperSignatureBatchService;
.super Ljava/lang/Object;
.source "PaperSignatureBatchService.java"


# virtual methods
.method public abstract countTendersAwaitingMerchantTip(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;Lcom/squareup/server/SquareCallback;)V
    .param p1    # Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/payments/awaiting-merchant-tip/count"
    .end annotation
.end method

.method public abstract listTendersAwaitingMerchantTip(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;Lcom/squareup/server/SquareCallback;)V
    .param p1    # Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/payments/awaiting-merchant-tip/list"
    .end annotation
.end method

.method public abstract submitTipAndSettleBatch(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;Lcom/squareup/server/SquareCallback;)V
    .param p1    # Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/payments/awaiting-merchant-tip/submit-tip-and-settle-batch"
    .end annotation
.end method

.method public abstract tenderStatus(Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;Lcom/squareup/server/SquareCallback;)V
    .param p1    # Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/payments/awaiting-merchant-tip/tender-processing-state/get"
    .end annotation
.end method
