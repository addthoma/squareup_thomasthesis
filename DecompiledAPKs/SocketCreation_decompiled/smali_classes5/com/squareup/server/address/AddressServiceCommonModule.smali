.class public final Lcom/squareup/server/address/AddressServiceCommonModule;
.super Ljava/lang/Object;
.source "AddressServiceCommonModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAddressService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/address/AddressService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitAuthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 18
    const-class v0, Lcom/squareup/server/address/AddressService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/address/AddressService;

    return-object p0
.end method
