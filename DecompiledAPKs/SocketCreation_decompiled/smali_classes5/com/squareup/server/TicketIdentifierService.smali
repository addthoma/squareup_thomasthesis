.class public interface abstract Lcom/squareup/server/TicketIdentifierService;
.super Ljava/lang/Object;
.source "TicketIdentifierService.java"


# virtual methods
.method public abstract getTicketIdentifier(Lcom/squareup/server/SquareCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/TicketIdentifierResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/1.0/ticket-identifier"
    .end annotation
.end method
