.class Lcom/squareup/server/analytics/EventStreamHelper;
.super Ljava/lang/Object;
.source "EventStreamHelper.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getResult(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/eventstream/EventBatchUploader$Result;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/common/messages/Empty;",
            ">;)",
            "Lcom/squareup/eventstream/EventBatchUploader$Result;"
        }
    .end annotation

    .line 12
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay;

    if-eqz v0, :cond_0

    .line 13
    sget-object p0, Lcom/squareup/eventstream/EventBatchUploader$Result;->SUCCESS:Lcom/squareup/eventstream/EventBatchUploader$Result;

    return-object p0

    .line 14
    :cond_0
    move-object v0, p0

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Error;->getRetryable()Z

    move-result v0

    if-nez v0, :cond_2

    instance-of p0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz p0, :cond_1

    goto :goto_0

    .line 19
    :cond_1
    sget-object p0, Lcom/squareup/eventstream/EventBatchUploader$Result;->FAILURE:Lcom/squareup/eventstream/EventBatchUploader$Result;

    return-object p0

    .line 17
    :cond_2
    :goto_0
    sget-object p0, Lcom/squareup/eventstream/EventBatchUploader$Result;->RETRY:Lcom/squareup/eventstream/EventBatchUploader$Result;

    return-object p0
.end method
