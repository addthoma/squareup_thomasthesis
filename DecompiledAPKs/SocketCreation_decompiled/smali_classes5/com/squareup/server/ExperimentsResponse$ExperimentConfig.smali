.class public Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
.super Ljava/lang/Object;
.source "ExperimentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/ExperimentsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExperimentConfig"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;
    }
.end annotation


# instance fields
.field public final bucket:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/ExperimentsResponse$Bucket;",
            ">;"
        }
    .end annotation
.end field

.field public final description:Ljava/lang/String;

.field public final id:I

.field public final name:Ljava/lang/String;

.field public final started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

.field public final status:Ljava/lang/String;

.field public final updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

.field public final version:Ljava/lang/String;

.field public final winner:Lcom/squareup/server/ExperimentsResponse$Bucket;


# direct methods
.method private constructor <init>(Ljava/util/List;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$Bucket;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/ExperimentsResponse$Bucket;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;",
            "Ljava/lang/String;",
            "Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;",
            "Ljava/lang/String;",
            "Lcom/squareup/server/ExperimentsResponse$Bucket;",
            ")V"
        }
    .end annotation

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->bucket:Ljava/util/List;

    .line 74
    iput-object p2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->description:Ljava/lang/String;

    .line 75
    iput p3, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->id:I

    .line 76
    iput-object p4, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    .line 77
    iput-object p5, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    .line 78
    iput-object p6, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->status:Ljava/lang/String;

    .line 79
    iput-object p7, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    .line 80
    iput-object p8, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->version:Ljava/lang/String;

    .line 81
    iput-object p9, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->winner:Lcom/squareup/server/ExperimentsResponse$Bucket;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$Bucket;Lcom/squareup/server/ExperimentsResponse$1;)V
    .locals 0

    .line 59
    invoke-direct/range {p0 .. p9}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;-><init>(Ljava/util/List;Ljava/lang/String;ILjava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$Bucket;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_13

    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_8

    .line 88
    :cond_1
    check-cast p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    .line 90
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->bucket:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->bucket:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->bucket:Ljava/util/List;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 93
    :cond_3
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->description:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->description:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 96
    :cond_5
    iget v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->id:I

    iget v3, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->id:I

    if-eq v2, v3, :cond_6

    return v1

    .line 99
    :cond_6
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v3, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    goto :goto_2

    :cond_7
    iget-object v2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    if-eqz v2, :cond_8

    :goto_2
    return v1

    .line 102
    :cond_8
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    if-eqz v2, :cond_9

    iget-object v3, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    invoke-virtual {v2, v3}, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    goto :goto_3

    :cond_9
    iget-object v2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    if-eqz v2, :cond_a

    :goto_3
    return v1

    .line 105
    :cond_a
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->status:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v3, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->status:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    goto :goto_4

    :cond_b
    iget-object v2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->status:Ljava/lang/String;

    if-eqz v2, :cond_c

    :goto_4
    return v1

    .line 108
    :cond_c
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    if-eqz v2, :cond_d

    iget-object v3, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    invoke-virtual {v2, v3}, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    goto :goto_5

    :cond_d
    iget-object v2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    if-eqz v2, :cond_e

    :goto_5
    return v1

    .line 111
    :cond_e
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->version:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v3, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->version:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    goto :goto_6

    :cond_f
    iget-object v2, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->version:Ljava/lang/String;

    if-eqz v2, :cond_10

    :goto_6
    return v1

    .line 114
    :cond_10
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->winner:Lcom/squareup/server/ExperimentsResponse$Bucket;

    iget-object p1, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->winner:Lcom/squareup/server/ExperimentsResponse$Bucket;

    if-eqz v2, :cond_11

    invoke-virtual {v2, p1}, Lcom/squareup/server/ExperimentsResponse$Bucket;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_12

    goto :goto_7

    :cond_11
    if-eqz p1, :cond_12

    :goto_7
    return v1

    :cond_12
    return v0

    :cond_13
    :goto_8
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->bucket:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 123
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->description:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 124
    iget v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->id:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 125
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 126
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->started_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 127
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->status:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 128
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->updated_at:Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 129
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->version:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 130
    iget-object v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->winner:Lcom/squareup/server/ExperimentsResponse$Bucket;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/squareup/server/ExperimentsResponse$Bucket;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method
