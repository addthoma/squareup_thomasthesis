.class public Lcom/squareup/server/employees/SetPasscodeRequest;
.super Ljava/lang/Object;
.source "SetPasscodeRequest.java"


# instance fields
.field public final employee_role_token:Ljava/lang/String;

.field public final employee_token:Ljava/lang/String;

.field public final passcode:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->employee_token:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->employee_role_token:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->passcode:Ljava/lang/String;

    return-void
.end method

.method public static createSetEmployeePasscodeRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/employees/SetPasscodeRequest;
    .locals 2

    .line 12
    new-instance v0, Lcom/squareup/server/employees/SetPasscodeRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/server/employees/SetPasscodeRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createSetRolePasscodeRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/employees/SetPasscodeRequest;
    .locals 2

    .line 17
    new-instance v0, Lcom/squareup/server/employees/SetPasscodeRequest;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/server/employees/SetPasscodeRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 28
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 29
    :cond_1
    check-cast p1, Lcom/squareup/server/employees/SetPasscodeRequest;

    .line 30
    iget-object v2, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->employee_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/employees/SetPasscodeRequest;->employee_token:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->employee_role_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/employees/SetPasscodeRequest;->employee_role_token:Ljava/lang/String;

    .line 31
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->passcode:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/employees/SetPasscodeRequest;->passcode:Ljava/lang/String;

    .line 32
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 36
    iget-object v1, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->employee_token:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->employee_role_token:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/employees/SetPasscodeRequest;->passcode:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
