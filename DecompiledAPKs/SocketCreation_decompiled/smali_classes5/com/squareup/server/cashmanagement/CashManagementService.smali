.class public interface abstract Lcom/squareup/server/cashmanagement/CashManagementService;
.super Ljava/lang/Object;
.source "CashManagementService.java"


# virtual methods
.method public abstract closeDrawer(Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest;)Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftResponse;
    .param p1    # Lcom/squareup/protos/client/cashdrawers/CloseCashDrawerShiftRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/cashdrawers/close"
    .end annotation
.end method

.method public abstract createDrawer(Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftResponse;
    .param p1    # Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/cashdrawers/create"
    .end annotation
.end method

.method public abstract emailDrawer(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;
    .param p1    # Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/cashdrawers/email"
    .end annotation
.end method

.method public abstract endDrawer(Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftResponse;
    .param p1    # Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/cashdrawers/end"
    .end annotation
.end method

.method public abstract getDrawerHistory(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;
    .param p1    # Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/cashdrawers/get"
    .end annotation
.end method

.method public abstract updateDrawer(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;
    .param p1    # Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/cashdrawers/update"
    .end annotation
.end method
