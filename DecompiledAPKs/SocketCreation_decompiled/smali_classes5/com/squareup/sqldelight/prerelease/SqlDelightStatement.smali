.class public abstract Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;
.super Ljava/lang/Object;
.source "SqlDelightStatement.java"

# interfaces
.implements Landroidx/sqlite/db/SupportSQLiteStatement;


# instance fields
.field private final program:Landroidx/sqlite/db/SupportSQLiteStatement;

.field private final table:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->table:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    return-void
.end method


# virtual methods
.method public final bindBlob(I[B)V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0, p1, p2}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindBlob(I[B)V

    return-void
.end method

.method public final bindDouble(ID)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0, p1, p2, p3}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindDouble(ID)V

    return-void
.end method

.method public final bindLong(IJ)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0, p1, p2, p3}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindLong(IJ)V

    return-void
.end method

.method public final bindNull(I)V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0, p1}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindNull(I)V

    return-void
.end method

.method public final bindString(ILjava/lang/String;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0, p1, p2}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindString(ILjava/lang/String;)V

    return-void
.end method

.method public final clearBindings()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->clearBindings()V

    return-void
.end method

.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->close()V

    return-void
.end method

.method public final execute()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->execute()V

    return-void
.end method

.method public final executeInsert()J
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->executeInsert()J

    move-result-wide v0

    return-wide v0
.end method

.method public final executeUpdateDelete()I
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->executeUpdateDelete()I

    move-result v0

    return v0
.end method

.method public final getTable()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->table:Ljava/lang/String;

    return-object v0
.end method

.method public final simpleQueryForLong()J
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->simpleQueryForLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public final simpleQueryForString()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;->program:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->simpleQueryForString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
