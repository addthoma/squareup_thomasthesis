.class final Lcom/squareup/sqldelight/android/AndroidPreparedStatement;
.super Ljava/lang/Object;
.source "AndroidSqliteDriver.kt"

# interfaces
.implements Lcom/squareup/sqldelight/android/AndroidStatement;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0010\u0006\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0001\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u001f\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016\u00a2\u0006\u0002\u0010\u000eJ\u001f\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016\u00a2\u0006\u0002\u0010\u0012J\u001a\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0006H\u0016J\u0008\u0010\u0017\u001a\u00020\u0006H\u0016J\u0008\u0010\u0018\u001a\u00020\u0019H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/sqldelight/android/AndroidPreparedStatement;",
        "Lcom/squareup/sqldelight/android/AndroidStatement;",
        "statement",
        "Landroidx/sqlite/db/SupportSQLiteStatement;",
        "(Landroidx/sqlite/db/SupportSQLiteStatement;)V",
        "bindBytes",
        "",
        "index",
        "",
        "bytes",
        "",
        "bindDouble",
        "double",
        "",
        "(ILjava/lang/Double;)V",
        "bindLong",
        "long",
        "",
        "(ILjava/lang/Long;)V",
        "bindString",
        "string",
        "",
        "close",
        "execute",
        "executeQuery",
        "",
        "sqldelight-android-driver_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final statement:Landroidx/sqlite/db/SupportSQLiteStatement;


# direct methods
.method public constructor <init>(Landroidx/sqlite/db/SupportSQLiteStatement;)V
    .locals 1

    const-string v0, "statement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    return-void
.end method


# virtual methods
.method public bindBytes(I[B)V
    .locals 1

    if-nez p2, :cond_0

    .line 174
    iget-object p2, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {p2, p1}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindNull(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0, p1, p2}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindBlob(I[B)V

    :goto_0
    return-void
.end method

.method public bindDouble(ILjava/lang/Double;)V
    .locals 3

    if-nez p2, :cond_0

    .line 182
    iget-object p2, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {p2, p1}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindNull(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindDouble(ID)V

    :goto_0
    return-void
.end method

.method public bindLong(ILjava/lang/Long;)V
    .locals 3

    if-nez p2, :cond_0

    .line 178
    iget-object p2, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {p2, p1}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindNull(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindLong(IJ)V

    :goto_0
    return-void
.end method

.method public bindString(ILjava/lang/String;)V
    .locals 1

    if-nez p2, :cond_0

    .line 186
    iget-object p2, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {p2, p1}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindNull(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0, p1, p2}, Landroidx/sqlite/db/SupportSQLiteStatement;->bindString(ILjava/lang/String;)V

    :goto_0
    return-void
.end method

.method public close()V
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->close()V

    return-void
.end method

.method public execute()V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->statement:Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteStatement;->execute()V

    return-void
.end method

.method public bridge synthetic executeQuery()Lcom/squareup/sqldelight/db/SqlCursor;
    .locals 1

    .line 170
    invoke-virtual {p0}, Lcom/squareup/sqldelight/android/AndroidPreparedStatement;->executeQuery()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqldelight/db/SqlCursor;

    return-object v0
.end method

.method public executeQuery()Ljava/lang/Void;
    .locals 1

    .line 189
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
