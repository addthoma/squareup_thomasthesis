.class final Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;
.super Ljava/util/concurrent/atomic/AtomicBoolean;
.source "RxJavaExtensions.kt"

# interfaces
.implements Lcom/squareup/sqldelight/Query$Listener;
.implements Lio/reactivex/disposables/Disposable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "Lcom/squareup/sqldelight/Query$Listener;",
        "Lio/reactivex/disposables/Disposable;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0002\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005B\'\u0012\u0012\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00080\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u000cH\u0016R\u001a\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;",
        "T",
        "",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "Lcom/squareup/sqldelight/Query$Listener;",
        "Lio/reactivex/disposables/Disposable;",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "Lcom/squareup/sqldelight/Query;",
        "query",
        "(Lio/reactivex/ObservableEmitter;Lcom/squareup/sqldelight/Query;)V",
        "dispose",
        "",
        "isDisposed",
        "",
        "queryResultsChanged",
        "sqldelight-rxjava2-extensions"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final emitter:Lio/reactivex/ObservableEmitter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableEmitter<",
            "Lcom/squareup/sqldelight/Query<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final query:Lcom/squareup/sqldelight/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/sqldelight/Query<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/ObservableEmitter;Lcom/squareup/sqldelight/Query;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Lcom/squareup/sqldelight/Query<",
            "TT;>;>;",
            "Lcom/squareup/sqldelight/Query<",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "query"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;->emitter:Lio/reactivex/ObservableEmitter;

    iput-object p2, p0, Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;->query:Lcom/squareup/sqldelight/Query;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 51
    invoke-virtual {p0, v0, v1}, Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;->query:Lcom/squareup/sqldelight/Query;

    move-object v1, p0

    check-cast v1, Lcom/squareup/sqldelight/Query$Listener;

    invoke-virtual {v0, v1}, Lcom/squareup/sqldelight/Query;->removeListener(Lcom/squareup/sqldelight/Query$Listener;)V

    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .line 48
    invoke-virtual {p0}, Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;->get()Z

    move-result v0

    return v0
.end method

.method public queryResultsChanged()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;->emitter:Lio/reactivex/ObservableEmitter;

    iget-object v1, p0, Lcom/squareup/sqldelight/runtime/rx/QueryListenerAndDisposable;->query:Lcom/squareup/sqldelight/Query;

    invoke-interface {v0, v1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method
