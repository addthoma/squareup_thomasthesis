.class public final Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;
.super Ljava/lang/Object;
.source "TransactionSearchMatch.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0008\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\t\u001a\u00020\u0005H\u00c2\u0003J\t\u0010\n\u001a\u00020\u0005H\u00c2\u0003J\'\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0003H\u00d6\u0001R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
        "",
        "matchedProperty",
        "",
        "searchMatchStartIndex",
        "",
        "searchMatchLength",
        "(Ljava/lang/String;II)V",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;


# instance fields
.field private final matchedProperty:Ljava/lang/String;

.field private final searchMatchLength:I

.field private final searchMatchStartIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->Companion:Lcom/squareup/transactionhistory/processed/TransactionSearchMatch$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->matchedProperty:Ljava/lang/String;

    iput p2, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchStartIndex:I

    iput p3, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchLength:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static final synthetic access$getMatchedProperty$p(Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;)Ljava/lang/String;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->matchedProperty:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getSearchMatchLength$p(Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;)I
    .locals 0

    .line 20
    iget p0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchLength:I

    return p0
.end method

.method public static final synthetic access$getSearchMatchStartIndex$p(Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;)I
    .locals 0

    .line 20
    iget p0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchStartIndex:I

    return p0
.end method

.method private final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->matchedProperty:Ljava/lang/String;

    return-object v0
.end method

.method private final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchStartIndex:I

    return v0
.end method

.method private final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchLength:I

    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;Ljava/lang/String;IIILjava/lang/Object;)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->matchedProperty:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchStartIndex:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchLength:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->copy(Ljava/lang/String;II)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Ljava/lang/String;II)Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;
    .locals 1

    const-string v0, "matchedProperty"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->matchedProperty:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->matchedProperty:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchStartIndex:I

    iget v1, p1, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchStartIndex:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchLength:I

    iget p1, p1, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchLength:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->matchedProperty:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchStartIndex:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchLength:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransactionSearchMatch(matchedProperty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->matchedProperty:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", searchMatchStartIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchStartIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", searchMatchLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;->searchMatchLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
