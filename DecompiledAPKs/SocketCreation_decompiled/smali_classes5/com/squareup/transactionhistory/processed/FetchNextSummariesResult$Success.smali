.class public final Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult$Success;
.super Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult;
.source "ProcessedTransactionsStore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Success"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult$Success;",
        "Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult;",
        "transactionSummaries",
        "",
        "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
        "(Ljava/util/List;)V",
        "getTransactionSummaries",
        "()Ljava/util/List;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final transactionSummaries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "transactionSummaries"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 99
    invoke-direct {p0, v0}, Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult$Success;->transactionSummaries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getTransactionSummaries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/transactionhistory/processed/FetchNextSummariesResult$Success;->transactionSummaries:Ljava/util/List;

    return-object v0
.end method
