.class final Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;
.super Lkotlin/jvm/internal/Lambda;
.source "FailureMessages.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transactionhistory/utilities/FailureMessagesKt;->statusFailureMessageFor(Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        "response",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;

    invoke-direct {v0}, Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;-><init>()V

    sput-object v0, Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;->INSTANCE:Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/transactionhistory/utilities/FailureMessagesKt$statusFailureMessageFor$1;->invoke(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p1

    return-object p1
.end method
