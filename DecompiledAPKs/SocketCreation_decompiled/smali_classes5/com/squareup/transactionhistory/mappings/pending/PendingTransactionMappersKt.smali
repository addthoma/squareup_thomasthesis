.class public final Lcom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt;
.super Ljava/lang/Object;
.source "PendingTransactionMappers.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPendingTransactionMappers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PendingTransactionMappers.kt\ncom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,42:1\n1360#2:43\n1429#2,3:44\n*E\n*S KotlinDebug\n*F\n+ 1 PendingTransactionMappers.kt\ncom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt\n*L\n37#1:43\n37#1,3:44\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0016\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0001\u001a \u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005*\n\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u00072\u0006\u0010\t\u001a\u00020\n\u001a\n\u0010\u000b\u001a\u00020\u0006*\u00020\u0003\u00a8\u0006\u000c"
    }
    d2 = {
        "toOptionalPendingTransaction",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "toPendingTransactionSummaries",
        "",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "",
        "Lcom/squareup/queue/PendingPayment;",
        "res",
        "Lcom/squareup/util/Res;",
        "toPendingTransactionSummary",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toOptionalPendingTransaction(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toOptionalPendingTransaction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt$toOptionalPendingTransaction$1;->INSTANCE:Lcom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt$toOptionalPendingTransaction$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0
.end method

.method public static final toPendingTransactionSummaries(Ljava/util/Collection;Lcom/squareup/util/Res;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/queue/PendingPayment;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_1

    .line 37
    check-cast p0, Ljava/lang/Iterable;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 44
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 45
    check-cast v1, Lcom/squareup/queue/PendingPayment;

    .line 39
    invoke-interface {v1, p1}, Lcom/squareup/queue/PendingPayment;->asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    const-string v2, "pendingPayment\n        .asBill(res)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt;->toPendingTransactionSummary(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_1

    .line 41
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public static final toPendingTransactionSummary(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;
    .locals 12

    const-string v0, "$this$toPendingTransactionSummary"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v1, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->Companion:Lcom/squareup/transactionhistory/pending/PendingTransactionSummary$Companion;

    .line 23
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v2, "id"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 24
    iget-object v3, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    iget-object v3, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 25
    iget-object v4, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    const-string/jumbo v2, "time"

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v5, p0, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    .line 27
    iget-object v6, p0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    const-string/jumbo v2, "total"

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v2, p0, Lcom/squareup/billhistory/model/BillHistory;->note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    const-string v7, "note"

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->getNote()Ljava/lang/String;

    move-result-object v7

    .line 29
    invoke-static {p0}, Lcom/squareup/transactionhistory/mappings/TransactionHistoryMappersKt;->extractTenderInfo(Lcom/squareup/billhistory/model/BillHistory;)Ljava/util/List;

    move-result-object v8

    .line 30
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 31
    iget-boolean v2, p0, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 32
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->hasError()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    move-object v2, v0

    .line 22
    invoke-virtual/range {v1 .. v11}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary$Companion;->newPendingTransactionSummary(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;

    move-result-object p0

    return-object p0
.end method
