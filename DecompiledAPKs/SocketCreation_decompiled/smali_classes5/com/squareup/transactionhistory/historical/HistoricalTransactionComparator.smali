.class public final Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;
.super Ljava/lang/Object;
.source "HistoricalTransactionComparator.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0003B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0008\u001a\u00020\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;",
        "Ljava/util/Comparator;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "Lkotlin/Comparator;",
        "()V",
        "compare",
        "",
        "lhs",
        "rhs",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;

    invoke-direct {v0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;-><init>()V

    sput-object v0, Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;->INSTANCE:Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)I
    .locals 3

    const-string v0, "lhs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rhs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 21
    :cond_0
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isPending(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v0

    invoke-static {p2}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isPending(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v2

    if-ne v0, v2, :cond_1

    goto :goto_0

    .line 22
    :cond_1
    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->isPending(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, -0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_3

    .line 28
    invoke-virtual {p2}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getDate()Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    :cond_3
    return v1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    check-cast p2, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionComparator;->compare(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)I

    move-result p1

    return p1
.end method
