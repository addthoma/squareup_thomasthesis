.class public final Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;
.super Ljava/lang/Object;
.source "HistoricalTransactionSummary.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHistoricalTransactionSummary.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HistoricalTransactionSummary.kt\ncom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,299:1\n1550#2,3:300\n*E\n*S KotlinDebug\n*F\n+ 1 HistoricalTransactionSummary.kt\ncom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt\n*L\n298#1,3:300\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0002\u001a\n\u0010\u0007\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0008\u001a\u00020\u0001*\u00020\u0002\u001a\u001d\u0010\t\u001a\u00020\u0001*\u0004\u0018\u00010\u00022\u0008\u0010\n\u001a\u0004\u0018\u00010\u0002H\u0007\u00a2\u0006\u0002\u0008\u000b\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0000\u0010\u0003\"\u0015\u0010\u0004\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0003\u00a8\u0006\u000c"
    }
    d2 = {
        "isPending",
        "",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z",
        "isProcessed",
        "getTransactionIds",
        "Lcom/squareup/transactionhistory/TransactionIds;",
        "hasCardTender",
        "isStoreAndForward",
        "sameIdentityAs",
        "other",
        "sameIdentities",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;
    .locals 2

    const-string v0, "$this$getTransactionIds"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    sget-object v0, Lcom/squareup/transactionhistory/TransactionIds;->Companion:Lcom/squareup/transactionhistory/TransactionIds$Companion;

    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lcom/squareup/transactionhistory/TransactionIds$Companion;->newTransactionIds(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object p0

    return-object p0
.end method

.method public static final hasCardTender(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 4

    const-string v0, "$this$hasCardTender"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getTenderInfo()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 300
    instance-of v0, p0, Ljava/util/Collection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 301
    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/transactionhistory/TenderInfo;

    .line 298
    invoke-virtual {v0}, Lcom/squareup/transactionhistory/TenderInfo;->getTenderType()Lcom/squareup/transactionhistory/TenderType;

    move-result-object v0

    sget-object v3, Lcom/squareup/transactionhistory/TenderType;->CARD:Lcom/squareup/transactionhistory/TenderType;

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_3
    :goto_1
    return v2
.end method

.method public static final isPending(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 1

    const-string v0, "$this$isPending"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isProcessed(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 1

    const-string v0, "$this$isProcessed"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 285
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isStoreAndForward(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 2

    const-string v0, "$this$isStoreAndForward"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v0

    sget-object v1, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->STORED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object p0

    sget-object v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final sameIdentities(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 0

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 263
    :cond_0
    invoke-static {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object p0

    invoke-static {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummaryKt;->getTransactionIds(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/transactionhistory/TransactionIdsKt;->atLeastOneIdIsEqual(Lcom/squareup/transactionhistory/TransactionIds;Lcom/squareup/transactionhistory/TransactionIds;)Z

    move-result p0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x0

    :goto_1
    return p0
.end method
