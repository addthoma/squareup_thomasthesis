.class public final Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;
.super Ljava/lang/Object;
.source "RealOpenTicketsSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tickets/RealOpenTicketsSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg11Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg12Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 62
    iput-object p10, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg9Provider:Ljavax/inject/Provider;

    .line 63
    iput-object p11, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg10Provider:Ljavax/inject/Provider;

    .line 64
    iput-object p12, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg11Provider:Ljavax/inject/Provider;

    .line 65
    iput-object p13, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg12Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;>;)",
            "Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;"
        }
    .end annotation

    .line 81
    new-instance v14, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/OpenTicketEnabledSetting;Ljava/lang/Object;Ljava/lang/Object;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/tickets/RealOpenTicketsSettings;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/tickets/OpenTicketEnabledSetting;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/tickets/RealOpenTicketsSettings;"
        }
    .end annotation

    .line 89
    new-instance v14, Lcom/squareup/tickets/RealOpenTicketsSettings;

    move-object/from16 v8, p7

    check-cast v8, Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;

    move-object/from16 v9, p8

    check-cast v9, Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/tickets/RealOpenTicketsSettings;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/OpenTicketEnabledSetting;Lcom/squareup/tickets/PredefinedTicketsEnabledSetting;Lcom/squareup/tickets/OpenTicketsAsHomeScreenSetting;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/tickets/RealOpenTicketsSettings;
    .locals 14

    .line 70
    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v4, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg3Provider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/tickets/OpenTicketEnabledSetting;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    iget-object v10, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg9Provider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg11Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->arg12Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/settings/LocalSetting;

    invoke-static/range {v1 .. v13}, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/OpenTicketEnabledSetting;Ljava/lang/Object;Ljava/lang/Object;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/tickets/RealOpenTicketsSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/tickets/RealOpenTicketsSettings_Factory;->get()Lcom/squareup/tickets/RealOpenTicketsSettings;

    move-result-object v0

    return-object v0
.end method
