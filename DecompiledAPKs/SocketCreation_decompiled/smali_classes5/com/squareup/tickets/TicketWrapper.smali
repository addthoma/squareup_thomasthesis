.class public Lcom/squareup/tickets/TicketWrapper;
.super Ljava/lang/Object;
.source "TicketWrapper.java"


# instance fields
.field protected ticketProto:Lcom/squareup/protos/client/tickets/Ticket;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/tickets/Ticket;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/squareup/tickets/VectorClocks;->addClientClockIfMissing(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    .line 30
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketWrapper;->assertValidTicket()V

    return-void
.end method

.method static byteArrayFromProto(Lcom/squareup/protos/client/tickets/Ticket;)[B
    .locals 1

    .line 148
    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p0}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p0

    return-object p0
.end method

.method static protoFromByteArray([B)Lcom/squareup/protos/client/tickets/Ticket;
    .locals 1

    .line 140
    :try_start_0
    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/tickets/Ticket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 142
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method assertValidTicket()V
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    const-string v1, "Ticket proto"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    const-string v1, "Ticket proto id pair"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 133
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 134
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    const-string v1, "Ticket proto cart"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method getClientClockVersion()J
    .locals 2

    .line 95
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketWrapper;->getVectorClock()Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/tickets/VectorClocks;->getClientClock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    return-object v0
.end method

.method getClientUpdatedAt()Ljava/util/Date;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->client_updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->requireIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method getClosedAt()Ljava/util/Date;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    if-eqz v0, :cond_0

    .line 74
    iget-object v1, v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_0

    .line 75
    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->requireIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    return-object v0
.end method

.method getProto()Lcom/squareup/protos/client/tickets/Ticket;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-object v0
.end method

.method getTicketProtoWithoutClientClock()Lcom/squareup/protos/client/tickets/Ticket;
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    .line 58
    invoke-static {v1}, Lcom/squareup/tickets/VectorClocks;->getClientlessVectorClock(Lcom/squareup/protos/client/tickets/Ticket;)Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object v0

    return-object v0
.end method

.method getVectorClock()Lcom/squareup/protos/client/tickets/VectorClock;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->vector_clock:Lcom/squareup/protos/client/tickets/VectorClock;

    return-object v0
.end method

.method incrementClientClock()J
    .locals 4

    .line 100
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketWrapper;->getClientClockVersion()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 102
    invoke-virtual {p0, v0, v1}, Lcom/squareup/tickets/TicketWrapper;->setClientClock(J)V

    return-wide v0
.end method

.method isClosed()Z
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket;->close_event:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;

    iget-object v0, v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;->close_reason:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method resetClientClock()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 108
    invoke-virtual {p0, v0, v1}, Lcom/squareup/tickets/TicketWrapper;->setClientClock(J)V

    return-void
.end method

.method setClientClock(J)V
    .locals 3

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/squareup/tickets/TicketWrapper;->getVectorClock()Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/tickets/VectorClock;->clock_list:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    .line 114
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    .line 115
    invoke-static {v2}, Lcom/squareup/tickets/VectorClocks;->assertClientClock(Lcom/squareup/protos/client/tickets/VectorClock$Clock;)V

    .line 117
    invoke-virtual {v2}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->newBuilder()Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;

    move-result-object v2

    .line 118
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->version(Ljava/lang/Long;)Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->build()Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    move-result-object p1

    .line 120
    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-virtual {p0}, Lcom/squareup/tickets/TicketWrapper;->getVectorClock()Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/VectorClock;->newBuilder()Lcom/squareup/protos/client/tickets/VectorClock$Builder;

    move-result-object p1

    .line 123
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/tickets/VectorClock$Builder;->clock_list(Ljava/util/List;)Lcom/squareup/protos/client/tickets/VectorClock$Builder;

    move-result-object p1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/VectorClock$Builder;->build()Lcom/squareup/protos/client/tickets/VectorClock;

    move-result-object p1

    .line 125
    iget-object p2, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {p2}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p2

    .line 126
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->vector_clock(Lcom/squareup/protos/client/tickets/VectorClock;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 127
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method

.method setClientUpdatedAt(Ljava/util/Date;)V
    .locals 1

    .line 87
    new-instance v0, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket;->newBuilder()Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->client_updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/tickets/Ticket$Builder;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/Ticket$Builder;->build()Lcom/squareup/protos/client/tickets/Ticket;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/tickets/TicketWrapper;->ticketProto:Lcom/squareup/protos/client/tickets/Ticket;

    return-void
.end method
