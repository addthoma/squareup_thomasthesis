.class public final enum Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;
.super Ljava/lang/Enum;
.source "LocalTicketUpdateEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/LocalTicketUpdateEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

.field public static final enum CREATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

.field public static final enum DELETED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

.field public static final enum TRANSFERRED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

.field public static final enum UNLOCKED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

.field public static final enum UPDATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 25
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v1, 0x0

    const-string v2, "CREATED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->CREATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    .line 31
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v2, 0x1

    const-string v3, "UPDATED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UPDATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    .line 37
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v3, 0x2

    const-string v4, "DELETED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->DELETED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    .line 43
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v4, 0x3

    const-string v5, "UNLOCKED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UNLOCKED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    .line 48
    new-instance v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v5, 0x4

    const-string v6, "TRANSFERRED"

    invoke-direct {v0, v6, v5}, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->TRANSFERRED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    .line 20
    sget-object v6, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->CREATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UPDATED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->DELETED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->UNLOCKED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->TRANSFERRED:Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->$VALUES:[Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;
    .locals 1

    .line 20
    const-class v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->$VALUES:[Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    invoke-virtual {v0}, [Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tickets/LocalTicketUpdateEvent$EventType;

    return-object v0
.end method
