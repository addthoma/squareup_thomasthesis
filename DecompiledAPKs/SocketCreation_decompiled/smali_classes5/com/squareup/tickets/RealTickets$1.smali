.class Lcom/squareup/tickets/RealTickets$1;
.super Ljava/lang/Object;
.source "RealTickets.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tickets/RealTickets;->getBundler()Lmortar/bundler/Bundler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/RealTickets;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/RealTickets;)V
    .locals 0

    .line 767
    iput-object p1, p0, Lcom/squareup/tickets/RealTickets$1;->this$0:Lcom/squareup/tickets/RealTickets;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 769
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 774
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets$1;->this$0:Lcom/squareup/tickets/RealTickets;

    invoke-static {v0}, Lcom/squareup/tickets/RealTickets;->access$000(Lcom/squareup/tickets/RealTickets;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "TICKET_LOCK_ID"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/tickets/RealTickets;->access$002(Lcom/squareup/tickets/RealTickets;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 779
    iget-object v0, p0, Lcom/squareup/tickets/RealTickets$1;->this$0:Lcom/squareup/tickets/RealTickets;

    invoke-static {v0}, Lcom/squareup/tickets/RealTickets;->access$000(Lcom/squareup/tickets/RealTickets;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TICKET_LOCK_ID"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
