.class public Lcom/squareup/tickets/TicketCounter;
.super Ljava/lang/Object;
.source "TicketCounter.java"


# static fields
.field public static final EMPTY:Lcom/squareup/tickets/TicketCounter;


# instance fields
.field private final allTicketsCount:I

.field private final customTicketsCount:I

.field private final groupTicketCounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 16
    new-instance v0, Lcom/squareup/tickets/TicketCounter;

    .line 17
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/squareup/tickets/TicketCounter;-><init>(IILjava/util/Map;)V

    sput-object v0, Lcom/squareup/tickets/TicketCounter;->EMPTY:Lcom/squareup/tickets/TicketCounter;

    return-void
.end method

.method public constructor <init>(IILjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/squareup/tickets/TicketCounter;->allTicketsCount:I

    .line 26
    iput p2, p0, Lcom/squareup/tickets/TicketCounter;->customTicketsCount:I

    .line 27
    iput-object p3, p0, Lcom/squareup/tickets/TicketCounter;->groupTicketCounts:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getAllTicketsCount()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/squareup/tickets/TicketCounter;->allTicketsCount:I

    return v0
.end method

.method public getCustomTicketsCount()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/tickets/TicketCounter;->customTicketsCount:I

    return v0
.end method

.method public getGroupTicketCount(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 42
    :cond_0
    iget-object v1, p0, Lcom/squareup/tickets/TicketCounter;->groupTicketCounts:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_1

    .line 43
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_1
    return v0
.end method

.method public hasTickets()Z
    .locals 1

    .line 47
    iget v0, p0, Lcom/squareup/tickets/TicketCounter;->allTicketsCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
