.class public final enum Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;
.super Ljava/lang/Enum;
.source "OpenTicketsLoaderV2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TicketLoadResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0004\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;",
        "",
        "(Ljava/lang/String;I)V",
        "Wait",
        "Skip",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

.field public static final enum Skip:Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

.field public static final enum Wait:Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    new-instance v1, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    const/4 v2, 0x0

    const-string v3, "Wait"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;->Wait:Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    const/4 v2, 0x1

    const-string v3, "Skip"

    invoke-direct {v1, v3, v2}, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;->Skip:Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;->$VALUES:[Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;
    .locals 1

    const-class v0, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;
    .locals 1

    sget-object v0, Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;->$VALUES:[Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    invoke-virtual {v0}, [Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tickets/v2/loader/OpenTicketsLoaderV2$TicketLoadResult;

    return-object v0
.end method
