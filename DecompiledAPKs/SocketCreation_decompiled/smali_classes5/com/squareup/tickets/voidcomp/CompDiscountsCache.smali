.class public Lcom/squareup/tickets/voidcomp/CompDiscountsCache;
.super Lcom/squareup/tickets/voidcomp/CogsCache;
.source "CompDiscountsCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/tickets/voidcomp/CogsCache<",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/tickets/voidcomp/CogsCache;-><init>(Lcom/squareup/cogs/Cogs;)V

    return-void
.end method

.method public varargs constructor <init>(Lcom/squareup/cogs/Cogs;[Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/tickets/voidcomp/CogsCache;-><init>(Lcom/squareup/cogs/Cogs;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$readDataFromCogsLocal$0(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/shared/catalog/models/CatalogDiscount;)I
    .locals 0

    .line 47
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getCompOrdinal()I

    move-result p0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getCompOrdinal()I

    move-result p1

    sub-int/2addr p0, p1

    return p0
.end method


# virtual methods
.method public getCompDiscountWithId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 3

    .line 36
    invoke-virtual {p0}, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;->getAll()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 37
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getReasons()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;->getAll()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 29
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method protected readDataFromCogsLocal(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation

    .line 45
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->readCompDiscounts()Ljava/util/List;

    move-result-object p1

    .line 46
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 47
    sget-object v0, Lcom/squareup/tickets/voidcomp/-$$Lambda$CompDiscountsCache$Rw4masOvamXRVDuDGvH0T8EHdN0;->INSTANCE:Lcom/squareup/tickets/voidcomp/-$$Lambda$CompDiscountsCache$Rw4masOvamXRVDuDGvH0T8EHdN0;

    invoke-static {p1, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 49
    invoke-static {p1}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
