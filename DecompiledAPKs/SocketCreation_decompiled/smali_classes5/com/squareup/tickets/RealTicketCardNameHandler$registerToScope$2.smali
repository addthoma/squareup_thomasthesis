.class final Lcom/squareup/tickets/RealTicketCardNameHandler$registerToScope$2;
.super Ljava/lang/Object;
.source "RealTicketCardNameHandler.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tickets/RealTicketCardNameHandler;->registerToScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/RealTicketCardNameHandler;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/RealTicketCardNameHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tickets/RealTicketCardNameHandler$registerToScope$2;->this$0:Lcom/squareup/tickets/RealTicketCardNameHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/tickets/RealTicketCardNameHandler$registerToScope$2;->this$0:Lcom/squareup/tickets/RealTicketCardNameHandler;

    invoke-static {v0, p1}, Lcom/squareup/tickets/RealTicketCardNameHandler;->access$onSwipe(Lcom/squareup/tickets/RealTicketCardNameHandler;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;

    invoke-virtual {p0, p1}, Lcom/squareup/tickets/RealTicketCardNameHandler$registerToScope$2;->accept(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    return-void
.end method
