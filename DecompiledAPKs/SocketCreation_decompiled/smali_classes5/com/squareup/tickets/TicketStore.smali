.class public interface abstract Lcom/squareup/tickets/TicketStore;
.super Ljava/lang/Object;
.source "TicketStore.java"

# interfaces
.implements Lcom/squareup/tickets/TicketDatabase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tickets/TicketStore$EmployeeAccess;
    }
.end annotation


# virtual methods
.method public abstract addTicket(Lcom/squareup/tickets/OpenTicket;)V
.end method

.method public abstract close()V
.end method

.method public abstract debugWipeStore()V
.end method

.method public abstract deleteTickets(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getAllDeletableTicketsOldestFirst()Lcom/squareup/util/ReadOnlyCursorList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "Lcom/squareup/tickets/TicketRowCursorList$TicketRow;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCustomTicketCount(Ljava/lang/String;)I
.end method

.method public abstract getCustomTicketList(Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
.end method

.method public abstract getGroupTicketCounts(Ljava/lang/String;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
.end method

.method public abstract getNonClosedTicketsUnordered()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNonZeroClientClockTickets()Lcom/squareup/tickets/TicketRowCursorList;
.end method

.method public abstract getTicketCount(Ljava/lang/String;)I
.end method

.method public abstract getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)Lcom/squareup/tickets/TicketRowCursorList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/tickets/TicketSort;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketStore$EmployeeAccess;",
            ")",
            "Lcom/squareup/tickets/TicketRowCursorList;"
        }
    .end annotation
.end method

.method public abstract retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;
.end method

.method public abstract updateTicket(Lcom/squareup/tickets/OpenTicket;)V
.end method
