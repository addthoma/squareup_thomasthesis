.class public Lcom/squareup/tickets/RealAvailableTemplateCountCache;
.super Ljava/lang/Object;
.source "RealAvailableTemplateCountCache.java"

# interfaces
.implements Lcom/squareup/opentickets/AvailableTemplateCountCache;


# instance fields
.field private final availableTicketCountMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/tickets/RealAvailableTemplateCountCache;->availableTicketCountMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/tickets/RealAvailableTemplateCountCache;->availableTicketCountMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public getAvailableTemplateCountForGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)I
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/tickets/RealAvailableTemplateCountCache;->availableTicketCountMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 32
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getTicketTemplateCount()I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    return p1
.end method

.method public updateAvailableTemplateCountForGroup(Ljava/lang/String;I)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/tickets/RealAvailableTemplateCountCache;->availableTicketCountMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
