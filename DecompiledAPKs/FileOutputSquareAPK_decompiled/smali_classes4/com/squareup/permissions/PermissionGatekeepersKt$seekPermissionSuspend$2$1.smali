.class public final Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "PermissionGatekeepers.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissions/PermissionGatekeepersKt;->seekPermissionSuspend(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPermissionGatekeepers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PermissionGatekeepers.kt\ncom/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1\n*L\n1#1,73:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "failure",
        "",
        "success",
        "employees_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $continuation:Lkotlin/coroutines/Continuation;


# direct methods
.method constructor <init>(Lkotlin/coroutines/Continuation;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1;->$continuation:Lkotlin/coroutines/Continuation;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1;->$continuation:Lkotlin/coroutines/Continuation;

    sget-object v1, Lcom/squareup/permissions/PermissionAccessResult$Failure;->INSTANCE:Lcom/squareup/permissions/PermissionAccessResult$Failure;

    sget-object v2, Lkotlin/Result;->Companion:Lkotlin/Result$Companion;

    invoke-static {v1}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/coroutines/Continuation;->resumeWith(Ljava/lang/Object;)V

    return-void
.end method

.method public success()V
    .locals 3

    .line 44
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1;->$continuation:Lkotlin/coroutines/Continuation;

    new-instance v1, Lcom/squareup/permissions/PermissionAccessResult$Success;

    invoke-virtual {p0}, Lcom/squareup/permissions/PermissionGatekeepersKt$seekPermissionSuspend$2$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/permissions/PermissionAccessResult$Success;-><init>(Ljava/lang/String;)V

    sget-object v2, Lkotlin/Result;->Companion:Lkotlin/Result$Companion;

    invoke-static {v1}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/coroutines/Continuation;->resumeWith(Ljava/lang/Object;)V

    return-void
.end method
