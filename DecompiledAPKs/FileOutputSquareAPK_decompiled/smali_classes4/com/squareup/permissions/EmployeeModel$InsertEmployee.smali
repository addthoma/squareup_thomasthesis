.class public final Lcom/squareup/permissions/EmployeeModel$InsertEmployee;
.super Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;
.source "EmployeeModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/EmployeeModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InsertEmployee"
.end annotation


# direct methods
.method public constructor <init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V
    .locals 1

    const-string v0, "INSERT OR REPLACE INTO employees_table (token, first_name, last_name, employee_number, hashed_passcode, salt, iterations, timecard_id, clockin_time, active, can_access_register_with_passcode, can_track_time,\nis_account_owner, is_owner, role_token)\nVALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    .line 286
    invoke-interface {p1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteStatement;

    move-result-object p1

    const-string v0, "employees_table"

    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;-><init>(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)V

    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 11

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p15

    const/4 v10, 0x1

    if-nez v1, :cond_0

    .line 300
    invoke-virtual {p0, v10}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_0

    .line 302
    :cond_0
    invoke-virtual {p0, v10, p1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_0
    const/4 v1, 0x2

    if-nez v2, :cond_1

    .line 305
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_1

    .line 307
    :cond_1
    invoke-virtual {p0, v1, p2}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_1
    const/4 v1, 0x3

    if-nez v3, :cond_2

    .line 310
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_2

    .line 312
    :cond_2
    invoke-virtual {p0, v1, p3}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_2
    const/4 v1, 0x4

    if-nez v4, :cond_3

    .line 315
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_3

    .line 317
    :cond_3
    invoke-virtual {p0, v1, p4}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_3
    const/4 v1, 0x5

    if-nez v5, :cond_4

    .line 320
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_4

    .line 322
    :cond_4
    invoke-virtual {p0, v1, v5}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_4
    const/4 v1, 0x6

    if-nez v6, :cond_5

    .line 325
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_5

    .line 327
    :cond_5
    invoke-virtual {p0, v1, v6}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_5
    const/4 v1, 0x7

    if-nez p7, :cond_6

    .line 330
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_6

    .line 332
    :cond_6
    invoke-virtual/range {p7 .. p7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindLong(IJ)V

    :goto_6
    const/16 v1, 0x8

    if-nez v7, :cond_7

    .line 335
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_7

    .line 337
    :cond_7
    invoke-virtual {p0, v1, v7}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_7
    const/16 v1, 0x9

    if-nez v8, :cond_8

    .line 340
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_8

    .line 342
    :cond_8
    invoke-virtual {p0, v1, v8}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_8
    const/16 v1, 0xa

    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x0

    if-nez p10, :cond_9

    .line 345
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_a

    .line 347
    :cond_9
    invoke-virtual/range {p10 .. p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_a

    move-wide v6, v2

    goto :goto_9

    :cond_a
    move-wide v6, v4

    :goto_9
    invoke-virtual {p0, v1, v6, v7}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindLong(IJ)V

    :goto_a
    const/16 v1, 0xb

    if-nez p11, :cond_b

    .line 350
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_c

    .line 352
    :cond_b
    invoke-virtual/range {p11 .. p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_c

    move-wide v6, v2

    goto :goto_b

    :cond_c
    move-wide v6, v4

    :goto_b
    invoke-virtual {p0, v1, v6, v7}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindLong(IJ)V

    :goto_c
    const/16 v1, 0xc

    if-nez p12, :cond_d

    .line 355
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_e

    .line 357
    :cond_d
    invoke-virtual/range {p12 .. p12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_e

    move-wide v6, v2

    goto :goto_d

    :cond_e
    move-wide v6, v4

    :goto_d
    invoke-virtual {p0, v1, v6, v7}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindLong(IJ)V

    :goto_e
    const/16 v1, 0xd

    if-nez p13, :cond_f

    .line 360
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_10

    .line 362
    :cond_f
    invoke-virtual/range {p13 .. p13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_10

    move-wide v6, v2

    goto :goto_f

    :cond_10
    move-wide v6, v4

    :goto_f
    invoke-virtual {p0, v1, v6, v7}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindLong(IJ)V

    :goto_10
    if-nez p14, :cond_11

    const/16 v1, 0xe

    .line 365
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_12

    :cond_11
    const/16 v1, 0xe

    .line 367
    invoke-virtual/range {p14 .. p14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_12

    goto :goto_11

    :cond_12
    move-wide v2, v4

    :goto_11
    invoke-virtual {p0, v1, v2, v3}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindLong(IJ)V

    :goto_12
    if-nez v9, :cond_13

    const/16 v1, 0xf

    .line 370
    invoke-virtual {p0, v1}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindNull(I)V

    goto :goto_13

    :cond_13
    const/16 v1, 0xf

    .line 372
    invoke-virtual {p0, v1, v9}, Lcom/squareup/permissions/EmployeeModel$InsertEmployee;->bindString(ILjava/lang/String;)V

    :goto_13
    return-void
.end method
