.class public interface abstract Lcom/squareup/permissions/EmployeeManagement;
.super Ljava/lang/Object;
.source "EmployeeManagement.java"


# virtual methods
.method public abstract arePasscodesEnabled()Z
.end method

.method public abstract canForcePasscodeReentry()Z
.end method

.method public abstract checkPermissionForPasscode(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCurrentEmployee()Lcom/squareup/permissions/Employee;
.end method

.method public abstract getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;
.end method

.method public abstract getCurrentEmployeeToken()Ljava/lang/String;
.end method

.method public abstract getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract hasAnyPermission(Ljava/util/Set;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)Z"
        }
    .end annotation
.end method

.method public abstract hasPermission(Lcom/squareup/permissions/Permission;)Z
.end method

.method public abstract isEldmEnabled()Z
.end method

.method public abstract isPasscodeLocked()Z
.end method

.method public abstract onFreeTier()Z
.end method

.method public abstract oneEmployeeProtoByToken(Ljava/lang/String;)Lio/reactivex/Maybe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/protos/client/Employee;",
            ">;"
        }
    .end annotation
.end method

.method public abstract shouldAskForPasscode()Z
.end method

.method public abstract shouldDisplayFeature(Lcom/squareup/permissions/Permission;)Z
.end method

.method public abstract shouldShowClockInButton()Z
.end method
