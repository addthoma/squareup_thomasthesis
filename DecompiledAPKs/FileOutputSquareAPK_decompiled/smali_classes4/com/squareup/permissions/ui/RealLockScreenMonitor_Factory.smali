.class public final Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;
.super Ljava/lang/Object;
.source "RealLockScreenMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/permissions/ui/RealLockScreenMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final jailKeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->posContainerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->timecardsStarterProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;)",
            "Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;"
        }
    .end annotation

    .line 81
    new-instance v11, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/squareup/jailkeeper/JailKeeper;)Lcom/squareup/permissions/ui/RealLockScreenMonitor;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ")",
            "Lcom/squareup/permissions/ui/RealLockScreenMonitor;"
        }
    .end annotation

    .line 89
    new-instance v11, Lcom/squareup/permissions/ui/RealLockScreenMonitor;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/permissions/ui/RealLockScreenMonitor;-><init>(Ldagger/Lazy;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/squareup/jailkeeper/JailKeeper;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/permissions/ui/RealLockScreenMonitor;
    .locals 11

    .line 70
    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->timecardsStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    iget-object v0, p0, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/jailkeeper/JailKeeper;

    invoke-static/range {v1 .. v10}, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/squareup/jailkeeper/JailKeeper;)Lcom/squareup/permissions/ui/RealLockScreenMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/permissions/ui/RealLockScreenMonitor_Factory;->get()Lcom/squareup/permissions/ui/RealLockScreenMonitor;

    move-result-object v0

    return-object v0
.end method
