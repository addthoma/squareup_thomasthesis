.class public Lcom/squareup/permissions/CurrentPasscodeEmployeeState;
.super Ljava/lang/Object;
.source "CurrentPasscodeEmployeeState.java"


# static fields
.field public static final GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;


# instance fields
.field private currentEmployee:Lcom/squareup/permissions/Employee;

.field private final currentEmployeeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation
.end field

.field private lastEmployee:Lcom/squareup/permissions/Employee;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 20
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;-><init>()V

    const/4 v1, 0x1

    .line 22
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 23
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 24
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 25
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 26
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    .line 20
    invoke-static {v0}, Lcom/squareup/permissions/Employee;->fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;

    move-result-object v0

    sput-object v0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployeeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public clearCurrentEmployee()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    iput-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->lastEmployee:Lcom/squareup/permissions/Employee;

    const/4 v0, 0x0

    .line 98
    iput-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    .line 99
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployeeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public clearLastEmployee()V
    .locals 1

    const/4 v0, 0x0

    .line 103
    iput-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->lastEmployee:Lcom/squareup/permissions/Employee;

    return-void
.end method

.method public currentEmployee()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployeeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public getCurrentEmployee()Lcom/squareup/permissions/Employee;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    return-object v0
.end method

.method public isCurrentEmployeeGuest()Z
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    sget-object v1, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isRepeatedLogin()Z
    .locals 4

    .line 83
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    if-eqz v0, :cond_3

    .line 87
    invoke-virtual {p0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->isCurrentEmployeeGuest()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->lastEmployee:Lcom/squareup/permissions/Employee;

    sget-object v3, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->lastEmployee:Lcom/squareup/permissions/Employee;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    iget-object v0, v0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->lastEmployee:Lcom/squareup/permissions/Employee;

    iget-object v3, v3, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 84
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No employee currently logged in, unable to compare."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public refreshCurrentEmployee()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployeeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    invoke-static {v1}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setCurrentEmployee(Lcom/squareup/permissions/Employee;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 52
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot login: another employee is currently logged in."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 55
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    .line 56
    iget-object p1, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployeeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    invoke-static {v0}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setCurrentEmployeeGuest()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    if-nez v0, :cond_0

    .line 74
    sget-object v0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    iput-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    .line 75
    iget-object v0, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployeeRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee:Lcom/squareup/permissions/Employee;

    invoke-static {v1}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot login: another employee is currently logged in."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
