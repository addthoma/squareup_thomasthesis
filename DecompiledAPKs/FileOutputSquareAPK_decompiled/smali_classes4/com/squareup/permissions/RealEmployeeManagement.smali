.class public Lcom/squareup/permissions/RealEmployeeManagement;
.super Ljava/lang/Object;
.source "RealEmployeeManagement.java"

# interfaces
.implements Lcom/squareup/permissions/EmployeeManagement;
.implements Lmortar/Scoped;


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/permissions/RealEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 58
    iput-object p2, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 59
    iput-object p3, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 60
    iput-object p4, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 61
    iput-object p5, p0, Lcom/squareup/permissions/RealEmployeeManagement;->res:Lcom/squareup/util/Res;

    .line 62
    iput-object p6, p0, Lcom/squareup/permissions/RealEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private doesMobileEmployeeHavePermission(Lcom/squareup/permissions/Permission;)Z
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->getEmployeesEntity()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/EmployeesEntity;->permissions:Ljava/util/List;

    .line 209
    invoke-virtual {p1}, Lcom/squareup/permissions/Permission;->getMobilePermissionString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private hasAnyPermissionForEmployeeLogin(Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)Z"
        }
    .end annotation

    .line 194
    sget-object v0, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 198
    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Permission;

    .line 199
    invoke-direct {p0, v0}, Lcom/squareup/permissions/RealEmployeeManagement;->doesMobileEmployeeHavePermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method private hasAnyPermissionForPasscodeEmployeeManagement(Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)Z"
        }
    .end annotation

    .line 183
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Permission;

    .line 184
    iget-object v1, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v1, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentEmployeeHasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public arePasscodesEnabled()Z
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public canForcePasscodeReentry()Z
    .locals 4

    .line 111
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 112
    sget-object v1, Lcom/squareup/permissions/RealEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 121
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected EmployeeManagementMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_2
    return v2
.end method

.method public checkPermissionForPasscode(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;",
            ">;"
        }
    .end annotation

    .line 231
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 232
    sget-object v1, Lcom/squareup/permissions/RealEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 p1, 0x2

    if-eq v1, p1, :cond_0

    const/4 p1, 0x3

    if-eq v1, p1, :cond_0

    .line 241
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown mode for permissions check: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 238
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Impossible to check passcode in mode "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->passcodeProvidesPermission(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getCurrentEmployee()Lcom/squareup/permissions/Employee;
    .locals 2

    .line 97
    sget-object v0, Lcom/squareup/permissions/RealEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    iget-object v1, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 103
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->getEmployeesEntity()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    .line 102
    invoke-static {v0}, Lcom/squareup/permissions/Employee;->fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;

    move-result-object v0

    return-object v0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/permissions/RealEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 93
    invoke-static {v0}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/permissions/EmployeeInfo;->EMPTY:Lcom/squareup/permissions/EmployeeInfo;

    :goto_0
    return-object v0
.end method

.method public getCurrentEmployeeToken()Ljava/lang/String;
    .locals 2

    .line 79
    sget-object v0, Lcom/squareup/permissions/RealEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    iget-object v1, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getEmployeeSettings()Lcom/squareup/settings/server/EmployeeSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeSettings;->getEmployeeToken()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 151
    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/-$$Lambda$9aZz-3JsbhaZJDu5ZiITOutUyY8;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$9aZz-3JsbhaZJDu5ZiITOutUyY8;

    .line 152
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 155
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 158
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrDefault(Lio/reactivex/Observable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/permissions/Employee;

    return-object p1
.end method

.method public hasAnyPermission(Ljava/util/Set;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)Z"
        }
    .end annotation

    .line 166
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 167
    sget-object v1, Lcom/squareup/permissions/RealEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    const/4 p1, 0x3

    if-ne v1, p1, :cond_0

    return v2

    .line 178
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown mode for permissions check: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 172
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/permissions/RealEmployeeManagement;->hasAnyPermissionForEmployeeLogin(Ljava/util/Set;)Z

    move-result p1

    return p1

    .line 169
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/permissions/RealEmployeeManagement;->hasAnyPermissionForPasscodeEmployeeManagement(Ljava/util/Set;)Z

    move-result p1

    return p1
.end method

.method public hasPermission(Lcom/squareup/permissions/Permission;)Z
    .locals 0

    .line 162
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/permissions/RealEmployeeManagement;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    return p1
.end method

.method public isEldmEnabled()Z
    .locals 3

    .line 126
    sget-object v0, Lcom/squareup/permissions/RealEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    iget-object v1, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    return v1
.end method

.method public isPasscodeLocked()Z
    .locals 2

    .line 274
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 275
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$oneEmployeeProtoByToken$0$RealEmployeeManagement(Lcom/squareup/permissions/Employee;)Lcom/squareup/protos/client/Employee;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 143
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 66
    iget-object p1, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne p1, v0, :cond_1

    .line 67
    iget-object p1, p0, Lcom/squareup/permissions/RealEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "v2"

    goto :goto_0

    :cond_0
    const-string p1, "v3"

    .line 70
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/permissions/RealEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/squareup/permissions/RealEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v2, v0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/permissions/Employee;->roleToken:Ljava/lang/String;

    invoke-static {p1, v2, v0}, Lcom/squareup/permissions/EmployeeManagementEvent;->forEmailLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_1
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onFreeTier()Z
    .locals 1

    .line 281
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSubscriptions()Lcom/squareup/settings/server/Subscriptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/Subscriptions;->hasActiveEmployeeManagementSubscription()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public oneEmployeeProtoByToken(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/protos/client/Employee;",
            ">;"
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    new-instance v0, Lcom/squareup/permissions/-$$Lambda$RealEmployeeManagement$5E1nkTdC_fcZFhgsafaqXkt2mAg;

    invoke-direct {v0, p0}, Lcom/squareup/permissions/-$$Lambda$RealEmployeeManagement$5E1nkTdC_fcZFhgsafaqXkt2mAg;-><init>(Lcom/squareup/permissions/RealEmployeeManagement;)V

    .line 143
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public shouldAskForPasscode()Z
    .locals 4

    .line 213
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 214
    sget-object v1, Lcom/squareup/permissions/RealEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 223
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to check passcode enabled status for mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_2
    return v2
.end method

.method public shouldDisplayFeature(Lcom/squareup/permissions/Permission;)Z
    .locals 4

    .line 248
    sget-object v0, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/Permission;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 253
    sget-object v2, Lcom/squareup/permissions/RealEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$EmployeeManagementModeDecider$Mode:[I

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    if-eq v2, v1, :cond_3

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    const/4 p1, 0x3

    if-ne v2, p1, :cond_1

    goto :goto_0

    .line 263
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown mode for permissions check: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 260
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/permissions/RealEmployeeManagement;->doesMobileEmployeeHavePermission(Lcom/squareup/permissions/Permission;)Z

    move-result p1

    return p1

    :cond_3
    :goto_0
    return v1
.end method

.method public shouldShowClockInButton()Z
    .locals 2

    .line 268
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    .line 269
    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->OWNER:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/RealEmployeeManagement;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 270
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTimecardEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
