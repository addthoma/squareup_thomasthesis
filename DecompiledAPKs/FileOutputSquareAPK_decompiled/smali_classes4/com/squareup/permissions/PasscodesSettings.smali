.class public Lcom/squareup/permissions/PasscodesSettings;
.super Ljava/lang/Object;
.source "PasscodesSettings.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/PasscodesSettings$Timeout;,
        Lcom/squareup/permissions/PasscodesSettings$State;,
        Lcom/squareup/permissions/PasscodesSettings$FailedRequestStateEvent;,
        Lcom/squareup/permissions/PasscodesSettings$SuccessRequestStateEvent;,
        Lcom/squareup/permissions/PasscodesSettings$LoadingRequestStateEvent;,
        Lcom/squareup/permissions/PasscodesSettings$ClearUnsavedOwnerPasscode;,
        Lcom/squareup/permissions/PasscodesSettings$ClearUnsavedTeamPasscode;,
        Lcom/squareup/permissions/PasscodesSettings$RemoveOwnerPasscodeConfirmationDigit;,
        Lcom/squareup/permissions/PasscodesSettings$RemoveOwnerPasscodeDigit;,
        Lcom/squareup/permissions/PasscodesSettings$RemoveTeamPasscodeDigit;,
        Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeConfirmationDigit;,
        Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeDigit;,
        Lcom/squareup/permissions/PasscodesSettings$AddTeamPasscodeDigit;,
        Lcom/squareup/permissions/PasscodesSettings$SaveTeamPasscodeEvent;,
        Lcom/squareup/permissions/PasscodesSettings$TimeoutChangedEvent;,
        Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterLogoutChangedEvent;,
        Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeWhenBackingOutOfASaleChangedEvent;,
        Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterEachSaleChangedEvent;,
        Lcom/squareup/permissions/PasscodesSettings$TeamPasscodeStateChangedEvent;,
        Lcom/squareup/permissions/PasscodesSettings$PasscodesSettingsStateChangedEvent;,
        Lcom/squareup/permissions/PasscodesSettings$Event;,
        Lcom/squareup/permissions/PasscodesSettings$RequestState;,
        Lcom/squareup/permissions/PasscodesSettings$AfterLogOutSetting;,
        Lcom/squareup/permissions/PasscodesSettings$BackingOutOfSaleSetting;
    }
.end annotation


# static fields
.field private static final AFTER_EACH_SALE_DEFAULT:Ljava/lang/Boolean;

.field private static final AFTER_LOG_OUT_DEFAULT:Ljava/lang/Boolean;

.field static final BACKING_OUT_OF_SALE_DEFAULT:Ljava/lang/Boolean;

.field public static final PASSCODE_LENGTH:I = 0x4


# instance fields
.field private accountOwnerEmployee:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation
.end field

.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final afterLogOutSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final backingOutOfSaleSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

.field private final employeesService:Lcom/squareup/server/employees/EmployeesService;

.field private final events:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/permissions/PasscodesSettings$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/permissions/PasscodesSettings$State;",
            ">;"
        }
    .end annotation
.end field

.field private final subs:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x1

    .line 74
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/permissions/PasscodesSettings;->AFTER_EACH_SALE_DEFAULT:Ljava/lang/Boolean;

    const/4 v1, 0x0

    .line 75
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/permissions/PasscodesSettings;->AFTER_LOG_OUT_DEFAULT:Ljava/lang/Boolean;

    .line 77
    sput-object v0, Lcom/squareup/permissions/PasscodesSettings;->BACKING_OUT_OF_SALE_DEFAULT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/server/employees/EmployeesService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lio/reactivex/Scheduler;)V
    .locals 2
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            "Lcom/squareup/server/employees/EmployeesService;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    invoke-direct {v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;-><init>()V

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings;->AFTER_EACH_SALE_DEFAULT:Ljava/lang/Boolean;

    .line 61
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeAfterEachSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings;->BACKING_OUT_OF_SALE_DEFAULT:Ljava/lang/Boolean;

    .line 62
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeWhenBackingOutOfSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings;->AFTER_LOG_OUT_DEFAULT:Ljava/lang/Boolean;

    .line 63
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeAfterLogout(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 64
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object v0

    .line 60
    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 66
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 67
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 71
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->accountOwnerEmployee:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 85
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 86
    iput-object p2, p0, Lcom/squareup/permissions/PasscodesSettings;->employeesService:Lcom/squareup/server/employees/EmployeesService;

    .line 87
    iput-object p3, p0, Lcom/squareup/permissions/PasscodesSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 88
    iput-object p4, p0, Lcom/squareup/permissions/PasscodesSettings;->backingOutOfSaleSetting:Lcom/squareup/settings/LocalSetting;

    .line 89
    iput-object p5, p0, Lcom/squareup/permissions/PasscodesSettings;->afterLogOutSetting:Lcom/squareup/settings/LocalSetting;

    .line 90
    iput-object p6, p0, Lcom/squareup/permissions/PasscodesSettings;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private initState()V
    .locals 5

    .line 107
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isEmployeeManagementAllowed()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 108
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getPasscodeAccess()Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    move-result-object v0

    sget-object v3, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ALWAYS_REQUIRE_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 109
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getPasscodeAccess()Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    move-result-object v0

    sget-object v3, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-ne v0, v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 115
    :goto_0
    iget-object v3, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v3}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isEmployeeManagementAllowed()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 116
    invoke-virtual {v3}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getPasscodeAccess()Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    move-result-object v3

    sget-object v4, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-ne v3, v4, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 118
    :goto_1
    iget-object v2, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getTimeout()Lcom/squareup/settings/server/PasscodeTimeout;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/permissions/PasscodesSettings$Timeout;->fromOldTimeout(Lcom/squareup/settings/server/PasscodeTimeout;)Lcom/squareup/permissions/PasscodesSettings$Timeout;

    move-result-object v2

    .line 120
    iget-object v3, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v4, Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    invoke-direct {v4}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;-><init>()V

    .line 121
    invoke-virtual {v4, v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPasscodesEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    .line 122
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPasscodeEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedTeamPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedOwnerPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedOwnerPasscodeConfirmation()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0, v2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPasscodeTimeout(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 127
    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTransactionLockModeEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeAfterEachSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings;->backingOutOfSaleSetting:Lcom/squareup/settings/LocalSetting;

    sget-object v2, Lcom/squareup/permissions/PasscodesSettings;->BACKING_OUT_OF_SALE_DEFAULT:Ljava/lang/Boolean;

    .line 129
    invoke-interface {v1, v2}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 128
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeWhenBackingOutOfSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings;->afterLogOutSetting:Lcom/squareup/settings/LocalSetting;

    sget-object v2, Lcom/squareup/permissions/PasscodesSettings;->AFTER_LOG_OUT_DEFAULT:Ljava/lang/Boolean;

    .line 130
    invoke-interface {v1, v2}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeAfterLogout(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 131
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 132
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setAccountStatusFields(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object v0

    .line 120
    invoke-virtual {v3, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic lambda$e6oy3hUa3TNm9VYgRtavRI_bAWs(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$Event;Lcom/squareup/permissions/PasscodesSettings$State;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/permissions/PasscodesSettings;->onEvent(Lcom/squareup/permissions/PasscodesSettings$Event;Lcom/squareup/permissions/PasscodesSettings$State;)V

    return-void
.end method

.method private onEvent(Lcom/squareup/permissions/PasscodesSettings$Event;Lcom/squareup/permissions/PasscodesSettings$State;)V
    .locals 3

    .line 137
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 138
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$PasscodesSettingsStateChangedEvent;

    if-eqz v0, :cond_2

    .line 139
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$PasscodesSettingsStateChangedEvent;

    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodesSettings$PasscodesSettingsStateChangedEvent;->shouldEnablePasscodes:Z

    if-eqz p1, :cond_1

    .line 141
    iget-boolean v0, p2, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscodeEnabled:Z

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ALWAYS_REQUIRE_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setPasscodeAccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    goto :goto_0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setPasscodeAccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->NO_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setPasscodeAccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    .line 149
    :goto_0
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 150
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPasscodesEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 149
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 151
    :cond_2
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$TeamPasscodeStateChangedEvent;

    if-eqz v0, :cond_4

    .line 152
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$TeamPasscodeStateChangedEvent;

    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodesSettings$TeamPasscodeStateChangedEvent;->shouldEnableTeamPasscode:Z

    if-eqz p1, :cond_3

    .line 154
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setPasscodeAccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    goto :goto_1

    .line 156
    :cond_3
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ALWAYS_REQUIRE_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setPasscodeAccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    .line 158
    :goto_1
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 159
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPasscodeEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 158
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 160
    :cond_4
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterEachSaleChangedEvent;

    if-eqz v0, :cond_5

    .line 161
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterEachSaleChangedEvent;

    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterEachSaleChangedEvent;->requirePasscodeAfterEachSale:Z

    .line 163
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;->TRANSACTION_LOCK:Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setBoolean(Lcom/squareup/settings/server/EmployeeManagementSettings$FieldName;Z)V

    .line 164
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 165
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    .line 166
    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeAfterEachSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 164
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 167
    :cond_5
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeWhenBackingOutOfASaleChangedEvent;

    if-eqz v0, :cond_6

    .line 168
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeWhenBackingOutOfASaleChangedEvent;

    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeWhenBackingOutOfASaleChangedEvent;->requirePasscodeWhenBackingOutOfASale:Z

    .line 170
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 171
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeWhenBackingOutOfSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p2

    .line 170
    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 172
    iget-object p2, p0, Lcom/squareup/permissions/PasscodesSettings;->backingOutOfSaleSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 173
    :cond_6
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterLogoutChangedEvent;

    if-eqz v0, :cond_7

    .line 174
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterLogoutChangedEvent;

    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterLogoutChangedEvent;->requirePasscodeAfterLogout:Z

    .line 176
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 177
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequirePasscodeAfterLogout(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p2

    .line 176
    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 178
    iget-object p2, p0, Lcom/squareup/permissions/PasscodesSettings;->afterLogOutSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 179
    :cond_7
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$TimeoutChangedEvent;

    if-eqz v0, :cond_8

    .line 180
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$TimeoutChangedEvent;

    iget-object p1, p1, Lcom/squareup/permissions/PasscodesSettings$TimeoutChangedEvent;->timeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    .line 181
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    iget-object v1, p1, Lcom/squareup/permissions/PasscodesSettings$Timeout;->oldTimeoutSetting:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setTimeout(Lcom/squareup/settings/server/PasscodeTimeout;)V

    .line 182
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 183
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPasscodeTimeout(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 182
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 184
    :cond_8
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$AddTeamPasscodeDigit;

    const/4 v1, 0x4

    if-eqz v0, :cond_a

    .line 185
    iget-object v0, p2, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedTeamPasscode:Ljava/lang/String;

    .line 186
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$AddTeamPasscodeDigit;

    iget-char p1, p1, Lcom/squareup/permissions/PasscodesSettings$AddTeamPasscodeDigit;->digit:C

    .line 187
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v1, :cond_9

    .line 188
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 190
    :cond_9
    iget-object p1, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 191
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p2

    .line 190
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 192
    :cond_a
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeDigit;

    if-eqz v0, :cond_c

    .line 193
    iget-object v0, p2, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscode:Ljava/lang/String;

    .line 194
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeDigit;

    iget-char p1, p1, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeDigit;->digit:C

    .line 195
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v1, :cond_b

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_b
    iget-object p1, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 199
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedOwnerPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p2

    .line 198
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 200
    :cond_c
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeConfirmationDigit;

    if-eqz v0, :cond_d

    .line 201
    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeConfirmationDigit;

    iget-char p1, p1, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeConfirmationDigit;->digit:C

    .line 202
    iget-object v0, p2, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 204
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 205
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    .line 206
    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedOwnerPasscodeConfirmation(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 204
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 207
    :cond_d
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$RemoveTeamPasscodeDigit;

    const/4 v2, 0x0

    if-eqz v0, :cond_e

    .line 208
    iget-object p1, p2, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedTeamPasscode:Ljava/lang/String;

    .line 209
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_17

    .line 210
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 211
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 212
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 211
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 214
    :cond_e
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$RemoveOwnerPasscodeDigit;

    if-eqz v0, :cond_f

    .line 215
    iget-object p1, p2, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscode:Ljava/lang/String;

    .line 216
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_17

    .line 217
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 218
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 219
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedOwnerPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 218
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 221
    :cond_f
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$RemoveOwnerPasscodeConfirmationDigit;

    if-eqz v0, :cond_10

    .line 222
    iget-object p1, p2, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    .line 223
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_17

    .line 225
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 224
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 226
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 227
    invoke-virtual {p2, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    .line 228
    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setUnsavedOwnerPasscodeConfirmation(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 226
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 230
    :cond_10
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$SaveTeamPasscodeEvent;

    if-eqz v0, :cond_12

    .line 231
    iget-object p1, p2, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedTeamPasscode:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-ne p1, v1, :cond_11

    .line 233
    iget-object p1, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->LOADING:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 234
    invoke-direct {p0, p2}, Lcom/squareup/permissions/PasscodesSettings;->setTeamPasscode(Lcom/squareup/permissions/PasscodesSettings$State;)V

    goto/16 :goto_2

    .line 236
    :cond_11
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "In order to save team passcode, unsavedTeamPasscode.length should be equal to expected passcode length i.e. 4, currentLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 240
    :cond_12
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$ClearUnsavedTeamPasscode;

    if-eqz v0, :cond_13

    .line 241
    iget-object p1, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v0, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 242
    invoke-virtual {p2, v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    .line 243
    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedTeamPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    .line 244
    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p2

    .line 241
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 245
    :cond_13
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$ClearUnsavedOwnerPasscode;

    if-eqz v0, :cond_14

    .line 246
    iget-object p1, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v0, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 247
    invoke-virtual {p2, v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    .line 248
    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedOwnerPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    .line 249
    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedOwnerPasscodeConfirmation()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    .line 250
    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p2

    .line 246
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_2

    .line 251
    :cond_14
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$LoadingRequestStateEvent;

    if-eqz v0, :cond_15

    .line 252
    iget-object p1, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v0, Lcom/squareup/permissions/PasscodesSettings$RequestState;->LOADING:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-virtual {p2, v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_2

    .line 253
    :cond_15
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$SuccessRequestStateEvent;

    if-eqz v0, :cond_16

    .line 254
    iget-object p1, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    sget-object v0, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-virtual {p2, v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_2

    .line 255
    :cond_16
    instance-of v0, p1, Lcom/squareup/permissions/PasscodesSettings$FailedRequestStateEvent;

    if-eqz v0, :cond_17

    .line 256
    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p2

    check-cast p1, Lcom/squareup/permissions/PasscodesSettings$FailedRequestStateEvent;

    iget-object p1, p1, Lcom/squareup/permissions/PasscodesSettings$FailedRequestStateEvent;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 257
    invoke-virtual {p2, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 258
    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedTeamPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 259
    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedOwnerPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 260
    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedOwnerPasscodeConfirmation()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 261
    iget-object p2, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_17
    :goto_2
    return-void
.end method

.method private setTeamPasscode(Lcom/squareup/permissions/PasscodesSettings$State;)V
    .locals 4

    .line 266
    iget-object v0, p1, Lcom/squareup/permissions/PasscodesSettings$State;->teamRoleToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedTeamPasscode:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/server/employees/SetPasscodeRequest;->createSetRolePasscodeRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/employees/SetPasscodeRequest;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/permissions/PasscodesSettings;->employeesService:Lcom/squareup/server/employees/EmployeesService;

    invoke-interface {v2, v0}, Lcom/squareup/server/employees/EmployeesService;->setPasscode(Lcom/squareup/server/employees/SetPasscodeRequest;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/permissions/PasscodesSettings;->mainScheduler:Lio/reactivex/Scheduler;

    .line 269
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 270
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$g72bvcl4kzOdBIrIS86UrkKhpOE;

    invoke-direct {v2, p0, p1}, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$g72bvcl4kzOdBIrIS86UrkKhpOE;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$State;)V

    .line 271
    invoke-static {v2}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    new-instance v3, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$rDBkTIhUxWTqGjbH-gBG1Tw_oqc;

    invoke-direct {v3, p0, p1}, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$rDBkTIhUxWTqGjbH-gBG1Tw_oqc;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$State;)V

    invoke-virtual {v0, v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 268
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method public addOwnerPasscodeConfirmationDigit(C)V
    .locals 2

    .line 356
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeConfirmationDigit;

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeConfirmationDigit;-><init>(Lcom/squareup/permissions/PasscodesSettings;C)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public addOwnerPasscodeDigit(C)V
    .locals 2

    .line 352
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeDigit;

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$AddOwnerPasscodeDigit;-><init>(Lcom/squareup/permissions/PasscodesSettings;C)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public addTeamPasscodeDigit(C)V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$AddTeamPasscodeDigit;

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$AddTeamPasscodeDigit;-><init>(Lcom/squareup/permissions/PasscodesSettings;C)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public changeTimeout(Lcom/squareup/permissions/PasscodesSettings$Timeout;)V
    .locals 2

    .line 340
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$TimeoutChangedEvent;

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$TimeoutChangedEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$Timeout;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public clearUnsavedOwnerPasscode()V
    .locals 3

    .line 376
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$ClearUnsavedOwnerPasscode;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/permissions/PasscodesSettings$ClearUnsavedOwnerPasscode;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public clearUnsavedTeamPasscode()V
    .locals 3

    .line 372
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$ClearUnsavedTeamPasscode;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/permissions/PasscodesSettings$ClearUnsavedTeamPasscode;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public enableOrDisablePasscodes(Ljava/lang/Boolean;)V
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$PasscodesSettingsStateChangedEvent;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$PasscodesSettingsStateChangedEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Z)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public enableOrDisableRequirePasscodeAfterEachSale(Ljava/lang/Boolean;)V
    .locals 2

    .line 326
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterEachSaleChangedEvent;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterEachSaleChangedEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Z)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public enableOrDisableRequirePasscodeAfterLogout(Ljava/lang/Boolean;)V
    .locals 2

    .line 336
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterLogoutChangedEvent;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeAfterLogoutChangedEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Z)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public enableOrDisableRequirePasscodeWhenBackingOutOfASale(Ljava/lang/Boolean;)V
    .locals 2

    .line 331
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeWhenBackingOutOfASaleChangedEvent;

    .line 332
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$RequirePasscodeWhenBackingOutOfASaleChangedEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Z)V

    .line 331
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public enableOrDisableTeamPasscode(Ljava/lang/Boolean;)V
    .locals 2

    .line 322
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$TeamPasscodeStateChangedEvent;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$TeamPasscodeStateChangedEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Z)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public getAccountOwnerEmployee()Lcom/squareup/permissions/Employee;
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->accountOwnerEmployee:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Employee;

    return-object v0
.end method

.method public getLatestState()Lcom/squareup/permissions/PasscodesSettings$State;
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/PasscodesSettings$State;

    return-object v0
.end method

.method public isTeamPasscodeEnabled(Lcom/squareup/permissions/PasscodesSettings$State;)Z
    .locals 1

    .line 388
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodesSettings;->isTeamPermissionsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscodeEnabled:Z

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isTeamPermissionsEnabled()Z
    .locals 1

    .line 384
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$setTeamPasscode$0$PasscodesSettings(Lcom/squareup/permissions/PasscodesSettings$State;Lcom/squareup/server/employees/SetPasscodeResponse;Lcom/squareup/permissions/PasscodesSettings$State;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 272
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/EmployeeManagementSettings;->setPasscodeAccess(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    .line 273
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p3}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p3

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->SUCCESS:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 274
    invoke-virtual {p3, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p3

    .line 275
    invoke-virtual {p3}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedTeamPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p3

    const/4 v1, 0x1

    .line 276
    invoke-virtual {p3, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPasscodeEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p3

    iget-object p1, p1, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    .line 277
    invoke-virtual {p3, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPreviousTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 278
    invoke-virtual {p2}, Lcom/squareup/server/employees/SetPasscodeResponse;->passcode()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 279
    invoke-virtual {p1, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPermissionRoleHasSharedPosAccess(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 280
    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 273
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$setTeamPasscode$1$PasscodesSettings(Lcom/squareup/permissions/PasscodesSettings$State;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 283
    instance-of v0, p2, Lretrofit/RetrofitError;

    if-eqz v0, :cond_1

    .line 284
    check-cast p2, Lretrofit/RetrofitError;

    .line 285
    invoke-virtual {p2}, Lretrofit/RetrofitError;->getBody()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/server/employees/SetPasscodeResponse;

    .line 286
    invoke-virtual {p2}, Lcom/squareup/server/employees/SetPasscodeResponse;->duplicatePasscode()Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/permissions/PasscodesSettings$RequestState;->FAILED_NOT_UNIQUE:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/permissions/PasscodesSettings$RequestState;->FAILED_ERROR:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    goto :goto_0

    .line 288
    :cond_1
    sget-object p2, Lcom/squareup/permissions/PasscodesSettings$RequestState;->FAILED_ERROR:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 290
    :goto_0
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State;->builder()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 291
    invoke-virtual {p1, p2}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 292
    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->clearUnsavedTeamPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    move-result-object p1

    .line 293
    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->build()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 290
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 94
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodesSettings;->initState()V

    .line 95
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 96
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$e6oy3hUa3TNm9VYgRtavRI_bAWs;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$e6oy3hUa3TNm9VYgRtavRI_bAWs;-><init>(Lcom/squareup/permissions/PasscodesSettings;)V

    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 95
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public removeOwnerPasscodeConfirmationDigit()V
    .locals 3

    .line 368
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$RemoveOwnerPasscodeConfirmationDigit;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/permissions/PasscodesSettings$RemoveOwnerPasscodeConfirmationDigit;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public removeOwnerPasscodeDigit()V
    .locals 3

    .line 364
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$RemoveOwnerPasscodeDigit;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/permissions/PasscodesSettings$RemoveOwnerPasscodeDigit;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public removeTeamPasscodeDigit()V
    .locals 3

    .line 360
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$RemoveTeamPasscodeDigit;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/permissions/PasscodesSettings$RemoveTeamPasscodeDigit;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public saveTeamPasscode()V
    .locals 3

    .line 344
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$SaveTeamPasscodeEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/permissions/PasscodesSettings$SaveTeamPasscodeEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setAccountOwnerEmployee(Lcom/squareup/permissions/Employee;)V
    .locals 1

    .line 398
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->accountOwnerEmployee:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setFailedRequest(Lcom/squareup/permissions/PasscodesSettings$RequestState;)V
    .locals 2

    .line 314
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$FailedRequestStateEvent;

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/PasscodesSettings$FailedRequestStateEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$RequestState;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setLoadingRequestState()V
    .locals 3

    .line 306
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$LoadingRequestStateEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/permissions/PasscodesSettings$LoadingRequestStateEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setSuccessRequestState()V
    .locals 3

    .line 310
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->events:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/permissions/PasscodesSettings$SuccessRequestStateEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/permissions/PasscodesSettings$SuccessRequestStateEvent;-><init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public state()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/permissions/PasscodesSettings$State;",
            ">;"
        }
    .end annotation

    .line 298
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings;->state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method
