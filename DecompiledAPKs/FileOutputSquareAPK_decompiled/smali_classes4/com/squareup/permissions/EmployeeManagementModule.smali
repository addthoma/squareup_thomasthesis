.class public abstract Lcom/squareup/permissions/EmployeeManagementModule;
.super Ljava/lang/Object;
.source "EmployeeManagementModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field static final PASSCODE_EMPLOYEE_MANAGEMENT_ENABLED:Ljava/lang/String; = "passcode_employee_management_enabled"

.field static final TIMECARD_ENABLED:Ljava/lang/String; = "timecard_enabled"

.field static final TRANSACTION_LOCK_MODE_ENABLED:Ljava/lang/String; = "transaction_lock_mode_enabled"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static providePasscodeEmployeeManagementEnabledSetting(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "passcode_employee_management_enabled"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideTimecardEnabledSetting(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "timecard_enabled"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method

.method static provideTransactionLockModeEnabledSetting(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/settings/BooleanLocalSetting;

    const-string v1, "transaction_lock_mode_enabled"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/settings/BooleanLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method abstract provideEmployeeManagement(Lcom/squareup/permissions/RealEmployeeManagement;)Lcom/squareup/permissions/EmployeeManagement;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEmployeeStore(Lcom/squareup/permissions/SqliteEmployeesStore;)Lcom/squareup/permissions/EmployeesStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
