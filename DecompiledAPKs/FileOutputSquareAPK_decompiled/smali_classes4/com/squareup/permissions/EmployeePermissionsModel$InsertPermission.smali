.class public final Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;
.super Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;
.source "EmployeePermissionsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/EmployeePermissionsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InsertPermission"
.end annotation


# direct methods
.method public constructor <init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V
    .locals 1

    const-string v0, "INSERT INTO employee_permissions_table (employee_token, permission)\nVALUES (?, ?)"

    .line 65
    invoke-interface {p1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteStatement;

    move-result-object p1

    const-string v0, "employee_permissions_table"

    invoke-direct {p0, v0, p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;-><init>(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)V

    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 71
    invoke-virtual {p0, v0, p1}, Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;->bindString(ILjava/lang/String;)V

    const/4 p1, 0x2

    if-nez p2, :cond_0

    .line 73
    invoke-virtual {p0, p1}, Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;->bindNull(I)V

    goto :goto_0

    .line 75
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/permissions/EmployeePermissionsModel$InsertPermission;->bindString(ILjava/lang/String;)V

    :goto_0
    return-void
.end method
