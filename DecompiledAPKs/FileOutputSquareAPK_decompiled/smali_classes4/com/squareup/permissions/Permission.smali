.class public final enum Lcom/squareup/permissions/Permission;
.super Ljava/lang/Enum;
.source "Permission.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/permissions/Permission;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/permissions/Permission;

.field public static final enum ACCESS_NOTIFICATION_CENTER:Lcom/squareup/permissions/Permission;

.field public static final enum ALLOW_APP_SIGN_OUT:Lcom/squareup/permissions/Permission;

.field public static final enum CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

.field public static final enum CARD_ON_FILE:Lcom/squareup/permissions/Permission;

.field public static final enum CHANGE_OFFLINE_MODE_SETTING:Lcom/squareup/permissions/Permission;

.field public static final enum CREATE_BANK_ACCOUNT:Lcom/squareup/permissions/Permission;

.field public static final enum DEPOSITS_APPLET:Lcom/squareup/permissions/Permission;

.field public static final enum DETAILED_SALES_REPORTS:Lcom/squareup/permissions/Permission;

.field public static final enum EDIT_ITEMS:Lcom/squareup/permissions/Permission;

.field public static final enum EDIT_SURCHARGES_IN_SALE:Lcom/squareup/permissions/Permission;

.field public static final enum EDIT_TAXES_IN_SALE:Lcom/squareup/permissions/Permission;

.field public static final enum EMPLOYEE_ACCESS_REGISTER:Lcom/squareup/permissions/Permission;

.field public static final enum END_TIMECARD_BREAK_EARLY:Lcom/squareup/permissions/Permission;

.field public static final enum FULL_SALES_HISTORY:Lcom/squareup/permissions/Permission;

.field public static final enum INVOICES_APPLET:Lcom/squareup/permissions/Permission;

.field public static final enum IPOS_MANAGE_NOTIFICATIONS:Lcom/squareup/permissions/Permission;

.field public static final enum ISSUE_REFUNDS:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_ALL_CALENDARS:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_APPOINTMENTS_BUSINESS_PROFILE:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_BALANCE_SETTINGS:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_DEVICES:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_DEVICE_SECURITY:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_DISPUTES:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_EMPLOYEES:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_HARDWARE_SETTINGS:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_ORDERS:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_ORDER_SETTINGS:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_RECEIPT_INFORMATION:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_SQUARE_CARD:Lcom/squareup/permissions/Permission;

.field public static final enum MANAGE_TIME_TRACKING_SETTINGS:Lcom/squareup/permissions/Permission;

.field public static final enum OPEN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

.field public static final enum OPEN_TICKET_MANAGE_ALL:Lcom/squareup/permissions/Permission;

.field public static final enum OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

.field public static final enum REOPEN_CHECKS:Lcom/squareup/permissions/Permission;

.field public static final enum SETTINGS:Lcom/squareup/permissions/Permission;

.field public static final enum SETTLE_ALL_TIPS:Lcom/squareup/permissions/Permission;

.field public static final enum SUMMARIES_AND_REPORTS:Lcom/squareup/permissions/Permission;

.field public static final enum UPDATE_LOYALTY:Lcom/squareup/permissions/Permission;

.field public static final enum USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

.field public static final enum USE_PASSCODE_RESTRICTED_DISCOUNT:Lcom/squareup/permissions/Permission;

.field public static final enum VIEW_BANK_ACCOUNT_INFORMATION:Lcom/squareup/permissions/Permission;

.field public static final enum VIEW_CASH_DRAWER_REPORTS:Lcom/squareup/permissions/Permission;

.field public static final enum VIEW_EXPECTED_IN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

.field public static final enum VIEW_LOYALTY_REPORT:Lcom/squareup/permissions/Permission;

.field public static final enum VIEW_SALES_REPORTS:Lcom/squareup/permissions/Permission;


# instance fields
.field private final permissionString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 12
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/4 v1, 0x0

    const-string v2, "EDIT_ITEMS"

    const-string v3, "employee_edit_items"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    .line 13
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/4 v2, 0x1

    const-string v3, "EMPLOYEE_ACCESS_REGISTER"

    const-string v4, "employee_access_register"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->EMPLOYEE_ACCESS_REGISTER:Lcom/squareup/permissions/Permission;

    .line 14
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/4 v3, 0x2

    const-string v4, "FULL_SALES_HISTORY"

    const-string v5, "employee_access_sales_history"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->FULL_SALES_HISTORY:Lcom/squareup/permissions/Permission;

    .line 15
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/4 v4, 0x3

    const-string v5, "SETTLE_ALL_TIPS"

    const-string v6, "employee_settle_all_payments_awaiting_tip"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->SETTLE_ALL_TIPS:Lcom/squareup/permissions/Permission;

    .line 16
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/4 v5, 0x4

    const-string v6, "ISSUE_REFUNDS"

    const-string v7, "employee_issue_refunds"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->ISSUE_REFUNDS:Lcom/squareup/permissions/Permission;

    .line 18
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/4 v6, 0x5

    const-string v7, "OPEN_TICKET_VOID"

    const-string v8, "employee_void_open_tickets"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    .line 19
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/4 v7, 0x6

    const-string v8, "OPEN_TICKET_MANAGE_ALL"

    const-string v9, "employee_manage_all_open_tickets"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->OPEN_TICKET_MANAGE_ALL:Lcom/squareup/permissions/Permission;

    .line 20
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/4 v8, 0x7

    const-string v9, "SETTINGS"

    const-string v10, "employee_change_register_settings"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    .line 21
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/16 v9, 0x8

    const-string v10, "USE_INSTANT_DEPOSIT"

    const-string v11, "employee_use_instant_deposit"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

    .line 26
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/16 v10, 0x9

    const-string v11, "CANCEL_BUYER_FLOW"

    const-string v12, "employee_cancel_transaction"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    .line 27
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/16 v11, 0xa

    const-string v12, "SUMMARIES_AND_REPORTS"

    const-string v13, "employee_view_summaries_reports"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->SUMMARIES_AND_REPORTS:Lcom/squareup/permissions/Permission;

    .line 28
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/16 v12, 0xb

    const-string v13, "VIEW_SALES_REPORTS"

    const-string v14, "employee_view_sales_reports"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->VIEW_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    .line 29
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/16 v13, 0xc

    const-string v14, "VIEW_CASH_DRAWER_REPORTS"

    const-string v15, "employee_view_cash_drawer_reports"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->VIEW_CASH_DRAWER_REPORTS:Lcom/squareup/permissions/Permission;

    .line 30
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/16 v14, 0xd

    const-string v15, "OPEN_CASH_DRAWER"

    const-string v13, "employee_open_cash_drawer_outside_sale"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->OPEN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    .line 31
    new-instance v0, Lcom/squareup/permissions/Permission;

    const/16 v13, 0xe

    const-string v15, "VIEW_EXPECTED_IN_CASH_DRAWER"

    const-string v14, "employee_view_expected_in_drawer"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->VIEW_EXPECTED_IN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    .line 32
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v14, "USE_PASSCODE_RESTRICTED_DISCOUNT"

    const/16 v15, 0xf

    const-string v13, "employee_apply_restricted_discounts"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->USE_PASSCODE_RESTRICTED_DISCOUNT:Lcom/squareup/permissions/Permission;

    .line 33
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "CHANGE_OFFLINE_MODE_SETTING"

    const/16 v14, 0x10

    const-string v15, "employee_toggle_offline_mode"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->CHANGE_OFFLINE_MODE_SETTING:Lcom/squareup/permissions/Permission;

    .line 34
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_CUSTOMERS"

    const/16 v14, 0x11

    const-string v15, "employee_manage_customers"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    .line 35
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "CARD_ON_FILE"

    const/16 v14, 0x12

    const-string v15, "employee_use_card_on_file"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->CARD_ON_FILE:Lcom/squareup/permissions/Permission;

    .line 36
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "UPDATE_LOYALTY"

    const/16 v14, 0x13

    const-string v15, "employee_adjust_punches"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->UPDATE_LOYALTY:Lcom/squareup/permissions/Permission;

    .line 37
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_DEVICE_SECURITY"

    const/16 v14, 0x14

    const-string v15, "employee_manage_device_security"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_DEVICE_SECURITY:Lcom/squareup/permissions/Permission;

    .line 38
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_DISPUTES"

    const/16 v14, 0x15

    const-string v15, "employee_manage_disputes"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_DISPUTES:Lcom/squareup/permissions/Permission;

    .line 39
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_EMPLOYEES"

    const/16 v14, 0x16

    const-string v15, "employee_manage_employees"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_EMPLOYEES:Lcom/squareup/permissions/Permission;

    .line 40
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "EDIT_SURCHARGES_IN_SALE"

    const/16 v14, 0x17

    const-string v15, "employee_edit_surcharges_in_sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->EDIT_SURCHARGES_IN_SALE:Lcom/squareup/permissions/Permission;

    .line 41
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "EDIT_TAXES_IN_SALE"

    const/16 v14, 0x18

    const-string v15, "employee_edit_taxes_in_sale"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->EDIT_TAXES_IN_SALE:Lcom/squareup/permissions/Permission;

    .line 42
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "INVOICES_APPLET"

    const/16 v14, 0x19

    const-string v15, "employee_use_invoices_applet"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->INVOICES_APPLET:Lcom/squareup/permissions/Permission;

    .line 43
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "CREATE_BANK_ACCOUNT"

    const/16 v14, 0x1a

    const-string v15, "employee_create_bank_account"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->CREATE_BANK_ACCOUNT:Lcom/squareup/permissions/Permission;

    .line 44
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "DEPOSITS_APPLET"

    const/16 v14, 0x1b

    const-string v15, "employee_view_deposits_and_square_card"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->DEPOSITS_APPLET:Lcom/squareup/permissions/Permission;

    .line 45
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_SQUARE_CARD"

    const/16 v14, 0x1c

    const-string v15, "employee_view_square_card_management"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_SQUARE_CARD:Lcom/squareup/permissions/Permission;

    .line 46
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "REOPEN_CHECKS"

    const/16 v14, 0x1d

    const-string v15, "employee_reopen_checks"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->REOPEN_CHECKS:Lcom/squareup/permissions/Permission;

    .line 47
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "IPOS_MANAGE_NOTIFICATIONS"

    const/16 v14, 0x1e

    const-string v15, "employee_manage_invoice_notifications"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->IPOS_MANAGE_NOTIFICATIONS:Lcom/squareup/permissions/Permission;

    .line 48
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_ORDERS"

    const/16 v14, 0x1f

    const-string v15, "employee_manage_orders"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_ORDERS:Lcom/squareup/permissions/Permission;

    .line 49
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_ALL_CALENDARS"

    const/16 v14, 0x20

    const-string v15, "employee_manage_all_calendars"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_ALL_CALENDARS:Lcom/squareup/permissions/Permission;

    .line 50
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_APPOINTMENTS_BUSINESS_PROFILE"

    const/16 v14, 0x21

    const-string v15, "employee_manage_appointments_business_profile"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_APPOINTMENTS_BUSINESS_PROFILE:Lcom/squareup/permissions/Permission;

    .line 51
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_RECEIPT_INFORMATION"

    const/16 v14, 0x22

    const-string v15, "employee_manage_receipt_information"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_RECEIPT_INFORMATION:Lcom/squareup/permissions/Permission;

    .line 52
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_TIME_TRACKING_SETTINGS"

    const/16 v14, 0x23

    const-string v15, "employee_manage_time_tracking_settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_TIME_TRACKING_SETTINGS:Lcom/squareup/permissions/Permission;

    .line 53
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_HARDWARE_SETTINGS"

    const/16 v14, 0x24

    const-string v15, "employee_manage_hardware_settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_HARDWARE_SETTINGS:Lcom/squareup/permissions/Permission;

    .line 54
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_CHECKOUT_SETTINGS"

    const/16 v14, 0x25

    const-string v15, "employee_manage_checkout_settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    .line 55
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_ORDER_SETTINGS"

    const/16 v14, 0x26

    const-string v15, "employee_manage_order_settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_ORDER_SETTINGS:Lcom/squareup/permissions/Permission;

    .line 56
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "VIEW_BANK_ACCOUNT_INFORMATION"

    const/16 v14, 0x27

    const-string v15, "employee_view_bank_account_information"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->VIEW_BANK_ACCOUNT_INFORMATION:Lcom/squareup/permissions/Permission;

    .line 57
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_BALANCE_SETTINGS"

    const/16 v14, 0x28

    const-string v15, "employee_manage_balance_settings"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_BALANCE_SETTINGS:Lcom/squareup/permissions/Permission;

    .line 58
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "MANAGE_DEVICES"

    const/16 v14, 0x29

    const-string v15, "employee_manage_devices"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->MANAGE_DEVICES:Lcom/squareup/permissions/Permission;

    .line 59
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "ALLOW_APP_SIGN_OUT"

    const/16 v14, 0x2a

    const-string v15, "employee_app_sign_out"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->ALLOW_APP_SIGN_OUT:Lcom/squareup/permissions/Permission;

    .line 62
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "DETAILED_SALES_REPORTS"

    const/16 v14, 0x2b

    const-string v15, "employee_view_detailed_sales_reports"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->DETAILED_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    .line 63
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "VIEW_LOYALTY_REPORT"

    const/16 v14, 0x2c

    const-string v15, "employee_view_loyalty_reports"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->VIEW_LOYALTY_REPORT:Lcom/squareup/permissions/Permission;

    .line 64
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "END_TIMECARD_BREAK_EARLY"

    const/16 v14, 0x2d

    const-string v15, "employee_end_timecard_break_early"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->END_TIMECARD_BREAK_EARLY:Lcom/squareup/permissions/Permission;

    .line 65
    new-instance v0, Lcom/squareup/permissions/Permission;

    const-string v13, "ACCESS_NOTIFICATION_CENTER"

    const/16 v14, 0x2e

    const-string v15, "access_notification_center"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/permissions/Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/permissions/Permission;->ACCESS_NOTIFICATION_CENTER:Lcom/squareup/permissions/Permission;

    const/16 v0, 0x2f

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 11
    sget-object v13, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/permissions/Permission;->EMPLOYEE_ACCESS_REGISTER:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->FULL_SALES_HISTORY:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/permissions/Permission;->SETTLE_ALL_TIPS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/permissions/Permission;->ISSUE_REFUNDS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_MANAGE_ALL:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/permissions/Permission;->SUMMARIES_AND_REPORTS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_CASH_DRAWER_REPORTS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_EXPECTED_IN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_PASSCODE_RESTRICTED_DISCOUNT:Lcom/squareup/permissions/Permission;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->CHANGE_OFFLINE_MODE_SETTING:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->CARD_ON_FILE:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->UPDATE_LOYALTY:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_DEVICE_SECURITY:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_DISPUTES:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_EMPLOYEES:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->EDIT_SURCHARGES_IN_SALE:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->EDIT_TAXES_IN_SALE:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->INVOICES_APPLET:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->CREATE_BANK_ACCOUNT:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->DEPOSITS_APPLET:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_SQUARE_CARD:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->REOPEN_CHECKS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->IPOS_MANAGE_NOTIFICATIONS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_ORDERS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_ALL_CALENDARS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_APPOINTMENTS_BUSINESS_PROFILE:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_RECEIPT_INFORMATION:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_TIME_TRACKING_SETTINGS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_HARDWARE_SETTINGS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_ORDER_SETTINGS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_BANK_ACCOUNT_INFORMATION:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_BALANCE_SETTINGS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_DEVICES:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->ALLOW_APP_SIGN_OUT:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->DETAILED_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_LOYALTY_REPORT:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->END_TIMECARD_BREAK_EARLY:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/permissions/Permission;->ACCESS_NOTIFICATION_CENTER:Lcom/squareup/permissions/Permission;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/permissions/Permission;->$VALUES:[Lcom/squareup/permissions/Permission;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 70
    iput-object p3, p0, Lcom/squareup/permissions/Permission;->permissionString:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/permissions/Permission;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/permissions/Permission;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/permissions/Permission;

    return-object p0
.end method

.method public static values()[Lcom/squareup/permissions/Permission;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/permissions/Permission;->$VALUES:[Lcom/squareup/permissions/Permission;

    invoke-virtual {v0}, [Lcom/squareup/permissions/Permission;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/permissions/Permission;

    return-object v0
.end method


# virtual methods
.method public getMobilePermissionString()Ljava/lang/String;
    .locals 4

    .line 80
    sget-object v0, Lcom/squareup/permissions/Permission;->MANAGE_ALL_CALENDARS:Lcom/squareup/permissions/Permission;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/permissions/Permission;->MANAGE_APPOINTMENTS_BUSINESS_PROFILE:Lcom/squareup/permissions/Permission;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/permissions/Permission;->ACCESS_NOTIFICATION_CENTER:Lcom/squareup/permissions/Permission;

    if-ne p0, v0, :cond_0

    goto :goto_0

    .line 85
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/permissions/Permission;->permissionString:Ljava/lang/String;

    aput-object v3, v1, v2

    const-string v2, "mobile_%s"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 83
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/permissions/Permission;->permissionString:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissionString()Ljava/lang/String;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/permissions/Permission;->permissionString:Ljava/lang/String;

    return-object v0
.end method
