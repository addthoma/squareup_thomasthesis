.class public final Lcom/squareup/permissions/EmployeePermissionsModel$Factory;
.super Ljava/lang/Object;
.source "EmployeePermissionsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/EmployeePermissionsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/permissions/EmployeePermissionsModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final creator:Lcom/squareup/permissions/EmployeePermissionsModel$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/permissions/EmployeePermissionsModel$Creator<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/EmployeePermissionsModel$Creator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/permissions/EmployeePermissionsModel$Creator<",
            "TT;>;)V"
        }
    .end annotation

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/permissions/EmployeePermissionsModel$Factory;->creator:Lcom/squareup/permissions/EmployeePermissionsModel$Creator;

    return-void
.end method
