.class public Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;
.super Ljava/lang/Object;
.source "ReferralRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/referrals/ReferralRunner$ReferralData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private balance:Ljava/lang/CharSequence;

.field private expiresAt:Ljava/lang/CharSequence;

.field private freeProcessingNote:Ljava/lang/CharSequence;

.field private freeProcessingTitle:Ljava/lang/CharSequence;

.field private prettyUrl:Ljava/lang/String;

.field private final response:Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;)V
    .locals 0

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->response:Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/referrals/ReferralRunner$ReferralData;
    .locals 10

    .line 248
    new-instance v9, Lcom/squareup/referrals/ReferralRunner$ReferralData;

    iget-object v2, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->response:Lcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;

    iget-object v3, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->balance:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->expiresAt:Ljava/lang/CharSequence;

    iget-object v5, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->freeProcessingTitle:Ljava/lang/CharSequence;

    iget-object v6, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->freeProcessingNote:Ljava/lang/CharSequence;

    iget-object v7, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->prettyUrl:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/referrals/ReferralRunner$ReferralData;-><init>(ZLcom/squareup/protos/client/onboard/ReferrerCurrentSignupTokenResponse;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/squareup/referrals/ReferralRunner$1;)V

    return-object v9
.end method

.method public setBalance(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->balance:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setExpiresAt(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->expiresAt:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setFreeProcessingNote(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->freeProcessingNote:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setFreeProcessingTitle(Ljava/lang/CharSequence;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->freeProcessingTitle:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setPrettyUrl(Ljava/lang/String;)Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner$ReferralData$Builder;->prettyUrl:Ljava/lang/String;

    return-object p0
.end method
