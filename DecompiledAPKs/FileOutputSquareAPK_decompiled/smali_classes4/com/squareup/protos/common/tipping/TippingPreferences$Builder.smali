.class public final Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TippingPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/tipping/TippingPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/tipping/TippingPreferences;",
        "Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_manual_tip_entry:Ljava/lang/Boolean;

.field public client_calculated_tip_option:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

.field public manual_tip_entry_max_percentage:Ljava/lang/Double;

.field public manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

.field public smart_tipping_over_threshold_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

.field public smart_tipping_under_threshold_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public tipping_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public use_smart_tipping:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 277
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 278
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    .line 279
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    .line 280
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->tipping_options:Ljava/util/List;

    .line 281
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->client_calculated_tip_option:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0

    .line 340
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->allow_manual_tip_entry:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/tipping/TippingPreferences;
    .locals 13

    .line 391
    new-instance v12, Lcom/squareup/protos/common/tipping/TippingPreferences;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->use_smart_tipping:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->tipping_options:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->allow_manual_tip_entry:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    iget-object v8, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    iget-object v9, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    iget-object v10, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->client_calculated_tip_option:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/common/tipping/TippingPreferences;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Ljava/util/List;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 256
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->build()Lcom/squareup/protos/common/tipping/TippingPreferences;

    move-result-object v0

    return-object v0
.end method

.method public client_calculated_tip_option(Ljava/util/List;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;)",
            "Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;"
        }
    .end annotation

    .line 384
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 385
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->client_calculated_tip_option:Ljava/util/List;

    return-object p0
.end method

.method public manual_tip_entry_largest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public manual_tip_entry_max_percentage(Ljava/lang/Double;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    return-object p0
.end method

.method public manual_tip_entry_smallest_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public smart_tipping_over_threshold_options(Ljava/util/List;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;)",
            "Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;"
        }
    .end annotation

    .line 319
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 320
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    return-object p0
.end method

.method public smart_tipping_threshold_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public smart_tipping_under_threshold_options(Ljava/util/List;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;)",
            "Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;"
        }
    .end annotation

    .line 308
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 309
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    return-object p0
.end method

.method public tipping_options(Ljava/util/List;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;)",
            "Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;"
        }
    .end annotation

    .line 331
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 332
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->tipping_options:Ljava/util/List;

    return-object p0
.end method

.method public use_smart_tipping(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->use_smart_tipping:Ljava/lang/Boolean;

    return-object p0
.end method
