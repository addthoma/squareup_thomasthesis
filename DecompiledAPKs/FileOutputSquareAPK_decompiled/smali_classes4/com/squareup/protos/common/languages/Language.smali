.class public final enum Lcom/squareup/protos/common/languages/Language;
.super Ljava/lang/Enum;
.source "Language.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/languages/Language$ProtoAdapter_Language;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/languages/Language;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/languages/Language;

.field public static final enum AA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AB:Lcom/squareup/protos/common/languages/Language;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/languages/Language;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AF:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AK:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AM:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AY:Lcom/squareup/protos/common/languages/Language;

.field public static final enum AZ:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BM:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum BS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CH:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum CY:Lcom/squareup/protos/common/languages/Language;

.field public static final enum DA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum DE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum DV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum DZ:Lcom/squareup/protos/common/languages/Language;

.field public static final enum EE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum EL:Lcom/squareup/protos/common/languages/Language;

.field public static final enum EN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum EO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ES:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ET:Lcom/squareup/protos/common/languages/Language;

.field public static final enum EU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum FA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum FF:Lcom/squareup/protos/common/languages/Language;

.field public static final enum FI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum FJ:Lcom/squareup/protos/common/languages/Language;

.field public static final enum FO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum FR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum FY:Lcom/squareup/protos/common/languages/Language;

.field public static final enum GA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum GD:Lcom/squareup/protos/common/languages/Language;

.field public static final enum GL:Lcom/squareup/protos/common/languages/Language;

.field public static final enum GN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum GU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum GV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HT:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HY:Lcom/squareup/protos/common/languages/Language;

.field public static final enum HZ:Lcom/squareup/protos/common/languages/Language;

.field public static final enum IA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ID:Lcom/squareup/protos/common/languages/Language;

.field public static final enum IE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum IG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum II:Lcom/squareup/protos/common/languages/Language;

.field public static final enum IK:Lcom/squareup/protos/common/languages/Language;

.field public static final enum IO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum IS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum IT:Lcom/squareup/protos/common/languages/Language;

.field public static final enum IU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum JA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum JV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KJ:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KK:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KL:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KM:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KW:Lcom/squareup/protos/common/languages/Language;

.field public static final enum KY:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LB:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LT:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum LV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MH:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MK:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ML:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MT:Lcom/squareup/protos/common/languages/Language;

.field public static final enum MY:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NB:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ND:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NL:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum NY:Lcom/squareup/protos/common/languages/Language;

.field public static final enum OC:Lcom/squareup/protos/common/languages/Language;

.field public static final enum OJ:Lcom/squareup/protos/common/languages/Language;

.field public static final enum OM:Lcom/squareup/protos/common/languages/Language;

.field public static final enum OR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum OS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum PA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum PI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum PL:Lcom/squareup/protos/common/languages/Language;

.field public static final enum PS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum PT:Lcom/squareup/protos/common/languages/Language;

.field public static final enum QU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum RM:Lcom/squareup/protos/common/languages/Language;

.field public static final enum RN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum RO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum RU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum RW:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SC:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SD:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SH:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SK:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SL:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SM:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SQ:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ST:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SU:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SV:Lcom/squareup/protos/common/languages/Language;

.field public static final enum SW:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TH:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TK:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TL:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TN:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TS:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TT:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TW:Lcom/squareup/protos/common/languages/Language;

.field public static final enum TY:Lcom/squareup/protos/common/languages/Language;

.field public static final enum UG:Lcom/squareup/protos/common/languages/Language;

.field public static final enum UK:Lcom/squareup/protos/common/languages/Language;

.field public static final enum UR:Lcom/squareup/protos/common/languages/Language;

.field public static final enum UZ:Lcom/squareup/protos/common/languages/Language;

.field public static final enum VE:Lcom/squareup/protos/common/languages/Language;

.field public static final enum VI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum VO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum WA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum WO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum XH:Lcom/squareup/protos/common/languages/Language;

.field public static final enum YI:Lcom/squareup/protos/common/languages/Language;

.field public static final enum YO:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ZA:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ZH:Lcom/squareup/protos/common/languages/Language;

.field public static final enum ZU:Lcom/squareup/protos/common/languages/Language;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 20
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/16 v1, 0x25

    const-string v2, "EN"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->EN:Lcom/squareup/protos/common/languages/Language;

    .line 25
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/4 v2, 0x1

    const-string v3, "AA"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AA:Lcom/squareup/protos/common/languages/Language;

    .line 30
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/4 v3, 0x2

    const-string v4, "AB"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AB:Lcom/squareup/protos/common/languages/Language;

    .line 35
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/4 v4, 0x3

    const-string v5, "AF"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AF:Lcom/squareup/protos/common/languages/Language;

    .line 40
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/4 v5, 0x4

    const-string v6, "AK"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AK:Lcom/squareup/protos/common/languages/Language;

    .line 45
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/4 v6, 0x5

    const-string v7, "AM"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AM:Lcom/squareup/protos/common/languages/Language;

    .line 50
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/4 v7, 0x6

    const-string v8, "AR"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AR:Lcom/squareup/protos/common/languages/Language;

    .line 55
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/4 v8, 0x7

    const-string v9, "AN"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AN:Lcom/squareup/protos/common/languages/Language;

    .line 60
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/16 v9, 0x8

    const-string v10, "AS"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AS:Lcom/squareup/protos/common/languages/Language;

    .line 65
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/16 v10, 0x9

    const-string v11, "AV"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AV:Lcom/squareup/protos/common/languages/Language;

    .line 70
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/16 v11, 0xa

    const-string v12, "AE"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AE:Lcom/squareup/protos/common/languages/Language;

    .line 75
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/16 v12, 0xb

    const-string v13, "AY"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AY:Lcom/squareup/protos/common/languages/Language;

    .line 80
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/16 v13, 0xc

    const-string v14, "AZ"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->AZ:Lcom/squareup/protos/common/languages/Language;

    .line 85
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/16 v14, 0xd

    const-string v15, "BA"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BA:Lcom/squareup/protos/common/languages/Language;

    .line 90
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const/16 v15, 0xe

    const-string v14, "BM"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BM:Lcom/squareup/protos/common/languages/Language;

    .line 95
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v14, "BE"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BE:Lcom/squareup/protos/common/languages/Language;

    .line 100
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "BN"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BN:Lcom/squareup/protos/common/languages/Language;

    .line 105
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "BI"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BI:Lcom/squareup/protos/common/languages/Language;

    .line 110
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "BO"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BO:Lcom/squareup/protos/common/languages/Language;

    .line 115
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "BS"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BS:Lcom/squareup/protos/common/languages/Language;

    .line 120
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "BR"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BR:Lcom/squareup/protos/common/languages/Language;

    .line 125
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "BG"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->BG:Lcom/squareup/protos/common/languages/Language;

    .line 130
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CA"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CA:Lcom/squareup/protos/common/languages/Language;

    .line 135
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CS"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CS:Lcom/squareup/protos/common/languages/Language;

    .line 140
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CH"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CH:Lcom/squareup/protos/common/languages/Language;

    .line 145
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CE"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CE:Lcom/squareup/protos/common/languages/Language;

    .line 150
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CU"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CU:Lcom/squareup/protos/common/languages/Language;

    .line 155
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CV"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CV:Lcom/squareup/protos/common/languages/Language;

    .line 160
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KW"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KW:Lcom/squareup/protos/common/languages/Language;

    .line 165
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CO"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CO:Lcom/squareup/protos/common/languages/Language;

    .line 170
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CR"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CR:Lcom/squareup/protos/common/languages/Language;

    .line 175
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "CY"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->CY:Lcom/squareup/protos/common/languages/Language;

    .line 180
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "DA"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->DA:Lcom/squareup/protos/common/languages/Language;

    .line 185
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "DE"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->DE:Lcom/squareup/protos/common/languages/Language;

    .line 190
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "DV"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->DV:Lcom/squareup/protos/common/languages/Language;

    .line 195
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "DZ"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->DZ:Lcom/squareup/protos/common/languages/Language;

    .line 200
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "EL"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->EL:Lcom/squareup/protos/common/languages/Language;

    .line 205
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "EO"

    const/16 v14, 0x26

    invoke-direct {v0, v13, v1, v14}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->EO:Lcom/squareup/protos/common/languages/Language;

    .line 210
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ET"

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ET:Lcom/squareup/protos/common/languages/Language;

    .line 215
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "EU"

    const/16 v14, 0x27

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->EU:Lcom/squareup/protos/common/languages/Language;

    .line 220
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "EE"

    const/16 v14, 0x28

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->EE:Lcom/squareup/protos/common/languages/Language;

    .line 225
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "FO"

    const/16 v14, 0x29

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->FO:Lcom/squareup/protos/common/languages/Language;

    .line 230
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "FA"

    const/16 v14, 0x2a

    const/16 v15, 0x2b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->FA:Lcom/squareup/protos/common/languages/Language;

    .line 235
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "FJ"

    const/16 v14, 0x2b

    const/16 v15, 0x2c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->FJ:Lcom/squareup/protos/common/languages/Language;

    .line 240
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "FI"

    const/16 v14, 0x2c

    const/16 v15, 0x2d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->FI:Lcom/squareup/protos/common/languages/Language;

    .line 245
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "FR"

    const/16 v14, 0x2d

    const/16 v15, 0x2e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->FR:Lcom/squareup/protos/common/languages/Language;

    .line 250
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "FY"

    const/16 v14, 0x2e

    const/16 v15, 0x2f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->FY:Lcom/squareup/protos/common/languages/Language;

    .line 255
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "FF"

    const/16 v14, 0x2f

    const/16 v15, 0x30

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->FF:Lcom/squareup/protos/common/languages/Language;

    .line 260
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "GD"

    const/16 v14, 0x30

    const/16 v15, 0x31

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->GD:Lcom/squareup/protos/common/languages/Language;

    .line 265
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "GA"

    const/16 v14, 0x31

    const/16 v15, 0x32

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->GA:Lcom/squareup/protos/common/languages/Language;

    .line 270
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "GL"

    const/16 v14, 0x32

    const/16 v15, 0x33

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->GL:Lcom/squareup/protos/common/languages/Language;

    .line 275
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "GV"

    const/16 v14, 0x33

    const/16 v15, 0x34

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->GV:Lcom/squareup/protos/common/languages/Language;

    .line 280
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "GN"

    const/16 v14, 0x34

    const/16 v15, 0x35

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->GN:Lcom/squareup/protos/common/languages/Language;

    .line 285
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "GU"

    const/16 v14, 0x35

    const/16 v15, 0x36

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->GU:Lcom/squareup/protos/common/languages/Language;

    .line 290
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HT"

    const/16 v14, 0x36

    const/16 v15, 0x37

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HT:Lcom/squareup/protos/common/languages/Language;

    .line 295
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HA"

    const/16 v14, 0x37

    const/16 v15, 0x38

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HA:Lcom/squareup/protos/common/languages/Language;

    .line 300
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SH"

    const/16 v14, 0x38

    const/16 v15, 0x39

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SH:Lcom/squareup/protos/common/languages/Language;

    .line 305
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HE"

    const/16 v14, 0x39

    const/16 v15, 0x3a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HE:Lcom/squareup/protos/common/languages/Language;

    .line 310
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HZ"

    const/16 v14, 0x3a

    const/16 v15, 0x3b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HZ:Lcom/squareup/protos/common/languages/Language;

    .line 315
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HI"

    const/16 v14, 0x3b

    const/16 v15, 0x3c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HI:Lcom/squareup/protos/common/languages/Language;

    .line 320
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HO"

    const/16 v14, 0x3c

    const/16 v15, 0x3d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HO:Lcom/squareup/protos/common/languages/Language;

    .line 325
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HR"

    const/16 v14, 0x3d

    const/16 v15, 0x3e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HR:Lcom/squareup/protos/common/languages/Language;

    .line 330
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HU"

    const/16 v14, 0x3e

    const/16 v15, 0x3f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HU:Lcom/squareup/protos/common/languages/Language;

    .line 335
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "HY"

    const/16 v14, 0x3f

    const/16 v15, 0x40

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->HY:Lcom/squareup/protos/common/languages/Language;

    .line 340
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "IG"

    const/16 v14, 0x40

    const/16 v15, 0x41

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->IG:Lcom/squareup/protos/common/languages/Language;

    .line 345
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "IO"

    const/16 v14, 0x41

    const/16 v15, 0x42

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->IO:Lcom/squareup/protos/common/languages/Language;

    .line 350
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "II"

    const/16 v14, 0x42

    const/16 v15, 0x43

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->II:Lcom/squareup/protos/common/languages/Language;

    .line 355
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "IU"

    const/16 v14, 0x43

    const/16 v15, 0x44

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->IU:Lcom/squareup/protos/common/languages/Language;

    .line 360
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "IE"

    const/16 v14, 0x44

    const/16 v15, 0x45

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->IE:Lcom/squareup/protos/common/languages/Language;

    .line 365
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "IA"

    const/16 v14, 0x45

    const/16 v15, 0x46

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->IA:Lcom/squareup/protos/common/languages/Language;

    .line 370
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ID"

    const/16 v14, 0x46

    const/16 v15, 0x47

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ID:Lcom/squareup/protos/common/languages/Language;

    .line 375
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "IK"

    const/16 v14, 0x47

    const/16 v15, 0x48

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->IK:Lcom/squareup/protos/common/languages/Language;

    .line 380
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "IS"

    const/16 v14, 0x48

    const/16 v15, 0x49

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->IS:Lcom/squareup/protos/common/languages/Language;

    .line 385
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "IT"

    const/16 v14, 0x49

    const/16 v15, 0x4a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->IT:Lcom/squareup/protos/common/languages/Language;

    .line 390
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "JV"

    const/16 v14, 0x4a

    const/16 v15, 0x4b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->JV:Lcom/squareup/protos/common/languages/Language;

    .line 395
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "JA"

    const/16 v14, 0x4b

    const/16 v15, 0x4c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->JA:Lcom/squareup/protos/common/languages/Language;

    .line 400
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KL"

    const/16 v14, 0x4c

    const/16 v15, 0x4d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KL:Lcom/squareup/protos/common/languages/Language;

    .line 405
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KN"

    const/16 v14, 0x4d

    const/16 v15, 0x4e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KN:Lcom/squareup/protos/common/languages/Language;

    .line 410
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KS"

    const/16 v14, 0x4e

    const/16 v15, 0x4f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KS:Lcom/squareup/protos/common/languages/Language;

    .line 415
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KA"

    const/16 v14, 0x4f

    const/16 v15, 0x50

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KA:Lcom/squareup/protos/common/languages/Language;

    .line 420
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KR"

    const/16 v14, 0x50

    const/16 v15, 0x51

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KR:Lcom/squareup/protos/common/languages/Language;

    .line 425
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KK"

    const/16 v14, 0x51

    const/16 v15, 0x52

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KK:Lcom/squareup/protos/common/languages/Language;

    .line 430
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KM"

    const/16 v14, 0x52

    const/16 v15, 0x53

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KM:Lcom/squareup/protos/common/languages/Language;

    .line 435
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KI"

    const/16 v14, 0x53

    const/16 v15, 0x54

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KI:Lcom/squareup/protos/common/languages/Language;

    .line 440
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "RW"

    const/16 v14, 0x54

    const/16 v15, 0x55

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->RW:Lcom/squareup/protos/common/languages/Language;

    .line 445
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KY"

    const/16 v14, 0x55

    const/16 v15, 0x56

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KY:Lcom/squareup/protos/common/languages/Language;

    .line 450
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KV"

    const/16 v14, 0x56

    const/16 v15, 0x57

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KV:Lcom/squareup/protos/common/languages/Language;

    .line 455
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KG"

    const/16 v14, 0x57

    const/16 v15, 0x58

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KG:Lcom/squareup/protos/common/languages/Language;

    .line 460
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KO"

    const/16 v14, 0x58

    const/16 v15, 0x59

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KO:Lcom/squareup/protos/common/languages/Language;

    .line 465
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KJ"

    const/16 v14, 0x59

    const/16 v15, 0x5a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KJ:Lcom/squareup/protos/common/languages/Language;

    .line 470
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "KU"

    const/16 v14, 0x5a

    const/16 v15, 0x5b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->KU:Lcom/squareup/protos/common/languages/Language;

    .line 475
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LO"

    const/16 v14, 0x5b

    const/16 v15, 0x5c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LO:Lcom/squareup/protos/common/languages/Language;

    .line 480
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LA"

    const/16 v14, 0x5c

    const/16 v15, 0x5d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LA:Lcom/squareup/protos/common/languages/Language;

    .line 485
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LV"

    const/16 v14, 0x5d

    const/16 v15, 0x5e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LV:Lcom/squareup/protos/common/languages/Language;

    .line 490
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LI"

    const/16 v14, 0x5e

    const/16 v15, 0x5f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LI:Lcom/squareup/protos/common/languages/Language;

    .line 495
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LN"

    const/16 v14, 0x5f

    const/16 v15, 0x60

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LN:Lcom/squareup/protos/common/languages/Language;

    .line 500
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LT"

    const/16 v14, 0x60

    const/16 v15, 0x61

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LT:Lcom/squareup/protos/common/languages/Language;

    .line 505
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LB"

    const/16 v14, 0x61

    const/16 v15, 0x62

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LB:Lcom/squareup/protos/common/languages/Language;

    .line 510
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LU"

    const/16 v14, 0x62

    const/16 v15, 0x63

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LU:Lcom/squareup/protos/common/languages/Language;

    .line 515
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "LG"

    const/16 v14, 0x63

    const/16 v15, 0x64

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->LG:Lcom/squareup/protos/common/languages/Language;

    .line 520
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MH"

    const/16 v14, 0x64

    const/16 v15, 0x65

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MH:Lcom/squareup/protos/common/languages/Language;

    .line 525
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ML"

    const/16 v14, 0x65

    const/16 v15, 0x66

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ML:Lcom/squareup/protos/common/languages/Language;

    .line 530
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MR"

    const/16 v14, 0x66

    const/16 v15, 0x67

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MR:Lcom/squareup/protos/common/languages/Language;

    .line 535
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MK"

    const/16 v14, 0x67

    const/16 v15, 0x68

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MK:Lcom/squareup/protos/common/languages/Language;

    .line 540
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MG"

    const/16 v14, 0x68

    const/16 v15, 0x69

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MG:Lcom/squareup/protos/common/languages/Language;

    .line 545
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MT"

    const/16 v14, 0x69

    const/16 v15, 0x6a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MT:Lcom/squareup/protos/common/languages/Language;

    .line 550
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MN"

    const/16 v14, 0x6a

    const/16 v15, 0x6b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MN:Lcom/squareup/protos/common/languages/Language;

    .line 555
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MI"

    const/16 v14, 0x6b

    const/16 v15, 0x6c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MI:Lcom/squareup/protos/common/languages/Language;

    .line 560
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MS"

    const/16 v14, 0x6c

    const/16 v15, 0x6d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MS:Lcom/squareup/protos/common/languages/Language;

    .line 565
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "MY"

    const/16 v14, 0x6d

    const/16 v15, 0x6e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->MY:Lcom/squareup/protos/common/languages/Language;

    .line 570
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NA"

    const/16 v14, 0x6e

    const/16 v15, 0x6f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NA:Lcom/squareup/protos/common/languages/Language;

    .line 575
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NV"

    const/16 v14, 0x6f

    const/16 v15, 0x70

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NV:Lcom/squareup/protos/common/languages/Language;

    .line 580
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NR"

    const/16 v14, 0x70

    const/16 v15, 0x71

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NR:Lcom/squareup/protos/common/languages/Language;

    .line 585
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ND"

    const/16 v14, 0x71

    const/16 v15, 0x72

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ND:Lcom/squareup/protos/common/languages/Language;

    .line 590
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NG"

    const/16 v14, 0x72

    const/16 v15, 0x73

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NG:Lcom/squareup/protos/common/languages/Language;

    .line 595
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NE"

    const/16 v14, 0x73

    const/16 v15, 0x74

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NE:Lcom/squareup/protos/common/languages/Language;

    .line 600
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NL"

    const/16 v14, 0x74

    const/16 v15, 0x75

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NL:Lcom/squareup/protos/common/languages/Language;

    .line 605
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NN"

    const/16 v14, 0x75

    const/16 v15, 0x76

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NN:Lcom/squareup/protos/common/languages/Language;

    .line 610
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NB"

    const/16 v14, 0x76

    const/16 v15, 0x77

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NB:Lcom/squareup/protos/common/languages/Language;

    .line 615
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NO"

    const/16 v14, 0x77

    const/16 v15, 0x78

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NO:Lcom/squareup/protos/common/languages/Language;

    .line 620
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "NY"

    const/16 v14, 0x78

    const/16 v15, 0x79

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->NY:Lcom/squareup/protos/common/languages/Language;

    .line 625
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "OC"

    const/16 v14, 0x79

    const/16 v15, 0x7a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->OC:Lcom/squareup/protos/common/languages/Language;

    .line 630
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "OJ"

    const/16 v14, 0x7a

    const/16 v15, 0x7b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->OJ:Lcom/squareup/protos/common/languages/Language;

    .line 635
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "OR"

    const/16 v14, 0x7b

    const/16 v15, 0x7c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->OR:Lcom/squareup/protos/common/languages/Language;

    .line 640
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "OM"

    const/16 v14, 0x7c

    const/16 v15, 0x7d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->OM:Lcom/squareup/protos/common/languages/Language;

    .line 645
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "OS"

    const/16 v14, 0x7d

    const/16 v15, 0x7e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->OS:Lcom/squareup/protos/common/languages/Language;

    .line 650
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "PA"

    const/16 v14, 0x7e

    const/16 v15, 0x7f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->PA:Lcom/squareup/protos/common/languages/Language;

    .line 655
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "PI"

    const/16 v14, 0x7f

    const/16 v15, 0x80

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->PI:Lcom/squareup/protos/common/languages/Language;

    .line 660
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "PL"

    const/16 v14, 0x80

    const/16 v15, 0x81

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->PL:Lcom/squareup/protos/common/languages/Language;

    .line 665
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "PT"

    const/16 v14, 0x81

    const/16 v15, 0x82

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->PT:Lcom/squareup/protos/common/languages/Language;

    .line 670
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "PS"

    const/16 v14, 0x82

    const/16 v15, 0x83

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->PS:Lcom/squareup/protos/common/languages/Language;

    .line 675
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "QU"

    const/16 v14, 0x83

    const/16 v15, 0x84

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->QU:Lcom/squareup/protos/common/languages/Language;

    .line 680
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "RM"

    const/16 v14, 0x84

    const/16 v15, 0x85

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->RM:Lcom/squareup/protos/common/languages/Language;

    .line 685
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "RO"

    const/16 v14, 0x85

    const/16 v15, 0x86

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->RO:Lcom/squareup/protos/common/languages/Language;

    .line 690
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "RN"

    const/16 v14, 0x86

    const/16 v15, 0x87

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->RN:Lcom/squareup/protos/common/languages/Language;

    .line 695
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "RU"

    const/16 v14, 0x87

    const/16 v15, 0x88

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->RU:Lcom/squareup/protos/common/languages/Language;

    .line 700
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SG"

    const/16 v14, 0x88

    const/16 v15, 0x89

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SG:Lcom/squareup/protos/common/languages/Language;

    .line 705
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SA"

    const/16 v14, 0x89

    const/16 v15, 0x8a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SA:Lcom/squareup/protos/common/languages/Language;

    .line 710
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SI"

    const/16 v14, 0x8a

    const/16 v15, 0x8b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SI:Lcom/squareup/protos/common/languages/Language;

    .line 715
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SK"

    const/16 v14, 0x8b

    const/16 v15, 0x8c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SK:Lcom/squareup/protos/common/languages/Language;

    .line 720
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SL"

    const/16 v14, 0x8c

    const/16 v15, 0x8d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SL:Lcom/squareup/protos/common/languages/Language;

    .line 725
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SE"

    const/16 v14, 0x8d

    const/16 v15, 0x8e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SE:Lcom/squareup/protos/common/languages/Language;

    .line 730
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SM"

    const/16 v14, 0x8e

    const/16 v15, 0x8f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SM:Lcom/squareup/protos/common/languages/Language;

    .line 735
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SN"

    const/16 v14, 0x8f

    const/16 v15, 0x90

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SN:Lcom/squareup/protos/common/languages/Language;

    .line 740
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SD"

    const/16 v14, 0x90

    const/16 v15, 0x91

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SD:Lcom/squareup/protos/common/languages/Language;

    .line 745
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SO"

    const/16 v14, 0x91

    const/16 v15, 0x92

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SO:Lcom/squareup/protos/common/languages/Language;

    .line 750
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ST"

    const/16 v14, 0x92

    const/16 v15, 0x93

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ST:Lcom/squareup/protos/common/languages/Language;

    .line 755
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ES"

    const/16 v14, 0x93

    const/16 v15, 0x94

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ES:Lcom/squareup/protos/common/languages/Language;

    .line 760
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SQ"

    const/16 v14, 0x94

    const/16 v15, 0x95

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SQ:Lcom/squareup/protos/common/languages/Language;

    .line 765
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SC"

    const/16 v14, 0x95

    const/16 v15, 0x96

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SC:Lcom/squareup/protos/common/languages/Language;

    .line 770
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SR"

    const/16 v14, 0x96

    const/16 v15, 0x97

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SR:Lcom/squareup/protos/common/languages/Language;

    .line 775
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SS"

    const/16 v14, 0x97

    const/16 v15, 0x98

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SS:Lcom/squareup/protos/common/languages/Language;

    .line 780
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SU"

    const/16 v14, 0x98

    const/16 v15, 0x99

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SU:Lcom/squareup/protos/common/languages/Language;

    .line 785
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SW"

    const/16 v14, 0x99

    const/16 v15, 0x9a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SW:Lcom/squareup/protos/common/languages/Language;

    .line 790
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "SV"

    const/16 v14, 0x9a

    const/16 v15, 0x9b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->SV:Lcom/squareup/protos/common/languages/Language;

    .line 795
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TY"

    const/16 v14, 0x9b

    const/16 v15, 0x9c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TY:Lcom/squareup/protos/common/languages/Language;

    .line 800
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TA"

    const/16 v14, 0x9c

    const/16 v15, 0x9d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TA:Lcom/squareup/protos/common/languages/Language;

    .line 805
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TT"

    const/16 v14, 0x9d

    const/16 v15, 0x9e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TT:Lcom/squareup/protos/common/languages/Language;

    .line 810
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TE"

    const/16 v14, 0x9e

    const/16 v15, 0x9f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TE:Lcom/squareup/protos/common/languages/Language;

    .line 815
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TG"

    const/16 v14, 0x9f

    const/16 v15, 0xa0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TG:Lcom/squareup/protos/common/languages/Language;

    .line 820
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TL"

    const/16 v14, 0xa0

    const/16 v15, 0xa1

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TL:Lcom/squareup/protos/common/languages/Language;

    .line 825
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TH"

    const/16 v14, 0xa1

    const/16 v15, 0xa2

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TH:Lcom/squareup/protos/common/languages/Language;

    .line 830
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TI"

    const/16 v14, 0xa2

    const/16 v15, 0xa3

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TI:Lcom/squareup/protos/common/languages/Language;

    .line 835
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TO"

    const/16 v14, 0xa3

    const/16 v15, 0xa4

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TO:Lcom/squareup/protos/common/languages/Language;

    .line 840
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TN"

    const/16 v14, 0xa4

    const/16 v15, 0xa5

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TN:Lcom/squareup/protos/common/languages/Language;

    .line 845
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TS"

    const/16 v14, 0xa5

    const/16 v15, 0xa6

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TS:Lcom/squareup/protos/common/languages/Language;

    .line 850
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TK"

    const/16 v14, 0xa6

    const/16 v15, 0xa7

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TK:Lcom/squareup/protos/common/languages/Language;

    .line 855
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TR"

    const/16 v14, 0xa7

    const/16 v15, 0xa8

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TR:Lcom/squareup/protos/common/languages/Language;

    .line 860
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "TW"

    const/16 v14, 0xa8

    const/16 v15, 0xa9

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->TW:Lcom/squareup/protos/common/languages/Language;

    .line 865
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "UG"

    const/16 v14, 0xa9

    const/16 v15, 0xaa

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->UG:Lcom/squareup/protos/common/languages/Language;

    .line 870
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "UK"

    const/16 v14, 0xaa

    const/16 v15, 0xab

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->UK:Lcom/squareup/protos/common/languages/Language;

    .line 875
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "UR"

    const/16 v14, 0xab

    const/16 v15, 0xac

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->UR:Lcom/squareup/protos/common/languages/Language;

    .line 880
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "UZ"

    const/16 v14, 0xac

    const/16 v15, 0xad

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->UZ:Lcom/squareup/protos/common/languages/Language;

    .line 885
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "VE"

    const/16 v14, 0xad

    const/16 v15, 0xae

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->VE:Lcom/squareup/protos/common/languages/Language;

    .line 890
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "VI"

    const/16 v14, 0xae

    const/16 v15, 0xaf

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->VI:Lcom/squareup/protos/common/languages/Language;

    .line 895
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "VO"

    const/16 v14, 0xaf

    const/16 v15, 0xb0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->VO:Lcom/squareup/protos/common/languages/Language;

    .line 900
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "WA"

    const/16 v14, 0xb0

    const/16 v15, 0xb1

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->WA:Lcom/squareup/protos/common/languages/Language;

    .line 905
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "WO"

    const/16 v14, 0xb1

    const/16 v15, 0xb2

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->WO:Lcom/squareup/protos/common/languages/Language;

    .line 910
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "XH"

    const/16 v14, 0xb2

    const/16 v15, 0xb3

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->XH:Lcom/squareup/protos/common/languages/Language;

    .line 915
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "YI"

    const/16 v14, 0xb3

    const/16 v15, 0xb4

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->YI:Lcom/squareup/protos/common/languages/Language;

    .line 920
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "YO"

    const/16 v14, 0xb4

    const/16 v15, 0xb5

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->YO:Lcom/squareup/protos/common/languages/Language;

    .line 925
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ZA"

    const/16 v14, 0xb5

    const/16 v15, 0xb6

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ZA:Lcom/squareup/protos/common/languages/Language;

    .line 930
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ZH"

    const/16 v14, 0xb6

    const/16 v15, 0xb7

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ZH:Lcom/squareup/protos/common/languages/Language;

    .line 935
    new-instance v0, Lcom/squareup/protos/common/languages/Language;

    const-string v13, "ZU"

    const/16 v14, 0xb7

    const/16 v15, 0xb8

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/languages/Language;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ZU:Lcom/squareup/protos/common/languages/Language;

    const/16 v0, 0xb8

    new-array v0, v0, [Lcom/squareup/protos/common/languages/Language;

    .line 16
    sget-object v13, Lcom/squareup/protos/common/languages/Language;->EN:Lcom/squareup/protos/common/languages/Language;

    const/4 v14, 0x0

    aput-object v13, v0, v14

    sget-object v13, Lcom/squareup/protos/common/languages/Language;->AA:Lcom/squareup/protos/common/languages/Language;

    aput-object v13, v0, v2

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AB:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AF:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v4

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AK:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AM:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v6

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AR:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v7

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AN:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v8

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AS:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v9

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AV:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v10

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AE:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v11

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AY:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v12

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->AZ:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0xc

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BA:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0xd

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BM:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0xe

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BE:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0xf

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BN:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x10

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BI:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x11

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BO:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x12

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BS:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x13

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BR:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x14

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->BG:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x15

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CA:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x16

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CS:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x17

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CH:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x18

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CE:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x19

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CU:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x1a

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CV:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x1b

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->KW:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x1c

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CO:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x1d

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CR:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x1e

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->CY:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x1f

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->DA:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x20

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->DE:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x21

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->DV:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x22

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->DZ:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x23

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->EL:Lcom/squareup/protos/common/languages/Language;

    const/16 v3, 0x24

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/languages/Language;->EO:Lcom/squareup/protos/common/languages/Language;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ET:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->EU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->EE:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->FO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->FA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->FJ:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->FI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->FR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->FY:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->FF:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->GD:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->GA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->GL:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->GV:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->GN:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->GU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HT:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SH:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HE:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HZ:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->HY:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->IG:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->IO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->II:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->IU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->IE:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->IA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ID:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->IK:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->IS:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->IT:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->JV:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->JA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KL:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KN:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KS:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KK:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KM:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->RW:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KY:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KV:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KG:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KJ:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->KU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LV:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LN:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LT:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LB:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->LG:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MH:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ML:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MK:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MG:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MT:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MN:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MS:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->MY:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NV:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ND:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NG:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NE:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NL:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NN:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NB:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->NY:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->OC:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->OJ:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->OR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->OM:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->OS:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->PA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->PI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->PL:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->PT:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->PS:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->QU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->RM:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->RO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->RN:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->RU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SG:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SK:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SL:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SE:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SM:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SN:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SD:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ST:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ES:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SQ:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SC:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SS:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SW:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->SV:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TY:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TT:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TE:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TG:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TL:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TH:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TN:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TS:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TK:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->TW:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->UG:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->UK:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->UR:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->UZ:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->VE:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->VI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->VO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->WA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->WO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->XH:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->YI:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->YO:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ZA:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ZH:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/languages/Language;->ZU:Lcom/squareup/protos/common/languages/Language;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->$VALUES:[Lcom/squareup/protos/common/languages/Language;

    .line 937
    new-instance v0, Lcom/squareup/protos/common/languages/Language$ProtoAdapter_Language;

    invoke-direct {v0}, Lcom/squareup/protos/common/languages/Language$ProtoAdapter_Language;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/languages/Language;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 941
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 942
    iput p3, p0, Lcom/squareup/protos/common/languages/Language;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/languages/Language;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 1133
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ZU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1132
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ZH:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1131
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ZA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1130
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->YO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1129
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->YI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1128
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->XH:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1127
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->WO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1126
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->WA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1125
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->VO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1124
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->VI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1123
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->VE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1122
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->UZ:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1121
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->UR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1120
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->UK:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1119
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->UG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1118
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TW:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1117
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1116
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TK:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1115
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1114
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1113
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1112
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1111
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TH:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1110
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TL:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1109
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1108
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1107
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TT:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1106
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1105
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->TY:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1104
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1103
    :pswitch_1e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SW:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1102
    :pswitch_1f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1101
    :pswitch_20
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1100
    :pswitch_21
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1099
    :pswitch_22
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SC:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1098
    :pswitch_23
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SQ:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1097
    :pswitch_24
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ES:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1096
    :pswitch_25
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ST:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1095
    :pswitch_26
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1094
    :pswitch_27
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SD:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1093
    :pswitch_28
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1092
    :pswitch_29
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SM:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1091
    :pswitch_2a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1090
    :pswitch_2b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SL:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1089
    :pswitch_2c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SK:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1088
    :pswitch_2d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1087
    :pswitch_2e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1086
    :pswitch_2f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1085
    :pswitch_30
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->RU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1084
    :pswitch_31
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->RN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1083
    :pswitch_32
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->RO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1082
    :pswitch_33
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->RM:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1081
    :pswitch_34
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->QU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1080
    :pswitch_35
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->PS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1079
    :pswitch_36
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->PT:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1078
    :pswitch_37
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->PL:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1077
    :pswitch_38
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->PI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1076
    :pswitch_39
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->PA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1075
    :pswitch_3a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->OS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1074
    :pswitch_3b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->OM:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1073
    :pswitch_3c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->OR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1072
    :pswitch_3d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->OJ:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1071
    :pswitch_3e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->OC:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1070
    :pswitch_3f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NY:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1069
    :pswitch_40
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1068
    :pswitch_41
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NB:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1067
    :pswitch_42
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1066
    :pswitch_43
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NL:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1065
    :pswitch_44
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1064
    :pswitch_45
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1063
    :pswitch_46
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ND:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1062
    :pswitch_47
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1061
    :pswitch_48
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1060
    :pswitch_49
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->NA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1059
    :pswitch_4a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MY:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1058
    :pswitch_4b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1057
    :pswitch_4c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1056
    :pswitch_4d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1055
    :pswitch_4e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MT:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1054
    :pswitch_4f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1053
    :pswitch_50
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MK:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1052
    :pswitch_51
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1051
    :pswitch_52
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ML:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1050
    :pswitch_53
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->MH:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1049
    :pswitch_54
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1048
    :pswitch_55
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1047
    :pswitch_56
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LB:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1046
    :pswitch_57
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LT:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1045
    :pswitch_58
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1044
    :pswitch_59
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1043
    :pswitch_5a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1042
    :pswitch_5b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1041
    :pswitch_5c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->LO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1040
    :pswitch_5d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1039
    :pswitch_5e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KJ:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1038
    :pswitch_5f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1037
    :pswitch_60
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1036
    :pswitch_61
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1035
    :pswitch_62
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KY:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1034
    :pswitch_63
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->RW:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1033
    :pswitch_64
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1032
    :pswitch_65
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KM:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1031
    :pswitch_66
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KK:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1030
    :pswitch_67
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1029
    :pswitch_68
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1028
    :pswitch_69
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1027
    :pswitch_6a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1026
    :pswitch_6b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KL:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1025
    :pswitch_6c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->JA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1024
    :pswitch_6d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->JV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1023
    :pswitch_6e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->IT:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1022
    :pswitch_6f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->IS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1021
    :pswitch_70
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->IK:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1020
    :pswitch_71
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ID:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1019
    :pswitch_72
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->IA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1018
    :pswitch_73
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->IE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1017
    :pswitch_74
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->IU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1016
    :pswitch_75
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->II:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1015
    :pswitch_76
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->IO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1014
    :pswitch_77
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->IG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1013
    :pswitch_78
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HY:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1012
    :pswitch_79
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1011
    :pswitch_7a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1010
    :pswitch_7b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1009
    :pswitch_7c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1008
    :pswitch_7d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HZ:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1007
    :pswitch_7e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1006
    :pswitch_7f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->SH:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1005
    :pswitch_80
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1004
    :pswitch_81
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->HT:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1003
    :pswitch_82
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->GU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1002
    :pswitch_83
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->GN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1001
    :pswitch_84
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->GV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 1000
    :pswitch_85
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->GL:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 999
    :pswitch_86
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->GA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 998
    :pswitch_87
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->GD:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 997
    :pswitch_88
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->FF:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 996
    :pswitch_89
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->FY:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 995
    :pswitch_8a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->FR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 994
    :pswitch_8b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->FI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 993
    :pswitch_8c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->FJ:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 992
    :pswitch_8d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->FA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 991
    :pswitch_8e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->FO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 990
    :pswitch_8f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->EE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 989
    :pswitch_90
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->EU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 988
    :pswitch_91
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->ET:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 987
    :pswitch_92
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->EO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 950
    :pswitch_93
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->EN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 986
    :pswitch_94
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->EL:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 985
    :pswitch_95
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->DZ:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 984
    :pswitch_96
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->DV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 983
    :pswitch_97
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->DE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 982
    :pswitch_98
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->DA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 981
    :pswitch_99
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CY:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 980
    :pswitch_9a
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 979
    :pswitch_9b
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 978
    :pswitch_9c
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->KW:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 977
    :pswitch_9d
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 976
    :pswitch_9e
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CU:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 975
    :pswitch_9f
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 974
    :pswitch_a0
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CH:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 973
    :pswitch_a1
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 972
    :pswitch_a2
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->CA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 971
    :pswitch_a3
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BG:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 970
    :pswitch_a4
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 969
    :pswitch_a5
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 968
    :pswitch_a6
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BO:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 967
    :pswitch_a7
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BI:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 966
    :pswitch_a8
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 965
    :pswitch_a9
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 964
    :pswitch_aa
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BM:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 963
    :pswitch_ab
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->BA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 962
    :pswitch_ac
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AZ:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 961
    :pswitch_ad
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AY:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 960
    :pswitch_ae
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AE:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 959
    :pswitch_af
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AV:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 958
    :pswitch_b0
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AS:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 957
    :pswitch_b1
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AN:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 956
    :pswitch_b2
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AR:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 955
    :pswitch_b3
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AM:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 954
    :pswitch_b4
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AK:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 953
    :pswitch_b5
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AF:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 952
    :pswitch_b6
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AB:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    .line 951
    :pswitch_b7
    sget-object p0, Lcom/squareup/protos/common/languages/Language;->AA:Lcom/squareup/protos/common/languages/Language;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b7
        :pswitch_b6
        :pswitch_b5
        :pswitch_b4
        :pswitch_b3
        :pswitch_b2
        :pswitch_b1
        :pswitch_b0
        :pswitch_af
        :pswitch_ae
        :pswitch_ad
        :pswitch_ac
        :pswitch_ab
        :pswitch_aa
        :pswitch_a9
        :pswitch_a8
        :pswitch_a7
        :pswitch_a6
        :pswitch_a5
        :pswitch_a4
        :pswitch_a3
        :pswitch_a2
        :pswitch_a1
        :pswitch_a0
        :pswitch_9f
        :pswitch_9e
        :pswitch_9d
        :pswitch_9c
        :pswitch_9b
        :pswitch_9a
        :pswitch_99
        :pswitch_98
        :pswitch_97
        :pswitch_96
        :pswitch_95
        :pswitch_94
        :pswitch_93
        :pswitch_92
        :pswitch_91
        :pswitch_90
        :pswitch_8f
        :pswitch_8e
        :pswitch_8d
        :pswitch_8c
        :pswitch_8b
        :pswitch_8a
        :pswitch_89
        :pswitch_88
        :pswitch_87
        :pswitch_86
        :pswitch_85
        :pswitch_84
        :pswitch_83
        :pswitch_82
        :pswitch_81
        :pswitch_80
        :pswitch_7f
        :pswitch_7e
        :pswitch_7d
        :pswitch_7c
        :pswitch_7b
        :pswitch_7a
        :pswitch_79
        :pswitch_78
        :pswitch_77
        :pswitch_76
        :pswitch_75
        :pswitch_74
        :pswitch_73
        :pswitch_72
        :pswitch_71
        :pswitch_70
        :pswitch_6f
        :pswitch_6e
        :pswitch_6d
        :pswitch_6c
        :pswitch_6b
        :pswitch_6a
        :pswitch_69
        :pswitch_68
        :pswitch_67
        :pswitch_66
        :pswitch_65
        :pswitch_64
        :pswitch_63
        :pswitch_62
        :pswitch_61
        :pswitch_60
        :pswitch_5f
        :pswitch_5e
        :pswitch_5d
        :pswitch_5c
        :pswitch_5b
        :pswitch_5a
        :pswitch_59
        :pswitch_58
        :pswitch_57
        :pswitch_56
        :pswitch_55
        :pswitch_54
        :pswitch_53
        :pswitch_52
        :pswitch_51
        :pswitch_50
        :pswitch_4f
        :pswitch_4e
        :pswitch_4d
        :pswitch_4c
        :pswitch_4b
        :pswitch_4a
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/languages/Language;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/protos/common/languages/Language;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/languages/Language;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/languages/Language;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/protos/common/languages/Language;->$VALUES:[Lcom/squareup/protos/common/languages/Language;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/languages/Language;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/languages/Language;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1140
    iget v0, p0, Lcom/squareup/protos/common/languages/Language;->value:I

    return v0
.end method
