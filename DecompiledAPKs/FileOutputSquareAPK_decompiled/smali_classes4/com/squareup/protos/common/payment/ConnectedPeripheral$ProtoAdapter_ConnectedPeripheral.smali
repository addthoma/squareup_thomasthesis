.class final Lcom/squareup/protos/common/payment/ConnectedPeripheral$ProtoAdapter_ConnectedPeripheral;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ConnectedPeripheral.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/payment/ConnectedPeripheral;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ConnectedPeripheral"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 287
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/payment/ConnectedPeripheral;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 312
    new-instance v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;-><init>()V

    .line 313
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 314
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 329
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 327
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->model_name(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    goto :goto_0

    .line 326
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->manufacturer_name(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    goto :goto_0

    .line 320
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->connection_type(Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 322
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 317
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->device_type(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    goto :goto_0

    .line 316
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->unique_identifier(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    goto :goto_0

    .line 333
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 334
    invoke-virtual {v0}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->build()Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 285
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ProtoAdapter_ConnectedPeripheral;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/payment/ConnectedPeripheral;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 302
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->unique_identifier:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 303
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->device_type:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 304
    sget-object v0, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->connection_type:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 305
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->manufacturer_name:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 306
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->model_name:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 307
    invoke-virtual {p2}, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 285
    check-cast p2, Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ProtoAdapter_ConnectedPeripheral;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/common/payment/ConnectedPeripheral;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/common/payment/ConnectedPeripheral;)I
    .locals 4

    .line 292
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->unique_identifier:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->device_type:Ljava/lang/String;

    const/4 v3, 0x2

    .line 293
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->connection_type:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    const/4 v3, 0x3

    .line 294
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->manufacturer_name:Ljava/lang/String;

    const/4 v3, 0x4

    .line 295
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->model_name:Ljava/lang/String;

    const/4 v3, 0x5

    .line 296
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 285
    check-cast p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ProtoAdapter_ConnectedPeripheral;->encodedSize(Lcom/squareup/protos/common/payment/ConnectedPeripheral;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/common/payment/ConnectedPeripheral;)Lcom/squareup/protos/common/payment/ConnectedPeripheral;
    .locals 0

    .line 339
    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral;->newBuilder()Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    move-result-object p1

    .line 340
    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 341
    invoke-virtual {p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->build()Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 285
    check-cast p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ProtoAdapter_ConnectedPeripheral;->redact(Lcom/squareup/protos/common/payment/ConnectedPeripheral;)Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    move-result-object p1

    return-object p1
.end method
