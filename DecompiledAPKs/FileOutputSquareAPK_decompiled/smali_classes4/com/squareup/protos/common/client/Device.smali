.class public final Lcom/squareup/protos/common/client/Device;
.super Lcom/squareup/wire/Message;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/client/Device$ProtoAdapter_Device;,
        Lcom/squareup/protos/common/client/Device$Orientation;,
        Lcom/squareup/protos/common/client/Device$Platform;,
        Lcom/squareup/protos/common/client/Device$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/client/Device;",
        "Lcom/squareup/protos/common/client/Device$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/client/Device;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAND:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LANGUAGE_CODE:Lcom/squareup/protos/common/languages/Language;

.field public static final DEFAULT_MANUFACTURER:Ljava/lang/String; = ""

.field public static final DEFAULT_MODEL:Ljava/lang/String; = ""

.field public static final DEFAULT_ORIENTATION:Lcom/squareup/protos/common/client/Device$Orientation;

.field public static final DEFAULT_PLATFORM:Lcom/squareup/protos/common/client/Device$Platform;

.field private static final serialVersionUID:J


# instance fields
.field public final brand:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final country_code:Lcom/squareup/protos/common/countries/Country;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.countries.Country#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final language_code:Lcom/squareup/protos/common/languages/Language;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.languages.Language#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final manufacturer:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final model:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final orientation:Lcom/squareup/protos/common/client/Device$Orientation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Device$Orientation#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final os_version:Lcom/squareup/protos/common/client/Version;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Version#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final platform:Lcom/squareup/protos/common/client/Device$Platform;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Device$Platform#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/protos/common/client/Device$ProtoAdapter_Device;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Device$ProtoAdapter_Device;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/client/Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 42
    sget-object v0, Lcom/squareup/protos/common/client/Device$Platform;->IOS:Lcom/squareup/protos/common/client/Device$Platform;

    sput-object v0, Lcom/squareup/protos/common/client/Device;->DEFAULT_PLATFORM:Lcom/squareup/protos/common/client/Device$Platform;

    .line 50
    sget-object v0, Lcom/squareup/protos/common/client/Device$Orientation;->PORTRAIT:Lcom/squareup/protos/common/client/Device$Orientation;

    sput-object v0, Lcom/squareup/protos/common/client/Device;->DEFAULT_ORIENTATION:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 52
    sget-object v0, Lcom/squareup/protos/common/languages/Language;->EN:Lcom/squareup/protos/common/languages/Language;

    sput-object v0, Lcom/squareup/protos/common/client/Device;->DEFAULT_LANGUAGE_CODE:Lcom/squareup/protos/common/languages/Language;

    .line 54
    sget-object v0, Lcom/squareup/protos/common/countries/Country;->US:Lcom/squareup/protos/common/countries/Country;

    sput-object v0, Lcom/squareup/protos/common/client/Device;->DEFAULT_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/client/Device$Platform;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/client/Version;Lcom/squareup/protos/common/client/Device$Orientation;Lcom/squareup/protos/common/languages/Language;Lcom/squareup/protos/common/countries/Country;)V
    .locals 11

    .line 132
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/common/client/Device;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/client/Device$Platform;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/client/Version;Lcom/squareup/protos/common/client/Device$Orientation;Lcom/squareup/protos/common/languages/Language;Lcom/squareup/protos/common/countries/Country;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/client/Device$Platform;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/client/Version;Lcom/squareup/protos/common/client/Device$Orientation;Lcom/squareup/protos/common/languages/Language;Lcom/squareup/protos/common/countries/Country;Lokio/ByteString;)V
    .locals 1

    .line 138
    sget-object v0, Lcom/squareup/protos/common/client/Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/common/client/Device;->id:Ljava/lang/String;

    .line 140
    iput-object p2, p0, Lcom/squareup/protos/common/client/Device;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    .line 141
    iput-object p3, p0, Lcom/squareup/protos/common/client/Device;->manufacturer:Ljava/lang/String;

    .line 142
    iput-object p4, p0, Lcom/squareup/protos/common/client/Device;->brand:Ljava/lang/String;

    .line 143
    iput-object p5, p0, Lcom/squareup/protos/common/client/Device;->model:Ljava/lang/String;

    .line 144
    iput-object p6, p0, Lcom/squareup/protos/common/client/Device;->os_version:Lcom/squareup/protos/common/client/Version;

    .line 145
    iput-object p7, p0, Lcom/squareup/protos/common/client/Device;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 146
    iput-object p8, p0, Lcom/squareup/protos/common/client/Device;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 147
    iput-object p9, p0, Lcom/squareup/protos/common/client/Device;->country_code:Lcom/squareup/protos/common/countries/Country;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 169
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/client/Device;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 170
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/client/Device;

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Device;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/client/Device;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Device;->id:Ljava/lang/String;

    .line 172
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Device;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    .line 173
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->manufacturer:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Device;->manufacturer:Ljava/lang/String;

    .line 174
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->brand:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Device;->brand:Ljava/lang/String;

    .line 175
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->model:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Device;->model:Ljava/lang/String;

    .line 176
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->os_version:Lcom/squareup/protos/common/client/Version;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Device;->os_version:Lcom/squareup/protos/common/client/Version;

    .line 177
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Device;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 178
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->language_code:Lcom/squareup/protos/common/languages/Language;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Device;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->country_code:Lcom/squareup/protos/common/countries/Country;

    iget-object p1, p1, Lcom/squareup/protos/common/client/Device;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 180
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 185
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 187
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Device;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Device$Platform;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->manufacturer:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->brand:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->model:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->os_version:Lcom/squareup/protos/common/client/Version;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Version;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Device$Orientation;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->language_code:Lcom/squareup/protos/common/languages/Language;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/languages/Language;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/common/countries/Country;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 197
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/client/Device$Builder;
    .locals 2

    .line 152
    new-instance v0, Lcom/squareup/protos/common/client/Device$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Device$Builder;-><init>()V

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->id:Ljava/lang/String;

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->manufacturer:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->manufacturer:Ljava/lang/String;

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->brand:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->brand:Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->model:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->model:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->os_version:Lcom/squareup/protos/common/client/Version;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->os_version:Lcom/squareup/protos/common/client/Version;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->language_code:Lcom/squareup/protos/common/languages/Language;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->country_code:Lcom/squareup/protos/common/countries/Country;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Device$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 162
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Device;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/client/Device$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Device;->newBuilder()Lcom/squareup/protos/common/client/Device$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    if-eqz v1, :cond_1

    const-string v1, ", platform="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->platform:Lcom/squareup/protos/common/client/Device$Platform;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->manufacturer:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", manufacturer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->brand:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->brand:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->model:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", model="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->os_version:Lcom/squareup/protos/common/client/Version;

    if-eqz v1, :cond_5

    const-string v1, ", os_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->os_version:Lcom/squareup/protos/common/client/Version;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    if-eqz v1, :cond_6

    const-string v1, ", orientation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->orientation:Lcom/squareup/protos/common/client/Device$Orientation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 212
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->language_code:Lcom/squareup/protos/common/languages/Language;

    if-eqz v1, :cond_7

    const-string v1, ", language_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->language_code:Lcom/squareup/protos/common/languages/Language;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 213
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_8

    const-string v1, ", country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Device;->country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Device{"

    .line 214
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
