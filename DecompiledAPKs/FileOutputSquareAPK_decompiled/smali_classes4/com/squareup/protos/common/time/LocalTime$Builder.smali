.class public final Lcom/squareup/protos/common/time/LocalTime$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LocalTime.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/time/LocalTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/time/LocalTime;",
        "Lcom/squareup/protos/common/time/LocalTime$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public hour_of_day:Ljava/lang/Integer;

.field public millis_of_second:Ljava/lang/Integer;

.field public minute_of_hour:Ljava/lang/Integer;

.field public second_of_minute:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 131
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/time/LocalTime;
    .locals 7

    .line 156
    new-instance v6, Lcom/squareup/protos/common/time/LocalTime;

    iget-object v1, p0, Lcom/squareup/protos/common/time/LocalTime$Builder;->hour_of_day:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/common/time/LocalTime$Builder;->minute_of_hour:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/common/time/LocalTime$Builder;->second_of_minute:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/common/time/LocalTime$Builder;->millis_of_second:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/common/time/LocalTime;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/LocalTime$Builder;->build()Lcom/squareup/protos/common/time/LocalTime;

    move-result-object v0

    return-object v0
.end method

.method public hour_of_day(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/LocalTime$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/common/time/LocalTime$Builder;->hour_of_day:Ljava/lang/Integer;

    return-object p0
.end method

.method public millis_of_second(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/LocalTime$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/common/time/LocalTime$Builder;->millis_of_second:Ljava/lang/Integer;

    return-object p0
.end method

.method public minute_of_hour(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/LocalTime$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/common/time/LocalTime$Builder;->minute_of_hour:Ljava/lang/Integer;

    return-object p0
.end method

.method public second_of_minute(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/LocalTime$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/common/time/LocalTime$Builder;->second_of_minute:Ljava/lang/Integer;

    return-object p0
.end method
