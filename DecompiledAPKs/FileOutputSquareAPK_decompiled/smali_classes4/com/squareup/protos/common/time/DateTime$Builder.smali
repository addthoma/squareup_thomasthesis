.class public final Lcom/squareup/protos/common/time/DateTime$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DateTime.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/time/DateTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/time/DateTime;",
        "Lcom/squareup/protos/common/time/DateTime$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public instant_usec:Ljava/lang/Long;

.field public ordinal:Ljava/lang/Long;

.field public posix_tz:Ljava/lang/String;

.field public timezone_offset_min:Ljava/lang/Integer;

.field public tz_name:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 202
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 203
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->tz_name:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/time/DateTime;
    .locals 8

    .line 255
    new-instance v7, Lcom/squareup/protos/common/time/DateTime;

    iget-object v1, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->timezone_offset_min:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->posix_tz:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->tz_name:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->ordinal:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/common/time/DateTime;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 191
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public instant_usec(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;
    .locals 0

    .line 213
    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec:Ljava/lang/Long;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->ordinal:Ljava/lang/Long;

    return-object p0
.end method

.method public posix_tz(Ljava/lang/String;)Lcom/squareup/protos/common/time/DateTime$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->posix_tz:Ljava/lang/String;

    return-object p0
.end method

.method public timezone_offset_min(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/DateTime$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->timezone_offset_min:Ljava/lang/Integer;

    return-object p0
.end method

.method public tz_name(Ljava/util/List;)Lcom/squareup/protos/common/time/DateTime$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/common/time/DateTime$Builder;"
        }
    .end annotation

    .line 238
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/common/time/DateTime$Builder;->tz_name:Ljava/util/List;

    return-object p0
.end method
