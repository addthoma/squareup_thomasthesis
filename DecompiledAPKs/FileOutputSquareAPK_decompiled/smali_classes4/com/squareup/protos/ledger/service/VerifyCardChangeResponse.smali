.class public final Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;
.super Lcom/squareup/wire/Message;
.source "VerifyCardChangeResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ProtoAdapter_VerifyCardChangeResponse;,
        Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;,
        Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;",
        "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_RESPONSE_CODE:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.ledger.service.VerifyCardChangeResponse$ResponseCode#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ProtoAdapter_VerifyCardChangeResponse;

    invoke-direct {v0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ProtoAdapter_VerifyCardChangeResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->UNKNOWN:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    sput-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->DEFAULT_RESPONSE_CODE:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;Ljava/lang/String;)V
    .locals 1

    .line 52
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;-><init>(Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 58
    iput-object p1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 59
    iput-object p2, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unit_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 74
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 75
    :cond_1
    check-cast p1, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    iget-object v3, p1, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unit_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unit_token:Ljava/lang/String;

    .line 78
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 83
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 88
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;
    .locals 2

    .line 64
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;-><init>()V

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    iput-object v1, v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;->unit_token:Ljava/lang/String;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->newBuilder()Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    if-eqz v1, :cond_0

    const-string v1, ", response_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VerifyCardChangeResponse{"

    .line 98
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
