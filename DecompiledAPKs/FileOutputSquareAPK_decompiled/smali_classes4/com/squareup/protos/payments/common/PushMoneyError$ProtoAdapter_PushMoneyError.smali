.class final Lcom/squareup/protos/payments/common/PushMoneyError$ProtoAdapter_PushMoneyError;
.super Lcom/squareup/wire/EnumAdapter;
.source "PushMoneyError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/payments/common/PushMoneyError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PushMoneyError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/payments/common/PushMoneyError;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 128
    const-class v0, Lcom/squareup/protos/payments/common/PushMoneyError;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/payments/common/PushMoneyError;
    .locals 0

    .line 133
    invoke-static {p1}, Lcom/squareup/protos/payments/common/PushMoneyError;->fromValue(I)Lcom/squareup/protos/payments/common/PushMoneyError;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 126
    invoke-virtual {p0, p1}, Lcom/squareup/protos/payments/common/PushMoneyError$ProtoAdapter_PushMoneyError;->fromValue(I)Lcom/squareup/protos/payments/common/PushMoneyError;

    move-result-object p1

    return-object p1
.end method
