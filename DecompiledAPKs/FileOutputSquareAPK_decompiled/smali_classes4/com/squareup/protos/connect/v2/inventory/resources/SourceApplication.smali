.class public final Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;
.super Lcom/squareup/wire/Message;
.source "SourceApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$ProtoAdapter_SourceApplication;,
        Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;",
        "Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PRODUCT:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field private static final serialVersionUID:J


# instance fields
.field public final application_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.inventory.resources.Product#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$ProtoAdapter_SourceApplication;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$ProtoAdapter_SourceApplication;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->PRODUCT_DO_NOT_USE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->DEFAULT_PRODUCT:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/inventory/resources/Product;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 72
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;-><init>(Lcom/squareup/protos/connect/v2/inventory/resources/Product;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/inventory/resources/Product;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 79
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->application_id:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 96
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 97
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->application_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->application_id:Ljava/lang/String;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->name:Ljava/lang/String;

    .line 101
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 106
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->application_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 112
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;
    .locals 2

    .line 85
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;-><init>()V

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->application_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->application_id:Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->name:Ljava/lang/String;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->newBuilder()Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    if-eqz v1, :cond_0

    const-string v1, ", product="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->product:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->application_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", application_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->application_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SourceApplication{"

    .line 123
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
