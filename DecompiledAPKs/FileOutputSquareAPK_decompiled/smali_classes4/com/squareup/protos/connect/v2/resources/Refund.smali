.class public final Lcom/squareup/protos/connect/v2/resources/Refund;
.super Lcom/squareup/wire/Message;
.source "Refund.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Refund$ProtoAdapter_Refund;,
        Lcom/squareup/protos/connect/v2/resources/Refund$Status;,
        Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/resources/Refund;",
        "Lcom/squareup/protos/connect/v2/resources/Refund$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Refund;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

.field public static final DEFAULT_TENDER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TRANSACTION_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final additional_recipients:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.AdditionalRecipient#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
            ">;"
        }
    .end annotation
.end field

.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Refund$Status#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final tender_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final transaction_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Refund$ProtoAdapter_Refund;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Refund$ProtoAdapter_Refund;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 45
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Refund$Status;->PENDING:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Refund;->DEFAULT_STATUS:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/resources/Refund$Status;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/resources/Refund$Status;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
            ">;)V"
        }
    .end annotation

    .line 147
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/connect/v2/resources/Refund;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/resources/Refund$Status;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/resources/Refund$Status;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/resources/Refund$Status;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 154
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    .line 156
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    .line 157
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    .line 158
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    .line 159
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    .line 160
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    .line 161
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 162
    iput-object p8, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    .line 163
    iput-object p9, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string p1, "additional_recipients"

    .line 164
    invoke-static {p1, p10}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 187
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/resources/Refund;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 188
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Refund;

    .line 189
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Refund;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Refund;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    .line 192
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    .line 195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    .line 197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 198
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    .line 199
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 204
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 206
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Refund;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Refund$Status;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    .locals 2

    .line 169
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;-><init>()V

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->id:Ljava/lang/String;

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->location_id:Ljava/lang/String;

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->transaction_id:Ljava/lang/String;

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->tender_id:Ljava/lang/String;

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->created_at:Ljava/lang/String;

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->reason:Ljava/lang/String;

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->additional_recipients:Ljava/util/List;

    .line 180
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Refund;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Refund;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", transaction_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", tender_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 232
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    if-eqz v1, :cond_7

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 233
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", processing_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 234
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", additional_recipients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Refund{"

    .line 235
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
