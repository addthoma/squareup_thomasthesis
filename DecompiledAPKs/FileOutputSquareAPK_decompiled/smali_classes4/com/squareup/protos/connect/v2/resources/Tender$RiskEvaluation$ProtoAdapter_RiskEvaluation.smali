.class final Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$ProtoAdapter_RiskEvaluation;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RiskEvaluation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1609
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1632
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;-><init>()V

    .line 1633
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1634
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 1655
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1653
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->reasons:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1652
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->numeric_score(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;

    goto :goto_0

    .line 1646
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->status(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1648
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1638
    :cond_3
    :try_start_1
    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->level(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 1640
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1659
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1660
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1607
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$ProtoAdapter_RiskEvaluation;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1623
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->level:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1624
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->numeric_score:Ljava/lang/Double;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1625
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->reasons:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1626
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->status:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1627
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1607
    check-cast p2, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$ProtoAdapter_RiskEvaluation;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;)I
    .locals 4

    .line 1614
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->level:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->numeric_score:Ljava/lang/Double;

    const/4 v3, 0x3

    .line 1615
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1616
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->reasons:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->status:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Status;

    const/4 v3, 0x2

    .line 1617
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1618
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1607
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$ProtoAdapter_RiskEvaluation;->encodedSize(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;
    .locals 0

    .line 1665
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;

    move-result-object p1

    .line 1666
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1667
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1607
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$ProtoAdapter_RiskEvaluation;->redact(Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    move-result-object p1

    return-object p1
.end method
