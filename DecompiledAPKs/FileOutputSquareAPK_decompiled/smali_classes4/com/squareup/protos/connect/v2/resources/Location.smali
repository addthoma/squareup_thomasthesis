.class public final Lcom/squareup/protos/connect/v2/resources/Location;
.super Lcom/squareup/wire/Message;
.source "Location.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Location$ProtoAdapter_Location;,
        Lcom/squareup/protos/connect/v2/resources/Location$Type;,
        Lcom/squareup/protos/connect/v2/resources/Location$Status;,
        Lcom/squareup/protos/connect/v2/resources/Location$Capability;,
        Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/resources/Location;",
        "Lcom/squareup/protos/connect/v2/resources/Location$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Location;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_BUSINESS_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_FACEBOOK_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_INSTAGRAM_USERNAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LANGUAGE_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_LOGO_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_MCC:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_POS_BACKGROUND_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/connect/v2/resources/Location$Status;

.field public static final DEFAULT_TIMEZONE:Ljava/lang/String; = ""

.field public static final DEFAULT_TWITTER_USERNAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/resources/Location$Type;

.field public static final DEFAULT_WEBSITE_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address:Lcom/squareup/protos/connect/v2/resources/Address;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Address#ADAPTER"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final business_email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x16
    .end annotation
.end field

.field public final business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.BusinessHours#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final business_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x12
    .end annotation
.end field

.field public final capabilities:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Location$Capability#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Location$Capability;",
            ">;"
        }
    .end annotation
.end field

.field public final coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Coordinates#ADAPTER"
        tag = 0x1b
    .end annotation
.end field

.field public final country:Lcom/squareup/protos/connect/v2/resources/Country;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Country#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final currency:Lcom/squareup/protos/connect/v2/common/Currency;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Currency#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x17
    .end annotation
.end field

.field public final facebook_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1a
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final instagram_username:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x19
    .end annotation
.end field

.field public final language_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final logo_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1c
    .end annotation
.end field

.field public final mcc:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1e
    .end annotation
.end field

.field public final merchant_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x11
    .end annotation
.end field

.field public final pos_background_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1d
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/connect/v2/resources/Location$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Location$Status#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final timezone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final twitter_username:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/connect/v2/resources/Location$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Location$Type#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final website_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$ProtoAdapter_Location;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Location$ProtoAdapter_Location;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 45
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Status;->ACTIVE:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location;->DEFAULT_STATUS:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    .line 51
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ZZ:Lcom/squareup/protos/connect/v2/resources/Country;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location;->DEFAULT_COUNTRY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 55
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UNKNOWN_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location;->DEFAULT_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 61
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->PHYSICAL:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location;->DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/resources/Location$Builder;Lokio/ByteString;)V
    .locals 1

    .line 376
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Location;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 377
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->id:Ljava/lang/String;

    .line 378
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->name:Ljava/lang/String;

    .line 379
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 380
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->timezone:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->timezone:Ljava/lang/String;

    .line 381
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->capabilities:Ljava/util/List;

    const-string v0, "capabilities"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->capabilities:Ljava/util/List;

    .line 382
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    .line 383
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->created_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->created_at:Ljava/lang/String;

    .line 384
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->merchant_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->merchant_id:Ljava/lang/String;

    .line 385
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 386
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->language_code:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->language_code:Ljava/lang/String;

    .line 387
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 388
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->phone_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->phone_number:Ljava/lang/String;

    .line 389
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_name:Ljava/lang/String;

    .line 390
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    .line 391
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->website_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->website_url:Ljava/lang/String;

    .line 392
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    .line 393
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_email:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_email:Ljava/lang/String;

    .line 394
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->description:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->description:Ljava/lang/String;

    .line 395
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->twitter_username:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->twitter_username:Ljava/lang/String;

    .line 396
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->instagram_username:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->instagram_username:Ljava/lang/String;

    .line 397
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->facebook_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->facebook_url:Ljava/lang/String;

    .line 398
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    .line 399
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->logo_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->logo_url:Ljava/lang/String;

    .line 400
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->pos_background_url:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Location;->pos_background_url:Ljava/lang/String;

    .line 401
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->mcc:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->mcc:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 439
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/resources/Location;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 440
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Location;

    .line 441
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Location;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Location;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->id:Ljava/lang/String;

    .line 442
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->name:Ljava/lang/String;

    .line 443
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 444
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->timezone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->timezone:Ljava/lang/String;

    .line 445
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->capabilities:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->capabilities:Ljava/util/List;

    .line 446
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    .line 447
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->created_at:Ljava/lang/String;

    .line 448
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->merchant_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->merchant_id:Ljava/lang/String;

    .line 449
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 450
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->language_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->language_code:Ljava/lang/String;

    .line 451
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 452
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->phone_number:Ljava/lang/String;

    .line 453
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->business_name:Ljava/lang/String;

    .line 454
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    .line 455
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->website_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->website_url:Ljava/lang/String;

    .line 456
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    .line 457
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->business_email:Ljava/lang/String;

    .line 458
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->description:Ljava/lang/String;

    .line 459
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->twitter_username:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->twitter_username:Ljava/lang/String;

    .line 460
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->instagram_username:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->instagram_username:Ljava/lang/String;

    .line 461
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->facebook_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->facebook_url:Ljava/lang/String;

    .line 462
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    .line 463
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->logo_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->logo_url:Ljava/lang/String;

    .line 464
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->pos_background_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Location;->pos_background_url:Ljava/lang/String;

    .line 465
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->mcc:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Location;->mcc:Ljava/lang/String;

    .line 466
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 471
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_18

    .line 473
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Location;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 474
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 475
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 476
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Address;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 477
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->timezone:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 478
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->capabilities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 479
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Location$Status;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 480
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 481
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 482
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Country;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 483
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->language_code:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 484
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Currency;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 485
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 486
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_name:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 487
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Location$Type;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 488
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->website_url:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 489
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/BusinessHours;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 490
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_email:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 491
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->description:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 492
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->twitter_username:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 493
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->instagram_username:Ljava/lang/String;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 494
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->facebook_url:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 495
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Coordinates;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 496
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->logo_url:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 497
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->pos_background_url:Ljava/lang/String;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 498
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->mcc:Ljava/lang/String;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_17
    add-int/2addr v0, v2

    .line 499
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_18
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 2

    .line 406
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Location$Builder;-><init>()V

    .line 407
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->id:Ljava/lang/String;

    .line 408
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->name:Ljava/lang/String;

    .line 409
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->timezone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->timezone:Ljava/lang/String;

    .line 411
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->capabilities:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->capabilities:Ljava/util/List;

    .line 412
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    .line 413
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->created_at:Ljava/lang/String;

    .line 414
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->merchant_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->merchant_id:Ljava/lang/String;

    .line 415
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 416
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->language_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->language_code:Ljava/lang/String;

    .line 417
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 418
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->phone_number:Ljava/lang/String;

    .line 419
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_name:Ljava/lang/String;

    .line 420
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    .line 421
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->website_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->website_url:Ljava/lang/String;

    .line 422
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    .line 423
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_email:Ljava/lang/String;

    .line 424
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->description:Ljava/lang/String;

    .line 425
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->twitter_username:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->twitter_username:Ljava/lang/String;

    .line 426
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->instagram_username:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->instagram_username:Ljava/lang/String;

    .line 427
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->facebook_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->facebook_url:Ljava/lang/String;

    .line 428
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    .line 429
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->logo_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->logo_url:Ljava/lang/String;

    .line 430
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->pos_background_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->pos_background_url:Ljava/lang/String;

    .line 431
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->mcc:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->mcc:Ljava/lang/String;

    .line 432
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Location;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Location;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Location$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 507
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_2

    const-string v1, ", address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->timezone:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", timezone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->capabilities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", capabilities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->capabilities:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 512
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    if-eqz v1, :cond_5

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 513
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", merchant_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    if-eqz v1, :cond_8

    const-string v1, ", country="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 516
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->language_code:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", language_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->language_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    if-eqz v1, :cond_a

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 518
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", phone_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_name:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", business_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    if-eqz v1, :cond_d

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 521
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->website_url:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", website_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->website_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    if-eqz v1, :cond_f

    const-string v1, ", business_hours="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 523
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_email:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", business_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->business_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->description:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 525
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->twitter_username:Ljava/lang/String;

    if-eqz v1, :cond_12

    const-string v1, ", twitter_username="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->twitter_username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->instagram_username:Ljava/lang/String;

    if-eqz v1, :cond_13

    const-string v1, ", instagram_username="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->instagram_username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->facebook_url:Ljava/lang/String;

    if-eqz v1, :cond_14

    const-string v1, ", facebook_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->facebook_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    if-eqz v1, :cond_15

    const-string v1, ", coordinates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 529
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->logo_url:Ljava/lang/String;

    if-eqz v1, :cond_16

    const-string v1, ", logo_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->logo_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->pos_background_url:Ljava/lang/String;

    if-eqz v1, :cond_17

    const-string v1, ", pos_background_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->pos_background_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->mcc:Ljava/lang/String;

    if-eqz v1, :cond_18

    const-string v1, ", mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Location;->mcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_18
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Location{"

    .line 532
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
