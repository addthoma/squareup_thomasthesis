.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;
.super Lcom/squareup/wire/Message;
.source "CatalogPricingRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$ProtoAdapter_CatalogPricingRule;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION_MODE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

.field public static final DEFAULT_APPLY_PRODUCTS_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DISCOUNT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DISCOUNT_TARGET_SCOPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

.field public static final DEFAULT_EXCLUDE_PRODUCTS_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_EXCLUDE_STRATEGY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

.field public static final DEFAULT_MATCH_PRODUCTS_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MAX_APPLICATIONS_PER_ATTACHMENT:Ljava/lang/Integer;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_STACKABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

.field public static final DEFAULT_VALID_FROM_DATE:Ljava/lang/String; = ""

.field public static final DEFAULT_VALID_FROM_LOCAL_TIME:Ljava/lang/String; = ""

.field public static final DEFAULT_VALID_UNTIL_DATE:Ljava/lang/String; = ""

.field public static final DEFAULT_VALID_UNTIL_LOCAL_TIME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogPricingRuleApplicationMode#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final apply_products_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final discount_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.DiscountTargetScope#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final exclude_products_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.ExcludeStrategy#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final match_products_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final max_applications_per_attachment:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x11
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.AggregationStrategy#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final time_period_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final valid_from_date:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final valid_from_local_time:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final valid_until_date:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final valid_until_local_time:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$ProtoAdapter_CatalogPricingRule;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$ProtoAdapter_CatalogPricingRule;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 41
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->EXCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->DEFAULT_STACKABLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    .line 53
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->DEFAULT_EXCLUDE_STRATEGY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    .line 55
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;->AUTOMATIC:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->DEFAULT_APPLICATION_MODE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    .line 57
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->LINE_ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->DEFAULT_DISCOUNT_TARGET_SCOPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    const/4 v0, -0x1

    .line 59
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->DEFAULT_MAX_APPLICATIONS_PER_ATTACHMENT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;Ljava/lang/Integer;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 266
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;",
            "Ljava/lang/Integer;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 275
    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 276
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->name:Ljava/lang/String;

    const-string v1, "time_period_ids"

    move-object v2, p2

    .line 277
    invoke-static {v1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->time_period_ids:Ljava/util/List;

    move-object v1, p3

    .line 278
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_id:Ljava/lang/String;

    move-object v1, p4

    .line 279
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->match_products_id:Ljava/lang/String;

    move-object v1, p5

    .line 280
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->apply_products_id:Ljava/lang/String;

    move-object v1, p6

    .line 281
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    move-object v1, p7

    .line 282
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_products_id:Ljava/lang/String;

    move-object v1, p8

    .line 283
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_date:Ljava/lang/String;

    move-object v1, p9

    .line 284
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_local_time:Ljava/lang/String;

    move-object v1, p10

    .line 285
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_date:Ljava/lang/String;

    move-object v1, p11

    .line 286
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_local_time:Ljava/lang/String;

    move-object v1, p12

    .line 287
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    move-object/from16 v1, p13

    .line 288
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    move-object/from16 v1, p14

    .line 289
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    move-object/from16 v1, p15

    .line 290
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 318
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 319
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    .line 320
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->name:Ljava/lang/String;

    .line 321
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->time_period_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->time_period_ids:Ljava/util/List;

    .line 322
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_id:Ljava/lang/String;

    .line 323
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->match_products_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->match_products_id:Ljava/lang/String;

    .line 324
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->apply_products_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->apply_products_id:Ljava/lang/String;

    .line 325
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    .line 326
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_products_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_products_id:Ljava/lang/String;

    .line 327
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_date:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_date:Ljava/lang/String;

    .line 328
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_local_time:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_local_time:Ljava/lang/String;

    .line 329
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_date:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_date:Ljava/lang/String;

    .line 330
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_local_time:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_local_time:Ljava/lang/String;

    .line 331
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    .line 332
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    .line 333
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    .line 334
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    .line 335
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 340
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_e

    .line 342
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->time_period_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->match_products_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->apply_products_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 348
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 349
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_products_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 350
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_date:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 351
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_local_time:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 352
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_date:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 353
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_local_time:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 356
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 357
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_d
    add-int/2addr v0, v2

    .line 358
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_e
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;
    .locals 2

    .line 295
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;-><init>()V

    .line 296
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->name:Ljava/lang/String;

    .line 297
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->time_period_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->time_period_ids:Ljava/util/List;

    .line 298
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->discount_id:Ljava/lang/String;

    .line 299
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->match_products_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->match_products_id:Ljava/lang/String;

    .line 300
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->apply_products_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->apply_products_id:Ljava/lang/String;

    .line 301
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    .line 302
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_products_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->exclude_products_id:Ljava/lang/String;

    .line 303
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_date:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_from_date:Ljava/lang/String;

    .line 304
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_local_time:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_from_local_time:Ljava/lang/String;

    .line 305
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_date:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_until_date:Ljava/lang/String;

    .line 306
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_local_time:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->valid_until_local_time:Ljava/lang/String;

    .line 307
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    .line 308
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    .line 309
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    .line 310
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->max_applications_per_attachment:Ljava/lang/Integer;

    .line 311
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 366
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->time_period_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", time_period_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->time_period_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 368
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", discount_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->match_products_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", match_products_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->match_products_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->apply_products_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", apply_products_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->apply_products_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    if-eqz v1, :cond_5

    const-string v1, ", stackable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->stackable:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/AggregationStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 372
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_products_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", exclude_products_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_products_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_date:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", valid_from_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_local_time:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", valid_from_local_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_from_local_time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_date:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", valid_until_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_local_time:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", valid_until_local_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->valid_until_local_time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    if-eqz v1, :cond_b

    const-string v1, ", exclude_strategy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->exclude_strategy:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ExcludeStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 378
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    if-eqz v1, :cond_c

    const-string v1, ", application_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->application_mode:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRuleApplicationMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 379
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    if-eqz v1, :cond_d

    const-string v1, ", discount_target_scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->discount_target_scope:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 380
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    const-string v1, ", max_applications_per_attachment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CatalogPricingRule{"

    .line 381
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
