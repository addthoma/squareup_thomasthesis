.class final Lcom/squareup/protos/connect/v2/common/TimeRange$ProtoAdapter_TimeRange;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TimeRange.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/TimeRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TimeRange"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/common/TimeRange;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 148
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/common/TimeRange;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 167
    new-instance v0, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;-><init>()V

    .line 168
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 169
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 174
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 172
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->end_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;

    goto :goto_0

    .line 171
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->start_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;

    goto :goto_0

    .line 178
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 179
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->build()Lcom/squareup/protos/connect/v2/common/TimeRange;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/TimeRange$ProtoAdapter_TimeRange;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/common/TimeRange;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/common/TimeRange;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 160
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/TimeRange;->start_at:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 161
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/TimeRange;->end_at:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 162
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/common/TimeRange;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    check-cast p2, Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/common/TimeRange$ProtoAdapter_TimeRange;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/common/TimeRange;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/common/TimeRange;)I
    .locals 4

    .line 153
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/common/TimeRange;->start_at:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/TimeRange;->end_at:Ljava/lang/String;

    const/4 v3, 0x2

    .line 154
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/TimeRange;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 146
    check-cast p1, Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/TimeRange$ProtoAdapter_TimeRange;->encodedSize(Lcom/squareup/protos/connect/v2/common/TimeRange;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/protos/connect/v2/common/TimeRange;
    .locals 0

    .line 184
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/TimeRange;->newBuilder()Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;

    move-result-object p1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 186
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/TimeRange$Builder;->build()Lcom/squareup/protos/connect/v2/common/TimeRange;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 146
    check-cast p1, Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/TimeRange$ProtoAdapter_TimeRange;->redact(Lcom/squareup/protos/connect/v2/common/TimeRange;)Lcom/squareup/protos/connect/v2/common/TimeRange;

    move-result-object p1

    return-object p1
.end method
