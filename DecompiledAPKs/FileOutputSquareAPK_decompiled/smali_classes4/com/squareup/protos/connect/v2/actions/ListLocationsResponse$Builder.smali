.class public final Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListLocationsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse;",
        "Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public locations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Location;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 111
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;->errors:Ljava/util/List;

    .line 112
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;->locations:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse;
    .locals 4

    .line 135
    new-instance v0, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;->locations:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;->build()Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse;

    move-result-object v0

    return-object v0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;"
        }
    .end annotation

    .line 119
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public locations(Ljava/util/List;)Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Location;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;"
        }
    .end annotation

    .line 128
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/actions/ListLocationsResponse$Builder;->locations:Ljava/util/List;

    return-object p0
.end method
