.class public final Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardPresentOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CardPresentOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/CardPresentOptions;",
        "Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public chip_card_fallback_indicator:Ljava/lang/String;

.field public is_emv_capable_reader_present:Ljava/lang/Boolean;

.field public offline_store_and_forward_created_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 136
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/CardPresentOptions;
    .locals 5

    .line 173
    new-instance v0, Lcom/squareup/protos/connect/v2/CardPresentOptions;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;->chip_card_fallback_indicator:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;->offline_store_and_forward_created_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/CardPresentOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;->build()Lcom/squareup/protos/connect/v2/CardPresentOptions;

    move-result-object v0

    return-object v0
.end method

.method public chip_card_fallback_indicator(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;->chip_card_fallback_indicator:Ljava/lang/String;

    return-object p0
.end method

.method public is_emv_capable_reader_present(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;->is_emv_capable_reader_present:Ljava/lang/Boolean;

    return-object p0
.end method

.method public offline_store_and_forward_created_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPresentOptions$Builder;->offline_store_and_forward_created_at:Ljava/lang/String;

    return-object p0
.end method
