.class public final Lcom/squareup/protos/connect/v2/CardPaymentDetails;
.super Lcom/squareup/wire/Message;
.source "CardPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/CardPaymentDetails$ProtoAdapter_CardPaymentDetails;,
        Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/CardPaymentDetails;",
        "Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/CardPaymentDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION_CRYPTOGRAM:Ljava/lang/String; = ""

.field public static final DEFAULT_APPLICATION_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_APPLICATION_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_AUTH_RESULT_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_AVS_STATUS:Ljava/lang/String; = ""

.field public static final DEFAULT_CVV_STATUS:Ljava/lang/String; = ""

.field public static final DEFAULT_ENTRY_METHOD:Ljava/lang/String; = ""

.field public static final DEFAULT_PAN_FIDELIUS_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_STATEMENT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Ljava/lang/String; = ""

.field public static final DEFAULT_VERIFICATION_METHOD:Ljava/lang/String; = ""

.field public static final DEFAULT_VERIFICATION_RESULTS:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final application_cryptogram:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final application_identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final application_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final auth_result_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final avs_status:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final card:Lcom/squareup/protos/connect/v2/resources/Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Card#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final cvv_status:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.DeviceDetails#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final entry_method:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Error#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public final pan_fidelius_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0xf
    .end annotation
.end field

.field public final statement_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final status:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final verification_method:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final verification_results:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$ProtoAdapter_CardPaymentDetails;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$ProtoAdapter_CardPaymentDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Card;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/DeviceDetails;Ljava/lang/String;Ljava/util/List;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/resources/Card;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/DeviceDetails;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 232
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Card;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/DeviceDetails;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Card;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/DeviceDetails;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/resources/Card;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/DeviceDetails;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 240
    sget-object v1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 241
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    move-object v1, p2

    .line 242
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    move-object v1, p3

    .line 243
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    move-object v1, p4

    .line 244
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    move-object v1, p5

    .line 245
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    move-object v1, p6

    .line 246
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    move-object v1, p7

    .line 247
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    move-object v1, p8

    .line 248
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    move-object v1, p9

    .line 249
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    move-object v1, p10

    .line 250
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    move-object v1, p11

    .line 251
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    move-object v1, p12

    .line 252
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    move-object/from16 v1, p13

    .line 253
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    move-object/from16 v1, p14

    .line 254
    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->pan_fidelius_token:Ljava/lang/String;

    const-string v1, "errors"

    move-object/from16 v2, p15

    .line 255
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 283
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 284
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    .line 285
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    .line 286
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    .line 287
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    .line 288
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    .line 289
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    .line 290
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    .line 291
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    .line 292
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    .line 293
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    .line 294
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    .line 295
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    .line 296
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    .line 297
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    .line 298
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->pan_fidelius_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->pan_fidelius_token:Ljava/lang/String;

    .line 299
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    .line 300
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 305
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_e

    .line 307
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 308
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 309
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Card;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 310
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 311
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 317
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 318
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/DeviceDetails;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->pan_fidelius_token:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_e
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 2

    .line 260
    new-instance v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;-><init>()V

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->status:Ljava/lang/String;

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->entry_method:Ljava/lang/String;

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->cvv_status:Ljava/lang/String;

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->avs_status:Ljava/lang/String;

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->auth_result_code:Ljava/lang/String;

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_identifier:Ljava/lang/String;

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_name:Ljava/lang/String;

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_cryptogram:Ljava/lang/String;

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->verification_method:Ljava/lang/String;

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->verification_results:Ljava/lang/String;

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->statement_description:Ljava/lang/String;

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    .line 274
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->pan_fidelius_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->pan_fidelius_token:Ljava/lang/String;

    .line 275
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->errors:Ljava/util/List;

    .line 276
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->newBuilder()Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 331
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    if-eqz v1, :cond_1

    const-string v1, ", card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 333
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", cvv_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", avs_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", auth_result_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", application_identifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", application_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", application_cryptogram="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", verification_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", verification_results="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", statement_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    if-eqz v1, :cond_c

    const-string v1, ", device_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 344
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->pan_fidelius_token:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", pan_fidelius_token=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardPaymentDetails{"

    .line 346
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
