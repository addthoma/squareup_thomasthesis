.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogTimePeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public event:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;
    .locals 3

    .line 133
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod$Builder;->event:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    move-result-object v0

    return-object v0
.end method

.method public event(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod$Builder;->event:Ljava/lang/String;

    return-object p0
.end method
