.class public final Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ExternalPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;",
        "Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public source:Ljava/lang/String;

.field public source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public source_id:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 165
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;
    .locals 7

    .line 225
    new-instance v6, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->type:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 156
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    move-result-object v0

    return-object v0
.end method

.method public source(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source:Ljava/lang/String;

    return-object p0
.end method

.method public source_fee_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public source_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_id:Ljava/lang/String;

    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->type:Ljava/lang/String;

    return-object p0
.end method
