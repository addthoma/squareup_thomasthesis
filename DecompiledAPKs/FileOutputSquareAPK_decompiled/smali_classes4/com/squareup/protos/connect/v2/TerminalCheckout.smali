.class public final Lcom/squareup/protos/connect/v2/TerminalCheckout;
.super Lcom/squareup/wire/Message;
.source "TerminalCheckout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/TerminalCheckout$ProtoAdapter_TerminalCheckout;,
        Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/TerminalCheckout;",
        "Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/TerminalCheckout;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CANCEL_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_DEADLINE:Ljava/lang/String; = ""

.field public static final DEFAULT_DEADLINE_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_OWNER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_REFERENCE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUIRED_INPUT:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Ljava/lang/String; = ""

.field public static final DEFAULT_TARGET_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Ljava/lang/String; = "DEVICE"

.field public static final DEFAULT_UPDATED_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final allowed_payment_methods:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final app_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final cancel_reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final deadline:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final deadline_duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field

.field public final device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.DeviceCheckoutOptions#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.CheckoutMetadata#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final options:Lcom/squareup/protos/connect/v2/PaymentOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.PaymentOptions#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final owner_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x12
    .end annotation
.end field

.field public final payment_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final reference_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final required_input:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final status:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final target_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x13
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final updated_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$ProtoAdapter_TerminalCheckout;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/TerminalCheckout$ProtoAdapter_TerminalCheckout;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;Lokio/ByteString;)V
    .locals 1

    .line 310
    sget-object v0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 311
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->id:Ljava/lang/String;

    .line 312
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->type:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->type:Ljava/lang/String;

    .line 313
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 314
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->reference_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->reference_id:Ljava/lang/String;

    .line 315
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->note:Ljava/lang/String;

    .line 316
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->allowed_payment_methods:Ljava/util/List;

    const-string v0, "allowed_payment_methods"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->allowed_payment_methods:Ljava/util/List;

    .line 317
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    .line 318
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    .line 319
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->deadline:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline:Ljava/lang/String;

    .line 320
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->deadline_duration:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline_duration:Ljava/lang/String;

    .line 321
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->status:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->status:Ljava/lang/String;

    .line 322
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->required_input:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->required_input:Ljava/lang/String;

    .line 323
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->cancel_reason:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->cancel_reason:Ljava/lang/String;

    .line 324
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->payment_ids:Ljava/util/List;

    const-string v0, "payment_ids"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->payment_ids:Ljava/util/List;

    .line 325
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->created_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->created_at:Ljava/lang/String;

    .line 326
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->updated_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->updated_at:Ljava/lang/String;

    .line 327
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->app_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->app_id:Ljava/lang/String;

    .line 328
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->owner_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->owner_token:Ljava/lang/String;

    .line 329
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->target_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->target_token:Ljava/lang/String;

    .line 330
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 363
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 364
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;

    .line 365
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TerminalCheckout;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/TerminalCheckout;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->id:Ljava/lang/String;

    .line 366
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->type:Ljava/lang/String;

    .line 367
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 368
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->reference_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->reference_id:Ljava/lang/String;

    .line 369
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->note:Ljava/lang/String;

    .line 370
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->allowed_payment_methods:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->allowed_payment_methods:Ljava/util/List;

    .line 371
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    .line 372
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    .line 373
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline:Ljava/lang/String;

    .line 374
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline_duration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline_duration:Ljava/lang/String;

    .line 375
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->status:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->status:Ljava/lang/String;

    .line 376
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->required_input:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->required_input:Ljava/lang/String;

    .line 377
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->cancel_reason:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->cancel_reason:Ljava/lang/String;

    .line 378
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->payment_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->payment_ids:Ljava/util/List;

    .line 379
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->created_at:Ljava/lang/String;

    .line 380
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->updated_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->updated_at:Ljava/lang/String;

    .line 381
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->app_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->app_id:Ljava/lang/String;

    .line 382
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->owner_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->owner_token:Ljava/lang/String;

    .line 383
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->target_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->target_token:Ljava/lang/String;

    .line 384
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/TerminalCheckout;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    .line 385
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 390
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_12

    .line 392
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TerminalCheckout;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 393
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 394
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->type:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 395
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 396
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 397
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->note:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 398
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->allowed_payment_methods:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 399
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/PaymentOptions;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 400
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 401
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 402
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline_duration:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 403
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->status:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 404
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->required_input:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 405
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 406
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->payment_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 407
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 408
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 409
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->app_id:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->owner_token:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 411
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->target_token:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 412
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/CheckoutMetadata;->hashCode()I

    move-result v2

    :cond_11
    add-int/2addr v0, v2

    .line 413
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_12
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 2

    .line 335
    new-instance v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;-><init>()V

    .line 336
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->id:Ljava/lang/String;

    .line 337
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->type:Ljava/lang/String;

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->reference_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->reference_id:Ljava/lang/String;

    .line 340
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->note:Ljava/lang/String;

    .line 341
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->allowed_payment_methods:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->allowed_payment_methods:Ljava/util/List;

    .line 342
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->deadline:Ljava/lang/String;

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline_duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->deadline_duration:Ljava/lang/String;

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->status:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->status:Ljava/lang/String;

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->required_input:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->required_input:Ljava/lang/String;

    .line 348
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->cancel_reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->cancel_reason:Ljava/lang/String;

    .line 349
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->payment_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->payment_ids:Ljava/util/List;

    .line 350
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->created_at:Ljava/lang/String;

    .line 351
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->updated_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->updated_at:Ljava/lang/String;

    .line 352
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->app_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->app_id:Ljava/lang/String;

    .line 353
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->owner_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->owner_token:Ljava/lang/String;

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->target_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->target_token:Ljava/lang/String;

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    .line 356
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TerminalCheckout;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TerminalCheckout;->newBuilder()Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 420
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 421
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->type:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 424
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->reference_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->note:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->allowed_payment_methods:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", allowed_payment_methods="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->allowed_payment_methods:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 427
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    if-eqz v1, :cond_6

    const-string v1, ", options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 428
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    if-eqz v1, :cond_7

    const-string v1, ", device_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 429
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", deadline="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 430
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline_duration:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", deadline_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->deadline_duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->status:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->required_input:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", required_input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->required_input:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", cancel_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->cancel_reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->payment_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", payment_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->payment_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 435
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->updated_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->app_id:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", app_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->app_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->owner_token:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", owner_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->owner_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->target_token:Ljava/lang/String;

    if-eqz v1, :cond_12

    const-string v1, ", target_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->target_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 440
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    if-eqz v1, :cond_13

    const-string v1, ", metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_13
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TerminalCheckout{"

    .line 441
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
