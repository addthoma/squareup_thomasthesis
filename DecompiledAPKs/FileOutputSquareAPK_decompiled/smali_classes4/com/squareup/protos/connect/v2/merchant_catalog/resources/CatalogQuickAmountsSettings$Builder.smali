.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogQuickAmountsSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;",
            ">;"
        }
    .end annotation
.end field

.field public eligible_for_auto_amounts:Ljava/lang/Boolean;

.field public option:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 135
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->amounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amounts(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;"
        }
    .end annotation

    .line 165
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->amounts:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;
    .locals 5

    .line 172
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->option:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->eligible_for_auto_amounts:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->amounts:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    move-result-object v0

    return-object v0
.end method

.method public eligible_for_auto_amounts(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->eligible_for_auto_amounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public option(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->option:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;

    return-object p0
.end method
