.class public final Lcom/squareup/protos/connect/v2/resources/Error;
.super Lcom/squareup/wire/Message;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Error$ProtoAdapter_Error;,
        Lcom/squareup/protos/connect/v2/resources/Error$Code;,
        Lcom/squareup/protos/connect/v2/resources/Error$Category;,
        Lcom/squareup/protos/connect/v2/resources/Error$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/resources/Error;",
        "Lcom/squareup/protos/connect/v2/resources/Error$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATEGORY:Lcom/squareup/protos/connect/v2/resources/Error$Category;

.field public static final DEFAULT_CODE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final DEFAULT_DETAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_FIELD:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final category:Lcom/squareup/protos/connect/v2/resources/Error$Category;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Error$Category#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final code:Lcom/squareup/protos/connect/v2/resources/Error$Code;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Error$Code#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final detail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final field:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$ProtoAdapter_Error;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Error$ProtoAdapter_Error;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 36
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->API_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->DEFAULT_CATEGORY:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 38
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INTERNAL_SERVER_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->DEFAULT_CODE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/resources/Error$Category;Lcom/squareup/protos/connect/v2/resources/Error$Code;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .line 95
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/resources/Error;-><init>(Lcom/squareup/protos/connect/v2/resources/Error$Category;Lcom/squareup/protos/connect/v2/resources/Error$Code;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/resources/Error$Category;Lcom/squareup/protos/connect/v2/resources/Error$Code;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 100
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 102
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 103
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/resources/Error;->detail:Ljava/lang/String;

    .line 104
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/resources/Error;->field:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 121
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/resources/Error;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 122
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Error;

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Error;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Error;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->detail:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Error;->detail:Ljava/lang/String;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->field:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Error;->field:Ljava/lang/String;

    .line 127
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 132
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Error;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Error$Category;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Error$Code;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->detail:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->field:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 139
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/resources/Error$Builder;
    .locals 2

    .line 109
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Error$Builder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->detail:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->detail:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->field:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->field:Ljava/lang/String;

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Error;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Error;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Error$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    if-eqz v1, :cond_0

    const-string v1, ", category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    if-eqz v1, :cond_1

    const-string v1, ", code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->detail:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", detail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->detail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->field:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", field="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Error{"

    .line 151
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
