.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;
.super Lcom/squareup/wire/Message;
.source "UpsertCatalogObjectRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$ProtoAdapter_UpsertCatalogObjectRequest;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IDEMPOTENCY_KEY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final idempotency_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogObject#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$ProtoAdapter_UpsertCatalogObjectRequest;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$ProtoAdapter_UpsertCatalogObjectRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V
    .locals 1

    .line 63
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;Lokio/ByteString;)V
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    .line 70
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 85
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 86
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 89
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 94
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 99
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;
    .locals 2

    .line 75
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;-><init>()V

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->idempotency_key:Ljava/lang/String;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", idempotency_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->idempotency_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    if-eqz v1, :cond_1

    const-string v1, ", object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;->object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UpsertCatalogObjectRequest{"

    .line 109
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
