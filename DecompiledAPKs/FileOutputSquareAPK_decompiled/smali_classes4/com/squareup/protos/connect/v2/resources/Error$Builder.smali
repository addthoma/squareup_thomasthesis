.class public final Lcom/squareup/protos/connect/v2/resources/Error$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Error;",
        "Lcom/squareup/protos/connect/v2/resources/Error$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

.field public code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public detail:Ljava/lang/String;

.field public field:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 163
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/resources/Error;
    .locals 7

    .line 214
    new-instance v6, Lcom/squareup/protos/connect/v2/resources/Error;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->detail:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->field:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/resources/Error;-><init>(Lcom/squareup/protos/connect/v2/resources/Error$Category;Lcom/squareup/protos/connect/v2/resources/Error$Code;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 154
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Error;

    move-result-object v0

    return-object v0
.end method

.method public category(Lcom/squareup/protos/connect/v2/resources/Error$Category;)Lcom/squareup/protos/connect/v2/resources/Error$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->category:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object p0
.end method

.method public code(Lcom/squareup/protos/connect/v2/resources/Error$Code;)Lcom/squareup/protos/connect/v2/resources/Error$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->code:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0
.end method

.method public detail(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Error$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->detail:Ljava/lang/String;

    return-object p0
.end method

.method public field(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Error$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Error$Builder;->field:Ljava/lang/String;

    return-object p0
.end method
