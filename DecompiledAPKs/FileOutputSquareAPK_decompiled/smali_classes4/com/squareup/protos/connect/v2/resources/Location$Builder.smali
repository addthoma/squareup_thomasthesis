.class public final Lcom/squareup/protos/connect/v2/resources/Location$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Location.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Location;",
        "Lcom/squareup/protos/connect/v2/resources/Location$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/connect/v2/resources/Address;

.field public business_email:Ljava/lang/String;

.field public business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

.field public business_name:Ljava/lang/String;

.field public capabilities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Location$Capability;",
            ">;"
        }
    .end annotation
.end field

.field public coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

.field public country:Lcom/squareup/protos/connect/v2/resources/Country;

.field public created_at:Ljava/lang/String;

.field public currency:Lcom/squareup/protos/connect/v2/common/Currency;

.field public description:Ljava/lang/String;

.field public facebook_url:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public instagram_username:Ljava/lang/String;

.field public language_code:Ljava/lang/String;

.field public logo_url:Ljava/lang/String;

.field public mcc:Ljava/lang/String;

.field public merchant_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public pos_background_url:Ljava/lang/String;

.field public status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

.field public timezone:Ljava/lang/String;

.field public twitter_username:Ljava/lang/String;

.field public type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

.field public website_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 586
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 587
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->capabilities:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 617
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/resources/Location;
    .locals 2

    .line 858
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/connect/v2/resources/Location;-><init>(Lcom/squareup/protos/connect/v2/resources/Location$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 535
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Location;

    move-result-object v0

    return-object v0
.end method

.method public business_email(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 775
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_email:Ljava/lang/String;

    return-object p0
.end method

.method public business_hours(Lcom/squareup/protos/connect/v2/resources/BusinessHours;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 763
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_hours:Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    return-object p0
.end method

.method public business_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 735
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->business_name:Ljava/lang/String;

    return-object p0
.end method

.method public capabilities(Ljava/util/List;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Location$Capability;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/resources/Location$Builder;"
        }
    .end annotation

    .line 641
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 642
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->capabilities:Ljava/util/List;

    return-object p0
.end method

.method public coordinates(Lcom/squareup/protos/connect/v2/common/Coordinates;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 815
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->coordinates:Lcom/squareup/protos/connect/v2/common/Coordinates;

    return-object p0
.end method

.method public country(Lcom/squareup/protos/connect/v2/resources/Country;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 690
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 664
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public currency(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 715
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 783
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public facebook_url(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 807
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->facebook_url:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 598
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public instagram_username(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 799
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->instagram_username:Ljava/lang/String;

    return-object p0
.end method

.method public language_code(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 701
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->language_code:Ljava/lang/String;

    return-object p0
.end method

.method public logo_url(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 827
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->logo_url:Ljava/lang/String;

    return-object p0
.end method

.method public mcc(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 852
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->mcc:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 676
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 609
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 723
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public pos_background_url(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 839
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->pos_background_url:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/connect/v2/resources/Location$Status;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 652
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Location$Status;

    return-object p0
.end method

.method public timezone(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 628
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->timezone:Ljava/lang/String;

    return-object p0
.end method

.method public twitter_username(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 791
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->twitter_username:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/connect/v2/resources/Location$Type;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 743
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->type:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    return-object p0
.end method

.method public website_url(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Builder;
    .locals 0

    .line 751
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Location$Builder;->website_url:Ljava/lang/String;

    return-object p0
.end method
