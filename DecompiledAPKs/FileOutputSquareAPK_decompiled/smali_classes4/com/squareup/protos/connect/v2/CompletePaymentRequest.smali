.class public final Lcom/squareup/protos/connect/v2/CompletePaymentRequest;
.super Lcom/squareup/wire/Message;
.source "CompletePaymentRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/CompletePaymentRequest$ProtoAdapter_CompletePaymentRequest;,
        Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/CompletePaymentRequest;",
        "Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/CompletePaymentRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENCRYPTED_EMV_DATA:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final encrypted_emv_data:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final payment_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final version_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest$ProtoAdapter_CompletePaymentRequest;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest$ProtoAdapter_CompletePaymentRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 76
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 81
    sget-object v0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 82
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->payment_id:Ljava/lang/String;

    .line 83
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->encrypted_emv_data:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->version_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 100
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 101
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->payment_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->payment_id:Ljava/lang/String;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->encrypted_emv_data:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->encrypted_emv_data:Ljava/lang/String;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->version_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->version_token:Ljava/lang/String;

    .line 105
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 110
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->payment_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->encrypted_emv_data:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->version_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 116
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;
    .locals 2

    .line 89
    new-instance v0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->payment_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;->payment_id:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->encrypted_emv_data:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;->encrypted_emv_data:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->version_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;->version_token:Ljava/lang/String;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->newBuilder()Lcom/squareup/protos/connect/v2/CompletePaymentRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->payment_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", payment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->payment_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->encrypted_emv_data:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", encrypted_emv_data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->version_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", version_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CompletePaymentRequest;->version_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CompletePaymentRequest{"

    .line 127
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
