.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchCatalogObjectsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cursor:Ljava/lang/String;

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public latest_time:Ljava/lang/String;

.field public objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public related_objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 163
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 164
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    .line 165
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    .line 166
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->related_objects:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;
    .locals 8

    .line 220
    new-instance v7, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->cursor:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->related_objects:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->latest_time:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;

    move-result-object v0

    return-object v0
.end method

.method public cursor(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->cursor:Ljava/lang/String;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 173
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public latest_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->latest_time:Ljava/lang/String;

    return-object p0
.end method

.method public objects(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 193
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    return-object p0
.end method

.method public related_objects(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 202
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse$Builder;->related_objects:Ljava/util/List;

    return-object p0
.end method
