.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogObjectBatch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 86
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;->objects:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;
    .locals 3

    .line 100
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;->objects:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;

    move-result-object v0

    return-object v0
.end method

.method public objects(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;"
        }
    .end annotation

    .line 93
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 94
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch$Builder;->objects:Ljava/util/List;

    return-object p0
.end method
