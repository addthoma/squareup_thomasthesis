.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Intermission.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public end_offset:Ljava/lang/Integer;

.field public start_offset:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission;
    .locals 4

    .line 127
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;->start_offset:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;->end_offset:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission;

    move-result-object v0

    return-object v0
.end method

.method public end_offset(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;->end_offset:Ljava/lang/Integer;

    return-object p0
.end method

.method public start_offset(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission$Builder;->start_offset:Ljava/lang/Integer;

    return-object p0
.end method
