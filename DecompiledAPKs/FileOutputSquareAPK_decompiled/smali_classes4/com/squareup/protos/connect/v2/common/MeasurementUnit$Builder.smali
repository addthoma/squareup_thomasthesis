.class public final Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

.field public custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

.field public generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

.field public length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

.field public type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 235
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 11

    .line 321
    new-instance v10, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    iget-object v8, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 218
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v0

    return-object v0
.end method

.method public custom_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    return-object p0
.end method

.method public generic_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    return-object p0
.end method

.method public length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    return-object p0
.end method

.method public time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0
.end method

.method public volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0
.end method

.method public weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 0

    .line 285
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    return-object p0
.end method
