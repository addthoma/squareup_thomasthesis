.class public final enum Lcom/squareup/protos/deposits/BalanceType;
.super Ljava/lang/Enum;
.source "BalanceType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/BalanceType$ProtoAdapter_BalanceType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/deposits/BalanceType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/deposits/BalanceType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/BalanceType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum MANUAL_SETTLEMENT:Lcom/squareup/protos/deposits/BalanceType;

.field public static final enum NO_STORED_BALANCE:Lcom/squareup/protos/deposits/BalanceType;

.field public static final enum SQUARE_CARD:Lcom/squareup/protos/deposits/BalanceType;

.field public static final enum UNKNOWN_BALANCE_TYPE_DO_NOT_USE:Lcom/squareup/protos/deposits/BalanceType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 15
    new-instance v0, Lcom/squareup/protos/deposits/BalanceType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_BALANCE_TYPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/deposits/BalanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/deposits/BalanceType;->UNKNOWN_BALANCE_TYPE_DO_NOT_USE:Lcom/squareup/protos/deposits/BalanceType;

    .line 20
    new-instance v0, Lcom/squareup/protos/deposits/BalanceType;

    const/4 v2, 0x1

    const-string v3, "NO_STORED_BALANCE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/deposits/BalanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/deposits/BalanceType;->NO_STORED_BALANCE:Lcom/squareup/protos/deposits/BalanceType;

    .line 25
    new-instance v0, Lcom/squareup/protos/deposits/BalanceType;

    const/4 v3, 0x2

    const-string v4, "SQUARE_CARD"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/deposits/BalanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/deposits/BalanceType;->SQUARE_CARD:Lcom/squareup/protos/deposits/BalanceType;

    .line 30
    new-instance v0, Lcom/squareup/protos/deposits/BalanceType;

    const/4 v4, 0x3

    const-string v5, "MANUAL_SETTLEMENT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/deposits/BalanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/deposits/BalanceType;->MANUAL_SETTLEMENT:Lcom/squareup/protos/deposits/BalanceType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/deposits/BalanceType;

    .line 14
    sget-object v5, Lcom/squareup/protos/deposits/BalanceType;->UNKNOWN_BALANCE_TYPE_DO_NOT_USE:Lcom/squareup/protos/deposits/BalanceType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceType;->NO_STORED_BALANCE:Lcom/squareup/protos/deposits/BalanceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/deposits/BalanceType;->SQUARE_CARD:Lcom/squareup/protos/deposits/BalanceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/deposits/BalanceType;->MANUAL_SETTLEMENT:Lcom/squareup/protos/deposits/BalanceType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/deposits/BalanceType;->$VALUES:[Lcom/squareup/protos/deposits/BalanceType;

    .line 32
    new-instance v0, Lcom/squareup/protos/deposits/BalanceType$ProtoAdapter_BalanceType;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceType$ProtoAdapter_BalanceType;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/BalanceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput p3, p0, Lcom/squareup/protos/deposits/BalanceType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/deposits/BalanceType;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 48
    :cond_0
    sget-object p0, Lcom/squareup/protos/deposits/BalanceType;->MANUAL_SETTLEMENT:Lcom/squareup/protos/deposits/BalanceType;

    return-object p0

    .line 47
    :cond_1
    sget-object p0, Lcom/squareup/protos/deposits/BalanceType;->SQUARE_CARD:Lcom/squareup/protos/deposits/BalanceType;

    return-object p0

    .line 46
    :cond_2
    sget-object p0, Lcom/squareup/protos/deposits/BalanceType;->NO_STORED_BALANCE:Lcom/squareup/protos/deposits/BalanceType;

    return-object p0

    .line 45
    :cond_3
    sget-object p0, Lcom/squareup/protos/deposits/BalanceType;->UNKNOWN_BALANCE_TYPE_DO_NOT_USE:Lcom/squareup/protos/deposits/BalanceType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceType;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/protos/deposits/BalanceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/deposits/BalanceType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/deposits/BalanceType;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/protos/deposits/BalanceType;->$VALUES:[Lcom/squareup/protos/deposits/BalanceType;

    invoke-virtual {v0}, [Lcom/squareup/protos/deposits/BalanceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/deposits/BalanceType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/squareup/protos/deposits/BalanceType;->value:I

    return v0
.end method
