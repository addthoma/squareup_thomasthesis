.class final Lcom/squareup/protos/deposits/InstrumentView$ProtoAdapter_InstrumentView;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InstrumentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/InstrumentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InstrumentView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/deposits/InstrumentView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 531
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/deposits/InstrumentView;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/InstrumentView;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 556
    new-instance v0, Lcom/squareup/protos/deposits/InstrumentView$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/InstrumentView$Builder;-><init>()V

    .line 557
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 558
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 566
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 564
    :cond_0
    sget-object v3, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_summary(Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;)Lcom/squareup/protos/deposits/InstrumentView$Builder;

    goto :goto_0

    .line 563
    :cond_1
    sget-object v3, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_summary(Lcom/squareup/protos/deposits/InstrumentView$CardSummary;)Lcom/squareup/protos/deposits/InstrumentView$Builder;

    goto :goto_0

    .line 562
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_number_suffix(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$Builder;

    goto :goto_0

    .line 561
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$Builder;

    goto :goto_0

    .line 560
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->instrument_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/InstrumentView$Builder;

    goto :goto_0

    .line 570
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 571
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->build()Lcom/squareup/protos/deposits/InstrumentView;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 529
    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/InstrumentView$ProtoAdapter_InstrumentView;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/InstrumentView;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/InstrumentView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 546
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 547
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 548
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 549
    sget-object v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 550
    sget-object v0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 551
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/InstrumentView;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 529
    check-cast p2, Lcom/squareup/protos/deposits/InstrumentView;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/deposits/InstrumentView$ProtoAdapter_InstrumentView;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/InstrumentView;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/deposits/InstrumentView;)I
    .locals 4

    .line 536
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/InstrumentView;->instrument_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/InstrumentView;->card_pan_suffix:Ljava/lang/String;

    const/4 v3, 0x2

    .line 537
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_number_suffix:Ljava/lang/String;

    const/4 v3, 0x3

    .line 538
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/InstrumentView;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    const/4 v3, 0x4

    .line 539
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/InstrumentView;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    const/4 v3, 0x5

    .line 540
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 541
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 529
    check-cast p1, Lcom/squareup/protos/deposits/InstrumentView;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/InstrumentView$ProtoAdapter_InstrumentView;->encodedSize(Lcom/squareup/protos/deposits/InstrumentView;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/deposits/InstrumentView;)Lcom/squareup/protos/deposits/InstrumentView;
    .locals 2

    .line 576
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView;->newBuilder()Lcom/squareup/protos/deposits/InstrumentView$Builder;

    move-result-object p1

    .line 577
    iget-object v0, p1, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    iput-object v0, p1, Lcom/squareup/protos/deposits/InstrumentView$Builder;->card_summary:Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    .line 578
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    iput-object v0, p1, Lcom/squareup/protos/deposits/InstrumentView$Builder;->bank_account_summary:Lcom/squareup/protos/deposits/InstrumentView$BankAccountSummary;

    .line 579
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 580
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView$Builder;->build()Lcom/squareup/protos/deposits/InstrumentView;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 529
    check-cast p1, Lcom/squareup/protos/deposits/InstrumentView;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/InstrumentView$ProtoAdapter_InstrumentView;->redact(Lcom/squareup/protos/deposits/InstrumentView;)Lcom/squareup/protos/deposits/InstrumentView;

    move-result-object p1

    return-object p1
.end method
