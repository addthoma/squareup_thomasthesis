.class public final Lcom/squareup/protos/deposits/BalanceActivity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BalanceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/BalanceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/BalanceActivity;",
        "Lcom/squareup/protos/deposits/BalanceActivity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/common/Money;

.field public balance_type:Lcom/squareup/protos/deposits/BalanceType;

.field public client_ip:Ljava/lang/String;

.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

.field public external_token:Ljava/lang/String;

.field public failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

.field public fee_amount:Lcom/squareup/protos/common/Money;

.field public fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

.field public pagination_token:Ljava/lang/String;

.field public source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

.field public state:Lcom/squareup/protos/deposits/BalanceActivity$State;

.field public sync_id:Ljava/lang/Long;

.field public token:Ljava/lang/String;

.field public type:Lcom/squareup/protos/deposits/BalanceActivityType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 336
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 359
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public balance_type(Lcom/squareup/protos/deposits/BalanceType;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 433
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/BalanceActivity;
    .locals 20

    move-object/from16 v0, p0

    .line 463
    new-instance v18, Lcom/squareup/protos/deposits/BalanceActivity;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iget-object v3, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iget-object v4, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    iget-object v5, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    iget-object v6, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    iget-object v7, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    iget-object v8, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->token:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v10, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    iget-object v11, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->pagination_token:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->sync_id:Ljava/lang/Long;

    iget-object v13, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    iget-object v14, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->client_ip:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->external_token:Ljava/lang/String;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/protos/deposits/BalanceActivity;-><init>(Lcom/squareup/protos/deposits/BalanceActivityEntityView;Lcom/squareup/protos/deposits/BalanceActivityEntityView;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/ledger/service/FeeStructure;Lcom/squareup/protos/deposits/BalanceActivityType;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/deposits/BalanceActivity$State;Ljava/lang/String;Ljava/lang/Long;Lcom/squareup/protos/deposits/BalanceType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/BalanceActivityFailure;Lokio/ByteString;)V

    return-object v18
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 305
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivity;

    move-result-object v0

    return-object v0
.end method

.method public client_ip(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 441
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->client_ip:Ljava/lang/String;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 400
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public destination(Lcom/squareup/protos/deposits/BalanceActivityEntityView;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    return-object p0
.end method

.method public external_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 449
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->external_token:Ljava/lang/String;

    return-object p0
.end method

.method public failure(Lcom/squareup/protos/deposits/BalanceActivityFailure;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 457
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    return-object p0
.end method

.method public fee_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public fee_structure(Lcom/squareup/protos/ledger/service/FeeStructure;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 375
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 416
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method

.method public source(Lcom/squareup/protos/deposits/BalanceActivityEntityView;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 343
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/deposits/BalanceActivity$State;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 408
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    return-object p0
.end method

.method public sync_id(Ljava/lang/Long;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 425
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->sync_id:Ljava/lang/Long;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 392
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/deposits/BalanceActivityType;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    .locals 0

    .line 383
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    return-object p0
.end method
