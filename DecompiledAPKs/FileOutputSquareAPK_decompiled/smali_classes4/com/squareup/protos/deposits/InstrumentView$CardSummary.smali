.class public final Lcom/squareup/protos/deposits/InstrumentView$CardSummary;
.super Lcom/squareup/wire/Message;
.source "InstrumentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/InstrumentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardSummary"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/InstrumentView$CardSummary$ProtoAdapter_CardSummary;,
        Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/deposits/InstrumentView$CardSummary;",
        "Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/InstrumentView$CardSummary;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAND:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final DEFAULT_PAN_SUFFIX:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.simple_instrument_store.model.Brand#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 220
    new-instance v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$ProtoAdapter_CardSummary;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$ProtoAdapter_CardSummary;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 226
    sget-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->INVALID:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    sput-object v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->DEFAULT_BRAND:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/Brand;)V
    .locals 1

    .line 247
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;-><init>(Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/Brand;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/Brand;Lokio/ByteString;)V
    .locals 1

    .line 251
    sget-object v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 252
    iput-object p1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->pan_suffix:Ljava/lang/String;

    .line 253
    iput-object p2, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 268
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 269
    :cond_1
    check-cast p1, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;

    .line 270
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->pan_suffix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->pan_suffix:Ljava/lang/String;

    .line 271
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    iget-object p1, p1, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 272
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 277
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 279
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 280
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->pan_suffix:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 281
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/simple_instrument_store/model/Brand;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 282
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;
    .locals 2

    .line 258
    new-instance v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;-><init>()V

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;->pan_suffix:Ljava/lang/String;

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    iput-object v1, v0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 261
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 219
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->newBuilder()Lcom/squareup/protos/deposits/InstrumentView$CardSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 290
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", pan_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    if-eqz v1, :cond_1

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/InstrumentView$CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardSummary{"

    .line 292
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
