.class public final enum Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;
.super Ljava/lang/Enum;
.source "GetDirectDebitInfoResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GetDirectDebitInfoError"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError$ProtoAdapter_GetDirectDebitInfoError;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GET_DIRECT_DEBIT_INFO_ERROR_DO_NOT_USE:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

.field public static final enum NO_ACCOUNT_OWNER:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

.field public static final enum RATE_LIMITED:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 253
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    const/4 v1, 0x0

    const-string v2, "GET_DIRECT_DEBIT_INFO_ERROR_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->GET_DIRECT_DEBIT_INFO_ERROR_DO_NOT_USE:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    .line 258
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    const/4 v2, 0x1

    const-string v3, "RATE_LIMITED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->RATE_LIMITED:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    .line 263
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    const/4 v3, 0x2

    const-string v4, "NO_ACCOUNT_OWNER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->NO_ACCOUNT_OWNER:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    .line 249
    sget-object v4, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->GET_DIRECT_DEBIT_INFO_ERROR_DO_NOT_USE:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->RATE_LIMITED:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->NO_ACCOUNT_OWNER:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->$VALUES:[Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    .line 265
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError$ProtoAdapter_GetDirectDebitInfoError;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError$ProtoAdapter_GetDirectDebitInfoError;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 269
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 270
    iput p3, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 280
    :cond_0
    sget-object p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->NO_ACCOUNT_OWNER:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    return-object p0

    .line 279
    :cond_1
    sget-object p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->RATE_LIMITED:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    return-object p0

    .line 278
    :cond_2
    sget-object p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->GET_DIRECT_DEBIT_INFO_ERROR_DO_NOT_USE:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;
    .locals 1

    .line 249
    const-class v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;
    .locals 1

    .line 249
    sget-object v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->$VALUES:[Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    invoke-virtual {v0}, [Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 287
    iget v0, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;->value:I

    return v0
.end method
