.class public final Lcom/squareup/protos/sawmill/EventstreamV2Event;
.super Lcom/squareup/wire/Message;
.source "EventstreamV2Event.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/sawmill/EventstreamV2Event$ProtoAdapter_EventstreamV2Event;,
        Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
        "Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ES2_DEBUG_TRACE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_JSON_DATA:Ljava/lang/String; = ""

.field public static final DEFAULT_RECORDED_AT_USEC:Ljava/lang/Long;

.field public static final DEFAULT_SECRET_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_UUID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final app_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final catalog_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final es2_debug_trace_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final json_data:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final recorded_at_usec:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final secret_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final uuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$ProtoAdapter_EventstreamV2Event;

    invoke-direct {v0}, Lcom/squareup/protos/sawmill/EventstreamV2Event$ProtoAdapter_EventstreamV2Event;-><init>()V

    sput-object v0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 31
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->DEFAULT_RECORDED_AT_USEC:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .line 89
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/sawmill/EventstreamV2Event;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->uuid:Ljava/lang/String;

    .line 97
    iput-object p2, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->catalog_name:Ljava/lang/String;

    .line 98
    iput-object p3, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->app_name:Ljava/lang/String;

    .line 99
    iput-object p4, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->recorded_at_usec:Ljava/lang/Long;

    .line 100
    iput-object p5, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->json_data:Ljava/lang/String;

    .line 101
    iput-object p6, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->secret_token:Ljava/lang/String;

    .line 102
    iput-object p7, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->es2_debug_trace_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 122
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 123
    :cond_1
    check-cast p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/sawmill/EventstreamV2Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/sawmill/EventstreamV2Event;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->uuid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;->uuid:Ljava/lang/String;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->catalog_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;->catalog_name:Ljava/lang/String;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->app_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;->app_name:Ljava/lang/String;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->recorded_at_usec:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;->recorded_at_usec:Ljava/lang/Long;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->json_data:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;->json_data:Ljava/lang/String;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->secret_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;->secret_token:Ljava/lang/String;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->es2_debug_trace_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/sawmill/EventstreamV2Event;->es2_debug_trace_id:Ljava/lang/String;

    .line 131
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 136
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/sawmill/EventstreamV2Event;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->uuid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->catalog_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->app_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->recorded_at_usec:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->json_data:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->secret_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->es2_debug_trace_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 146
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;
    .locals 2

    .line 107
    new-instance v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;-><init>()V

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->uuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->uuid:Ljava/lang/String;

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->catalog_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->catalog_name:Ljava/lang/String;

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->app_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->app_name:Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->recorded_at_usec:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->recorded_at_usec:Ljava/lang/Long;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->json_data:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->json_data:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->secret_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->secret_token:Ljava/lang/String;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->es2_debug_trace_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->es2_debug_trace_id:Ljava/lang/String;

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/sawmill/EventstreamV2Event;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/sawmill/EventstreamV2Event;->newBuilder()Lcom/squareup/protos/sawmill/EventstreamV2Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->uuid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->catalog_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", catalog_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->catalog_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->app_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", app_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->app_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->recorded_at_usec:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", recorded_at_usec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->recorded_at_usec:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->json_data:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", json_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->json_data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->secret_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", secret_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->secret_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->es2_debug_trace_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", es2_debug_trace_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/EventstreamV2Event;->es2_debug_trace_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EventstreamV2Event{"

    .line 161
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
