.class public final enum Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;
.super Ljava/lang/Enum;
.source "OnlineStoreItemData.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/items/OnlineStoreItemData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OnlineStoreItemType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType$ProtoAdapter_OnlineStoreItemType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DONATION:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

.field public static final enum EVENT:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

.field public static final enum GENERIC:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

.field public static final enum GIFT_CARD:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

.field public static final enum SERVICE:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

.field public static final enum UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 206
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_DO_NOT_SET"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 208
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v2, 0x1

    const-string v3, "GENERIC"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->GENERIC:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 210
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v3, 0x2

    const-string v4, "EVENT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->EVENT:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 212
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v4, 0x3

    const-string v5, "SERVICE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->SERVICE:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 214
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v5, 0x4

    const-string v6, "DONATION"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->DONATION:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 216
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v6, 0x5

    const-string v7, "GIFT_CARD"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->GIFT_CARD:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 202
    sget-object v7, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->GENERIC:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->EVENT:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->SERVICE:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->DONATION:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->GIFT_CARD:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->$VALUES:[Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    .line 218
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType$ProtoAdapter_OnlineStoreItemType;

    invoke-direct {v0}, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType$ProtoAdapter_OnlineStoreItemType;-><init>()V

    sput-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 222
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 223
    iput p3, p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 236
    :cond_0
    sget-object p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->GIFT_CARD:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object p0

    .line 235
    :cond_1
    sget-object p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->DONATION:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object p0

    .line 234
    :cond_2
    sget-object p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->SERVICE:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object p0

    .line 233
    :cond_3
    sget-object p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->EVENT:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object p0

    .line 232
    :cond_4
    sget-object p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->GENERIC:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object p0

    .line 231
    :cond_5
    sget-object p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;
    .locals 1

    .line 202
    const-class v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;
    .locals 1

    .line 202
    sget-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->$VALUES:[Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    invoke-virtual {v0}, [Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 243
    iget v0, p0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->value:I

    return v0
.end method
