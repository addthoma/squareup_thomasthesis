.class public final Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;
.super Lcom/squareup/wire/Message;
.source "R4CardData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$ProtoAdapter_R4CardData;,
        Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;,
        Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BIT_PERIOD_BIT_140:Ljava/lang/Integer;

.field public static final DEFAULT_BIT_PERIOD_BWD:Ljava/lang/Integer;

.field public static final DEFAULT_BIT_PERIOD_END_OF_SWIPE:Ljava/lang/Integer;

.field public static final DEFAULT_COUNTER:Ljava/lang/Long;

.field public static final DEFAULT_ENTROPY:Ljava/lang/Long;

.field public static final DEFAULT_NAME_LENGTH:Ljava/lang/Integer;

.field public static final DEFAULT_NAME_NOT_BLANK:Ljava/lang/Boolean;

.field public static final DEFAULT_TRACK1_LEN:Ljava/lang/Integer;

.field public static final DEFAULT_TRACK2_LEN:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final bit_period_bit_140:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field

.field public final bit_period_bwd:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final bit_period_end_of_swipe:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final counter:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final entropy:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final name_length:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xc
    .end annotation
.end field

.field public final name_not_blank:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final track1_len:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.R4CardData$R4TrackDecodeResult#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final track2_len:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x9
    .end annotation
.end field

.field public final track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.R4CardData$R4TrackDecodeResult#ADAPTER"
        tag = 0xb
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$ProtoAdapter_R4CardData;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$ProtoAdapter_R4CardData;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 30
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_COUNTER:Ljava/lang/Long;

    .line 32
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_ENTROPY:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_BIT_PERIOD_BWD:Ljava/lang/Integer;

    .line 36
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_BIT_PERIOD_BIT_140:Ljava/lang/Integer;

    .line 38
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_BIT_PERIOD_END_OF_SWIPE:Ljava/lang/Integer;

    .line 40
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_TRACK1_LEN:Ljava/lang/Integer;

    .line 42
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_TRACK2_LEN:Ljava/lang/Integer;

    .line 44
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_NAME_LENGTH:Ljava/lang/Integer;

    .line 46
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->DEFAULT_NAME_NOT_BLANK:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 13

    .line 180
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 187
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    .line 189
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    .line 190
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    .line 191
    iput-object p4, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    .line 192
    iput-object p5, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    .line 193
    iput-object p6, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    .line 194
    iput-object p7, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    .line 195
    iput-object p8, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 196
    iput-object p9, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 197
    iput-object p10, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    .line 198
    iput-object p11, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 222
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 223
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    .line 224
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    .line 235
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 240
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 242
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 254
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;
    .locals 2

    .line 203
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;-><init>()V

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->counter:Ljava/lang/Long;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->entropy:Ljava/lang/Long;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bwd:Ljava/lang/Integer;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_bit_140:Ljava/lang/Integer;

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->bit_period_end_of_swipe:Ljava/lang/Integer;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_len:Ljava/lang/Integer;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_len:Ljava/lang/Integer;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_length:Ljava/lang/Integer;

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->name_not_blank:Ljava/lang/Boolean;

    .line 215
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", counter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->counter:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", entropy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->entropy:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", bit_period_bwd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bwd:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 265
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", bit_period_bit_140="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_bit_140:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 266
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", bit_period_end_of_swipe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->bit_period_end_of_swipe:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 267
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", track1_len="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_len:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 268
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", track2_len="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_len:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    if-eqz v1, :cond_7

    const-string v1, ", track1_result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track1_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 270
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    if-eqz v1, :cond_8

    const-string v1, ", track2_result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->track2_result:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData$R4TrackDecodeResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    const-string v1, ", name_length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_length:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 272
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", name_not_blank="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->name_not_blank:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "R4CardData{"

    .line 273
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
