.class public final Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;
.super Lcom/squareup/wire/Message;
.source "O1Packet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$ProtoAdapter_O1Packet;,
        Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TOTAL_LENGTH:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.O1FlashError#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.O1GeneralError#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.O1SuccessfulSwipe#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final total_length:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$ProtoAdapter_O1Packet;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$ProtoAdapter_O1Packet;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->DEFAULT_TOTAL_LENGTH:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;)V
    .locals 6

    .line 66
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;Lokio/ByteString;)V
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    .line 73
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    .line 74
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    .line 75
    iput-object p4, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 92
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    .line 98
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 103
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 110
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->total_length:Ljava/lang/Integer;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", total_length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->total_length:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    if-eqz v1, :cond_1

    const-string v1, ", o1_successful_swipe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    if-eqz v1, :cond_2

    const-string v1, ", o1_general_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    if-eqz v1, :cond_3

    const-string v1, ", o1_flash_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "O1Packet{"

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
