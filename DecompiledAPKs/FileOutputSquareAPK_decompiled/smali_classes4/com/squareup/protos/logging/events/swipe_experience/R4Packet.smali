.class public final Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;
.super Lcom/squareup/wire/Message;
.source "R4Packet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$ProtoAdapter_R4Packet;,
        Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;",
        "Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HARDWARE_MAJOR_REVISION:Ljava/lang/Integer;

.field public static final DEFAULT_HARDWARE_MINOR_REVISION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final hardware_major_revision:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final hardware_minor_revision:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.R4CardData#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$ProtoAdapter_R4Packet;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$ProtoAdapter_R4Packet;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->DEFAULT_HARDWARE_MAJOR_REVISION:Ljava/lang/Integer;

    .line 27
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->DEFAULT_HARDWARE_MINOR_REVISION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;)V
    .locals 1

    .line 71
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;Lokio/ByteString;)V
    .locals 1

    .line 76
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 77
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_major_revision:Ljava/lang/Integer;

    .line 78
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_minor_revision:Ljava/lang/Integer;

    .line 79
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 95
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_major_revision:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_major_revision:Ljava/lang/Integer;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_minor_revision:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_minor_revision:Ljava/lang/Integer;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    .line 100
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 105
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_major_revision:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_minor_revision:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 111
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;
    .locals 2

    .line 84
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_major_revision:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->hardware_major_revision:Ljava/lang/Integer;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_minor_revision:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->hardware_minor_revision:Ljava/lang/Integer;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/R4Packet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_major_revision:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", hardware_major_revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_major_revision:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_minor_revision:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", hardware_minor_revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->hardware_minor_revision:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    if-eqz v1, :cond_2

    const-string v1, ", r4_card_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/R4Packet;->r4_card_data:Lcom/squareup/protos/logging/events/swipe_experience/R4CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "R4Packet{"

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
