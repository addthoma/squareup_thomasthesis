.class public final Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;
.super Lcom/squareup/wire/Message;
.source "ReaderCarrierDetectEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$ProtoAdapter_ReaderCarrierDetectEvent;,
        Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;,
        Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;",
        "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

.field private static final serialVersionUID:J


# instance fields
.field public final carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.CarrierDetectInfo#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.ReaderCarrierDetectEvent$Event#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SignalFound#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$ProtoAdapter_ReaderCarrierDetectEvent;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$ProtoAdapter_ReaderCarrierDetectEvent;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->SIGNAL_FOUND:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->DEFAULT_EVENT:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)V
    .locals 1

    .line 64
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;Lokio/ByteString;)V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 71
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    .line 72
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 88
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 89
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    .line 93
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 98
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;
    .locals 2

    .line 77
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    if-eqz v1, :cond_0

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    if-eqz v1, :cond_1

    const-string v1, ", carrier_detect_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    if-eqz v1, :cond_2

    const-string v1, ", signal_found="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReaderCarrierDetectEvent{"

    .line 115
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
