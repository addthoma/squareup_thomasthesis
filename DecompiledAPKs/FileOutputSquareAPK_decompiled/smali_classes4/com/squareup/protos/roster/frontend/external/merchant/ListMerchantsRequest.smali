.class public final Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;
.super Lcom/squareup/wire/Message;
.source "ListMerchantsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$ProtoAdapter_ListMerchantsRequest;,
        Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;",
        "Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CURSOR:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final cursor:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$ProtoAdapter_ListMerchantsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$ProtoAdapter_ListMerchantsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->DEFAULT_CURSOR:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 1

    .line 42
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;-><init>(Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->cursor:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 61
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 62
    :cond_1
    check-cast p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;

    .line 63
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->cursor:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->cursor:Ljava/lang/Integer;

    .line 64
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 69
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->cursor:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 73
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$Builder;
    .locals 2

    .line 52
    new-instance v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$Builder;-><init>()V

    .line 53
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->cursor:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$Builder;->cursor:Ljava/lang/Integer;

    .line 54
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->newBuilder()Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->cursor:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", cursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsRequest;->cursor:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListMerchantsRequest{"

    .line 82
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
