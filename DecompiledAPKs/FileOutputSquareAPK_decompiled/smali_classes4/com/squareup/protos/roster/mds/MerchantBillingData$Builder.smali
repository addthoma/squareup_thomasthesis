.class public final Lcom/squareup/protos/roster/mds/MerchantBillingData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MerchantBillingData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/roster/mds/MerchantBillingData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/roster/mds/MerchantBillingData;",
        "Lcom/squareup/protos/roster/mds/MerchantBillingData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public subscription_feature_status:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 87
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 88
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/roster/mds/MerchantBillingData$Builder;->subscription_feature_status:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/roster/mds/MerchantBillingData;
    .locals 3

    .line 103
    new-instance v0, Lcom/squareup/protos/roster/mds/MerchantBillingData;

    iget-object v1, p0, Lcom/squareup/protos/roster/mds/MerchantBillingData$Builder;->subscription_feature_status:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/roster/mds/MerchantBillingData;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/roster/mds/MerchantBillingData$Builder;->build()Lcom/squareup/protos/roster/mds/MerchantBillingData;

    move-result-object v0

    return-object v0
.end method

.method public subscription_feature_status(Ljava/util/List;)Lcom/squareup/protos/roster/mds/MerchantBillingData$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/roster/common/SubscriptionFeatureStatus;",
            ">;)",
            "Lcom/squareup/protos/roster/mds/MerchantBillingData$Builder;"
        }
    .end annotation

    .line 96
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/roster/mds/MerchantBillingData$Builder;->subscription_feature_status:Ljava/util/List;

    return-object p0
.end method
