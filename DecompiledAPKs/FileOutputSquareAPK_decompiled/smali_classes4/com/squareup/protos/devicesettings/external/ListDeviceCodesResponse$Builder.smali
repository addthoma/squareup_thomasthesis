.class public final Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListDeviceCodesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse;",
        "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cursor:Ljava/lang/String;

.field public device_codes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/devicesettings/external/DeviceCode;",
            ">;"
        }
    .end annotation
.end field

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 129
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 130
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->errors:Ljava/util/List;

    .line 131
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->device_codes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse;
    .locals 5

    .line 172
    new-instance v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->device_codes:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->cursor:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->build()Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse;

    move-result-object v0

    return-object v0
.end method

.method public cursor(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->cursor:Ljava/lang/String;

    return-object p0
.end method

.method public device_codes(Ljava/util/List;)Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/devicesettings/external/DeviceCode;",
            ">;)",
            "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;"
        }
    .end annotation

    .line 151
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->device_codes:Ljava/util/List;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;"
        }
    .end annotation

    .line 140
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method
