.class public final Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ResetDeviceCodeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse;",
        "Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public device_code:Lcom/squareup/protos/devicesettings/external/DeviceCode;

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 105
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse;
    .locals 4

    .line 131
    new-instance v0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;->device_code:Lcom/squareup/protos/devicesettings/external/DeviceCode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/devicesettings/external/DeviceCode;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;->build()Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse;

    move-result-object v0

    return-object v0
.end method

.method public device_code(Lcom/squareup/protos/devicesettings/external/DeviceCode;)Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;->device_code:Lcom/squareup/protos/devicesettings/external/DeviceCode;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;"
        }
    .end annotation

    .line 114
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ResetDeviceCodeResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method
