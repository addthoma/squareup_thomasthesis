.class public final enum Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;
.super Ljava/lang/Enum;
.source "SubscribeResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/SubscribeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DUPLICATE:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

.field public static final enum SUCCESS:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

.field public static final enum UNKNOWN:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 96
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->UNKNOWN:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    .line 98
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->SUCCESS:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    .line 100
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    const/4 v3, 0x2

    const-string v4, "DUPLICATE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->DUPLICATE:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    .line 95
    sget-object v4, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->UNKNOWN:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->SUCCESS:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->DUPLICATE:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->$VALUES:[Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    .line 102
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 106
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 107
    iput p3, p0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 117
    :cond_0
    sget-object p0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->DUPLICATE:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    return-object p0

    .line 116
    :cond_1
    sget-object p0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->SUCCESS:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    return-object p0

    .line 115
    :cond_2
    sget-object p0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->UNKNOWN:Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;
    .locals 1

    .line 95
    const-class v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;
    .locals 1

    .line 95
    sget-object v0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->$VALUES:[Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 124
    iget v0, p0, Lcom/squareup/protos/postoffice/sms/SubscribeResponse$Status;->value:I

    return v0
.end method
