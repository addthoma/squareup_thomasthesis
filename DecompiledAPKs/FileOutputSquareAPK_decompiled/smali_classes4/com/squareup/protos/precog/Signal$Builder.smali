.class public final Lcom/squareup/protos/precog/Signal$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Signal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/precog/Signal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/precog/Signal;",
        "Lcom/squareup/protos/precog/Signal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public anonymous_token:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

.field public product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

.field public signal_name:Lcom/squareup/protos/precog/SignalName;

.field public value:Ljava/lang/String;

.field public variant_value:Lcom/squareup/protos/precog/Variant;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 193
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public anonymous_token(Ljava/lang/String;)Lcom/squareup/protos/precog/Signal$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->anonymous_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/precog/Signal;
    .locals 11

    .line 253
    new-instance v10, Lcom/squareup/protos/precog/Signal;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/precog/Signal$Builder;->value:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/precog/Signal$Builder;->anonymous_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/precog/Signal$Builder;->signal_name:Lcom/squareup/protos/precog/SignalName;

    iget-object v5, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    iget-object v6, p0, Lcom/squareup/protos/precog/Signal$Builder;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    iget-object v7, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    iget-object v8, p0, Lcom/squareup/protos/precog/Signal$Builder;->variant_value:Lcom/squareup/protos/precog/Variant;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/precog/Signal;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/precog/SignalName;Lcom/squareup/protos/precog/ProductIntent;Lcom/squareup/protos/precog/OnboardingEntryPoint;Lcom/squareup/protos/precog/ProductInterestWeight;Lcom/squareup/protos/precog/Variant;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/precog/Signal$Builder;->build()Lcom/squareup/protos/precog/Signal;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/precog/Signal$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public onboarding_entry_point_value(Lcom/squareup/protos/precog/OnboardingEntryPoint;)Lcom/squareup/protos/precog/Signal$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 p1, 0x0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    .line 229
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->variant_value:Lcom/squareup/protos/precog/Variant;

    return-object p0
.end method

.method public product_intent_value(Lcom/squareup/protos/precog/ProductIntent;)Lcom/squareup/protos/precog/Signal$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    const/4 p1, 0x0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 221
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->variant_value:Lcom/squareup/protos/precog/Variant;

    return-object p0
.end method

.method public product_interest_weight_value(Lcom/squareup/protos/precog/ProductInterestWeight;)Lcom/squareup/protos/precog/Signal$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    const/4 p1, 0x0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->variant_value:Lcom/squareup/protos/precog/Variant;

    return-object p0
.end method

.method public signal_name(Lcom/squareup/protos/precog/SignalName;)Lcom/squareup/protos/precog/Signal$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->signal_name:Lcom/squareup/protos/precog/SignalName;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/precog/Signal$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->value:Ljava/lang/String;

    return-object p0
.end method

.method public variant_value(Lcom/squareup/protos/precog/Variant;)Lcom/squareup/protos/precog/Signal$Builder;
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->variant_value:Lcom/squareup/protos/precog/Variant;

    const/4 p1, 0x0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 247
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal$Builder;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    return-object p0
.end method
