.class final Lcom/squareup/protos/precog/MerchantSignals$ProtoAdapter_MerchantSignals;
.super Lcom/squareup/wire/ProtoAdapter;
.source "MerchantSignals.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/precog/MerchantSignals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_MerchantSignals"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/precog/MerchantSignals;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 117
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/precog/MerchantSignals;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/precog/MerchantSignals;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    new-instance v0, Lcom/squareup/protos/precog/MerchantSignals$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/precog/MerchantSignals$Builder;-><init>()V

    .line 137
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 138
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 143
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 141
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/precog/MerchantSignals$Builder;->signals:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/precog/Signal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/precog/MerchantSignals$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/precog/MerchantSignals$Builder;

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/precog/MerchantSignals$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 148
    invoke-virtual {v0}, Lcom/squareup/protos/precog/MerchantSignals$Builder;->build()Lcom/squareup/protos/precog/MerchantSignals;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    invoke-virtual {p0, p1}, Lcom/squareup/protos/precog/MerchantSignals$ProtoAdapter_MerchantSignals;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/precog/MerchantSignals;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/precog/MerchantSignals;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 129
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/precog/MerchantSignals;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 130
    sget-object v0, Lcom/squareup/protos/precog/Signal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/precog/MerchantSignals;->signals:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 131
    invoke-virtual {p2}, Lcom/squareup/protos/precog/MerchantSignals;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    check-cast p2, Lcom/squareup/protos/precog/MerchantSignals;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/precog/MerchantSignals$ProtoAdapter_MerchantSignals;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/precog/MerchantSignals;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/precog/MerchantSignals;)I
    .locals 4

    .line 122
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/precog/MerchantSignals;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/precog/Signal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 123
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/precog/MerchantSignals;->signals:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/protos/precog/MerchantSignals;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/protos/precog/MerchantSignals;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/precog/MerchantSignals$ProtoAdapter_MerchantSignals;->encodedSize(Lcom/squareup/protos/precog/MerchantSignals;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/precog/MerchantSignals;)Lcom/squareup/protos/precog/MerchantSignals;
    .locals 2

    .line 153
    invoke-virtual {p1}, Lcom/squareup/protos/precog/MerchantSignals;->newBuilder()Lcom/squareup/protos/precog/MerchantSignals$Builder;

    move-result-object p1

    .line 154
    iget-object v0, p1, Lcom/squareup/protos/precog/MerchantSignals$Builder;->signals:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/precog/Signal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 155
    invoke-virtual {p1}, Lcom/squareup/protos/precog/MerchantSignals$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/protos/precog/MerchantSignals$Builder;->build()Lcom/squareup/protos/precog/MerchantSignals;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/protos/precog/MerchantSignals;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/precog/MerchantSignals$ProtoAdapter_MerchantSignals;->redact(Lcom/squareup/protos/precog/MerchantSignals;)Lcom/squareup/protos/precog/MerchantSignals;

    move-result-object p1

    return-object p1
.end method
