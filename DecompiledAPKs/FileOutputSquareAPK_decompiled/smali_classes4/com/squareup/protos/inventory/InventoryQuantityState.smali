.class public final enum Lcom/squareup/protos/inventory/InventoryQuantityState;
.super Ljava/lang/Enum;
.source "InventoryQuantityState.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/inventory/InventoryQuantityState$ProtoAdapter_InventoryQuantityState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/inventory/InventoryQuantityState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/inventory/InventoryQuantityState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/inventory/InventoryQuantityState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ON_HAND:Lcom/squareup/protos/inventory/InventoryQuantityState;

.field public static final enum ON_PURCHASE_ORDER:Lcom/squareup/protos/inventory/InventoryQuantityState;

.field public static final enum ON_SALES_ORDER:Lcom/squareup/protos/inventory/InventoryQuantityState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 18
    new-instance v0, Lcom/squareup/protos/inventory/InventoryQuantityState;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "ON_HAND"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/inventory/InventoryQuantityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_HAND:Lcom/squareup/protos/inventory/InventoryQuantityState;

    .line 23
    new-instance v0, Lcom/squareup/protos/inventory/InventoryQuantityState;

    const/4 v3, 0x2

    const-string v4, "ON_SALES_ORDER"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/inventory/InventoryQuantityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_SALES_ORDER:Lcom/squareup/protos/inventory/InventoryQuantityState;

    .line 28
    new-instance v0, Lcom/squareup/protos/inventory/InventoryQuantityState;

    const/4 v4, 0x3

    const-string v5, "ON_PURCHASE_ORDER"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/inventory/InventoryQuantityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_PURCHASE_ORDER:Lcom/squareup/protos/inventory/InventoryQuantityState;

    new-array v0, v4, [Lcom/squareup/protos/inventory/InventoryQuantityState;

    .line 14
    sget-object v4, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_HAND:Lcom/squareup/protos/inventory/InventoryQuantityState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_SALES_ORDER:Lcom/squareup/protos/inventory/InventoryQuantityState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_PURCHASE_ORDER:Lcom/squareup/protos/inventory/InventoryQuantityState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/inventory/InventoryQuantityState;->$VALUES:[Lcom/squareup/protos/inventory/InventoryQuantityState;

    .line 30
    new-instance v0, Lcom/squareup/protos/inventory/InventoryQuantityState$ProtoAdapter_InventoryQuantityState;

    invoke-direct {v0}, Lcom/squareup/protos/inventory/InventoryQuantityState$ProtoAdapter_InventoryQuantityState;-><init>()V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryQuantityState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput p3, p0, Lcom/squareup/protos/inventory/InventoryQuantityState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/inventory/InventoryQuantityState;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 45
    :cond_0
    sget-object p0, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_PURCHASE_ORDER:Lcom/squareup/protos/inventory/InventoryQuantityState;

    return-object p0

    .line 44
    :cond_1
    sget-object p0, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_SALES_ORDER:Lcom/squareup/protos/inventory/InventoryQuantityState;

    return-object p0

    .line 43
    :cond_2
    sget-object p0, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_HAND:Lcom/squareup/protos/inventory/InventoryQuantityState;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/inventory/InventoryQuantityState;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/protos/inventory/InventoryQuantityState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/inventory/InventoryQuantityState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/inventory/InventoryQuantityState;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/protos/inventory/InventoryQuantityState;->$VALUES:[Lcom/squareup/protos/inventory/InventoryQuantityState;

    invoke-virtual {v0}, [Lcom/squareup/protos/inventory/InventoryQuantityState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/inventory/InventoryQuantityState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/squareup/protos/inventory/InventoryQuantityState;->value:I

    return v0
.end method
