.class public final Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryAdjustmentLine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/inventory/InventoryAdjustmentLine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/inventory/InventoryAdjustmentLine;",
        "Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cost:Lcom/squareup/protos/common/Money;

.field public item_variation_id:Lcom/squareup/api/sync/ObjectId;

.field public quantity_delta:Ljava/lang/Long;

.field public quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 145
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/inventory/InventoryAdjustmentLine;
    .locals 7

    .line 190
    new-instance v6, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v2, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->quantity_delta:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    iget-object v4, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->cost:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;-><init>(Lcom/squareup/api/sync/ObjectId;Ljava/lang/Long;Lcom/squareup/protos/inventory/InventoryQuantityState;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->build()Lcom/squareup/protos/inventory/InventoryAdjustmentLine;

    move-result-object v0

    return-object v0
.end method

.method public cost(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->cost:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public item_variation_id(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public quantity_delta(Ljava/lang/Long;)Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->quantity_delta:Ljava/lang/Long;

    return-object p0
.end method

.method public quantity_state(Lcom/squareup/protos/inventory/InventoryQuantityState;)Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    return-object p0
.end method
