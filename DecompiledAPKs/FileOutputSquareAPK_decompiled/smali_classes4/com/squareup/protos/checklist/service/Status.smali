.class public final Lcom/squareup/protos/checklist/service/Status;
.super Lcom/squareup/wire/Message;
.source "Status.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/service/Status$ProtoAdapter_Status;,
        Lcom/squareup/protos/checklist/service/Status$StatusCode;,
        Lcom/squareup/protos/checklist/service/Status$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/checklist/service/Status;",
        "Lcom/squareup/protos/checklist/service/Status$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/service/Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUSCODE:Lcom/squareup/protos/checklist/service/Status$StatusCode;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.service.Status$StatusCode#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/checklist/service/Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/service/Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/service/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/checklist/service/Status;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 29
    sget-object v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->DO_NOT_USE:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    sput-object v0, Lcom/squareup/protos/checklist/service/Status;->DEFAULT_STATUSCODE:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/checklist/service/Status$StatusCode;)V
    .locals 1

    .line 44
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/checklist/service/Status;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/checklist/service/Status$StatusCode;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/checklist/service/Status$StatusCode;Lokio/ByteString;)V
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/protos/checklist/service/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/protos/checklist/service/Status;->success:Ljava/lang/Boolean;

    .line 50
    iput-object p2, p0, Lcom/squareup/protos/checklist/service/Status;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 65
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/checklist/service/Status;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 66
    :cond_1
    check-cast p1, Lcom/squareup/protos/checklist/service/Status;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/Status;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/checklist/service/Status;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/service/Status;->success:Ljava/lang/Boolean;

    .line 68
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    iget-object p1, p1, Lcom/squareup/protos/checklist/service/Status;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    .line 69
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 74
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/Status;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/checklist/service/Status$StatusCode;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 79
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/checklist/service/Status$Builder;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/protos/checklist/service/Status$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/service/Status$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/service/Status$Builder;->success:Ljava/lang/Boolean;

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    iput-object v1, v0, Lcom/squareup/protos/checklist/service/Status$Builder;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/Status;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/checklist/service/Status$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/Status;->newBuilder()Lcom/squareup/protos/checklist/service/Status$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    if-eqz v1, :cond_1

    const-string v1, ", statusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/Status;->statusCode:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Status{"

    .line 89
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
