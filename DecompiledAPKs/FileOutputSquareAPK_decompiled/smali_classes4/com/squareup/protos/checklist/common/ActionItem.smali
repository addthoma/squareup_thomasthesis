.class public final Lcom/squareup/protos/checklist/common/ActionItem;
.super Lcom/squareup/wire/Message;
.source "ActionItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/common/ActionItem$ProtoAdapter_ActionItem;,
        Lcom/squareup/protos/checklist/common/ActionItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/checklist/common/ActionItem;",
        "Lcom/squareup/protos/checklist/common/ActionItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/common/ActionItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTION_ITEM_NAME:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final DEFAULT_COMPLETED:Ljava/lang/Boolean;

.field public static final DEFAULT_DISMISSABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_DISMISSED:Ljava/lang/Boolean;

.field public static final DEFAULT_DISMISSIBLE:Ljava/lang/Boolean;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.common.ActionItemName#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final completed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final completed_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final dismissable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final dismissed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final dismissed_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final dismissible:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final signal_fetch_result:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.SignalFetchResult#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/SignalFetchResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItem$ProtoAdapter_ActionItem;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/common/ActionItem$ProtoAdapter_ActionItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItem;->DEFAULT_DISMISSED:Ljava/lang/Boolean;

    .line 33
    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItem;->DEFAULT_DISMISSABLE:Ljava/lang/Boolean;

    .line 35
    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItem;->DEFAULT_DISMISSIBLE:Ljava/lang/Boolean;

    .line 37
    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItem;->DEFAULT_COMPLETED:Ljava/lang/Boolean;

    .line 39
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->UNKNOWN:Lcom/squareup/protos/checklist/common/ActionItemName;

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItem;->DEFAULT_ACTION_ITEM_NAME:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Lcom/squareup/protos/checklist/common/ActionItemName;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/SignalFetchResult;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            ")V"
        }
    .end annotation

    .line 117
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/checklist/common/ActionItem;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Lcom/squareup/protos/checklist/common/ActionItemName;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Lcom/squareup/protos/checklist/common/ActionItemName;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/SignalFetchResult;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 123
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->name:Ljava/lang/String;

    const-string p1, "signal_fetch_result"

    .line 125
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->signal_fetch_result:Ljava/util/List;

    .line 126
    iput-object p3, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed:Ljava/lang/Boolean;

    .line 127
    iput-object p4, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissable:Ljava/lang/Boolean;

    .line 128
    iput-object p5, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissible:Ljava/lang/Boolean;

    .line 129
    iput-object p6, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 130
    iput-object p7, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 131
    iput-object p8, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed:Ljava/lang/Boolean;

    .line 132
    iput-object p9, p0, Lcom/squareup/protos/checklist/common/ActionItem;->action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 154
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/checklist/common/ActionItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 155
    :cond_1
    check-cast p1, Lcom/squareup/protos/checklist/common/ActionItem;

    .line 156
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/common/ActionItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/checklist/common/ActionItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/checklist/common/ActionItem;->name:Ljava/lang/String;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->signal_fetch_result:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/common/ActionItem;->signal_fetch_result:Ljava/util/List;

    .line 158
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed:Ljava/lang/Boolean;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/common/ActionItem;->dismissable:Ljava/lang/Boolean;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissible:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/common/ActionItem;->dismissible:Ljava/lang/Boolean;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/checklist/common/ActionItem;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/common/ActionItem;->completed:Ljava/lang/Boolean;

    .line 164
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;

    iget-object p1, p1, Lcom/squareup/protos/checklist/common/ActionItem;->action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 165
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 170
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 172
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/common/ActionItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->signal_fetch_result:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissable:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissible:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/checklist/common/ActionItemName;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 182
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/checklist/common/ActionItem$Builder;
    .locals 2

    .line 137
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/common/ActionItem$Builder;-><init>()V

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->name:Ljava/lang/String;

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->signal_fetch_result:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->signal_fetch_result:Ljava/util/List;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->dismissed:Ljava/lang/Boolean;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->dismissable:Ljava/lang/Boolean;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissible:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->dismissible:Ljava/lang/Boolean;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->completed:Ljava/lang/Boolean;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;

    iput-object v1, v0, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/common/ActionItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/checklist/common/ActionItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/common/ActionItem;->newBuilder()Lcom/squareup/protos/checklist/common/ActionItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->signal_fetch_result:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", signal_fetch_result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->signal_fetch_result:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", dismissed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissable:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", dismissable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissible:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", dismissible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissible:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_5

    const-string v1, ", dismissed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->dismissed_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_6

    const-string v1, ", completed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", completed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->completed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 198
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;

    if-eqz v1, :cond_8

    const-string v1, ", action_item_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/common/ActionItem;->action_item_name:Lcom/squareup/protos/checklist/common/ActionItemName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ActionItem{"

    .line 199
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
