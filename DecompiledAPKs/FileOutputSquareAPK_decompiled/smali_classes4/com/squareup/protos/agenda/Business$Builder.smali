.class public final Lcom/squareup/protos/agenda/Business$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Business.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/Business;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/Business;",
        "Lcom/squareup/protos/agenda/Business$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public bank_account_verified:Ljava/lang/Boolean;

.field public booking_sites_enabled:Ljava/lang/Boolean;

.field public buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

.field public confirmation_lead_time:Ljava/lang/Integer;

.field public conversations_enabled:Ljava/lang/Boolean;

.field public eligible_for_reserve_with_google:Ljava/lang/Boolean;

.field public id:Ljava/lang/String;

.field public is_sole_proprietor:Ljava/lang/Boolean;

.field public location_type:Lcom/squareup/protos/agenda/Business$LocationType;

.field public name:Ljava/lang/String;

.field public needs_mobile_onboarding:Ljava/lang/Boolean;

.field public no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

.field public no_show_cutoff_time:Ljava/lang/Long;

.field public no_show_flat_fee:Lcom/squareup/protos/common/Money;

.field public no_show_percentage:Ljava/lang/Integer;

.field public no_show_protection_enabled:Ljava/lang/Boolean;

.field public phone_number:Ljava/lang/String;

.field public reminder_email_lead_time:Ljava/lang/Integer;

.field public reminder_sms_lead_time:Ljava/lang/Integer;

.field public reserve_with_google_enabled:Ljava/lang/Boolean;

.field public send_confirmation_email:Ljava/lang/Boolean;

.field public send_confirmation_sms:Ljava/lang/Boolean;

.field public send_reminder_email:Ljava/lang/Boolean;

.field public send_reminder_sms:Ljava/lang/Boolean;

.field public time_zone:Ljava/lang/String;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 512
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 552
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public bank_account_verified(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 606
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->bank_account_verified:Ljava/lang/Boolean;

    return-object p0
.end method

.method public booking_sites_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 611
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->booking_sites_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/agenda/Business;
    .locals 2

    .line 691
    new-instance v0, Lcom/squareup/protos/agenda/Business;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/agenda/Business;-><init>(Lcom/squareup/protos/agenda/Business$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 457
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Business$Builder;->build()Lcom/squareup/protos/agenda/Business;

    move-result-object v0

    return-object v0
.end method

.method public buyer_prepayment_mode(Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 583
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->buyer_prepayment_mode:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    return-object p0
.end method

.method public confirmation_lead_time(Ljava/lang/Integer;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->confirmation_lead_time:Ljava/lang/Integer;

    return-object p0
.end method

.method public conversations_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 677
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->conversations_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public eligible_for_reserve_with_google(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->eligible_for_reserve_with_google:Ljava/lang/Boolean;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 516
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public is_sole_proprietor(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 685
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->is_sole_proprietor:Ljava/lang/Boolean;

    return-object p0
.end method

.method public location_type(Lcom/squareup/protos/agenda/Business$LocationType;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 542
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->location_type:Lcom/squareup/protos/agenda/Business$LocationType;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 524
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public needs_mobile_onboarding(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 537
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->needs_mobile_onboarding:Ljava/lang/Boolean;

    return-object p0
.end method

.method public no_show_charge_type(Lcom/squareup/protos/agenda/Business$NoShowChargeType;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 588
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_charge_type:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    return-object p0
.end method

.method public no_show_cutoff_time(Ljava/lang/Long;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 562
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_cutoff_time:Ljava/lang/Long;

    return-object p0
.end method

.method public no_show_flat_fee(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 598
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_flat_fee:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public no_show_percentage(Ljava/lang/Integer;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 593
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_percentage:Ljava/lang/Integer;

    return-object p0
.end method

.method public no_show_protection_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 578
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->no_show_protection_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 529
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public reminder_email_lead_time(Ljava/lang/Integer;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 634
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->reminder_email_lead_time:Ljava/lang/Integer;

    return-object p0
.end method

.method public reminder_sms_lead_time(Ljava/lang/Integer;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 629
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->reminder_sms_lead_time:Ljava/lang/Integer;

    return-object p0
.end method

.method public reserve_with_google_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 639
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->reserve_with_google_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public send_confirmation_email(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 660
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->send_confirmation_email:Ljava/lang/Boolean;

    return-object p0
.end method

.method public send_confirmation_sms(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 655
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->send_confirmation_sms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public send_reminder_email(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 621
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->send_reminder_email:Ljava/lang/Boolean;

    return-object p0
.end method

.method public send_reminder_sms(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->send_reminder_sms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public time_zone(Ljava/lang/String;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 547
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->time_zone:Ljava/lang/String;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/protos/agenda/Business$Builder;
    .locals 0

    .line 570
    iput-object p1, p0, Lcom/squareup/protos/agenda/Business$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
