.class public final enum Lcom/squareup/protos/agenda/Business$NoShowChargeType;
.super Ljava/lang/Enum;
.source "Business.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/Business;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NoShowChargeType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/Business$NoShowChargeType$ProtoAdapter_NoShowChargeType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/agenda/Business$NoShowChargeType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/agenda/Business$NoShowChargeType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/Business$NoShowChargeType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FLAT_FEE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

.field public static final enum PERCENTAGE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

.field public static final enum PER_SERVICE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 814
    new-instance v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    const/4 v1, 0x0

    const-string v2, "PERCENTAGE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/agenda/Business$NoShowChargeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->PERCENTAGE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    .line 819
    new-instance v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    const/4 v2, 0x1

    const-string v3, "FLAT_FEE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/agenda/Business$NoShowChargeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->FLAT_FEE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    .line 824
    new-instance v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    const/4 v3, 0x2

    const-string v4, "PER_SERVICE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/agenda/Business$NoShowChargeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->PER_SERVICE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    .line 810
    sget-object v4, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->PERCENTAGE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->FLAT_FEE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->PER_SERVICE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->$VALUES:[Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    .line 826
    new-instance v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType$ProtoAdapter_NoShowChargeType;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/Business$NoShowChargeType$ProtoAdapter_NoShowChargeType;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 830
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 831
    iput p3, p0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/agenda/Business$NoShowChargeType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 841
    :cond_0
    sget-object p0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->PER_SERVICE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    return-object p0

    .line 840
    :cond_1
    sget-object p0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->FLAT_FEE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    return-object p0

    .line 839
    :cond_2
    sget-object p0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->PERCENTAGE:Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/agenda/Business$NoShowChargeType;
    .locals 1

    .line 810
    const-class v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/agenda/Business$NoShowChargeType;
    .locals 1

    .line 810
    sget-object v0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->$VALUES:[Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    invoke-virtual {v0}, [Lcom/squareup/protos/agenda/Business$NoShowChargeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/agenda/Business$NoShowChargeType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 848
    iget v0, p0, Lcom/squareup/protos/agenda/Business$NoShowChargeType;->value:I

    return v0
.end method
