.class public final Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ChangeAppointmentStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;",
        "Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/squareup/protos/agenda/ReservationAction;

.field public appointment_id:Ljava/lang/String;

.field public charge_fee:Ljava/lang/Boolean;

.field public client_message:Lcom/squareup/protos/agenda/ClientMessage;

.field public context:Lcom/squareup/protos/agenda/RequestContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public action(Lcom/squareup/protos/agenda/ReservationAction;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->action:Lcom/squareup/protos/agenda/ReservationAction;

    return-object p0
.end method

.method public appointment_id(Ljava/lang/String;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->appointment_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;
    .locals 8

    .line 187
    new-instance v7, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->context:Lcom/squareup/protos/agenda/RequestContext;

    iget-object v2, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->action:Lcom/squareup/protos/agenda/ReservationAction;

    iget-object v3, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->appointment_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    iget-object v5, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->charge_fee:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;-><init>(Lcom/squareup/protos/agenda/RequestContext;Lcom/squareup/protos/agenda/ReservationAction;Ljava/lang/String;Lcom/squareup/protos/agenda/ClientMessage;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->build()Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;

    move-result-object v0

    return-object v0
.end method

.method public charge_fee(Ljava/lang/Boolean;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->charge_fee:Ljava/lang/Boolean;

    return-object p0
.end method

.method public client_message(Lcom/squareup/protos/agenda/ClientMessage;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    return-object p0
.end method

.method public context(Lcom/squareup/protos/agenda/RequestContext;)Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->context:Lcom/squareup/protos/agenda/RequestContext;

    return-object p0
.end method
