.class final Lcom/squareup/protos/agenda/ResponseStatus$ProtoAdapter_ResponseStatus;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ResponseStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/ResponseStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ResponseStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/agenda/ResponseStatus;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 189
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/agenda/ResponseStatus;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/agenda/ResponseStatus;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 212
    new-instance v0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;-><init>()V

    .line 213
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 214
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 228
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 226
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->application_errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/agenda/ApplicationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 225
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->validation_errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/agenda/ValidationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 224
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/protos/agenda/ResponseStatus$Builder;

    goto :goto_0

    .line 218
    :cond_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/agenda/StatusType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/agenda/StatusType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->type(Lcom/squareup/protos/agenda/StatusType;)Lcom/squareup/protos/agenda/ResponseStatus$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 220
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 232
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 233
    invoke-virtual {v0}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->build()Lcom/squareup/protos/agenda/ResponseStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 187
    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/ResponseStatus$ProtoAdapter_ResponseStatus;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/agenda/ResponseStatus;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/agenda/ResponseStatus;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 203
    sget-object v0, Lcom/squareup/protos/agenda/StatusType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 204
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/agenda/ResponseStatus;->error_message:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 205
    sget-object v0, Lcom/squareup/protos/agenda/ValidationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 206
    sget-object v0, Lcom/squareup/protos/agenda/ApplicationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 207
    invoke-virtual {p2}, Lcom/squareup/protos/agenda/ResponseStatus;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 187
    check-cast p2, Lcom/squareup/protos/agenda/ResponseStatus;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/agenda/ResponseStatus$ProtoAdapter_ResponseStatus;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/agenda/ResponseStatus;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/agenda/ResponseStatus;)I
    .locals 4

    .line 194
    sget-object v0, Lcom/squareup/protos/agenda/StatusType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/agenda/ResponseStatus;->type:Lcom/squareup/protos/agenda/StatusType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/agenda/ResponseStatus;->error_message:Ljava/lang/String;

    const/4 v3, 0x2

    .line 195
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/ValidationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 196
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/agenda/ResponseStatus;->validation_errors:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/ApplicationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 197
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/agenda/ResponseStatus;->application_errors:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ResponseStatus;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 187
    check-cast p1, Lcom/squareup/protos/agenda/ResponseStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/ResponseStatus$ProtoAdapter_ResponseStatus;->encodedSize(Lcom/squareup/protos/agenda/ResponseStatus;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/agenda/ResponseStatus;)Lcom/squareup/protos/agenda/ResponseStatus;
    .locals 2

    .line 238
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ResponseStatus;->newBuilder()Lcom/squareup/protos/agenda/ResponseStatus$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 239
    iput-object v0, p1, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->error_message:Ljava/lang/String;

    .line 240
    iget-object v0, p1, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->validation_errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/agenda/ValidationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 241
    iget-object v0, p1, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->application_errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/agenda/ApplicationError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 242
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 243
    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ResponseStatus$Builder;->build()Lcom/squareup/protos/agenda/ResponseStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 187
    check-cast p1, Lcom/squareup/protos/agenda/ResponseStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/agenda/ResponseStatus$ProtoAdapter_ResponseStatus;->redact(Lcom/squareup/protos/agenda/ResponseStatus;)Lcom/squareup/protos/agenda/ResponseStatus;

    move-result-object p1

    return-object p1
.end method
