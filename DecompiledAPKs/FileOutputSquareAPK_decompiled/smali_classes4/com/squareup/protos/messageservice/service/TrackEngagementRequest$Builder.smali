.class public final Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TrackEngagementRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;",
        "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/squareup/protos/messageservice/service/Action;

.field public click_url:Ljava/lang/String;

.field public entity:Lcom/squareup/protos/messageservice/service/Entity;

.field public message_id:Ljava/lang/String;

.field public request_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 157
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public action(Lcom/squareup/protos/messageservice/service/Action;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->action:Lcom/squareup/protos/messageservice/service/Action;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;
    .locals 8

    .line 204
    new-instance v7, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->action:Lcom/squareup/protos/messageservice/service/Action;

    iget-object v2, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->click_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    iget-object v4, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->request_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->message_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;-><init>(Lcom/squareup/protos/messageservice/service/Action;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/Entity;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->build()Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    move-result-object v0

    return-object v0
.end method

.method public click_url(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->click_url:Ljava/lang/String;

    return-object p0
.end method

.method public entity(Lcom/squareup/protos/messageservice/service/Entity;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    return-object p0
.end method

.method public message_id(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->message_id:Ljava/lang/String;

    const/4 p1, 0x0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->request_token:Ljava/lang/String;

    return-object p0
.end method

.method public request_token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->request_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->message_id:Ljava/lang/String;

    return-object p0
.end method
