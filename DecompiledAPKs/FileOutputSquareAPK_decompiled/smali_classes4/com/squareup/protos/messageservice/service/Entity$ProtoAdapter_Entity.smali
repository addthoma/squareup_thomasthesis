.class final Lcom/squareup/protos/messageservice/service/Entity$ProtoAdapter_Entity;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Entity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/Entity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Entity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/messageservice/service/Entity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 127
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/messageservice/service/Entity;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messageservice/service/Entity;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    new-instance v0, Lcom/squareup/protos/messageservice/service/Entity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/Entity$Builder;-><init>()V

    .line 147
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 148
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 160
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 158
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/Entity$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/Entity$Builder;

    goto :goto_0

    .line 152
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/messageservice/service/EntityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/messageservice/service/EntityType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/messageservice/service/Entity$Builder;->type(Lcom/squareup/protos/messageservice/service/EntityType;)Lcom/squareup/protos/messageservice/service/Entity$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 154
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/messageservice/service/Entity$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/messageservice/service/Entity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 165
    invoke-virtual {v0}, Lcom/squareup/protos/messageservice/service/Entity$Builder;->build()Lcom/squareup/protos/messageservice/service/Entity;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 125
    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/Entity$ProtoAdapter_Entity;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messageservice/service/Entity;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messageservice/service/Entity;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    sget-object v0, Lcom/squareup/protos/messageservice/service/EntityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/Entity;->type:Lcom/squareup/protos/messageservice/service/EntityType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 140
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/Entity;->token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 141
    invoke-virtual {p2}, Lcom/squareup/protos/messageservice/service/Entity;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 125
    check-cast p2, Lcom/squareup/protos/messageservice/service/Entity;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/messageservice/service/Entity$ProtoAdapter_Entity;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messageservice/service/Entity;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/messageservice/service/Entity;)I
    .locals 4

    .line 132
    sget-object v0, Lcom/squareup/protos/messageservice/service/EntityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/Entity;->type:Lcom/squareup/protos/messageservice/service/EntityType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/Entity;->token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 133
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/Entity;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 125
    check-cast p1, Lcom/squareup/protos/messageservice/service/Entity;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/Entity$ProtoAdapter_Entity;->encodedSize(Lcom/squareup/protos/messageservice/service/Entity;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/messageservice/service/Entity;)Lcom/squareup/protos/messageservice/service/Entity;
    .locals 0

    .line 170
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/Entity;->newBuilder()Lcom/squareup/protos/messageservice/service/Entity$Builder;

    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/Entity$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 172
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/Entity$Builder;->build()Lcom/squareup/protos/messageservice/service/Entity;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 125
    check-cast p1, Lcom/squareup/protos/messageservice/service/Entity;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/Entity$ProtoAdapter_Entity;->redact(Lcom/squareup/protos/messageservice/service/Entity;)Lcom/squareup/protos/messageservice/service/Entity;

    move-result-object p1

    return-object p1
.end method
