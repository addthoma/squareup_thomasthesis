.class public final enum Lcom/squareup/protos/messageservice/service/Locale;
.super Ljava/lang/Enum;
.source "Locale.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/messageservice/service/Locale$ProtoAdapter_Locale;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/messageservice/service/Locale;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/messageservice/service/Locale;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/messageservice/service/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT_DO_NOT_USE:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum EN_AU:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum EN_CA:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum EN_GB:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum EN_JP:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum EN_US:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum ES_US:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum FR_CA:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum FR_FR:Lcom/squareup/protos/messageservice/service/Locale;

.field public static final enum JA_JP:Lcom/squareup/protos/messageservice/service/Locale;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 14
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/messageservice/service/Locale;

    .line 16
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/4 v2, 0x1

    const-string v3, "EN_US"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->EN_US:Lcom/squareup/protos/messageservice/service/Locale;

    .line 18
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/4 v3, 0x2

    const-string v4, "ES_US"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->ES_US:Lcom/squareup/protos/messageservice/service/Locale;

    .line 20
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/4 v4, 0x3

    const-string v5, "EN_CA"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->EN_CA:Lcom/squareup/protos/messageservice/service/Locale;

    .line 22
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/4 v5, 0x4

    const-string v6, "FR_CA"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->FR_CA:Lcom/squareup/protos/messageservice/service/Locale;

    .line 24
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/4 v6, 0x5

    const-string v7, "JA_JP"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->JA_JP:Lcom/squareup/protos/messageservice/service/Locale;

    .line 26
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/4 v7, 0x6

    const-string v8, "EN_GB"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->EN_GB:Lcom/squareup/protos/messageservice/service/Locale;

    .line 28
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/4 v8, 0x7

    const-string v9, "EN_AU"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->EN_AU:Lcom/squareup/protos/messageservice/service/Locale;

    .line 30
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/16 v9, 0x8

    const-string v10, "EN_JP"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->EN_JP:Lcom/squareup/protos/messageservice/service/Locale;

    .line 32
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale;

    const/16 v10, 0x9

    const-string v11, "FR_FR"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/messageservice/service/Locale;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->FR_FR:Lcom/squareup/protos/messageservice/service/Locale;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/messageservice/service/Locale;

    .line 13
    sget-object v11, Lcom/squareup/protos/messageservice/service/Locale;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->EN_US:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->ES_US:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->EN_CA:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->FR_CA:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->JA_JP:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->EN_GB:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->EN_AU:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->EN_JP:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/messageservice/service/Locale;->FR_FR:Lcom/squareup/protos/messageservice/service/Locale;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->$VALUES:[Lcom/squareup/protos/messageservice/service/Locale;

    .line 34
    new-instance v0, Lcom/squareup/protos/messageservice/service/Locale$ProtoAdapter_Locale;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/Locale$ProtoAdapter_Locale;-><init>()V

    sput-object v0, Lcom/squareup/protos/messageservice/service/Locale;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput p3, p0, Lcom/squareup/protos/messageservice/service/Locale;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/messageservice/service/Locale;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 56
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->FR_FR:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 55
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->EN_JP:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 54
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->EN_AU:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 53
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->EN_GB:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 52
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->JA_JP:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 51
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->FR_CA:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 50
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->EN_CA:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 49
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->ES_US:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 48
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->EN_US:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    .line 47
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/messageservice/service/Locale;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/Locale;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/messageservice/service/Locale;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/messageservice/service/Locale;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/messageservice/service/Locale;->$VALUES:[Lcom/squareup/protos/messageservice/service/Locale;

    invoke-virtual {v0}, [Lcom/squareup/protos/messageservice/service/Locale;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/messageservice/service/Locale;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 63
    iget v0, p0, Lcom/squareup/protos/messageservice/service/Locale;->value:I

    return v0
.end method
