.class public final Lcom/squareup/protos/messageservice/service/MessageUnit;
.super Lcom/squareup/wire/Message;
.source "MessageUnit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/messageservice/service/MessageUnit$ProtoAdapter_MessageUnit;,
        Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/messageservice/service/MessageUnit;",
        "Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/messageservice/service/MessageUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CREATED_AT:Ljava/lang/Long;

.field public static final DEFAULT_MESSAGE_CLASS:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final DEFAULT_MESSAGE_FORMAT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MESSAGE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MESSAGE_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUEST_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/messageservice/service/State;

.field private static final serialVersionUID:J


# instance fields
.field public final created_at:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x6
    .end annotation
.end field

.field public final default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.messageservice.service.DefaultFormat#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final message_class:Lcom/squareup/protos/messageservice/service/MessageClass;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.messageservice.service.MessageClass#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final message_format_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final message_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final message_unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.messageservice.service.NotificationMessageFormat#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final request_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/messageservice/service/State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.messageservice.service.State#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageUnit$ProtoAdapter_MessageUnit;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/MessageUnit$ProtoAdapter_MessageUnit;-><init>()V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/messageservice/service/State;->UNREAD:Lcom/squareup/protos/messageservice/service/State;

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageUnit;->DEFAULT_STATE:Lcom/squareup/protos/messageservice/service/State;

    .line 33
    sget-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->DO_NOT_USE_MESSAGE_CLASS:Lcom/squareup/protos/messageservice/service/MessageClass;

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageUnit;->DEFAULT_MESSAGE_CLASS:Lcom/squareup/protos/messageservice/service/MessageClass;

    const-wide/16 v0, 0x0

    .line 35
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageUnit;->DEFAULT_CREATED_AT:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/State;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/MessageClass;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/DefaultFormat;Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)V
    .locals 11

    .line 117
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/messageservice/service/MessageUnit;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/State;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/MessageClass;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/DefaultFormat;Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/State;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/MessageClass;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/DefaultFormat;Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;Lokio/ByteString;)V
    .locals 1

    .line 124
    sget-object v0, Lcom/squareup/protos/messageservice/service/MessageUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 125
    invoke-static {p8, p9}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p10

    const/4 v0, 0x1

    if-gt p10, v0, :cond_0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    .line 129
    iput-object p2, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    .line 130
    iput-object p3, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    .line 131
    iput-object p4, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    .line 132
    iput-object p5, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 133
    iput-object p6, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    .line 134
    iput-object p7, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    .line 135
    iput-object p8, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    .line 136
    iput-object p9, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    return-void

    .line 126
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of default_format, notification_message_format may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 158
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 159
    :cond_1
    check-cast p1, Lcom/squareup/protos/messageservice/service/MessageUnit;

    .line 160
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/MessageUnit;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/MessageUnit;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    .line 164
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    .line 166
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    .line 167
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    .line 168
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    iget-object p1, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    .line 169
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 174
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/MessageUnit;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/State;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/MessageClass;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/DefaultFormat;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 186
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    .locals 2

    .line 141
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;-><init>()V

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->request_token:Ljava/lang/String;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_format_id:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->state:Lcom/squareup/protos/messageservice/service/State;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_unit_token:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->created_at:Ljava/lang/Long;

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_id:Ljava/lang/String;

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/MessageUnit;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/MessageUnit;->newBuilder()Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", request_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", message_format_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    if-eqz v1, :cond_2

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", message_unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    if-eqz v1, :cond_4

    const-string v1, ", message_class="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 199
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 200
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", message_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    if-eqz v1, :cond_7

    const-string v1, ", default_format="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 202
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    if-eqz v1, :cond_8

    const-string v1, ", notification_message_format="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MessageUnit{"

    .line 203
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
