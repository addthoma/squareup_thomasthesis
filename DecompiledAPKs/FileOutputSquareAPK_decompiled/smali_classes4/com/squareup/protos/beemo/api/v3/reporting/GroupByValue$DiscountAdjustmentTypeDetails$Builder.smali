.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4971
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;
    .locals 3

    .line 4984
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4968
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    move-result-object v0

    return-object v0
.end method

.method public discount_adjustment_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;
    .locals 0

    .line 4978
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-object p0
.end method
