.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RestaurantCoverDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$ProtoAdapter_RestaurantCoverDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COVER_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

.field private static final serialVersionUID:J


# instance fields
.field public final cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$RestaurantCoverDetails$CoverType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7622
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$ProtoAdapter_RestaurantCoverDetails;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$ProtoAdapter_RestaurantCoverDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7626
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->DEFAULT_COVER_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;)V
    .locals 1

    .line 7638
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;Lokio/ByteString;)V
    .locals 1

    .line 7642
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 7643
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 7657
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 7658
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    .line 7659
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    .line 7660
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 7665
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 7667
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 7668
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 7669
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;
    .locals 2

    .line 7648
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;-><init>()V

    .line 7649
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    .line 7650
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7621
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 7676
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7677
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    if-eqz v1, :cond_0

    const-string v1, ", cover_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->cover_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RestaurantCoverDetails{"

    .line 7678
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
