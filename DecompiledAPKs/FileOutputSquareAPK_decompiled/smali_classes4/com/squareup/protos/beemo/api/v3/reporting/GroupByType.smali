.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
.super Ljava/lang/Enum;
.source "GroupByType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType$ProtoAdapter_GroupByType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ACTING_EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BILL_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum BUSINESS_DAY_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum COMP_REASON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum CONTACT_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DAY_OF_WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DEVICE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DEVICE_CREDENTIAL_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DEVICE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DEVICE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DINING_OPTION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum EVENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum FEE_PLAN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum FEE_PLAN_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum FEE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum FULFILLMENT_RECIPIENT_EMAIL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum FULFILLMENT_RECIPIENT_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum FULFILLMENT_RECIPIENT_PHONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum FULFILLMENT_STATE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum FULFILLMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum GIFT_CARD_ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum GIFT_CARD_ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum GIFT_CARD_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum HOUR:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum HOUR_OF_DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ITEMIZATION_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ITEMIZATION_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ITEM_CATEGORY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ITEM_VARIATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ITEM_VARIATION_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum LAST_ITEMIZATION_UPDATE_EMPLOYEE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum MENU_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum MOBILE_STAFF:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum MODIFIER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum MODIFIER_SET:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum MONTH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ORDER_DISPLAY_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum PAN_SUFFIX:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum PAYMENT_METHOD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum PAYMENT_METHOD_SEPARATED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum RECEIPT_NUMBER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum RESTAURANT_COVER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum RISK_EVALUATION_ACTION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum RISK_EVALUATION_LEVEL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum RISK_EVALUATION_RULE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum SUBUNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum SURCHARGE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum SURCHARGE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum TAX_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum TENDER_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum TICKET_GROUP:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum TICKET_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum TICKET_TEMPLATE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum TIME_WINDOW:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum TOTAL_COLLECTED_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum UNIQUE_ITEMIZATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum UNIT_PRICE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum VENDOR:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum VIRTUAL_REGISTER_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum VOID_REASON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public static final enum WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 14
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v1, 0x1

    const-string v2, "NONE"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 16
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x2

    const-string v3, "DISCOUNT"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 18
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v3, 0x3

    const-string v4, "ITEM_CATEGORY"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_CATEGORY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 20
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v4, 0x4

    const-string v5, "EMPLOYEE"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 28
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v5, 0x38

    const-string v6, "ACTING_EMPLOYEE"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ACTING_EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 30
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v6, 0x28

    const/4 v7, 0x5

    const-string v8, "EMPLOYEE_ROLE"

    invoke-direct {v0, v8, v7, v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 35
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v8, 0x6

    const-string v9, "DEVICE"

    invoke-direct {v0, v9, v8, v7}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 37
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v9, 0x7

    const-string v10, "ITEM"

    invoke-direct {v0, v10, v9, v8}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 39
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v10, 0x8

    const-string v11, "ITEM_VARIATION"

    invoke-direct {v0, v11, v10, v9}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_VARIATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 41
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v11, 0x9

    const-string v12, "MOBILE_STAFF"

    invoke-direct {v0, v12, v11, v10}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MOBILE_STAFF:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 43
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v12, 0xa

    const-string v13, "SUBUNIT"

    invoke-direct {v0, v13, v12, v11}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SUBUNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 45
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v13, 0xb

    const-string v14, "MODIFIER"

    invoke-direct {v0, v14, v13, v12}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MODIFIER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 47
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v14, 0xc

    const-string v15, "PAYMENT_METHOD"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_METHOD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 49
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v15, 0xd

    const-string v13, "MODIFIER_SET"

    invoke-direct {v0, v13, v15, v14}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MODIFIER_SET:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 54
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "DEVICE_NAME"

    const/16 v14, 0xe

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 56
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "DEVICE_TOKEN"

    const/16 v14, 0xf

    const/16 v15, 0xe

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 61
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "HOUR"

    const/16 v14, 0x10

    const/16 v15, 0xf

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 67
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "TIME_WINDOW"

    const/16 v14, 0x11

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TIME_WINDOW:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 72
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "UNIT_PRICE"

    const/16 v14, 0x12

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->UNIT_PRICE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 77
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "PAYMENT_METHOD_SEPARATED"

    const/16 v14, 0x13

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_METHOD_SEPARATED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 79
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "TAX_NAME"

    const/16 v14, 0x14

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TAX_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 81
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "DAY"

    const/16 v14, 0x15

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 83
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "MONTH"

    const/16 v14, 0x16

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MONTH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 85
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "FEE_TYPE"

    const/16 v14, 0x17

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 87
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "PRODUCT"

    const/16 v14, 0x18

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 89
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "TAXABLE"

    const/16 v14, 0x19

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 94
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "DAY_OF_WEEK"

    const/16 v14, 0x1a

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY_OF_WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 99
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "HOUR_OF_DAY"

    const/16 v14, 0x1b

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR_OF_DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 104
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "WEEK"

    const/16 v14, 0x1c

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 109
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "DINING_OPTION"

    const/16 v14, 0x1d

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DINING_OPTION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 114
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "GIFT_CARD_ITEM"

    const/16 v14, 0x1e

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 119
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "GIFT_CARD_ACTIVITY_TYPE"

    const/16 v14, 0x1f

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 124
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "GIFT_CARD_TENDER"

    const/16 v14, 0x20

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 130
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "PAN_SUFFIX"

    const/16 v14, 0x21

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAN_SUFFIX:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 135
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "VIRTUAL_REGISTER_TOKEN"

    const/16 v14, 0x22

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VIRTUAL_REGISTER_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 140
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "BUSINESS_DAY_TOKEN"

    const/16 v14, 0x23

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->BUSINESS_DAY_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 145
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "ACTIVITY_TYPE"

    const/16 v14, 0x24

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 150
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "VOID_REASON"

    const/16 v14, 0x25

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VOID_REASON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 155
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "EVENT_TYPE"

    const/16 v14, 0x26

    const/16 v15, 0x25

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EVENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 160
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "DISCOUNT_ADJUSTMENT_TYPE"

    const/16 v14, 0x27

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 165
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "COMP_REASON"

    invoke-direct {v0, v13, v6, v14}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->COMP_REASON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 167
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "UNIQUE_ITEMIZATION"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->UNIQUE_ITEMIZATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 172
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "TICKET_GROUP"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_GROUP:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 177
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "DEVICE_CREDENTIAL"

    const/16 v14, 0x2b

    const/16 v15, 0x2b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 182
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "LAST_ITEMIZATION_UPDATE_EMPLOYEE_TOKEN"

    const/16 v14, 0x2c

    const/16 v15, 0x2c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->LAST_ITEMIZATION_UPDATE_EMPLOYEE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 187
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "ITEMIZATION_NOTE"

    const/16 v14, 0x2d

    const/16 v15, 0x2d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEMIZATION_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 192
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "RECEIPT_NUMBER"

    const/16 v14, 0x2e

    const/16 v15, 0x2e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RECEIPT_NUMBER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 197
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "TICKET_NAME"

    const/16 v14, 0x2f

    const/16 v15, 0x2f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 203
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "DEVICE_CREDENTIAL_NAME"

    const/16 v14, 0x30

    const/16 v15, 0x30

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_CREDENTIAL_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 208
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "TOTAL_COLLECTED_AMOUNT"

    const/16 v14, 0x31

    const/16 v15, 0x31

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TOTAL_COLLECTED_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 210
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "CONVERSATIONAL_MODE"

    const/16 v14, 0x32

    const/16 v15, 0x32

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 215
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "BILL_NOTE"

    const/16 v14, 0x33

    const/16 v15, 0x33

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->BILL_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 220
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "TENDER_NOTE"

    const/16 v14, 0x34

    const/16 v15, 0x34

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TENDER_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 222
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "MENU_NAME"

    const/16 v14, 0x35

    const/16 v15, 0x35

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MENU_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 227
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "FEE_PLAN"

    const/16 v14, 0x36

    const/16 v15, 0x36

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_PLAN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 232
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "FEE_PLAN_TYPE"

    const/16 v14, 0x37

    const/16 v15, 0x37

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_PLAN_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 237
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "TICKET_TEMPLATE"

    const/16 v14, 0x39

    invoke-direct {v0, v13, v5, v14}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_TEMPLATE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 242
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "RESTAURANT_COVER"

    const/16 v15, 0x3a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RESTAURANT_COVER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 247
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "PAYMENT_SOURCE_NAME"

    const/16 v14, 0x3a

    const/16 v15, 0x3b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 254
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "ITEMIZATION_TYPE"

    const/16 v14, 0x3b

    const/16 v15, 0x3c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEMIZATION_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 259
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "SURCHARGE_TYPE"

    const/16 v14, 0x3c

    const/16 v15, 0x3d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SURCHARGE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 265
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "CONTACT_TOKEN"

    const/16 v14, 0x3d

    const/16 v15, 0x3e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->CONTACT_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 270
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "SURCHARGE_NAME"

    const/16 v14, 0x3e

    const/16 v15, 0x3f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SURCHARGE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 275
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "ORDER_SOURCE_NAME"

    const/16 v14, 0x3f

    const/16 v15, 0x40

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 280
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "RISK_EVALUATION_LEVEL"

    const/16 v14, 0x40

    const/16 v15, 0x41

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_LEVEL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 285
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "RISK_EVALUATION_ACTION"

    const/16 v14, 0x41

    const/16 v15, 0x4a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_ACTION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 291
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "RISK_EVALUATION_RULE"

    const/16 v14, 0x42

    const/16 v15, 0x4b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_RULE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 296
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "ITEM_VARIATION_MEASUREMENT_UNIT"

    const/16 v14, 0x43

    const/16 v15, 0x42

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_VARIATION_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 301
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "ORDER_DISPLAY_NAME"

    const/16 v14, 0x44

    const/16 v15, 0x43

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ORDER_DISPLAY_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 306
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "FULFILLMENT_TYPE"

    const/16 v14, 0x45

    const/16 v15, 0x44

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 311
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "FULFILLMENT_STATE"

    const/16 v14, 0x46

    const/16 v15, 0x45

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_STATE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 316
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "FULFILLMENT_RECIPIENT_NAME"

    const/16 v14, 0x47

    const/16 v15, 0x46

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 321
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "FULFILLMENT_RECIPIENT_PHONE"

    const/16 v14, 0x48

    const/16 v15, 0x47

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_PHONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 326
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "FULFILLMENT_RECIPIENT_EMAIL"

    const/16 v14, 0x49

    const/16 v15, 0x48

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_EMAIL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 333
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string v13, "VENDOR"

    const/16 v14, 0x4a

    const/16 v15, 0x49

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VENDOR:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v0, 0x4b

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 10
    sget-object v13, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v14, 0x0

    aput-object v13, v0, v14

    sget-object v13, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_CATEGORY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ACTING_EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_VARIATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MOBILE_STAFF:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SUBUNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MODIFIER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_METHOD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MODIFIER_SET:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TIME_WINDOW:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->UNIT_PRICE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_METHOD_SEPARATED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TAX_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MONTH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY_OF_WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR_OF_DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DINING_OPTION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAN_SUFFIX:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VIRTUAL_REGISTER_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->BUSINESS_DAY_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VOID_REASON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EVENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->COMP_REASON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->UNIQUE_ITEMIZATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_GROUP:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->LAST_ITEMIZATION_UPDATE_EMPLOYEE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEMIZATION_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RECEIPT_NUMBER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_CREDENTIAL_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TOTAL_COLLECTED_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->BILL_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TENDER_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MENU_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_PLAN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_PLAN_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_TEMPLATE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RESTAURANT_COVER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEMIZATION_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SURCHARGE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->CONTACT_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SURCHARGE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_LEVEL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_ACTION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_RULE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_VARIATION_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ORDER_DISPLAY_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_STATE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_PHONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_EMAIL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VENDOR:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 335
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType$ProtoAdapter_GroupByType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType$ProtoAdapter_GroupByType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 339
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 340
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 414
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_RULE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 413
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_ACTION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 422
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VENDOR:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 421
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_EMAIL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 420
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_PHONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 419
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_RECIPIENT_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 418
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_STATE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 417
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FULFILLMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 416
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ORDER_DISPLAY_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 415
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_VARIATION_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 412
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RISK_EVALUATION_LEVEL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 411
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 410
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SURCHARGE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 409
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->CONTACT_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 408
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SURCHARGE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 407
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEMIZATION_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 406
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 405
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RESTAURANT_COVER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 404
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_TEMPLATE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 352
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ACTING_EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 403
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_PLAN_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 402
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_PLAN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 401
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MENU_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 400
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TENDER_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 399
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->BILL_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 398
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 397
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TOTAL_COLLECTED_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 396
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_CREDENTIAL_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 395
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 394
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->RECEIPT_NUMBER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 393
    :pswitch_1e
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEMIZATION_NOTE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 392
    :pswitch_1f
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->LAST_ITEMIZATION_UPDATE_EMPLOYEE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 391
    :pswitch_20
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 390
    :pswitch_21
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TICKET_GROUP:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 389
    :pswitch_22
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->UNIQUE_ITEMIZATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 353
    :pswitch_23
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 388
    :pswitch_24
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->COMP_REASON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 387
    :pswitch_25
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 386
    :pswitch_26
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EVENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 385
    :pswitch_27
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VOID_REASON:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 384
    :pswitch_28
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 383
    :pswitch_29
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->BUSINESS_DAY_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 382
    :pswitch_2a
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->VIRTUAL_REGISTER_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 381
    :pswitch_2b
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAN_SUFFIX:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 380
    :pswitch_2c
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 379
    :pswitch_2d
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 378
    :pswitch_2e
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->GIFT_CARD_ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 377
    :pswitch_2f
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DINING_OPTION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 376
    :pswitch_30
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 375
    :pswitch_31
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR_OF_DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 374
    :pswitch_32
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY_OF_WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 373
    :pswitch_33
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 372
    :pswitch_34
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 371
    :pswitch_35
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->FEE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 370
    :pswitch_36
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MONTH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 369
    :pswitch_37
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 368
    :pswitch_38
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TAX_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 367
    :pswitch_39
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_METHOD_SEPARATED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 366
    :pswitch_3a
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->UNIT_PRICE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 365
    :pswitch_3b
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->TIME_WINDOW:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 364
    :pswitch_3c
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 363
    :pswitch_3d
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 362
    :pswitch_3e
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_NAME:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 361
    :pswitch_3f
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MODIFIER_SET:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 360
    :pswitch_40
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->PAYMENT_METHOD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 359
    :pswitch_41
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MODIFIER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 358
    :pswitch_42
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->SUBUNIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 357
    :pswitch_43
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MOBILE_STAFF:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 356
    :pswitch_44
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_VARIATION:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 355
    :pswitch_45
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 354
    :pswitch_46
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 351
    :pswitch_47
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 350
    :pswitch_48
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ITEM_CATEGORY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 349
    :pswitch_49
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    .line 348
    :pswitch_4a
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4a
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 429
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->value:I

    return v0
.end method
