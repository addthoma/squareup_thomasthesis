.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;
.super Lcom/squareup/wire/Message;
.source "CustomReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$ProtoAdapter_CustomReportResponse;,
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;,
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final bill_event_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final custom_report:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.CustomReport#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;"
        }
    .end annotation
.end field

.field public final sales_inside_range_detail:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.CustomReportResponse$PartialPaymentDetail#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;"
        }
    .end annotation
.end field

.field public final sales_outside_range_detail:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.CustomReportResponse$PartialPaymentDetail#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$ProtoAdapter_CustomReportResponse;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$ProtoAdapter_CustomReportResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;)V"
        }
    .end annotation

    .line 67
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 73
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p5, "custom_report"

    .line 74
    invoke-static {p5, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const-string p1, "bill_event_token"

    .line 75
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    const-string p1, "sales_inside_range_detail"

    .line 76
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    const-string p1, "sales_outside_range_detail"

    .line 77
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 94
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 95
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    .line 97
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    .line 98
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    .line 99
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    .line 100
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 105
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;-><init>()V

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->custom_report:Ljava/util/List;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->bill_event_token:Ljava/util/List;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_inside_range_detail:Ljava/util/List;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->sales_outside_range_detail:Ljava/util/List;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", custom_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", bill_event_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->bill_event_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", sales_inside_range_detail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_inside_range_detail:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", sales_outside_range_detail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->sales_outside_range_detail:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CustomReportResponse{"

    .line 124
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
