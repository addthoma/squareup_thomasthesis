.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

.field public activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

.field public bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

.field public business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

.field public card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

.field public comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

.field public contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

.field public conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

.field public day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

.field public day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

.field public device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

.field public dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

.field public discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

.field public discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

.field public employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

.field public employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

.field public event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

.field public fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

.field public fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

.field public fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

.field public fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

.field public gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

.field public hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

.field public hour_of_day:Ljava/lang/Integer;

.field public item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

.field public item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

.field public item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

.field public itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

.field public itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

.field public menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

.field public mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

.field public modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

.field public modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

.field public month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

.field public order_display_name:Ljava/lang/String;

.field public order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

.field public payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

.field public product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

.field public restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

.field public risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

.field public risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

.field public risk_evaluation_rule:Ljava/lang/String;

.field public subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

.field public surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

.field public surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

.field public tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

.field public taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

.field public tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

.field public ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

.field public ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

.field public ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

.field public time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

.field public transaction_completed_at_ms:Ljava/lang/Long;

.field public transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

.field public unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

.field public unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

.field public vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

.field public virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

.field public void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

.field public week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 977
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public acting_employee(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1005
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    return-object p0
.end method

.method public activity_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1145
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    return-object p0
.end method

.method public bill_note(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1214
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
    .locals 2

    .line 1325
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 844
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object v0

    return-object v0
.end method

.method public business_day_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1140
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    return-object p0
.end method

.method public card_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1130
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    return-object p0
.end method

.method public comp_reason(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1169
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    return-object p0
.end method

.method public contact_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1269
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    return-object p0
.end method

.method public conversational_mode(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1209
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    return-object p0
.end method

.method public day(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1075
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    return-object p0
.end method

.method public day_of_week(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1100
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0
.end method

.method public device(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1015
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    return-object p0
.end method

.method public device_credential(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1184
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    return-object p0
.end method

.method public dining_option(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1115
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    return-object p0
.end method

.method public discount(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 989
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    return-object p0
.end method

.method public discount_adjustment_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1164
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    return-object p0
.end method

.method public employee(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 999
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    return-object p0
.end method

.method public employee_role(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1010
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    return-object p0
.end method

.method public event_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1155
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    return-object p0
.end method

.method public fee_plan(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1229
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    return-object p0
.end method

.method public fee_plan_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1234
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    return-object p0
.end method

.method public fee_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1085
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    return-object p0
.end method

.method public fulfillment_recipient(Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1314
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    return-object p0
.end method

.method public fulfillment_state(Lcom/squareup/orders/model/Order$Fulfillment$State;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1309
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0
.end method

.method public fulfillment_type(Lcom/squareup/orders/model/Order$FulfillmentType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1304
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0
.end method

.method public gift_card(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1120
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    return-object p0
.end method

.method public gift_card_activity_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1125
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    return-object p0
.end method

.method public hour(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1055
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    return-object p0
.end method

.method public hour_of_day(Ljava/lang/Integer;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1105
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour_of_day:Ljava/lang/Integer;

    return-object p0
.end method

.method public item(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1020
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    return-object p0
.end method

.method public item_category(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 994
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    return-object p0
.end method

.method public item_variation(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1025
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    return-object p0
.end method

.method public itemization_note(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1189
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    return-object p0
.end method

.method public itemization_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1259
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    return-object p0
.end method

.method public menu(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1224
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    return-object p0
.end method

.method public mobile_staff(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1030
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    return-object p0
.end method

.method public modifier(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1040
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    return-object p0
.end method

.method public modifier_set(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1050
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    return-object p0
.end method

.method public month(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1080
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    return-object p0
.end method

.method public order_display_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1299
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_display_name:Ljava/lang/String;

    return-object p0
.end method

.method public order_source_name(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1279
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    return-object p0
.end method

.method public payment_method(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1045
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    return-object p0
.end method

.method public payment_source_name(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1249
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    return-object p0
.end method

.method public product(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1090
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0
.end method

.method public receipt_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1194
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    return-object p0
.end method

.method public restaurant_cover_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1244
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    return-object p0
.end method

.method public risk_evaluation_action(Lcom/squareup/protos/riskevaluation/external/Action;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1289
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    return-object p0
.end method

.method public risk_evaluation_level(Lcom/squareup/protos/riskevaluation/external/Level;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1284
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    return-object p0
.end method

.method public risk_evaluation_rule(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1294
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_rule:Ljava/lang/String;

    return-object p0
.end method

.method public subunit(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1035
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    return-object p0
.end method

.method public surcharge_name_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1274
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    return-object p0
.end method

.method public surcharge_type_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1264
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    return-object p0
.end method

.method public tax(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1070
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    return-object p0
.end method

.method public taxable(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1095
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    return-object p0
.end method

.method public tender_note(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1219
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    return-object p0
.end method

.method public ticket_group(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1179
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    return-object p0
.end method

.method public ticket_name(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1199
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    return-object p0
.end method

.method public ticket_template(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1239
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    return-object p0
.end method

.method public time_window(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1060
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    return-object p0
.end method

.method public transaction_completed_at_ms(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 984
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_completed_at_ms:Ljava/lang/Long;

    return-object p0
.end method

.method public transaction_total(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1204
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    return-object p0
.end method

.method public unique_itemization(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1174
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    return-object p0
.end method

.method public unit_price(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1065
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    return-object p0
.end method

.method public vendor(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1319
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    return-object p0
.end method

.method public virtual_register_details(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1135
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    return-object p0
.end method

.method public void_reason(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1150
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    return-object p0
.end method

.method public week(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 0

    .line 1110
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    return-object p0
.end method
