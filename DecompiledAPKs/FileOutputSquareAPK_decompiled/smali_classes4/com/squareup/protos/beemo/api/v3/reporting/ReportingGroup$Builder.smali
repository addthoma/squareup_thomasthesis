.class public final Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReportingGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group_name:Ljava/lang/String;

.field public reporting_group_member:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 96
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;->reporting_group_member:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;->group_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;->reporting_group_member:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;-><init>(Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;

    move-result-object v0

    return-object v0
.end method

.method public group_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;->group_name:Ljava/lang/String;

    return-object p0
.end method

.method public reporting_group_member(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupMember;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;"
        }
    .end annotation

    .line 105
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup$Builder;->reporting_group_member:Ljava/util/List;

    return-object p0
.end method
