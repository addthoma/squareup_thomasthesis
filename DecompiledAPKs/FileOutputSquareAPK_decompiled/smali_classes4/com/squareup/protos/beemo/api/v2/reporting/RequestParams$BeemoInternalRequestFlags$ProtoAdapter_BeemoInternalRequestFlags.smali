.class final Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$ProtoAdapter_BeemoInternalRequestFlags;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BeemoInternalRequestFlags"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 618
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 639
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;-><init>()V

    .line 640
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 641
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 647
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 645
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->use_ledger_summary_results_in_settlement_report_response(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;

    goto :goto_0

    .line 644
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->use_new_ledger_summary_endpoint(Ljava/lang/Boolean;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;

    goto :goto_0

    .line 643
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->transactions_time_range_from_bewfo_start_time_ms(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;

    goto :goto_0

    .line 651
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 652
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 616
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$ProtoAdapter_BeemoInternalRequestFlags;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 631
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 632
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 633
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 634
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 616
    check-cast p2, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$ProtoAdapter_BeemoInternalRequestFlags;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)I
    .locals 4

    .line 623
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 624
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 625
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 626
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 616
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$ProtoAdapter_BeemoInternalRequestFlags;->encodedSize(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;
    .locals 0

    .line 657
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;

    move-result-object p1

    .line 658
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 659
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 616
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$ProtoAdapter_BeemoInternalRequestFlags;->redact(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    move-result-object p1

    return-object p1
.end method
