.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

.field public refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

.field public sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;
    .locals 5

    .line 145
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    move-result-object v0

    return-object v0
.end method

.method public net(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    return-object p0
.end method

.method public refunds(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    return-object p0
.end method

.method public sales(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    return-object p0
.end method
