.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public event_type:Lcom/squareup/protos/beemo/v3/EventType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4794
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;
    .locals 3

    .line 4804
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;-><init>(Lcom/squareup/protos/beemo/v3/EventType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4791
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    move-result-object v0

    return-object v0
.end method

.method public event_type(Lcom/squareup/protos/beemo/v3/EventType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;
    .locals 0

    .line 4798
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    return-object p0
.end method
