.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeePlanTypeDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$ProtoAdapter_FeePlanTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FEE_PLAN_TYPE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

.field private static final serialVersionUID:J


# instance fields
.field public final fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v2.models.SquareProcessingFeeAmountDetails$FeeType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7382
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$ProtoAdapter_FeePlanTypeDetails;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$ProtoAdapter_FeePlanTypeDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7386
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->FIXED_RATE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->DEFAULT_FEE_PLAN_TYPE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;)V
    .locals 1

    .line 7395
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;-><init>(Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;Lokio/ByteString;)V
    .locals 1

    .line 7400
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 7401
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 7415
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 7416
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    .line 7417
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    .line 7418
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 7423
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 7425
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 7426
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 7427
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;
    .locals 2

    .line 7406
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;-><init>()V

    .line 7407
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    .line 7408
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7381
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 7434
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7435
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    if-eqz v1, :cond_0

    const-string v1, ", fee_plan_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeePlanTypeDetails{"

    .line 7436
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
