.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoverType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType$ProtoAdapter_CoverType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COVERED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

.field public static final enum NOT_COVERED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 7702
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    .line 7704
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    const/4 v2, 0x1

    const-string v3, "COVERED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->COVERED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    .line 7706
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    const/4 v3, 0x2

    const-string v4, "NOT_COVERED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->NOT_COVERED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    .line 7701
    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->COVERED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->NOT_COVERED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    .line 7708
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType$ProtoAdapter_CoverType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType$ProtoAdapter_CoverType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 7712
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 7713
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 7723
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->NOT_COVERED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    return-object p0

    .line 7722
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->COVERED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    return-object p0

    .line 7721
    :cond_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;
    .locals 1

    .line 7701
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;
    .locals 1

    .line 7701
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 7730
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails$CoverType;->value:I

    return v0
.end method
