.class public final Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;
.super Lcom/squareup/wire/Message;
.source "ReportingGroupConfigDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$ProtoAdapter_ReportingGroupConfigDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GROUP_BY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field private static final serialVersionUID:J


# instance fields
.field public final group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final reporting_group:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.ReportingGroup#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$ProtoAdapter_ReportingGroupConfigDetails;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$ProtoAdapter_ReportingGroupConfigDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->DEFAULT_GROUP_BY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;",
            ">;)V"
        }
    .end annotation

    .line 42
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroup;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 47
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 48
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string p1, "reporting_group"

    .line 49
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 64
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 65
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 67
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    .line 68
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 73
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;
    .locals 2

    .line 54
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;-><init>()V

    .line 55
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 56
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->reporting_group:Ljava/util/List;

    .line 57
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    if-eqz v1, :cond_0

    const-string v1, ", group_by_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", reporting_group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupConfigDetails;->reporting_group:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReportingGroupConfigDetails{"

    .line 88
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
