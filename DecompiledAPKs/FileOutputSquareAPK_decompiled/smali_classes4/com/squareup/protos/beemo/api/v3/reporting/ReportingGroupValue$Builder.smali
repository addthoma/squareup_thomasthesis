.class public final Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReportingGroupValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public reporting_group_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->reporting_group_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    move-result-object v0

    return-object v0
.end method

.method public reporting_group_name(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->reporting_group_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method
