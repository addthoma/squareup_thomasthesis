.class final Lcom/squareup/protos/bizbank/UnifiedActivityDetails$ProtoAdapter_UnifiedActivityDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UnifiedActivityDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UnifiedActivityDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 447
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 472
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;-><init>()V

    .line 473
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 474
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 482
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 480
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->net_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    goto :goto_0

    .line 479
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->gross_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    goto :goto_0

    .line 478
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->modifiers:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 477
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    goto :goto_0

    .line 476
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    goto :goto_0

    .line 486
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 487
    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 445
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$ProtoAdapter_UnifiedActivityDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 462
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 463
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 464
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 465
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 466
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 467
    invoke-virtual {p2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 445
    check-cast p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$ProtoAdapter_UnifiedActivityDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)I
    .locals 4

    .line 452
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->description:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 453
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 454
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 455
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->net_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 456
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 445
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$ProtoAdapter_UnifiedActivityDetails;->encodedSize(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
    .locals 2

    .line 492
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    move-result-object p1

    .line 493
    iget-object v0, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->modifiers:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 494
    iget-object v0, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->gross_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->gross_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->gross_amount:Lcom/squareup/protos/common/Money;

    .line 495
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->net_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->net_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->net_amount:Lcom/squareup/protos/common/Money;

    .line 496
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 497
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 445
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$ProtoAdapter_UnifiedActivityDetails;->redact(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p1

    return-object p1
.end method
