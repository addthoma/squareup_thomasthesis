.class public final Lcom/squareup/protos/jedi/service/StartSessionResponse;
.super Lcom/squareup/wire/Message;
.source "StartSessionResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/jedi/service/StartSessionResponse$ProtoAdapter_StartSessionResponse;,
        Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/jedi/service/StartSessionResponse;",
        "Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/jedi/service/StartSessionResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CODE:Ljava/lang/Integer;

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_SESSION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final code:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x4
    .end annotation
.end field

.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final session_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/protos/jedi/service/StartSessionResponse$ProtoAdapter_StartSessionResponse;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/StartSessionResponse$ProtoAdapter_StartSessionResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/jedi/service/StartSessionResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->DEFAULT_CODE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 6

    .line 60
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/jedi/service/StartSessionResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->success:Ljava/lang/Boolean;

    .line 67
    iput-object p2, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->session_token:Ljava/lang/String;

    .line 68
    iput-object p3, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->error_message:Ljava/lang/String;

    .line 69
    iput-object p4, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->code:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 86
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/jedi/service/StartSessionResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 87
    :cond_1
    check-cast p1, Lcom/squareup/protos/jedi/service/StartSessionResponse;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/StartSessionResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionResponse;->success:Ljava/lang/Boolean;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->session_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionResponse;->session_token:Ljava/lang/String;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/StartSessionResponse;->error_message:Ljava/lang/String;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->code:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/jedi/service/StartSessionResponse;->code:Ljava/lang/Integer;

    .line 92
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 97
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->session_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->code:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;
    .locals 2

    .line 74
    new-instance v0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->success:Ljava/lang/Boolean;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->session_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->session_token:Ljava/lang/String;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->error_message:Ljava/lang/String;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->code:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->code:Ljava/lang/Integer;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionResponse;->newBuilder()Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->session_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", session_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->session_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->code:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse;->code:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartSessionResponse{"

    .line 116
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
