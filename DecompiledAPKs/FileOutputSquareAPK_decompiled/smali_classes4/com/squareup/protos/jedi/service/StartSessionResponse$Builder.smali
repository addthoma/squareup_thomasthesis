.class public final Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartSessionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/StartSessionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/jedi/service/StartSessionResponse;",
        "Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public code:Ljava/lang/Integer;

.field public error_message:Ljava/lang/String;

.field public session_token:Ljava/lang/String;

.field public success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/jedi/service/StartSessionResponse;
    .locals 7

    .line 153
    new-instance v6, Lcom/squareup/protos/jedi/service/StartSessionResponse;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->session_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->error_message:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->code:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/jedi/service/StartSessionResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->build()Lcom/squareup/protos/jedi/service/StartSessionResponse;

    move-result-object v0

    return-object v0
.end method

.method public code(Ljava/lang/Integer;)Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->code:Ljava/lang/Integer;

    return-object p0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method
