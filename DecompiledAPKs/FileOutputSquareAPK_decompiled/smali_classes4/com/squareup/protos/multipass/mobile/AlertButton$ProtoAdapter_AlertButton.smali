.class final Lcom/squareup/protos/multipass/mobile/AlertButton$ProtoAdapter_AlertButton;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AlertButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/mobile/AlertButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AlertButton"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/mobile/AlertButton;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 185
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/mobile/AlertButton;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/mobile/AlertButton;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 208
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;-><init>()V

    .line 209
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 210
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 217
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 215
    :cond_0
    sget-object v3, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    goto :goto_0

    .line 214
    :cond_1
    sget-object v3, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url(Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    goto :goto_0

    .line 213
    :cond_2
    sget-object v3, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss(Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    goto :goto_0

    .line 212
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->title(Ljava/lang/String;)Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    goto :goto_0

    .line 221
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 222
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButton;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 183
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/mobile/AlertButton$ProtoAdapter_AlertButton;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/mobile/AlertButton;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/mobile/AlertButton;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 199
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 200
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 201
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 202
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 203
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/mobile/AlertButton;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 183
    check-cast p2, Lcom/squareup/protos/multipass/mobile/AlertButton;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/mobile/AlertButton$ProtoAdapter_AlertButton;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/mobile/AlertButton;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/mobile/AlertButton;)I
    .locals 4

    .line 190
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    const/4 v3, 0x2

    .line 191
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    const/4 v3, 0x3

    .line 192
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    const/4 v3, 0x4

    .line 193
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButton;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/protos/multipass/mobile/AlertButton;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/mobile/AlertButton$ProtoAdapter_AlertButton;->encodedSize(Lcom/squareup/protos/multipass/mobile/AlertButton;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/mobile/AlertButton;)Lcom/squareup/protos/multipass/mobile/AlertButton;
    .locals 2

    .line 227
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButton;->newBuilder()Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    move-result-object p1

    .line 228
    iget-object v0, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    iput-object v0, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    .line 229
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    iput-object v0, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    .line 230
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    iput-object v0, p1, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    .line 231
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 232
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButton;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/protos/multipass/mobile/AlertButton;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/mobile/AlertButton$ProtoAdapter_AlertButton;->redact(Lcom/squareup/protos/multipass/mobile/AlertButton;)Lcom/squareup/protos/multipass/mobile/AlertButton;

    move-result-object p1

    return-object p1
.end method
