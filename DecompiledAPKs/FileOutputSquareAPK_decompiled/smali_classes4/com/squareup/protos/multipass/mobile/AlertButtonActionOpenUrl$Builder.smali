.class public final Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AlertButtonActionOpenUrl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public dismiss_after_open:Ljava/lang/Boolean;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;
    .locals 4

    .line 123
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;->dismiss_after_open:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    move-result-object v0

    return-object v0
.end method

.method public dismiss_after_open(Ljava/lang/Boolean;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;->dismiss_after_open:Ljava/lang/Boolean;

    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl$Builder;->url:Ljava/lang/String;

    return-object p0
.end method
