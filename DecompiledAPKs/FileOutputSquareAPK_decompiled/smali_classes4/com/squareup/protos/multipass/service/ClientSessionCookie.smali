.class public final Lcom/squareup/protos/multipass/service/ClientSessionCookie;
.super Lcom/squareup/wire/Message;
.source "ClientSessionCookie.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/service/ClientSessionCookie$ProtoAdapter_ClientSessionCookie;,
        Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/service/ClientSessionCookie;",
        "Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/service/ClientSessionCookie;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SCOPE:Lcom/squareup/protos/multipass/common/ScopedSession;

.field public static final DEFAULT_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final scope:Lcom/squareup/protos/multipass/common/ScopedSession;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.common.ScopedSession#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie$ProtoAdapter_ClientSessionCookie;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/ClientSessionCookie$ProtoAdapter_ClientSessionCookie;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/protos/multipass/common/ScopedSession;->SQUARE:Lcom/squareup/protos/multipass/common/ScopedSession;

    sput-object v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->DEFAULT_SCOPE:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/multipass/common/ScopedSession;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/multipass/service/ClientSessionCookie;-><init>(Ljava/lang/String;Lcom/squareup/protos/multipass/common/ScopedSession;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/multipass/common/ScopedSession;Lokio/ByteString;)V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->value:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 72
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 73
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->value:Ljava/lang/String;

    .line 75
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    iget-object p1, p1, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 76
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 81
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->value:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/ScopedSession;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 86
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;-><init>()V

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;->value:Ljava/lang/String;

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->newBuilder()Lcom/squareup/protos/multipass/service/ClientSessionCookie$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->value:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", value=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    if-eqz v1, :cond_1

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->scope:Lcom/squareup/protos/multipass/common/ScopedSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ClientSessionCookie{"

    .line 96
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
