.class final Lcom/squareup/protos/multipass/service/SessionHeaders$ProtoAdapter_SessionHeaders;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SessionHeaders.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/SessionHeaders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SessionHeaders"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/service/SessionHeaders;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 138
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/service/SessionHeaders;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/service/SessionHeaders;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 157
    new-instance v0, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;-><init>()V

    .line 158
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 159
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 164
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 162
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;->acceptable_scopes:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Headers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Headers;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;->headers(Lcom/squareup/protos/common/Headers;)Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;

    goto :goto_0

    .line 168
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 169
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;->build()Lcom/squareup/protos/multipass/service/SessionHeaders;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/SessionHeaders$ProtoAdapter_SessionHeaders;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/service/SessionHeaders;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/service/SessionHeaders;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 150
    sget-object v0, Lcom/squareup/protos/common/Headers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/SessionHeaders;->headers:Lcom/squareup/protos/common/Headers;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 151
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/SessionHeaders;->acceptable_scopes:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 152
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/service/SessionHeaders;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    check-cast p2, Lcom/squareup/protos/multipass/service/SessionHeaders;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/service/SessionHeaders$ProtoAdapter_SessionHeaders;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/service/SessionHeaders;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/service/SessionHeaders;)I
    .locals 4

    .line 143
    sget-object v0, Lcom/squareup/protos/common/Headers;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/SessionHeaders;->headers:Lcom/squareup/protos/common/Headers;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 144
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/multipass/service/SessionHeaders;->acceptable_scopes:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/SessionHeaders;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 136
    check-cast p1, Lcom/squareup/protos/multipass/service/SessionHeaders;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/SessionHeaders$ProtoAdapter_SessionHeaders;->encodedSize(Lcom/squareup/protos/multipass/service/SessionHeaders;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/service/SessionHeaders;)Lcom/squareup/protos/multipass/service/SessionHeaders;
    .locals 1

    .line 174
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/SessionHeaders;->newBuilder()Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 175
    iput-object v0, p1, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;->headers:Lcom/squareup/protos/common/Headers;

    .line 176
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 177
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/SessionHeaders$Builder;->build()Lcom/squareup/protos/multipass/service/SessionHeaders;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 136
    check-cast p1, Lcom/squareup/protos/multipass/service/SessionHeaders;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/SessionHeaders$ProtoAdapter_SessionHeaders;->redact(Lcom/squareup/protos/multipass/service/SessionHeaders;)Lcom/squareup/protos/multipass/service/SessionHeaders;

    move-result-object p1

    return-object p1
.end method
