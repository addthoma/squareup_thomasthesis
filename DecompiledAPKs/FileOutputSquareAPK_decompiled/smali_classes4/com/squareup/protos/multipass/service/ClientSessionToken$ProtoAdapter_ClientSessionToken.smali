.class final Lcom/squareup/protos/multipass/service/ClientSessionToken$ProtoAdapter_ClientSessionToken;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientSessionToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/ClientSessionToken;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ClientSessionToken"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/service/ClientSessionToken;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 110
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/service/ClientSessionToken;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/service/ClientSessionToken;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 127
    new-instance v0, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;-><init>()V

    .line 128
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 129
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 133
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 131
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;->value(Lokio/ByteString;)Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;

    goto :goto_0

    .line 137
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 138
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;->build()Lcom/squareup/protos/multipass/service/ClientSessionToken;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/ClientSessionToken$ProtoAdapter_ClientSessionToken;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/service/ClientSessionToken;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/service/ClientSessionToken;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/service/ClientSessionToken;->value:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 122
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/service/ClientSessionToken;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    check-cast p2, Lcom/squareup/protos/multipass/service/ClientSessionToken;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/service/ClientSessionToken$ProtoAdapter_ClientSessionToken;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/service/ClientSessionToken;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/service/ClientSessionToken;)I
    .locals 3

    .line 115
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/service/ClientSessionToken;->value:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 116
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientSessionToken;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 108
    check-cast p1, Lcom/squareup/protos/multipass/service/ClientSessionToken;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/ClientSessionToken$ProtoAdapter_ClientSessionToken;->encodedSize(Lcom/squareup/protos/multipass/service/ClientSessionToken;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/service/ClientSessionToken;)Lcom/squareup/protos/multipass/service/ClientSessionToken;
    .locals 1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientSessionToken;->newBuilder()Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 144
    iput-object v0, p1, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;->value:Lokio/ByteString;

    .line 145
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 146
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;->build()Lcom/squareup/protos/multipass/service/ClientSessionToken;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 108
    check-cast p1, Lcom/squareup/protos/multipass/service/ClientSessionToken;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/service/ClientSessionToken$ProtoAdapter_ClientSessionToken;->redact(Lcom/squareup/protos/multipass/service/ClientSessionToken;)Lcom/squareup/protos/multipass/service/ClientSessionToken;

    move-result-object p1

    return-object p1
.end method
