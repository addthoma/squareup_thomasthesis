.class public final Lcom/squareup/protos/multipass/service/OneTimeKey;
.super Lcom/squareup/wire/Message;
.source "OneTimeKey.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/service/OneTimeKey$ProtoAdapter_OneTimeKey;,
        Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/service/OneTimeKey;",
        "Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/service/OneTimeKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_VALUE:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final value:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/multipass/service/OneTimeKey$ProtoAdapter_OneTimeKey;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/OneTimeKey$ProtoAdapter_OneTimeKey;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/service/OneTimeKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/multipass/service/OneTimeKey;->DEFAULT_VALUE:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;)V
    .locals 1

    .line 42
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/multipass/service/OneTimeKey;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/protos/multipass/service/OneTimeKey;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/OneTimeKey;->value:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 61
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/service/OneTimeKey;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 62
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/service/OneTimeKey;

    .line 63
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/OneTimeKey;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/OneTimeKey;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/OneTimeKey;->value:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/multipass/service/OneTimeKey;->value:Lokio/ByteString;

    .line 64
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 69
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/OneTimeKey;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/OneTimeKey;->value:Lokio/ByteString;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 73
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;
    .locals 2

    .line 52
    new-instance v0, Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;-><init>()V

    .line 53
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/OneTimeKey;->value:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;->value:Lokio/ByteString;

    .line 54
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/OneTimeKey;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/OneTimeKey;->newBuilder()Lcom/squareup/protos/multipass/service/OneTimeKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/OneTimeKey;->value:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", value=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OneTimeKey{"

    .line 82
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
