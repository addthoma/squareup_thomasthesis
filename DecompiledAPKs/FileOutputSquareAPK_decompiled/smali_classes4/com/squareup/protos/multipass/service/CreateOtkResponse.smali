.class public final Lcom/squareup/protos/multipass/service/CreateOtkResponse;
.super Lcom/squareup/wire/Message;
.source "CreateOtkResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/service/CreateOtkResponse$ProtoAdapter_CreateOtkResponse;,
        Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/service/CreateOtkResponse;",
        "Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/service/CreateOtkResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR:Lcom/squareup/protos/multipass/service/Error;

.field private static final serialVersionUID:J


# instance fields
.field public final error:Lcom/squareup/protos/multipass/service/Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.service.Error#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.service.OneTimeKey#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/multipass/service/CreateOtkResponse$ProtoAdapter_CreateOtkResponse;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/CreateOtkResponse$ProtoAdapter_CreateOtkResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lcom/squareup/protos/multipass/service/Error;->NO_ERROR:Lcom/squareup/protos/multipass/service/Error;

    sput-object v0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->DEFAULT_ERROR:Lcom/squareup/protos/multipass/service/Error;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/service/Error;Lcom/squareup/protos/multipass/service/OneTimeKey;)V
    .locals 1

    .line 46
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/multipass/service/CreateOtkResponse;-><init>(Lcom/squareup/protos/multipass/service/Error;Lcom/squareup/protos/multipass/service/OneTimeKey;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/service/Error;Lcom/squareup/protos/multipass/service/OneTimeKey;Lokio/ByteString;)V
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 51
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->error:Lcom/squareup/protos/multipass/service/Error;

    .line 52
    iput-object p2, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 67
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/service/CreateOtkResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 68
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/service/CreateOtkResponse;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->error:Lcom/squareup/protos/multipass/service/Error;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->error:Lcom/squareup/protos/multipass/service/Error;

    .line 70
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    iget-object p1, p1, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    .line 71
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 76
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->error:Lcom/squareup/protos/multipass/service/Error;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/service/Error;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/service/OneTimeKey;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 81
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;
    .locals 2

    .line 57
    new-instance v0, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;-><init>()V

    .line 58
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->error:Lcom/squareup/protos/multipass/service/Error;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;->error:Lcom/squareup/protos/multipass/service/Error;

    .line 59
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    .line 60
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->newBuilder()Lcom/squareup/protos/multipass/service/CreateOtkResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->error:Lcom/squareup/protos/multipass/service/Error;

    if-eqz v1, :cond_0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->error:Lcom/squareup/protos/multipass/service/Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    if-eqz v1, :cond_1

    const-string v1, ", one_time_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkResponse;->one_time_key:Lcom/squareup/protos/multipass/service/OneTimeKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreateOtkResponse{"

    .line 91
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
