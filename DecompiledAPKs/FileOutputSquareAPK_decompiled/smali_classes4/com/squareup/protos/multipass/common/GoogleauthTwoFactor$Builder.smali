.class public final Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GoogleauthTwoFactor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;",
        "Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_name:Ljava/lang/String;

.field public authenticator_key_base32:Ljava/lang/String;

.field public authenticator_key_uri:Ljava/lang/String;

.field public verification_code:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 142
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_name(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->account_name:Ljava/lang/String;

    return-object p0
.end method

.method public authenticator_key_base32(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->authenticator_key_base32:Ljava/lang/String;

    return-object p0
.end method

.method public authenticator_key_uri(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->authenticator_key_uri:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;
    .locals 7

    .line 179
    new-instance v6, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->verification_code:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->authenticator_key_base32:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->account_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->authenticator_key_uri:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 133
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    move-result-object v0

    return-object v0
.end method

.method public verification_code(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->verification_code:Ljava/lang/String;

    return-object p0
.end method
