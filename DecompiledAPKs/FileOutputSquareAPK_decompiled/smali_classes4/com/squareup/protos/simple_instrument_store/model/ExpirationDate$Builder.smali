.class public final Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ExpirationDate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;",
        "Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public month:Ljava/lang/Integer;

.field public year:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;
    .locals 4

    .line 124
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;->year:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;->month:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;->build()Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    move-result-object v0

    return-object v0
.end method

.method public month(Ljava/lang/Integer;)Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;->month:Ljava/lang/Integer;

    return-object p0
.end method

.method public year(Ljava/lang/Integer;)Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;->year:Ljava/lang/Integer;

    return-object p0
.end method
