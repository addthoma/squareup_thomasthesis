.class final Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$ProtoAdapter_Loan;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FinancingTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Loan"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 321
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 354
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;-><init>()V

    .line 355
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 356
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 368
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 366
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_amount_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 365
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->post_maturity_sweeper_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 364
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->late_fee_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 363
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->apr_bps(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 362
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->total_owed_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 361
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->outstanding_balance_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 360
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->deposit_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 359
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 358
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->financed_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    goto :goto_0

    .line 372
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 373
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 319
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$ProtoAdapter_Loan;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 340
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->financed_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 341
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->fee_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 342
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->deposit_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 343
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 344
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->total_owed_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 345
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->apr_bps:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 346
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->late_fee_bps:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 347
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->post_maturity_sweeper_bps:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 348
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->fee_amount_bps:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 349
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 319
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$ProtoAdapter_Loan;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)I
    .locals 4

    .line 326
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->financed_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->fee_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 327
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->deposit_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 328
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 329
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->total_owed_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 330
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->apr_bps:Ljava/lang/String;

    const/4 v3, 0x6

    .line 331
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->late_fee_bps:Ljava/lang/String;

    const/4 v3, 0x7

    .line 332
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->post_maturity_sweeper_bps:Ljava/lang/String;

    const/16 v3, 0x8

    .line 333
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->fee_amount_bps:Ljava/lang/String;

    const/16 v3, 0x9

    .line 334
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 319
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$ProtoAdapter_Loan;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;
    .locals 2

    .line 378
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;

    move-result-object p1

    .line 379
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->financed_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->financed_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->financed_money:Lcom/squareup/protos/common/Money;

    .line 380
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->fee_money:Lcom/squareup/protos/common/Money;

    .line 381
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->deposit_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->deposit_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->deposit_money:Lcom/squareup/protos/common/Money;

    .line 382
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->outstanding_balance_money:Lcom/squareup/protos/common/Money;

    .line 383
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->total_owed_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->total_owed_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->total_owed_money:Lcom/squareup/protos/common/Money;

    .line 384
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 385
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 319
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan$ProtoAdapter_Loan;->redact(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    move-result-object p1

    return-object p1
.end method
