.class final Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$ProtoAdapter_InstrumentSummary;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InstrumentSummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InstrumentSummary"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 186
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 209
    new-instance v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;-><init>()V

    .line 210
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 211
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 218
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 216
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->is_active(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;

    goto :goto_0

    .line 215
    :cond_1
    sget-object v3, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->card_summary(Lcom/squareup/protos/simple_instrument_store/api/CardSummary;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;

    goto :goto_0

    .line 214
    :cond_2
    sget-object v3, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->bank_account_summary(Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;

    goto :goto_0

    .line 213
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;

    goto :goto_0

    .line 222
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 223
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->build()Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 184
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$ProtoAdapter_InstrumentSummary;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 200
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 201
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 202
    sget-object v0, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 203
    sget-object v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 204
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 184
    check-cast p2, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$ProtoAdapter_InstrumentSummary;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;)I
    .locals 4

    .line 191
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 192
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    const/4 v3, 0x2

    .line 193
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    const/4 v3, 0x3

    .line 194
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 184
    check-cast p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$ProtoAdapter_InstrumentSummary;->encodedSize(Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;
    .locals 2

    .line 228
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->newBuilder()Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;

    move-result-object p1

    .line 229
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    .line 230
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    .line 231
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 232
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->build()Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 184
    check-cast p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$ProtoAdapter_InstrumentSummary;->redact(Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;)Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    move-result-object p1

    return-object p1
.end method
