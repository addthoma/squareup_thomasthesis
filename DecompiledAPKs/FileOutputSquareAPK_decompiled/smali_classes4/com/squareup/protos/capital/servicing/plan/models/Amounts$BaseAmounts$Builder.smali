.class public final Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Amounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public paid_money:Lcom/squareup/protos/common/Money;

.field public remaining_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 241
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;
    .locals 4

    .line 256
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 236
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    move-result-object v0

    return-object v0
.end method

.method public paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
