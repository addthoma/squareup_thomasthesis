.class final Lcom/squareup/protos/capital/servicing/plan/models/Payer$ProtoAdapter_Payer;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Payer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Payer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Payer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Payer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 225
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/Payer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 250
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;-><init>()V

    .line 251
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 252
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_2

    const/4 v4, 0x5

    if-eq v3, v4, :cond_1

    const/4 v4, 0x6

    if-eq v3, v4, :cond_0

    .line 260
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 258
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->instruments:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 257
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->business_name(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;

    goto :goto_0

    .line 256
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->can_debit_sq_balance_linked_account(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;

    goto :goto_0

    .line 255
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->is_primary(Ljava/lang/Boolean;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;

    goto :goto_0

    .line 254
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->customer_id(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;

    goto :goto_0

    .line 264
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 265
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 223
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$ProtoAdapter_Payer;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/Payer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 240
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 241
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 242
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 243
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->business_name:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 244
    sget-object v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 245
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 223
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$ProtoAdapter_Payer;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/Payer;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/Payer;)I
    .locals 4

    .line 230
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 231
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 232
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->business_name:Ljava/lang/String;

    const/4 v3, 0x5

    .line 233
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 234
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 223
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$ProtoAdapter_Payer;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/Payer;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/Payer;)Lcom/squareup/protos/capital/servicing/plan/models/Payer;
    .locals 2

    .line 270
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 271
    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->business_name:Ljava/lang/String;

    .line 272
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->instruments:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 273
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 274
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 223
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$ProtoAdapter_Payer;->redact(Lcom/squareup/protos/capital/servicing/plan/models/Payer;)Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    move-result-object p1

    return-object p1
.end method
