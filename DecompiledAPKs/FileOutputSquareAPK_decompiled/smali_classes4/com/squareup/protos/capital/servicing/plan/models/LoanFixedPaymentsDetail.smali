.class public final Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;
.super Lcom/squareup/wire/Message;
.source "LoanFixedPaymentsDetail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$ProtoAdapter_LoanFixedPaymentsDetail;,
        Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAST_DUE_STARTED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE_COMPLETE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final due_and_past_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.FinancingTerms$Loan#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final late_fee_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final paid_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final past_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final past_due_started_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final percentage_complete:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final remaining_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.RepaymentTerms$FixedPayments#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final scheduled_payments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.ScheduledPayment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;",
            ">;"
        }
    .end annotation
.end field

.field public final total_due_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xc
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$ProtoAdapter_LoanFixedPaymentsDetail;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$ProtoAdapter_LoanFixedPaymentsDetail;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;Lokio/ByteString;)V
    .locals 1

    .line 205
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 206
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    .line 207
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    .line 208
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    .line 209
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_started_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    .line 210
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 211
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    .line 212
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    .line 213
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 214
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    .line 215
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->percentage_complete:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    .line 216
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->scheduled_payments:Ljava/util/List;

    const-string v0, "scheduled_payments"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    .line 217
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    .line 218
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    .line 219
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    .line 220
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 221
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 222
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 223
    iget-object p2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 224
    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 256
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 257
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;

    .line 258
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    .line 259
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    .line 260
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    .line 261
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    .line 262
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 263
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    .line 264
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    .line 265
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 266
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    .line 267
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    .line 268
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    .line 269
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    .line 270
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    .line 271
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    .line 272
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 273
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 274
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 275
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 276
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 277
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 282
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_12

    .line 284
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 285
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 286
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 287
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 288
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 289
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 290
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 291
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 292
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 293
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 294
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 295
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 296
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 297
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 298
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 299
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 300
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 301
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 302
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 303
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_11
    add-int/2addr v0, v2

    .line 304
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_12
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;
    .locals 2

    .line 229
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;-><init>()V

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_started_at:Ljava/lang/String;

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->percentage_complete:Ljava/lang/String;

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->scheduled_payments:Ljava/util/List;

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 249
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    if-eqz v1, :cond_0

    const-string v1, ", financing_terms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 313
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    if-eqz v1, :cond_1

    const-string v1, ", repayment_terms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$FixedPayments;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 314
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 315
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", past_due_started_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_started_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", past_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 317
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", late_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 318
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", paid_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 319
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", remaining_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 320
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 321
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", percentage_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->percentage_complete:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", scheduled_payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->scheduled_payments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 323
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    const-string v1, ", total_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 324
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_c

    const-string v1, ", due_and_past_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 325
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_d

    const-string v1, ", remaining_excluding_past_due_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 326
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_e

    const-string v1, ", due_and_past_due_excluding_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->due_and_past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 327
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    const-string v1, ", remaining_excluding_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 328
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_10

    const-string v1, ", remaining_excluding_past_due_and_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 329
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_11

    const-string v1, ", late_fee_excluding_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->late_fee_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 330
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_12

    const-string v1, ", past_due_excluding_pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/LoanFixedPaymentsDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_12
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoanFixedPaymentsDetail{"

    .line 331
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
