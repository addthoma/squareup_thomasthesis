.class public final Lcom/squareup/protos/capital/external/business/models/PreviewPlan;
.super Lcom/squareup/wire/Message;
.source "PreviewPlan.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/external/business/models/PreviewPlan$ProtoAdapter_PreviewPlan;,
        Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
        "Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LEGACY_PLAN_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PLAN_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PLAN_SERVICING_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_PRODUCT_TYPE:Lcom/squareup/protos/capital/external/business/models/ProductType;

.field public static final DEFAULT_RFC3986_PLAN_SERVICING_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final legacy_plan_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final plan_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final plan_servicing_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.ProductType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final rfc3986_plan_servicing_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$ProtoAdapter_PreviewPlan;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$ProtoAdapter_PreviewPlan;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/ProductType;->PT_DO_NOT_USE:Lcom/squareup/protos/capital/external/business/models/ProductType;

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->DEFAULT_PRODUCT_TYPE:Lcom/squareup/protos/capital/external/business/models/ProductType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ProductType;Ljava/lang/String;)V
    .locals 7

    .line 80
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ProductType;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ProductType;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 86
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_servicing_url:Ljava/lang/String;

    .line 88
    iput-object p3, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->legacy_plan_id:Ljava/lang/String;

    .line 89
    iput-object p4, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    .line 90
    iput-object p5, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 108
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 109
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_servicing_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_servicing_url:Ljava/lang/String;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->legacy_plan_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->legacy_plan_id:Ljava/lang/String;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_servicing_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->legacy_plan_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/ProductType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 128
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;
    .locals 2

    .line 95
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;-><init>()V

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->plan_id:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_servicing_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->plan_servicing_url:Ljava/lang/String;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->legacy_plan_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->legacy_plan_id:Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->rfc3986_plan_servicing_url:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->newBuilder()Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", plan_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_servicing_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", plan_servicing_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_servicing_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->legacy_plan_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", legacy_plan_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->legacy_plan_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    if-eqz v1, :cond_3

    const-string v1, ", product_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", rfc3986_plan_servicing_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PreviewPlan{"

    .line 141
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
