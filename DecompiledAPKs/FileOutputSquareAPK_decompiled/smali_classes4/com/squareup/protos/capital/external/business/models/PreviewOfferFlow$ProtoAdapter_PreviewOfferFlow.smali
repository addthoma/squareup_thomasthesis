.class final Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$ProtoAdapter_PreviewOfferFlow;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PreviewOfferFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PreviewOfferFlow"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 154
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 175
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;-><init>()V

    .line 176
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 177
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 197
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 190
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/capital/external/business/models/ReviewType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/capital/external/business/models/ReviewType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->review_type(Lcom/squareup/protos/capital/external/business/models/ReviewType;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 192
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 182
    :cond_1
    :try_start_1
    sget-object v4, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->status(Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 184
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 179
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->offer_flow_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;

    goto :goto_0

    .line 201
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 202
    invoke-virtual {v0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->build()Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$ProtoAdapter_PreviewOfferFlow;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 167
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 168
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 169
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/ReviewType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 170
    invoke-virtual {p2}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 152
    check-cast p2, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$ProtoAdapter_PreviewOfferFlow;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;)I
    .locals 4

    .line 159
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    const/4 v3, 0x2

    .line 160
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/external/business/models/ReviewType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    const/4 v3, 0x3

    .line 161
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 152
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$ProtoAdapter_PreviewOfferFlow;->encodedSize(Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;
    .locals 0

    .line 207
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->newBuilder()Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;

    move-result-object p1

    .line 208
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 209
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->build()Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 152
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$ProtoAdapter_PreviewOfferFlow;->redact(Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;)Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    move-result-object p1

    return-object p1
.end method
