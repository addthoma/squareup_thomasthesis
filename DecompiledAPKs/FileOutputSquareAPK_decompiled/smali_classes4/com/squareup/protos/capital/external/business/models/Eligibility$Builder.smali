.class public final Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Eligibility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/Eligibility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/external/business/models/Eligibility;",
        "Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public expired_at:Ljava/lang/String;

.field public type:Lcom/squareup/protos/capital/external/business/models/EligibilityType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/external/business/models/Eligibility;
    .locals 4

    .line 109
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/Eligibility;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;->type:Lcom/squareup/protos/capital/external/business/models/EligibilityType;

    iget-object v2, p0, Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;->expired_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/capital/external/business/models/Eligibility;-><init>(Lcom/squareup/protos/capital/external/business/models/EligibilityType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;->build()Lcom/squareup/protos/capital/external/business/models/Eligibility;

    move-result-object v0

    return-object v0
.end method

.method public expired_at(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;->expired_at:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/capital/external/business/models/EligibilityType;)Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Eligibility$Builder;->type:Lcom/squareup/protos/capital/external/business/models/EligibilityType;

    return-object p0
.end method
