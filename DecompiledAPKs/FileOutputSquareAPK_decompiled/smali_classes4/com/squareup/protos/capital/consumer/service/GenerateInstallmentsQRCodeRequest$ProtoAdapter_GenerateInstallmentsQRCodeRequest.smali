.class final Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$ProtoAdapter_GenerateInstallmentsQRCodeRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GenerateInstallmentsQRCodeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GenerateInstallmentsQRCodeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 163
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 187
    new-instance v0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;-><init>()V

    .line 188
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 189
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 196
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 194
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->image_height_px(Ljava/lang/Integer;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;

    goto :goto_0

    .line 193
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->image_width_px(Ljava/lang/Integer;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;

    goto :goto_0

    .line 192
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;

    goto :goto_0

    .line 191
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;

    goto :goto_0

    .line 200
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 201
    invoke-virtual {v0}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->build()Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 161
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$ProtoAdapter_GenerateInstallmentsQRCodeRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 178
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->idempotence_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 179
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 180
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->image_width_px:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 181
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->image_height_px:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 182
    invoke-virtual {p2}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 161
    check-cast p2, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$ProtoAdapter_GenerateInstallmentsQRCodeRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;)I
    .locals 4

    .line 168
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->idempotence_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v3, 0x2

    .line 169
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->image_width_px:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 170
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->image_height_px:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 171
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 161
    check-cast p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$ProtoAdapter_GenerateInstallmentsQRCodeRequest;->encodedSize(Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;
    .locals 2

    .line 206
    invoke-virtual {p1}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;->newBuilder()Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;

    move-result-object p1

    .line 207
    iget-object v0, p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart;

    iput-object v0, p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 208
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 209
    invoke-virtual {p1}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$Builder;->build()Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 161
    check-cast p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest$ProtoAdapter_GenerateInstallmentsQRCodeRequest;->redact(Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;)Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    move-result-object p1

    return-object p1
.end method
