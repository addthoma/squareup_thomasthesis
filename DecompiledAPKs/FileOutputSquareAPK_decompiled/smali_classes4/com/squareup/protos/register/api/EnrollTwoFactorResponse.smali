.class public final Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;
.super Lcom/squareup/wire/Message;
.source "EnrollTwoFactorResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$ProtoAdapter_EnrollTwoFactorResponse;,
        Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COMPLETE:Ljava/lang/Boolean;

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_SESSION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final complete:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final session_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.common.TrustedDeviceDetails#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.common.TwoFactorDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$ProtoAdapter_EnrollTwoFactorResponse;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$ProtoAdapter_EnrollTwoFactorResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->DEFAULT_COMPLETE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 95
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 101
    sget-object v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    .line 103
    iput-object p2, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->session_token:Ljava/lang/String;

    .line 104
    iput-object p3, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 105
    iput-object p4, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    .line 106
    iput-object p5, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    .line 107
    iput-object p6, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 126
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 127
    :cond_1
    check-cast p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->session_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->session_token:Ljava/lang/String;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v3, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    iget-object v3, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    .line 134
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 139
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 141
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->session_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 148
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;
    .locals 2

    .line 112
    new-instance v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;-><init>()V

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->complete:Ljava/lang/Boolean;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->session_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->session_token:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iput-object v1, v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    iput-object v1, v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->error_title:Ljava/lang/String;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->error_message:Ljava/lang/String;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->newBuilder()Lcom/squareup/protos/register/api/EnrollTwoFactorResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->complete:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->session_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", session_token=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-eqz v1, :cond_2

    const-string v1, ", two_factor_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    if-eqz v1, :cond_3

    const-string v1, ", trusted_device_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EnrollTwoFactorResponse{"

    .line 162
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
