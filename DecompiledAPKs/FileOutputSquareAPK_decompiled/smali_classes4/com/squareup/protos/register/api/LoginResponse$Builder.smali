.class public final Lcom/squareup/protos/register/api/LoginResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoginResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/LoginResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "Lcom/squareup/protos/register/api/LoginResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_skip_two_factor_enroll:Ljava/lang/Boolean;

.field public can_use_google_authenticator:Ljava/lang/Boolean;

.field public can_use_sms:Ljava/lang/Boolean;

.field public captcha_api_key:Ljava/lang/String;

.field public captcha_required:Ljava/lang/Boolean;

.field public error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public requires_two_factor:Ljava/lang/Boolean;

.field public session_token:Ljava/lang/String;

.field public two_factor_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;"
        }
    .end annotation
.end field

.field public unit:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 304
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 305
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->unit:Ljava/util/List;

    .line 306
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->two_factor_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/LoginResponse;
    .locals 15

    .line 420
    new-instance v14, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->session_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->unit:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->requires_two_factor:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->two_factor_details:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_message:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    iget-object v9, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_use_google_authenticator:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_use_sms:Ljava/lang/Boolean;

    iget-object v11, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->captcha_required:Ljava/lang/Boolean;

    iget-object v12, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->captcha_api_key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v13

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/register/api/LoginResponse;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/Alert;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v14
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 279
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginResponse$Builder;->build()Lcom/squareup/protos/register/api/LoginResponse;

    move-result-object v0

    return-object v0
.end method

.method public can_skip_two_factor_enroll(Ljava/lang/Boolean;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 352
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_skip_two_factor_enroll:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_google_authenticator(Ljava/lang/Boolean;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 387
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_use_google_authenticator:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_sms(Ljava/lang/Boolean;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->can_use_sms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public captcha_api_key(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 414
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->captcha_api_key:Ljava/lang/String;

    return-object p0
.end method

.method public captcha_required(Ljava/lang/Boolean;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 405
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->captcha_required:Ljava/lang/Boolean;

    return-object p0
.end method

.method public error_alert(Lcom/squareup/protos/multipass/mobile/Alert;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 379
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    return-object p0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 368
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public requires_two_factor(Ljava/lang/Boolean;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->requires_two_factor:Ljava/lang/Boolean;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method

.method public two_factor_details(Ljava/util/List;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;)",
            "Lcom/squareup/protos/register/api/LoginResponse$Builder;"
        }
    .end annotation

    .line 343
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 344
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->two_factor_details:Ljava/util/List;

    return-object p0
.end method

.method public unit(Ljava/util/List;)Lcom/squareup/protos/register/api/LoginResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/register/api/Unit;",
            ">;)",
            "Lcom/squareup/protos/register/api/LoginResponse$Builder;"
        }
    .end annotation

    .line 324
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 325
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginResponse$Builder;->unit:Ljava/util/List;

    return-object p0
.end method
