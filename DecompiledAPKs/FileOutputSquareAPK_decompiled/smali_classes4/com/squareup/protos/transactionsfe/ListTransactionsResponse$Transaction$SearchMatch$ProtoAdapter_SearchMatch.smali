.class final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$ProtoAdapter_SearchMatch;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SearchMatch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 967
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 988
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;-><init>()V

    .line 989
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 990
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 996
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 994
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value_length(Ljava/lang/Integer;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;

    goto :goto_0

    .line 993
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value_start_index(Ljava/lang/Integer;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;

    goto :goto_0

    .line 992
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;

    goto :goto_0

    .line 1000
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1001
    invoke-virtual {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 965
    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$ProtoAdapter_SearchMatch;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 980
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 981
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 982
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 983
    invoke-virtual {p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 965
    check-cast p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$ProtoAdapter_SearchMatch;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;)I
    .locals 4

    .line 972
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 973
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 974
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 975
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 965
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$ProtoAdapter_SearchMatch;->encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;
    .locals 0

    .line 1006
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;

    move-result-object p1

    .line 1007
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1008
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 965
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$ProtoAdapter_SearchMatch;->redact(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    move-result-object p1

    return-object p1
.end method
