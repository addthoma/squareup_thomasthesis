.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public matching_value:Ljava/lang/String;

.field public matching_value_length:Ljava/lang/Integer;

.field public matching_value_start_index:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 932
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;
    .locals 5

    .line 961
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value_start_index:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value_length:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 925
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    move-result-object v0

    return-object v0
.end method

.method public matching_value(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;
    .locals 0

    .line 939
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value:Ljava/lang/String;

    return-object p0
.end method

.method public matching_value_length(Ljava/lang/Integer;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;
    .locals 0

    .line 955
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value_length:Ljava/lang/Integer;

    return-object p0
.end method

.method public matching_value_start_index(Ljava/lang/Integer;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;
    .locals 0

    .line 947
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value_start_index:Ljava/lang/Integer;

    return-object p0
.end method
