.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;
.super Lcom/squareup/wire/Message;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TenderInformation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$ProtoAdapter_TenderInformation;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CARD_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final DEFAULT_FELICA_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public static final DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$Brand#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$EntryMethod#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$FelicaBrand#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final tender_type:Lcom/squareup/protos/client/bills/Tender$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 584
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$ProtoAdapter_TenderInformation;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$ProtoAdapter_TenderInformation;-><init>()V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 588
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 590
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->DEFAULT_CARD_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 592
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->DEFAULT_FELICA_BRAND_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->DEFAULT_FELICA_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 594
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 6

    .line 634
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;-><init>(Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V
    .locals 1

    .line 640
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 641
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 642
    iput-object p2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 643
    iput-object p3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 644
    iput-object p4, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 661
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 662
    :cond_1
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    .line 663
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 664
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 665
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 666
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object p1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 667
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 672
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 674
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 675
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 676
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 677
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 678
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 679
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    .locals 2

    .line 649
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;-><init>()V

    .line 650
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 651
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 652
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 653
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 654
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 583
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 686
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 687
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v1, :cond_0

    const-string v1, ", tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 688
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_1

    const-string v1, ", card_brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 689
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    if-eqz v1, :cond_2

    const-string v1, ", felica_brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 690
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_3

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TenderInformation{"

    .line 691
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
