.class public final Lcom/squareup/protos/bank_accounts/FrAccount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FrAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/FrAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bank_accounts/FrAccount;",
        "Lcom/squareup/protos/bank_accounts/FrAccount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bank_code:Ljava/lang/String;

.field public branch_code:Ljava/lang/String;

.field public swift_bic:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bank_code(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/FrAccount$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->bank_code:Ljava/lang/String;

    return-object p0
.end method

.method public branch_code(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/FrAccount$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->branch_code:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/bank_accounts/FrAccount;
    .locals 5

    .line 148
    new-instance v0, Lcom/squareup/protos/bank_accounts/FrAccount;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->bank_code:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->branch_code:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->swift_bic:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/bank_accounts/FrAccount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->build()Lcom/squareup/protos/bank_accounts/FrAccount;

    move-result-object v0

    return-object v0
.end method

.method public swift_bic(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/FrAccount$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/FrAccount$Builder;->swift_bic:Ljava/lang/String;

    return-object p0
.end method
