.class final Lcom/squareup/protos/bank_accounts/GbAccount$ProtoAdapter_GbAccount;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GbAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/GbAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GbAccount"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bank_accounts/GbAccount;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 125
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bank_accounts/GbAccount;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bank_accounts/GbAccount;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 144
    new-instance v0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;-><init>()V

    .line 145
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 146
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 151
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 149
    :cond_0
    sget-object v3, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->account_details(Lcom/squareup/protos/bank_accounts/GbAccountDetails;)Lcom/squareup/protos/bank_accounts/GbAccount$Builder;

    goto :goto_0

    .line 148
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->sort_code(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/GbAccount$Builder;

    goto :goto_0

    .line 155
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 156
    invoke-virtual {v0}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->build()Lcom/squareup/protos/bank_accounts/GbAccount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 123
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/GbAccount$ProtoAdapter_GbAccount;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bank_accounts/GbAccount;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bank_accounts/GbAccount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 137
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 138
    sget-object v0, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 139
    invoke-virtual {p2}, Lcom/squareup/protos/bank_accounts/GbAccount;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 123
    check-cast p2, Lcom/squareup/protos/bank_accounts/GbAccount;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bank_accounts/GbAccount$ProtoAdapter_GbAccount;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bank_accounts/GbAccount;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bank_accounts/GbAccount;)I
    .locals 4

    .line 130
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/GbAccount;->sort_code:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/GbAccount;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    const/4 v3, 0x2

    .line 131
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccount;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/protos/bank_accounts/GbAccount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/GbAccount$ProtoAdapter_GbAccount;->encodedSize(Lcom/squareup/protos/bank_accounts/GbAccount;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bank_accounts/GbAccount;)Lcom/squareup/protos/bank_accounts/GbAccount;
    .locals 2

    .line 161
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccount;->newBuilder()Lcom/squareup/protos/bank_accounts/GbAccount$Builder;

    move-result-object p1

    .line 162
    iget-object v0, p1, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    iput-object v0, p1, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->account_details:Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    .line 163
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 164
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->build()Lcom/squareup/protos/bank_accounts/GbAccount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/protos/bank_accounts/GbAccount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/GbAccount$ProtoAdapter_GbAccount;->redact(Lcom/squareup/protos/bank_accounts/GbAccount;)Lcom/squareup/protos/bank_accounts/GbAccount;

    move-result-object p1

    return-object p1
.end method
