.class final Lcom/squareup/protos/bank_accounts/GbAccountDetails$ProtoAdapter_GbAccountDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GbAccountDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/GbAccountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GbAccountDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bank_accounts/GbAccountDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 184
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bank_accounts/GbAccountDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 207
    new-instance v0, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;-><init>()V

    .line 208
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 209
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 216
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 214
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->bic(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;

    goto :goto_0

    .line 213
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_faster_payments(Ljava/lang/Boolean;)Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;

    goto :goto_0

    .line 212
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_direct_debit(Ljava/lang/Boolean;)Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;

    goto :goto_0

    .line 211
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->supports_bacs(Ljava/lang/Boolean;)Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;

    goto :goto_0

    .line 220
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 221
    invoke-virtual {v0}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->build()Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 182
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$ProtoAdapter_GbAccountDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bank_accounts/GbAccountDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 198
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->supports_bacs:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 199
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->supports_direct_debit:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 200
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->supports_faster_payments:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 201
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->bic:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 202
    invoke-virtual {p2}, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 182
    check-cast p2, Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$ProtoAdapter_GbAccountDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bank_accounts/GbAccountDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bank_accounts/GbAccountDetails;)I
    .locals 4

    .line 189
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->supports_bacs:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->supports_direct_debit:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 190
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->supports_faster_payments:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 191
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->bic:Ljava/lang/String;

    const/4 v3, 0x4

    .line 192
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 182
    check-cast p1, Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$ProtoAdapter_GbAccountDetails;->encodedSize(Lcom/squareup/protos/bank_accounts/GbAccountDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bank_accounts/GbAccountDetails;)Lcom/squareup/protos/bank_accounts/GbAccountDetails;
    .locals 0

    .line 226
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails;->newBuilder()Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;

    move-result-object p1

    .line 227
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 228
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$Builder;->build()Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 182
    check-cast p1, Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bank_accounts/GbAccountDetails$ProtoAdapter_GbAccountDetails;->redact(Lcom/squareup/protos/bank_accounts/GbAccountDetails;)Lcom/squareup/protos/bank_accounts/GbAccountDetails;

    move-result-object p1

    return-object p1
.end method
