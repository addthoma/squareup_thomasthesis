.class final Lcom/squareup/protos/privacyvault/service/Protected$ProtoAdapter_Protected;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Protected.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/service/Protected;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Protected"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/privacyvault/service/Protected;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 387
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/privacyvault/service/Protected;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/privacyvault/service/Protected;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 412
    new-instance v0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;-><init>()V

    .line 413
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 414
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 422
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 420
    :cond_0
    sget-object v3, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message(Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    goto :goto_0

    .line 419
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->trunk_token(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    goto :goto_0

    .line 418
    :cond_2
    sget-object v3, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token(Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    goto :goto_0

    .line 417
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data(Lokio/ByteString;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    goto :goto_0

    .line 416
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    goto :goto_0

    .line 426
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 427
    invoke-virtual {v0}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->build()Lcom/squareup/protos/privacyvault/service/Protected;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 385
    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtoAdapter_Protected;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/privacyvault/service/Protected;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/privacyvault/service/Protected;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 402
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/Protected;->string_data:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 403
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/Protected;->binary_data:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 404
    sget-object v0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 405
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 406
    sget-object v0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 407
    invoke-virtual {p2}, Lcom/squareup/protos/privacyvault/service/Protected;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 385
    check-cast p2, Lcom/squareup/protos/privacyvault/service/Protected;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/privacyvault/service/Protected$ProtoAdapter_Protected;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/privacyvault/service/Protected;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/privacyvault/service/Protected;)I
    .locals 4

    .line 392
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/privacyvault/service/Protected;->string_data:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/privacyvault/service/Protected;->binary_data:Lokio/ByteString;

    const/4 v3, 0x2

    .line 393
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/privacyvault/service/Protected;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    const/4 v3, 0x3

    .line 394
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/privacyvault/service/Protected;->trunk_token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 395
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/privacyvault/service/Protected;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    const/4 v3, 0x5

    .line 396
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 397
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 385
    check-cast p1, Lcom/squareup/protos/privacyvault/service/Protected;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtoAdapter_Protected;->encodedSize(Lcom/squareup/protos/privacyvault/service/Protected;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/privacyvault/service/Protected;)Lcom/squareup/protos/privacyvault/service/Protected;
    .locals 2

    .line 432
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected;->newBuilder()Lcom/squareup/protos/privacyvault/service/Protected$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 433
    iput-object v0, p1, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data:Ljava/lang/String;

    .line 434
    iput-object v0, p1, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data:Lokio/ByteString;

    .line 435
    iget-object v0, p1, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    iput-object v0, p1, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    .line 436
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    iput-object v0, p1, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    .line 437
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 438
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->build()Lcom/squareup/protos/privacyvault/service/Protected;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 385
    check-cast p1, Lcom/squareup/protos/privacyvault/service/Protected;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtoAdapter_Protected;->redact(Lcom/squareup/protos/privacyvault/service/Protected;)Lcom/squareup/protos/privacyvault/service/Protected;

    move-result-object p1

    return-object p1
.end method
