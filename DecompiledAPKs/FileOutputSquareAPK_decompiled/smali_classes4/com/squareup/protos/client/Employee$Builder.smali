.class public final Lcom/squareup/protos/client/Employee$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Employee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/Employee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/Employee;",
        "Lcom/squareup/protos/client/Employee$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_token:Ljava/lang/String;

.field public first_name:Ljava/lang/String;

.field public last_name:Ljava/lang/String;

.field public read_only_full_name:Ljava/lang/String;

.field public read_only_photo_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 163
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/Employee;
    .locals 8

    .line 207
    new-instance v7, Lcom/squareup/protos/client/Employee;

    iget-object v1, p0, Lcom/squareup/protos/client/Employee$Builder;->employee_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/Employee$Builder;->read_only_photo_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/Employee$Builder;->read_only_full_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/Employee$Builder;->first_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/Employee$Builder;->last_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/Employee;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v0

    return-object v0
.end method

.method public employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/client/Employee$Builder;->employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public first_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/client/Employee$Builder;->first_name:Ljava/lang/String;

    return-object p0
.end method

.method public last_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/Employee$Builder;->last_name:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_full_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/client/Employee$Builder;->read_only_full_name:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_photo_url(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/Employee$Builder;->read_only_photo_url:Ljava/lang/String;

    return-object p0
.end method
