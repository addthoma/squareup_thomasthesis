.class public final Lcom/squareup/protos/client/kds/KdsStation;
.super Lcom/squareup/wire/Message;
.source "KdsStation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/kds/KdsStation$ProtoAdapter_KdsStation;,
        Lcom/squareup/protos/client/kds/KdsStation$Role;,
        Lcom/squareup/protos/client/kds/KdsStation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/kds/KdsStation;",
        "Lcom/squareup/protos/client/kds/KdsStation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/kds/KdsStation;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ROLE:Lcom/squareup/protos/client/kds/KdsStation$Role;

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final role:Lcom/squareup/protos/client/kds/KdsStation$Role;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.kds.KdsStation$Role#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/kds/KdsStation$ProtoAdapter_KdsStation;

    invoke-direct {v0}, Lcom/squareup/protos/client/kds/KdsStation$ProtoAdapter_KdsStation;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/kds/KdsStation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/protos/client/kds/KdsStation$Role;->PREP:Lcom/squareup/protos/client/kds/KdsStation$Role;

    sput-object v0, Lcom/squareup/protos/client/kds/KdsStation;->DEFAULT_ROLE:Lcom/squareup/protos/client/kds/KdsStation$Role;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/kds/KdsStation$Role;)V
    .locals 1

    .line 60
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/kds/KdsStation;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/kds/KdsStation$Role;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/kds/KdsStation$Role;Lokio/ByteString;)V
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/protos/client/kds/KdsStation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 65
    iput-object p1, p0, Lcom/squareup/protos/client/kds/KdsStation;->name:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/squareup/protos/client/kds/KdsStation;->id:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/squareup/protos/client/kds/KdsStation;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 83
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/kds/KdsStation;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 84
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/kds/KdsStation;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/client/kds/KdsStation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/kds/KdsStation;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/kds/KdsStation;->name:Ljava/lang/String;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/kds/KdsStation;->id:Ljava/lang/String;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    iget-object p1, p1, Lcom/squareup/protos/client/kds/KdsStation;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    .line 88
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 93
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/kds/KdsStation;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/kds/KdsStation$Role;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 99
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/kds/KdsStation$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/protos/client/kds/KdsStation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/kds/KdsStation$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->name:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->id:Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    iput-object v1, v0, Lcom/squareup/protos/client/kds/KdsStation$Builder;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/kds/KdsStation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/kds/KdsStation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/kds/KdsStation;->newBuilder()Lcom/squareup/protos/client/kds/KdsStation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    if-eqz v1, :cond_2

    const-string v1, ", role="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/kds/KdsStation;->role:Lcom/squareup/protos/client/kds/KdsStation$Role;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "KdsStation{"

    .line 110
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
