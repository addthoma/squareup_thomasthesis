.class public final enum Lcom/squareup/protos/client/kds/KdsStation$Role;
.super Ljava/lang/Enum;
.source "KdsStation.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/kds/KdsStation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Role"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/kds/KdsStation$Role$ProtoAdapter_Role;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/kds/KdsStation$Role;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/kds/KdsStation$Role;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/kds/KdsStation$Role;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum EXPEDITE:Lcom/squareup/protos/client/kds/KdsStation$Role;

.field public static final enum IN_FLIGHT:Lcom/squareup/protos/client/kds/KdsStation$Role;

.field public static final enum PREP:Lcom/squareup/protos/client/kds/KdsStation$Role;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 159
    new-instance v0, Lcom/squareup/protos/client/kds/KdsStation$Role;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "PREP"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/kds/KdsStation$Role;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/kds/KdsStation$Role;->PREP:Lcom/squareup/protos/client/kds/KdsStation$Role;

    .line 165
    new-instance v0, Lcom/squareup/protos/client/kds/KdsStation$Role;

    const/4 v3, 0x2

    const-string v4, "EXPEDITE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/kds/KdsStation$Role;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/kds/KdsStation$Role;->EXPEDITE:Lcom/squareup/protos/client/kds/KdsStation$Role;

    .line 171
    new-instance v0, Lcom/squareup/protos/client/kds/KdsStation$Role;

    const/4 v4, 0x3

    const-string v5, "IN_FLIGHT"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/kds/KdsStation$Role;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/kds/KdsStation$Role;->IN_FLIGHT:Lcom/squareup/protos/client/kds/KdsStation$Role;

    new-array v0, v4, [Lcom/squareup/protos/client/kds/KdsStation$Role;

    .line 154
    sget-object v4, Lcom/squareup/protos/client/kds/KdsStation$Role;->PREP:Lcom/squareup/protos/client/kds/KdsStation$Role;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/kds/KdsStation$Role;->EXPEDITE:Lcom/squareup/protos/client/kds/KdsStation$Role;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/kds/KdsStation$Role;->IN_FLIGHT:Lcom/squareup/protos/client/kds/KdsStation$Role;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/kds/KdsStation$Role;->$VALUES:[Lcom/squareup/protos/client/kds/KdsStation$Role;

    .line 173
    new-instance v0, Lcom/squareup/protos/client/kds/KdsStation$Role$ProtoAdapter_Role;

    invoke-direct {v0}, Lcom/squareup/protos/client/kds/KdsStation$Role$ProtoAdapter_Role;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/kds/KdsStation$Role;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 178
    iput p3, p0, Lcom/squareup/protos/client/kds/KdsStation$Role;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/kds/KdsStation$Role;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 188
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/kds/KdsStation$Role;->IN_FLIGHT:Lcom/squareup/protos/client/kds/KdsStation$Role;

    return-object p0

    .line 187
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/kds/KdsStation$Role;->EXPEDITE:Lcom/squareup/protos/client/kds/KdsStation$Role;

    return-object p0

    .line 186
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/kds/KdsStation$Role;->PREP:Lcom/squareup/protos/client/kds/KdsStation$Role;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/kds/KdsStation$Role;
    .locals 1

    .line 154
    const-class v0, Lcom/squareup/protos/client/kds/KdsStation$Role;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/kds/KdsStation$Role;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/kds/KdsStation$Role;
    .locals 1

    .line 154
    sget-object v0, Lcom/squareup/protos/client/kds/KdsStation$Role;->$VALUES:[Lcom/squareup/protos/client/kds/KdsStation$Role;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/kds/KdsStation$Role;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/kds/KdsStation$Role;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 195
    iget v0, p0, Lcom/squareup/protos/client/kds/KdsStation$Role;->value:I

    return v0
.end method
