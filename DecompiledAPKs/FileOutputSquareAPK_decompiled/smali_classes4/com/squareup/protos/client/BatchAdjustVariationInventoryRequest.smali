.class public final Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;
.super Lcom/squareup/wire/Message;
.source "BatchAdjustVariationInventoryRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$ProtoAdapter_BatchAdjustVariationInventoryRequest;,
        Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
        "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IDEMPOTENCY_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final adjustments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.InventoryAdjustment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/InventoryAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field public final counts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.InventoryCount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/InventoryCount;",
            ">;"
        }
    .end annotation
.end field

.field public final idempotency_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$ProtoAdapter_BatchAdjustVariationInventoryRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$ProtoAdapter_BatchAdjustVariationInventoryRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/InventoryCount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/InventoryAdjustment;",
            ">;)V"
        }
    .end annotation

    .line 52
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/InventoryCount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/InventoryAdjustment;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 57
    sget-object v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 58
    iput-object p1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    const-string p1, "counts"

    .line 59
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->counts:Ljava/util/List;

    const-string p1, "adjustments"

    .line 60
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 76
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 77
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    .line 79
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->counts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->counts:Ljava/util/List;

    .line 80
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    .line 81
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 86
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->counts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;
    .locals 2

    .line 65
    new-instance v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;-><init>()V

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->idempotency_token:Ljava/lang/String;

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->counts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->counts:Ljava/util/List;

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->adjustments:Ljava/util/List;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->newBuilder()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", idempotency_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->idempotency_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->counts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", counts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->counts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", adjustments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BatchAdjustVariationInventoryRequest{"

    .line 103
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
