.class final Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$ProtoAdapter_ListTendersAwaitingMerchantTipResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListTendersAwaitingMerchantTipResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ListTendersAwaitingMerchantTipResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 275
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 303
    new-instance v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;-><init>()V

    .line 304
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 305
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 314
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 312
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_with_bill_id:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 311
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->next_request_backoff_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 310
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 309
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->error_title(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 308
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 307
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;

    goto :goto_0

    .line 318
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 319
    invoke-virtual {v0}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 273
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$ProtoAdapter_ListTendersAwaitingMerchantTipResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 292
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 293
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 294
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 295
    sget-object v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 296
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 297
    sget-object v0, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 298
    invoke-virtual {p2}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 273
    check-cast p2, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$ProtoAdapter_ListTendersAwaitingMerchantTipResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)I
    .locals 4

    .line 280
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_message:Ljava/lang/String;

    const/4 v3, 0x2

    .line 281
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->error_title:Ljava/lang/String;

    const/4 v3, 0x3

    .line 282
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 283
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 284
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 285
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->tender_with_bill_id:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 286
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 273
    check-cast p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$ProtoAdapter_ListTendersAwaitingMerchantTipResponse;->encodedSize(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;
    .locals 2

    .line 325
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;->newBuilder()Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;

    move-result-object p1

    .line 326
    iget-object v0, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_awaiting_merchant_tip_list:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 327
    iget-object v0, p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->tender_with_bill_id:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 328
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 329
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 273
    check-cast p1, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse$ProtoAdapter_ListTendersAwaitingMerchantTipResponse;->redact(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;)Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;

    move-result-object p1

    return-object p1
.end method
