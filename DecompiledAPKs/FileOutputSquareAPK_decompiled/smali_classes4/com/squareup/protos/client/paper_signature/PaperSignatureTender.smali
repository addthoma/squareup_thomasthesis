.class public final Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;
.super Lcom/squareup/wire/Message;
.source "PaperSignatureTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$ProtoAdapter_PaperSignatureTender;,
        Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
        "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BILL_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CURRENT_STATE:Lcom/squareup/protos/client/paper_signature/TenderState;

.field public static final DEFAULT_TENDER_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bill_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final current_state:Lcom/squareup/protos/client/paper_signature/TenderState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.paper_signature.TenderState#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final sale_time_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final subtotal_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final tax_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final tender_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final tip_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$ProtoAdapter_PaperSignatureTender;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$ProtoAdapter_PaperSignatureTender;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/paper_signature/TenderState;->TENDERED:Lcom/squareup/protos/client/paper_signature/TenderState;

    sput-object v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->DEFAULT_CURRENT_STATE:Lcom/squareup/protos/client/paper_signature/TenderState;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/paper_signature/TenderState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 10

    .line 83
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/paper_signature/TenderState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/paper_signature/TenderState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    .line 91
    iput-object p2, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    .line 92
    iput-object p3, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    .line 93
    iput-object p4, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    .line 94
    iput-object p5, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    .line 95
    iput-object p6, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    .line 96
    iput-object p7, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    .line 97
    iput-object p8, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 118
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 119
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    .line 124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    .line 128
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 133
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/paper_signature/TenderState;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 144
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tender_id:Ljava/lang/String;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->bill_id:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->subtotal_money:Lcom/squareup/protos/common/Money;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->newBuilder()Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", tender_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tender_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", bill_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->bill_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    if-eqz v1, :cond_2

    const-string v1, ", current_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", subtotal_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->subtotal_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->tax_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_7

    const-string v1, ", sale_time_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PaperSignatureTender{"

    .line 160
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
