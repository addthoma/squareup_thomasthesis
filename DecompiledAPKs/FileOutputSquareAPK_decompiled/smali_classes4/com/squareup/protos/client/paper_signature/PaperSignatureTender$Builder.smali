.class public final Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PaperSignatureTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;",
        "Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id:Ljava/lang/String;

.field public current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

.field public sale_time_at:Lcom/squareup/protos/common/time/DateTime;

.field public subtotal_money:Lcom/squareup/protos/common/Money;

.field public tax_money:Lcom/squareup/protos/common/Money;

.field public tender_id:Ljava/lang/String;

.field public tip_money:Lcom/squareup/protos/common/Money;

.field public total_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 180
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_id(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->bill_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;
    .locals 11

    .line 225
    new-instance v10, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tender_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->bill_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    iget-object v4, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->subtotal_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    iget-object v8, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/paper_signature/TenderState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->build()Lcom/squareup/protos/client/paper_signature/PaperSignatureTender;

    move-result-object v0

    return-object v0
.end method

.method public current_state(Lcom/squareup/protos/client/paper_signature/TenderState;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->current_state:Lcom/squareup/protos/client/paper_signature/TenderState;

    return-object p0
.end method

.method public sale_time_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->sale_time_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public subtotal_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->subtotal_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tender_id(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tender_id:Ljava/lang/String;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/PaperSignatureTender$Builder;->total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
