.class final Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$ProtoAdapter_TenderStatusResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TenderStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TenderStatusResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 221
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 246
    new-instance v0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;-><init>()V

    .line 247
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 248
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x5

    if-eq v3, v4, :cond_1

    const/4 v4, 0x6

    if-eq v3, v4, :cond_0

    .line 256
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 254
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->next_request_backoff_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;

    goto :goto_0

    .line 253
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->tender_status_list:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->error_title(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;

    goto :goto_0

    .line 251
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;

    goto :goto_0

    .line 250
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;

    goto :goto_0

    .line 260
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 261
    invoke-virtual {v0}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 219
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$ProtoAdapter_TenderStatusResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 236
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 237
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->error_message:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 238
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->error_title:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 239
    sget-object v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->tender_status_list:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 240
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 241
    invoke-virtual {p2}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 219
    check-cast p2, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$ProtoAdapter_TenderStatusResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)I
    .locals 4

    .line 226
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->error_message:Ljava/lang/String;

    const/4 v3, 0x2

    .line 227
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->error_title:Ljava/lang/String;

    const/4 v3, 0x3

    .line 228
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 229
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->tender_status_list:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->next_request_backoff_seconds:Ljava/lang/Integer;

    const/4 v3, 0x6

    .line 230
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 219
    check-cast p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$ProtoAdapter_TenderStatusResponse;->encodedSize(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;
    .locals 2

    .line 266
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;->newBuilder()Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;

    move-result-object p1

    .line 267
    iget-object v0, p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->tender_status_list:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 268
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 269
    invoke-virtual {p1}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 219
    check-cast p1, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/paper_signature/TenderStatusResponse$ProtoAdapter_TenderStatusResponse;->redact(Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;)Lcom/squareup/protos/client/paper_signature/TenderStatusResponse;

    move-result-object p1

    return-object p1
.end method
