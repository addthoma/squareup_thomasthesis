.class public final Lcom/squareup/protos/client/deposits/InstantDepositDetails;
.super Lcom/squareup/wire/Message;
.source "InstantDepositDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/deposits/InstantDepositDetails$ProtoAdapter_InstantDepositDetails;,
        Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/deposits/InstantDepositDetails;",
        "Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/deposits/InstantDepositDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ELIGIBILITY_BLOCKER:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final DEFAULT_SHOW_PRICE_CHANGE_MODAL:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.instantdeposits.EligibilityBlocker#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final eligibility_status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final max_deposit_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final show_price_change_modal:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$ProtoAdapter_InstantDepositDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/InstantDepositDetails$ProtoAdapter_InstantDepositDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    sput-object v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->DEFAULT_ELIGIBILITY_BLOCKER:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->DEFAULT_SHOW_PRICE_CHANGE_MODAL:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;)V
    .locals 6

    .line 71
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/deposits/InstantDepositDetails;-><init>(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/common/Money;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 76
    sget-object v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 77
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 78
    iput-object p2, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    .line 79
    iput-object p3, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 80
    iput-object p4, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->show_price_change_modal:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 97
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 98
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->show_price_change_modal:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->show_price_change_modal:Ljava/lang/Boolean;

    .line 103
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 108
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->show_price_change_modal:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 115
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;
    .locals 2

    .line 85
    new-instance v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;-><init>()V

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->eligibility_status:Lcom/squareup/protos/client/Status;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->show_price_change_modal:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->show_price_change_modal:Ljava/lang/Boolean;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->newBuilder()Lcom/squareup/protos/client/deposits/InstantDepositDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    if-eqz v1, :cond_0

    const-string v1, ", eligibility_blocker="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_blocker:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_1

    const-string v1, ", eligibility_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->eligibility_status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", max_deposit_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->max_deposit_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->show_price_change_modal:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", show_price_change_modal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->show_price_change_modal:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InstantDepositDetails{"

    .line 127
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
