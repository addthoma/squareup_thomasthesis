.class public final Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ArchiveInvoiceRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;",
        "Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id_pair:Lcom/squareup/protos/client/IdPair;

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;
    .locals 4

    .line 108
    new-instance v0, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->version:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    move-result-object v0

    return-object v0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public version(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->version:Ljava/lang/String;

    return-object p0
.end method
