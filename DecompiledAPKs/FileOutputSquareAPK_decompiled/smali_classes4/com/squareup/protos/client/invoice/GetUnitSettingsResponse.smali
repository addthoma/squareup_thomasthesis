.class public final Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;
.super Lcom/squareup/wire/Message;
.source "GetUnitSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$ProtoAdapter_GetUnitSettingsResponse;,
        Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
        "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final invoice_defaults:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceDefaults#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.UnitSettings#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$ProtoAdapter_GetUnitSettingsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$ProtoAdapter_GetUnitSettingsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/UnitSettings;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Lcom/squareup/protos/client/invoice/UnitSettings;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;)V"
        }
    .end annotation

    .line 53
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/UnitSettings;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/UnitSettings;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Lcom/squareup/protos/client/invoice/UnitSettings;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 58
    sget-object v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 59
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 60
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    const-string p1, "invoice_defaults"

    .line 61
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 77
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 78
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 80
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    .line 81
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    .line 82
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 87
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitSettings;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;
    .locals 2

    .line 66
    new-instance v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->invoice_defaults:Ljava/util/List;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->newBuilder()Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    if-eqz v1, :cond_1

    const-string v1, ", unit_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", invoice_defaults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetUnitSettingsResponse{"

    .line 104
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
