.class public final Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;
.super Lcom/squareup/wire/Message;
.source "PayInvoiceOutOfBandRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$ProtoAdapter_PayInvoiceOutOfBandRequest;,
        Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;",
        "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SEND_EMAIL_TO_RECIPIENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_TENDER_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field public static final DEFAULT_VERSION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final payment_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final send_email_to_recipients:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final tender_note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTenderDetails$TenderType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$ProtoAdapter_PayInvoiceOutOfBandRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$ProtoAdapter_PayInvoiceOutOfBandRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CASH:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    sput-object v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    const/4 v0, 0x1

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->DEFAULT_SEND_EMAIL_TO_RECIPIENTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 8

    .line 76
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 83
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 84
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 85
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_note:Ljava/lang/String;

    .line 86
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 87
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->version:Ljava/lang/String;

    .line 88
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->payment_amount:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 107
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 108
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_note:Ljava/lang/String;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->version:Ljava/lang/String;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->payment_amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->payment_amount:Lcom/squareup/protos/common/Money;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_note:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->version:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->payment_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 129
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;
    .locals 2

    .line 93
    new-instance v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;-><init>()V

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->tender_note:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->version:Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->payment_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->newBuilder()Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    if-eqz v1, :cond_1

    const-string v1, ", tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_note:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", tender_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->tender_note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", send_email_to_recipients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->version:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->payment_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", payment_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;->payment_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PayInvoiceOutOfBandRequest{"

    .line 143
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
