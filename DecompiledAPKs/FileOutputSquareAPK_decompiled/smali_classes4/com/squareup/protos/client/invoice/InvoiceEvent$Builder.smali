.class public final Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
        "Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public detailed_description:Ljava/lang/String;

.field public event_call_to_action:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

.field public occurred_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/InvoiceEvent;
    .locals 7

    .line 148
    new-instance v6, Lcom/squareup/protos/client/invoice/InvoiceEvent;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->description:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->detailed_description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->event_call_to_action:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/invoice/InvoiceEvent;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceEvent;

    move-result-object v0

    return-object v0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public detailed_description(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->detailed_description:Ljava/lang/String;

    return-object p0
.end method

.method public event_call_to_action(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->event_call_to_action:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    return-object p0
.end method

.method public occurred_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceEvent$Builder;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
