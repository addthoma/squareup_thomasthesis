.class public final Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PaymentRequestDefaults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;",
        "Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public relative_due_on:Ljava/lang/Integer;

.field public tipping_enabled:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;
    .locals 5

    .line 151
    new-instance v0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->relative_due_on:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequestDefaults;

    move-result-object v0

    return-object v0
.end method

.method public payment_method(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0
.end method

.method public relative_due_on(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->relative_due_on:Ljava/lang/Integer;

    return-object p0
.end method

.method public tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/PaymentRequestDefaults$Builder;->tipping_enabled:Ljava/lang/Boolean;

    return-object p0
.end method
