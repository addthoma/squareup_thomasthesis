.class public final enum Lcom/squareup/protos/client/invoice/MetricType;
.super Ljava/lang/Enum;
.source "MetricType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/MetricType$ProtoAdapter_MetricType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/invoice/MetricType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum ACCEPTED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum ACTIVE_RECURRING_SERIES_COUNT:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/MetricType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AVG_DAYS_LATE:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum AVG_DAYS_TO_PAY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum DRAFT_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum INVOICED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum METRIC_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum OUTSTANDING_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum OVERDUE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum OVERDUE_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum PAID_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum PENDING_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

.field public static final enum UNPAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v1, 0x0

    const-string v2, "METRIC_TYPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->METRIC_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/invoice/MetricType;

    .line 13
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v2, 0x1

    const-string v3, "UNPAID_MONEY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->UNPAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 15
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v3, 0x2

    const-string v4, "PAID_MONEY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 17
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v4, 0x3

    const-string v5, "DRAFT_MONEY"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 19
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v5, 0x4

    const-string v6, "OUTSTANDING_MONEY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 21
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v6, 0x5

    const-string v7, "ACTIVE_RECURRING_SERIES_COUNT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->ACTIVE_RECURRING_SERIES_COUNT:Lcom/squareup/protos/client/invoice/MetricType;

    .line 23
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v7, 0x6

    const-string v8, "OUTSTANDING_RECURRING_MONEY"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 25
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v8, 0x7

    const-string v9, "PAID_RECURRING_MONEY"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->PAID_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 32
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v9, 0x8

    const-string v10, "AVG_DAYS_LATE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->AVG_DAYS_LATE:Lcom/squareup/protos/client/invoice/MetricType;

    .line 38
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v10, 0x9

    const-string v11, "AVG_DAYS_TO_PAY"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->AVG_DAYS_TO_PAY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 40
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v11, 0xa

    const-string v12, "ACCEPTED_ESTIMATE_MONEY"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->ACCEPTED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 42
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v12, 0xb

    const-string v13, "PENDING_ESTIMATE_MONEY"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->PENDING_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 44
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v13, 0xc

    const-string v14, "DRAFT_ESTIMATE_MONEY"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 46
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v14, 0xd

    const-string v15, "INVOICED_ESTIMATE_MONEY"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->INVOICED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 48
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v15, 0xe

    const-string v14, "OVERDUE_MONEY"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->OVERDUE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    .line 50
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType;

    const-string v14, "OVERDUE_RECURRING_MONEY"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/client/invoice/MetricType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->OVERDUE_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/MetricType;

    .line 10
    sget-object v13, Lcom/squareup/protos/client/invoice/MetricType;->METRIC_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->UNPAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->ACTIVE_RECURRING_SERIES_COUNT:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->PAID_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->AVG_DAYS_LATE:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->AVG_DAYS_TO_PAY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->ACCEPTED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->PENDING_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->INVOICED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->OVERDUE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/MetricType;->OVERDUE_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->$VALUES:[Lcom/squareup/protos/client/invoice/MetricType;

    .line 52
    new-instance v0, Lcom/squareup/protos/client/invoice/MetricType$ProtoAdapter_MetricType;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/MetricType$ProtoAdapter_MetricType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/MetricType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/squareup/protos/client/invoice/MetricType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/invoice/MetricType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 80
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->OVERDUE_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 79
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->OVERDUE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 78
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->INVOICED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 77
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 76
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->PENDING_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 75
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->ACCEPTED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 74
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->AVG_DAYS_TO_PAY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 73
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->AVG_DAYS_LATE:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 72
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->PAID_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 71
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_RECURRING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 70
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->ACTIVE_RECURRING_SERIES_COUNT:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 69
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 68
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 67
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 66
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->UNPAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    .line 65
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/client/invoice/MetricType;->METRIC_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/MetricType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/invoice/MetricType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/invoice/MetricType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/invoice/MetricType;->$VALUES:[Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/invoice/MetricType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/invoice/MetricType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 87
    iget v0, p0, Lcom/squareup/protos/client/invoice/MetricType;->value:I

    return v0
.end method
