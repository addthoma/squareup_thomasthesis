.class public final Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceReminderInstance.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public custom_message:Ljava/lang/String;

.field public relative_days:Ljava/lang/Integer;

.field public reminder_config_token:Ljava/lang/String;

.field public sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

.field public state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 179
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
    .locals 9

    .line 232
    new-instance v8, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->reminder_config_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->custom_message:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->relative_days:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 166
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v0

    return-object v0
.end method

.method public custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->custom_message:Ljava/lang/String;

    return-object p0
.end method

.method public relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->relative_days:Ljava/lang/Integer;

    return-object p0
.end method

.method public reminder_config_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->reminder_config_token:Ljava/lang/String;

    return-object p0
.end method

.method public sent_on(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
