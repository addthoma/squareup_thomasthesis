.class public final Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnitMetadataDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public automatic_reminder_count_limit:Ljava/lang/Integer;

.field public custom_message_char_limit:Ljava/lang/Integer;

.field public default_reminder:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 525
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 526
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->default_reminder:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public automatic_reminder_count_limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;
    .locals 0

    .line 533
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->automatic_reminder_count_limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 5

    .line 556
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->automatic_reminder_count_limit:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->custom_message_char_limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->default_reminder:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 518
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    return-object v0
.end method

.method public custom_message_char_limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;
    .locals 0

    .line 541
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->custom_message_char_limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public default_reminder(Ljava/util/List;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;"
        }
    .end annotation

    .line 549
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 550
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings$Builder;->default_reminder:Ljava/util/List;

    return-object p0
.end method
