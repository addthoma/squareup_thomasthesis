.class final Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$ProtoAdapter_InvoiceDisplayDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InvoiceDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InvoiceDisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 949
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1010
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;-><init>()V

    .line 1011
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1012
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1045
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1043
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->is_archived(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto :goto_0

    .line 1042
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->first_sent_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto :goto_0

    .line 1041
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payments:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1040
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payment_request_state:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1039
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->automatic_reminder_instance:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1038
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/invoice/ShippingAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/ShippingAddress;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->shipping_address(Lcom/squareup/protos/client/invoice/ShippingAddress;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto :goto_0

    .line 1037
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice_timeline(Lcom/squareup/protos/client/invoice/InvoiceTimeline;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto :goto_0

    .line 1036
    :pswitch_7
    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refund:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1035
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_edited_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto :goto_0

    .line 1034
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_viewed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1033
    :pswitch_a
    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->event:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1032
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/client/invoice/SeriesDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/SeriesDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->series_details(Lcom/squareup/protos/client/invoice/SeriesDetails;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1031
    :pswitch_c
    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_v1_refund:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1030
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1024
    :pswitch_e
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->display_state(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 1026
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1021
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sent_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1020
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refunded_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1019
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->cancelled_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1018
    :pswitch_12
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->paid_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1017
    :pswitch_13
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1016
    :pswitch_14
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1015
    :pswitch_15
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sort_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1014
    :pswitch_16
    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    goto/16 :goto_0

    .line 1049
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1050
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 947
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$ProtoAdapter_InvoiceDisplayDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 982
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 983
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 984
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 985
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 986
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 987
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 988
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 989
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 990
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 991
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 992
    sget-object v0, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 993
    sget-object v0, Lcom/squareup/protos/client/invoice/SeriesDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 994
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 995
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 996
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 997
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 998
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 999
    sget-object v0, Lcom/squareup/protos/client/invoice/ShippingAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1000
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1001
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1002
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1003
    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1004
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1005
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 947
    check-cast p2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$ProtoAdapter_InvoiceDisplayDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)I
    .locals 4

    .line 954
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x2

    .line 955
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x3

    .line 956
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x4

    .line 957
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x5

    .line 958
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x6

    .line 959
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x7

    .line 960
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0x8

    .line 961
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const/16 v3, 0x9

    .line 962
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/16 v3, 0xa

    .line 963
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 964
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_v1_refund:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/SeriesDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    const/16 v3, 0xc

    .line 965
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 966
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->event:Ljava/util/List;

    const/16 v3, 0xd

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0xe

    .line 967
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0xf

    .line 968
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 969
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->refund:Ljava/util/List;

    const/16 v3, 0x10

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    const/16 v3, 0x11

    .line 970
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/ShippingAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    const/16 v3, 0x12

    .line 971
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 972
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->automatic_reminder_instance:Ljava/util/List;

    const/16 v3, 0x13

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 973
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    const/16 v3, 0x14

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 974
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payments:Ljava/util/List;

    const/16 v3, 0x15

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/16 v3, 0x16

    .line 975
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->is_archived:Ljava/lang/Boolean;

    const/16 v3, 0x17

    .line 976
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 977
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 947
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$ProtoAdapter_InvoiceDisplayDetails;->encodedSize(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
    .locals 2

    .line 1055
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;

    move-result-object p1

    .line 1056
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 1057
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 1058
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1059
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1060
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1061
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1062
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1063
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1064
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1065
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_v1_refund:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1066
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/client/invoice/SeriesDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/SeriesDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    .line 1067
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->event:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1068
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1069
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1070
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refund:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1071
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    .line 1072
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/client/invoice/ShippingAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/ShippingAddress;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    .line 1073
    :cond_d
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->automatic_reminder_instance:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1074
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payment_request_state:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1075
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payments:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1076
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 1077
    :cond_e
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1078
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 947
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$ProtoAdapter_InvoiceDisplayDetails;->redact(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    return-object p1
.end method
