.class public final Lcom/squareup/protos/client/invoice/Invoice$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Invoice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/Invoice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public additional_recipient_email:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public attachment:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public automatic_reminder_config:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation
.end field

.field public automatic_reminders_enabled:Ljava/lang/Boolean;

.field public buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

.field public buyer_entered_instrument_enabled:Ljava/lang/Boolean;

.field public buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

.field public cart:Lcom/squareup/protos/client/bills/Cart;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public description:Ljava/lang/String;

.field public due_on:Lcom/squareup/protos/common/time/YearMonthDay;

.field public id_pair:Lcom/squareup/protos/client/IdPair;

.field public instrument_token:Ljava/lang/String;

.field public invoice_name:Ljava/lang/String;

.field public merchant_invoice_number:Ljava/lang/String;

.field public payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

.field public payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public payment_request:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation
.end field

.field public scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

.field public scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

.field public tipping_enabled:Ljava/lang/Boolean;

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 465
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 466
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->additional_recipient_email:Ljava/util/List;

    .line 467
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->attachment:Ljava/util/List;

    .line 468
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    .line 469
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public additional_recipient_email(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;"
        }
    .end annotation

    .line 553
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 554
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->additional_recipient_email:Ljava/util/List;

    return-object p0
.end method

.method public attachment(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;"
        }
    .end annotation

    .line 588
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 589
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->attachment:Ljava/util/List;

    return-object p0
.end method

.method public automatic_reminder_config(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;"
        }
    .end annotation

    .line 651
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 652
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    return-object p0
.end method

.method public automatic_reminders_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 642
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminders_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/Invoice;
    .locals 2

    .line 667
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/client/invoice/Invoice;-><init>(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 420
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    return-object v0
.end method

.method public buyer_entered_automatic_charge_enroll_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 634
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public buyer_entered_instrument_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 607
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public buyer_entered_shipping_address_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 625
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 527
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 562
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 484
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public due_on(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 519
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 476
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 599
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->instrument_token:Ljava/lang/String;

    return-object p0
.end method

.method public invoice_name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->invoice_name:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_invoice_number(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 500
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->merchant_invoice_number:Ljava/lang/String;

    return-object p0
.end method

.method public payer(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 535
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    return-object p0
.end method

.method public payment_method(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 580
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    return-object p0
.end method

.method public payment_request(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/Invoice$Builder;"
        }
    .end annotation

    .line 660
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 661
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    return-object p0
.end method

.method public scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 492
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public scheduled_at_time(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 616
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 545
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->tipping_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public version(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 571
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->version:Ljava/lang/String;

    return-object p0
.end method
