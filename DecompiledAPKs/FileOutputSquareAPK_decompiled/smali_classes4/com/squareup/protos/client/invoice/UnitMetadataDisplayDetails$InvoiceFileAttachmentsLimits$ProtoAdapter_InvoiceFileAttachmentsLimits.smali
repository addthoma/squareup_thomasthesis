.class final Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$ProtoAdapter_InvoiceFileAttachmentsLimits;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UnitMetadataDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InvoiceFileAttachmentsLimits"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 370
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 392
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;-><init>()V

    .line 393
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 394
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 407
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 400
    :cond_0
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->allowed_mime_type:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 402
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 397
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->max_total_size_bytes(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;

    goto :goto_0

    .line 396
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->max_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;

    goto :goto_0

    .line 411
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 412
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 368
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$ProtoAdapter_InvoiceFileAttachmentsLimits;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 384
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->max_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 385
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->max_total_size_bytes:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 386
    sget-object v0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->allowed_mime_type:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 387
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 368
    check-cast p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$ProtoAdapter_InvoiceFileAttachmentsLimits;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)I
    .locals 4

    .line 375
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->max_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->max_total_size_bytes:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 376
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 377
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->allowed_mime_type:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 378
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 368
    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$ProtoAdapter_InvoiceFileAttachmentsLimits;->encodedSize(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;
    .locals 0

    .line 417
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->newBuilder()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;

    move-result-object p1

    .line 418
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 419
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 368
    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$ProtoAdapter_InvoiceFileAttachmentsLimits;->redact(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    move-result-object p1

    return-object p1
.end method
