.class public final Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendInvoiceReminderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;",
        "Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id_pair:Lcom/squareup/protos/client/IdPair;

.field public reminder_message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;->reminder_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;->build()Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;

    move-result-object v0

    return-object v0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public reminder_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest$Builder;->reminder_message:Ljava/lang/String;

    return-object p0
.end method
