.class public final Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnitAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/UnitAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/UnitAnalytics;",
        "Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public recent_paid_money:Lcom/squareup/protos/common/Money;

.field public total_draft_money:Lcom/squareup/protos/common/Money;

.field public total_unpaid_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 117
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/UnitAnalytics;
    .locals 5

    .line 146
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitAnalytics;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->total_unpaid_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->recent_paid_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->total_draft_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/UnitAnalytics;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->build()Lcom/squareup/protos/client/invoice/UnitAnalytics;

    move-result-object v0

    return-object v0
.end method

.method public recent_paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->recent_paid_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_draft_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->total_draft_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_unpaid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitAnalytics$Builder;->total_unpaid_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
