.class public final Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SaveDraftInvoiceResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;",
        "Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;
    .locals 4

    .line 107
    new-instance v0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;->build()Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;

    move-result-object v0

    return-object v0
.end method

.method public invoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
