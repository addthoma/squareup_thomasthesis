.class public final Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;
.super Lcom/squareup/wire/Message;
.source "SetDepositScheduleResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$ProtoAdapter_SetDepositScheduleResponse;,
        Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.WeeklyDepositSchedule#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.WeeklyDepositSchedule#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$ProtoAdapter_SetDepositScheduleResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$ProtoAdapter_SetDepositScheduleResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)V
    .locals 1

    .line 45
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;-><init>(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lokio/ByteString;)V
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 51
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 52
    iput-object p2, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 67
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 68
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 70
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 71
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 76
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 81
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;
    .locals 2

    .line 57
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;-><init>()V

    .line 58
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 59
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 60
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->newBuilder()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v1, :cond_0

    const-string v1, ", current_weekly_deposit_schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    if-eqz v1, :cond_1

    const-string v1, ", next_weekly_deposit_schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SetDepositScheduleResponse{"

    .line 91
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
