.class public final Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetDepositScheduleResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

.field public next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;
    .locals 4

    .line 123
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object v2, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;-><init>(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;->build()Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;

    move-result-object v0

    return-object v0
.end method

.method public current_weekly_deposit_schedule(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    return-object p0
.end method

.method public next_weekly_deposit_schedule(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    return-object p0
.end method
