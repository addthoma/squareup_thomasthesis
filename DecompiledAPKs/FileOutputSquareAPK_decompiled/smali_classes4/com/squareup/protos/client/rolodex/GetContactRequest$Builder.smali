.class public final Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetContactRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GetContactRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/GetContactRequest;",
        "Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

.field public contact_token:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/GetContactRequest;
    .locals 5

    .line 137
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetContactRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->contact_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/GetContactRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/ContactOptions;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/GetContactRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_options(Lcom/squareup/protos/client/rolodex/ContactOptions;)Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    return-object p0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetContactRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method
