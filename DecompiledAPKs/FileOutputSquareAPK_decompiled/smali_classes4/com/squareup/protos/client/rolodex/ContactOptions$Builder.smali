.class public final Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ContactOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ContactOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ContactOptions;",
        "Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public include_application_fields:Ljava/lang/Boolean;

.field public include_attachments:Ljava/lang/Boolean;

.field public include_attributes:Ljava/lang/Boolean;

.field public include_buyer_summary:Ljava/lang/Boolean;

.field public include_email_subscriptions:Ljava/lang/Boolean;

.field public include_frequent_items:Ljava/lang/Boolean;

.field public include_groups:Ljava/lang/Boolean;

.field public include_instruments_on_file:Ljava/lang/Boolean;

.field public include_loyalty_app_data:Ljava/lang/Boolean;

.field public include_loyalty_status:Ljava/lang/Boolean;

.field public include_notes:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 274
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ContactOptions;
    .locals 14

    .line 369
    new-instance v13, Lcom/squareup/protos/client/rolodex/ContactOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_instruments_on_file:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_loyalty_status:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_notes:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_buyer_summary:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_groups:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attributes:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_email_subscriptions:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attachments:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_loyalty_app_data:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_frequent_items:Ljava/lang/Boolean;

    iget-object v11, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_application_fields:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/rolodex/ContactOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 251
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactOptions;

    move-result-object v0

    return-object v0
.end method

.method public include_application_fields(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_application_fields:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_attachments(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 339
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attachments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_attributes(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attributes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_buyer_summary(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 306
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_buyer_summary:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_email_subscriptions(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_email_subscriptions:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_frequent_items(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 355
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_frequent_items:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_groups(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_groups:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_instruments_on_file(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_instruments_on_file:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_loyalty_app_data(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 347
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_loyalty_app_data:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_loyalty_status(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_loyalty_status:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_notes(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_notes:Ljava/lang/Boolean;

    return-object p0
.end method
