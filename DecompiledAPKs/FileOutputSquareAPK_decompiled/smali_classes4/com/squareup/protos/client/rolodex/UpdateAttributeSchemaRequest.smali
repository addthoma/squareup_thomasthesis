.class public final Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;
.super Lcom/squareup/wire/Message;
.source "UpdateAttributeSchemaRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$ProtoAdapter_UpdateAttributeSchemaRequest;,
        Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SCHEMA_VERSION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final attribute_definitions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AttributeSchema$AttributeDefinition#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final attribute_keys_in_order:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final schema_version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$ProtoAdapter_UpdateAttributeSchemaRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$ProtoAdapter_UpdateAttributeSchemaRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 56
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 62
    sget-object v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 63
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->schema_version:Ljava/lang/String;

    const-string p1, "attribute_definitions"

    .line 64
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_definitions:Ljava/util/List;

    const-string p1, "attribute_keys_in_order"

    .line 65
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_keys_in_order:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 81
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 82
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->schema_version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->schema_version:Ljava/lang/String;

    .line 84
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_definitions:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_definitions:Ljava/util/List;

    .line 85
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_keys_in_order:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_keys_in_order:Ljava/util/List;

    .line 86
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 91
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->schema_version:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_definitions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_keys_in_order:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;
    .locals 2

    .line 70
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->schema_version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->schema_version:Ljava/lang/String;

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_definitions:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_definitions:Ljava/util/List;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_keys_in_order:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->attribute_keys_in_order:Ljava/util/List;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->schema_version:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", schema_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->schema_version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_definitions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", attribute_definitions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_definitions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_keys_in_order:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", attribute_keys_in_order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaRequest;->attribute_keys_in_order:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UpdateAttributeSchemaRequest{"

    .line 108
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
