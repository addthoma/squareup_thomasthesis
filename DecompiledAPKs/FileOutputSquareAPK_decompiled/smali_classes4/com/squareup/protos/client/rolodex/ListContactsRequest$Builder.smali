.class public final Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListContactsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListContactsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListContactsRequest;",
        "Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

.field public context:Lcom/squareup/protos/client/rolodex/QueryContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListContactsRequest;
    .locals 4

    .line 118
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListContactsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/ListContactsRequest;-><init>(Lcom/squareup/protos/client/rolodex/QueryContext;Lcom/squareup/protos/client/rolodex/ContactOptions;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListContactsRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_options(Lcom/squareup/protos/client/rolodex/ContactOptions;)Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->contact_options:Lcom/squareup/protos/client/rolodex/ContactOptions;

    return-object p0
.end method

.method public context(Lcom/squareup/protos/client/rolodex/QueryContext;)Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListContactsRequest$Builder;->context:Lcom/squareup/protos/client/rolodex/QueryContext;

    return-object p0
.end method
