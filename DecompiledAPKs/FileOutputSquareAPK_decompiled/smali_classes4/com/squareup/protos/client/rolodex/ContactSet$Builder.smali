.class public final Lcom/squareup/protos/client/rolodex/ContactSet$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ContactSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ContactSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ContactSet;",
        "Lcom/squareup/protos/client/rolodex/ContactSet$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_count:Ljava/lang/Integer;

.field public contact_tokens_excluded:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public contact_tokens_included:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public group_token:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/rolodex/ContactSetType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 156
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 157
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_included:Ljava/util/List;

    .line 158
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_excluded:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ContactSet;
    .locals 8

    .line 202
    new-instance v7, Lcom/squareup/protos/client/rolodex/ContactSet;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->type:Lcom/squareup/protos/client/rolodex/ContactSetType;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_included:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->group_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_excluded:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_count:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/rolodex/ContactSet;-><init>(Lcom/squareup/protos/client/rolodex/ContactSetType;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 145
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    return-object v0
.end method

.method public contact_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public contact_tokens_excluded(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/ContactSet$Builder;"
        }
    .end annotation

    .line 187
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_excluded:Ljava/util/List;

    return-object p0
.end method

.method public contact_tokens_included(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/ContactSet$Builder;"
        }
    .end annotation

    .line 170
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->contact_tokens_included:Ljava/util/List;

    return-object p0
.end method

.method public group_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->group_token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/rolodex/ContactSetType;)Lcom/squareup/protos/client/rolodex/ContactSet$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactSet$Builder;->type:Lcom/squareup/protos/client/rolodex/ContactSetType;

    return-object p0
.end method
