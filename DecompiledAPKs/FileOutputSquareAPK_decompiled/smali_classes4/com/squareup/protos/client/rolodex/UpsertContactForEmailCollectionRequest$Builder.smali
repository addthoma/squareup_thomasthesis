.class public final Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertContactForEmailCollectionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;",
        "Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact:Lcom/squareup/protos/client/rolodex/Contact;

.field public request_token:Ljava/lang/String;

.field public tender_server_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;
    .locals 5

    .line 152
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->request_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->tender_server_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method public request_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->request_token:Ljava/lang/String;

    return-object p0
.end method

.method public tender_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertContactForEmailCollectionRequest$Builder;->tender_server_id:Ljava/lang/String;

    return-object p0
.end method
