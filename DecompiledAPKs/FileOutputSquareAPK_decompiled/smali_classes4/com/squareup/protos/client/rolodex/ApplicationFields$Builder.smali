.class public final Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ApplicationFields.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ApplicationFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ApplicationFields;",
        "Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public appointments_email_notification_setting:Ljava/lang/Boolean;

.field public appointments_phone_notification_setting:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public appointments_email_notification_setting(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;->appointments_email_notification_setting:Ljava/lang/Boolean;

    return-object p0
.end method

.method public appointments_phone_notification_setting(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;->appointments_phone_notification_setting:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/ApplicationFields;
    .locals 4

    .line 129
    new-instance v0, Lcom/squareup/protos/client/rolodex/ApplicationFields;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;->appointments_email_notification_setting:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;->appointments_phone_notification_setting:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/ApplicationFields;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ApplicationFields$Builder;->build()Lcom/squareup/protos/client/rolodex/ApplicationFields;

    move-result-object v0

    return-object v0
.end method
