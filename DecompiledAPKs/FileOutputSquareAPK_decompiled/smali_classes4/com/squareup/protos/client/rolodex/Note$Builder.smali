.class public final Lcom/squareup/protos/client/rolodex/Note$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Note.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Note;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Note;",
        "Lcom/squareup/protos/client/rolodex/Note$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public body:Ljava/lang/String;

.field public contact_token:Ljava/lang/String;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public note_token:Ljava/lang/String;

.field public occurred_at:Lcom/squareup/protos/common/time/DateTime;

.field public reminder:Lcom/squareup/protos/client/rolodex/Reminder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 167
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public body(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Note$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->body:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/Note;
    .locals 9

    .line 217
    new-instance v8, Lcom/squareup/protos/client/rolodex/Note;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->note_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->body:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->contact_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/rolodex/Note;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Reminder;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 154
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Note$Builder;->build()Lcom/squareup/protos/client/rolodex/Note;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Note$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/rolodex/Note$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public note_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Note$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->note_token:Ljava/lang/String;

    return-object p0
.end method

.method public occurred_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/Note$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public reminder(Lcom/squareup/protos/client/rolodex/Reminder;)Lcom/squareup/protos/client/rolodex/Note$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Note$Builder;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    return-object p0
.end method
