.class public final Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;
.super Lcom/squareup/wire/Message;
.source "DeleteAttachmentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$ProtoAdapter_DeleteAttachmentsResponse;,
        Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
        "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FAILED_ATTACHMENTS:Ljava/lang/Integer;

.field public static final DEFAULT_SUCCEEDED_ATTACHMENTS:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final attachment_responses:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AttachmentResponse#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;"
        }
    .end annotation
.end field

.field public final failed_attachments:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final succeeded_attachments:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$ProtoAdapter_DeleteAttachmentsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$ProtoAdapter_DeleteAttachmentsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->DEFAULT_SUCCEEDED_ATTACHMENTS:Ljava/lang/Integer;

    .line 29
    sput-object v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->DEFAULT_FAILED_ATTACHMENTS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Lcom/squareup/protos/client/Status;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;",
            "Lcom/squareup/protos/client/Status;",
            ")V"
        }
    .end annotation

    .line 58
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/AttachmentResponse;",
            ">;",
            "Lcom/squareup/protos/client/Status;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 63
    sget-object v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    const-string p1, "attachment_responses"

    .line 66
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->attachment_responses:Ljava/util/List;

    .line 67
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 84
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 85
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->attachment_responses:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->attachment_responses:Ljava/util/List;

    .line 89
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 90
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 95
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->attachment_responses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 102
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->succeeded_attachments:Ljava/lang/Integer;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->failed_attachments:Ljava/lang/Integer;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->attachment_responses:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->attachment_responses:Ljava/util/List;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->newBuilder()Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", succeeded_attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->succeeded_attachments:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", failed_attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->failed_attachments:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->attachment_responses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", attachment_responses="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->attachment_responses:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_3

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/DeleteAttachmentsResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeleteAttachmentsResponse{"

    .line 114
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
