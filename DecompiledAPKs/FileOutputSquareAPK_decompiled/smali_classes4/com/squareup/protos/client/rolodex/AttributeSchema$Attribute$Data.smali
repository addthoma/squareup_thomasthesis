.class public final Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;
.super Lcom/squareup/wire/Message;
.source "AttributeSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$ProtoAdapter_Data;,
        Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BOOLEAN_:Ljava/lang/Boolean;

.field public static final DEFAULT_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_NUMBER:Ljava/lang/Float;

.field public static final DEFAULT_NUMBER_TEXT:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE:Ljava/lang/String; = ""

.field public static final DEFAULT_TEXT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address:Lcom/squareup/protos/common/location/GlobalAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GlobalAddress#ADAPTER"
        redacted = true
        tag = 0x8
    .end annotation
.end field

.field public final boolean_:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final date:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        redacted = true
        tag = 0x9
    .end annotation
.end field

.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x7
    .end annotation
.end field

.field public final enum_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final number:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final number_text:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final phone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 799
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$ProtoAdapter_Data;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$ProtoAdapter_Data;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 803
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->DEFAULT_NUMBER:Ljava/lang/Float;

    const/4 v0, 0x0

    .line 805
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->DEFAULT_BOOLEAN_:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Float;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Float;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            "Lcom/squareup/protos/common/time/DateTime;",
            ")V"
        }
    .end annotation

    .line 882
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;-><init>(Ljava/lang/Float;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Float;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Float;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 888
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 889
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    .line 890
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    .line 891
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    .line 892
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    const-string p1, "enum_values"

    .line 893
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    .line 894
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    .line 895
    iput-object p7, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    .line 896
    iput-object p8, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 897
    iput-object p9, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 919
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 920
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    .line 921
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    .line 922
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    .line 923
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    .line 924
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    .line 925
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    .line 926
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    .line 927
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    .line 928
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 929
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    .line 930
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 935
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 937
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 938
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 939
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 940
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 941
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 942
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 943
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 944
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 945
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GlobalAddress;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 946
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 947
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 2

    .line 902
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    .line 903
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number:Ljava/lang/Float;

    .line 904
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->boolean_:Ljava/lang/Boolean;

    .line 905
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->text:Ljava/lang/String;

    .line 906
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number_text:Ljava/lang/String;

    .line 907
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->enum_values:Ljava/util/List;

    .line 908
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->phone:Ljava/lang/String;

    .line 909
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->email:Ljava/lang/String;

    .line 910
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 911
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    .line 912
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 798
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 954
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 955
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    if-eqz v1, :cond_0

    const-string v1, ", number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 956
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", boolean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 957
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 958
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", number_text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 959
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", enum_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 960
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", phone=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 961
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 962
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_7

    const-string v1, ", address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 963
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_8

    const-string v1, ", date=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Data{"

    .line 964
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
