.class final Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$ProtoAdapter_UpdateAttachmentRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UpdateAttachmentRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UpdateAttachmentRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 123
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 142
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;-><init>()V

    .line 143
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 144
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 149
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 147
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Attachment;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->attachment(Lcom/squareup/protos/client/rolodex/Attachment;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;

    goto :goto_0

    .line 146
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;

    goto :goto_0

    .line 153
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 154
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$ProtoAdapter_UpdateAttachmentRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 135
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 136
    sget-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 137
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 121
    check-cast p2, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$ProtoAdapter_UpdateAttachmentRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;)I
    .locals 4

    .line 128
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    const/4 v3, 0x2

    .line 129
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 121
    check-cast p1, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$ProtoAdapter_UpdateAttachmentRequest;->encodedSize(Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;
    .locals 2

    .line 159
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;

    move-result-object p1

    .line 160
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Attachment;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    .line 161
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 162
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 121
    check-cast p1, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest$ProtoAdapter_UpdateAttachmentRequest;->redact(Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;)Lcom/squareup/protos/client/rolodex/UpdateAttachmentRequest;

    move-result-object p1

    return-object p1
.end method
