.class public final Lcom/squareup/protos/client/rolodex/Attachment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Attachment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Attachment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Attachment;",
        "Lcom/squareup/protos/client/rolodex/Attachment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attachment_token:Ljava/lang/String;

.field public contact_token:Ljava/lang/String;

.field public content_type:Ljava/lang/String;

.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public file_content:Lokio/ByteString;

.field public file_name:Ljava/lang/String;

.field public file_size_in_bytes:Ljava/lang/Long;

.field public file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

.field public state:Lcom/squareup/protos/client/rolodex/Attachment$State;

.field public updated_at:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 241
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attachment_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->attachment_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/Attachment;
    .locals 13

    .line 316
    new-instance v12, Lcom/squareup/protos/client/rolodex/Attachment;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->attachment_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->contact_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->content_type:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_size_in_bytes:Ljava/lang/Long;

    iget-object v8, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_content:Lokio/ByteString;

    iget-object v9, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v10, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/rolodex/Attachment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Attachment$FileType;Lcom/squareup/protos/client/rolodex/Attachment$State;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->build()Lcom/squareup/protos/client/rolodex/Attachment;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public content_type(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->content_type:Ljava/lang/String;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public file_content(Lokio/ByteString;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_content:Lokio/ByteString;

    return-object p0
.end method

.method public file_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_name:Ljava/lang/String;

    return-object p0
.end method

.method public file_size_in_bytes(Ljava/lang/Long;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_size_in_bytes:Ljava/lang/Long;

    return-object p0
.end method

.method public file_type(Lcom/squareup/protos/client/rolodex/Attachment$FileType;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->file_type:Lcom/squareup/protos/client/rolodex/Attachment$FileType;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/rolodex/Attachment$State;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->state:Lcom/squareup/protos/client/rolodex/Attachment$State;

    return-object p0
.end method

.method public updated_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/Attachment$Builder;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Attachment$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
