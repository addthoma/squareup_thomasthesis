.class public final Lcom/squareup/protos/client/rolodex/BuyerSummary;
.super Lcom/squareup/wire/Message;
.source "BuyerSummary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/BuyerSummary$ProtoAdapter_BuyerSummary;,
        Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/BuyerSummary;",
        "Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/BuyerSummary;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AVERAGE_TRANSACTION_FREQUENCY_MILLIS:Ljava/lang/Long;

.field public static final DEFAULT_TRANSACTION_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_TRANSACTION_FREQUENCY_DESCRIPTION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final average_transaction_frequency_millis:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final first_visit:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final last_visit:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final total_spent:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final transaction_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final transaction_frequency_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/client/rolodex/BuyerSummary$ProtoAdapter_BuyerSummary;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/BuyerSummary$ProtoAdapter_BuyerSummary;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 30
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->DEFAULT_TRANSACTION_COUNT:Ljava/lang/Long;

    .line 32
    sput-object v0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->DEFAULT_AVERAGE_TRANSACTION_FREQUENCY_MILLIS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 8

    .line 84
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/rolodex/BuyerSummary;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 90
    sget-object v0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 91
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    .line 92
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    .line 93
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    .line 94
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->average_transaction_frequency_millis:Ljava/lang/Long;

    .line 95
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    .line 96
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 115
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 116
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/BuyerSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/BuyerSummary;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->average_transaction_frequency_millis:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->average_transaction_frequency_millis:Ljava/lang/Long;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    .line 123
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 128
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/BuyerSummary;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->average_transaction_frequency_millis:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 137
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;
    .locals 2

    .line 101
    new-instance v0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->transaction_count:Ljava/lang/Long;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->average_transaction_frequency_millis:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->average_transaction_frequency_millis:Ljava/lang/Long;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->total_spent:Lcom/squareup/protos/common/Money;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->transaction_frequency_description:Ljava/lang/String;

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/BuyerSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/BuyerSummary;->newBuilder()Lcom/squareup/protos/client/rolodex/BuyerSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", transaction_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", first_visit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->first_visit:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    const-string v1, ", last_visit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->last_visit:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->average_transaction_frequency_millis:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", average_transaction_frequency_millis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->average_transaction_frequency_millis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", total_spent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->total_spent:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", transaction_frequency_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/BuyerSummary;->transaction_frequency_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BuyerSummary{"

    .line 151
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
