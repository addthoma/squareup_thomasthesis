.class public final Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;
.super Lcom/squareup/wire/Message;
.source "ListMergeProposalResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$ProtoAdapter_ListMergeProposalResponse;,
        Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DUPLICATE_CONTACT_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_PAGING_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_TOTAL_DUPLICATE_COUNT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final duplicate_contact_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final merge_proposals:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.MergeProposal#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            ">;"
        }
    .end annotation
.end field

.field public final paging_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final total_duplicate_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$ProtoAdapter_ListMergeProposalResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$ProtoAdapter_ListMergeProposalResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->DEFAULT_TOTAL_DUPLICATE_COUNT:Ljava/lang/Integer;

    .line 31
    sput-object v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->DEFAULT_DUPLICATE_CONTACT_COUNT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 72
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 78
    sget-object v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->status:Lcom/squareup/protos/client/Status;

    const-string p1, "merge_proposals"

    .line 80
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->merge_proposals:Ljava/util/List;

    .line 81
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->paging_key:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->total_duplicate_count:Ljava/lang/Integer;

    .line 83
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->duplicate_contact_count:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 101
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 102
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->status:Lcom/squareup/protos/client/Status;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->merge_proposals:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->merge_proposals:Ljava/util/List;

    .line 105
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->paging_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->paging_key:Ljava/lang/String;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->total_duplicate_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->total_duplicate_count:Ljava/lang/Integer;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->duplicate_contact_count:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->duplicate_contact_count:Ljava/lang/Integer;

    .line 108
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 113
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->merge_proposals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->total_duplicate_count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->duplicate_contact_count:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 121
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->merge_proposals:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->merge_proposals:Ljava/util/List;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->paging_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->paging_key:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->total_duplicate_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->total_duplicate_count:Ljava/lang/Integer;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->duplicate_contact_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->duplicate_contact_count:Ljava/lang/Integer;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->newBuilder()Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->merge_proposals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", merge_proposals="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->merge_proposals:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", paging_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->paging_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->total_duplicate_count:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", total_duplicate_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->total_duplicate_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->duplicate_contact_count:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", duplicate_contact_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->duplicate_contact_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListMergeProposalResponse{"

    .line 134
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
