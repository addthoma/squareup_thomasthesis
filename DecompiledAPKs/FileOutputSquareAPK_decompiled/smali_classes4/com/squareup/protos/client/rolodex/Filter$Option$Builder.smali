.class public final Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Filter$Option;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Filter$Option;",
        "Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public label:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1325
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/Filter$Option;
    .locals 4

    .line 1346
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->value:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->label:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/Filter$Option;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1320
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter$Option;

    move-result-object v0

    return-object v0
.end method

.method public label(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;
    .locals 0

    .line 1340
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->label:Ljava/lang/String;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;
    .locals 0

    .line 1332
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Filter$Option$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
