.class public final enum Lcom/squareup/protos/client/rolodex/Attachment$State;
.super Ljava/lang/Enum;
.source "Attachment.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Attachment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Attachment$State$ProtoAdapter_State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/Attachment$State;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/Attachment$State;

.field public static final enum ACTIVE:Lcom/squareup/protos/client/rolodex/Attachment$State;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Attachment$State;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DELETED:Lcom/squareup/protos/client/rolodex/Attachment$State;

.field public static final enum PENDING:Lcom/squareup/protos/client/rolodex/Attachment$State;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 371
    new-instance v0, Lcom/squareup/protos/client/rolodex/Attachment$State;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "PENDING"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/rolodex/Attachment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment$State;->PENDING:Lcom/squareup/protos/client/rolodex/Attachment$State;

    .line 373
    new-instance v0, Lcom/squareup/protos/client/rolodex/Attachment$State;

    const/4 v3, 0x2

    const-string v4, "ACTIVE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/rolodex/Attachment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment$State;->ACTIVE:Lcom/squareup/protos/client/rolodex/Attachment$State;

    .line 375
    new-instance v0, Lcom/squareup/protos/client/rolodex/Attachment$State;

    const/4 v4, 0x3

    const-string v5, "DELETED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/rolodex/Attachment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment$State;->DELETED:Lcom/squareup/protos/client/rolodex/Attachment$State;

    new-array v0, v4, [Lcom/squareup/protos/client/rolodex/Attachment$State;

    .line 370
    sget-object v4, Lcom/squareup/protos/client/rolodex/Attachment$State;->PENDING:Lcom/squareup/protos/client/rolodex/Attachment$State;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Attachment$State;->ACTIVE:Lcom/squareup/protos/client/rolodex/Attachment$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/Attachment$State;->DELETED:Lcom/squareup/protos/client/rolodex/Attachment$State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment$State;->$VALUES:[Lcom/squareup/protos/client/rolodex/Attachment$State;

    .line 377
    new-instance v0, Lcom/squareup/protos/client/rolodex/Attachment$State$ProtoAdapter_State;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Attachment$State$ProtoAdapter_State;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Attachment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 382
    iput p3, p0, Lcom/squareup/protos/client/rolodex/Attachment$State;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/Attachment$State;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 392
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/Attachment$State;->DELETED:Lcom/squareup/protos/client/rolodex/Attachment$State;

    return-object p0

    .line 391
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/Attachment$State;->ACTIVE:Lcom/squareup/protos/client/rolodex/Attachment$State;

    return-object p0

    .line 390
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/rolodex/Attachment$State;->PENDING:Lcom/squareup/protos/client/rolodex/Attachment$State;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Attachment$State;
    .locals 1

    .line 370
    const-class v0, Lcom/squareup/protos/client/rolodex/Attachment$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/Attachment$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/Attachment$State;
    .locals 1

    .line 370
    sget-object v0, Lcom/squareup/protos/client/rolodex/Attachment$State;->$VALUES:[Lcom/squareup/protos/client/rolodex/Attachment$State;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/Attachment$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/Attachment$State;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 399
    iget v0, p0, Lcom/squareup/protos/client/rolodex/Attachment$State;->value:I

    return v0
.end method
