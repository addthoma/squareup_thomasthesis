.class public final Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TriggerMergeProposalResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public job_id:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;->job_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse;

    move-result-object v0

    return-object v0
.end method

.method public job_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;->job_id:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/TriggerMergeProposalResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
