.class public final Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertGroupResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group:Lcom/squareup/protos/client/rolodex/Group;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;
    .locals 4

    .line 118
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;->group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/rolodex/Group;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;

    move-result-object v0

    return-object v0
.end method

.method public group(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;->group:Lcom/squareup/protos/client/rolodex/Group;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
