.class public final Lcom/squareup/protos/client/retail/inventory/Hours$Interval;
.super Lcom/squareup/wire/Message;
.source "Hours.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/Hours;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Interval"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/retail/inventory/Hours$Interval$ProtoAdapter_Interval;,
        Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/retail/inventory/Hours$Interval;",
        "Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/retail/inventory/Hours$Interval;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DURATION_MINUTES:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final duration_minutes:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final start_time:Lcom/squareup/protos/common/time/LocalTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.LocalTime#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 127
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$ProtoAdapter_Interval;

    invoke-direct {v0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$ProtoAdapter_Interval;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 131
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->DEFAULT_DURATION_MINUTES:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/LocalTime;Ljava/lang/Integer;)V
    .locals 1

    .line 146
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;-><init>(Lcom/squareup/protos/common/time/LocalTime;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/LocalTime;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 150
    sget-object v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    .line 152
    iput-object p2, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 167
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 168
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;

    .line 169
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    iget-object v3, p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    .line 170
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    .line 171
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 176
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 178
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/LocalTime;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 181
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;
    .locals 2

    .line 157
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;-><init>()V

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    iput-object v1, v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->duration_minutes:Ljava/lang/Integer;

    .line 160
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->newBuilder()Lcom/squareup/protos/client/retail/inventory/Hours$Interval$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    if-eqz v1, :cond_0

    const-string v1, ", start_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->start_time:Lcom/squareup/protos/common/time/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", duration_minutes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Interval;->duration_minutes:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Interval{"

    .line 191
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
