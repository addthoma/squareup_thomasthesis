.class public final Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AssetUpdateResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

.field public assets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 106
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->assets:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public app_update(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;)Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    return-object p0
.end method

.method public assets(Ljava/util/List;)Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;)",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;"
        }
    .end annotation

    .line 116
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->assets:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;
    .locals 4

    .line 131
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->assets:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->app_update:Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$AppUpdate;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateResponse$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;

    move-result-object v0

    return-object v0
.end method
