.class public final Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;
.super Lcom/squareup/wire/Message;
.source "DiagnosticReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$ProtoAdapter_Item;,
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;,
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final DEFAULT_URI:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final URI:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.DiagnosticReportRequest$Item$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 470
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$ProtoAdapter_Item;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$ProtoAdapter_Item;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 474
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->OTHER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->DEFAULT_TYPE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;Ljava/lang/String;)V
    .locals 1

    .line 494
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;-><init>(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 498
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 499
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 500
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->URI:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 515
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 516
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;

    .line 517
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 518
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->URI:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->URI:Ljava/lang/String;

    .line 519
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 524
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 526
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 527
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 528
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->URI:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 529
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;
    .locals 2

    .line 505
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;-><init>()V

    .line 506
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 507
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->URI:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;->URI:Ljava/lang/String;

    .line 508
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 469
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->newBuilder()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 536
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 537
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->type:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 538
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->URI:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", URI="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;->URI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Item{"

    .line 539
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
