.class public final enum Lcom/squareup/protos/client/tarkin/ReaderType;
.super Ljava/lang/Enum;
.source "ReaderType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/ReaderType$ProtoAdapter_ReaderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/tarkin/ReaderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum A10:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/ReaderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum R12:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum R12C:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum R6:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum S1:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum T2:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum T2B:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum X2:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final enum X2B:Lcom/squareup/protos/client/tarkin/ReaderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 11
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->UNKNOWN:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 13
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v2, 0x1

    const-string v3, "R6"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->R6:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 18
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v3, 0x2

    const-string v4, "R12"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->R12:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 20
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v4, 0x3

    const-string v5, "X2"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->X2:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 25
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v5, 0x4

    const-string v6, "A10"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->A10:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 27
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v6, 0x5

    const-string v7, "T2"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->T2:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 29
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v7, 0x6

    const-string v8, "R12C"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->R12C:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 31
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/4 v8, 0x7

    const-string v9, "T2B"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->T2B:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 33
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/16 v9, 0x8

    const-string v10, "X2B"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->X2B:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 35
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    const/16 v10, 0x9

    const-string v11, "S1"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/tarkin/ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->S1:Lcom/squareup/protos/client/tarkin/ReaderType;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 10
    sget-object v11, Lcom/squareup/protos/client/tarkin/ReaderType;->UNKNOWN:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->R6:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->R12:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->X2:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->A10:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->T2:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->R12C:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->T2B:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->X2B:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/tarkin/ReaderType;->S1:Lcom/squareup/protos/client/tarkin/ReaderType;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->$VALUES:[Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 37
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReaderType$ProtoAdapter_ReaderType;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/ReaderType$ProtoAdapter_ReaderType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput p3, p0, Lcom/squareup/protos/client/tarkin/ReaderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/tarkin/ReaderType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 59
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->S1:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 58
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->X2B:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 57
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->T2B:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 56
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->R12C:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 55
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->T2:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 54
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->A10:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 53
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->X2:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 52
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->R12:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 51
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->R6:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    .line 50
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/tarkin/ReaderType;->UNKNOWN:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReaderType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/tarkin/ReaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/tarkin/ReaderType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->$VALUES:[Lcom/squareup/protos/client/tarkin/ReaderType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/tarkin/ReaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 66
    iget v0, p0, Lcom/squareup/protos/client/tarkin/ReaderType;->value:I

    return v0
.end method
