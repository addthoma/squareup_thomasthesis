.class public final Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;
.super Lcom/squareup/wire/Message;
.source "DiagnosticReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Zip"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$ProtoAdapter_Zip;,
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ARCHIVE:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final archive:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x2
    .end annotation
.end field

.field public final metadata:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.DiagnosticReportRequest$Item#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 693
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$ProtoAdapter_Zip;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$ProtoAdapter_Zip;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 697
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->DEFAULT_ARCHIVE:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 713
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;-><init>(Ljava/util/List;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;",
            "Lokio/ByteString;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 717
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "metadata"

    .line 718
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    .line 719
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 734
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 735
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    .line 736
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    .line 737
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    .line 738
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 743
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 745
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 746
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 747
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 748
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;
    .locals 2

    .line 724
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;-><init>()V

    .line 725
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->metadata:Ljava/util/List;

    .line 726
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->archive:Lokio/ByteString;

    .line 727
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 692
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->newBuilder()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 755
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 756
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->metadata:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 757
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", archive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->archive:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Zip{"

    .line 758
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
