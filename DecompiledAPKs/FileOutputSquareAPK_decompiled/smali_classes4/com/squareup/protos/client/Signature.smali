.class public final Lcom/squareup/protos/client/Signature;
.super Lcom/squareup/wire/Message;
.source "Signature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/Signature$ProtoAdapter_Signature;,
        Lcom/squareup/protos/client/Signature$DataType;,
        Lcom/squareup/protos/client/Signature$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/Signature;",
        "Lcom/squareup/protos/client/Signature$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/Signature;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DATA:Lokio/ByteString;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/Signature$DataType;

.field private static final serialVersionUID:J


# instance fields
.field public final data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/Signature$DataType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Signature$DataType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/Signature$ProtoAdapter_Signature;

    invoke-direct {v0}, Lcom/squareup/protos/client/Signature$ProtoAdapter_Signature;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/Signature;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/client/Signature$DataType;->UNKNOWN:Lcom/squareup/protos/client/Signature$DataType;

    sput-object v0, Lcom/squareup/protos/client/Signature;->DEFAULT_TYPE:Lcom/squareup/protos/client/Signature$DataType;

    .line 28
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/Signature;->DEFAULT_DATA:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Signature$DataType;Lokio/ByteString;)V
    .locals 1

    .line 48
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/Signature;-><init>(Lcom/squareup/protos/client/Signature$DataType;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Signature$DataType;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/protos/client/Signature;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 53
    iput-object p1, p0, Lcom/squareup/protos/client/Signature;->type:Lcom/squareup/protos/client/Signature$DataType;

    .line 54
    iput-object p2, p0, Lcom/squareup/protos/client/Signature;->data:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 69
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/Signature;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 70
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/Signature;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/client/Signature;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/Signature;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->type:Lcom/squareup/protos/client/Signature$DataType;

    iget-object v3, p1, Lcom/squareup/protos/client/Signature;->type:Lcom/squareup/protos/client/Signature$DataType;

    .line 72
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->data:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/client/Signature;->data:Lokio/ByteString;

    .line 73
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 78
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/client/Signature;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->type:Lcom/squareup/protos/client/Signature$DataType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Signature$DataType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->data:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 83
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/Signature$Builder;
    .locals 2

    .line 59
    new-instance v0, Lcom/squareup/protos/client/Signature$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/Signature$Builder;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->type:Lcom/squareup/protos/client/Signature$DataType;

    iput-object v1, v0, Lcom/squareup/protos/client/Signature$Builder;->type:Lcom/squareup/protos/client/Signature$DataType;

    .line 61
    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/Signature$Builder;->data:Lokio/ByteString;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/protos/client/Signature;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/Signature$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/Signature;->newBuilder()Lcom/squareup/protos/client/Signature$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->type:Lcom/squareup/protos/client/Signature$DataType;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->type:Lcom/squareup/protos/client/Signature$DataType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/Signature;->data:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", data=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Signature{"

    .line 93
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
