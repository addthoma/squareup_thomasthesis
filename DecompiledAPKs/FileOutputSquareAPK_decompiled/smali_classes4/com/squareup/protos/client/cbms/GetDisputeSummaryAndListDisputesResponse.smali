.class public final Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
.super Lcom/squareup/wire/Message;
.source "GetDisputeSummaryAndListDisputesResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$ProtoAdapter_GetDisputeSummaryAndListDisputesResponse;,
        Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
        "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIONABLE_DISPUTES_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_RESOLVED_DISPUTE_CURSOR:Ljava/lang/Integer;

.field public static final DEFAULT_TOTAL_DISPUTES_COUNT:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final actionable_disputes_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final resolved_dispute:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.DisputedPayment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation
.end field

.field public final resolved_dispute_cursor:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final total_covered_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final total_disputed_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final total_disputes_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final unresolved_dispute:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.DisputedPayment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$ProtoAdapter_GetDisputeSummaryAndListDisputesResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$ProtoAdapter_GetDisputeSummaryAndListDisputesResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->DEFAULT_ACTIONABLE_DISPUTES_COUNT:Ljava/lang/Long;

    .line 31
    sput-object v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->DEFAULT_TOTAL_DISPUTES_COUNT:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->DEFAULT_RESOLVED_DISPUTE_CURSOR:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 92
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;",
            "Ljava/lang/Integer;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 99
    sget-object v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    .line 101
    iput-object p2, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    .line 102
    iput-object p3, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    .line 103
    iput-object p4, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    .line 104
    iput-object p5, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    const-string p1, "unresolved_dispute"

    .line 105
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    const-string p1, "resolved_dispute"

    .line 106
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    .line 107
    iput-object p8, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 128
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 129
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    .line 136
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    .line 137
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    .line 138
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 143
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 145
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 154
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 2

    .line 112
    new-instance v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;-><init>()V

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->actionable_disputes_count:Ljava/lang/Long;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputes_count:Ljava/lang/Long;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_covered_amount:Lcom/squareup/protos/common/Money;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->unresolved_dispute:Ljava/util/List;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute:Ljava/util/List;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute_cursor:Ljava/lang/Integer;

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->newBuilder()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", actionable_disputes_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->actionable_disputes_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", total_disputes_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputes_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", total_disputed_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", total_covered_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->total_covered_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", unresolved_dispute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->unresolved_dispute:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 168
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", resolved_dispute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", resolved_dispute_cursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;->resolved_dispute_cursor:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetDisputeSummaryAndListDisputesResponse{"

    .line 170
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
