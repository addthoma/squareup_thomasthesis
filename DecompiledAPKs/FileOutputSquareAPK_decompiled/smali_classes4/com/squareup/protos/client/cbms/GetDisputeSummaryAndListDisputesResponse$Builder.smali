.class public final Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetDisputeSummaryAndListDisputesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;",
        "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public actionable_disputes_count:Ljava/lang/Long;

.field public resolved_dispute:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation
.end field

.field public resolved_dispute_cursor:Ljava/lang/Integer;

.field public status:Lcom/squareup/protos/client/Status;

.field public total_covered_amount:Lcom/squareup/protos/common/Money;

.field public total_disputed_amount:Lcom/squareup/protos/common/Money;

.field public total_disputes_count:Ljava/lang/Long;

.field public unresolved_dispute:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 190
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 191
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->unresolved_dispute:Ljava/util/List;

    .line 192
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public actionable_disputes_count(Ljava/lang/Long;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->actionable_disputes_count:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;
    .locals 11

    .line 242
    new-instance v10, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->actionable_disputes_count:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputes_count:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_covered_amount:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->unresolved_dispute:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute_cursor:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 173
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->build()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v0

    return-object v0
.end method

.method public resolved_dispute(Ljava/util/List;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;)",
            "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;"
        }
    .end annotation

    .line 227
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute:Ljava/util/List;

    return-object p0
.end method

.method public resolved_dispute_cursor(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->resolved_dispute_cursor:Ljava/lang/Integer;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public total_covered_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_covered_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_disputed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputed_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_disputes_count(Ljava/lang/Long;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->total_disputes_count:Ljava/lang/Long;

    return-object p0
.end method

.method public unresolved_dispute(Ljava/util/List;)Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;)",
            "Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;"
        }
    .end annotation

    .line 221
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse$Builder;->unresolved_dispute:Ljava/util/List;

    return-object p0
.end method
