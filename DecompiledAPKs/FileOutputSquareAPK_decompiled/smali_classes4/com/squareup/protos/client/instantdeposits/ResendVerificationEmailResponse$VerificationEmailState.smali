.class public final enum Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;
.super Ljava/lang/Enum;
.source "ResendVerificationEmailResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VerificationEmailState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState$ProtoAdapter_VerificationEmailState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALREADY_VERIFIED:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

.field public static final enum SENT:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 105
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    .line 110
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    const/4 v2, 0x1

    const-string v3, "SENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->SENT:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    .line 115
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    const/4 v3, 0x2

    const-string v4, "ALREADY_VERIFIED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->ALREADY_VERIFIED:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    .line 101
    sget-object v4, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->SENT:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->ALREADY_VERIFIED:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->$VALUES:[Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    .line 117
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState$ProtoAdapter_VerificationEmailState;

    invoke-direct {v0}, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState$ProtoAdapter_VerificationEmailState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 121
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 122
    iput p3, p0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 132
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->ALREADY_VERIFIED:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    return-object p0

    .line 131
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->SENT:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    return-object p0

    .line 130
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;
    .locals 1

    .line 101
    const-class v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;
    .locals 1

    .line 101
    sget-object v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->$VALUES:[Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 139
    iget v0, p0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->value:I

    return v0
.end method
