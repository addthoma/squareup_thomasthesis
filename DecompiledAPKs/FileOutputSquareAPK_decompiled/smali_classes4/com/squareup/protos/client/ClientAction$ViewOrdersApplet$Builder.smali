.class public final Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;",
        "Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1728
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;
    .locals 2

    .line 1733
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1727
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    move-result-object v0

    return-object v0
.end method
