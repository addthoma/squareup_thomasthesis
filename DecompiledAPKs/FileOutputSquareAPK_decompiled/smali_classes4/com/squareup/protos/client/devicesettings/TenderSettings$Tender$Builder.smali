.class public final Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TenderSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public legacy_other_tender_type_id:Ljava/lang/Integer;

.field public tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 296
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;
    .locals 4

    .line 314
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    iget-object v2, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 291
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    move-result-object v0

    return-object v0
.end method

.method public legacy_other_tender_type_id(Ljava/lang/Integer;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;->legacy_other_tender_type_id:Ljava/lang/Integer;

    return-object p0
.end method

.method public tender_type(Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender$Builder;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    return-object p0
.end method
