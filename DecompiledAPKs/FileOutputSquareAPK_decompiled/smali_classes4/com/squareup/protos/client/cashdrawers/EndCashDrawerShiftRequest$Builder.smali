.class public final Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EndCashDrawerShiftRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;",
        "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cash_paid_in_money:Lcom/squareup/protos/common/Money;

.field public cash_paid_out_money:Lcom/squareup/protos/common/Money;

.field public cash_payment_money:Lcom/squareup/protos/common/Money;

.field public cash_refunds_money:Lcom/squareup/protos/common/Money;

.field public client_cash_drawer_shift_id:Ljava/lang/String;

.field public client_unique_key:Ljava/lang/String;

.field public device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

.field public employee_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ended_at:Lcom/squareup/protos/client/ISO8601Date;

.field public ending_employee_id:Ljava/lang/String;

.field public entered_description:Ljava/lang/String;

.field public events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field public expected_cash_money:Lcom/squareup/protos/common/Money;

.field public merchant_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 307
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 308
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->employee_id:Ljava/util/List;

    .line 309
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;
    .locals 18

    move-object/from16 v0, p0

    .line 419
    new-instance v17, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;

    iget-object v2, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->employee_id:Ljava/util/List;

    iget-object v6, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->entered_description:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->ending_employee_id:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v9, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    iget-object v10, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    iget-object v11, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iget-object v12, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    iget-object v13, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v14, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    iget-object v15, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 278
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;

    move-result-object v0

    return-object v0
.end method

.method public cash_paid_in_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public cash_paid_out_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public cash_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public cash_refunds_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    return-object p0
.end method

.method public device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 413
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-object p0
.end method

.method public employee_id(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;"
        }
    .end annotation

    .line 337
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->employee_id:Ljava/util/List;

    return-object p0
.end method

.method public ended_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 356
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public ending_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->ending_employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public entered_description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->entered_description:Ljava/lang/String;

    return-object p0
.end method

.method public events(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;)",
            "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;"
        }
    .end annotation

    .line 404
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 405
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    return-object p0
.end method

.method public expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 396
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method
