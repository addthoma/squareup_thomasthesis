.class public final Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;
.super Lcom/squareup/wire/Message;
.source "CreateCashDrawerShiftRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$ProtoAdapter_CreateCashDrawerShiftRequest;,
        Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;",
        "Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_DAY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_CASH_DRAWER_SHIFT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_UNIQUE_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_VIRTUAL_REGISTER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_OPENING_EMPLOYEE_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final business_day_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final client_cash_drawer_shift_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final client_unique_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final client_virtual_register_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.DeviceInfo#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final merchant_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final opened_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final opening_employee_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final starting_cash_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$ProtoAdapter_CreateCashDrawerShiftRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$ProtoAdapter_CreateCashDrawerShiftRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)V
    .locals 11

    .line 108
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V
    .locals 1

    .line 115
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    .line 118
    iput-object p3, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 119
    iput-object p4, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opening_employee_id:Ljava/lang/String;

    .line 120
    iput-object p5, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 121
    iput-object p6, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->starting_cash_money:Lcom/squareup/protos/common/Money;

    .line 122
    iput-object p7, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_virtual_register_id:Ljava/lang/String;

    .line 123
    iput-object p8, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->business_day_id:Ljava/lang/String;

    .line 124
    iput-object p9, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 146
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 147
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opening_employee_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opening_employee_id:Ljava/lang/String;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->starting_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->starting_cash_money:Lcom/squareup/protos/common/Money;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_virtual_register_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_virtual_register_id:Ljava/lang/String;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->business_day_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->business_day_id:Ljava/lang/String;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 157
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 162
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 164
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opening_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->starting_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_virtual_register_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->business_day_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 174
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 2

    .line 129
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;-><init>()V

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opening_employee_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->opening_employee_id:Ljava/lang/String;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->starting_cash_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_virtual_register_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_virtual_register_id:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->business_day_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->business_day_id:Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", client_unique_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", merchant_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", client_cash_drawer_shift_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opening_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", opening_employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opening_employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", opened_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->starting_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", starting_cash_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->starting_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_virtual_register_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", client_virtual_register_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->client_virtual_register_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->business_day_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", business_day_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->business_day_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_8

    const-string v1, ", device_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreateCashDrawerShiftRequest{"

    .line 191
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
