.class public final Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
.super Lcom/squareup/wire/Message;
.source "CashDrawerShift.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$ProtoAdapter_CashDrawerShift;,
        Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;,
        Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_DAY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_CASH_DRAWER_SHIFT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_VIRTUAL_REGISTER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLOSING_EMPLOYEE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_ENDING_EMPLOYEE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_EVENT_DATA_OMITTED:Ljava/lang/Boolean;

.field public static final DEFAULT_MERCHANT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_OPENING_EMPLOYEE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

.field private static final serialVersionUID:J


# instance fields
.field public final business_day_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x15
    .end annotation
.end field

.field public final cash_paid_in_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final cash_paid_out_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final cash_payment_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final cash_refunds_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final client_cash_drawer_shift_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final client_virtual_register_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field

.field public final closed_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final closed_cash_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.DeviceInfo#ADAPTER"
        tag = 0x18
    .end annotation
.end field

.field public final closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.EmployeeInfo#ADAPTER"
        tag = 0x1b
    .end annotation
.end field

.field public final closing_employee_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final employee_id:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ended_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.DeviceInfo#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.EmployeeInfo#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final ending_employee_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final event_data_omitted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1c
    .end annotation
.end field

.field public final events:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.CashDrawerShiftEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x13
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final expected_cash_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final merchant_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final opened_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.DeviceInfo#ADAPTER"
        tag = 0x16
    .end annotation
.end field

.field public final opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.EmployeeInfo#ADAPTER"
        tag = 0x19
    .end annotation
.end field

.field public final opening_employee_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final starting_cash_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.CashDrawerShift$State#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$ProtoAdapter_CashDrawerShift;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$ProtoAdapter_CashDrawerShift;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 37
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->UNKNOWN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->DEFAULT_STATE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    const/4 v0, 0x0

    .line 51
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->DEFAULT_EVENT_DATA_OMITTED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;Lokio/ByteString;)V
    .locals 1

    .line 276
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 277
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->merchant_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    .line 278
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 279
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 280
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id:Ljava/util/List;

    const-string v0, "employee_id"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    .line 281
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->description:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    .line 282
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    .line 283
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    .line 284
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    .line 285
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 286
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 287
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 288
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    .line 289
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 290
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    .line 291
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    .line 292
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    .line 293
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 294
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    .line 295
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->events:Ljava/util/List;

    const-string v0, "events"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    .line 296
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_virtual_register_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    .line 297
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->business_day_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    .line 298
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 299
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 300
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 301
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 302
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 303
    iget-object p2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 304
    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->event_data_omitted:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 345
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 346
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    .line 347
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    .line 348
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 349
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 350
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    .line 351
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    .line 352
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    .line 353
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    .line 354
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    .line 355
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 356
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 357
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 358
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    .line 359
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 360
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    .line 361
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    .line 362
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    .line 363
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 364
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    .line 365
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    .line 366
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    .line 367
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    .line 368
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 369
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 370
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 371
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 372
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 373
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 374
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    .line 375
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 380
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1a

    .line 382
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 383
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 384
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 385
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 386
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 387
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 388
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 389
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 390
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 391
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 392
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 393
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 394
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 395
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 396
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 397
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 398
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 399
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 400
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 401
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 402
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 403
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 404
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 405
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 406
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 407
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 408
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 409
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_19
    add-int/2addr v0, v2

    .line 411
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    .locals 2

    .line 309
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;-><init>()V

    .line 310
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->merchant_id:Ljava/lang/String;

    .line 311
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id:Ljava/util/List;

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->description:Ljava/lang/String;

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee_id:Ljava/lang/String;

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee_id:Ljava/lang/String;

    .line 317
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee_id:Ljava/lang/String;

    .line 318
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    .line 326
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 327
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money:Lcom/squareup/protos/common/Money;

    .line 328
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->events:Ljava/util/List;

    .line 329
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_virtual_register_id:Ljava/lang/String;

    .line 330
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->business_day_id:Ljava/lang/String;

    .line 331
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 332
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 333
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 334
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 335
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 336
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 337
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->event_data_omitted:Ljava/lang/Boolean;

    .line 338
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 418
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 419
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", merchant_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", client_cash_drawer_shift_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    if-eqz v1, :cond_2

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 422
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 423
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", description=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", opening_employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", ending_employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", closing_employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_8

    const-string v1, ", opened_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 428
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_9

    const-string v1, ", ended_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 429
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_a

    const-string v1, ", closed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 430
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    const-string v1, ", starting_cash_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 431
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_c

    const-string v1, ", cash_payment_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 432
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_d

    const-string v1, ", cash_refunds_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 433
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_e

    const-string v1, ", cash_paid_in_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 434
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    const-string v1, ", cash_paid_out_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 435
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_10

    const-string v1, ", expected_cash_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 436
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_11

    const-string v1, ", closed_cash_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 437
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, ", events="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 438
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    if-eqz v1, :cond_13

    const-string v1, ", client_virtual_register_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    if-eqz v1, :cond_14

    const-string v1, ", business_day_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 440
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_15

    const-string v1, ", opening_device_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 441
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_16

    const-string v1, ", ending_device_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 442
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_17

    const-string v1, ", closing_device_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 443
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v1, :cond_18

    const-string v1, ", opening_employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 444
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v1, :cond_19

    const-string v1, ", ending_employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 445
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v1, :cond_1a

    const-string v1, ", closing_employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 446
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    const-string v1, ", event_data_omitted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1b
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CashDrawerShift{"

    .line 447
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
