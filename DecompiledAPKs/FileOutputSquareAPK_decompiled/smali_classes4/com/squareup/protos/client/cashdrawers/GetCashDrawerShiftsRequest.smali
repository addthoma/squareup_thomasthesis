.class public final Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;
.super Lcom/squareup/wire/Message;
.source "GetCashDrawerShiftsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$ProtoAdapter_GetCashDrawerShiftsRequest;,
        Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;",
        "Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_DAY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_VIRTUAL_REGISTER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IGNORE_DEVICE_HEADER_FOR_FILTER:Ljava/lang/Boolean;

.field public static final DEFAULT_MERCHANT_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final business_day_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final client_virtual_register_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.DeviceInfo#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final end_date:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final ignore_device_header_for_filter:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final include_state:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.CashDrawerShift$State#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            ">;"
        }
    .end annotation
.end field

.field public final merchant_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final selected_client_cash_drawer_shift_id:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final start_date:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$ProtoAdapter_GetCashDrawerShiftsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$ProtoAdapter_GetCashDrawerShiftsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->DEFAULT_IGNORE_DEVICE_HEADER_FOR_FILTER:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Ljava/lang/Boolean;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            ">;",
            "Lcom/squareup/protos/client/cashdrawers/DeviceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 126
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            ">;",
            "Lcom/squareup/protos/client/cashdrawers/DeviceInfo;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 134
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    .line 136
    iput-object p2, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 137
    iput-object p3, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 138
    iput-object p4, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    .line 139
    iput-object p5, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    const-string p1, "include_state"

    .line 140
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    .line 141
    iput-object p7, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 142
    iput-object p8, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    const-string p1, "selected_client_cash_drawer_shift_id"

    .line 143
    invoke-static {p1, p9}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 165
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 166
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    .line 167
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    .line 168
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 169
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 170
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    .line 171
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    .line 172
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    .line 173
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 174
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    .line 175
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    .line 176
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 181
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 183
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 2

    .line 148
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;-><init>()V

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->merchant_id:Ljava/lang/String;

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->client_virtual_register_id:Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->business_day_id:Ljava/lang/String;

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->include_state:Ljava/util/List;

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    .line 158
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", merchant_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", start_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 203
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", end_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 204
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", client_virtual_register_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", business_day_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", include_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 207
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_6

    const-string v1, ", device_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 208
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", ignore_device_header_for_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 209
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", selected_client_cash_drawer_shift_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetCashDrawerShiftsRequest{"

    .line 210
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
