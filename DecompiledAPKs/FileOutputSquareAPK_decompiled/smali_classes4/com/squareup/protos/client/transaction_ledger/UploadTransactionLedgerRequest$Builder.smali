.class public final Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UploadTransactionLedgerRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;",
        "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public diagnostics_data:Lokio/ByteString;

.field public entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;"
        }
    .end annotation
.end field

.field public id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 125
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->entries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;
    .locals 5

    .line 160
    new-instance v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->entries:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->diagnostics_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    move-result-object v0

    return-object v0
.end method

.method public diagnostics_data(Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->diagnostics_data:Lokio/ByteString;

    return-object p0
.end method

.method public entries(Ljava/util/List;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;",
            ">;)",
            "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;"
        }
    .end annotation

    .line 143
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->entries:Ljava/util/List;

    return-object p0
.end method

.method public id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
