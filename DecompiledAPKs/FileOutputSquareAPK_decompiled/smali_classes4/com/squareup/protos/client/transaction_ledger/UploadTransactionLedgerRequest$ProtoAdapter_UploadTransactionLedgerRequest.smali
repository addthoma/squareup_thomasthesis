.class final Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$ProtoAdapter_UploadTransactionLedgerRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UploadTransactionLedgerRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UploadTransactionLedgerRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 166
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 188
    new-instance v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;-><init>()V

    .line 189
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 190
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 196
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 194
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->diagnostics_data(Lokio/ByteString;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    goto :goto_0

    .line 193
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->entries:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    goto :goto_0

    .line 200
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 201
    invoke-virtual {v0}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$ProtoAdapter_UploadTransactionLedgerRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 180
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 181
    sget-object v0, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 182
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 183
    invoke-virtual {p2}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    check-cast p2, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$ProtoAdapter_UploadTransactionLedgerRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;)I
    .locals 4

    .line 171
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 172
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->entries:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->diagnostics_data:Lokio/ByteString;

    const/4 v3, 0x3

    .line 173
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    invoke-virtual {p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 164
    check-cast p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$ProtoAdapter_UploadTransactionLedgerRequest;->encodedSize(Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;
    .locals 2

    .line 206
    invoke-virtual {p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;->newBuilder()Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;

    move-result-object p1

    .line 207
    iget-object v0, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 208
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->entries:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/transaction_ledger/TransactionLedgerEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 209
    invoke-virtual {p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 210
    invoke-virtual {p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$Builder;->build()Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 164
    check-cast p1, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest$ProtoAdapter_UploadTransactionLedgerRequest;->redact(Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;)Lcom/squareup/protos/client/transaction_ledger/UploadTransactionLedgerRequest;

    move-result-object p1

    return-object p1
.end method
