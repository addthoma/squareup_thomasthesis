.class public final Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FinishCardActivationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cvv:Ljava/lang/String;

.field public expiration_mmyy:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;
    .locals 4

    .line 244
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;->cvv:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;->expiration_mmyy:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 224
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;->build()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    move-result-object v0

    return-object v0
.end method

.method public cvv(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;->cvv:Ljava/lang/String;

    return-object p0
.end method

.method public expiration_mmyy(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration$Builder;->expiration_mmyy:Ljava/lang/String;

    return-object p0
.end method
