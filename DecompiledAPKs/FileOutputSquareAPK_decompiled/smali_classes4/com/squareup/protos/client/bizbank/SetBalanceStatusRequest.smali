.class public final Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;
.super Lcom/squareup/wire/Message;
.source "SetBalanceStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$ProtoAdapter_SetBalanceStatusRequest;,
        Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;",
        "Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BIZBANK_ENABLED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final bizbank_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$ProtoAdapter_SetBalanceStatusRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$ProtoAdapter_SetBalanceStatusRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->DEFAULT_BIZBANK_ENABLED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1

    .line 34
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 39
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->bizbank_enabled:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 53
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 54
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;

    .line 55
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->bizbank_enabled:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->bizbank_enabled:Ljava/lang/Boolean;

    .line 56
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 61
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 63
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->bizbank_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 65
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;
    .locals 2

    .line 44
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;-><init>()V

    .line 45
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->bizbank_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;->bizbank_enabled:Ljava/lang/Boolean;

    .line 46
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->bizbank_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", bizbank_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;->bizbank_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SetBalanceStatusRequest{"

    .line 74
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
