.class public final Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ToggleCardFreezeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;",
        "Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_token:Ljava/lang/String;

.field public enable_card:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;->card_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;->enable_card:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;->card_token:Ljava/lang/String;

    return-object p0
.end method

.method public enable_card(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest$Builder;->enable_card:Ljava/lang/Boolean;

    return-object p0
.end method
