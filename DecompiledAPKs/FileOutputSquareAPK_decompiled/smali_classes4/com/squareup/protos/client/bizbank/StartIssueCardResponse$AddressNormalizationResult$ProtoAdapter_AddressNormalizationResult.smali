.class final Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult$ProtoAdapter_AddressNormalizationResult;
.super Lcom/squareup/wire/EnumAdapter;
.source "StartIssueCardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AddressNormalizationResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 229
    const-class v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;
    .locals 0

    .line 234
    invoke-static {p1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->fromValue(I)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 227
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult$ProtoAdapter_AddressNormalizationResult;->fromValue(I)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    move-result-object p1

    return-object p1
.end method
