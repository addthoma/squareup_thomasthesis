.class public final Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetCardActivityResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activity_list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation
.end field

.field public balance:Lcom/squareup/protos/common/Money;

.field public batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

.field public fetched_at:Lcom/squareup/protos/client/ISO8601Date;

.field public max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 163
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 164
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->activity_list:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public activity_list(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;"
        }
    .end annotation

    .line 171
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->activity_list:Ljava/util/List;

    return-object p0
.end method

.method public balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public batch_response(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;
    .locals 9

    .line 212
    new-instance v8, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->activity_list:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v5, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;

    move-result-object v0

    return-object v0
.end method

.method public fetched_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->fetched_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public max_instant_deposit_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
