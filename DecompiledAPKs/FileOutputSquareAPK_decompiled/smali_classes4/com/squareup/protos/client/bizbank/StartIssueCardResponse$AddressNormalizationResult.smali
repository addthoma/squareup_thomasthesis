.class public final enum Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;
.super Ljava/lang/Enum;
.source "StartIssueCardResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AddressNormalizationResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult$ProtoAdapter_AddressNormalizationResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SUCCESS:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

.field public static final enum UNKNOWN_ERROR:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

.field public static final enum VERIFICATION_FAILED:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

.field public static final enum VERIFICATION_NOT_AVAILABLE:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 193
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->SUCCESS:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    .line 195
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    const/4 v3, 0x2

    const-string v4, "VERIFICATION_NOT_AVAILABLE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->VERIFICATION_NOT_AVAILABLE:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    .line 197
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    const/4 v4, 0x3

    const-string v5, "VERIFICATION_FAILED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->VERIFICATION_FAILED:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    .line 199
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    const/4 v5, 0x4

    const-string v6, "UNKNOWN_ERROR"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->UNKNOWN_ERROR:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    new-array v0, v5, [Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    .line 192
    sget-object v5, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->SUCCESS:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->VERIFICATION_NOT_AVAILABLE:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->VERIFICATION_FAILED:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->UNKNOWN_ERROR:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->$VALUES:[Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    .line 201
    new-instance v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult$ProtoAdapter_AddressNormalizationResult;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult$ProtoAdapter_AddressNormalizationResult;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 206
    iput p3, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 217
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->UNKNOWN_ERROR:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    return-object p0

    .line 216
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->VERIFICATION_FAILED:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    return-object p0

    .line 215
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->VERIFICATION_NOT_AVAILABLE:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    return-object p0

    .line 214
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->SUCCESS:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;
    .locals 1

    .line 192
    const-class v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;
    .locals 1

    .line 192
    sget-object v0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->$VALUES:[Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 224
    iget v0, p0, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->value:I

    return v0
.end method
