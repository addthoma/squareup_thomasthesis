.class final Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$ProtoAdapter_BatchResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetCardActivityResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BatchResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 298
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 315
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;-><init>()V

    .line 316
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 317
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 321
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 319
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;->next_batch_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;

    goto :goto_0

    .line 325
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 326
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 296
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$ProtoAdapter_BatchResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 309
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->next_batch_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 310
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 296
    check-cast p2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$ProtoAdapter_BatchResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;)I
    .locals 3

    .line 303
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->next_batch_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 304
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 296
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$ProtoAdapter_BatchResponse;->encodedSize(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;
    .locals 0

    .line 331
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;

    move-result-object p1

    .line 332
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 333
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 296
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse$ProtoAdapter_BatchResponse;->redact(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;)Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    move-result-object p1

    return-object p1
.end method
