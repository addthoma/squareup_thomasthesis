.class public final Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SwitchJobsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/SwitchJobsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/SwitchJobsResponse;",
        "Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public employee_job_infos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;"
        }
    .end annotation
.end field

.field public new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

.field public valid:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 151
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 152
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->employee_job_infos:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/SwitchJobsResponse;
    .locals 8

    .line 196
    new-instance v7, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->employee_job_infos:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->valid:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 140
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->build()Lcom/squareup/protos/client/timecards/SwitchJobsResponse;

    move-result-object v0

    return-object v0
.end method

.method public employee_job_infos(Ljava/util/List;)Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;)",
            "Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;"
        }
    .end annotation

    .line 184
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->employee_job_infos:Ljava/util/List;

    return-object p0
.end method

.method public new_timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-object p0
.end method

.method public old_timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-object p0
.end method

.method public old_timecard_break(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    return-object p0
.end method

.method public valid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->valid:Ljava/lang/Boolean;

    return-object p0
.end method
