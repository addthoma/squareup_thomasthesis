.class public final Lcom/squareup/protos/client/timecards/TimecardBreak;
.super Lcom/squareup/wire/Message;
.source "TimecardBreak.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/TimecardBreak$ProtoAdapter_TimecardBreak;,
        Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/TimecardBreak;",
        "Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/TimecardBreak;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BREAK_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_EXPECTED_DURATION_SECONDS:Ljava/lang/Integer;

.field public static final DEFAULT_IS_PAID:Ljava/lang/Boolean;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MINIMUM_DURATION_SECONDS:Ljava/lang/Integer;

.field public static final DEFAULT_TIMECARD_BREAK_DEFINITION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TIMECARD_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final break_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final end_timestamp:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final expected_duration_seconds:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x9
    .end annotation
.end field

.field public final is_paid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final minimum_duration_seconds:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0xa
    .end annotation
.end field

.field public final start_timestamp:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final timecard_break_definition_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final timecard_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/timecards/TimecardBreak$ProtoAdapter_TimecardBreak;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/TimecardBreak$ProtoAdapter_TimecardBreak;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->DEFAULT_IS_PAID:Ljava/lang/Boolean;

    .line 39
    sput-object v1, Lcom/squareup/protos/client/timecards/TimecardBreak;->DEFAULT_EXPECTED_DURATION_SECONDS:Ljava/lang/Integer;

    .line 41
    sput-object v1, Lcom/squareup/protos/client/timecards/TimecardBreak;->DEFAULT_MINIMUM_DURATION_SECONDS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 12

    .line 137
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/timecards/TimecardBreak;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 145
    sget-object v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    .line 147
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    .line 148
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    .line 149
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 150
    iput-object p5, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 151
    iput-object p6, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    .line 152
    iput-object p7, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    .line 153
    iput-object p8, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    .line 154
    iput-object p9, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    .line 155
    iput-object p10, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 178
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 179
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 180
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreak;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreak;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 185
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    .line 190
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 195
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 197
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreak;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 208
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;
    .locals 2

    .line 160
    new-instance v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->token:Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->timecard_token:Ljava/lang/String;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->merchant_token:Ljava/lang/String;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->timecard_break_definition_token:Ljava/lang/String;

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->is_paid:Ljava/lang/Boolean;

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->break_name:Ljava/lang/String;

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->expected_duration_seconds:Ljava/lang/Integer;

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->minimum_duration_seconds:Ljava/lang/Integer;

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreak;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreak;->newBuilder()Lcom/squareup/protos/client/timecards/TimecardBreak$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", timecard_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_3

    const-string v1, ", start_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->start_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 220
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", end_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->end_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 221
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", timecard_break_definition_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->timecard_break_definition_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", is_paid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->is_paid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 223
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", break_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->break_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", expected_duration_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->expected_duration_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 225
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    const-string v1, ", minimum_duration_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreak;->minimum_duration_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TimecardBreak{"

    .line 226
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
