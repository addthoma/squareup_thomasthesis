.class public final Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StopTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public job_summaries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
            ">;"
        }
    .end annotation
.end field

.field public paid_seconds:Ljava/lang/Long;

.field public total_seconds:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 259
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 260
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->job_summaries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;
    .locals 5

    .line 287
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->paid_seconds:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->total_seconds:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->job_summaries:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 252
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    move-result-object v0

    return-object v0
.end method

.method public job_summaries(Ljava/util/List;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
            ">;)",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;"
        }
    .end annotation

    .line 280
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->job_summaries:Ljava/util/List;

    return-object p0
.end method

.method public paid_seconds(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->paid_seconds:Ljava/lang/Long;

    return-object p0
.end method

.method public total_seconds(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->total_seconds:Ljava/lang/Long;

    return-object p0
.end method
