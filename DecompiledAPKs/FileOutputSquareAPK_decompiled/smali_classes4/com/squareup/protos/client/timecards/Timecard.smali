.class public final Lcom/squareup/protos/client/timecards/Timecard;
.super Lcom/squareup/wire/Message;
.source "Timecard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/Timecard$ProtoAdapter_Timecard;,
        Lcom/squareup/protos/client/timecards/Timecard$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/Timecard;",
        "Lcom/squareup/protos/client/timecards/Timecard$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLOCKIN_TIMESTAMP_MS:Ljava/lang/Long;

.field public static final DEFAULT_CLOCKIN_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CLOCKOUT_TIMESTAMP_MS:Ljava/lang/Long;

.field public static final DEFAULT_CLOCKOUT_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_DELETED:Ljava/lang/Boolean;

.field public static final DEFAULT_EMPLOYEE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TIMECARD_NOTES:Ljava/lang/String; = ""

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final clockin_timestamp_ms:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x8
    .end annotation
.end field

.field public final clockin_unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final clockout_timestamp_ms:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x9
    .end annotation
.end field

.field public final clockout_unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final deleted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.employeejobs.EmployeeJobInfo#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final employee_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final hourly_wage:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xa
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final timecard_notes:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/protos/client/timecards/Timecard$ProtoAdapter_Timecard;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/Timecard$ProtoAdapter_Timecard;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/Timecard;->DEFAULT_DELETED:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 42
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/Timecard;->DEFAULT_CLOCKIN_TIMESTAMP_MS:Ljava/lang/Long;

    .line 44
    sput-object v0, Lcom/squareup/protos/client/timecards/Timecard;->DEFAULT_CLOCKOUT_TIMESTAMP_MS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/String;)V
    .locals 12

    .line 143
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/timecards/Timecard;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 150
    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    .line 152
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_token:Ljava/lang/String;

    .line 153
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    .line 154
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_unit_token:Ljava/lang/String;

    .line 155
    iput-object p5, p0, Lcom/squareup/protos/client/timecards/Timecard;->deleted:Ljava/lang/Boolean;

    .line 156
    iput-object p6, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 157
    iput-object p7, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    .line 158
    iput-object p8, p0, Lcom/squareup/protos/client/timecards/Timecard;->hourly_wage:Lcom/squareup/protos/common/Money;

    .line 159
    iput-object p9, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    .line 160
    iput-object p10, p0, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 183
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 184
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/Timecard;

    .line 185
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/Timecard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/Timecard;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    .line 186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->employee_token:Ljava/lang/String;

    .line 187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    .line 188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->clockout_unit_token:Ljava/lang/String;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->deleted:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->deleted:Ljava/lang/Boolean;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    .line 192
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->hourly_wage:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->hourly_wage:Lcom/squareup/protos/common/Money;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    .line 195
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 200
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 202
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/Timecard;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->hourly_wage:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 213
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 2

    .line 165
    new-instance v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/Timecard$Builder;-><init>()V

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->token:Ljava/lang/String;

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->employee_token:Ljava/lang/String;

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_unit_token:Ljava/lang/String;

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockout_unit_token:Ljava/lang/String;

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->deleted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->deleted:Ljava/lang/Boolean;

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockout_timestamp_ms:Ljava/lang/Long;

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->hourly_wage:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->hourly_wage:Lcom/squareup/protos/common/Money;

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->timecard_notes:Ljava/lang/String;

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/Timecard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/Timecard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/Timecard;->newBuilder()Lcom/squareup/protos/client/timecards/Timecard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", employee_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", clockin_unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", clockout_unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", deleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->deleted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 226
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", clockin_timestamp_ms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 227
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", clockout_timestamp_ms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->clockout_timestamp_ms:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 228
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->hourly_wage:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", hourly_wage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->hourly_wage:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 229
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    if-eqz v1, :cond_8

    const-string v1, ", employee_job_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 230
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", timecard_notes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard;->timecard_notes:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Timecard{"

    .line 231
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
