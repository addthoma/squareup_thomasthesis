.class final Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$ProtoAdapter_UpdateTimecardNotesRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UpdateTimecardNotesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UpdateTimecardNotesRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 116
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 135
    new-instance v0, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;-><init>()V

    .line 136
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 137
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 142
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 140
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->note_content(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;

    goto :goto_0

    .line 139
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->timecard_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;

    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 147
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->build()Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$ProtoAdapter_UpdateTimecardNotesRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 128
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;->timecard_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 129
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;->note_content:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 130
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    check-cast p2, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$ProtoAdapter_UpdateTimecardNotesRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;)I
    .locals 4

    .line 121
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;->timecard_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;->note_content:Ljava/lang/String;

    const/4 v3, 0x2

    .line 122
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 114
    check-cast p1, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$ProtoAdapter_UpdateTimecardNotesRequest;->encodedSize(Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;)Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;
    .locals 0

    .line 152
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;->newBuilder()Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;

    move-result-object p1

    .line 153
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 154
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$Builder;->build()Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 114
    check-cast p1, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest$ProtoAdapter_UpdateTimecardNotesRequest;->redact(Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;)Lcom/squareup/protos/client/timecards/UpdateTimecardNotesRequest;

    move-result-object p1

    return-object p1
.end method
