.class public final Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;
.super Lcom/squareup/wire/Message;
.source "TimecardBreakDefinition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$ProtoAdapter_TimecardBreakDefinition;,
        Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
        "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BREAK_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_EXPECTED_DURATION_SECONDS:Ljava/lang/Integer;

.field public static final DEFAULT_IS_PAID:Ljava/lang/Boolean;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final break_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final expected_duration_seconds:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x3
    .end annotation
.end field

.field public final is_paid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$ProtoAdapter_TimecardBreakDefinition;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$ProtoAdapter_TimecardBreakDefinition;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->DEFAULT_EXPECTED_DURATION_SECONDS:Ljava/lang/Integer;

    .line 33
    sput-object v1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->DEFAULT_IS_PAID:Ljava/lang/Boolean;

    .line 35
    sput-object v1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/client/ISO8601Date;)V
    .locals 8

    .line 93
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    .line 102
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    .line 103
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    .line 104
    iput-object p5, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    .line 105
    iput-object p6, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 124
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 125
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 132
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 137
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 146
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
    .locals 2

    .line 110
    new-instance v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->token:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->break_name:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->expected_duration_seconds:Ljava/lang/Integer;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->is_paid:Ljava/lang/Boolean;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->enabled:Ljava/lang/Boolean;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->newBuilder()Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", break_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", expected_duration_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", is_paid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->is_paid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    const-string v1, ", updated_at_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TimecardBreakDefinition{"

    .line 160
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
