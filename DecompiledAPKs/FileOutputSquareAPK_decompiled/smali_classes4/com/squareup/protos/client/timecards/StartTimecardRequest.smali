.class public final Lcom/squareup/protos/client/timecards/StartTimecardRequest;
.super Lcom/squareup/wire/Message;
.source "StartTimecardRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/StartTimecardRequest$ProtoAdapter_StartTimecardRequest;,
        Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/StartTimecardRequest;",
        "Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/StartTimecardRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLOCKIN_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYEE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_JOB_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_USE_JOB_TOKEN:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final clockin_unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final employee_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final job_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final use_job_token:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$ProtoAdapter_StartTimecardRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$ProtoAdapter_StartTimecardRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->DEFAULT_USE_JOB_TOKEN:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 6

    .line 66
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    .line 75
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 92
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    .line 98
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 103
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 110
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->employee_token:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->clockin_unit_token:Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->use_job_token:Ljava/lang/Boolean;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->job_token:Ljava/lang/String;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->newBuilder()Lcom/squareup/protos/client/timecards/StartTimecardRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", employee_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->employee_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", clockin_unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->clockin_unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", use_job_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->use_job_token:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", job_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardRequest;->job_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartTimecardRequest{"

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
