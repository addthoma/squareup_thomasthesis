.class public final Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;
.super Lcom/squareup/wire/Message;
.source "OvertimeReportByTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ByTimecard"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$ProtoAdapter_ByTimecard;,
        Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;",
        "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTimeInterval#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.CalculationTotal#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final timecard:Lcom/squareup/protos/client/timecards/Timecard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.Timecard#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 137
    new-instance v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$ProtoAdapter_ByTimecard;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$ProtoAdapter_ByTimecard;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/timecards/CalculationTotal;Lcom/squareup/protos/client/timecards/Timecard;)V
    .locals 1

    .line 164
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;-><init>(Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/timecards/CalculationTotal;Lcom/squareup/protos/client/timecards/Timecard;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/timecards/CalculationTotal;Lcom/squareup/protos/client/timecards/Timecard;Lokio/ByteString;)V
    .locals 1

    .line 169
    sget-object v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 171
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    .line 172
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 188
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 189
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;

    .line 190
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    .line 192
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 193
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 198
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 200
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTimeInterval;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/CalculationTotal;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 204
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;
    .locals 2

    .line 177
    new-instance v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;-><init>()V

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 181
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->newBuilder()Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_0

    const-string v1, ", calculated_time_interval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculated_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 213
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    if-eqz v1, :cond_1

    const-string v1, ", calculation_total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->calculation_total:Lcom/squareup/protos/client/timecards/CalculationTotal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 214
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_2

    const-string v1, ", timecard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/OvertimeReportByTimecardResponse$ByTimecard;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ByTimecard{"

    .line 215
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
