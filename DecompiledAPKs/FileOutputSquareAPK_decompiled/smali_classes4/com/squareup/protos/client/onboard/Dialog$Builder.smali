.class public final Lcom/squareup/protos/client/onboard/Dialog$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Dialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/Dialog;",
        "Lcom/squareup/protos/client/onboard/Dialog$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public message:Ljava/lang/String;

.field public negative_button_text:Ljava/lang/String;

.field public neutral_button_text:Ljava/lang/String;

.field public positive_button_text:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/Dialog;
    .locals 8

    .line 171
    new-instance v7, Lcom/squareup/protos/client/onboard/Dialog;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->message:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->negative_button_text:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->positive_button_text:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->neutral_button_text:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/onboard/Dialog;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Dialog$Builder;->build()Lcom/squareup/protos/client/onboard/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Dialog$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public negative_button_text(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Dialog$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->negative_button_text:Ljava/lang/String;

    return-object p0
.end method

.method public neutral_button_text(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Dialog$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->neutral_button_text:Ljava/lang/String;

    return-object p0
.end method

.method public positive_button_text(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Dialog$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->positive_button_text:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Dialog$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Dialog$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
