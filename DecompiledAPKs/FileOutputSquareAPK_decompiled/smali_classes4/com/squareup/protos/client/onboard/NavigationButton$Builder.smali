.class public final Lcom/squareup/protos/client/onboard/NavigationButton$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/NavigationButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/NavigationButton;",
        "Lcom/squareup/protos/client/onboard/NavigationButton$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Ljava/lang/String;

.field public label:Ljava/lang/String;

.field public style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public action(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NavigationButton$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->action:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/onboard/NavigationButton;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->label:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->action:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/onboard/NavigationButton;-><init>(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->build()Lcom/squareup/protos/client/onboard/NavigationButton;

    move-result-object v0

    return-object v0
.end method

.method public label(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/NavigationButton$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->label:Ljava/lang/String;

    return-object p0
.end method

.method public style(Lcom/squareup/protos/client/onboard/NavigationButton$Style;)Lcom/squareup/protos/client/onboard/NavigationButton$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-object p0
.end method
