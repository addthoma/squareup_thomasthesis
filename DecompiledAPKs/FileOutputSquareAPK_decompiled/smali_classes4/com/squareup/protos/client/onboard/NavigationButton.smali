.class public final Lcom/squareup/protos/client/onboard/NavigationButton;
.super Lcom/squareup/wire/Message;
.source "NavigationButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/onboard/NavigationButton$ProtoAdapter_NavigationButton;,
        Lcom/squareup/protos/client/onboard/NavigationButton$Style;,
        Lcom/squareup/protos/client/onboard/NavigationButton$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/onboard/NavigationButton;",
        "Lcom/squareup/protos/client/onboard/NavigationButton$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/onboard/NavigationButton;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTION:Ljava/lang/String; = ""

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_STYLE:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

.field private static final serialVersionUID:J


# instance fields
.field public final action:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.onboard.NavigationButton$Style#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton$ProtoAdapter_NavigationButton;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/NavigationButton$ProtoAdapter_NavigationButton;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->STYLE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    sput-object v0, Lcom/squareup/protos/client/onboard/NavigationButton;->DEFAULT_STYLE:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/onboard/NavigationButton;-><init>(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/onboard/NavigationButton$Style;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/protos/client/onboard/NavigationButton;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 57
    iput-object p2, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->action:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 74
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/onboard/NavigationButton;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 75
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/onboard/NavigationButton;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/NavigationButton;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/NavigationButton;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    iget-object v3, p1, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->action:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/NavigationButton;->action:Ljava/lang/String;

    .line 79
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/NavigationButton;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->action:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 90
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/onboard/NavigationButton$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->label:Ljava/lang/String;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->action:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->action:Ljava/lang/String;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/NavigationButton;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/onboard/NavigationButton$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/NavigationButton;->newBuilder()Lcom/squareup/protos/client/onboard/NavigationButton$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    if-eqz v1, :cond_0

    const-string v1, ", style="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->action:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/NavigationButton;->action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "NavigationButton{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
