.class final Lcom/squareup/protos/client/dialogue/Message$Data$ProtoAdapter_Data;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Message.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/Message$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Data"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/dialogue/Message$Data;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 405
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/dialogue/Message$Data;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/dialogue/Message$Data;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 426
    new-instance v0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;-><init>()V

    .line 427
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 428
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 434
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 432
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/dialogue/Message$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/dialogue/Message$Transaction;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->transaction(Lcom/squareup/protos/client/dialogue/Message$Transaction;)Lcom/squareup/protos/client/dialogue/Message$Data$Builder;

    goto :goto_0

    .line 431
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->coupon(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/protos/client/dialogue/Message$Data$Builder;

    goto :goto_0

    .line 430
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/dialogue/Comment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/dialogue/Comment;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->comment(Lcom/squareup/protos/client/dialogue/Comment;)Lcom/squareup/protos/client/dialogue/Message$Data$Builder;

    goto :goto_0

    .line 438
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 439
    invoke-virtual {v0}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->build()Lcom/squareup/protos/client/dialogue/Message$Data;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 403
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/Message$Data$ProtoAdapter_Data;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/dialogue/Message$Data;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/dialogue/Message$Data;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 418
    sget-object v0, Lcom/squareup/protos/client/dialogue/Comment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 419
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 420
    sget-object v0, Lcom/squareup/protos/client/dialogue/Message$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 421
    invoke-virtual {p2}, Lcom/squareup/protos/client/dialogue/Message$Data;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 403
    check-cast p2, Lcom/squareup/protos/client/dialogue/Message$Data;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/dialogue/Message$Data$ProtoAdapter_Data;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/dialogue/Message$Data;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/dialogue/Message$Data;)I
    .locals 4

    .line 410
    sget-object v0, Lcom/squareup/protos/client/dialogue/Comment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/Message$Data;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/dialogue/Message$Data;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const/4 v3, 0x2

    .line 411
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/dialogue/Message$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/dialogue/Message$Data;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    const/4 v3, 0x3

    .line 412
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 413
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Message$Data;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 403
    check-cast p1, Lcom/squareup/protos/client/dialogue/Message$Data;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/Message$Data$ProtoAdapter_Data;->encodedSize(Lcom/squareup/protos/client/dialogue/Message$Data;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/dialogue/Message$Data;)Lcom/squareup/protos/client/dialogue/Message$Data;
    .locals 2

    .line 444
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Message$Data;->newBuilder()Lcom/squareup/protos/client/dialogue/Message$Data$Builder;

    move-result-object p1

    .line 445
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/dialogue/Comment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/dialogue/Comment;

    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    .line 446
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/Coupon;

    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 447
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/dialogue/Message$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/dialogue/Message$Transaction;

    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    .line 448
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 449
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->build()Lcom/squareup/protos/client/dialogue/Message$Data;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 403
    check-cast p1, Lcom/squareup/protos/client/dialogue/Message$Data;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/Message$Data$ProtoAdapter_Data;->redact(Lcom/squareup/protos/client/dialogue/Message$Data;)Lcom/squareup/protos/client/dialogue/Message$Data;

    move-result-object p1

    return-object p1
.end method
