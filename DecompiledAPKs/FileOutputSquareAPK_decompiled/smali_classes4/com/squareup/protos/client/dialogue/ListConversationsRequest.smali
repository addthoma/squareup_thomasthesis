.class public final Lcom/squareup/protos/client/dialogue/ListConversationsRequest;
.super Lcom/squareup/wire/Message;
.source "ListConversationsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/dialogue/ListConversationsRequest$ProtoAdapter_ListConversationsRequest;,
        Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/dialogue/ListConversationsRequest;",
        "Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/dialogue/ListConversationsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LIMIT:Ljava/lang/Integer;

.field public static final DEFAULT_PAGING_KEY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final limit:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field

.field public final list_options:Lcom/squareup/protos/client/dialogue/ListOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.ListOptions#ADAPTER"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final paging_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$ProtoAdapter_ListConversationsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$ProtoAdapter_ListConversationsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->DEFAULT_LIMIT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/dialogue/ListOptions;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 1

    .line 59
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;-><init>(Lcom/squareup/protos/client/dialogue/ListOptions;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/dialogue/ListOptions;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 65
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    .line 66
    iput-object p2, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->limit:Ljava/lang/Integer;

    .line 67
    iput-object p3, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->paging_key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 83
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 84
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->limit:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->limit:Ljava/lang/Integer;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->paging_key:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->paging_key:Ljava/lang/String;

    .line 88
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 93
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/dialogue/ListOptions;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 99
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->limit:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->limit:Ljava/lang/Integer;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->paging_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->paging_key:Ljava/lang/String;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->newBuilder()Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    if-eqz v1, :cond_0

    const-string v1, ", list_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->limit:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", paging_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest;->paging_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListConversationsRequest{"

    .line 110
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
