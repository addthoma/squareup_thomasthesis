.class public final Lcom/squareup/protos/client/dialogue/Conversation;
.super Lcom/squareup/wire/Message;
.source "Conversation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/dialogue/Conversation$ProtoAdapter_Conversation;,
        Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/dialogue/Conversation;",
        "Lcom/squareup/protos/client/dialogue/Conversation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/dialogue/Conversation;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_MERCHANT_RESPONSE:Ljava/lang/Boolean;

.field public static final DEFAULT_CHANNEL:Lcom/squareup/protos/client/dialogue/Channel;

.field public static final DEFAULT_OPENED:Ljava/lang/Boolean;

.field public static final DEFAULT_PAYMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_REFERENCE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final allow_merchant_response:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final bill:Lcom/squareup/protos/client/bills/Bill;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Bill#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final channel:Lcom/squareup/protos/client/dialogue/Channel;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.Channel#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final comments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.Comment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Comment;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final contact:Lcom/squareup/protos/client/dialogue/Contact;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.Contact#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final facets:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final messages:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.Message#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Message;",
            ">;"
        }
    .end annotation
.end field

.field public final opened:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final payment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final reference_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.Sentiment#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/protos/client/dialogue/Conversation$ProtoAdapter_Conversation;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/Conversation$ProtoAdapter_Conversation;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/dialogue/Conversation;->DEFAULT_OPENED:Ljava/lang/Boolean;

    .line 39
    sget-object v1, Lcom/squareup/protos/client/dialogue/Sentiment;->INVALID_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

    sput-object v1, Lcom/squareup/protos/client/dialogue/Conversation;->DEFAULT_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 41
    sput-object v0, Lcom/squareup/protos/client/dialogue/Conversation;->DEFAULT_ALLOW_MERCHANT_RESPONSE:Ljava/lang/Boolean;

    .line 45
    sget-object v0, Lcom/squareup/protos/client/dialogue/Channel;->UNKNOWN_CHANNEL:Lcom/squareup/protos/client/dialogue/Channel;

    sput-object v0, Lcom/squareup/protos/client/dialogue/Conversation;->DEFAULT_CHANNEL:Lcom/squareup/protos/client/dialogue/Channel;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Contact;Ljava/lang/Boolean;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Channel;Ljava/util/List;Lcom/squareup/protos/client/bills/Bill;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/dialogue/Contact;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/client/dialogue/Sentiment;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Comment;",
            ">;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/dialogue/Channel;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Message;",
            ">;",
            "Lcom/squareup/protos/client/bills/Bill;",
            ")V"
        }
    .end annotation

    .line 185
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/protos/client/dialogue/Conversation;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Contact;Ljava/lang/Boolean;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Channel;Ljava/util/List;Lcom/squareup/protos/client/bills/Bill;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Contact;Ljava/lang/Boolean;Lcom/squareup/protos/client/dialogue/Sentiment;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/dialogue/Channel;Ljava/util/List;Lcom/squareup/protos/client/bills/Bill;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/dialogue/Contact;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/client/dialogue/Sentiment;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Comment;",
            ">;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/dialogue/Channel;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/Message;",
            ">;",
            "Lcom/squareup/protos/client/bills/Bill;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 193
    sget-object v1, Lcom/squareup/protos/client/dialogue/Conversation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 194
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    move-object v1, p2

    .line 195
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    move-object v1, p3

    .line 196
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    move-object v1, p4

    .line 197
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    move-object v1, p5

    .line 198
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->opened:Ljava/lang/Boolean;

    move-object v1, p6

    .line 199
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    const-string v1, "facets"

    move-object v2, p7

    .line 200
    invoke-static {v1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->facets:Ljava/util/List;

    const-string v1, "comments"

    move-object v2, p8

    .line 201
    invoke-static {v1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->comments:Ljava/util/List;

    move-object v1, p9

    .line 202
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    move-object v1, p10

    .line 203
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    move-object v1, p11

    .line 204
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->reference_token:Ljava/lang/String;

    move-object v1, p12

    .line 205
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    const-string v1, "messages"

    move-object/from16 v2, p13

    .line 206
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    move-object/from16 v1, p14

    .line 207
    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation;->bill:Lcom/squareup/protos/client/bills/Bill;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 234
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/dialogue/Conversation;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 235
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/dialogue/Conversation;

    .line 236
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Conversation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Conversation;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    .line 237
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    .line 240
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->opened:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->opened:Ljava/lang/Boolean;

    .line 241
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->facets:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->facets:Ljava/util/List;

    .line 243
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->comments:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->comments:Ljava/util/List;

    .line 244
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 245
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    .line 246
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->reference_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->reference_token:Ljava/lang/String;

    .line 247
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    .line 248
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    .line 249
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/Conversation;->bill:Lcom/squareup/protos/client/bills/Bill;

    .line 250
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 255
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 257
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Conversation;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/dialogue/Contact;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->opened:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/dialogue/Sentiment;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->facets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->comments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->reference_token:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/dialogue/Channel;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->bill:Lcom/squareup/protos/client/bills/Bill;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Bill;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 272
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/dialogue/Conversation$Builder;
    .locals 2

    .line 212
    new-instance v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/Conversation$Builder;-><init>()V

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->token:Ljava/lang/String;

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->payment_token:Ljava/lang/String;

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->opened:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->opened:Ljava/lang/Boolean;

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->facets:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->facets:Ljava/util/List;

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->comments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->comments:Ljava/util/List;

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->allow_merchant_response:Ljava/lang/Boolean;

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->reference_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->reference_token:Ljava/lang/String;

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->messages:Ljava/util/List;

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->bill:Lcom/squareup/protos/client/bills/Bill;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->bill:Lcom/squareup/protos/client/bills/Bill;

    .line 227
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Conversation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/dialogue/Conversation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Conversation;->newBuilder()Lcom/squareup/protos/client/dialogue/Conversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 280
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", last_comment_created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->last_comment_created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", payment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->payment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    if-eqz v1, :cond_3

    const-string v1, ", contact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->contact:Lcom/squareup/protos/client/dialogue/Contact;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 284
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->opened:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", opened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->opened:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 285
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    if-eqz v1, :cond_5

    const-string v1, ", sentiment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->sentiment:Lcom/squareup/protos/client/dialogue/Sentiment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 286
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->facets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", facets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->facets:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 287
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->comments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", comments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->comments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 288
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_8

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 289
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", allow_merchant_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->allow_merchant_response:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 290
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->reference_token:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", reference_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->reference_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    if-eqz v1, :cond_b

    const-string v1, ", channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->channel:Lcom/squareup/protos/client/dialogue/Channel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 292
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", messages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->messages:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 293
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->bill:Lcom/squareup/protos/client/bills/Bill;

    if-eqz v1, :cond_d

    const-string v1, ", bill="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Conversation;->bill:Lcom/squareup/protos/client/bills/Bill;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Conversation{"

    .line 294
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
