.class final Lcom/squareup/protos/client/dialogue/Comment$ProtoAdapter_Comment;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Comment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/Comment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Comment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/dialogue/Comment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 213
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/dialogue/Comment;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/dialogue/Comment;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 238
    new-instance v0, Lcom/squareup/protos/client/dialogue/Comment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/Comment$Builder;-><init>()V

    .line 239
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 240
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 255
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 253
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->subject(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Comment$Builder;

    goto :goto_0

    .line 247
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/dialogue/Commenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/dialogue/Commenter;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->commenter(Lcom/squareup/protos/client/dialogue/Commenter;)Lcom/squareup/protos/client/dialogue/Comment$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 249
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 244
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->content(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Comment$Builder;

    goto :goto_0

    .line 243
    :cond_3
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/dialogue/Comment$Builder;

    goto :goto_0

    .line 242
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Comment$Builder;

    goto :goto_0

    .line 259
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 260
    invoke-virtual {v0}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->build()Lcom/squareup/protos/client/dialogue/Comment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 211
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/Comment$ProtoAdapter_Comment;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/dialogue/Comment;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/dialogue/Comment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 228
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Comment;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 229
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Comment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 230
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Comment;->content:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 231
    sget-object v0, Lcom/squareup/protos/client/dialogue/Commenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Comment;->commenter:Lcom/squareup/protos/client/dialogue/Commenter;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 232
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/dialogue/Comment;->subject:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 233
    invoke-virtual {p2}, Lcom/squareup/protos/client/dialogue/Comment;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 211
    check-cast p2, Lcom/squareup/protos/client/dialogue/Comment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/dialogue/Comment$ProtoAdapter_Comment;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/dialogue/Comment;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/dialogue/Comment;)I
    .locals 4

    .line 218
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/Comment;->token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/dialogue/Comment;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x2

    .line 219
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/dialogue/Comment;->content:Ljava/lang/String;

    const/4 v3, 0x3

    .line 220
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/dialogue/Commenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/dialogue/Comment;->commenter:Lcom/squareup/protos/client/dialogue/Commenter;

    const/4 v3, 0x4

    .line 221
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/dialogue/Comment;->subject:Ljava/lang/String;

    const/4 v3, 0x5

    .line 222
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Comment;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 211
    check-cast p1, Lcom/squareup/protos/client/dialogue/Comment;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/Comment$ProtoAdapter_Comment;->encodedSize(Lcom/squareup/protos/client/dialogue/Comment;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/dialogue/Comment;)Lcom/squareup/protos/client/dialogue/Comment;
    .locals 2

    .line 265
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Comment;->newBuilder()Lcom/squareup/protos/client/dialogue/Comment$Builder;

    move-result-object p1

    .line 266
    iget-object v0, p1, Lcom/squareup/protos/client/dialogue/Comment$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/dialogue/Comment$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/Comment$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    :cond_0
    const/4 v0, 0x0

    .line 267
    iput-object v0, p1, Lcom/squareup/protos/client/dialogue/Comment$Builder;->content:Ljava/lang/String;

    .line 268
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 269
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/Comment$Builder;->build()Lcom/squareup/protos/client/dialogue/Comment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 211
    check-cast p1, Lcom/squareup/protos/client/dialogue/Comment;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/dialogue/Comment$ProtoAdapter_Comment;->redact(Lcom/squareup/protos/client/dialogue/Comment;)Lcom/squareup/protos/client/dialogue/Comment;

    move-result-object p1

    return-object p1
.end method
