.class final Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$ProtoAdapter_ProvisionReaderRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProvisionReaderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProvisionReaderRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 203
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 228
    new-instance v0, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;-><init>()V

    .line 229
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 230
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 245
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 243
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->iksn(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;

    goto :goto_0

    .line 242
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->key_injection_cert(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;

    goto :goto_0

    .line 241
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->terminal_cert(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;

    goto :goto_0

    .line 240
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->signer_cert(Ljava/lang/String;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;

    goto :goto_0

    .line 234
    :cond_4
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->reader_type(Lcom/squareup/protos/client/enigma/ProvisionReaderType;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 236
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 249
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 250
    invoke-virtual {v0}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->build()Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 201
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$ProtoAdapter_ProvisionReaderRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 218
    sget-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->reader_type:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 219
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->signer_cert:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 220
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->terminal_cert:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 221
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->key_injection_cert:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 222
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->iksn:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    invoke-virtual {p2}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 201
    check-cast p2, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$ProtoAdapter_ProvisionReaderRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;)I
    .locals 4

    .line 208
    sget-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->reader_type:Lcom/squareup/protos/client/enigma/ProvisionReaderType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->signer_cert:Ljava/lang/String;

    const/4 v3, 0x2

    .line 209
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->terminal_cert:Ljava/lang/String;

    const/4 v3, 0x3

    .line 210
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->key_injection_cert:Ljava/lang/String;

    const/4 v3, 0x4

    .line 211
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->iksn:Ljava/lang/String;

    const/4 v3, 0x5

    .line 212
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    invoke-virtual {p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 201
    check-cast p1, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$ProtoAdapter_ProvisionReaderRequest;->encodedSize(Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;
    .locals 0

    .line 255
    invoke-virtual {p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;->newBuilder()Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;

    move-result-object p1

    .line 256
    invoke-virtual {p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 257
    invoke-virtual {p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$Builder;->build()Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 201
    check-cast p1, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderRequest$ProtoAdapter_ProvisionReaderRequest;->redact(Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;)Lcom/squareup/protos/client/enigma/ProvisionReaderRequest;

    move-result-object p1

    return-object p1
.end method
