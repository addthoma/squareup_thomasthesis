.class public final Lcom/squareup/protos/client/irf/InformationRequestDetail;
.super Lcom/squareup/wire/Message;
.source "InformationRequestDetail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/irf/InformationRequestDetail$ProtoAdapter_InformationRequestDetail;,
        Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;,
        Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/irf/InformationRequestDetail;",
        "Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/irf/InformationRequestDetail;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FORM_TYPE:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final form_type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final page:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.irf.Page#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Page;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.irf.InformationRequestDetail$Status#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$ProtoAdapter_InformationRequestDetail;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/InformationRequestDetail$ProtoAdapter_InformationRequestDetail;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    sput-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->DEFAULT_STATUS:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Page;",
            ">;)V"
        }
    .end annotation

    .line 59
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/irf/InformationRequestDetail;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Page;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 64
    sget-object v0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 65
    iput-object p1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->token:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->form_type:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    const-string p1, "page"

    .line 68
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->page:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 85
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 86
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/InformationRequestDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/irf/InformationRequestDetail;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;->token:Ljava/lang/String;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->form_type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;->form_type:Ljava/lang/String;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->page:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/irf/InformationRequestDetail;->page:Ljava/util/List;

    .line 91
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 96
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/InformationRequestDetail;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->form_type:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->page:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;
    .locals 2

    .line 73
    new-instance v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->token:Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->form_type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->form_type:Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->page:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->page:Ljava/util/List;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/InformationRequestDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/InformationRequestDetail;->newBuilder()Lcom/squareup/protos/client/irf/InformationRequestDetail$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->form_type:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", form_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->form_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    if-eqz v1, :cond_2

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->status:Lcom/squareup/protos/client/irf/InformationRequestDetail$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->page:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/InformationRequestDetail;->page:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InformationRequestDetail{"

    .line 115
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
