.class public final Lcom/squareup/protos/client/irf/Field$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Field.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/irf/Field;",
        "Lcom/squareup/protos/client/irf/Field$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public field_label:Ljava/lang/String;

.field public field_name:Ljava/lang/String;

.field public field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public option:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Option;",
            ">;"
        }
    .end annotation
.end field

.field public prompt:Ljava/lang/String;

.field public required:Ljava/lang/Boolean;

.field public tooltip:Ljava/lang/String;

.field public value:Ljava/lang/String;

.field public visibility:Lcom/squareup/protos/client/irf/Visibility;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 222
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 223
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/irf/Field$Builder;->option:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/irf/Field;
    .locals 12

    .line 292
    new-instance v11, Lcom/squareup/protos/client/irf/Field;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->field_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/irf/Field$Builder;->value:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/irf/Field$Builder;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    iget-object v4, p0, Lcom/squareup/protos/client/irf/Field$Builder;->prompt:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/irf/Field$Builder;->field_label:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/irf/Field$Builder;->tooltip:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/irf/Field$Builder;->option:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/protos/client/irf/Field$Builder;->required:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/protos/client/irf/Field$Builder;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/irf/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/Field$FieldType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/client/irf/Visibility;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 203
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Field$Builder;->build()Lcom/squareup/protos/client/irf/Field;

    move-result-object v0

    return-object v0
.end method

.method public field_label(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->field_label:Ljava/lang/String;

    return-object p0
.end method

.method public field_name(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->field_name:Ljava/lang/String;

    return-object p0
.end method

.method public field_type(Lcom/squareup/protos/client/irf/Field$FieldType;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    return-object p0
.end method

.method public option(Ljava/util/List;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Option;",
            ">;)",
            "Lcom/squareup/protos/client/irf/Field$Builder;"
        }
    .end annotation

    .line 275
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 276
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->option:Ljava/util/List;

    return-object p0
.end method

.method public prompt(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->prompt:Ljava/lang/String;

    return-object p0
.end method

.method public required(Ljava/lang/Boolean;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->required:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tooltip(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->tooltip:Ljava/lang/String;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->value:Ljava/lang/String;

    return-object p0
.end method

.method public visibility(Lcom/squareup/protos/client/irf/Visibility;)Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field$Builder;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    return-object p0
.end method
