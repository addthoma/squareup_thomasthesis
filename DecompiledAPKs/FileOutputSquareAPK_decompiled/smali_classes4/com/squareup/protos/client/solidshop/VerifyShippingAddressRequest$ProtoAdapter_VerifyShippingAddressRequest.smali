.class final Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$ProtoAdapter_VerifyShippingAddressRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "VerifyShippingAddressRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_VerifyShippingAddressRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 210
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 235
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;-><init>()V

    .line 236
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 237
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 245
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 243
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->save(Ljava/lang/Boolean;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    goto :goto_0

    .line 242
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/location/Phone;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/location/Phone;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->phone(Lcom/squareup/protos/common/location/Phone;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    goto :goto_0

    .line 241
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    goto :goto_0

    .line 240
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    goto :goto_0

    .line 239
    :cond_4
    sget-object v3, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->shipping_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    goto :goto_0

    .line 249
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 250
    invoke-virtual {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 208
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$ProtoAdapter_VerifyShippingAddressRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 225
    sget-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 226
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->first_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 227
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->last_name:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 228
    sget-object v0, Lcom/squareup/protos/common/location/Phone;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->phone:Lcom/squareup/protos/common/location/Phone;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 229
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 230
    invoke-virtual {p2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 208
    check-cast p2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$ProtoAdapter_VerifyShippingAddressRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)I
    .locals 4

    .line 215
    sget-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->first_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 216
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->last_name:Ljava/lang/String;

    const/4 v3, 0x3

    .line 217
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/location/Phone;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->phone:Lcom/squareup/protos/common/location/Phone;

    const/4 v3, 0x4

    .line 218
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 219
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 208
    check-cast p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$ProtoAdapter_VerifyShippingAddressRequest;->encodedSize(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
    .locals 1

    .line 255
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->newBuilder()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 256
    iput-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 257
    iput-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->first_name:Ljava/lang/String;

    .line 258
    iput-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->last_name:Ljava/lang/String;

    .line 259
    iput-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->phone:Lcom/squareup/protos/common/location/Phone;

    .line 260
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 261
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 208
    check-cast p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$ProtoAdapter_VerifyShippingAddressRequest;->redact(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object p1

    return-object p1
.end method
