.class final Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$ProtoAdapter_FinishAccountSetup;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FinishAccountSetup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4076
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4091
    new-instance v0, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;-><init>()V

    .line 4092
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4093
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 4096
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4100
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4101
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;->build()Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4074
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$ProtoAdapter_FinishAccountSetup;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4086
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4074
    check-cast p2, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$ProtoAdapter_FinishAccountSetup;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)I
    .locals 0

    .line 4081
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4074
    check-cast p1, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$ProtoAdapter_FinishAccountSetup;->encodedSize(Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;
    .locals 0

    .line 4106
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;->newBuilder()Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;

    move-result-object p1

    .line 4107
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4108
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;->build()Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4074
    check-cast p1, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$ProtoAdapter_FinishAccountSetup;->redact(Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    move-result-object p1

    return-object p1
.end method
