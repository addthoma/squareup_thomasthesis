.class public final Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstrumentSummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instruments/InstrumentSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card:Lcom/squareup/protos/client/instruments/CardSummary;

.field public instrument_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/instruments/InstrumentSummary;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;->instrument_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/instruments/InstrumentSummary;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/instruments/CardSummary;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;->build()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v0

    return-object v0
.end method

.method public card(Lcom/squareup/protos/client/instruments/CardSummary;)Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    return-object p0
.end method

.method public instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/InstrumentSummary$Builder;->instrument_token:Ljava/lang/String;

    return-object p0
.end method
