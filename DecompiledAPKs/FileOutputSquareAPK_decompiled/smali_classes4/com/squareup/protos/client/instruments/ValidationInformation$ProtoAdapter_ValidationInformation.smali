.class final Lcom/squareup/protos/client/instruments/ValidationInformation$ProtoAdapter_ValidationInformation;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ValidationInformation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instruments/ValidationInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ValidationInformation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/instruments/ValidationInformation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 160
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/instruments/ValidationInformation;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/instruments/ValidationInformation;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 181
    new-instance v0, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;-><init>()V

    .line 182
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 183
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 189
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 187
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->expiration_date(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    goto :goto_0

    .line 186
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->cvv(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    goto :goto_0

    .line 185
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    goto :goto_0

    .line 193
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 194
    invoke-virtual {v0}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->build()Lcom/squareup/protos/client/instruments/ValidationInformation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 158
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instruments/ValidationInformation$ProtoAdapter_ValidationInformation;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/instruments/ValidationInformation;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/instruments/ValidationInformation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 173
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/ValidationInformation;->postal_code:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/ValidationInformation;->cvv:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 175
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/ValidationInformation;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 176
    invoke-virtual {p2}, Lcom/squareup/protos/client/instruments/ValidationInformation;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 158
    check-cast p2, Lcom/squareup/protos/client/instruments/ValidationInformation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/instruments/ValidationInformation$ProtoAdapter_ValidationInformation;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/instruments/ValidationInformation;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/instruments/ValidationInformation;)I
    .locals 4

    .line 165
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instruments/ValidationInformation;->postal_code:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instruments/ValidationInformation;->cvv:Ljava/lang/String;

    const/4 v3, 0x3

    .line 166
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instruments/ValidationInformation;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    const/4 v3, 0x4

    .line 167
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/ValidationInformation;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 158
    check-cast p1, Lcom/squareup/protos/client/instruments/ValidationInformation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instruments/ValidationInformation$ProtoAdapter_ValidationInformation;->encodedSize(Lcom/squareup/protos/client/instruments/ValidationInformation;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/instruments/ValidationInformation;)Lcom/squareup/protos/client/instruments/ValidationInformation;
    .locals 1

    .line 199
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/ValidationInformation;->newBuilder()Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 200
    iput-object v0, p1, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->postal_code:Ljava/lang/String;

    .line 201
    iput-object v0, p1, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->cvv:Ljava/lang/String;

    .line 202
    iput-object v0, p1, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    .line 203
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 204
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/ValidationInformation$Builder;->build()Lcom/squareup/protos/client/instruments/ValidationInformation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 158
    check-cast p1, Lcom/squareup/protos/client/instruments/ValidationInformation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instruments/ValidationInformation$ProtoAdapter_ValidationInformation;->redact(Lcom/squareup/protos/client/instruments/ValidationInformation;)Lcom/squareup/protos/client/instruments/ValidationInformation;

    move-result-object p1

    return-object p1
.end method
