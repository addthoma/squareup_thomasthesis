.class public final Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;
.super Lcom/squareup/wire/Message;
.source "VerifyAndLinkInstrumentResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$ProtoAdapter_VerifyAndLinkInstrumentResponse;,
        Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.instruments.InstrumentSummary#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$ProtoAdapter_VerifyAndLinkInstrumentResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$ProtoAdapter_VerifyAndLinkInstrumentResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/instruments/InstrumentSummary;)V
    .locals 1

    .line 41
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/instruments/InstrumentSummary;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/instruments/InstrumentSummary;Lokio/ByteString;)V
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    .line 48
    iput-object p2, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 63
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 64
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    .line 66
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 67
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 72
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/instruments/InstrumentSummary;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 77
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;
    .locals 2

    .line 53
    new-instance v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;-><init>()V

    .line 54
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 55
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iput-object v1, v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 56
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->newBuilder()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    if-eqz v1, :cond_1

    const-string v1, ", instrument="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VerifyAndLinkInstrumentResponse{"

    .line 87
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
