.class public final Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MarkShippedAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/MarkShippedAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/MarkShippedAction;",
        "Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public carrier:Ljava/lang/String;

.field public shipped_at:Ljava/lang/String;

.field public tracking_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/MarkShippedAction;
    .locals 5

    .line 142
    new-instance v0, Lcom/squareup/protos/client/orders/MarkShippedAction;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->shipped_at:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->tracking_number:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->carrier:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/orders/MarkShippedAction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->build()Lcom/squareup/protos/client/orders/MarkShippedAction;

    move-result-object v0

    return-object v0
.end method

.method public carrier(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->carrier:Ljava/lang/String;

    return-object p0
.end method

.method public shipped_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->shipped_at:Ljava/lang/String;

    return-object p0
.end method

.method public tracking_number(Ljava/lang/String;)Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/orders/MarkShippedAction$Builder;->tracking_number:Ljava/lang/String;

    return-object p0
.end method
