.class public final Lcom/squareup/protos/client/orders/SearchOrdersRequest;
.super Lcom/squareup/wire/Message;
.source "SearchOrdersRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/SearchOrdersRequest$ProtoAdapter_SearchOrdersRequest;,
        Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/SearchOrdersRequest;",
        "Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/SearchOrdersRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CURSOR:Ljava/lang/String; = ""

.field public static final DEFAULT_LIMIT:Ljava/lang/Integer;

.field public static final DEFAULT_SEARCH_TERM:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bazaar_location_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final client_support:Lcom/squareup/protos/client/orders/ClientSupport;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.ClientSupport#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final cursor:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.SearchOrdersDateTimeFilter#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.SearchOrdersFulfillmentFilter#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final limit:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final location_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final order_groups:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.OrderGroup#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final order_source_name:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final search_term:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final sort:Lcom/squareup/orders/SearchOrdersSort;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.SearchOrdersSort#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$ProtoAdapter_SearchOrdersRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$ProtoAdapter_SearchOrdersRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->DEFAULT_LIMIT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Lcom/squareup/orders/SearchOrdersSort;Lcom/squareup/orders/SearchOrdersFulfillmentFilter;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lcom/squareup/orders/SearchOrdersDateTimeFilter;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/ClientSupport;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;",
            "Lcom/squareup/orders/SearchOrdersSort;",
            "Lcom/squareup/orders/SearchOrdersFulfillmentFilter;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/orders/SearchOrdersDateTimeFilter;",
            ")V"
        }
    .end annotation

    .line 151
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;-><init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Lcom/squareup/orders/SearchOrdersSort;Lcom/squareup/orders/SearchOrdersFulfillmentFilter;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lcom/squareup/orders/SearchOrdersDateTimeFilter;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/orders/ClientSupport;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;Lcom/squareup/orders/SearchOrdersSort;Lcom/squareup/orders/SearchOrdersFulfillmentFilter;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lcom/squareup/orders/SearchOrdersDateTimeFilter;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/orders/ClientSupport;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;",
            "Lcom/squareup/orders/SearchOrdersSort;",
            "Lcom/squareup/orders/SearchOrdersFulfillmentFilter;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/orders/SearchOrdersDateTimeFilter;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 159
    sget-object v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    const-string p1, "location_ids"

    .line 161
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    .line 162
    iput-object p3, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    .line 163
    iput-object p4, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    const-string p1, "order_groups"

    .line 164
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    .line 165
    iput-object p6, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    .line 166
    iput-object p7, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    const-string p1, "order_source_name"

    .line 167
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    .line 168
    iput-object p9, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    const-string p1, "bazaar_location_ids"

    .line 169
    invoke-static {p1, p10}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    .line 170
    iput-object p11, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 194
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 195
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;

    .line 196
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    .line 198
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    .line 199
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    .line 200
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    .line 201
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    .line 202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    .line 203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    .line 204
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    .line 206
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    .line 207
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 212
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 214
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/ClientSupport;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/orders/SearchOrdersSort;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 226
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;
    .locals 2

    .line 175
    new-instance v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;-><init>()V

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->location_ids:Ljava/util/List;

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->limit:Ljava/lang/Integer;

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->cursor:Ljava/lang/String;

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_groups:Ljava/util/List;

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->sort:Lcom/squareup/orders/SearchOrdersSort;

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->order_source_name:Ljava/util/List;

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->search_term:Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->bazaar_location_ids:Ljava/util/List;

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    .line 187
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->newBuilder()Lcom/squareup/protos/client/orders/SearchOrdersRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 233
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    if-eqz v1, :cond_0

    const-string v1, ", client_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->client_support:Lcom/squareup/protos/client/orders/ClientSupport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", location_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->location_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 236
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->limit:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", cursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->cursor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", order_groups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_groups:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 239
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    if-eqz v1, :cond_5

    const-string v1, ", sort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->sort:Lcom/squareup/orders/SearchOrdersSort;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 240
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    if-eqz v1, :cond_6

    const-string v1, ", fulfillments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->fulfillments:Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 241
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", order_source_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->order_source_name:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 242
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", search_term="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->search_term:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", bazaar_location_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->bazaar_location_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 244
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    if-eqz v1, :cond_a

    const-string v1, ", date_time_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/SearchOrdersRequest;->date_time_filter:Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SearchOrdersRequest{"

    .line 245
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
