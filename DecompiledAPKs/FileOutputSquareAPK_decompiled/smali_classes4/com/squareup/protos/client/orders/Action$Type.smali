.class public final enum Lcom/squareup/protos/client/orders/Action$Type;
.super Ljava/lang/Enum;
.source "Action.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/Action$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/orders/Action$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/orders/Action$Type;

.field public static final enum ACCEPT:Lcom/squareup/protos/client/orders/Action$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCEL:Lcom/squareup/protos/client/orders/Action$Type;

.field public static final enum COMPLETE:Lcom/squareup/protos/client/orders/Action$Type;

.field public static final enum MARK_IN_PROGRESS:Lcom/squareup/protos/client/orders/Action$Type;

.field public static final enum MARK_PICKED_UP:Lcom/squareup/protos/client/orders/Action$Type;

.field public static final enum MARK_READY:Lcom/squareup/protos/client/orders/Action$Type;

.field public static final enum MARK_SHIPPED:Lcom/squareup/protos/client/orders/Action$Type;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/orders/Action$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 197
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/orders/Action$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->UNKNOWN:Lcom/squareup/protos/client/orders/Action$Type;

    .line 202
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v2, 0x1

    const-string v3, "ACCEPT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/orders/Action$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->ACCEPT:Lcom/squareup/protos/client/orders/Action$Type;

    .line 207
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v3, 0x2

    const-string v4, "MARK_READY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/orders/Action$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_READY:Lcom/squareup/protos/client/orders/Action$Type;

    .line 212
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v4, 0x3

    const-string v5, "MARK_PICKED_UP"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/orders/Action$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_PICKED_UP:Lcom/squareup/protos/client/orders/Action$Type;

    .line 217
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v5, 0x4

    const-string v6, "MARK_SHIPPED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/orders/Action$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_SHIPPED:Lcom/squareup/protos/client/orders/Action$Type;

    .line 222
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v6, 0x5

    const-string v7, "CANCEL"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/orders/Action$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->CANCEL:Lcom/squareup/protos/client/orders/Action$Type;

    .line 227
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v7, 0x6

    const-string v8, "MARK_IN_PROGRESS"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/orders/Action$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_IN_PROGRESS:Lcom/squareup/protos/client/orders/Action$Type;

    .line 232
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v8, 0x7

    const-string v9, "COMPLETE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/orders/Action$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->COMPLETE:Lcom/squareup/protos/client/orders/Action$Type;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/client/orders/Action$Type;

    .line 196
    sget-object v9, Lcom/squareup/protos/client/orders/Action$Type;->UNKNOWN:Lcom/squareup/protos/client/orders/Action$Type;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/Action$Type;->ACCEPT:Lcom/squareup/protos/client/orders/Action$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/orders/Action$Type;->MARK_READY:Lcom/squareup/protos/client/orders/Action$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/orders/Action$Type;->MARK_PICKED_UP:Lcom/squareup/protos/client/orders/Action$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/orders/Action$Type;->MARK_SHIPPED:Lcom/squareup/protos/client/orders/Action$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/orders/Action$Type;->CANCEL:Lcom/squareup/protos/client/orders/Action$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/orders/Action$Type;->MARK_IN_PROGRESS:Lcom/squareup/protos/client/orders/Action$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/orders/Action$Type;->COMPLETE:Lcom/squareup/protos/client/orders/Action$Type;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->$VALUES:[Lcom/squareup/protos/client/orders/Action$Type;

    .line 234
    new-instance v0, Lcom/squareup/protos/client/orders/Action$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/Action$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/Action$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 238
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 239
    iput p3, p0, Lcom/squareup/protos/client/orders/Action$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/orders/Action$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 254
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/orders/Action$Type;->COMPLETE:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0

    .line 253
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_IN_PROGRESS:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0

    .line 252
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/orders/Action$Type;->CANCEL:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0

    .line 251
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_SHIPPED:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0

    .line 250
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_PICKED_UP:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0

    .line 249
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/orders/Action$Type;->MARK_READY:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0

    .line 248
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/orders/Action$Type;->ACCEPT:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0

    .line 247
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/orders/Action$Type;->UNKNOWN:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/orders/Action$Type;
    .locals 1

    .line 196
    const-class v0, Lcom/squareup/protos/client/orders/Action$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/orders/Action$Type;
    .locals 1

    .line 196
    sget-object v0, Lcom/squareup/protos/client/orders/Action$Type;->$VALUES:[Lcom/squareup/protos/client/orders/Action$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/orders/Action$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/orders/Action$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 261
    iget v0, p0, Lcom/squareup/protos/client/orders/Action$Type;->value:I

    return v0
.end method
