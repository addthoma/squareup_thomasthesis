.class final Lcom/squareup/protos/client/bills/PaymentInstrument$ProtoAdapter_PaymentInstrument;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PaymentInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/PaymentInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PaymentInstrument"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/PaymentInstrument;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 174
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/PaymentInstrument;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/PaymentInstrument;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 195
    new-instance v0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;-><init>()V

    .line 196
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 197
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 203
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 201
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->bitcoin_lightning_instrument_hackweek(Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    goto :goto_0

    .line 200
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/StoredInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/StoredInstrument;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument(Lcom/squareup/protos/client/bills/StoredInstrument;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    goto :goto_0

    .line 199
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    goto :goto_0

    .line 207
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 208
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 172
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/PaymentInstrument$ProtoAdapter_PaymentInstrument;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/PaymentInstrument;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 187
    sget-object v0, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/PaymentInstrument;->card_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 188
    sget-object v0, Lcom/squareup/protos/client/bills/StoredInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/PaymentInstrument;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/PaymentInstrument;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 190
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/PaymentInstrument;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 172
    check-cast p2, Lcom/squareup/protos/client/bills/PaymentInstrument;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/PaymentInstrument$ProtoAdapter_PaymentInstrument;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/PaymentInstrument;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/PaymentInstrument;)I
    .locals 4

    .line 179
    sget-object v0, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/PaymentInstrument;->card_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/StoredInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/PaymentInstrument;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    const/4 v3, 0x2

    .line 180
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/PaymentInstrument;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    const/4 v3, 0x3

    .line 181
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/PaymentInstrument;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 172
    check-cast p1, Lcom/squareup/protos/client/bills/PaymentInstrument;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/PaymentInstrument$ProtoAdapter_PaymentInstrument;->encodedSize(Lcom/squareup/protos/client/bills/PaymentInstrument;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/PaymentInstrument;
    .locals 2

    .line 213
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/PaymentInstrument;->newBuilder()Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    move-result-object p1

    .line 214
    iget-object v0, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 215
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/StoredInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/StoredInstrument;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument:Lcom/squareup/protos/client/bills/StoredInstrument;

    .line 216
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->bitcoin_lightning_instrument_hackweek:Lcom/squareup/protos/client/bills/BitcoinLightningInstrument;

    .line 217
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 218
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 172
    check-cast p1, Lcom/squareup/protos/client/bills/PaymentInstrument;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/PaymentInstrument$ProtoAdapter_PaymentInstrument;->redact(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object p1

    return-object p1
.end method
