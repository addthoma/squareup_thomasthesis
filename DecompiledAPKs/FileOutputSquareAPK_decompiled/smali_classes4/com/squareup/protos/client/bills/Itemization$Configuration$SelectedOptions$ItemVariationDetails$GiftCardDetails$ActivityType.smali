.class public final enum Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;
.super Ljava/lang/Enum;
.source "Itemization.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType$ProtoAdapter_ActivityType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

.field public static final enum ACTIVATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum RELOAD:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1503
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    .line 1505
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    const/4 v2, 0x1

    const-string v3, "ACTIVATION"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->ACTIVATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    .line 1507
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    const/4 v3, 0x2

    const-string v4, "RELOAD"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->RELOAD:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    .line 1502
    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->ACTIVATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->RELOAD:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->$VALUES:[Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    .line 1509
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType$ProtoAdapter_ActivityType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType$ProtoAdapter_ActivityType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1514
    iput p3, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1524
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->RELOAD:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    return-object p0

    .line 1523
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->ACTIVATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    return-object p0

    .line 1522
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->UNKNOWN:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;
    .locals 1

    .line 1502
    const-class v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;
    .locals 1

    .line 1502
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->$VALUES:[Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1531
    iget v0, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails$ActivityType;->value:I

    return v0
.end method
