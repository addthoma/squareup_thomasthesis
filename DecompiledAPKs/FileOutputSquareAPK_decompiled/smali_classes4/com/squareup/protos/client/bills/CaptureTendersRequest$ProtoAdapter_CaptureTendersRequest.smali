.class final Lcom/squareup/protos/client/bills/CaptureTendersRequest$ProtoAdapter_CaptureTendersRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CaptureTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CaptureTendersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CaptureTendersRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 269
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CaptureTendersRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 298
    new-instance v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;-><init>()V

    .line 299
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 300
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 310
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 308
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tender_group_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    goto :goto_0

    .line 307
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    goto :goto_0

    .line 306
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    goto :goto_0

    .line 305
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    goto :goto_0

    .line 304
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tenders:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 303
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    goto :goto_0

    .line 302
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    goto :goto_0

    .line 314
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 315
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 267
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$ProtoAdapter_CaptureTendersRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CaptureTendersRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 286
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 287
    sget-object v0, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 288
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 289
    sget-object v0, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 290
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 291
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 292
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 293
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 267
    check-cast p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$ProtoAdapter_CaptureTendersRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CaptureTendersRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)I
    .locals 4

    .line 274
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    const/4 v3, 0x2

    .line 275
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 276
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    const/4 v3, 0x4

    .line 277
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/4 v3, 0x5

    .line 278
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v3, 0x6

    .line 279
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    const/4 v3, 0x7

    .line 280
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 267
    check-cast p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$ProtoAdapter_CaptureTendersRequest;->encodedSize(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)Lcom/squareup/protos/client/bills/CaptureTendersRequest;
    .locals 2

    .line 320
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object p1

    .line 321
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 322
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Merchant;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 323
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tenders:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/CompleteTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 324
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Bill$Dates;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 325
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 326
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 327
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 328
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 267
    check-cast p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$ProtoAdapter_CaptureTendersRequest;->redact(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    move-result-object p1

    return-object p1
.end method
