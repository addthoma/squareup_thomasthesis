.class public final Lcom/squareup/protos/client/bills/CardTender$Authorization;
.super Lcom/squareup/wire/Message;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Authorization"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Authorization$ProtoAdapter_Authorization;,
        Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;,
        Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;,
        Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CardTender$Authorization;",
        "Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Authorization;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUTHORIZATION_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_DECLINE_REASON:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

.field public static final DEFAULT_SIGNATURE_BEHAVIOR:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

.field private static final serialVersionUID:J


# instance fields
.field public final authorization_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final authorized_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Authorization$DeclineReason#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Authorization$SignatureBehavior#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final signature_optional_max_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1002
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$ProtoAdapter_Authorization;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Authorization$ProtoAdapter_Authorization;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1008
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->DEFAULT_DECLINE_REASON:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    .line 1010
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->DEFAULT_SIG_BEHAVIOR_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->DEFAULT_SIGNATURE_BEHAVIOR:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;)V
    .locals 7

    .line 1057
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/CardTender$Authorization;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;Lokio/ByteString;)V
    .locals 1

    .line 1063
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1064
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    .line 1065
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorized_money:Lcom/squareup/protos/common/Money;

    .line 1066
    iput-object p3, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    .line 1067
    iput-object p4, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    .line 1068
    iput-object p5, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1086
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CardTender$Authorization;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1087
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CardTender$Authorization;

    .line 1088
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Authorization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Authorization;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    .line 1089
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorized_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorized_money:Lcom/squareup/protos/common/Money;

    .line 1090
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    .line 1091
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardTender$Authorization;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    .line 1092
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    .line 1093
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1098
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 1100
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Authorization;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1101
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1102
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorized_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1103
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1104
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1105
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 1106
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;
    .locals 2

    .line 1073
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;-><init>()V

    .line 1074
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->authorization_code:Ljava/lang/String;

    .line 1075
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorized_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->authorized_money:Lcom/squareup/protos/common/Money;

    .line 1076
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    .line 1077
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    .line 1078
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    .line 1079
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Authorization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1001
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Authorization;->newBuilder()Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1114
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", authorization_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorization_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorized_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", authorized_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->authorized_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1116
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", signature_optional_max_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1117
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    if-eqz v1, :cond_3

    const-string v1, ", decline_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1118
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    if-eqz v1, :cond_4

    const-string v1, ", signature_behavior="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Authorization{"

    .line 1119
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
