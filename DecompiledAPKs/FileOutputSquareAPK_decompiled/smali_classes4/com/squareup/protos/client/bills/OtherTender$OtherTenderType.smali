.class public final enum Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;
.super Ljava/lang/Enum;
.source "OtherTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/OtherTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OtherTenderType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType$ProtoAdapter_OtherTenderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CHECK:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum CREDIT_OR_DEBIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum CUSTOM:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum DEBIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum DEBIT_OR_CREDIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum E_MONEY:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum HACKWEEK_CRYPTO:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum MEAL_VOUCHER:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum MERCHANT_GIFT_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum THIRD_PARTY_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 184
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->UNKNOWN:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 186
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v2, 0x1

    const-string v3, "CHECK"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CHECK:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 188
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v3, 0x2

    const-string v4, "MERCHANT_GIFT_CARD"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->MERCHANT_GIFT_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 190
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v4, 0x3

    const-string v5, "DEBIT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->DEBIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 192
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v5, 0x4

    const-string v6, "CREDIT_OR_DEBIT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CREDIT_OR_DEBIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 194
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v6, 0x5

    const-string v7, "E_MONEY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->E_MONEY:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 196
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v7, 0x6

    const-string v8, "DEBIT_OR_CREDIT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->DEBIT_OR_CREDIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 202
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v8, 0x7

    const-string v9, "THIRD_PARTY_CARD"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 204
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/16 v9, 0x8

    const-string v10, "MEAL_VOUCHER"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->MEAL_VOUCHER:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 209
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/16 v10, 0x9

    const-string v11, "HACKWEEK_CRYPTO"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->HACKWEEK_CRYPTO:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 211
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/16 v11, 0xa

    const-string v12, "CUSTOM"

    const/16 v13, 0x64

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CUSTOM:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 183
    sget-object v12, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->UNKNOWN:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CHECK:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->MERCHANT_GIFT_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->DEBIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CREDIT_OR_DEBIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->E_MONEY:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->DEBIT_OR_CREDIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->MEAL_VOUCHER:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->HACKWEEK_CRYPTO:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CUSTOM:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->$VALUES:[Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    .line 213
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType$ProtoAdapter_OtherTenderType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType$ProtoAdapter_OtherTenderType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 218
    iput p3, p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;
    .locals 1

    const/16 v0, 0x64

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 235
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->HACKWEEK_CRYPTO:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 234
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->MEAL_VOUCHER:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 233
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 232
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->DEBIT_OR_CREDIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 231
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->E_MONEY:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 230
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CREDIT_OR_DEBIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 229
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->DEBIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 228
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->MERCHANT_GIFT_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 227
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CHECK:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 226
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->UNKNOWN:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    .line 236
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->CUSTOM:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;
    .locals 1

    .line 183
    const-class v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;
    .locals 1

    .line 183
    sget-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->$VALUES:[Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 243
    iget v0, p0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->value:I

    return v0
.end method
