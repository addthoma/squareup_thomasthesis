.class final Lcom/squareup/protos/client/bills/CardData$KeyedCard$ProtoAdapter_KeyedCard;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$KeyedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_KeyedCard"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardData$KeyedCard;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 713
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$KeyedCard;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 738
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;-><init>()V

    .line 739
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 740
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 748
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 746
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->raw_card_number_utf8(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    goto :goto_0

    .line 745
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    goto :goto_0

    .line 744
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->cvv(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    goto :goto_0

    .line 743
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    goto :goto_0

    .line 742
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    goto :goto_0

    .line 752
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 753
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 711
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$ProtoAdapter_KeyedCard;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$KeyedCard;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 728
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 729
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->cvv:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 730
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->postal_code:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 731
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->card_number:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 732
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->raw_card_number_utf8:Lokio/ByteString;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 733
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 711
    check-cast p2, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$ProtoAdapter_KeyedCard;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$KeyedCard;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)I
    .locals 4

    .line 718
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->cvv:Ljava/lang/String;

    const/4 v3, 0x3

    .line 719
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->postal_code:Ljava/lang/String;

    const/4 v3, 0x4

    .line 720
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->card_number:Ljava/lang/String;

    const/4 v3, 0x1

    .line 721
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->raw_card_number_utf8:Lokio/ByteString;

    const/4 v3, 0x5

    .line 722
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 723
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 711
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$ProtoAdapter_KeyedCard;->encodedSize(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)Lcom/squareup/protos/client/bills/CardData$KeyedCard;
    .locals 2

    .line 758
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->newBuilder()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object p1

    .line 759
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    :cond_0
    const/4 v0, 0x0

    .line 760
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->cvv:Ljava/lang/String;

    .line 761
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->postal_code:Ljava/lang/String;

    .line 762
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number:Ljava/lang/String;

    .line 763
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->raw_card_number_utf8:Lokio/ByteString;

    .line 764
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 765
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 711
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$ProtoAdapter_KeyedCard;->redact(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object p1

    return-object p1
.end method
