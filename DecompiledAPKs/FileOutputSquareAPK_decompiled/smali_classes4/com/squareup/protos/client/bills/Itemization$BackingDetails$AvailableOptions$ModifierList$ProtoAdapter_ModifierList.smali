.class final Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$ProtoAdapter_ModifierList;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ModifierList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2236
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2255
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;-><init>()V

    .line 2256
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2257
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 2262
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2260
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_option:Ljava/util/List;

    sget-object v4, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2259
    :cond_1
    sget-object v3, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    goto :goto_0

    .line 2266
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2267
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2234
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$ProtoAdapter_ModifierList;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2248
    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2249
    sget-object v0, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->modifier_option:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2250
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2234
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$ProtoAdapter_ModifierList;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;)I
    .locals 4

    .line 2241
    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 2242
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->modifier_option:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2243
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2234
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$ProtoAdapter_ModifierList;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;
    .locals 2

    .line 2272
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;

    move-result-object p1

    .line 2273
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/items/ItemModifierList;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemModifierList;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    .line 2274
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->modifier_option:Ljava/util/List;

    sget-object v1, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 2275
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2276
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2234
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList$ProtoAdapter_ModifierList;->redact(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions$ModifierList;

    move-result-object p1

    return-object p1
.end method
