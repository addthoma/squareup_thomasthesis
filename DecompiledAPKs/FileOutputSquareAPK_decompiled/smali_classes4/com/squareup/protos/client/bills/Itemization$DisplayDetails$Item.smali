.class public final Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$ProtoAdapter_Item;,
        Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;",
        "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ABBREVIATION:Ljava/lang/String; = ""

.field public static final DEFAULT_COGS_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_COLOR:Ljava/lang/String; = ""

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_IMAGE_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final abbreviation:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final cogs_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final color:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final image_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2463
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$ProtoAdapter_Item;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$ProtoAdapter_Item;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 2523
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 2528
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2529
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->color:Ljava/lang/String;

    .line 2530
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->description:Ljava/lang/String;

    .line 2531
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    .line 2532
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->abbreviation:Ljava/lang/String;

    .line 2533
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->image_url:Ljava/lang/String;

    .line 2534
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->cogs_object_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2553
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2554
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    .line 2555
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->color:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->color:Ljava/lang/String;

    .line 2556
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->description:Ljava/lang/String;

    .line 2557
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    .line 2558
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->abbreviation:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->abbreviation:Ljava/lang/String;

    .line 2559
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->image_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->image_url:Ljava/lang/String;

    .line 2560
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->cogs_object_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->cogs_object_id:Ljava/lang/String;

    .line 2561
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2566
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 2568
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2569
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->color:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2570
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2571
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2572
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2573
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2574
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->cogs_object_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 2575
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;
    .locals 2

    .line 2539
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;-><init>()V

    .line 2540
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->color:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->color:Ljava/lang/String;

    .line 2541
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->description:Ljava/lang/String;

    .line 2542
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->name:Ljava/lang/String;

    .line 2543
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->abbreviation:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->abbreviation:Ljava/lang/String;

    .line 2544
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->image_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->image_url:Ljava/lang/String;

    .line 2545
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->cogs_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->cogs_object_id:Ljava/lang/String;

    .line 2546
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2462
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2582
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2583
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->color:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->color:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2584
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2585
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2586
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->abbreviation:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", abbreviation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->abbreviation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2587
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->image_url:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", image_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->image_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2588
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->cogs_object_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", cogs_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Item{"

    .line 2589
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
