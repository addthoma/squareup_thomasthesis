.class public final Lcom/squareup/protos/client/bills/RefundV1;
.super Lcom/squareup/wire/Message;
.source "RefundV1.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/RefundV1$ProtoAdapter_RefundV1;,
        Lcom/squareup/protos/client/bills/RefundV1$Amounts;,
        Lcom/squareup/protos/client/bills/RefundV1$State;,
        Lcom/squareup/protos/client/bills/RefundV1$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/RefundV1;",
        "Lcom/squareup/protos/client/bills/RefundV1$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_REFUND_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/bills/RefundV1$State;

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.RefundV1$Amounts#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final refund_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final refund_reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/bills/RefundV1$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.RefundV1$State#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final tender_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$ProtoAdapter_RefundV1;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RefundV1$ProtoAdapter_RefundV1;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/bills/RefundV1$State;->UNKNOWN:Lcom/squareup/protos/client/bills/RefundV1$State;

    sput-object v0, Lcom/squareup/protos/client/bills/RefundV1;->DEFAULT_STATE:Lcom/squareup/protos/client/bills/RefundV1$State;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/RefundV1$State;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/bills/RefundV1$Amounts;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/CreatorDetails;)V
    .locals 9

    .line 90
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/RefundV1;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/RefundV1$State;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/bills/RefundV1$Amounts;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/RefundV1$State;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Lcom/squareup/protos/client/bills/RefundV1$Amounts;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 97
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 98
    iput-object p2, p0, Lcom/squareup/protos/client/bills/RefundV1;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 99
    iput-object p3, p0, Lcom/squareup/protos/client/bills/RefundV1;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 100
    iput-object p4, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_reason:Ljava/lang/String;

    .line 101
    iput-object p5, p0, Lcom/squareup/protos/client/bills/RefundV1;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    .line 102
    iput-object p6, p0, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 103
    iput-object p7, p0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 123
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/RefundV1;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 124
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/RefundV1;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundV1;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RefundV1;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RefundV1;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RefundV1;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RefundV1;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_reason:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RefundV1;->refund_reason:Ljava/lang/String;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RefundV1;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 132
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 137
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundV1;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RefundV1$State;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_reason:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RefundV1$Amounts;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 147
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/RefundV1$Builder;
    .locals 2

    .line 108
    new-instance v0, Lcom/squareup/protos/client/bills/RefundV1$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RefundV1$Builder;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->refund_reason:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RefundV1$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundV1;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/RefundV1$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundV1;->newBuilder()Lcom/squareup/protos/client/bills/RefundV1$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", refund_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    if-eqz v1, :cond_1

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->state:Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_2

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_reason:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", refund_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->refund_reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    if-eqz v1, :cond_4

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->amounts:Lcom/squareup/protos/client/bills/RefundV1$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_5

    const-string v1, ", tender_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 161
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_6

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundV1;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RefundV1{"

    .line 162
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
