.class final Lcom/squareup/protos/client/bills/ExternalTender$ProtoAdapter_ExternalTender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ExternalTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ExternalTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ExternalTender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/ExternalTender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 263
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/ExternalTender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/ExternalTender;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 280
    new-instance v0, Lcom/squareup/protos/client/bills/ExternalTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ExternalTender$Builder;-><init>()V

    .line 281
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 282
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 286
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 284
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->payment_source(Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;)Lcom/squareup/protos/client/bills/ExternalTender$Builder;

    goto :goto_0

    .line 290
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 291
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->build()Lcom/squareup/protos/client/bills/ExternalTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 261
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ExternalTender$ProtoAdapter_ExternalTender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/ExternalTender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/ExternalTender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 274
    sget-object v0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ExternalTender;->payment_source:Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 275
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/ExternalTender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 261
    check-cast p2, Lcom/squareup/protos/client/bills/ExternalTender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/ExternalTender$ProtoAdapter_ExternalTender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/ExternalTender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/ExternalTender;)I
    .locals 3

    .line 268
    sget-object v0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ExternalTender;->payment_source:Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 269
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ExternalTender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 261
    check-cast p1, Lcom/squareup/protos/client/bills/ExternalTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ExternalTender$ProtoAdapter_ExternalTender;->encodedSize(Lcom/squareup/protos/client/bills/ExternalTender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/ExternalTender;)Lcom/squareup/protos/client/bills/ExternalTender;
    .locals 2

    .line 296
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ExternalTender;->newBuilder()Lcom/squareup/protos/client/bills/ExternalTender$Builder;

    move-result-object p1

    .line 297
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->payment_source:Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->payment_source:Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->payment_source:Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    .line 298
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 299
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->build()Lcom/squareup/protos/client/bills/ExternalTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 261
    check-cast p1, Lcom/squareup/protos/client/bills/ExternalTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ExternalTender$ProtoAdapter_ExternalTender;->redact(Lcom/squareup/protos/client/bills/ExternalTender;)Lcom/squareup/protos/client/bills/ExternalTender;

    move-result-object p1

    return-object p1
.end method
