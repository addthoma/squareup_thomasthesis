.class public final Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;
.super Lcom/squareup/wire/Message;
.source "SurchargeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SurchargeLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackingDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$ProtoAdapter_BackingDetails;,
        Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;,
        Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BACKING_TYPE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

.field private static final serialVersionUID:J


# instance fields
.field public final backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SurchargeLineItem$BackingDetails$BackingType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final surcharge:Lcom/squareup/api/items/Surcharge;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Surcharge#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 185
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$ProtoAdapter_BackingDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$ProtoAdapter_BackingDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 189
    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->ITEMS_SERVICE_SURCHARGE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    sput-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->DEFAULT_BACKING_TYPE:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Surcharge;Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;)V
    .locals 1

    .line 204
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/Surcharge;Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Surcharge;Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;Lokio/ByteString;)V
    .locals 1

    .line 208
    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    .line 210
    iput-object p2, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 225
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 226
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    .line 227
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    .line 229
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 234
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 236
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/Surcharge;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 239
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;
    .locals 2

    .line 215
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;-><init>()V

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;->surcharge:Lcom/squareup/api/items/Surcharge;

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    .line 218
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 184
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    if-eqz v1, :cond_0

    const-string v1, ", surcharge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->surcharge:Lcom/squareup/api/items/Surcharge;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    if-eqz v1, :cond_1

    const-string v1, ", backing_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails$BackingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BackingDetails{"

    .line 249
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
