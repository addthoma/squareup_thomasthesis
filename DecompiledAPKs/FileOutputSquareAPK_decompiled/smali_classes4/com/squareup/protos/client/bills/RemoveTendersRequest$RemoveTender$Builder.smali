.class public final Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RemoveTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public tender_id_pair:Lcom/squareup/protos/client/IdPair;

.field public tender_type:Lcom/squareup/protos/client/bills/Tender$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 226
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;
    .locals 4

    .line 245
    new-instance v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 221
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object v0

    return-object v0
.end method

.method public tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0
.end method
