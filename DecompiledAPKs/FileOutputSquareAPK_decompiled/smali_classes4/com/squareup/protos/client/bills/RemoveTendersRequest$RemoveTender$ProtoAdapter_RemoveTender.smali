.class final Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$ProtoAdapter_RemoveTender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RemoveTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RemoveTender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 251
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 270
    new-instance v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;-><init>()V

    .line 271
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 272
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 284
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 277
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 279
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 274
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    goto :goto_0

    .line 288
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 289
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 249
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$ProtoAdapter_RemoveTender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 263
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 264
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 265
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 249
    check-cast p2, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$ProtoAdapter_RemoveTender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;)I
    .locals 4

    .line 256
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v3, 0x2

    .line 257
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 249
    check-cast p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$ProtoAdapter_RemoveTender;->encodedSize(Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;
    .locals 2

    .line 294
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;->newBuilder()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object p1

    .line 295
    iget-object v0, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 296
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 297
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 249
    check-cast p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$ProtoAdapter_RemoveTender;->redact(Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object p1

    return-object p1
.end method
