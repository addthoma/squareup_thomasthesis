.class public final Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SurchargeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SurchargeLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

.field public backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

.field public fee:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

.field public write_only_deleted:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 145
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 146
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    return-object p0
.end method

.method public backing_details(Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/SurchargeLineItem;
    .locals 8

    .line 180
    new-instance v7, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/SurchargeLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object v0

    return-object v0
.end method

.method public fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;"
        }
    .end annotation

    .line 173
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee:Ljava/util/List;

    return-object p0
.end method

.method public surcharge_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->write_only_deleted:Ljava/lang/Boolean;

    return-object p0
.end method
