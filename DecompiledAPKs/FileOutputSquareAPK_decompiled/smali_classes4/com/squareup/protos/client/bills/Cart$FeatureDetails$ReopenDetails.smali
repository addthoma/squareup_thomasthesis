.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReopenDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$ProtoAdapter_ReopenDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final reopened_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final tender_display_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4744
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$ProtoAdapter_ReopenDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$ProtoAdapter_ReopenDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;",
            ">;)V"
        }
    .end annotation

    .line 4770
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;-><init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 4775
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4776
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    const-string p1, "tender_display_details"

    .line 4777
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->tender_display_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4792
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4793
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    .line 4794
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4795
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->tender_display_details:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->tender_display_details:Ljava/util/List;

    .line 4796
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 4801
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 4803
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4804
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4805
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->tender_display_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4806
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;
    .locals 2

    .line 4782
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;-><init>()V

    .line 4783
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4784
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->tender_display_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->tender_display_details:Ljava/util/List;

    .line 4785
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4743
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4813
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4814
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_0

    const-string v1, ", reopened_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4815
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->tender_display_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", tender_display_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->tender_display_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReopenDetails{"

    .line 4816
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
