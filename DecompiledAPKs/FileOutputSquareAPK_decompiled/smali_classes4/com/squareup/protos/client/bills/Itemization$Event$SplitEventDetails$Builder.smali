.class public final Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public child_itemization:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 3531
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 3532
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->child_itemization:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;
    .locals 3

    .line 3549
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->child_itemization:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3528
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    move-result-object v0

    return-object v0
.end method

.method public child_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;"
        }
    .end annotation

    .line 3542
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3543
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$Builder;->child_itemization:Ljava/util/List;

    return-object p0
.end method
