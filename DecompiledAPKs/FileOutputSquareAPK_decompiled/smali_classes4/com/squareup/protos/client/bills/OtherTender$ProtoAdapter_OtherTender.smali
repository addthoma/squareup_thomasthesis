.class final Lcom/squareup/protos/client/bills/OtherTender$ProtoAdapter_OtherTender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "OtherTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/OtherTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OtherTender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/OtherTender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 260
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/OtherTender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/OtherTender;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 283
    new-instance v0, Lcom/squareup/protos/client/bills/OtherTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/OtherTender$Builder;-><init>()V

    .line 284
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 285
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 299
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 297
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/ExternalMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ExternalMetadata;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->external_metadata(Lcom/squareup/protos/client/bills/ExternalMetadata;)Lcom/squareup/protos/client/bills/OtherTender$Builder;

    goto :goto_0

    .line 296
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/TranslatedName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/TranslatedName;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->read_only_translated_name(Lcom/squareup/protos/client/bills/TranslatedName;)Lcom/squareup/protos/client/bills/OtherTender$Builder;

    goto :goto_0

    .line 295
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->tender_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/OtherTender$Builder;

    goto :goto_0

    .line 289
    :cond_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->other_tender_type(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)Lcom/squareup/protos/client/bills/OtherTender$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 291
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 303
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 304
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->build()Lcom/squareup/protos/client/bills/OtherTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 258
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/OtherTender$ProtoAdapter_OtherTender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/OtherTender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/OtherTender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 274
    sget-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 275
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 276
    sget-object v0, Lcom/squareup/protos/client/bills/TranslatedName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 277
    sget-object v0, Lcom/squareup/protos/client/bills/ExternalMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 278
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/OtherTender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 258
    check-cast p2, Lcom/squareup/protos/client/bills/OtherTender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/OtherTender$ProtoAdapter_OtherTender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/OtherTender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/OtherTender;)I
    .locals 4

    .line 265
    sget-object v0, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/OtherTender;->other_tender_type:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/OtherTender;->tender_note:Ljava/lang/String;

    const/4 v3, 0x2

    .line 266
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/TranslatedName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/OtherTender;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    const/4 v3, 0x3

    .line 267
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ExternalMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/OtherTender;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    const/4 v3, 0x4

    .line 268
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 258
    check-cast p1, Lcom/squareup/protos/client/bills/OtherTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/OtherTender$ProtoAdapter_OtherTender;->encodedSize(Lcom/squareup/protos/client/bills/OtherTender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/OtherTender;)Lcom/squareup/protos/client/bills/OtherTender;
    .locals 2

    .line 309
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender;->newBuilder()Lcom/squareup/protos/client/bills/OtherTender$Builder;

    move-result-object p1

    .line 310
    iget-object v0, p1, Lcom/squareup/protos/client/bills/OtherTender$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/TranslatedName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/OtherTender$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/TranslatedName;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/OtherTender$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    .line 311
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/OtherTender$Builder;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/ExternalMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/OtherTender$Builder;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ExternalMetadata;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/OtherTender$Builder;->external_metadata:Lcom/squareup/protos/client/bills/ExternalMetadata;

    .line 312
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 313
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender$Builder;->build()Lcom/squareup/protos/client/bills/OtherTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 258
    check-cast p1, Lcom/squareup/protos/client/bills/OtherTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/OtherTender$ProtoAdapter_OtherTender;->redact(Lcom/squareup/protos/client/bills/OtherTender;)Lcom/squareup/protos/client/bills/OtherTender;

    move-result-object p1

    return-object p1
.end method
