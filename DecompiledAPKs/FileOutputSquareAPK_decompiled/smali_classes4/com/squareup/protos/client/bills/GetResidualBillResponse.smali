.class public final Lcom/squareup/protos/client/bills/GetResidualBillResponse;
.super Lcom/squareup/wire/Message;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ProtoAdapter_GetResidualBillResponse;,
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;,
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;,
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;,
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;,
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;,
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AMENDMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_CAN_NOT_ISSUE_EXCHANGE:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_ONLY_ISSUE_AMOUNT_BASED_REFUND:Ljava/lang/Boolean;

.field public static final DEFAULT_SOLD_FROM_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amendment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final can_not_issue_exchange:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final can_only_issue_amount_based_refund:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final refund_constraints:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.GetResidualBillResponse$BillRefundConstraint#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x9
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;",
            ">;"
        }
    .end annotation
.end field

.field public final residual_itemization:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.GetResidualBillResponse$ResidualItemization#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            ">;"
        }
    .end annotation
.end field

.field public final residual_surcharge:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.GetResidualBillResponse$ResidualSurcharge#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;",
            ">;"
        }
    .end annotation
.end field

.field public final residual_tender:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.GetResidualBillResponse$ResidualTender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            ">;"
        }
    .end annotation
.end field

.field public final residual_tip:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.GetResidualBillResponse$ResidualTip#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            ">;"
        }
    .end annotation
.end field

.field public final sold_from_unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ProtoAdapter_GetResidualBillResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ProtoAdapter_GetResidualBillResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 38
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->DEFAULT_CAN_ONLY_ISSUE_AMOUNT_BASED_REFUND:Ljava/lang/Boolean;

    .line 42
    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->DEFAULT_CAN_NOT_ISSUE_EXCHANGE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;",
            ">;)V"
        }
    .end annotation

    .line 154
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/bills/GetResidualBillResponse;-><init>(Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 162
    sget-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    const-string p1, "residual_itemization"

    .line 164
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    const-string p1, "residual_tender"

    .line 165
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    const-string p1, "residual_tip"

    .line 166
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    const-string p1, "residual_surcharge"

    .line 167
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_surcharge:Ljava/util/List;

    .line 168
    iput-object p6, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->amendment_token:Ljava/lang/String;

    .line 169
    iput-object p7, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_not_issue_exchange:Ljava/lang/Boolean;

    .line 170
    iput-object p8, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->sold_from_unit_token:Ljava/lang/String;

    const-string p1, "refund_constraints"

    .line 171
    invoke-static {p1, p9}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->refund_constraints:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 193
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 194
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    .line 195
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    .line 196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    .line 197
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    .line 198
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    .line 199
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_surcharge:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_surcharge:Ljava/util/List;

    .line 200
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->amendment_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->amendment_token:Ljava/lang/String;

    .line 201
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_not_issue_exchange:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_not_issue_exchange:Ljava/lang/Boolean;

    .line 202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->sold_from_unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->sold_from_unit_token:Ljava/lang/String;

    .line 203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->refund_constraints:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->refund_constraints:Ljava/util/List;

    .line 204
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 209
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 211
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_surcharge:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->amendment_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_not_issue_exchange:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->sold_from_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->refund_constraints:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 2

    .line 176
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;-><init>()V

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_itemization:Ljava/util/List;

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_tender:Ljava/util/List;

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_tip:Ljava/util/List;

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_surcharge:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_surcharge:Ljava/util/List;

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->amendment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->amendment_token:Ljava/lang/String;

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_not_issue_exchange:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->can_not_issue_exchange:Ljava/lang/Boolean;

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->sold_from_unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->sold_from_unit_token:Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->refund_constraints:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->refund_constraints:Ljava/util/List;

    .line 186
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", can_only_issue_amount_based_refund="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 230
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", residual_itemization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 231
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", residual_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 232
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", residual_tip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 233
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_surcharge:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", residual_surcharge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_surcharge:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 234
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->amendment_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", amendment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->amendment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_not_issue_exchange:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", can_not_issue_exchange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_not_issue_exchange:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 236
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->sold_from_unit_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", sold_from_unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->sold_from_unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->refund_constraints:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", refund_constraints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->refund_constraints:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetResidualBillResponse{"

    .line 238
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
