.class public final Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CheckBalanceRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CheckBalanceRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CheckBalanceRequest;",
        "Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public check_balance_uuid:Ljava/lang/String;

.field public entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public merchant_token:Ljava/lang/String;

.field public payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CheckBalanceRequest;
    .locals 7

    .line 180
    new-instance v6, Lcom/squareup/protos/client/bills/CheckBalanceRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->check_balance_uuid:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/CheckBalanceRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/PaymentInstrument;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->build()Lcom/squareup/protos/client/bills/CheckBalanceRequest;

    move-result-object v0

    return-object v0
.end method

.method public check_balance_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->check_balance_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CheckBalanceRequest$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    return-object p0
.end method
