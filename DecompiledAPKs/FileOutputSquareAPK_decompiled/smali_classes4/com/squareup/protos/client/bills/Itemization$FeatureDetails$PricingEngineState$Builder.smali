.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public blacklisted_discount_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5266
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 5267
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;->blacklisted_discount_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public blacklisted_discount_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;"
        }
    .end annotation

    .line 5275
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 5276
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;->blacklisted_discount_ids:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;
    .locals 3

    .line 5282
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;->blacklisted_discount_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5263
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    move-result-object v0

    return-object v0
.end method
