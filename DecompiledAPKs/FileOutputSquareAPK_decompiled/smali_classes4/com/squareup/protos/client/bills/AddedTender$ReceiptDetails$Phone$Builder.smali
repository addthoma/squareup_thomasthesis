.class public final Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public obfuscated_phone:Ljava/lang/String;

.field public phone_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 652
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;
    .locals 4

    .line 670
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;->phone_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;->obfuscated_phone:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 647
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    move-result-object v0

    return-object v0
.end method

.method public obfuscated_phone(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;
    .locals 0

    .line 664
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;->obfuscated_phone:Ljava/lang/String;

    return-object p0
.end method

.method public phone_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;
    .locals 0

    .line 656
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone$Builder;->phone_id:Ljava/lang/String;

    return-object p0
.end method
