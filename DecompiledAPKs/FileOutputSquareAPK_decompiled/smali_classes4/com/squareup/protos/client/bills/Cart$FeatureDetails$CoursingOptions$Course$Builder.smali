.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public course_id_pair:Lcom/squareup/protos/client/IdPair;

.field public event:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
            ">;"
        }
    .end annotation
.end field

.field public ordinal:Ljava/lang/Integer;

.field public source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

.field public straight_fire:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 3634
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 3635
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->event:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;
    .locals 8

    .line 3685
    new-instance v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->straight_fire:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->event:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3623
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    move-result-object v0

    return-object v0
.end method

.method public course_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;
    .locals 0

    .line 3642
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public event(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;"
        }
    .end annotation

    .line 3667
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3668
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->event:Ljava/util/List;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;
    .locals 0

    .line 3652
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public source_ticket_information(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;
    .locals 0

    .line 3679
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->source_ticket_information:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;

    return-object p0
.end method

.method public straight_fire(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;
    .locals 0

    .line 3662
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->straight_fire:Ljava/lang/Boolean;

    return-object p0
.end method
