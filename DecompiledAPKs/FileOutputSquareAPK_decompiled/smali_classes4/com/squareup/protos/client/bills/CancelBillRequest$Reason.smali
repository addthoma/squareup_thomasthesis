.class public final enum Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;
.super Ljava/lang/Enum;
.source "CancelBillRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CancelBillRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CancelBillRequest$Reason$ProtoAdapter_Reason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

.field public static final enum BACKGROUNDED_APP_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

.field public static final enum INACTIVE_APP_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

.field public static final enum OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

.field public static final enum SELLER_OR_BUYER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 239
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->UNKNOWN:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 244
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    const/4 v2, 0x1

    const-string v3, "SELLER_OR_BUYER_INITIATED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->SELLER_OR_BUYER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 249
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    const/4 v3, 0x2

    const-string v4, "BACKGROUNDED_APP_TIMEOUT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->BACKGROUNDED_APP_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 254
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    const/4 v4, 0x3

    const-string v5, "INACTIVE_APP_TIMEOUT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->INACTIVE_APP_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 259
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    const/4 v5, 0x4

    const-string v6, "APP_TERMINATION"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 264
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    const/4 v6, 0x5

    const-string v7, "OTHER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 238
    sget-object v7, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->UNKNOWN:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->SELLER_OR_BUYER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->BACKGROUNDED_APP_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->INACTIVE_APP_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->$VALUES:[Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 266
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason$ProtoAdapter_Reason;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason$ProtoAdapter_Reason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 270
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 271
    iput p3, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 284
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object p0

    .line 283
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object p0

    .line 282
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->INACTIVE_APP_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object p0

    .line 281
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->BACKGROUNDED_APP_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object p0

    .line 280
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->SELLER_OR_BUYER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object p0

    .line 279
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->UNKNOWN:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;
    .locals 1

    .line 238
    const-class v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;
    .locals 1

    .line 238
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->$VALUES:[Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 291
    iget v0, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->value:I

    return v0
.end method
