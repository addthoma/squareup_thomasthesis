.class final Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$ProtoAdapter_ResidualSurcharge;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ResidualSurcharge"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1134
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1153
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;-><init>()V

    .line 1154
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1155
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1160
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1158
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/SurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->residual_surcharge(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;

    goto :goto_0

    .line 1157
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->source_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;

    goto :goto_0

    .line 1164
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1165
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1132
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$ProtoAdapter_ResidualSurcharge;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1146
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;->source_bill_server_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1147
    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;->residual_surcharge:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1148
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1132
    check-cast p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$ProtoAdapter_ResidualSurcharge;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;)I
    .locals 4

    .line 1139
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;->source_bill_server_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;->residual_surcharge:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    const/4 v3, 0x2

    .line 1140
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1141
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1132
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$ProtoAdapter_ResidualSurcharge;->encodedSize(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;
    .locals 2

    .line 1170
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;->newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;

    move-result-object p1

    .line 1171
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->residual_surcharge:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->residual_surcharge:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->residual_surcharge:Lcom/squareup/protos/client/bills/SurchargeLineItem;

    .line 1172
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1173
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1132
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge$ProtoAdapter_ResidualSurcharge;->redact(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;

    move-result-object p1

    return-object p1
.end method
