.class public final Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public item_variation:Lcom/squareup/api/items/ItemVariation;

.field public measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1293
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;
    .locals 4

    .line 1320
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;-><init>(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/orders/model/Order$QuantityUnit;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1288
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    move-result-object v0

    return-object v0
.end method

.method public item_variation(Lcom/squareup/api/items/ItemVariation;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;
    .locals 0

    .line 1300
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->item_variation:Lcom/squareup/api/items/ItemVariation;

    return-object p0
.end method

.method public measurement_unit(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;
    .locals 0

    .line 1314
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails$Builder;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    return-object p0
.end method
