.class public final Lcom/squareup/protos/client/bills/Bill;
.super Lcom/squareup/wire/Message;
.source "Bill.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Bill$ProtoAdapter_Bill;,
        Lcom/squareup/protos/client/bills/Bill$Dates;,
        Lcom/squareup/protos/client/bills/Bill$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Bill;",
        "Lcom/squareup/protos/client/bills/Bill$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CURRENT_AMENDMENT_CLIENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SQUARE_PRODUCT:Lcom/squareup/protos/client/bills/SquareProduct;

.field private static final serialVersionUID:J


# instance fields
.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final current_amendment_client_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final dates:Lcom/squareup/protos/client/bills/Bill$Dates;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Bill$Dates#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final merchant:Lcom/squareup/protos/client/Merchant;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Merchant#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final order:Lcom/squareup/orders/model/Order;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ElectronicSignatureDetails#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$LoyaltyDetails#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final read_only_refund:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.RefundV1#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;"
        }
    .end annotation
.end field

.field public final refund:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Refund#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Refund;",
            ">;"
        }
    .end annotation
.end field

.field public final square_product:Lcom/squareup/protos/client/bills/SquareProduct;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SquareProduct#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SquareProductAttributes#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final tender:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/protos/client/bills/Bill$ProtoAdapter_Bill;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Bill$ProtoAdapter_Bill;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Bill;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->UNKNOWN_DO_NOT_SET:Lcom/squareup/protos/client/bills/SquareProduct;

    sput-object v0, Lcom/squareup/protos/client/bills/Bill;->DEFAULT_SQUARE_PRODUCT:Lcom/squareup/protos/client/bills/SquareProduct;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/Bill$Dates;Ljava/util/List;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/SquareProduct;Ljava/util/List;Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;Lcom/squareup/orders/model/Order;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/bills/Bill$Dates;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;",
            "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
            "Lcom/squareup/protos/client/bills/SquareProduct;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Refund;",
            ">;",
            "Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;",
            ")V"
        }
    .end annotation

    .line 181
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/client/bills/Bill;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/Bill$Dates;Ljava/util/List;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/SquareProduct;Ljava/util/List;Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;Lcom/squareup/orders/model/Order;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/Bill$Dates;Ljava/util/List;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/SquareProduct;Ljava/util/List;Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;Lcom/squareup/orders/model/Order;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/bills/Bill$Dates;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;",
            "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
            "Lcom/squareup/protos/client/bills/SquareProduct;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Refund;",
            ">;",
            "Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 190
    sget-object v0, Lcom/squareup/protos/client/bills/Bill;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 192
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    const-string p1, "tender"

    .line 193
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    .line 194
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 195
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    const-string p1, "read_only_refund"

    .line 196
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    .line 197
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 198
    iput-object p8, p0, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    const-string p1, "refund"

    .line 199
    invoke-static {p1, p9}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    .line 200
    iput-object p10, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    .line 201
    iput-object p11, p0, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    .line 202
    iput-object p12, p0, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    .line 203
    iput-object p13, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 229
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Bill;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 230
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Bill;

    .line 231
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Bill;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    .line 234
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 236
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    .line 237
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    .line 240
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    .line 241
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    .line 243
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 244
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 249
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 251
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Bill;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Merchant;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Bill$Dates;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SquareProductAttributes;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SquareProduct;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 265
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Bill$Builder;
    .locals 2

    .line 208
    new-instance v0, Lcom/squareup/protos/client/bills/Bill$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Bill$Builder;-><init>()V

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->tender:Ljava/util/List;

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_refund:Ljava/util/List;

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->refund:Ljava/util/List;

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->order:Lcom/squareup/orders/model/Order;

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->current_amendment_client_token:Ljava/lang/String;

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 222
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Bill;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Bill$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Bill;->newBuilder()Lcom/squareup/protos/client/bills/Bill$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v1, :cond_1

    const-string v1, ", merchant="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 275
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 276
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_3

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v1, :cond_4

    const-string v1, ", dates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 278
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", read_only_refund="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 279
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_6

    const-string v1, ", square_product_attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 280
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    if-eqz v1, :cond_7

    const-string v1, ", square_product="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", refund="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    if-eqz v1, :cond_9

    const-string v1, ", read_only_electronic_signature_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 283
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_a

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 284
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", current_amendment_client_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    if-eqz v1, :cond_c

    const-string v1, ", read_only_loyalty_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Bill{"

    .line 286
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
