.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
.super Lcom/squareup/wire/Message;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$ProtoAdapter_ModifierOptionLineItem;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HIDE_FROM_CUSTOMER:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ModifierOptionLineItem$Amounts#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ModifierOptionLineItem$FeatureDetails#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final hide_from_customer:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ModifierOptionLineItem$DisplayDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ModifierOptionLineItem$BackingDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$ProtoAdapter_ModifierOptionLineItem;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$ProtoAdapter_ModifierOptionLineItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->DEFAULT_HIDE_FROM_CUSTOMER:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)V
    .locals 8

    .line 86
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;Ljava/lang/Boolean;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;Lokio/ByteString;)V
    .locals 1

    .line 92
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 93
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 94
    iput-object p2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    .line 95
    iput-object p3, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    .line 96
    iput-object p4, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    .line 97
    iput-object p5, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    .line 98
    iput-object p6, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 117
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 118
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    .line 124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    .line 125
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 130
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 132
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 139
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;
    .locals 2

    .line 103
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->hide_from_customer:Ljava/lang/Boolean;

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", modifier_option_line_item_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->modifier_option_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    if-eqz v1, :cond_1

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->amounts:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    if-eqz v1, :cond_2

    const-string v1, ", read_only_display_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    if-eqz v1, :cond_3

    const-string v1, ", write_only_backing_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 151
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", hide_from_customer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->hide_from_customer:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    if-eqz v1, :cond_5

    const-string v1, ", feature_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem;->feature_details:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ModifierOptionLineItem{"

    .line 153
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
