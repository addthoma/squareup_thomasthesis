.class public final Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
.super Lcom/squareup/wire/Message;
.source "RoundingAdjustmentLineItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$ProtoAdapter_RoundingAdjustmentLineItem;,
        Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;,
        Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
        "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final DEFAULT_LOCALIZED_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.RoundingAdjustmentLineItem$Amounts#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final calculation_phase:Lcom/squareup/api/items/CalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.CalculationPhase#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final localized_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$ProtoAdapter_RoundingAdjustmentLineItem;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$ProtoAdapter_RoundingAdjustmentLineItem;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->DISCOUNT_PERCENTAGE_PHASE:Lcom/squareup/api/items/CalculationPhase;

    sput-object v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)V
    .locals 6

    .line 65
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;-><init>(Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/CalculationPhase;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 73
    iput-object p2, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    .line 74
    iput-object p3, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 75
    iput-object p4, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 92
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    .line 98
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 103
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/CalculationPhase;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 110
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->localized_name:Ljava/lang/String;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->newBuilder()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_0

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    if-eqz v1, :cond_1

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_2

    const-string v1, ", rounding_adjustment_line_item_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", localized_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RoundingAdjustmentLineItem{"

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
