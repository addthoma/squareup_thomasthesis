.class public final Lcom/squareup/protos/client/bills/CompleteTender$Amounts;
.super Lcom/squareup/wire/Message;
.source "CompleteTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Amounts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CompleteTender$Amounts$ProtoAdapter_Amounts;,
        Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CompleteTender$Amounts;",
        "Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CompleteTender$Amounts;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TIP_PERCENTAGE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final tip_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final tip_percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final total_charged_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 164
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$ProtoAdapter_Amounts;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$ProtoAdapter_Amounts;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 1

    .line 202
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 207
    sget-object v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 209
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    .line 210
    iput-object p3, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 226
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 227
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    .line 228
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    .line 231
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 236
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 238
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 242
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;
    .locals 2

    .line 215
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;-><init>()V

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money:Lcom/squareup/protos/common/Money;

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_percentage:Ljava/lang/String;

    .line 219
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", total_charged_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", tip_percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Amounts{"

    .line 253
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
