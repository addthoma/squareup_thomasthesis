.class public final Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$S3Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$S3Card;",
        "Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_reader_data:Lokio/ByteString;

.field public encrypted_swipe_data:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2445
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardData$S3Card;
    .locals 4

    .line 2466
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$S3Card;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;->encrypted_swipe_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/CardData$S3Card;-><init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2440
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$S3Card;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;
    .locals 0

    .line 2452
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    return-object p0
.end method

.method public encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;
    .locals 0

    .line 2460
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$S3Card$Builder;->encrypted_swipe_data:Lokio/ByteString;

    return-object p0
.end method
