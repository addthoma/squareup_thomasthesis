.class public final Lcom/squareup/protos/client/bills/AddTendersRequest;
.super Lcom/squareup/wire/Message;
.source "AddTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddTendersRequest$ProtoAdapter_AddTendersRequest;,
        Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;,
        Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;,
        Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/AddTendersRequest;",
        "Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDER_GROUP_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final add_tender:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddTender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTender;",
            ">;"
        }
    .end annotation
.end field

.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SquareProductAttributes#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final tender_group_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final tender_info:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddTendersRequest$TenderInfo#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddTendersRequest$TenderInfoLoyaltyOptions#ADAPTER"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersRequest$ProtoAdapter_AddTendersRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$ProtoAdapter_AddTendersRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 118
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/AddTendersRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;",
            ">;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
            "Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 125
    sget-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    .line 127
    iput-object p2, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const-string p1, "add_tender"

    .line 128
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    const-string p1, "tender_info"

    .line 129
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    .line 130
    iput-object p5, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 131
    iput-object p6, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 132
    iput-object p7, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    .line 133
    iput-object p8, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 154
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 155
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersRequest;

    .line 156
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    .line 159
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    .line 160
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    .line 164
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 169
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SquareProductAttributes;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 180
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;
    .locals 2

    .line 138
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender:Ljava/util/List;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info:Ljava/util/List;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_group_token:Ljava/lang/String;

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", add_tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", tender_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_4

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_5

    const-string v1, ", square_product_attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    if-eqz v1, :cond_6

    const-string v1, ", tender_info_loyalty_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", tender_group_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AddTendersRequest{"

    .line 196
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
