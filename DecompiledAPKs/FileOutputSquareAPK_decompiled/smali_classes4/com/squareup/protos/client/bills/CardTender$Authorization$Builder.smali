.class public final Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Authorization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardTender$Authorization;",
        "Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public authorization_code:Ljava/lang/String;

.field public authorized_money:Lcom/squareup/protos/common/Money;

.field public decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

.field public signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

.field public signature_optional_max_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public authorization_code(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;
    .locals 0

    .line 1140
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->authorization_code:Ljava/lang/String;

    return-object p0
.end method

.method public authorized_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;
    .locals 0

    .line 1151
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->authorized_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CardTender$Authorization;
    .locals 8

    .line 1175
    new-instance v7, Lcom/squareup/protos/client/bills/CardTender$Authorization;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->authorization_code:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->authorized_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/CardTender$Authorization;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1122
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Authorization;

    move-result-object v0

    return-object v0
.end method

.method public decline_reason(Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;)Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;
    .locals 0

    .line 1164
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    return-object p0
.end method

.method public signature_behavior(Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;)Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;
    .locals 0

    .line 1169
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->signature_behavior:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    return-object p0
.end method

.method public signature_optional_max_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;
    .locals 0

    .line 1159
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$Builder;->signature_optional_max_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
