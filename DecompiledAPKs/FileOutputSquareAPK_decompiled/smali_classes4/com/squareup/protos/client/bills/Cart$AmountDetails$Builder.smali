.class public final Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$AmountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$AmountDetails;",
        "Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public net:Lcom/squareup/protos/client/bills/Cart$Amounts;

.field public return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

.field public sale:Lcom/squareup/protos/client/bills/Cart$Amounts;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8498
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$AmountDetails;
    .locals 5

    .line 8535
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;-><init>(Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8491
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    move-result-object v0

    return-object v0
.end method

.method public net(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;
    .locals 0

    .line 8529
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    return-object p0
.end method

.method public return_(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;
    .locals 0

    .line 8507
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    return-object p0
.end method

.method public sale(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;
    .locals 0

    .line 8516
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    return-object p0
.end method
