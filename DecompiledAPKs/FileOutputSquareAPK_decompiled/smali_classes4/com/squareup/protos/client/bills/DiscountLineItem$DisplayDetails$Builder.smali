.class public final Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

.field public cogs_object_id:Ljava/lang/String;

.field public coupon_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 921
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 922
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->coupon_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public application_method(Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;
    .locals 0

    .line 951
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;
    .locals 8

    .line 967
    new-instance v7, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->percentage:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->coupon_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;Ljava/util/List;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 910
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public cogs_object_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;
    .locals 0

    .line 942
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->cogs_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public coupon_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;"
        }
    .end annotation

    .line 960
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 961
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->coupon_ids:Ljava/util/List;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;
    .locals 0

    .line 926
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;
    .locals 0

    .line 934
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method
