.class final Lcom/squareup/protos/client/bills/GetBillsRequest$ProtoAdapter_GetBillsRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetBillsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetBillsRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/GetBillsRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 785
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/GetBillsRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetBillsRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 810
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;-><init>()V

    .line 811
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 812
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 820
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 818
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query_params(Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;

    goto :goto_0

    .line 817
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;

    goto :goto_0

    .line 816
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;

    goto :goto_0

    .line 815
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;

    goto :goto_0

    .line 814
    :cond_4
    sget-object v3, Lcom/squareup/protos/client/bills/GetBillsRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query(Lcom/squareup/protos/client/bills/GetBillsRequest$Query;)Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;

    goto :goto_0

    .line 824
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 825
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 783
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$ProtoAdapter_GetBillsRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetBillsRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetBillsRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 800
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillsRequest;->query:Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 801
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillsRequest;->limit:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 802
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillsRequest;->pagination_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 803
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillsRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 804
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillsRequest;->query_params:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 805
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/GetBillsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 783
    check-cast p2, Lcom/squareup/protos/client/bills/GetBillsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/GetBillsRequest$ProtoAdapter_GetBillsRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetBillsRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/GetBillsRequest;)I
    .locals 4

    .line 790
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillsRequest;->query:Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillsRequest;->limit:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 791
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillsRequest;->pagination_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 792
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillsRequest;->merchant_token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 793
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillsRequest;->query_params:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    const/4 v3, 0x5

    .line 794
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 795
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 783
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$ProtoAdapter_GetBillsRequest;->encodedSize(Lcom/squareup/protos/client/bills/GetBillsRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/GetBillsRequest;)Lcom/squareup/protos/client/bills/GetBillsRequest;
    .locals 2

    .line 830
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest;->newBuilder()Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;

    move-result-object p1

    .line 831
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    .line 832
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query_params:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query_params:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->query_params:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    .line 833
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 834
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 783
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$ProtoAdapter_GetBillsRequest;->redact(Lcom/squareup/protos/client/bills/GetBillsRequest;)Lcom/squareup/protos/client/bills/GetBillsRequest;

    move-result-object p1

    return-object p1
.end method
