.class public final Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SurchargeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 437
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;
    .locals 0

    .line 441
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;
    .locals 3

    .line 447
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 434
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    move-result-object v0

    return-object v0
.end method
