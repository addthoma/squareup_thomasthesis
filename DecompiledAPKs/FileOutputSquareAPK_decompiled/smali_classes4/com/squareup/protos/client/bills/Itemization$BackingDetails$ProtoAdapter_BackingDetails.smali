.class final Lcom/squareup/protos/client/bills/Itemization$BackingDetails$ProtoAdapter_BackingDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BackingDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2330
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2355
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;-><init>()V

    .line 2356
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2357
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 2365
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2363
    :cond_0
    sget-object v3, Lcom/squareup/api/items/Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Menu;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->menu(Lcom/squareup/api/items/Menu;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    goto :goto_0

    .line 2362
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    goto :goto_0

    .line 2361
    :cond_2
    sget-object v3, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    goto :goto_0

    .line 2360
    :cond_3
    sget-object v3, Lcom/squareup/api/items/ItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/ItemImage;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item_image(Lcom/squareup/api/items/ItemImage;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    goto :goto_0

    .line 2359
    :cond_4
    sget-object v3, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Item;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item(Lcom/squareup/api/items/Item;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    goto :goto_0

    .line 2369
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2370
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2328
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$ProtoAdapter_BackingDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2345
    sget-object v0, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2346
    sget-object v0, Lcom/squareup/api/items/ItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2347
    sget-object v0, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2348
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2349
    sget-object v0, Lcom/squareup/api/items/Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2350
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2328
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$ProtoAdapter_BackingDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)I
    .locals 4

    .line 2335
    sget-object v0, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/ItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    const/4 v3, 0x2

    .line 2336
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->category:Lcom/squareup/api/items/MenuCategory;

    const/4 v3, 0x3

    .line 2337
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    const/4 v3, 0x4

    .line 2338
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->menu:Lcom/squareup/api/items/Menu;

    const/4 v3, 0x5

    .line 2339
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2340
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2328
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$ProtoAdapter_BackingDetails;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
    .locals 2

    .line 2375
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;

    move-result-object p1

    .line 2376
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item:Lcom/squareup/api/items/Item;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/items/Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item:Lcom/squareup/api/items/Item;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item:Lcom/squareup/api/items/Item;

    .line 2377
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/api/items/ItemImage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemImage;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    .line 2378
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/api/items/MenuCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MenuCategory;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    .line 2379
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    .line 2380
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->menu:Lcom/squareup/api/items/Menu;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/api/items/Menu;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->menu:Lcom/squareup/api/items/Menu;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Menu;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->menu:Lcom/squareup/api/items/Menu;

    .line 2381
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2382
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2328
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$ProtoAdapter_BackingDetails;->redact(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object p1

    return-object p1
.end method
