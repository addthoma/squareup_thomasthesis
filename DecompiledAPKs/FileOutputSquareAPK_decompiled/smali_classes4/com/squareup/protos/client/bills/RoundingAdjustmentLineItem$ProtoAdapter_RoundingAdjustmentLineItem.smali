.class final Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$ProtoAdapter_RoundingAdjustmentLineItem;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RoundingAdjustmentLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RoundingAdjustmentLineItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 289
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 312
    new-instance v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;-><init>()V

    .line 313
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 314
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 328
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 326
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->localized_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    goto :goto_0

    .line 325
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    goto :goto_0

    .line 324
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    goto :goto_0

    .line 318
    :cond_3
    :try_start_0
    sget-object v4, Lcom/squareup/api/items/CalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 320
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 332
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 333
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->build()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 287
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$ProtoAdapter_RoundingAdjustmentLineItem;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 303
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 304
    sget-object v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 305
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 306
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 307
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 287
    check-cast p2, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$ProtoAdapter_RoundingAdjustmentLineItem;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)I
    .locals 4

    .line 294
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    const/4 v3, 0x2

    .line 295
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x3

    .line 296
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->localized_name:Ljava/lang/String;

    const/4 v3, 0x4

    .line 297
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 287
    check-cast p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$ProtoAdapter_RoundingAdjustmentLineItem;->encodedSize(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .locals 2

    .line 338
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->newBuilder()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    move-result-object p1

    .line 339
    iget-object v0, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    .line 340
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 341
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 342
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->build()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 287
    check-cast p1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$ProtoAdapter_RoundingAdjustmentLineItem;->redact(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object p1

    return-object p1
.end method
