.class public final Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReturnTipLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ReturnTipLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
        "Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public source_tip_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

.field public tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ReturnTipLineItem;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->source_tip_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/ReturnTipLineItem;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/TipLineItem;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    move-result-object v0

    return-object v0
.end method

.method public source_tip_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->source_tip_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public tip_line_item(Lcom/squareup/protos/client/bills/TipLineItem;)Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ReturnTipLineItem$Builder;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    return-object p0
.end method
