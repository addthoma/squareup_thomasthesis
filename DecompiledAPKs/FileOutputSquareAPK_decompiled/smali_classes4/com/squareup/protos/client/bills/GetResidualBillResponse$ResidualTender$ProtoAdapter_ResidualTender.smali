.class final Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$ProtoAdapter_ResidualTender;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ResidualTender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 816
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 837
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;-><init>()V

    .line 838
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 839
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 845
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 843
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;

    goto :goto_0

    .line 842
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->source_tender_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;

    goto :goto_0

    .line 841
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->source_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;

    goto :goto_0

    .line 849
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 850
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 814
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$ProtoAdapter_ResidualTender;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 829
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->source_bill_server_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 830
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->source_tender_server_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 831
    sget-object v0, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 832
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 814
    check-cast p2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$ProtoAdapter_ResidualTender;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;)I
    .locals 4

    .line 821
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->source_bill_server_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->source_tender_server_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 822
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    const/4 v3, 0x3

    .line 823
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 824
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 814
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$ProtoAdapter_ResidualTender;->encodedSize(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;
    .locals 2

    .line 855
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->newBuilder()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;

    move-result-object p1

    .line 856
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 857
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 858
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 814
    check-cast p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender$ProtoAdapter_ResidualTender;->redact(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    move-result-object p1

    return-object p1
.end method
