.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public display_name:Ljava/lang/String;

.field public email:Ljava/lang/String;

.field public last_visit:Lcom/squareup/protos/client/ISO8601Date;

.field public loyalty_account_phone_token:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public phone_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2963
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;
    .locals 9

    .line 3018
    new-instance v8, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->display_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->email:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_number:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_token:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->loyalty_account_phone_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2950
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
    .locals 0

    .line 2972
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
    .locals 0

    .line 2988
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public last_visit(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
    .locals 0

    .line 2980
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->last_visit:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public loyalty_account_phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
    .locals 0

    .line 3012
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->loyalty_account_phone_token:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
    .locals 0

    .line 2996
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;
    .locals 0

    .line 3004
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$DisplayDetails$Builder;->phone_token:Ljava/lang/String;

    return-object p0
.end method
