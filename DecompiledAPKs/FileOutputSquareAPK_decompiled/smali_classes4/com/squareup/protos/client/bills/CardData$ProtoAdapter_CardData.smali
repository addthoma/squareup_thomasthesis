.class final Lcom/squareup/protos/client/bills/CardData$ProtoAdapter_CardData;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CardData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2802
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardData;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2847
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 2848
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2849
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 2874
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2872
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$S3Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$S3Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->s3_card(Lcom/squareup/protos/client/bills/CardData$S3Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2871
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->server_completed(Lcom/squareup/protos/client/bills/CardData$ServerCompleted;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2870
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$T2Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$T2Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card(Lcom/squareup/protos/client/bills/CardData$T2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2869
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$MCRCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$MCRCard;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->mcr_card(Lcom/squareup/protos/client/bills/CardData$MCRCard;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2868
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$X2Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$X2Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card(Lcom/squareup/protos/client/bills/CardData$X2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2867
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$R12Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$R12Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card(Lcom/squareup/protos/client/bills/CardData$R12Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2866
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$A10Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$A10Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->a10_card(Lcom/squareup/protos/client/bills/CardData$A10Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2865
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->encrypted_keyed_card(Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2864
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$R6Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$R6Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card(Lcom/squareup/protos/client/bills/CardData$R6Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto :goto_0

    .line 2863
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$R4Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$R4Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card(Lcom/squareup/protos/client/bills/CardData$R4Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto/16 :goto_0

    .line 2862
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$S1Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$S1Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->s1_card(Lcom/squareup/protos/client/bills/CardData$S1Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto/16 :goto_0

    .line 2861
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$O1Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$O1Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card(Lcom/squareup/protos/client/bills/CardData$O1Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto/16 :goto_0

    .line 2860
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->unencrypted_card(Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto/16 :goto_0

    .line 2859
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;

    goto/16 :goto_0

    .line 2853
    :pswitch_e
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 2855
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/CardData$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 2878
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2879
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2800
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$ProtoAdapter_CardData;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2827
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2828
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2829
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2830
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$O1Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2831
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$S1Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2832
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$R4Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2833
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$R6Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2834
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2835
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2836
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$R12Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2837
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$X2Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2838
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$MCRCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2839
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$T2Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2840
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2841
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$S3Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2842
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2800
    check-cast p2, Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardData$ProtoAdapter_CardData;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardData;)I
    .locals 4

    .line 2807
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    const/4 v3, 0x2

    .line 2808
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    const/4 v3, 0x3

    .line 2809
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$O1Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    const/4 v3, 0x4

    .line 2810
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$S1Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    const/4 v3, 0x5

    .line 2811
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$R4Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    const/4 v3, 0x6

    .line 2812
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$R6Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    const/4 v3, 0x7

    .line 2813
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    const/16 v3, 0x8

    .line 2814
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$A10Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    const/16 v3, 0x9

    .line 2815
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$R12Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    const/16 v3, 0xa

    .line 2816
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$X2Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    const/16 v3, 0xb

    .line 2817
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$MCRCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    const/16 v3, 0xc

    .line 2818
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$T2Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    const/16 v3, 0xd

    .line 2819
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    const/16 v3, 0xe

    .line 2820
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$S3Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    const/16 v3, 0xf

    .line 2821
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2822
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2800
    check-cast p1, Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$ProtoAdapter_CardData;->encodedSize(Lcom/squareup/protos/client/bills/CardData;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/CardData;
    .locals 2

    .line 2884
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData;->newBuilder()Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    .line 2885
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    .line 2886
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    .line 2887
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$O1Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$O1Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    .line 2888
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$S1Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$S1Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    .line 2889
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$R4Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$R4Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    .line 2890
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$R6Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$R6Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    .line 2891
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    .line 2892
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$A10Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    .line 2893
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$R12Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$R12Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    .line 2894
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$X2Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$X2Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    .line 2895
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$MCRCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$MCRCard;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    .line 2896
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$T2Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$T2Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    .line 2897
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    .line 2898
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$S3Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData$S3Card;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$Builder;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    .line 2899
    :cond_d
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2900
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2800
    check-cast p1, Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$ProtoAdapter_CardData;->redact(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method
