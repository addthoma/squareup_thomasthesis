.class public final Lcom/squareup/protos/client/bills/CaptureTendersRequest;
.super Lcom/squareup/wire/Message;
.source "CaptureTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CaptureTendersRequest$ProtoAdapter_CaptureTendersRequest;,
        Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
        "Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TENDER_GROUP_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final dates:Lcom/squareup/protos/client/bills/Bill$Dates;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Bill$Dates#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final merchant:Lcom/squareup/protos/client/Merchant;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Merchant#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SquareProductAttributes#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final tender_group_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final tenders:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CompleteTender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$ProtoAdapter_CaptureTendersRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$ProtoAdapter_CaptureTendersRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Bill$Dates;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;",
            "Lcom/squareup/protos/client/bills/Bill$Dates;",
            "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 103
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Bill$Dates;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lcom/squareup/protos/client/bills/Bill$Dates;Lcom/squareup/protos/client/bills/SquareProductAttributes;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;",
            "Lcom/squareup/protos/client/bills/Bill$Dates;",
            "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 109
    sget-object v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 111
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    const-string p1, "tenders"

    .line 112
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    .line 113
    iput-object p4, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 114
    iput-object p5, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 115
    iput-object p6, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 116
    iput-object p7, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 137
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    .line 141
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    .line 145
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 150
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Merchant;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Bill$Dates;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SquareProductAttributes;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 160
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;
    .locals 2

    .line 121
    new-instance v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tenders:Ljava/util/List;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tender_group_token:Ljava/lang/String;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v1, :cond_1

    const-string v1, ", merchant="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", tenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v1, :cond_3

    const-string v1, ", dates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_4

    const-string v1, ", square_product_attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_5

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", tender_group_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tender_group_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CaptureTendersRequest{"

    .line 175
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
