.class public final Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;",
        "Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1554
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
    .locals 3

    .line 1564
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;-><init>(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1551
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object v0

    return-object v0
.end method

.method public receipt_display_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;)Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;
    .locals 0

    .line 1558
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$Builder;->receipt_display_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails$ReceiptDisplayDetails;

    return-object p0
.end method
