.class public final Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public itemization:Lcom/squareup/protos/client/bills/Itemization;

.field public source_bill_server_token:Ljava/lang/String;

.field public source_itemization_quantity:Ljava/lang/String;

.field public source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 555
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;
    .locals 7

    .line 608
    new-instance v6, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_bill_server_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_itemization_quantity:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 546
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    move-result-object v0

    return-object v0
.end method

.method public itemization(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;
    .locals 0

    .line 602
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    return-object p0
.end method

.method public source_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;
    .locals 0

    .line 562
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_bill_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public source_itemization_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;
    .locals 0

    .line 586
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_itemization_quantity:Ljava/lang/String;

    return-object p0
.end method

.method public source_itemization_token_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;
    .locals 0

    .line 577
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization$Builder;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
