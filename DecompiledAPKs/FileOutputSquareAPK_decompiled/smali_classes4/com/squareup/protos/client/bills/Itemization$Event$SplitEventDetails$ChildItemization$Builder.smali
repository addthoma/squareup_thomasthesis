.class public final Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;",
        "Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

.field public ticket_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3625
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;
    .locals 4

    .line 3640
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3620
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    move-result-object v0

    return-object v0
.end method

.method public child_itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;
    .locals 0

    .line 3634
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;
    .locals 0

    .line 3629
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization$Builder;->ticket_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
