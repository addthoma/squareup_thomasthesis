.class public final Lcom/squareup/protos/client/bills/AddTender;
.super Lcom/squareup/wire/Message;
.source "AddTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddTender$ProtoAdapter_AddTender;,
        Lcom/squareup/protos/client/bills/AddTender$Logging;,
        Lcom/squareup/protos/client/bills/AddTender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/AddTender;",
        "Lcom/squareup/protos/client/bills/AddTender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddTender;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_VARIABLE_CAPTURE_POSSIBLE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final logging:Lcom/squareup/protos/client/bills/AddTender$Logging;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddTender$Logging#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.PaymentInstrument#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SquareProductAttributes#ADAPTER"
        tag = 0x5
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.StoreAndForwardPaymentInstrument#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final tender:Lcom/squareup/protos/client/bills/Tender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final variable_capture_possible:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bills/AddTender$ProtoAdapter_AddTender;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTender$ProtoAdapter_AddTender;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/AddTender;->DEFAULT_VARIABLE_CAPTURE_POSSIBLE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/PaymentInstrument;Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;Lcom/squareup/protos/client/bills/AddTender$Logging;Lcom/squareup/protos/client/bills/SquareProductAttributes;Ljava/lang/Boolean;)V
    .locals 8

    .line 95
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/AddTender;-><init>(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/PaymentInstrument;Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;Lcom/squareup/protos/client/bills/AddTender$Logging;Lcom/squareup/protos/client/bills/SquareProductAttributes;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/PaymentInstrument;Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;Lcom/squareup/protos/client/bills/AddTender$Logging;Lcom/squareup/protos/client/bills/SquareProductAttributes;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 102
    sget-object v0, Lcom/squareup/protos/client/bills/AddTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 104
    iput-object p2, p0, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 105
    iput-object p3, p0, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    .line 106
    iput-object p4, p0, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    .line 107
    iput-object p5, p0, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 108
    iput-object p6, p0, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 127
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/AddTender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 128
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/AddTender;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    .line 135
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 140
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/PaymentInstrument;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddTender$Logging;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/SquareProductAttributes;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 149
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/AddTender$Builder;
    .locals 2

    .line 113
    new-instance v0, Lcom/squareup/protos/client/bills/AddTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTender$Builder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTender$Builder;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTender$Builder;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTender$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddTender$Builder;->variable_capture_possible:Ljava/lang/Boolean;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTender;->newBuilder()Lcom/squareup/protos/client/bills/AddTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v1, :cond_0

    const-string v1, ", tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    if-eqz v1, :cond_1

    const-string v1, ", payment_instrument="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    if-eqz v1, :cond_2

    const-string v1, ", store_and_forward_payment_instrument="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    if-eqz v1, :cond_3

    const-string v1, ", logging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 161
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v1, :cond_4

    const-string v1, ", square_product_attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", variable_capture_possible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender;->variable_capture_possible:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AddTender{"

    .line 163
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
