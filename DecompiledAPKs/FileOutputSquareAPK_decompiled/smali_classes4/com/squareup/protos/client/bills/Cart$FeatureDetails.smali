.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeatureDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ProtoAdapter_FeatureDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$SourceTicketInformation;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$AppointmentsDetails#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$AuthAndDelayedCapture#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final available_instrument_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$InstrumentDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$BillPrintState#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$BuyerInfo#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$CoursingOptions#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final cover_count_change_log:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$CoverCountEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$Invoice#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$KitchenPrinting#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final lifecycle_event:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$LifecycleEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OpenTicket#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$Order#ADAPTER"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$OrderDetails#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$PricingEngineState#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$ReopenDetails#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$RSTOrderMetadata#ADAPTER"
        tag = 0xf
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$Seating#ADAPTER"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 893
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ProtoAdapter_FeatureDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ProtoAdapter_FeatureDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;Lokio/ByteString;)V
    .locals 1

    .line 1034
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1035
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    .line 1036
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    .line 1037
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    .line 1038
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    .line 1039
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 1040
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    .line 1041
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->available_instrument_details:Ljava/util/List;

    const-string v0, "available_instrument_details"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    .line 1042
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    .line 1043
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    .line 1044
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    .line 1045
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    .line 1046
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->cover_count_change_log:Ljava/util/List;

    const-string v0, "cover_count_change_log"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    .line 1047
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    .line 1048
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    .line 1049
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->lifecycle_event:Ljava/util/List;

    const-string v0, "lifecycle_event"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    .line 1050
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    .line 1051
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1081
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1082
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    .line 1083
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    .line 1084
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    .line 1085
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    .line 1086
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    .line 1087
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 1088
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    .line 1089
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    .line 1090
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    .line 1091
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    .line 1092
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    .line 1093
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    .line 1094
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    .line 1095
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    .line 1096
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    .line 1097
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    .line 1098
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    .line 1099
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    .line 1100
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1105
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_e

    .line 1107
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1108
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1109
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1110
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1111
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1112
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1113
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1114
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1115
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1116
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1117
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1118
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1119
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1120
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1121
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1122
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1123
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1124
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;->hashCode()I

    move-result v2

    :cond_d
    add-int/2addr v0, v2

    .line 1125
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_e
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 2

    .line 1056
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;-><init>()V

    .line 1057
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    .line 1058
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    .line 1059
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    .line 1060
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    .line 1061
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 1062
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    .line 1063
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->available_instrument_details:Ljava/util/List;

    .line 1064
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    .line 1065
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    .line 1066
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    .line 1067
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    .line 1068
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->cover_count_change_log:Ljava/util/List;

    .line 1069
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    .line 1070
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    .line 1071
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->lifecycle_event:Ljava/util/List;

    .line 1072
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    .line 1073
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    .line 1074
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 892
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1133
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    if-eqz v1, :cond_0

    const-string v1, ", order_deprecated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1134
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    if-eqz v1, :cond_1

    const-string v1, ", open_ticket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1135
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    if-eqz v1, :cond_2

    const-string v1, ", invoice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1136
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    if-eqz v1, :cond_3

    const-string v1, ", kitchen_printing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1137
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    if-eqz v1, :cond_4

    const-string v1, ", buyer_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1138
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    if-eqz v1, :cond_5

    const-string v1, ", appointments_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1139
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", available_instrument_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1140
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    if-eqz v1, :cond_7

    const-string v1, ", coursing_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1141
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz v1, :cond_8

    const-string v1, ", seating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1142
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    if-eqz v1, :cond_9

    const-string v1, ", bill_print_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1143
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    if-eqz v1, :cond_a

    const-string v1, ", reopen_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1144
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", cover_count_change_log="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->cover_count_change_log:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1145
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    if-eqz v1, :cond_c

    const-string v1, ", auth_and_delayed_capture="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1146
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    if-eqz v1, :cond_d

    const-string v1, ", rst_order_metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1147
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, ", lifecycle_event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->lifecycle_event:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1148
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    if-eqz v1, :cond_f

    const-string v1, ", order_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1149
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    if-eqz v1, :cond_10

    const-string v1, ", pricing_engine_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_10
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeatureDetails{"

    .line 1150
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
