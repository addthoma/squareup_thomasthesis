.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OwnershipTransferDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$ProtoAdapter_OwnershipTransferDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final transfer_from_employee:Lcom/squareup/protos/client/Employee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Employee#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final transfer_to_employee:Lcom/squareup/protos/client/Employee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Employee#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6346
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$ProtoAdapter_OwnershipTransferDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$ProtoAdapter_OwnershipTransferDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Employee;Lcom/squareup/protos/client/Employee;)V
    .locals 1

    .line 6370
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;-><init>(Lcom/squareup/protos/client/Employee;Lcom/squareup/protos/client/Employee;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Employee;Lcom/squareup/protos/client/Employee;Lokio/ByteString;)V
    .locals 1

    .line 6375
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 6376
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    .line 6377
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6392
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6393
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    .line 6394
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    .line 6395
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    .line 6396
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6401
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 6403
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6404
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Employee;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6405
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Employee;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 6406
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;
    .locals 2

    .line 6382
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;-><init>()V

    .line 6383
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    .line 6384
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    .line 6385
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 6345
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6414
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    if-eqz v1, :cond_0

    const-string v1, ", transfer_from_employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6415
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    if-eqz v1, :cond_1

    const-string v1, ", transfer_to_employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OwnershipTransferDetails{"

    .line 6416
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
