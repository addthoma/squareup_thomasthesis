.class final Lcom/squareup/protos/client/bills/IssueRefundsRequest$ProtoAdapter_IssueRefundsRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "IssueRefundsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/IssueRefundsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_IssueRefundsRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/IssueRefundsRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 206
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/IssueRefundsRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 231
    new-instance v0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;-><init>()V

    .line 232
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 233
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 241
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 239
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/ReturnCartDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ReturnCartDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->return_cart_details(Lcom/squareup/protos/client/bills/ReturnCartDetails;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    goto :goto_0

    .line 238
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->refund_request:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/RefundRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 237
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    goto :goto_0

    .line 236
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    goto :goto_0

    .line 235
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    goto :goto_0

    .line 245
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 246
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->build()Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 204
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$ProtoAdapter_IssueRefundsRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/IssueRefundsRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 221
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->request_uuid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 222
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->unit_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    sget-object v0, Lcom/squareup/protos/client/bills/RefundRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->refund_request:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 225
    sget-object v0, Lcom/squareup/protos/client/bills/ReturnCartDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->return_cart_details:Lcom/squareup/protos/client/bills/ReturnCartDetails;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 226
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 204
    check-cast p2, Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$ProtoAdapter_IssueRefundsRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/IssueRefundsRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)I
    .locals 4

    .line 211
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->request_uuid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->unit_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 212
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/4 v3, 0x3

    .line 213
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/RefundRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 214
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->refund_request:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ReturnCartDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->return_cart_details:Lcom/squareup/protos/client/bills/ReturnCartDetails;

    const/4 v3, 0x5

    .line 215
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 204
    check-cast p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$ProtoAdapter_IssueRefundsRequest;->encodedSize(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)Lcom/squareup/protos/client/bills/IssueRefundsRequest;
    .locals 2

    .line 251
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest;->newBuilder()Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;

    move-result-object p1

    .line 252
    iget-object v0, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/CreatorDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 253
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->refund_request:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/RefundRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 254
    iget-object v0, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->return_cart_details:Lcom/squareup/protos/client/bills/ReturnCartDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/ReturnCartDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->return_cart_details:Lcom/squareup/protos/client/bills/ReturnCartDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ReturnCartDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->return_cart_details:Lcom/squareup/protos/client/bills/ReturnCartDetails;

    .line 255
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 256
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->build()Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 204
    check-cast p1, Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$ProtoAdapter_IssueRefundsRequest;->redact(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object p1

    return-object p1
.end method
