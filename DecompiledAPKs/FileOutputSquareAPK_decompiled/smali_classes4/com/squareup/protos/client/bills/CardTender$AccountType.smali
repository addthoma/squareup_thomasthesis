.class public final enum Lcom/squareup/protos/client/bills/CardTender$AccountType;
.super Ljava/lang/Enum;
.source "CardTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AccountType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$AccountType$ProtoAdapter_AccountType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardTender$AccountType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardTender$AccountType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$AccountType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CHECKING:Lcom/squareup/protos/client/bills/CardTender$AccountType;

.field public static final enum CREDIT:Lcom/squareup/protos/client/bills/CardTender$AccountType;

.field public static final enum EBT_CASH:Lcom/squareup/protos/client/bills/CardTender$AccountType;

.field public static final enum EBT_FOOD:Lcom/squareup/protos/client/bills/CardTender$AccountType;

.field public static final enum SAVINGS:Lcom/squareup/protos/client/bills/CardTender$AccountType;

.field public static final enum UNKNOWN_ACCOUNT_TYPE:Lcom/squareup/protos/client/bills/CardTender$AccountType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 1915
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_ACCOUNT_TYPE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CardTender$AccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->UNKNOWN_ACCOUNT_TYPE:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    .line 1917
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v2, 0x1

    const-string v3, "SAVINGS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CardTender$AccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->SAVINGS:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    .line 1919
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v3, 0x2

    const-string v4, "CHECKING"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/CardTender$AccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CHECKING:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    .line 1921
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v4, 0x3

    const-string v5, "CREDIT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/CardTender$AccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CREDIT:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    .line 1923
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v5, 0x4

    const-string v6, "EBT_CASH"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/CardTender$AccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->EBT_CASH:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    .line 1925
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v6, 0x5

    const-string v7, "EBT_FOOD"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/CardTender$AccountType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->EBT_FOOD:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardTender$AccountType;

    .line 1914
    sget-object v7, Lcom/squareup/protos/client/bills/CardTender$AccountType;->UNKNOWN_ACCOUNT_TYPE:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$AccountType;->SAVINGS:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CHECKING:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CREDIT:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$AccountType;->EBT_CASH:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$AccountType;->EBT_FOOD:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$AccountType;

    .line 1927
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$AccountType$ProtoAdapter_AccountType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$AccountType$ProtoAdapter_AccountType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1931
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1932
    iput p3, p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardTender$AccountType;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1945
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->EBT_FOOD:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0

    .line 1944
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->EBT_CASH:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0

    .line 1943
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CREDIT:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0

    .line 1942
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CHECKING:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0

    .line 1941
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->SAVINGS:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0

    .line 1940
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->UNKNOWN_ACCOUNT_TYPE:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$AccountType;
    .locals 1

    .line 1914
    const-class v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardTender$AccountType;
    .locals 1

    .line 1914
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$AccountType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardTender$AccountType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1952
    iget v0, p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->value:I

    return v0
.end method
