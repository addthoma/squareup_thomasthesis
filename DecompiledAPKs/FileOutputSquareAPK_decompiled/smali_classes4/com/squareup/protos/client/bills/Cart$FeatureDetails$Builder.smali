.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

.field public auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

.field public available_instrument_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end field

.field public bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

.field public buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

.field public coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

.field public cover_count_change_log:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
            ">;"
        }
    .end annotation
.end field

.field public invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

.field public kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

.field public lifecycle_event:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;",
            ">;"
        }
    .end annotation
.end field

.field public open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

.field public order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

.field public order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

.field public pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

.field public reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

.field public rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

.field public seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1188
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1189
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->available_instrument_details:Ljava/util/List;

    .line 1190
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->cover_count_change_log:Ljava/util/List;

    .line 1191
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->lifecycle_event:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public appointments_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1236
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    return-object p0
.end method

.method public auth_and_delayed_capture(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1283
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->auth_and_delayed_capture:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    return-object p0
.end method

.method public available_instrument_details(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1247
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1248
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->available_instrument_details:Ljava/util/List;

    return-object p0
.end method

.method public bill_print_state(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1263
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->bill_print_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
    .locals 2

    .line 1316
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1153
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object v0

    return-object v0
.end method

.method public buyer_info(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1228
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    return-object p0
.end method

.method public coursing_options(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1253
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    return-object p0
.end method

.method public cover_count_change_log(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1277
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1278
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->cover_count_change_log:Ljava/util/List;

    return-object p0
.end method

.method public invoice(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1212
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->invoice:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    return-object p0
.end method

.method public kitchen_printing(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1220
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    return-object p0
.end method

.method public lifecycle_event(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;"
        }
    .end annotation

    .line 1299
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1300
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->lifecycle_event:Ljava/util/List;

    return-object p0
.end method

.method public open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1207
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    return-object p0
.end method

.method public order_deprecated(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1199
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_deprecated:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Order;

    return-object p0
.end method

.method public order_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1305
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->order_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OrderDetails;

    return-object p0
.end method

.method public pricing_engine_state(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1310
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    return-object p0
.end method

.method public reopen_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1268
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->reopen_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    return-object p0
.end method

.method public rst_order_metadata(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1294
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->rst_order_metadata:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata;

    return-object p0
.end method

.method public seating(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;
    .locals 0

    .line 1258
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    return-object p0
.end method
