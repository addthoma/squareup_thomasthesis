.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CoverCountEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$ProtoAdapter_CoverCountEvent;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_EVENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

.field public static final DEFAULT_IS_CONFIRMED:Ljava/lang/Boolean;

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x3
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final event_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$CoverCountEvent$EventType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final is_confirmed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 5110
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$ProtoAdapter_CoverCountEvent;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$ProtoAdapter_CoverCountEvent;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5116
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->DEFAULT_EVENT_TYPE:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    const/4 v0, 0x0

    .line 5118
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->DEFAULT_COUNT:Ljava/lang/Integer;

    .line 5120
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->DEFAULT_IS_CONFIRMED:Ljava/lang/Boolean;

    .line 5122
    sput-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Integer;)V
    .locals 9

    .line 5191
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 5197
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5198
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    .line 5199
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5200
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    .line 5201
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    .line 5202
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 5203
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 5204
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5224
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5225
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    .line 5226
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    .line 5227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    .line 5229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    .line 5230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 5231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 5232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    .line 5233
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5238
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 5240
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5241
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5242
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5243
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5244
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5245
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5246
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5247
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 5248
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    .locals 2

    .line 5209
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;-><init>()V

    .line 5210
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_id:Ljava/lang/String;

    .line 5211
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    .line 5212
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->count:Ljava/lang/Integer;

    .line 5213
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->is_confirmed:Ljava/lang/Boolean;

    .line 5214
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 5215
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 5216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->ordinal:Ljava/lang/Integer;

    .line 5217
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5109
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5256
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", event_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5257
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    if-eqz v1, :cond_1

    const-string v1, ", event_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5258
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5259
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", is_confirmed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->is_confirmed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5260
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_4

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5261
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5262
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CoverCountEvent{"

    .line 5263
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
