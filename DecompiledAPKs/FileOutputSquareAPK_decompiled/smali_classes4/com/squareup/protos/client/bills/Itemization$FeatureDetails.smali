.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeatureDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$ProtoAdapter_FeatureDetails;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$FeatureDetails$AppointmentsServiceDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final course_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$FeatureDetails$InheritedItemizationDetails#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$FeatureDetails$KitchenDisplay#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$FeatureDetails$PricingEngineState#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$FeatureDetails$Seating#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3970
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$ProtoAdapter_FeatureDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$ProtoAdapter_FeatureDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;)V
    .locals 8

    .line 4022
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;-><init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;Lokio/ByteString;)V
    .locals 1

    .line 4029
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4030
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    .line 4031
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 4032
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    .line 4033
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    .line 4034
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    .line 4035
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4054
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4055
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    .line 4056
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    .line 4057
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 4058
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    .line 4059
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    .line 4060
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    .line 4061
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    .line 4062
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4067
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 4069
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4070
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4071
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4072
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4073
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4074
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4075
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 4076
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    .locals 2

    .line 4040
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;-><init>()V

    .line 4041
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    .line 4042
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 4043
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    .line 4044
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    .line 4045
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    .line 4046
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    .line 4047
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3969
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4084
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    if-eqz v1, :cond_0

    const-string v1, ", kitchen_display="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4085
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    const-string v1, ", course_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4086
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    if-eqz v1, :cond_2

    const-string v1, ", seating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4087
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    if-eqz v1, :cond_3

    const-string v1, ", appointments_service_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4088
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    if-eqz v1, :cond_4

    const-string v1, ", inherited_itemization_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4089
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    if-eqz v1, :cond_5

    const-string v1, ", pricing_engine_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeatureDetails{"

    .line 4090
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
