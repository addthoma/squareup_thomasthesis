.class public final Lcom/squareup/protos/client/bills/CardData$KeyedCard;
.super Lcom/squareup/wire/Message;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KeyedCard"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardData$KeyedCard$ProtoAdapter_KeyedCard;,
        Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;,
        Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CardData$KeyedCard;",
        "Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardData$KeyedCard;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CARD_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_CVV:Ljava/lang/String; = ""

.field public static final DEFAULT_POSTAL_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_RAW_CARD_NUMBER_UTF8:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final card_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final cvv:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData$KeyedCard$Expiry#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final postal_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final raw_card_number_utf8:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 374
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$ProtoAdapter_KeyedCard;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$ProtoAdapter_KeyedCard;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 384
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->DEFAULT_RAW_CARD_NUMBER_UTF8:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 7

    .line 434
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;-><init>(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 439
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 440
    invoke-static {p4, p5}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p6

    const/4 v0, 0x1

    if-gt p6, v0, :cond_0

    .line 443
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    .line 444
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->cvv:Ljava/lang/String;

    .line 445
    iput-object p3, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->postal_code:Ljava/lang/String;

    .line 446
    iput-object p4, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->card_number:Ljava/lang/String;

    .line 447
    iput-object p5, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->raw_card_number_utf8:Lokio/ByteString;

    return-void

    .line 441
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of card_number, raw_card_number_utf8 may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 465
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 466
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    .line 467
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    .line 468
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->cvv:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->cvv:Ljava/lang/String;

    .line 469
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->postal_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->postal_code:Ljava/lang/String;

    .line 470
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->card_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->card_number:Ljava/lang/String;

    .line 471
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->raw_card_number_utf8:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->raw_card_number_utf8:Lokio/ByteString;

    .line 472
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 477
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 479
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 480
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 481
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->cvv:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 482
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->postal_code:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 483
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->card_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 484
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->raw_card_number_utf8:Lokio/ByteString;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 485
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;
    .locals 2

    .line 452
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;-><init>()V

    .line 453
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    .line 454
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->cvv:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->cvv:Ljava/lang/String;

    .line 455
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->postal_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->postal_code:Ljava/lang/String;

    .line 456
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->card_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number:Ljava/lang/String;

    .line 457
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->raw_card_number_utf8:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->raw_card_number_utf8:Lokio/ByteString;

    .line 458
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 373
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->newBuilder()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 492
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 493
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    if-eqz v1, :cond_0

    const-string v1, ", expiry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->expiry:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 494
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->cvv:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", cvv=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 495
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->postal_code:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", postal_code=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->card_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", card_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->raw_card_number_utf8:Lokio/ByteString;

    if-eqz v1, :cond_4

    const-string v1, ", raw_card_number_utf8=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "KeyedCard{"

    .line 498
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
