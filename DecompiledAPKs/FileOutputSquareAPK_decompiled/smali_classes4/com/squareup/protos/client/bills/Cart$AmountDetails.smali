.class public final Lcom/squareup/protos/client/bills/Cart$AmountDetails;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AmountDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$AmountDetails$ProtoAdapter_AmountDetails;,
        Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$AmountDetails;",
        "Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$AmountDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final net:Lcom/squareup/protos/client/bills/Cart$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$Amounts#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final return_:Lcom/squareup/protos/client/bills/Cart$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$Amounts#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final sale:Lcom/squareup/protos/client/bills/Cart$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$Amounts#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8398
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$ProtoAdapter_AmountDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$ProtoAdapter_AmountDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;)V
    .locals 1

    .line 8438
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;-><init>(Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;Lokio/ByteString;)V
    .locals 1

    .line 8442
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 8443
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8444
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8445
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8461
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8462
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    .line 8463
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8464
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8465
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8466
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8471
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 8473
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8474
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$Amounts;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8475
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$Amounts;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8476
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$Amounts;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 8477
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;
    .locals 2

    .line 8450
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;-><init>()V

    .line 8451
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8452
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8453
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8454
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 8397
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8485
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v1, :cond_0

    const-string v1, ", return="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8486
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v1, :cond_1

    const-string v1, ", sale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8487
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v1, :cond_2

    const-string v1, ", net="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AmountDetails{"

    .line 8488
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
