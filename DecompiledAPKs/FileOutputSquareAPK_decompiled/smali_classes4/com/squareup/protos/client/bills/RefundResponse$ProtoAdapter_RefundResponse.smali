.class final Lcom/squareup/protos/client/bills/RefundResponse$ProtoAdapter_RefundResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RefundResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RefundResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RefundResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/RefundResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 99
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/RefundResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/RefundResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 116
    new-instance v0, Lcom/squareup/protos/client/bills/RefundResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RefundResponse$Builder;-><init>()V

    .line 117
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 118
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 122
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 120
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Refund;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->refund(Lcom/squareup/protos/client/bills/Refund;)Lcom/squareup/protos/client/bills/RefundResponse$Builder;

    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 127
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->build()Lcom/squareup/protos/client/bills/RefundResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 97
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RefundResponse$ProtoAdapter_RefundResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/RefundResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/RefundResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 110
    sget-object v0, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/RefundResponse;->refund:Lcom/squareup/protos/client/bills/Refund;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 111
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/RefundResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 97
    check-cast p2, Lcom/squareup/protos/client/bills/RefundResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/RefundResponse$ProtoAdapter_RefundResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/RefundResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/RefundResponse;)I
    .locals 3

    .line 104
    sget-object v0, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/RefundResponse;->refund:Lcom/squareup/protos/client/bills/Refund;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 105
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RefundResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/protos/client/bills/RefundResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RefundResponse$ProtoAdapter_RefundResponse;->encodedSize(Lcom/squareup/protos/client/bills/RefundResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/RefundResponse;)Lcom/squareup/protos/client/bills/RefundResponse;
    .locals 2

    .line 132
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RefundResponse;->newBuilder()Lcom/squareup/protos/client/bills/RefundResponse$Builder;

    move-result-object p1

    .line 133
    iget-object v0, p1, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->refund:Lcom/squareup/protos/client/bills/Refund;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->refund:Lcom/squareup/protos/client/bills/Refund;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Refund;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->refund:Lcom/squareup/protos/client/bills/Refund;

    .line 134
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 135
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RefundResponse$Builder;->build()Lcom/squareup/protos/client/bills/RefundResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/protos/client/bills/RefundResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/RefundResponse$ProtoAdapter_RefundResponse;->redact(Lcom/squareup/protos/client/bills/RefundResponse;)Lcom/squareup/protos/client/bills/RefundResponse;

    move-result-object p1

    return-object p1
.end method
