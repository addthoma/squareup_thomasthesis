.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
.super Lcom/squareup/wire/Message;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeatureDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ProtoAdapter_FeatureDetails;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final conversational_mode:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ModifierOptionLineItem$FeatureDetails$ConversationalMode#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;"
        }
    .end annotation
.end field

.field public final quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ModifierOptionLineItem$FeatureDetails$Quantities#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 735
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ProtoAdapter_FeatureDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ProtoAdapter_FeatureDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;",
            ")V"
        }
    .end annotation

    .line 756
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 761
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "conversational_mode"

    .line 762
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    .line 763
    iput-object p2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 778
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 779
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    .line 780
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    .line 781
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    .line 782
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 787
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 789
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 790
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 791
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 792
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;
    .locals 2

    .line 768
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;-><init>()V

    .line 769
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->conversational_mode:Ljava/util/List;

    .line 770
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    .line 771
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 734
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 799
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 800
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", conversational_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 801
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    if-eqz v1, :cond_1

    const-string v1, ", quantities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeatureDetails{"

    .line 802
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
