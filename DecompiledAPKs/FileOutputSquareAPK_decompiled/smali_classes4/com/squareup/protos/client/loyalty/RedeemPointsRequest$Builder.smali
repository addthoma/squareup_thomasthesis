.class public final Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RedeemPointsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public coupon_definition_token:Ljava/lang/String;

.field public idempotence_token:Ljava/lang/String;

.field public loyalty_account_token:Ljava/lang/String;

.field public phone_token:Ljava/lang/String;

.field public return_coupon_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;
    .locals 8

    .line 194
    new-instance v7, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->coupon_definition_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->return_coupon_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->idempotence_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->phone_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->loyalty_account_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    move-result-object v0

    return-object v0
.end method

.method public coupon_definition_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->coupon_definition_token:Ljava/lang/String;

    return-object p0
.end method

.method public idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->idempotence_token:Ljava/lang/String;

    return-object p0
.end method

.method public loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->loyalty_account_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->phone_token:Ljava/lang/String;

    return-object p0
.end method

.method public phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->phone_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->loyalty_account_token:Ljava/lang/String;

    return-object p0
.end method

.method public return_coupon_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->return_coupon_token:Ljava/lang/String;

    return-object p0
.end method
