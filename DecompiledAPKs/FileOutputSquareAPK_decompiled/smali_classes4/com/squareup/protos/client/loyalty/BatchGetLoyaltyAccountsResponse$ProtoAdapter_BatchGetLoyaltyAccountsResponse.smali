.class final Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BatchGetLoyaltyAccountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BatchGetLoyaltyAccountsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final loyaltyAccounts:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccount;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 173
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    .line 170
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->newMapAdapter(Lcom/squareup/wire/ProtoAdapter;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 195
    new-instance v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;-><init>()V

    .line 196
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 197
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 210
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 208
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 202
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->status(Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 204
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 199
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->loyaltyAccounts:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 214
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 215
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 188
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 190
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    check-cast p2, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)I
    .locals 4

    .line 178
    iget-object v0, p0, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const/4 v3, 0x3

    .line 179
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 180
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->errors:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;->encodedSize(Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;
    .locals 2

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;

    move-result-object p1

    .line 221
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->loyaltyAccounts:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/Map;Lcom/squareup/wire/ProtoAdapter;)V

    .line 222
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 223
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 224
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse$ProtoAdapter_BatchGetLoyaltyAccountsResponse;->redact(Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    move-result-object p1

    return-object p1
.end method
