.class public final Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendBuyerLoyaltyStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;",
        "Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public phone_token:Ljava/lang/String;

.field public request_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;
    .locals 5

    .line 153
    new-instance v0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->phone_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->request_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;

    move-result-object v0

    return-object v0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->phone_token:Ljava/lang/String;

    return-object p0
.end method

.method public request_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->request_token:Ljava/lang/String;

    return-object p0
.end method
