.class public final Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RedeemPointsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public coupon:Lcom/squareup/protos/client/coupons/Coupon;

.field public loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

.field public response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;
    .locals 7

    .line 161
    new-instance v6, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    move-result-object v0

    return-object v0
.end method

.method public coupon(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    return-object p0
.end method

.method public loyalty_status(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    return-object p0
.end method

.method public response_status(Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
