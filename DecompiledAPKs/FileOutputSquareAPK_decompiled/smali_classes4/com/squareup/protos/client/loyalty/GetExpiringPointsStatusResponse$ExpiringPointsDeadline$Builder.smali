.class public final Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetExpiringPointsStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;",
        "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public expires_at:Lcom/squareup/protos/common/time/YearMonthDay;

.field public expiring_points_amount:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 209
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;
    .locals 4

    .line 233
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;->expires_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;->expiring_points_amount:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 204
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;->build()Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline;

    move-result-object v0

    return-object v0
.end method

.method public expires_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;->expires_at:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public expiring_points_amount(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse$ExpiringPointsDeadline$Builder;->expiring_points_amount:Ljava/lang/Long;

    return-object p0
.end method
