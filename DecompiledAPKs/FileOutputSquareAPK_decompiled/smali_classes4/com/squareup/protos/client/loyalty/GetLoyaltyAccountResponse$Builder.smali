.class public final Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetLoyaltyAccountResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;"
        }
    .end annotation
.end field

.field public loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

.field public status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 125
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;
    .locals 5

    .line 156
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;->errors:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;

    move-result-object v0

    return-object v0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;"
        }
    .end annotation

    .line 149
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public loyalty_account(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;)Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-object p0
.end method
