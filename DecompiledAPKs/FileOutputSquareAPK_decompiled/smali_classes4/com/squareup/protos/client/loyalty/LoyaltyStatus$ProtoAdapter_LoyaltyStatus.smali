.class final Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ProtoAdapter_LoyaltyStatus;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LoyaltyStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LoyaltyStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 550
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 593
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;-><init>()V

    .line 594
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 595
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 619
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 617
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->balance(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto :goto_0

    .line 611
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->early_computed_reason(Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 613
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 608
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->lifetime_points(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto :goto_0

    .line 607
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_tiers:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/loyalty/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 606
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->earned_points(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto :goto_0

    .line 605
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->current_points(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto :goto_0

    .line 604
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto :goto_0

    .line 603
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->obfuscated_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto :goto_0

    .line 602
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->new_rewards_earned(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto/16 :goto_0

    .line 601
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_name(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto/16 :goto_0

    .line 600
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->earned_reward(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto/16 :goto_0

    .line 599
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->new_stars_earned(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto/16 :goto_0

    .line 598
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->previous_stars_remaining(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto/16 :goto_0

    .line 597
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->previous_stars_earned(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    goto/16 :goto_0

    .line 623
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 624
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 548
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ProtoAdapter_LoyaltyStatus;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 574
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 575
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 576
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 577
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 578
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 579
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 580
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 581
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 582
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 583
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 584
    sget-object v0, Lcom/squareup/protos/client/loyalty/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 585
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 586
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 587
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 588
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 548
    check-cast p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ProtoAdapter_LoyaltyStatus;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)I
    .locals 4

    .line 555
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 556
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 557
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 558
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    const/4 v3, 0x5

    .line 559
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    const/4 v3, 0x6

    .line 560
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    const/4 v3, 0x7

    .line 561
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    const/16 v3, 0x8

    .line 562
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    const/16 v3, 0x9

    .line 563
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    const/16 v3, 0xa

    .line 564
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 565
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    const/16 v3, 0xc

    .line 566
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    const/16 v3, 0xd

    .line 567
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    const/16 v3, 0xe

    .line 568
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 569
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 548
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ProtoAdapter_LoyaltyStatus;->encodedSize(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
    .locals 2

    .line 629
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    move-result-object p1

    .line 630
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_tiers:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/loyalty/RewardTier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 631
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 632
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 548
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ProtoAdapter_LoyaltyStatus;->redact(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    move-result-object p1

    return-object p1
.end method
