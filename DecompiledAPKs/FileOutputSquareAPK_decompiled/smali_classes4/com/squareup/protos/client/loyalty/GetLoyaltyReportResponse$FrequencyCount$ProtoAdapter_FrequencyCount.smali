.class final Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$ProtoAdapter_FrequencyCount;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetLoyaltyReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FrequencyCount"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 404
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 423
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;-><init>()V

    .line 424
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 425
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 430
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 428
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->count(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;

    goto :goto_0

    .line 427
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;

    goto :goto_0

    .line 434
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 435
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 402
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$ProtoAdapter_FrequencyCount;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 416
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 417
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 418
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 402
    check-cast p2, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$ProtoAdapter_FrequencyCount;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;)I
    .locals 4

    .line 409
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->count:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 410
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 411
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 402
    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$ProtoAdapter_FrequencyCount;->encodedSize(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;
    .locals 0

    .line 440
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;->newBuilder()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;

    move-result-object p1

    .line 441
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 442
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 402
    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount$ProtoAdapter_FrequencyCount;->redact(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;

    move-result-object p1

    return-object p1
.end method
