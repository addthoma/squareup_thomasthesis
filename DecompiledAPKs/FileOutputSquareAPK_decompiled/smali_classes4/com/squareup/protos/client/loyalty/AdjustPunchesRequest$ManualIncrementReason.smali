.class public final enum Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;
.super Ljava/lang/Enum;
.source "AdjustPunchesRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ManualIncrementReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason$ProtoAdapter_ManualIncrementReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COMPLIMENTARY_POINTS:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

.field public static final enum INCREMENT_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

.field public static final enum MISSED_POINTS_IN_TRANSACTION:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 339
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    const/4 v1, 0x0

    const-string v2, "INCREMENT_REASON_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->INCREMENT_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    .line 341
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    const/4 v2, 0x1

    const-string v3, "COMPLIMENTARY_POINTS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->COMPLIMENTARY_POINTS:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    .line 343
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    const/4 v3, 0x2

    const-string v4, "MISSED_POINTS_IN_TRANSACTION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->MISSED_POINTS_IN_TRANSACTION:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    .line 338
    sget-object v4, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->INCREMENT_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->COMPLIMENTARY_POINTS:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->MISSED_POINTS_IN_TRANSACTION:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->$VALUES:[Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    .line 345
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason$ProtoAdapter_ManualIncrementReason;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason$ProtoAdapter_ManualIncrementReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 349
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 350
    iput p3, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 360
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->MISSED_POINTS_IN_TRANSACTION:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-object p0

    .line 359
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->COMPLIMENTARY_POINTS:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-object p0

    .line 358
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->INCREMENT_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;
    .locals 1

    .line 338
    const-class v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;
    .locals 1

    .line 338
    sget-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->$VALUES:[Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 367
    iget v0, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->value:I

    return v0
.end method
