.class final Lcom/squareup/protos/client/loyalty/LoyaltyAccount$ProtoAdapter_LoyaltyAccount;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LoyaltyAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LoyaltyAccount"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccount;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 228
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 253
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;-><init>()V

    .line 254
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 255
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 263
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 261
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->enrolled_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;

    goto :goto_0

    .line 260
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;

    goto :goto_0

    .line 259
    :cond_2
    iget-object v3, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->mappings:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;

    goto :goto_0

    .line 257
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;

    goto :goto_0

    .line 267
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 268
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 226
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$ProtoAdapter_LoyaltyAccount;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/LoyaltyAccount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 243
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->loyalty_account_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 244
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 245
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 246
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 247
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->enrolled_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 248
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 226
    check-cast p2, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$ProtoAdapter_LoyaltyAccount;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/LoyaltyAccount;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;)I
    .locals 4

    .line 233
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->loyalty_account_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x2

    .line 234
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 235
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v3, 0x4

    .line 236
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->enrolled_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x5

    .line 237
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 226
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$ProtoAdapter_LoyaltyAccount;->encodedSize(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount;
    .locals 2

    .line 273
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;

    move-result-object p1

    .line 274
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 275
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->mappings:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 276
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 277
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->enrolled_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->enrolled_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->enrolled_at:Lcom/squareup/protos/common/time/DateTime;

    .line 278
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 279
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 226
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccount$ProtoAdapter_LoyaltyAccount;->redact(Lcom/squareup/protos/client/loyalty/LoyaltyAccount;)Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    move-result-object p1

    return-object p1
.end method
