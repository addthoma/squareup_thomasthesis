.class public final enum Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;
.super Ljava/lang/Enum;
.source "SafetyNetValidateAttestationResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResultCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode$ProtoAdapter_ResultCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FAILED:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

.field public static final enum FAILED_NO_ATTESTATION_GMS_VERSION:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

.field public static final enum PASSED:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 103
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->UNKNOWN:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    .line 108
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    const/4 v2, 0x1

    const-string v3, "FAILED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->FAILED:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    .line 113
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    const/4 v3, 0x2

    const-string v4, "FAILED_NO_ATTESTATION_GMS_VERSION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->FAILED_NO_ATTESTATION_GMS_VERSION:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    .line 118
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    const/4 v4, 0x3

    const-string v5, "PASSED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->PASSED:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    .line 99
    sget-object v5, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->UNKNOWN:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->FAILED:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->FAILED_NO_ATTESTATION_GMS_VERSION:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->PASSED:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->$VALUES:[Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    .line 120
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode$ProtoAdapter_ResultCode;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode$ProtoAdapter_ResultCode;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 125
    iput p3, p0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 136
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->PASSED:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    return-object p0

    .line 135
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->FAILED_NO_ATTESTATION_GMS_VERSION:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    return-object p0

    .line 134
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->FAILED:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    return-object p0

    .line 133
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->UNKNOWN:Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;
    .locals 1

    .line 99
    const-class v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->$VALUES:[Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 143
    iget v0, p0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse$ResultCode;->value:I

    return v0
.end method
