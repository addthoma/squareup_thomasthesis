.class final Lcom/squareup/protos/client/flipper/SealedTicket$ProtoAdapter_SealedTicket;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SealedTicket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/SealedTicket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SealedTicket"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/flipper/SealedTicket;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 205
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/flipper/SealedTicket;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 228
    new-instance v0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;-><init>()V

    .line 229
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 230
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 237
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 235
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->key_index(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    goto :goto_0

    .line 234
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->creation(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    goto :goto_0

    .line 233
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->ciphertext(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    goto :goto_0

    .line 232
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->expiration(Ljava/lang/Long;)Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    goto :goto_0

    .line 241
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 242
    invoke-virtual {v0}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->build()Lcom/squareup/protos/client/flipper/SealedTicket;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 203
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/flipper/SealedTicket$ProtoAdapter_SealedTicket;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/flipper/SealedTicket;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/flipper/SealedTicket;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 219
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 220
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 221
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 222
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    invoke-virtual {p2}, Lcom/squareup/protos/client/flipper/SealedTicket;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 203
    check-cast p2, Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/flipper/SealedTicket$ProtoAdapter_SealedTicket;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/flipper/SealedTicket;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/flipper/SealedTicket;)I
    .locals 4

    .line 210
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    const/4 v3, 0x2

    .line 211
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    const/4 v3, 0x3

    .line 212
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    const/4 v3, 0x4

    .line 213
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SealedTicket;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 203
    check-cast p1, Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/flipper/SealedTicket$ProtoAdapter_SealedTicket;->encodedSize(Lcom/squareup/protos/client/flipper/SealedTicket;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/flipper/SealedTicket;)Lcom/squareup/protos/client/flipper/SealedTicket;
    .locals 0

    .line 247
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SealedTicket;->newBuilder()Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    move-result-object p1

    .line 248
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 249
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->build()Lcom/squareup/protos/client/flipper/SealedTicket;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 203
    check-cast p1, Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/flipper/SealedTicket$ProtoAdapter_SealedTicket;->redact(Lcom/squareup/protos/client/flipper/SealedTicket;)Lcom/squareup/protos/client/flipper/SealedTicket;

    move-result-object p1

    return-object p1
.end method
