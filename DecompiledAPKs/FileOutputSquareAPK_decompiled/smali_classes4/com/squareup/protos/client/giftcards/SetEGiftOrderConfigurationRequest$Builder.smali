.class public final Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetEGiftOrderConfigurationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_request_uuid:Ljava/lang/String;

.field public new_custom_egift_themes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field public order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 117
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->new_custom_egift_themes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;
    .locals 5

    .line 144
    new-instance v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->new_custom_egift_themes:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->client_request_uuid:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;-><init>(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->client_request_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public new_custom_egift_themes(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;"
        }
    .end annotation

    .line 129
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->new_custom_egift_themes:Ljava/util/List;

    return-object p0
.end method

.method public order_configuration(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    return-object p0
.end method
