.class public final enum Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;
.super Ljava/lang/Enum;
.source "HistoryEvent.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/HistoryEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HistoryEventType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType$ProtoAdapter_HistoryEventType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CLEAR:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

.field public static final enum LOAD:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

.field public static final enum REDEEM:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

.field public static final enum REFUND:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 186
    new-instance v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->UNKNOWN:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    .line 191
    new-instance v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    const/4 v2, 0x1

    const-string v3, "LOAD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->LOAD:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    .line 196
    new-instance v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    const/4 v3, 0x2

    const-string v4, "REDEEM"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->REDEEM:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    .line 201
    new-instance v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    const/4 v4, 0x3

    const-string v5, "CLEAR"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->CLEAR:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    .line 206
    new-instance v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    const/4 v5, 0x4

    const-string v6, "REFUND"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->REFUND:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    .line 182
    sget-object v6, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->UNKNOWN:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->LOAD:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->REDEEM:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->CLEAR:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->REFUND:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->$VALUES:[Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    .line 208
    new-instance v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType$ProtoAdapter_HistoryEventType;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType$ProtoAdapter_HistoryEventType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 213
    iput p3, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 225
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->REFUND:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    return-object p0

    .line 224
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->CLEAR:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    return-object p0

    .line 223
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->REDEEM:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    return-object p0

    .line 222
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->LOAD:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    return-object p0

    .line 221
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->UNKNOWN:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;
    .locals 1

    .line 182
    const-class v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;
    .locals 1

    .line 182
    sget-object v0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->$VALUES:[Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 232
    iget v0, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;->value:I

    return v0
.end method
