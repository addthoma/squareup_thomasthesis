.class final Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$ProtoAdapter_SetEGiftOrderConfigurationResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SetEGiftOrderConfigurationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SetEGiftOrderConfigurationResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 150
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 172
    new-instance v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;-><init>()V

    .line 173
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 174
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 180
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 178
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->order_configuration(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;

    goto :goto_0

    .line 177
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/giftcards/EGiftTheme;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;

    goto :goto_0

    .line 184
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$ProtoAdapter_SetEGiftOrderConfigurationResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 165
    sget-object v0, Lcom/squareup/protos/client/giftcards/EGiftTheme;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 166
    sget-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 167
    invoke-virtual {p2}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    check-cast p2, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$ProtoAdapter_SetEGiftOrderConfigurationResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;)I
    .locals 4

    .line 155
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/giftcards/EGiftTheme;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 156
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->all_egift_themes:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    const/4 v3, 0x3

    .line 157
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 148
    check-cast p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$ProtoAdapter_SetEGiftOrderConfigurationResponse;->encodedSize(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;
    .locals 2

    .line 190
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;->newBuilder()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;

    move-result-object p1

    .line 191
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 192
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/giftcards/EGiftTheme;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 193
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iput-object v0, p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 194
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 148
    check-cast p1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse$ProtoAdapter_SetEGiftOrderConfigurationResponse;->redact(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;

    move-result-object p1

    return-object p1
.end method
