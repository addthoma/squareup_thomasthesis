.class public final Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "HistoryEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/HistoryEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/HistoryEvent;",
        "Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/common/Money;

.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public description:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/giftcards/HistoryEvent;
    .locals 7

    .line 175
    new-instance v6, Lcom/squareup/protos/client/giftcards/HistoryEvent;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->type:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/giftcards/HistoryEvent;-><init>(Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->build()Lcom/squareup/protos/client/giftcards/HistoryEvent;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;)Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/HistoryEvent$Builder;->type:Lcom/squareup/protos/client/giftcards/HistoryEvent$HistoryEventType;

    return-object p0
.end method
