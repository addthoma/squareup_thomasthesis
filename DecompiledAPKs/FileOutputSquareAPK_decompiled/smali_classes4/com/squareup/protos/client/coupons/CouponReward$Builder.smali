.class public final Lcom/squareup/protos/client/coupons/CouponReward$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CouponReward.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/CouponReward;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/coupons/CouponReward;",
        "Lcom/squareup/protos/client/coupons/CouponReward$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fixed_discount:Lcom/squareup/protos/common/Money;

.field public item_constraint:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/ItemConstraint;",
            ">;"
        }
    .end annotation
.end field

.field public max_discount:Lcom/squareup/protos/common/Money;

.field public percentage_discount:Ljava/lang/Integer;

.field public scope:Lcom/squareup/protos/client/coupons/Scope;

.field public type:Lcom/squareup/protos/client/coupons/CouponReward$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 177
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 178
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->item_constraint:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/coupons/CouponReward;
    .locals 9

    .line 234
    new-instance v8, Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->scope:Lcom/squareup/protos/client/coupons/Scope;

    iget-object v2, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->item_constraint:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    iget-object v4, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->fixed_discount:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->percentage_discount:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->max_discount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/coupons/CouponReward;-><init>(Lcom/squareup/protos/client/coupons/Scope;Ljava/util/List;Lcom/squareup/protos/client/coupons/CouponReward$Type;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 164
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->build()Lcom/squareup/protos/client/coupons/CouponReward;

    move-result-object v0

    return-object v0
.end method

.method public fixed_discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->fixed_discount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public item_constraint(Ljava/util/List;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/ItemConstraint;",
            ">;)",
            "Lcom/squareup/protos/client/coupons/CouponReward$Builder;"
        }
    .end annotation

    .line 195
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->item_constraint:Ljava/util/List;

    return-object p0
.end method

.method public max_discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->max_discount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public percentage_discount(Ljava/lang/Integer;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->percentage_discount:Ljava/lang/Integer;

    return-object p0
.end method

.method public scope(Lcom/squareup/protos/client/coupons/Scope;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->scope:Lcom/squareup/protos/client/coupons/Scope;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/coupons/CouponReward$Type;)Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    return-object p0
.end method
