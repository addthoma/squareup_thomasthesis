.class public final enum Lcom/squareup/protos/client/coupons/Coupon$Reason;
.super Ljava/lang/Enum;
.source "Coupon.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/Coupon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/coupons/Coupon$Reason$ProtoAdapter_Reason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/coupons/Coupon$Reason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/coupons/Coupon$Reason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FEEDBACK:Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field public static final enum LOYALTY:Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field public static final enum MARKETING:Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field public static final enum UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 401
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_REASON"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/coupons/Coupon$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 403
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;

    const/4 v2, 0x1

    const-string v3, "LOYALTY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/coupons/Coupon$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->LOYALTY:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 405
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;

    const/4 v3, 0x2

    const-string v4, "MARKETING"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/coupons/Coupon$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->MARKETING:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 407
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;

    const/4 v4, 0x3

    const-string v5, "FEEDBACK"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/coupons/Coupon$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->FEEDBACK:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 400
    sget-object v5, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon$Reason;->LOYALTY:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon$Reason;->MARKETING:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon$Reason;->FEEDBACK:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->$VALUES:[Lcom/squareup/protos/client/coupons/Coupon$Reason;

    .line 409
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$Reason$ProtoAdapter_Reason;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/Coupon$Reason$ProtoAdapter_Reason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 413
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 414
    iput p3, p0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/coupons/Coupon$Reason;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 425
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->FEEDBACK:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object p0

    .line 424
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->MARKETING:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object p0

    .line 423
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->LOYALTY:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object p0

    .line 422
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/Coupon$Reason;
    .locals 1

    .line 400
    const-class v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/coupons/Coupon$Reason;
    .locals 1

    .line 400
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->$VALUES:[Lcom/squareup/protos/client/coupons/Coupon$Reason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/coupons/Coupon$Reason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 432
    iget v0, p0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->value:I

    return v0
.end method
