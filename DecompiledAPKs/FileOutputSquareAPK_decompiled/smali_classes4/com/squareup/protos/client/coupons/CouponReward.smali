.class public final Lcom/squareup/protos/client/coupons/CouponReward;
.super Lcom/squareup/wire/Message;
.source "CouponReward.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/coupons/CouponReward$ProtoAdapter_CouponReward;,
        Lcom/squareup/protos/client/coupons/CouponReward$Type;,
        Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/coupons/CouponReward;",
        "Lcom/squareup/protos/client/coupons/CouponReward$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/coupons/CouponReward;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PERCENTAGE_DISCOUNT:Ljava/lang/Integer;

.field public static final DEFAULT_SCOPE:Lcom/squareup/protos/client/coupons/Scope;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/coupons/CouponReward$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final fixed_discount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final item_constraint:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.ItemConstraint#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/ItemConstraint;",
            ">;"
        }
    .end annotation
.end field

.field public final max_discount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final percentage_discount:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x5
    .end annotation
.end field

.field public final scope:Lcom/squareup/protos/client/coupons/Scope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.Scope#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/coupons/CouponReward$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.CouponReward$Type#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/coupons/CouponReward$ProtoAdapter_CouponReward;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/CouponReward$ProtoAdapter_CouponReward;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/coupons/CouponReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/client/coupons/Scope;->UNKNOWN:Lcom/squareup/protos/client/coupons/Scope;

    sput-object v0, Lcom/squareup/protos/client/coupons/CouponReward;->DEFAULT_SCOPE:Lcom/squareup/protos/client/coupons/Scope;

    .line 31
    sget-object v0, Lcom/squareup/protos/client/coupons/CouponReward$Type;->UNKNOWN:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    sput-object v0, Lcom/squareup/protos/client/coupons/CouponReward;->DEFAULT_TYPE:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/coupons/CouponReward;->DEFAULT_PERCENTAGE_DISCOUNT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/coupons/Scope;Ljava/util/List;Lcom/squareup/protos/client/coupons/CouponReward$Type;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/protos/common/Money;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/coupons/Scope;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/ItemConstraint;",
            ">;",
            "Lcom/squareup/protos/client/coupons/CouponReward$Type;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    .line 94
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/coupons/CouponReward;-><init>(Lcom/squareup/protos/client/coupons/Scope;Ljava/util/List;Lcom/squareup/protos/client/coupons/CouponReward$Type;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/coupons/Scope;Ljava/util/List;Lcom/squareup/protos/client/coupons/CouponReward$Type;Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/coupons/Scope;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/ItemConstraint;",
            ">;",
            "Lcom/squareup/protos/client/coupons/CouponReward$Type;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/common/Money;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 100
    sget-object v0, Lcom/squareup/protos/client/coupons/CouponReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    const-string p1, "item_constraint"

    .line 102
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    .line 103
    iput-object p3, p0, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    .line 104
    iput-object p4, p0, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    .line 105
    iput-object p5, p0, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    .line 106
    iput-object p6, p0, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 125
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/coupons/CouponReward;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 126
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/coupons/CouponReward;

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/CouponReward;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/CouponReward;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    .line 129
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    .line 133
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 138
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 140
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/CouponReward;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/Scope;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/CouponReward$Type;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 147
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/coupons/CouponReward$Builder;
    .locals 2

    .line 111
    new-instance v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->scope:Lcom/squareup/protos/client/coupons/Scope;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->item_constraint:Ljava/util/List;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->fixed_discount:Lcom/squareup/protos/common/Money;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->percentage_discount:Ljava/lang/Integer;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->max_discount:Lcom/squareup/protos/common/Money;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/CouponReward;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/coupons/CouponReward$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/CouponReward;->newBuilder()Lcom/squareup/protos/client/coupons/CouponReward$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    if-eqz v1, :cond_0

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", item_constraint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->type:Lcom/squareup/protos/client/coupons/CouponReward$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", fixed_discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->fixed_discount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", percentage_discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->percentage_discount:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", max_discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/CouponReward;->max_discount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CouponReward{"

    .line 161
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
