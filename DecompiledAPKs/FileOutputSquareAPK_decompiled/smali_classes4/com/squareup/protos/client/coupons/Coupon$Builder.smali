.class public final Lcom/squareup/protos/client/coupons/Coupon$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Coupon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/Coupon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "Lcom/squareup/protos/client/coupons/Coupon$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public code:Ljava/lang/String;

.field public condition:Lcom/squareup/protos/client/coupons/CouponCondition;

.field public coupon_id:Ljava/lang/String;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public discount:Lcom/squareup/api/items/Discount;

.field public expires_at:Lcom/squareup/protos/client/ISO8601Date;

.field public image_url:Ljava/lang/String;

.field public item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

.field public loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

.field public reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field public reward:Lcom/squareup/protos/client/coupons/CouponReward;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 253
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/coupons/Coupon;
    .locals 14

    .line 340
    new-instance v13, Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->coupon_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->discount:Lcom/squareup/api/items/Discount;

    iget-object v3, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    iget-object v4, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->image_url:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    iget-object v6, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v7, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v8, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v9, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    iget-object v10, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    iget-object v11, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->code:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/coupons/Coupon;-><init>(Ljava/lang/String;Lcom/squareup/api/items/Discount;Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;Ljava/lang/String;Lcom/squareup/protos/client/coupons/CouponCondition;Lcom/squareup/protos/client/coupons/CouponReward;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/coupons/LoyaltyPoints;Lcom/squareup/protos/client/coupons/Coupon$Reason;Ljava/lang/String;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 230
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->build()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    return-object v0
.end method

.method public code(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->code:Ljava/lang/String;

    return-object p0
.end method

.method public condition(Lcom/squareup/protos/client/coupons/CouponCondition;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    return-object p0
.end method

.method public coupon_id(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->coupon_id:Ljava/lang/String;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->discount:Lcom/squareup/api/items/Discount;

    return-object p0
.end method

.method public expires_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public image_url(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->image_url:Ljava/lang/String;

    return-object p0
.end method

.method public item_constraint_type(Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-object p0
.end method

.method public loyalty_points(Lcom/squareup/protos/client/coupons/LoyaltyPoints;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    return-object p0
.end method

.method public reason(Lcom/squareup/protos/client/coupons/Coupon$Reason;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object p0
.end method

.method public reward(Lcom/squareup/protos/client/coupons/CouponReward;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    return-object p0
.end method
