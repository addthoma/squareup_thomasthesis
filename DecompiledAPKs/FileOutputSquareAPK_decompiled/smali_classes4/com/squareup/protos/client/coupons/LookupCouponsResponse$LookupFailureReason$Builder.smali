.class public final Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LookupCouponsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public expired_at:Lcom/squareup/protos/client/ISO8601Date;

.field public redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

.field public type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 221
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;
    .locals 5

    .line 252
    new-instance v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    iget-object v2, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;-><init>(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 214
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->build()Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    move-result-object v0

    return-object v0
.end method

.method public expired_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public redeemed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$Builder;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    return-object p0
.end method
