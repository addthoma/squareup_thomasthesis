.class public final Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BatchResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

.field public response:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/Response;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 98
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->response:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->response:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;)Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->error:Lcom/squareup/protos/client/posfe/inventory/sync/SyncError;

    return-object p0
.end method

.method public response(Ljava/util/List;)Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/Response;",
            ">;)",
            "Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;"
        }
    .end annotation

    .line 110
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/BatchResponse$Builder;->response:Ljava/util/List;

    return-object p0
.end method
