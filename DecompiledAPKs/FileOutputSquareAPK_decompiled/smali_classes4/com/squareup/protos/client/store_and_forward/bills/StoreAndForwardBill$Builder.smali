.class public final Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StoreAndForwardBill.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;",
        "Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

.field public bill_id:Lcom/squareup/protos/client/IdPair;

.field public bill_payload_encryption_key_token:Ljava/lang/String;

.field public encrypted_payload:Lokio/ByteString;

.field public payment_instrument_encryption_key_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 162
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public auth_code(Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;
    .locals 0

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    return-object p0
.end method

.method public bill_id(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->bill_id:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public bill_payload_encryption_key_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->bill_payload_encryption_key_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;
    .locals 8

    .line 214
    new-instance v7, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->bill_id:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->payment_instrument_encryption_key_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->bill_payload_encryption_key_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->encrypted_payload:Lokio/ByteString;

    iget-object v5, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_payload(Lokio/ByteString;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->encrypted_payload:Lokio/ByteString;

    return-object p0
.end method

.method public payment_instrument_encryption_key_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill$Builder;->payment_instrument_encryption_key_token:Ljava/lang/String;

    return-object p0
.end method
