.class public final Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;
.super Lcom/squareup/wire/Message;
.source "GetBillsStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$ProtoAdapter_GetBillsStatusRequest;,
        Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;",
        "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final client_id:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$ProtoAdapter_GetBillsStatusRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$ProtoAdapter_GetBillsStatusRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 36
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 40
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p2, "client_id"

    .line 41
    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->client_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 55
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 56
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;

    .line 57
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->client_id:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->client_id:Ljava/util/List;

    .line 58
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 63
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->client_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;
    .locals 2

    .line 46
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;-><init>()V

    .line 47
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->client_id:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;->client_id:Ljava/util/List;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->newBuilder()Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->client_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", client_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;->client_id:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetBillsStatusRequest{"

    .line 76
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
