.class final Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$ProtoAdapter_QueueRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "QueueRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_QueueRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 132
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 151
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;-><init>()V

    .line 152
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 153
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 158
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 156
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->bill:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 155
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->payment:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 162
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 163
    invoke-virtual {v0}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 130
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$ProtoAdapter_QueueRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 144
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;->payment:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 145
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;->bill:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 146
    invoke-virtual {p2}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 130
    check-cast p2, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$ProtoAdapter_QueueRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;)I
    .locals 4

    .line 137
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;->payment:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 138
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;->bill:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 130
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$ProtoAdapter_QueueRequest;->encodedSize(Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;
    .locals 2

    .line 168
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;->newBuilder()Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;

    move-result-object p1

    .line 169
    iget-object v0, p1, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->payment:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 170
    iget-object v0, p1, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->bill:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 171
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 172
    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 130
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$ProtoAdapter_QueueRequest;->redact(Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    move-result-object p1

    return-object p1
.end method
