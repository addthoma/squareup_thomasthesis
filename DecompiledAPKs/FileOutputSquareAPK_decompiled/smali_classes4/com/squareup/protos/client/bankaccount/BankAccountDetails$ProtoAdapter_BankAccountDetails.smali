.class final Lcom/squareup/protos/client/bankaccount/BankAccountDetails$ProtoAdapter_BankAccountDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BankAccountDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/BankAccountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BankAccountDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 213
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 238
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;-><init>()V

    .line 239
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 240
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 255
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 253
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->secondary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    goto :goto_0

    .line 247
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_type(Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 249
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 244
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    goto :goto_0

    .line 243
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->primary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    goto :goto_0

    .line 242
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_name(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    goto :goto_0

    .line 259
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 260
    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->build()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 211
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$ProtoAdapter_BankAccountDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 228
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->account_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 229
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->primary_institution_number:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 230
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->secondary_institution_number:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 231
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->account_number:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 232
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 233
    invoke-virtual {p2}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 211
    check-cast p2, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$ProtoAdapter_BankAccountDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)I
    .locals 4

    .line 218
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->account_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->primary_institution_number:Ljava/lang/String;

    const/4 v3, 0x2

    .line 219
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->secondary_institution_number:Ljava/lang/String;

    const/4 v3, 0x5

    .line 220
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->account_number:Ljava/lang/String;

    const/4 v3, 0x3

    .line 221
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const/4 v3, 0x4

    .line 222
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 211
    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$ProtoAdapter_BankAccountDetails;->encodedSize(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails;
    .locals 1

    .line 265
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;->newBuilder()Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 266
    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_name:Ljava/lang/String;

    .line 267
    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->primary_institution_number:Ljava/lang/String;

    .line 268
    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_number:Ljava/lang/String;

    .line 269
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 270
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->build()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 211
    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$ProtoAdapter_BankAccountDetails;->redact(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object p1

    return-object p1
.end method
