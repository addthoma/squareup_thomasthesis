.class public final Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AdjustVariationInventoryRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/AdjustVariationInventoryRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/AdjustVariationInventoryRequest;",
        "Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

.field public count:Lcom/squareup/protos/client/InventoryCount;

.field public idempotency_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public adjustment(Lcom/squareup/protos/client/InventoryAdjustment;)Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    const/4 p1, 0x0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->count:Lcom/squareup/protos/client/InventoryCount;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;
    .locals 5

    .line 138
    new-instance v0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->idempotency_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->count:Lcom/squareup/protos/client/InventoryCount;

    iget-object v3, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/InventoryCount;Lcom/squareup/protos/client/InventoryAdjustment;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->build()Lcom/squareup/protos/client/AdjustVariationInventoryRequest;

    move-result-object v0

    return-object v0
.end method

.method public count(Lcom/squareup/protos/client/InventoryCount;)Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->count:Lcom/squareup/protos/client/InventoryCount;

    const/4 p1, 0x0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->adjustment:Lcom/squareup/protos/client/InventoryAdjustment;

    return-object p0
.end method

.method public idempotency_token(Ljava/lang/String;)Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/AdjustVariationInventoryRequest$Builder;->idempotency_token:Ljava/lang/String;

    return-object p0
.end method
