.class public final Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bfd/cart/DisplayItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bfd/cart/DisplayItem;",
        "Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_id:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Discount;",
            ">;"
        }
    .end annotation
.end field

.field public is_discount_row:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;

.field public price:Ljava/lang/String;

.field public quantity:Ljava/lang/Integer;

.field public taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Tax;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 188
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 189
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->taxes:Ljava/util/List;

    .line 190
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->discounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bfd/cart/DisplayItem;
    .locals 11

    .line 237
    new-instance v10, Lcom/squareup/protos/client/bfd/cart/DisplayItem;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->price:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->client_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->taxes:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->is_discount_row:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->quantity:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->discounts:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bfd/cart/DisplayItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v10
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->build()Lcom/squareup/protos/client/bfd/cart/DisplayItem;

    move-result-object v0

    return-object v0
.end method

.method public client_id(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->client_id:Ljava/lang/String;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public discounts(Ljava/util/List;)Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Discount;",
            ">;)",
            "Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;"
        }
    .end annotation

    .line 230
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->discounts:Ljava/util/List;

    return-object p0
.end method

.method public is_discount_row(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->is_discount_row:Ljava/lang/Boolean;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public price(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->price:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/lang/Integer;)Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->quantity:Ljava/lang/Integer;

    return-object p0
.end method

.method public taxes(Ljava/util/List;)Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/Tax;",
            ">;)",
            "Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;"
        }
    .end annotation

    .line 214
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayItem$Builder;->taxes:Ljava/util/List;

    return-object p0
.end method
