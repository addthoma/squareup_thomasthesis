.class public final Lcom/squareup/protos/client/bfd/cart/DisplayCart;
.super Lcom/squareup/wire/Message;
.source "DisplayCart.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bfd/cart/DisplayCart$ProtoAdapter_DisplayCart;,
        Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bfd/cart/DisplayCart;",
        "Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bfd/cart/DisplayCart;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IS_CARD_STILL_INSERTED:Ljava/lang/Boolean;

.field public static final DEFAULT_OFFLINE_MODE:Ljava/lang/Boolean;

.field public static final DEFAULT_TOTAL_AMOUNT:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bfd.cart.DisplayBanner#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final is_card_still_inserted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bfd.cart.DisplayItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field public final offline_mode:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final total_amount:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$ProtoAdapter_DisplayCart;

    invoke-direct {v0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$ProtoAdapter_DisplayCart;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 27
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->DEFAULT_TOTAL_AMOUNT:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->DEFAULT_OFFLINE_MODE:Ljava/lang/Boolean;

    .line 31
    sput-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->DEFAULT_IS_CARD_STILL_INSERTED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/bfd/cart/DisplayBanner;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/DisplayItem;",
            ">;",
            "Lcom/squareup/protos/client/bfd/cart/DisplayBanner;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 66
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bfd/cart/DisplayBanner;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/bfd/cart/DisplayBanner;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/DisplayItem;",
            ">;",
            "Lcom/squareup/protos/client/bfd/cart/DisplayBanner;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 71
    sget-object v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p6, "items"

    .line 72
    invoke-static {p6, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    .line 73
    iput-object p2, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    .line 74
    iput-object p3, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    .line 75
    iput-object p4, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    .line 76
    iput-object p5, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 94
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 95
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    .line 97
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    .line 101
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 106
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 114
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;-><init>()V

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->items:Ljava/util/List;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->total_amount:Ljava/lang/Long;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->offline_mode:Ljava/lang/Boolean;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->is_card_still_inserted:Ljava/lang/Boolean;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->newBuilder()Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    if-eqz v1, :cond_1

    const-string v1, ", banner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", total_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->total_amount:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", offline_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->offline_mode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", is_card_still_inserted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart;->is_card_still_inserted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisplayCart{"

    .line 127
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
