.class public final Lcom/squareup/protos/eventstream/v1/Sim$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Sim.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Sim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Sim;",
        "Lcom/squareup/protos/eventstream/v1/Sim$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public country_iso:Ljava/lang/String;

.field public mcc:Ljava/lang/Integer;

.field public mnc:Ljava/lang/Integer;

.field public operator_name:Ljava/lang/String;

.field public serial_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 142
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/eventstream/v1/Sim;
    .locals 8

    .line 172
    new-instance v7, Lcom/squareup/protos/eventstream/v1/Sim;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->country_iso:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->mcc:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->mnc:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->operator_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->serial_number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/eventstream/v1/Sim;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->build()Lcom/squareup/protos/eventstream/v1/Sim;

    move-result-object v0

    return-object v0
.end method

.method public country_iso(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->country_iso:Ljava/lang/String;

    return-object p0
.end method

.method public mcc(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->mcc:Ljava/lang/Integer;

    return-object p0
.end method

.method public mnc(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->mnc:Ljava/lang/Integer;

    return-object p0
.end method

.method public operator_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->operator_name:Ljava/lang/String;

    return-object p0
.end method

.method public serial_number(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Sim$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->serial_number:Ljava/lang/String;

    return-object p0
.end method
