.class public final Lcom/squareup/protos/eventstream/v1/Data$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Data.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Data;",
        "Lcom/squareup/protos/eventstream/v1/Data$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public raw_bytes:Lokio/ByteString;

.field public raw_data:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/eventstream/v1/Data;
    .locals 4

    .line 151
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Data;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Data$Builder;->raw_data:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/eventstream/v1/Data$Builder;->raw_bytes:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/eventstream/v1/Data;-><init>(Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Data$Builder;->build()Lcom/squareup/protos/eventstream/v1/Data;

    move-result-object v0

    return-object v0
.end method

.method public raw_bytes(Lokio/ByteString;)Lcom/squareup/protos/eventstream/v1/Data$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Data$Builder;->raw_bytes:Lokio/ByteString;

    return-object p0
.end method

.method public raw_data(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Data$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Data$Builder;->raw_data:Ljava/lang/String;

    return-object p0
.end method
