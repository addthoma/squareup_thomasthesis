.class Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;
.super Lcom/squareup/payment/RealDanglingAuth$Voidable;
.source "RealDanglingAuth.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealDanglingAuth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BillPaymentVoidable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/RealDanglingAuth;


# direct methods
.method constructor <init>(Lcom/squareup/payment/RealDanglingAuth;Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)V
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    .line 233
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/RealDanglingAuth$Voidable;-><init>(Lcom/squareup/payment/RealDanglingAuth;Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)V

    return-void
.end method


# virtual methods
.method doVoidAuth(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 4

    .line 242
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    invoke-static {v0}, Lcom/squareup/payment/RealDanglingAuth;->access$700(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 244
    invoke-static {p1}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->access$100(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 245
    invoke-static {p1}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->access$200(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 246
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    if-eqz p2, :cond_1

    .line 249
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 250
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/IdPair;

    .line 251
    new-instance v2, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;-><init>()V

    .line 253
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object v1

    .line 254
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object v1

    .line 251
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 256
    :cond_0
    iget-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    invoke-static {p1}, Lcom/squareup/payment/RealDanglingAuth;->access$500(Lcom/squareup/payment/RealDanglingAuth;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/bills/RemoveTendersTask;

    iget-object v2, p0, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;->billId:Lcom/squareup/protos/client/IdPair;

    new-instance v3, Lcom/squareup/protos/client/Merchant$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/Merchant$Builder;-><init>()V

    .line 258
    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/Merchant$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/Merchant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/Merchant$Builder;->build()Lcom/squareup/protos/client/Merchant;

    move-result-object v0

    invoke-direct {v1, v2, v0, p2}, Lcom/squareup/queue/bills/RemoveTendersTask;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;)V

    .line 257
    invoke-interface {p1, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 247
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Have a partial payment auth with no tenderIds"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 260
    :cond_2
    iget-object p1, p0, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;->this$0:Lcom/squareup/payment/RealDanglingAuth;

    invoke-static {p1}, Lcom/squareup/payment/RealDanglingAuth;->access$500(Lcom/squareup/payment/RealDanglingAuth;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/bills/CancelBill;

    iget-object v2, p0, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;->billId:Lcom/squareup/protos/client/IdPair;

    invoke-direct {v1, v2, p2, v0}, Lcom/squareup/queue/bills/CancelBill;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BillPaymentVoidable: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;->billId:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
