.class public final Lcom/squareup/payment/TransactionConfig$Builder;
.super Ljava/lang/Object;
.source "TransactionConfig.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/TransactionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionConfig.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionConfig.kt\ncom/squareup/payment/TransactionConfig$Builder\n*L\n1#1,56:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010*\u001a\u00020+J\u000e\u0010\u000f\u001a\u00020\u00002\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0015\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u0010J\u0010\u0010\u0018\u001a\u00020\u00002\u0008\u0010\u0018\u001a\u0004\u0018\u00010\nJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010!\u001a\u00020\u00002\u0006\u0010!\u001a\u00020\u0010J\u0010\u0010$\u001a\u00020\u00002\u0008\u0010$\u001a\u0004\u0018\u00010%R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0012\"\u0004\u0008\u0017\u0010\u0014R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u000c\"\u0004\u0008\u001a\u0010\u000eR\u001a\u0010\u001b\u001a\u00020\u001cX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\"\u0004\u0008\u001f\u0010 R\u001a\u0010!\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\"\u0010\u0012\"\u0004\u0008#\u0010\u0014R\u001c\u0010$\u001a\u0004\u0018\u00010%X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008&\u0010\'\"\u0004\u0008(\u0010)\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/payment/TransactionConfig$Builder;",
        "",
        "()V",
        "amount",
        "",
        "getAmount",
        "()J",
        "setAmount",
        "(J)V",
        "apiClientId",
        "",
        "getApiClientId",
        "()Ljava/lang/String;",
        "setApiClientId",
        "(Ljava/lang/String;)V",
        "clearTaxes",
        "",
        "getClearTaxes",
        "()Z",
        "setClearTaxes",
        "(Z)V",
        "delayCapture",
        "getDelayCapture",
        "setDelayCapture",
        "note",
        "getNote",
        "setNote",
        "receiptScreenStrategy",
        "Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;",
        "getReceiptScreenStrategy",
        "()Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;",
        "setReceiptScreenStrategy",
        "(Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;)V",
        "skipSignature",
        "getSkipSignature",
        "setSkipSignature",
        "tipSettings",
        "Lcom/squareup/settings/server/TipSettings;",
        "getTipSettings",
        "()Lcom/squareup/settings/server/TipSettings;",
        "setTipSettings",
        "(Lcom/squareup/settings/server/TipSettings;)V",
        "build",
        "Lcom/squareup/payment/TransactionConfig;",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private amount:J

.field private apiClientId:Ljava/lang/String;

.field private clearTaxes:Z

.field private delayCapture:Z

.field private note:Ljava/lang/String;

.field private receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

.field private skipSignature:Z

.field private tipSettings:Lcom/squareup/settings/server/TipSettings;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->SHOW_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    iput-object v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    const-string v0, "NO_CLIENT_ID"

    .line 22
    iput-object v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->apiClientId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final amount(J)Lcom/squareup/payment/TransactionConfig$Builder;
    .locals 1

    .line 27
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/TransactionConfig$Builder;

    iput-wide p1, v0, Lcom/squareup/payment/TransactionConfig$Builder;->amount:J

    return-object v0
.end method

.method public final apiClientId(Ljava/lang/String;)Lcom/squareup/payment/TransactionConfig$Builder;
    .locals 1

    const-string v0, "apiClientId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/TransactionConfig$Builder;

    iput-object p1, v0, Lcom/squareup/payment/TransactionConfig$Builder;->apiClientId:Ljava/lang/String;

    return-object v0
.end method

.method public final build()Lcom/squareup/payment/TransactionConfig;
    .locals 11

    .line 44
    new-instance v10, Lcom/squareup/payment/TransactionConfig;

    .line 45
    iget-wide v1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->amount:J

    .line 46
    iget-object v3, p0, Lcom/squareup/payment/TransactionConfig$Builder;->note:Ljava/lang/String;

    .line 47
    iget-object v4, p0, Lcom/squareup/payment/TransactionConfig$Builder;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    .line 48
    iget-object v5, p0, Lcom/squareup/payment/TransactionConfig$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    .line 49
    iget-object v6, p0, Lcom/squareup/payment/TransactionConfig$Builder;->apiClientId:Ljava/lang/String;

    .line 50
    iget-boolean v7, p0, Lcom/squareup/payment/TransactionConfig$Builder;->skipSignature:Z

    .line 51
    iget-boolean v8, p0, Lcom/squareup/payment/TransactionConfig$Builder;->delayCapture:Z

    .line 52
    iget-boolean v9, p0, Lcom/squareup/payment/TransactionConfig$Builder;->clearTaxes:Z

    move-object v0, v10

    .line 44
    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/TransactionConfig;-><init>(JLjava/lang/String;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZZZ)V

    return-object v10
.end method

.method public final clearTaxes(Z)Lcom/squareup/payment/TransactionConfig$Builder;
    .locals 1

    .line 42
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/TransactionConfig$Builder;

    iput-boolean p1, v0, Lcom/squareup/payment/TransactionConfig$Builder;->clearTaxes:Z

    return-object v0
.end method

.method public final delayCapture(Z)Lcom/squareup/payment/TransactionConfig$Builder;
    .locals 1

    .line 40
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/TransactionConfig$Builder;

    iput-boolean p1, v0, Lcom/squareup/payment/TransactionConfig$Builder;->delayCapture:Z

    return-object v0
.end method

.method public final getAmount()J
    .locals 2

    .line 18
    iget-wide v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->amount:J

    return-wide v0
.end method

.method public final getApiClientId()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->apiClientId:Ljava/lang/String;

    return-object v0
.end method

.method public final getClearTaxes()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->clearTaxes:Z

    return v0
.end method

.method public final getDelayCapture()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->delayCapture:Z

    return v0
.end method

.method public final getNote()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final getReceiptScreenStrategy()Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-object v0
.end method

.method public final getSkipSignature()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->skipSignature:Z

    return v0
.end method

.method public final getTipSettings()Lcom/squareup/settings/server/TipSettings;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/payment/TransactionConfig$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    return-object v0
.end method

.method public final note(Ljava/lang/String;)Lcom/squareup/payment/TransactionConfig$Builder;
    .locals 1

    .line 29
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/TransactionConfig$Builder;

    iput-object p1, v0, Lcom/squareup/payment/TransactionConfig$Builder;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final receiptScreenStrategy(Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;)Lcom/squareup/payment/TransactionConfig$Builder;
    .locals 1

    const-string v0, "receiptScreenStrategy"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/TransactionConfig$Builder;

    iput-object p1, v0, Lcom/squareup/payment/TransactionConfig$Builder;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-object v0
.end method

.method public final setAmount(J)V
    .locals 0

    .line 18
    iput-wide p1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->amount:J

    return-void
.end method

.method public final setApiClientId(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->apiClientId:Ljava/lang/String;

    return-void
.end method

.method public final setClearTaxes(Z)V
    .locals 0

    .line 25
    iput-boolean p1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->clearTaxes:Z

    return-void
.end method

.method public final setDelayCapture(Z)V
    .locals 0

    .line 24
    iput-boolean p1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->delayCapture:Z

    return-void
.end method

.method public final setNote(Ljava/lang/String;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->note:Ljava/lang/String;

    return-void
.end method

.method public final setReceiptScreenStrategy(Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->receiptScreenStrategy:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    return-void
.end method

.method public final setSkipSignature(Z)V
    .locals 0

    .line 23
    iput-boolean p1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->skipSignature:Z

    return-void
.end method

.method public final setTipSettings(Lcom/squareup/settings/server/TipSettings;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/payment/TransactionConfig$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    return-void
.end method

.method public final skipSignature(Z)Lcom/squareup/payment/TransactionConfig$Builder;
    .locals 1

    .line 38
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/TransactionConfig$Builder;

    iput-boolean p1, v0, Lcom/squareup/payment/TransactionConfig$Builder;->skipSignature:Z

    return-object v0
.end method

.method public final tipSettings(Lcom/squareup/settings/server/TipSettings;)Lcom/squareup/payment/TransactionConfig$Builder;
    .locals 1

    .line 34
    move-object v0, p0

    check-cast v0, Lcom/squareup/payment/TransactionConfig$Builder;

    iput-object p1, v0, Lcom/squareup/payment/TransactionConfig$Builder;->tipSettings:Lcom/squareup/settings/server/TipSettings;

    return-object v0
.end method
