.class public Lcom/squareup/payment/DiscountLineItemJsonAdapter;
.super Ljava/lang/Object;
.source "DiscountLineItemJsonAdapter.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/squareup/protos/client/bills/DiscountLineItem;
    .locals 10

    .line 29
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object p1

    const-string p2, "discount_line_item_id_pair"

    .line 30
    invoke-virtual {p1, p2}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p2

    const-class v0, Lcom/squareup/protos/client/IdPair;

    invoke-interface {p3, p2, v0}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/IdPair;

    const-string v0, "created_at"

    .line 31
    invoke-virtual {p1, v0}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    const-class v1, Lcom/squareup/protos/client/ISO8601Date;

    invoke-interface {p3, v0, v1}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    const-string v1, "configuration"

    .line 33
    invoke-virtual {p1, v1}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v1

    const-class v2, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    invoke-interface {p3, v1, v2}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    const-string/jumbo v2, "write_only_backing_details"

    .line 35
    invoke-virtual {p1, v2}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v2

    const-class v3, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    invoke-interface {p3, v2, v3}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    const-string v3, "read_only_display_details"

    .line 37
    invoke-virtual {p1, v3}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    const-class v4, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    invoke-interface {p3, v3, v4}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    const-string v4, "amounts"

    .line 38
    invoke-virtual {p1, v4}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v4

    const-class v5, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    invoke-interface {p3, v4, v5}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    const-string/jumbo v5, "write_only_deleted"

    .line 39
    invoke-virtual {p1, v5}, Lcom/google/gson/JsonObject;->getAsJsonPrimitive(Ljava/lang/String;)Lcom/google/gson/JsonPrimitive;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v5, :cond_0

    .line 41
    invoke-virtual {v5}, Lcom/google/gson/JsonPrimitive;->getAsBoolean()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v8, "itemization_application_scope"

    .line 44
    invoke-virtual {p1, v8}, Lcom/google/gson/JsonObject;->getAsJsonPrimitive(Ljava/lang/String;)Lcom/google/gson/JsonPrimitive;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 46
    invoke-virtual {v8}, Lcom/google/gson/JsonPrimitive;->getAsString()Ljava/lang/String;

    move-result-object p1

    const/4 p3, -0x1

    .line 47
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v8

    const v9, -0x73dc7b78

    if-eq v8, v9, :cond_2

    const v6, -0x53747160

    if-eq v8, v6, :cond_1

    goto :goto_1

    :cond_1
    const-string v6, "ALL_ITEMIZATIONS"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p3, 0x1

    goto :goto_1

    :cond_2
    const-string v8, "INDIVIDUAL_ITEMIZATIONS"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p3, 0x0

    :cond_3
    :goto_1
    if-eqz p3, :cond_5

    if-eq p3, v7, :cond_4

    .line 55
    sget-object p1, Lcom/squareup/protos/client/bills/ApplicationScope;->UNKNOWN:Lcom/squareup/protos/client/bills/ApplicationScope;

    goto :goto_2

    .line 52
    :cond_4
    sget-object p1, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    goto :goto_2

    .line 49
    :cond_5
    sget-object p1, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    goto :goto_2

    :cond_6
    const-string v6, "application_scope"

    .line 59
    invoke-virtual {p1, v6}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object p1

    const-class v6, Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-interface {p3, p1, v6}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 62
    :goto_2
    new-instance p3, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;-><init>()V

    .line 63
    invoke-virtual {p3, p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->discount_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    .line 64
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    .line 65
    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->configuration(Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    .line 66
    invoke-virtual {p2, v2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    .line 67
    invoke-virtual {p2, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->read_only_display_details(Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    .line 68
    invoke-virtual {p2, v4}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    .line 69
    invoke-virtual {p2, v5}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p2

    .line 70
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->application_scope(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .line 23
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/payment/DiscountLineItemJsonAdapter;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object p1

    return-object p1
.end method
