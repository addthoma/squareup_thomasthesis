.class public Lcom/squareup/payment/CompVoidTicketPaymentFactory;
.super Lcom/squareup/payment/BillPayment$Factory;
.source "CompVoidTicketPaymentFactory.java"


# static fields
.field private static final NO_ONLINE_STRATEGY_FACTORY:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

.field private static final NO_RECEIPT_FACTORY:Lcom/squareup/payment/PaymentReceipt$Factory;


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final paymentFlowTaskProviderFactory:Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/payment/CompVoidTicketPaymentFactory$1;

    invoke-direct {v0}, Lcom/squareup/payment/CompVoidTicketPaymentFactory$1;-><init>()V

    sput-object v0, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->NO_RECEIPT_FACTORY:Lcom/squareup/payment/PaymentReceipt$Factory;

    .line 42
    new-instance v0, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;-><init>(Lcom/squareup/server/bills/BillCreationService;Lcom/squareup/payment/BackgroundCaptor;)V

    sput-object v0, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->NO_ONLINE_STRATEGY_FACTORY:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;Lcom/squareup/settings/server/Features;)V
    .locals 24
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v13, p0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v14, p6

    move-object/from16 v17, p7

    move-object/from16 v18, p8

    move-object/from16 v19, p9

    move-object/from16 v21, p10

    move-object/from16 v22, p11

    move-object/from16 v23, p13

    .line 53
    sget-object v6, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->NO_RECEIPT_FACTORY:Lcom/squareup/payment/PaymentReceipt$Factory;

    sget-object v15, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->NO_ONLINE_STRATEGY_FACTORY:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v16, 0x0

    move-object/from16 v13, v16

    const/16 v20, 0x0

    invoke-direct/range {v0 .. v23}, Lcom/squareup/payment/BillPayment$Factory;-><init>(Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/BackgroundCaptor;Lcom/squareup/protos/common/CurrencyCode;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/payment/PaymentReceipt$Factory;Lrx/Scheduler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/settings/server/Features;)V

    move-object/from16 v1, p12

    .line 57
    iput-object v1, v0, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->paymentFlowTaskProviderFactory:Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    move-object/from16 v1, p3

    .line 58
    iput-object v1, v0, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public create(ZLcom/squareup/payment/Order;)Lcom/squareup/payment/BillPayment;
    .locals 7

    const-string v0, "Must be single tender."

    .line 62
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 64
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getNotVoidedItems()Ljava/util/List;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 66
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    .line 71
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    const/4 v3, 0x1

    :cond_3
    const-string v0, "Entire cart must be comped or voided."

    invoke-static {v3, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 72
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 73
    new-instance v6, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    const/4 v1, 0x0

    new-instance v2, Lcom/squareup/protos/common/Money;

    const-wide/16 v3, 0x0

    .line 74
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v2, v3, v4}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-direct {v6, v1, v0, v2, v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->paymentFlowTaskProviderFactory:Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    .line 76
    invoke-virtual {v0, v6}, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->create(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/payment/PaymentFlowTaskProvider;

    move-result-object v5

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    .line 75
    invoke-super/range {v1 .. v6}, Lcom/squareup/payment/BillPayment$Factory;->create(ZLcom/squareup/payment/Order;Lcom/squareup/settings/server/TipSettings;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/payment/BillPayment;

    move-result-object p1

    return-object p1
.end method
