.class public final Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;
.super Ljava/lang/Object;
.source "BillPaymentLocalStrategy_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localPaymentsQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final merchantTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final taskQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->taskQueueProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->localPaymentsQueueProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->merchantTokenProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Ljava/lang/String;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;-><init>(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Ljava/lang/String;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;
    .locals 4

    .line 37
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->taskQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, p0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->localPaymentsQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v2, p0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->merchantTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->newInstance(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Ljava/lang/String;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/payment/BillPaymentLocalStrategy_Factory_Factory;->get()Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    move-result-object v0

    return-object v0
.end method
