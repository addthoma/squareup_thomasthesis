.class public Lcom/squareup/payment/TransactionDiscountAdapter;
.super Ljava/lang/Object;
.source "TransactionDiscountAdapter.kt"

# interfaces
.implements Lcom/squareup/checkout/HoldsCoupons;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionDiscountAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionDiscountAdapter.kt\ncom/squareup/payment/TransactionDiscountAdapter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,103:1\n1550#2,2:104\n1552#2:109\n1550#2,3:110\n704#2:113\n777#2,2:114\n203#3,3:106\n*E\n*S KotlinDebug\n*F\n+ 1 TransactionDiscountAdapter.kt\ncom/squareup/payment/TransactionDiscountAdapter\n*L\n70#1,2:104\n70#1:109\n81#1,3:110\n85#1:113\n85#1,2:114\n70#1,3:106\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0008\u0016\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0010H\u0016J\u0016\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u0017\u001a\u00020\u00082\u0006\u0010\u0018\u001a\u00020\u0015H\u0016J\u0018\u0010\u0019\u001a\u00020\u000c2\u0006\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u001b\u001a\u00020\u0008H\u0002J\u0010\u0010\u0019\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u001c\u001a\u00020\u000c2\u0006\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u0008H\u0016R\u0014\u0010\u0007\u001a\u00020\u00088BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/payment/TransactionDiscountAdapter;",
        "Lcom/squareup/checkout/HoldsCoupons;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;)V",
        "discountApplicationIdEnabled",
        "",
        "getDiscountApplicationIdEnabled",
        "()Z",
        "apply",
        "",
        "coupon",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "discountsChanged",
        "Lio/reactivex/Observable;",
        "getAllAddedCoupons",
        "",
        "Lcom/squareup/checkout/Discount;",
        "couponDefinitionToken",
        "",
        "hasQualifyingItem",
        "isCouponAddedToCart",
        "couponToken",
        "remove",
        "discount",
        "maybeQueueReturnCouponTask",
        "removeCoupon",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final getDiscountApplicationIdEnabled()Z
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final remove(Lcom/squareup/checkout/Discount;Z)V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object p1, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/payment/Transaction;->removeDiscountsFromAllItems(Ljava/lang/Iterable;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;Z)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 3

    const-string v0, "coupon"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/squareup/payment/TransactionDiscountAdapter;->getDiscountApplicationIdEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 26
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-direct {p0}, Lcom/squareup/payment/TransactionDiscountAdapter;->getDiscountApplicationIdEnabled()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/payment/Transaction;->addCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    goto :goto_1

    .line 30
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/payment/TransactionDiscountAdapter;->remove(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 32
    new-instance v0, Lcom/squareup/checkout/Discount$Builder;

    invoke-direct {p0}, Lcom/squareup/payment/TransactionDiscountAdapter;->getDiscountApplicationIdEnabled()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    .line 33
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 37
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "d"

    .line 38
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/squareup/payment/TransactionDiscountAdapter;->remove(Lcom/squareup/checkout/Discount;Z)V

    goto :goto_0

    .line 43
    :cond_2
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-direct {p0}, Lcom/squareup/payment/TransactionDiscountAdapter;->getDiscountApplicationIdEnabled()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/payment/Transaction;->addCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    :cond_3
    :goto_1
    return-void
.end method

.method public discountsChanged()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cartChanges()Lrx/Observable;

    move-result-object v0

    const-string v1, "transaction.cartChanges()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 90
    sget-object v1, Lcom/squareup/payment/TransactionDiscountAdapter$discountsChanged$1;->INSTANCE:Lcom/squareup/payment/TransactionDiscountAdapter$discountsChanged$1;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/squareup/payment/TransactionDiscountAdapter$discountsChanged$2;->INSTANCE:Lcom/squareup/payment/TransactionDiscountAdapter$discountsChanged$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "transaction.cartChanges(\u2026d }\n        .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getAllAddedCoupons(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    const-string v0, "couponDefinitionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 113
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 114
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/checkout/Discount;

    .line 85
    invoke-virtual {v3}, Lcom/squareup/checkout/Discount;->getCouponDefinitionToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public hasQualifyingItem(Lcom/squareup/protos/client/coupons/Coupon;)Z
    .locals 5

    const-string v0, "coupon"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/squareup/checkout/Discount$Builder;

    invoke-direct {p0}, Lcom/squareup/payment/TransactionDiscountAdapter;->getDiscountApplicationIdEnabled()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    .line 68
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object p1

    .line 70
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "transaction.items"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 104
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2

    .line 105
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    const-string v4, "item"

    .line 71
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/squareup/checkout/Discount;->isItemEligible(Lcom/squareup/checkout/CartItem;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    const-string v4, "item.appliedDiscounts"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 107
    :cond_3
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Discount;

    .line 74
    invoke-virtual {v4}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_5

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_1

    const/4 v3, 0x1

    :cond_6
    :goto_2
    return v3
.end method

.method public isCouponAddedToCart(Ljava/lang/String;)Z
    .locals 3

    const-string v0, "couponToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 110
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 111
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 81
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    return v2
.end method

.method public remove(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 2

    const-string v0, "coupon"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/squareup/payment/TransactionDiscountAdapter;->getDiscountApplicationIdEnabled()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 49
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    const-string v0, "coupon.coupon_id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v1}, Lcom/squareup/payment/TransactionDiscountAdapter;->removeCoupon(Ljava/lang/String;Z)V

    goto :goto_0

    .line 51
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    const-string v0, "coupon.discount.id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v1}, Lcom/squareup/payment/TransactionDiscountAdapter;->removeCoupon(Ljava/lang/String;Z)V

    :goto_0
    return-void
.end method

.method public removeCoupon(Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "couponToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/payment/TransactionDiscountAdapter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/Discount;

    if-eqz p1, :cond_0

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/TransactionDiscountAdapter;->remove(Lcom/squareup/checkout/Discount;Z)V

    :cond_0
    return-void
.end method
