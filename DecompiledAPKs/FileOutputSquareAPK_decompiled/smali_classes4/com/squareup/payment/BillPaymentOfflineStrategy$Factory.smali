.class Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;
.super Ljava/lang/Object;
.source "BillPaymentOfflineStrategy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/BillPaymentOfflineStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Factory"
.end annotation


# instance fields
.field private final backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

.field private final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private final storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

.field private final storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

.field private final transactionLazy:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Lcom/squareup/payment/BackgroundCaptor;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            "Lcom/squareup/payment/BackgroundCaptor;",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    iput-object p1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 279
    iput-object p2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->res:Lcom/squareup/util/Res;

    .line 280
    iput-object p3, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->transactionLazy:Ldagger/Lazy;

    .line 281
    iput-object p4, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->locationProvider:Ljavax/inject/Provider;

    .line 282
    iput-object p5, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    .line 283
    iput-object p6, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    .line 284
    iput-object p7, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    .line 285
    iput-object p8, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    .line 286
    iput-object p9, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/util/Res;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Ldagger/Lazy;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->transactionLazy:Ldagger/Lazy;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Ljavax/inject/Provider;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->locationProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/analytics/StoreAndForwardAnalytics;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->storeAndForwardAnalytics:Lcom/squareup/analytics/StoreAndForwardAnalytics;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/payment/PaymentAccuracyLogger;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/payment/offline/StoreAndForwardPaymentService;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->storeAndForwardPaymentService:Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/payment/BackgroundCaptor;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;)Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;
    .locals 0

    .line 260
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    return-object p0
.end method


# virtual methods
.method create()Lcom/squareup/payment/BillPaymentOfflineStrategy;
    .locals 2

    .line 290
    new-instance v0, Lcom/squareup/payment/BillPaymentOfflineStrategy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/BillPaymentOfflineStrategy;-><init>(Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;Lcom/squareup/payment/BillPaymentOfflineStrategy$1;)V

    return-object v0
.end method
