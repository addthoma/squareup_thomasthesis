.class public Lcom/squareup/payment/tender/CashTender;
.super Lcom/squareup/payment/tender/BaseLocalTender;
.source "CashTender.java"

# interfaces
.implements Lcom/squareup/payment/tender/BaseTender$ReturnsChange;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/CashTender$Builder;
    }
.end annotation


# instance fields
.field private final change:Lcom/squareup/protos/common/Money;

.field private final res:Lcom/squareup/util/Res;

.field private final tendered:Lcom/squareup/protos/common/Money;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/tender/CashTender$Builder;)V
    .locals 1

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseLocalTender;-><init>(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    .line 24
    invoke-static {p1}, Lcom/squareup/payment/tender/CashTender$Builder;->access$000(Lcom/squareup/payment/tender/CashTender$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/CashTender;->tendered:Lcom/squareup/protos/common/Money;

    .line 25
    iget-object p1, p1, Lcom/squareup/payment/tender/CashTender$Builder;->res:Lcom/squareup/util/Res;

    iput-object p1, p0, Lcom/squareup/payment/tender/CashTender;->res:Lcom/squareup/util/Res;

    .line 27
    iget-object p1, p0, Lcom/squareup/payment/tender/CashTender;->tendered:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/CashTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/CashTender;->change:Lcom/squareup/protos/common/Money;

    .line 28
    iget-object p1, p0, Lcom/squareup/payment/tender/CashTender;->change:Lcom/squareup/protos/common/Money;

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isNegative(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 29
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Amount cannot exceed the received tender!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/tender/CashTender$Builder;Lcom/squareup/payment/tender/CashTender$1;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/CashTender;-><init>(Lcom/squareup/payment/tender/CashTender$Builder;)V

    return-void
.end method


# virtual methods
.method public getChange()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/payment/tender/CashTender;->change:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getDeclinedMessage()Ljava/lang/String;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/payment/tender/CashTender;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->split_tender_cash_declined:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 4

    .line 42
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/CashTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CashTender$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;-><init>()V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/payment/tender/CashTender;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;->buyer_tendered_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;

    move-result-object v2

    .line 46
    invoke-virtual {p0}, Lcom/squareup/payment/tender/CashTender;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;->change_back_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;

    move-result-object v2

    .line 47
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CashTender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/CashTender$Amounts;

    move-result-object v2

    .line 44
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CashTender$Builder;->amounts(Lcom/squareup/protos/client/bills/CashTender$Amounts;)Lcom/squareup/protos/client/bills/CashTender$Builder;

    move-result-object v1

    .line 48
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CashTender$Builder;->build()Lcom/squareup/protos/client/bills/CashTender;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->cash_tender(Lcom/squareup/protos/client/bills/CashTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 53
    sget v0, Lcom/squareup/transaction/R$string;->buyer_printed_receipt_tender_cash:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getShortTenderMessage()Ljava/lang/String;
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/payment/tender/CashTender;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->cash:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CASH:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public getTenderTypeGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 65
    invoke-virtual {p0}, Lcom/squareup/payment/tender/CashTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/util/ProtoGlyphs;->cash(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method public getTendered()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/payment/tender/CashTender;->tendered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public supportsPaperSigAndTip()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
