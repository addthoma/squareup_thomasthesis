.class public final Lcom/squareup/payment/tender/EmoneyTender;
.super Lcom/squareup/payment/tender/BaseCardTender;
.source "EmoneyTender.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/EmoneyTender$Builder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001&B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016J\u0008\u0010\u0017\u001a\u00020\u0016H\u0016J\u0008\u0010\u0018\u001a\u00020\u0016H\u0016J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u001cH\u0014J\u0008\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020!H\u0016J\u0008\u0010\"\u001a\u00020\u000eH\u0016J\u0008\u0010#\u001a\u00020$H\u0016J\u0008\u0010%\u001a\u00020\u0016H\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0010\u0010\t\u001a\u00020\n8\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\r\u001a\u0004\u0018\u00010\u000e8BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u001e\u0010\u0011\u001a\u0004\u0018\u00010\u000e8FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\u0010\"\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/payment/tender/EmoneyTender;",
        "Lcom/squareup/payment/tender/BaseCardTender;",
        "builder",
        "Lcom/squareup/payment/tender/EmoneyTender$Builder;",
        "(Lcom/squareup/payment/tender/EmoneyTender$Builder;)V",
        "brand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "getBrand",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;",
        "brandDisplay",
        "",
        "cardData",
        "Lokio/ByteString;",
        "maskedCardNumber",
        "",
        "getMaskedCardNumber",
        "()Ljava/lang/String;",
        "terminalId",
        "getTerminalId",
        "setTerminalId",
        "(Ljava/lang/String;)V",
        "askForSignature",
        "",
        "askForTip",
        "canQuickCapture",
        "getCard",
        "Lcom/squareup/Card;",
        "getMethod",
        "Lcom/squareup/protos/client/bills/Tender$Method;",
        "getPaymentInstrument",
        "Lcom/squareup/protos/client/bills/PaymentInstrument;",
        "getReceiptTenderName",
        "res",
        "Lcom/squareup/util/Res;",
        "getShortTenderMessage",
        "getTenderTypeForLogging",
        "Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;",
        "supportsPaperSigAndTip",
        "Builder",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field private final brandDisplay:I

.field private final cardData:Lokio/ByteString;

.field private maskedCardNumber:Ljava/lang/String;

.field private terminalId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/tender/EmoneyTender$Builder;)V
    .locals 1

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    move-object v0, p1

    check-cast v0, Lcom/squareup/payment/tender/BaseCardTender$Builder;

    invoke-direct {p0, v0}, Lcom/squareup/payment/tender/BaseCardTender;-><init>(Lcom/squareup/payment/tender/BaseCardTender$Builder;)V

    .line 45
    invoke-virtual {p1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->getCardData()Lokio/ByteString;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iput-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender;->cardData:Lokio/ByteString;

    .line 46
    invoke-virtual {p1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->getMaskedCardNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender;->maskedCardNumber:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->getBrand()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iput-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 48
    invoke-virtual {p1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->getTerminalId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender;->terminalId:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->getBrandDisplay()Ljava/lang/Integer;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/squareup/payment/tender/EmoneyTender;->brandDisplay:I

    return-void
.end method

.method private final getMaskedCardNumber()Ljava/lang/String;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/payment/tender/EmoneyTender;->getTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_masked_card_number:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method


# virtual methods
.method public askForSignature()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public askForTip()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canQuickCapture()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getBrand()Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    return-object v0
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 2

    .line 83
    new-instance v0, Lcom/squareup/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/Card$Builder;-><init>()V

    .line 84
    sget-object v1, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v0

    const-string v1, "com.squareup.Card.Builde\u2026.FELICA)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 4

    .line 53
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    .line 55
    new-instance v1, Lcom/squareup/protos/client/bills/CardTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;-><init>()V

    .line 57
    new-instance v2, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;-><init>()V

    .line 58
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    .line 59
    iget-object v3, p0, Lcom/squareup/payment/tender/EmoneyTender;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->felica_brand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    .line 60
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object v2

    .line 56
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->card(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    const-string v1, "Tender.Method.Builder()\n\u2026       )\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;
    .locals 4

    .line 68
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 69
    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/squareup/protos/client/bills/CardData$R12Card;

    iget-object v2, p0, Lcom/squareup/payment/tender/EmoneyTender;->cardData:Lokio/ByteString;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/squareup/protos/client/bills/CardData$R12Card;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card(Lcom/squareup/protos/client/bills/CardData$R12Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object v0

    .line 73
    new-instance v1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;-><init>()V

    .line 74
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    const-string v1, "PaymentInstrument.Builde\u2026ardData)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    sget-object v1, Lcom/squareup/payment/tender/EmoneyTender$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 101
    invoke-super {p0, p1}, Lcom/squareup/payment/tender/BaseCardTender;->getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 100
    :cond_0
    sget v0, Lcom/squareup/utilities/R$string;->receipt_tender_felica_suica:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 99
    :cond_1
    sget v0, Lcom/squareup/utilities/R$string;->receipt_tender_felica_id:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 98
    :cond_2
    sget v0, Lcom/squareup/utilities/R$string;->receipt_tender_felica_qp:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 104
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x20

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/squareup/payment/tender/EmoneyTender;->getMaskedCardNumber()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getShortTenderMessage()Ljava/lang/String;
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/payment/tender/EmoneyTender;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/payment/tender/EmoneyTender;->brandDisplay:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->EMONEY:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public final getTerminalId()Ljava/lang/String;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/payment/tender/EmoneyTender;->getTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->felica_sprwid:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final setTerminalId(Ljava/lang/String;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/payment/tender/EmoneyTender;->terminalId:Ljava/lang/String;

    return-void
.end method

.method public supportsPaperSigAndTip()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
