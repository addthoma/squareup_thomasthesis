.class public Lcom/squareup/payment/tender/CashTender$Builder;
.super Lcom/squareup/payment/tender/BaseLocalTender$Builder;
.source "CashTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/CashTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private tendered:Lcom/squareup/protos/common/Money;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 2

    .line 80
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/client/bills/Tender$Type;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/tender/CashTender$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 76
    iget-object p0, p0, Lcom/squareup/payment/tender/CashTender$Builder;->tendered:Lcom/squareup/protos/common/Money;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/payment/tender/CashTender$Builder;->build()Lcom/squareup/payment/tender/CashTender;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/CashTender;
    .locals 2

    .line 111
    new-instance v0, Lcom/squareup/payment/tender/CashTender;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/tender/CashTender;-><init>(Lcom/squareup/payment/tender/CashTender$Builder;Lcom/squareup/payment/tender/CashTender$1;)V

    return-object v0
.end method

.method public canBeZeroAmount()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getChange()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/payment/tender/CashTender$Builder;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/payment/tender/CashTender$Builder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getMaxAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 102
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->getMaxAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTendered()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/payment/tender/CashTender$Builder;->tendered:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public bridge synthetic setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 0

    .line 76
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/CashTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;
    .locals 0

    .line 97
    invoke-super {p0, p1}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    return-object p0
.end method

.method public setTendered(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/payment/tender/CashTender$Builder;->tendered:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
