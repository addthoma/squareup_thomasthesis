.class public Lcom/squareup/payment/tender/SmartCardTenderBuilder;
.super Lcom/squareup/payment/tender/BaseCardTender$Builder;
.source "SmartCardTenderBuilder.java"


# instance fields
.field protected accountType:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

.field protected buyerSelectedAccountName:Ljava/lang/String;

.field protected cardholderName:Ljava/lang/String;

.field protected entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field protected fallbackCard:Lcom/squareup/Card;

.field protected fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

.field protected smartCardAuthRequestData:[B

.field private startedPaymentOnReader:Z

.field protected wasTenderApprovedOffline:Z


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, v0}, Lcom/squareup/payment/tender/BaseCardTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Ljava/lang/String;)V

    .line 28
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->NONE:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    .line 29
    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->tipAmount:Lcom/squareup/protos/common/Money;

    .line 30
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->build()Lcom/squareup/payment/tender/SmartCardTender;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/SmartCardTender;
    .locals 1

    .line 104
    new-instance v0, Lcom/squareup/payment/tender/SmartCardTender;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/SmartCardTender;-><init>(Lcom/squareup/payment/tender/SmartCardTenderBuilder;)V

    return-object v0
.end method

.method getFallbackType()Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    return-object v0
.end method

.method public hasAuthData()Z
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->smartCardAuthRequestData:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasCardholderName()Z
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->cardholderName:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasFallback()Z
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->NONE:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasStartedPaymentOnReader()Z
    .locals 1

    .line 38
    iget-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->startedPaymentOnReader:Z

    return v0
.end method

.method public isContactless()Z
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public paymentStartedOnReader()V
    .locals 1

    const/4 v0, 0x1

    .line 34
    iput-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->startedPaymentOnReader:Z

    return-void
.end method

.method public setAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->accountType:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    return-void
.end method

.method public setBuyerSelectedAccountName(Ljava/lang/String;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->buyerSelectedAccountName:Ljava/lang/String;

    return-void
.end method

.method public setCardholderName(Ljava/lang/String;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->cardholderName:Ljava/lang/String;

    return-void
.end method

.method public setFallbackSwipeData(Lcom/squareup/Card;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->fallbackCard:Lcom/squareup/Card;

    return-void
.end method

.method public setFallbackType(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 1

    const-string v0, "fallbackType"

    .line 42
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    return-void
.end method

.method public setSmartCardAuthRequestData(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;[B)V
    .locals 0

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 56
    iput-object p2, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->smartCardAuthRequestData:[B

    return-void
.end method

.method public setSmartCardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->EMV:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 65
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "entryMethod must be set for smart payment (see RA-9783)"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 67
    :cond_1
    :goto_0
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->NONE:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    .line 68
    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method

.method public setTenderApprovedOffline()V
    .locals 1

    const/4 v0, 0x1

    .line 72
    iput-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->wasTenderApprovedOffline:Z

    return-void
.end method

.method public wasTenderApprovedOffline()Z
    .locals 1

    .line 76
    iget-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->wasTenderApprovedOffline:Z

    return v0
.end method
