.class public Lcom/squareup/payment/tender/TenderFactory;
.super Ljava/lang/Object;
.source "TenderFactory.java"


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field protected final branRemoteCardReader:Lcom/squareup/cardreader/BranRemoteCardReader;

.field protected final cardConverter:Lcom/squareup/payment/CardConverter;

.field protected final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field protected final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field protected final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field protected final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field protected final localTenderCache:Lcom/squareup/print/LocalTenderCache;

.field protected final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field protected final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field protected final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field protected final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field protected final res:Lcom/squareup/util/Res;

.field protected final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field protected final swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

.field protected final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field protected final tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

.field private final tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

.field protected final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;Ljavax/inject/Provider;Lcom/squareup/payment/tender/TenderProtoFactory;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/CardConverter;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/BranRemoteCardReader;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/print/LocalTenderCache;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/OfflineModeMonitor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Lcom/squareup/payment/tender/TenderProtoFactory;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/CardConverter;",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/cardreader/BranRemoteCardReader;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/print/LocalTenderCache;",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            "Lcom/squareup/settings/server/SwipeChipCardsSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 71
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-object v1, p2

    .line 72
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p3

    .line 73
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object v1, p4

    .line 74
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->locationProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 75
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

    move-object v1, p6

    .line 76
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p7

    .line 77
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->res:Lcom/squareup/util/Res;

    move-object v1, p8

    .line 78
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object v1, p9

    .line 79
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p10

    .line 80
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->cardConverter:Lcom/squareup/payment/CardConverter;

    move-object v1, p11

    .line 81
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    move-object v1, p12

    .line 82
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object v1, p13

    .line 83
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->branRemoteCardReader:Lcom/squareup/cardreader/BranRemoteCardReader;

    move-object/from16 v1, p14

    .line 84
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    move-object/from16 v1, p15

    .line 85
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    move-object/from16 v1, p16

    .line 86
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    move-object/from16 v1, p17

    .line 87
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    move-object/from16 v1, p18

    .line 88
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    move-object/from16 v1, p19

    .line 89
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p20

    .line 90
    iput-object v1, v0, Lcom/squareup/payment/tender/TenderFactory;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    return-void
.end method


# virtual methods
.method protected cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->branRemoteCardReader:Lcom/squareup/cardreader/BranRemoteCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/BranRemoteCardReader;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->branRemoteCardReader:Lcom/squareup/cardreader/BranRemoteCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/BranRemoteCardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    return-object v0

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    .line 162
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public createCash()Lcom/squareup/payment/tender/CashTender$Builder;
    .locals 1

    .line 114
    new-instance v0, Lcom/squareup/payment/tender/CashTender$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/CashTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;)V

    return-object v0
.end method

.method public createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/payment/tender/TenderFactory;->createCash()Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/payment/tender/CashTender$Builder;->setTendered(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p2

    .line 119
    invoke-virtual {p2, p1}, Lcom/squareup/payment/tender/CashTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    return-object p2
.end method

.method public createCreditCard()Lcom/squareup/payment/tender/MagStripeTenderBuilder$CreditCardTenderBuilder;
    .locals 2

    .line 98
    new-instance v0, Lcom/squareup/payment/tender/MagStripeTenderBuilder$CreditCardTenderBuilder;

    iget-object v1, p0, Lcom/squareup/payment/tender/TenderFactory;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/tender/MagStripeTenderBuilder$CreditCardTenderBuilder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/settings/server/SwipeChipCardsSettings;)V

    return-object v0
.end method

.method public createEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;
    .locals 2

    .line 94
    new-instance v0, Lcom/squareup/payment/tender/EmoneyTender$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/tender/EmoneyTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Ljava/lang/String;)V

    return-object v0
.end method

.method public createGiftCard(Ljava/lang/String;)Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;

    iget-object v1, p0, Lcom/squareup/payment/tender/TenderFactory;->swipeChipCardsSettings:Lcom/squareup/settings/server/SwipeChipCardsSettings;

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/payment/tender/MagStripeTenderBuilder$GiftCardTenderBuilder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/settings/server/SwipeChipCardsSettings;Ljava/lang/String;)V

    return-object v0
.end method

.method public createInstrument()Lcom/squareup/payment/tender/InstrumentTender$Builder;
    .locals 1

    .line 106
    new-instance v0, Lcom/squareup/payment/tender/InstrumentTender$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/InstrumentTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;)V

    return-object v0
.end method

.method public createNoSale()Lcom/squareup/payment/tender/NoSaleTender$Builder;
    .locals 1

    .line 141
    new-instance v0, Lcom/squareup/payment/tender/NoSaleTender$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/NoSaleTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;)V

    return-object v0
.end method

.method public createOther()Lcom/squareup/payment/tender/OtherTender$Builder;
    .locals 1

    .line 129
    new-instance v0, Lcom/squareup/payment/tender/OtherTender$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/OtherTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;)V

    return-object v0
.end method

.method public createOther(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/OtherTender$Builder;
    .locals 1

    .line 133
    new-instance v0, Lcom/squareup/payment/tender/OtherTender$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/OtherTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;)V

    .line 134
    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/OtherTender$Builder;->setType(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    .line 135
    invoke-virtual {p1, p2}, Lcom/squareup/payment/tender/OtherTender$Builder;->setNote(Ljava/lang/String;)Lcom/squareup/payment/tender/OtherTender$Builder;

    move-result-object p1

    .line 136
    invoke-virtual {p1, p3}, Lcom/squareup/payment/tender/OtherTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    return-object p1
.end method

.method public createSmartCard()Lcom/squareup/payment/tender/SmartCardTenderBuilder;
    .locals 1

    .line 110
    new-instance v0, Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;-><init>(Lcom/squareup/payment/tender/TenderFactory;)V

    return-object v0
.end method

.method public createZeroCash()Lcom/squareup/payment/tender/CashTender$Builder;
    .locals 3

    .line 124
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 125
    invoke-virtual {p0, v0, v0}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public createZeroToReplaceCard()Lcom/squareup/payment/tender/ZeroTender$Builder;
    .locals 2

    .line 154
    new-instance v0, Lcom/squareup/payment/tender/ZeroTender$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/tender/ZeroTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;)V

    return-object v0
.end method

.method public createZeroToReplaceCash()Lcom/squareup/payment/tender/ZeroTender$Builder;
    .locals 2

    .line 146
    new-instance v0, Lcom/squareup/payment/tender/ZeroTender$Builder;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->CASH_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/tender/ZeroTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;)V

    return-object v0
.end method

.method public isEmvCapableReaderPresent()Z
    .locals 1

    .line 166
    invoke-virtual {p0}, Lcom/squareup/payment/tender/TenderFactory;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/squareup/payment/tender/TenderFactory;->cardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareDeviceCardReader()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    .line 168
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isPaymentReadySmartReaderConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    .line 169
    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTakingPaymentForInvoice()Z
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    return v0
.end method

.method public showSignatureForCardNotPresent()Z
    .locals 2

    .line 181
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CARD_NOT_PRESENT_SIGN:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public showSignatureForCardOnFile()Z
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_SIGN:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public tipSettings()Lcom/squareup/settings/server/TipSettings;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    return-object v0
.end method

.method public usePreAuthTipping()Z
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/payment/tender/TenderFactory;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminerFactory;->usePreAuthTipping()Z

    move-result v0

    return v0
.end method
