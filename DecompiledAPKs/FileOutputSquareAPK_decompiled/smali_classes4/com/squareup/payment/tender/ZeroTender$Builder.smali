.class public Lcom/squareup/payment/tender/ZeroTender$Builder;
.super Lcom/squareup/payment/tender/BaseLocalTender$Builder;
.source "ZeroTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/ZeroTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;)V
    .locals 2

    .line 72
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->ZERO_AMOUNT:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/client/bills/Tender$Type;)V

    .line 73
    iget-object p1, p1, Lcom/squareup/payment/tender/TenderFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p1, p0, Lcom/squareup/payment/tender/ZeroTender$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 74
    iget-object p1, p0, Lcom/squareup/payment/tender/ZeroTender$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/ZeroTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/tender/ZeroTender$Builder;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/squareup/payment/tender/ZeroTender$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 67
    invoke-virtual {p0}, Lcom/squareup/payment/tender/ZeroTender$Builder;->build()Lcom/squareup/payment/tender/ZeroTender;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/ZeroTender;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/payment/tender/ZeroTender;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/tender/ZeroTender;-><init>(Lcom/squareup/payment/tender/ZeroTender$Builder;Lcom/squareup/payment/tender/ZeroTender$1;)V

    return-object v0
.end method

.method public canBeZeroAmount()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getMaxAmount()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 78
    iget-object v0, p0, Lcom/squareup/payment/tender/ZeroTender$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method
