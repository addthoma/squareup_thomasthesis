.class public final Lcom/squareup/payment/BackgroundCaptor_Factory;
.super Ljava/lang/Object;
.source "BackgroundCaptor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/BackgroundCaptor;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoCaptureControlProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;"
        }
    .end annotation
.end field

.field private final danglingAuthProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingCapturesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final taskQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private final voidMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->autoCaptureControlProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->pendingCapturesProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->danglingAuthProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->taskQueueProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->voidMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/BackgroundCaptor_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/autocapture/AutoCaptureControl;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;)",
            "Lcom/squareup/payment/BackgroundCaptor_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/payment/BackgroundCaptor_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/BackgroundCaptor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/VoidMonitor;)Lcom/squareup/payment/BackgroundCaptor;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/payment/BackgroundCaptor;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/BackgroundCaptor;-><init>(Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/VoidMonitor;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/payment/BackgroundCaptor;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->autoCaptureControlProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/autocapture/AutoCaptureControl;

    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->pendingCapturesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->danglingAuthProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->taskQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v0, p0, Lcom/squareup/payment/BackgroundCaptor_Factory;->voidMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/VoidMonitor;

    invoke-static/range {v1 .. v6}, Lcom/squareup/payment/BackgroundCaptor_Factory;->newInstance(Lcom/squareup/autocapture/AutoCaptureControl;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/VoidMonitor;)Lcom/squareup/payment/BackgroundCaptor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/payment/BackgroundCaptor_Factory;->get()Lcom/squareup/payment/BackgroundCaptor;

    move-result-object v0

    return-object v0
.end method
