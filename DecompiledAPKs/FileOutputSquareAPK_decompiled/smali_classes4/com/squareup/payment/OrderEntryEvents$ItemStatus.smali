.class public Lcom/squareup/payment/OrderEntryEvents$ItemStatus;
.super Ljava/lang/Object;
.source "OrderEntryEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/OrderEntryEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemStatus"
.end annotation


# instance fields
.field public final changeType:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

.field public final item:Lcom/squareup/checkout/CartItem;

.field public final itemType:Lcom/squareup/payment/OrderEntryEvents$ItemType;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ItemType;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->changeType:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    .line 53
    iput-object p2, p0, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->item:Lcom/squareup/checkout/CartItem;

    .line 54
    iput-object p3, p0, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->itemType:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    return-void
.end method
