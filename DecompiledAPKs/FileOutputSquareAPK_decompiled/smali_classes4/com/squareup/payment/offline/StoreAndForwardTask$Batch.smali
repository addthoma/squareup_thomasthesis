.class Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;
.super Ljava/lang/Object;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Batch"
.end annotation


# instance fields
.field private includesAllPayments:Z

.field private lastUniqueKey:Ljava/lang/String;

.field private final paymentsByKey:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 508
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->paymentsByKey:Ljava/util/Map;

    const/4 v0, 0x0

    .line 509
    iput-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->lastUniqueKey:Ljava/lang/String;

    const/4 v0, 0x1

    .line 510
    iput-boolean v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->includesAllPayments:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask$1;)V
    .locals 0

    .line 507
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)Ljava/util/Map;
    .locals 0

    .line 507
    iget-object p0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->paymentsByKey:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)Z
    .locals 0

    .line 507
    iget-boolean p0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->includesAllPayments:Z

    return p0
.end method


# virtual methods
.method addIfNotFull(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 4

    .line 516
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v0

    .line 524
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->paymentsByKey:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x14

    if-lt v1, v3, :cond_1

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->paymentsByKey:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 529
    :cond_0
    iput-boolean v2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->includesAllPayments:Z

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    const-string v2, "QueueBert: found payment %s"

    .line 525
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 526
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->paymentsByKey:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    iput-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->lastUniqueKey:Ljava/lang/String;

    :goto_1
    return-void
.end method

.method isEmpty()Z
    .locals 1

    .line 547
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->paymentsByKey:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method removeLastPayment()V
    .locals 2

    .line 543
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->paymentsByKey:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->lastUniqueKey:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method requireLastPayment()Lcom/squareup/payment/offline/StoredPayment;
    .locals 2

    .line 535
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->lastUniqueKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 539
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->paymentsByKey:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/offline/StoredPayment;

    return-object v0

    .line 536
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "lastUniqueKeyInBatch should not be null."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
