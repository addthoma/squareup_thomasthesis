.class Lcom/squareup/payment/offline/ForwardedPaymentManager$2;
.super Ljava/lang/Object;
.source "ForwardedPaymentManager.java"

# interfaces
.implements Lio/reactivex/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/ForwardedPaymentManager;->updateStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/Observer<",
        "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/ForwardedPaymentManager;)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 3

    .line 237
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$902(Lcom/squareup/payment/offline/ForwardedPaymentManager;Z)Z

    .line 239
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$800(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v2}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$700(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/squareup/settings/LocalSetting;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "Payment/Bill status update complete"

    .line 240
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$902(Lcom/squareup/payment/offline/ForwardedPaymentManager;Z)Z

    .line 245
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    .line 247
    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {p1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$800(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$700(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void

    .line 250
    :cond_0
    new-instance v0, Lio/reactivex/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p1}, Lio/reactivex/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public onNext(Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;)V
    .locals 4

    .line 218
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$200(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$300(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Ljava/util/Map;

    move-result-object v0

    .line 223
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 224
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 225
    iget-object v3, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v3, p1, v1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$400(Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;Ljava/util/List;)V

    .line 226
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;

    .line 227
    iget-object v3, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v3, v0, v1, v2}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$500(Lcom/squareup/payment/offline/ForwardedPaymentManager;Ljava/util/Map;Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;Ljava/util/List;)V

    goto :goto_0

    .line 231
    :cond_1
    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {p1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$600(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 232
    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {p1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$700(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/squareup/settings/LocalSetting;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 233
    iget-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {p1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$800(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 211
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->onNext(Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;)V

    return-void
.end method

.method public onSubscribe(Lio/reactivex/disposables/Disposable;)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$2;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$100(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Lio/reactivex/disposables/CompositeDisposable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
