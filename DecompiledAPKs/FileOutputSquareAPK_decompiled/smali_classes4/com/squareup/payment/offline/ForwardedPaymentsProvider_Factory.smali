.class public final Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;
.super Ljava/lang/Object;
.source "ForwardedPaymentsProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/offline/ForwardedPaymentsProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/google/gson/Gson;)Lcom/squareup/payment/offline/ForwardedPaymentsProvider;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;-><init>(Landroid/app/Application;Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/offline/ForwardedPaymentsProvider;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/Gson;

    invoke-static {v0, v1}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;->newInstance(Landroid/app/Application;Lcom/google/gson/Gson;)Lcom/squareup/payment/offline/ForwardedPaymentsProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider_Factory;->get()Lcom/squareup/payment/offline/ForwardedPaymentsProvider;

    move-result-object v0

    return-object v0
.end method
