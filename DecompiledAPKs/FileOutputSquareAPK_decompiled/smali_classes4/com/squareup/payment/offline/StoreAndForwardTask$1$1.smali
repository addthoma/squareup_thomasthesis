.class Lcom/squareup/payment/offline/StoreAndForwardTask$1$1;
.super Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask$1;->doRun(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$1;

.field final synthetic val$storedPaymentCount:I


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask$1;Lcom/squareup/server/SquareCallback;I)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$1$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$1;

    iput p3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$1$1;->val$storedPaymentCount:I

    iget-object p1, p1, Lcom/squareup/payment/offline/StoreAndForwardTask$1;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method


# virtual methods
.method protected doRun(Lcom/squareup/server/SquareCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 134
    iget v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$1$1;->val$storedPaymentCount:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$1$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$1;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$1;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-static {v0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$000(Lcom/squareup/payment/offline/StoreAndForwardTask;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$1$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$1;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$1;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask;->storedPaymentNotifier:Lcom/squareup/notifications/StoredPaymentNotifier;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$1$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$1;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask$1;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask;->merchantName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/notifications/StoredPaymentNotifier;->showNotification(Ljava/lang/String;)V

    .line 138
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/server/SquareCallback;->networkError()V

    return-void
.end method
