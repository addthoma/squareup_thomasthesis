.class Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;
.super Ljava/lang/Object;
.source "StoreAndForwardTask.java"

# interfaces
.implements Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultUserData"
.end annotation


# instance fields
.field private storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

.field final synthetic this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

.field private transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask;)V
    .locals 0

    .line 572
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/StoreAndForwardTask$1;)V
    .locals 0

    .line 572
    invoke-direct {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;)V

    return-void
.end method


# virtual methods
.method public getStoredPayments()Lcom/squareup/queue/StoredPaymentsQueue;
    .locals 2

    .line 578
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    if-nez v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask;->dataDirectory:Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask;->userId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/user/Users;->getUserDirectory(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 580
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask;->queueCache:Lcom/squareup/queue/retrofit/QueueCache;

    invoke-static {v0, v1}, Lcom/squareup/queue/StoredPaymentsQueue;->getStoredPaymentsQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->storedPayments:Lcom/squareup/queue/StoredPaymentsQueue;

    return-object v0
.end method

.method public getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 2

    .line 586
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    if-nez v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask;->transactionLedgerManagerFactory:Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask;->userId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;->create(Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-object v0
.end method
