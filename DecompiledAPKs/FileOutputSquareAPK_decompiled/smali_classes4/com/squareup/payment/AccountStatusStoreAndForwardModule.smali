.class public abstract Lcom/squareup/payment/AccountStatusStoreAndForwardModule;
.super Ljava/lang/Object;
.source "AccountStatusStoreAndForwardModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindStoreAndForwardSetting(Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;)Lcom/squareup/payment/StoreAndForwardEnabledSetting;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
