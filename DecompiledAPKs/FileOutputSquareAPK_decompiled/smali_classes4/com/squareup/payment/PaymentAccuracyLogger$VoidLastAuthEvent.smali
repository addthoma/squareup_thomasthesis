.class Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;
.super Lcom/squareup/log/accuracy/PaymentAccuracyEvent;
.source "PaymentAccuracyLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentAccuracyLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VoidLastAuthEvent"
.end annotation


# instance fields
.field public final amountToReport:Lcom/squareup/protos/common/Money;

.field public final logReason:Ljava/lang/String;

.field public final reason:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public final voidableType:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 2

    .line 88
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->VOID_LAST_AUTH:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    invoke-direct {p0, v0, v1, p3}, Lcom/squareup/log/accuracy/PaymentAccuracyEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/protos/client/IdPair;)V

    .line 89
    iput-object p1, p0, Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;->reason:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 90
    iput-object p2, p0, Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;->logReason:Ljava/lang/String;

    .line 91
    iput-object p4, p0, Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;->amountToReport:Lcom/squareup/protos/common/Money;

    .line 92
    iput-object p5, p0, Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;->voidableType:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/payment/PaymentAccuracyLogger$1;)V
    .locals 0

    .line 80
    invoke-direct/range {p0 .. p5}, Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;-><init>(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    return-void
.end method
