.class Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;
.super Ljava/lang/Object;
.source "BillPaymentOnlineStrategy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/BillPaymentOnlineStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Factory"
.end annotation


# instance fields
.field private final backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

.field private final billCreationService:Lcom/squareup/server/bills/BillCreationService;


# direct methods
.method constructor <init>(Lcom/squareup/server/bills/BillCreationService;Lcom/squareup/payment/BackgroundCaptor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    .line 93
    iput-object p2, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;)Lcom/squareup/server/bills/BillCreationService;
    .locals 0

    .line 87
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;)Lcom/squareup/payment/BackgroundCaptor;
    .locals 0

    .line 87
    iget-object p0, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    return-object p0
.end method


# virtual methods
.method create(Ljava/lang/String;)Lcom/squareup/payment/BillPaymentOnlineStrategy;
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/payment/BillPaymentOnlineStrategy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/payment/BillPaymentOnlineStrategy;-><init>(Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;Ljava/lang/String;Lcom/squareup/payment/BillPaymentOnlineStrategy$1;)V

    return-object v0
.end method
