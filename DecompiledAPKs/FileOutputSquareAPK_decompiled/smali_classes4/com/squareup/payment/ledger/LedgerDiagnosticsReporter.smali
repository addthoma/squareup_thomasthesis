.class public Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;
.super Ljava/lang/Object;
.source "LedgerDiagnosticsReporter.java"


# instance fields
.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final transactionLedgerManagerFactory:Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;Lcom/squareup/log/OhSnapLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 24
    iput-object p2, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;->transactionLedgerManagerFactory:Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    .line 25
    iput-object p3, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    return-void
.end method


# virtual methods
.method public sendLedgerAndDiagnostics(Ljava/io/File;)Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->SERVER_CALL:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 31
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "Uploading zipped attachments: total %d bytes"

    .line 30
    invoke-static {v2, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 29
    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getId()Ljava/lang/String;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/squareup/payment/ledger/LedgerDiagnosticsReporter;->transactionLedgerManagerFactory:Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;

    invoke-interface {v2, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;->create(Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v0

    .line 36
    invoke-interface {v0, v1, p1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->uploadLedgerWithDiagnosticsData(Ljava/lang/String;Ljava/io/File;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
