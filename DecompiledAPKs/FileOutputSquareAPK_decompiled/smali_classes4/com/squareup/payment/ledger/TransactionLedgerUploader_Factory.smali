.class public final Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;
.super Ljava/lang/Object;
.source "TransactionLedgerUploader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/ledger/TransactionLedgerUploader;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final ioSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/transaction_ledger/TransactionLedgerService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/transaction_ledger/TransactionLedgerService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->transactionLedgerServiceProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->resProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->serverProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->clientProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->ioSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/transaction_ledger/TransactionLedgerService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/server/transaction_ledger/TransactionLedgerService;Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)Lcom/squareup/payment/ledger/TransactionLedgerUploader;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/ledger/TransactionLedgerUploader;-><init>(Lcom/squareup/server/transaction_ledger/TransactionLedgerService;Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/payment/ledger/TransactionLedgerUploader;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->transactionLedgerServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/server/transaction_ledger/TransactionLedgerService;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/http/Server;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->clientProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lokhttp3/OkHttpClient;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->ioSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v6}, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->newInstance(Lcom/squareup/server/transaction_ledger/TransactionLedgerService;Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lokhttp3/OkHttpClient;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/TransactionLedgerUploader_Factory;->get()Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    move-result-object v0

    return-object v0
.end method
