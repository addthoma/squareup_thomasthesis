.class public interface abstract Lcom/squareup/payment/ledger/TransactionLedgerManager;
.super Ljava/lang/Object;
.source "TransactionLedgerManager.java"

# interfaces
.implements Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;,
        Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;
    }
.end annotation


# virtual methods
.method public abstract clearLedger()V
.end method

.method public abstract close()V
.end method

.method public abstract emailLedger(Ljava/lang/String;)V
.end method

.method public abstract expireOldPayments()V
.end method

.method public abstract logAddTenderBeforeAuth(Lcom/squareup/payment/tender/BaseTender;)V
.end method

.method public abstract logAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V
.end method

.method public abstract logAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
.end method

.method public abstract logCancelBillEnqueued(Lcom/squareup/protos/client/bills/CancelBillRequest;)V
.end method

.method public abstract logCancelBillResponse(Lcom/squareup/protos/client/bills/CancelBillResponse;)V
.end method

.method public abstract logCancelEnqueued(Lcom/squareup/queue/Cancel;)V
.end method

.method public abstract logCaptureEnqueued(Lcom/squareup/queue/Capture;)V
.end method

.method public abstract logCaptureFailed(Lcom/squareup/queue/Capture;Ljava/lang/String;)V
.end method

.method public abstract logCaptureProcessed(Lcom/squareup/queue/Capture;)V
.end method

.method public abstract logCaptureTenderRequest(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)V
.end method

.method public abstract logCaptureTenderResponse(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)V
.end method

.method public abstract logCompleteBillEnqueued(Lcom/squareup/protos/client/bills/CompleteBillRequest;)V
.end method

.method public abstract logCompleteBillResponse(Lcom/squareup/protos/client/bills/CompleteBillResponse;)V
.end method

.method public abstract logIssueRefundsRequest(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)V
.end method

.method public abstract logIssueRefundsResponse(Lcom/squareup/protos/client/bills/IssueRefundsResponse;)V
.end method

.method public abstract logRemoveTendersRequest(Lcom/squareup/protos/client/bills/RemoveTendersRequest;)V
.end method

.method public abstract logRemoveTendersResponse(Lcom/squareup/protos/client/bills/RemoveTendersResponse;)V
.end method

.method public abstract logShowScreen(Ljava/lang/String;)V
.end method

.method public abstract logStoreAndForwardBillFailed(Lcom/squareup/payment/offline/BillInFlight;Ljava/lang/String;)V
.end method

.method public abstract logStoreAndForwardBillReady(Lcom/squareup/payment/offline/BillInFlight;Z)V
.end method

.method public abstract logStoreAndForwardPaymentEnqueued(Lcom/squareup/payment/offline/StoredPayment;)V
.end method

.method public abstract logStoreAndForwardPaymentProcessed(Lcom/squareup/payment/offline/StoredPayment;)V
.end method

.method public abstract logStoreAndForwardTaskStatus(Ljava/lang/String;)V
.end method

.method public abstract logVoidDanglingAuthorization(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract uploadLedger()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract uploadLedger(JJ)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract uploadLedgerWithDiagnosticsData(Ljava/lang/String;Ljava/io/File;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation
.end method
