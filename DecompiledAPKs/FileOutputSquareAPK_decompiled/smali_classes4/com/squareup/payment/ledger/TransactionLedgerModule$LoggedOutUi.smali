.class public Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedOutUi;
.super Ljava/lang/Object;
.source "TransactionLedgerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/TransactionLedgerModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoggedOutUi"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideScreenChangeLedgerManager(Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;)Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p0
.end method

.method static provideTransactionLedgerManager()Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/payment/ledger/NullTransactionLedgerManager;

    invoke-direct {v0}, Lcom/squareup/payment/ledger/NullTransactionLedgerManager;-><init>()V

    return-object v0
.end method
