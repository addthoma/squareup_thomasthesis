.class public final synthetic Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;


# instance fields
.field private final synthetic f$0:Landroid/app/Application;

.field private final synthetic f$1:Lcom/google/gson/Gson;

.field private final synthetic f$2:Ljava/io/File;

.field private final synthetic f$3:Lcom/squareup/util/Clock;

.field private final synthetic f$4:Ljava/util/concurrent/Executor;

.field private final synthetic f$5:Lcom/squareup/settings/server/Features;

.field private final synthetic f$6:Lcom/squareup/payment/ledger/TransactionLedgerUploader;


# direct methods
.method public synthetic constructor <init>(Landroid/app/Application;Lcom/google/gson/Gson;Ljava/io/File;Lcom/squareup/util/Clock;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$0:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$1:Lcom/google/gson/Gson;

    iput-object p3, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$2:Ljava/io/File;

    iput-object p4, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$3:Lcom/squareup/util/Clock;

    iput-object p5, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$4:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$5:Lcom/squareup/settings/server/Features;

    iput-object p7, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$6:Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 8

    iget-object v0, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$0:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$1:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$2:Ljava/io/File;

    iget-object v3, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$3:Lcom/squareup/util/Clock;

    iget-object v4, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$4:Ljava/util/concurrent/Executor;

    iget-object v5, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$5:Lcom/squareup/settings/server/Features;

    iget-object v6, p0, Lcom/squareup/payment/ledger/-$$Lambda$TransactionLedgerModule$LoggedOut$X1FblijWje72z2yfSBY9nAivxzY;->f$6:Lcom/squareup/payment/ledger/TransactionLedgerUploader;

    move-object v7, p1

    invoke-static/range {v0 .. v7}, Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedOut;->lambda$provideFactory$0(Landroid/app/Application;Lcom/google/gson/Gson;Ljava/io/File;Lcom/squareup/util/Clock;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/ledger/TransactionLedgerUploader;Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object p1

    return-object p1
.end method
