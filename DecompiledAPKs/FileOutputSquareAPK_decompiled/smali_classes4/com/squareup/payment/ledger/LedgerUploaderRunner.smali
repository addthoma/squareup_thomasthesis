.class public final Lcom/squareup/payment/ledger/LedgerUploaderRunner;
.super Ljava/lang/Object;
.source "LedgerUploaderRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLedgerUploaderRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LedgerUploaderRunner.kt\ncom/squareup/payment/ledger/LedgerUploaderRunner\n+ 2 PushMessageDelegate.kt\ncom/squareup/pushmessages/PushMessageDelegateKt\n*L\n1#1,32:1\n16#2:33\n*E\n*S KotlinDebug\n*F\n+ 1 LedgerUploaderRunner.kt\ncom/squareup/payment/ledger/LedgerUploaderRunner\n*L\n16#1:33\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u0008H\u0016J\u0010\u0010\u000c\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000eH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/payment/ledger/LedgerUploaderRunner;",
        "Lmortar/Scoped;",
        "pushMessageDelegate",
        "Lcom/squareup/pushmessages/PushMessageDelegate;",
        "transactionLedgerManager",
        "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
        "(Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "uploadLedger",
        "event",
        "Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final pushMessageDelegate:Lcom/squareup/pushmessages/PushMessageDelegate;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;


# direct methods
.method public constructor <init>(Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "pushMessageDelegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionLedgerManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerUploaderRunner;->pushMessageDelegate:Lcom/squareup/pushmessages/PushMessageDelegate;

    iput-object p2, p0, Lcom/squareup/payment/ledger/LedgerUploaderRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method

.method public static final synthetic access$uploadLedger(Lcom/squareup/payment/ledger/LedgerUploaderRunner;Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/LedgerUploaderRunner;->uploadLedger(Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;)V

    return-void
.end method

.method private final uploadLedger(Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;)V
    .locals 5

    .line 22
    iget-object v0, p0, Lcom/squareup/payment/ledger/LedgerUploaderRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    .line 23
    invoke-virtual {p1}, Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;->getStartTimestamp()J

    move-result-wide v1

    .line 24
    invoke-virtual {p1}, Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;->getEndTimestamp()J

    move-result-wide v3

    .line 22
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->uploadLedger(JJ)Lio/reactivex/Single;

    move-result-object p1

    .line 26
    invoke-virtual {p1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/squareup/payment/ledger/LedgerUploaderRunner;->pushMessageDelegate:Lcom/squareup/pushmessages/PushMessageDelegate;

    .line 33
    const-class v1, Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;

    invoke-interface {v0, v1}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 17
    new-instance v1, Lcom/squareup/payment/ledger/LedgerUploaderRunner$onEnterScope$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/payment/ledger/LedgerUploaderRunner;

    invoke-direct {v1, v2}, Lcom/squareup/payment/ledger/LedgerUploaderRunner$onEnterScope$1;-><init>(Lcom/squareup/payment/ledger/LedgerUploaderRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/payment/ledger/LedgerUploaderRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/payment/ledger/LedgerUploaderRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "pushMessageDelegate.obse\u2026subscribe(::uploadLedger)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
