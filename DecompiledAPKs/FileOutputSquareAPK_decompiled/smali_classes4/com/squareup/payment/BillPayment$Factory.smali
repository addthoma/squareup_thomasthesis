.class public Lcom/squareup/payment/BillPayment$Factory;
.super Ljava/lang/Object;
.source "BillPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/BillPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final advancedModifierLogger:Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

.field private final billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final gson:Lcom/google/gson/Gson;

.field private final localStrategyFactory:Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

.field private final mainScheduler:Lrx/Scheduler;

.field private final offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

.field private final offlineStrategyFactory:Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

.field private final onlineStrategyFactory:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

.field private final paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

.field private final receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

.field private final rpcScheduler:Lrx/Scheduler;

.field private final serverClock:Lcom/squareup/account/ServerClock;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

.field private final voidMonitor:Lcom/squareup/payment/VoidMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/BackgroundCaptor;Lcom/squareup/protos/common/CurrencyCode;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/payment/PaymentReceipt$Factory;Lrx/Scheduler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/settings/server/Features;)V
    .locals 2
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p7    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 1535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 1536
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-object v1, p2

    .line 1537
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    move-object v1, p3

    .line 1538
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p4

    .line 1539
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->mainScheduler:Lrx/Scheduler;

    move-object v1, p5

    .line 1540
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    move-object v1, p6

    .line 1541
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

    move-object v1, p7

    .line 1542
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->rpcScheduler:Lrx/Scheduler;

    move-object v1, p8

    .line 1543
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p9

    .line 1544
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->serverClock:Lcom/squareup/account/ServerClock;

    move-object v1, p10

    .line 1545
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->gson:Lcom/google/gson/Gson;

    move-object v1, p11

    .line 1546
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    move-object v1, p12

    .line 1547
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    move-object v1, p13

    .line 1548
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    move-object/from16 v1, p14

    .line 1549
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->advancedModifierLogger:Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    move-object/from16 v1, p15

    .line 1550
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->onlineStrategyFactory:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    move-object/from16 v1, p16

    .line 1551
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->offlineStrategyFactory:Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    move-object/from16 v1, p17

    .line 1552
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->localStrategyFactory:Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    move-object/from16 v1, p18

    .line 1553
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object/from16 v1, p19

    .line 1554
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p20

    .line 1555
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    move-object/from16 v1, p21

    .line 1556
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    move-object/from16 v1, p22

    .line 1557
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->voidMonitor:Lcom/squareup/payment/VoidMonitor;

    move-object/from16 v1, p23

    .line 1558
    iput-object v1, v0, Lcom/squareup/payment/BillPayment$Factory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/payment/BillPayment$Factory;)Lcom/google/gson/Gson;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->gson:Lcom/google/gson/Gson;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/OfflineModeMonitor;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/PaymentAccuracyLogger;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->advancedModifierLogger:Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->onlineStrategyFactory:Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->offlineStrategyFactory:Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    return-object p0
.end method

.method static synthetic access$1700(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->localStrategyFactory:Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/TenderInEdit;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-object p0
.end method

.method static synthetic access$1900(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BackgroundCaptor;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->backgroundCaptor:Lcom/squareup/payment/BackgroundCaptor;

    return-object p0
.end method

.method static synthetic access$2000(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/VoidMonitor;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->voidMonitor:Lcom/squareup/payment/VoidMonitor;

    return-object p0
.end method

.method static synthetic access$2100(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/BillPaymentEvents;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/BillPayment$Factory;)Lrx/Scheduler;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->mainScheduler:Lrx/Scheduler;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/receiving/StandardReceiver;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/payment/PaymentReceipt$Factory;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/payment/BillPayment$Factory;)Lrx/Scheduler;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->rpcScheduler:Lrx/Scheduler;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/payment/BillPayment$Factory;)Lcom/squareup/account/ServerClock;
    .locals 0

    .line 1497
    iget-object p0, p0, Lcom/squareup/payment/BillPayment$Factory;->serverClock:Lcom/squareup/account/ServerClock;

    return-object p0
.end method


# virtual methods
.method public create(ZLcom/squareup/payment/Order;Lcom/squareup/settings/server/TipSettings;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/payment/BillPayment;
    .locals 9

    const-string v4, "NO_CLIENT_ID"

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v7, p4

    move-object v8, p5

    .line 1567
    invoke-virtual/range {v0 .. v8}, Lcom/squareup/payment/BillPayment$Factory;->create(ZLcom/squareup/payment/Order;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZLcom/squareup/checkoutflow/services/NetworkRequestModifier;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/payment/BillPayment;

    move-result-object p1

    return-object p1
.end method

.method public create(ZLcom/squareup/payment/Order;Lcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZLcom/squareup/checkoutflow/services/NetworkRequestModifier;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/payment/BillPayment;
    .locals 14

    move-object v12, p0

    .line 1580
    new-instance v13, Lcom/squareup/payment/BillPayment;

    iget-object v10, v12, Lcom/squareup/payment/BillPayment$Factory;->features:Lcom/squareup/settings/server/Features;

    const/4 v11, 0x0

    move-object v0, v13

    move-object v1, p0

    move-object/from16 v2, p2

    move v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v11}, Lcom/squareup/payment/BillPayment;-><init>(Lcom/squareup/payment/BillPayment$Factory;Lcom/squareup/payment/Order;ZLcom/squareup/settings/server/TipSettings;Ljava/lang/String;ZLcom/squareup/checkoutflow/services/NetworkRequestModifier;Lcom/squareup/payment/PaymentFlowTaskProvider;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/BillPayment$1;)V

    .line 1584
    iget-object v0, v12, Lcom/squareup/payment/BillPayment$Factory;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    invoke-virtual {v0, v13}, Lcom/squareup/payment/BillPaymentEvents;->emitNewBillPayment(Lcom/squareup/payment/BillPayment;)V

    return-object v13
.end method
