.class public final enum Lcom/squareup/payment/OrderEntryEvents$ItemType;
.super Ljava/lang/Enum;
.source "OrderEntryEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/OrderEntryEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/payment/OrderEntryEvents$ItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/payment/OrderEntryEvents$ItemType;

.field public static final enum CART_ITEM:Lcom/squareup/payment/OrderEntryEvents$ItemType;

.field public static final enum DISCOUNT:Lcom/squareup/payment/OrderEntryEvents$ItemType;

.field public static final enum TAX:Lcom/squareup/payment/OrderEntryEvents$ItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 30
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;

    const/4 v1, 0x0

    const-string v2, "CART_ITEM"

    invoke-direct {v0, v2, v1}, Lcom/squareup/payment/OrderEntryEvents$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;->CART_ITEM:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    .line 31
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;

    const/4 v2, 0x1

    const-string v3, "DISCOUNT"

    invoke-direct {v0, v3, v2}, Lcom/squareup/payment/OrderEntryEvents$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;->DISCOUNT:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    .line 32
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;

    const/4 v3, 0x2

    const-string v4, "TAX"

    invoke-direct {v0, v4, v3}, Lcom/squareup/payment/OrderEntryEvents$ItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;->TAX:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/payment/OrderEntryEvents$ItemType;

    .line 29
    sget-object v4, Lcom/squareup/payment/OrderEntryEvents$ItemType;->CART_ITEM:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ItemType;->DISCOUNT:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ItemType;->TAX:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;->$VALUES:[Lcom/squareup/payment/OrderEntryEvents$ItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/payment/OrderEntryEvents$ItemType;
    .locals 1

    .line 29
    const-class v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/OrderEntryEvents$ItemType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/payment/OrderEntryEvents$ItemType;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;->$VALUES:[Lcom/squareup/payment/OrderEntryEvents$ItemType;

    invoke-virtual {v0}, [Lcom/squareup/payment/OrderEntryEvents$ItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/payment/OrderEntryEvents$ItemType;

    return-object v0
.end method
