.class public Lcom/squareup/payment/pending/PaymentNotifierJobCreator;
.super Lcom/squareup/backgroundjob/BackgroundJobCreator;
.source "PaymentNotifierJobCreator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final paymentNotifier:Lcom/squareup/payment/pending/PaymentNotifier;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/payment/pending/PaymentNotifier;)V
    .locals 0

    .line 37
    invoke-direct {p0, p4}, Lcom/squareup/backgroundjob/BackgroundJobCreator;-><init>(Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    .line 38
    iput-object p1, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;->application:Landroid/app/Application;

    .line 39
    iput-object p2, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 40
    iput-object p3, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    .line 41
    iput-object p5, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;->paymentNotifier:Lcom/squareup/payment/pending/PaymentNotifier;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;)Lcom/evernote/android/job/Job;
    .locals 4

    .line 45
    sget-object v0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->UPDATE_TAG:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;

    iget-object v0, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;->application:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v2, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iget-object v3, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator;->paymentNotifier:Lcom/squareup/payment/pending/PaymentNotifier;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/payment/pending/PaymentNotifier;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method
