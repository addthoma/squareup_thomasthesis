.class public final Lcom/squareup/payment/pending/PendingTransactionComparator;
.super Ljava/lang/Object;
.source "PendingTransactionComparator.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0003B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0008\u001a\u00020\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/payment/pending/PendingTransactionComparator;",
        "Ljava/util/Comparator;",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "Lkotlin/Comparator;",
        "()V",
        "compare",
        "",
        "lhs",
        "rhs",
        "payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/pending/PendingTransactionComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/payment/pending/PendingTransactionComparator;

    invoke-direct {v0}, Lcom/squareup/payment/pending/PendingTransactionComparator;-><init>()V

    sput-object v0, Lcom/squareup/payment/pending/PendingTransactionComparator;->INSTANCE:Lcom/squareup/payment/pending/PendingTransactionComparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;)I
    .locals 1

    const-string v0, "lhs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rhs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-virtual {p2}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getDate()Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;

    check-cast p2, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/payment/pending/PendingTransactionComparator;->compare(Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;)I

    move-result p1

    return p1
.end method
