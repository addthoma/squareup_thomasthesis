.class public Lcom/squareup/payment/InvoicePayment$Factory;
.super Ljava/lang/Object;
.source "InvoicePayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/InvoicePayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final receiptFactory:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentReceipt$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentReceipt$Factory;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment$Factory;->receiptFactory:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/payment/InvoicePayment$Factory;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/InvoicePayment$Factory;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/payment/InvoicePayment$Factory;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/InvoicePayment$Factory;)Ljavax/inject/Provider;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/payment/InvoicePayment$Factory;->receiptFactory:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public create(Lcom/squareup/payment/Order;)Lcom/squareup/payment/InvoicePayment;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/payment/InvoicePayment;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/InvoicePayment;-><init>(Lcom/squareup/payment/InvoicePayment$Factory;Lcom/squareup/payment/Order;)V

    return-object v0
.end method
