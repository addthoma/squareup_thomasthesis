.class public final Lcom/squareup/payment/OrderCoursingHandler;
.super Ljava/lang/Object;
.source "OrderCoursingHandler.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderCoursingHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderCoursingHandler.kt\ncom/squareup/payment/OrderCoursingHandler\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,83:1\n704#2:84\n777#2,2:85\n1529#2,3:87\n1360#2:90\n1429#2,3:91\n*E\n*S KotlinDebug\n*F\n+ 1 OrderCoursingHandler.kt\ncom/squareup/payment/OrderCoursingHandler\n*L\n29#1:84\n29#1,2:85\n56#1,3:87\n81#1:90\n81#1,3:91\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0007J\u0018\u0010\u0016\u001a\u00020\u00072\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aJ\u0018\u0010\u001b\u001a\u00020\u00072\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aJ\u0008\u0010\u001c\u001a\u00020\u001dH\u0002J*\u0010\u001e\u001a\n  *\u0004\u0018\u00010\u001f0\u001f2\u0006\u0010!\u001a\u00020\"2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\"\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0010\n\u001a\u0004\u0018\u00010\u0003@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u00020\u000e8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u0017\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\t\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/payment/OrderCoursingHandler;",
        "",
        "coursing",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;",
        "(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)V",
        "courses",
        "",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
        "getCourses",
        "()Ljava/util/List;",
        "<set-?>",
        "getCoursing",
        "()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;",
        "nextCourseOrdinal",
        "",
        "getNextCourseOrdinal",
        "()I",
        "straightFiredCourses",
        "getStraightFiredCourses",
        "addCourse",
        "",
        "course",
        "addNewCourse",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "currentDate",
        "Ljava/util/Date;",
        "addNewStraightFireCourse",
        "coursingBuilder",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;",
        "event",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;",
        "kotlin.jvm.PlatformType",
        "type",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/payment/OrderCoursingHandler;->coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    return-void
.end method

.method private final coursingBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/payment/OrderCoursingHandler;->coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;-><init>()V

    :goto_0
    return-object v0
.end method

.method private final event(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;
    .locals 4

    .line 69
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;-><init>()V

    .line 70
    new-instance v1, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->event_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->event_type(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;

    move-result-object p1

    .line 73
    new-instance v0, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 74
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object p2

    .line 75
    invoke-virtual {p2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p2

    .line 72
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;

    move-result-object p1

    .line 77
    new-instance p2, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p3}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;

    move-result-object p1

    return-object p1
.end method

.method private final getNextCourseOrdinal()I
    .locals 3

    .line 81
    invoke-virtual {p0}, Lcom/squareup/payment/OrderCoursingHandler;->getCourses()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 90
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 91
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 92
    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 81
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->ordinal:Ljava/lang/Integer;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 81
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->max(Ljava/lang/Iterable;)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public final addCourse(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;)V
    .locals 5

    const-string v0, "course"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/payment/OrderCoursingHandler;->coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 55
    :goto_0
    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    .line 87
    instance-of v2, v1, Ljava/util/Collection;

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 88
    :cond_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 56
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v3

    if-nez v2, :cond_2

    const/4 v3, 0x0

    :cond_3
    :goto_1
    if-eqz v3, :cond_4

    .line 57
    check-cast v0, Ljava/util/Collection;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 58
    invoke-direct {p0}, Lcom/squareup/payment/OrderCoursingHandler;->coursingBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;->course(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/OrderCoursingHandler;->coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    return-void

    .line 56
    :cond_4
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 55
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final addNewCourse(Lcom/squareup/protos/client/Employee;Ljava/util/Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;
    .locals 4

    const-string v0, "currentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;-><init>()V

    .line 35
    new-instance v1, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->course_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object v0

    .line 36
    invoke-direct {p0}, Lcom/squareup/payment/OrderCoursingHandler;->getNextCourseOrdinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 37
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->straight_fire(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object v0

    .line 38
    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;->CREATION:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    invoke-direct {p0, v1, p1, p2}, Lcom/squareup/payment/OrderCoursingHandler;->event(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->event(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    move-result-object p1

    const-string p2, "it"

    .line 40
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderCoursingHandler;->addCourse(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;)V

    const-string p2, "Course.Builder()\n       \u2026  .also { addCourse(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final addNewStraightFireCourse(Lcom/squareup/protos/client/Employee;Ljava/util/Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;
    .locals 4

    const-string v0, "currentDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;-><init>()V

    .line 46
    new-instance v1, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->course_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 47
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 48
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->straight_fire(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object v0

    .line 49
    sget-object v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;->CREATION:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;

    invoke-direct {p0, v1, p1, p2}, Lcom/squareup/payment/OrderCoursingHandler;->event(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Event;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->event(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    move-result-object p1

    const-string p2, "it"

    .line 51
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderCoursingHandler;->addCourse(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;)V

    const-string p2, "Course.Builder()\n      .\u2026  .also { addCourse(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getCourses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/payment/OrderCoursingHandler;->coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final getCoursing()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/payment/OrderCoursingHandler;->coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    return-object v0
.end method

.method public final getStraightFiredCourses()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;"
        }
    .end annotation

    .line 29
    invoke-virtual {p0}, Lcom/squareup/payment/OrderCoursingHandler;->getCourses()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 85
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;

    .line 29
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;->straight_fire:Ljava/lang/Boolean;

    const-string v4, "it.straight_fire"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 86
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method
