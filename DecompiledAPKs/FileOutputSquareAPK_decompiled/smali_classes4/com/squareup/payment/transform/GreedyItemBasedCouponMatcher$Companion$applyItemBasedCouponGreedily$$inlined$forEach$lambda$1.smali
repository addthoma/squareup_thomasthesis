.class final Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$$inlined$forEach$lambda$1;
.super Ljava/lang/Object;
.source "GreedyItemBasedCouponMatcher.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;->applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/checkout/CartItem$Builder;",
        "Lcom/squareup/checkout/CartItem$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/checkout/CartItem$Builder;",
        "kotlin.jvm.PlatformType",
        "builder",
        "call",
        "com/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$3$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cartBuilder$inlined:Lcom/squareup/checkout/Cart$Builder;

.field final synthetic $discount$inlined:Lcom/squareup/checkout/Discount;


# direct methods
.method constructor <init>(Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/checkout/Discount;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$$inlined$forEach$lambda$1;->$cartBuilder$inlined:Lcom/squareup/checkout/Cart$Builder;

    iput-object p2, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$$inlined$forEach$lambda$1;->$discount$inlined:Lcom/squareup/checkout/Discount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$$inlined$forEach$lambda$1;->$discount$inlined:Lcom/squareup/checkout/Discount;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 76
    iget-object v0, p0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$$inlined$forEach$lambda$1;->$discount$inlined:Lcom/squareup/checkout/Discount;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->removeDiscountAddEvents(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion$applyItemBasedCouponGreedily$$inlined$forEach$lambda$1;->call(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    return-object p1
.end method
