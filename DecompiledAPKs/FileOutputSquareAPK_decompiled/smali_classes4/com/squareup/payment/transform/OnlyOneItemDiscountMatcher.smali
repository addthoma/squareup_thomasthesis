.class public Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;
.super Ljava/lang/Object;
.source "OnlyOneItemDiscountMatcher.java"

# interfaces
.implements Lrx/functions/Action2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action2<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/checkout/CartItem;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private bestMatchIndex:Ljava/lang/Integer;

.field private bestMatchItem:Lcom/squareup/checkout/CartItem;

.field private final discount:Lcom/squareup/checkout/Discount;

.field private final indexesWithDiscount:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/checkout/Discount;)V
    .locals 2

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->indexesWithDiscount:Ljava/util/List;

    .line 44
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v0

    const-string v1, "OnlyItemItemDiscountMatcher discount cannot be applied to only one item"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    return-void
.end method

.method private isBetterMatching(Lcom/squareup/checkout/CartItem;)Z
    .locals 3

    .line 61
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Discount;->isItemEligible(Lcom/squareup/checkout/CartItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 67
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem;->isLessExpensiveThan(Lcom/squareup/checkout/CartItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem;->isAsExpensiveAs(Lcom/squareup/checkout/CartItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    sget-object v2, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-static {v0, v2}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    sget-object v2, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-static {v0, v2}, Lcom/squareup/util/BigDecimals;->equalsIgnoringScale(Ljava/math/BigDecimal;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    .line 80
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    return v1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public apply(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;)V
    .locals 1

    const/4 v0, 0x0

    .line 92
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->apply(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method public apply(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V
    .locals 3

    .line 96
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->indexesWithDiscount:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchIndex:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 99
    iget-object v1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->indexesWithDiscount:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->indexesWithDiscount:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 104
    new-instance v2, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$xd7IZuGnb8NkikugghJJ8uquum8;

    invoke-direct {v2, p0}, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$xd7IZuGnb8NkikugghJJ8uquum8;-><init>(Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;)V

    invoke-virtual {p2, v1, v2}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    if-eqz v0, :cond_5

    .line 111
    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-static {v0, v1}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v1, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$XAluHALxADk91IVRNxr72p2PmmM;

    invoke-direct {v1, p0}, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$XAluHALxADk91IVRNxr72p2PmmM;-><init>(Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    sget-object v2, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/payment/Order;->splitItem(ILcom/squareup/protos/client/Employee;Ljava/math/BigDecimal;)V

    .line 122
    iget-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchIndex:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    new-instance v0, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$70D8vFIesNTqm0J7MXnPtID1N4w;

    invoke-direct {v0, p0, p3}, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$70D8vFIesNTqm0J7MXnPtID1N4w;-><init>(Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;Lcom/squareup/protos/client/Employee;)V

    invoke-virtual {p2, p1, v0}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    goto :goto_1

    .line 129
    :cond_3
    iget-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-static {p1, v0}, Lcom/squareup/util/BigDecimals;->equalsIgnoringScale(Ljava/math/BigDecimal;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    .line 130
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 132
    iget-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchIndex:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    new-instance v0, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$vJ-w_hB32Y1QPywk4t0XCyHZIaw;

    invoke-direct {v0, p0, p3}, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$vJ-w_hB32Y1QPywk4t0XCyHZIaw;-><init>(Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;Lcom/squareup/protos/client/Employee;)V

    invoke-virtual {p2, p1, v0}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    goto :goto_1

    .line 139
    :cond_4
    iget-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    sget-object p3, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-static {p1, p3}, Lcom/squareup/util/BigDecimals;->lessThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    .line 140
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object p1

    iget-object p3, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {p3}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 142
    iget-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchIndex:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    new-instance p3, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$hDGFKl_lcpQoD9DlXDuCQ-WaQHQ;

    invoke-direct {p3, p0}, Lcom/squareup/payment/transform/-$$Lambda$OnlyOneItemDiscountMatcher$hDGFKl_lcpQoD9DlXDuCQ-WaQHQ;-><init>(Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;)V

    invoke-virtual {p2, p1, p3}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    :cond_5
    :goto_1
    return-void
.end method

.method public call(Ljava/lang/Integer;Lcom/squareup/checkout/CartItem;)V
    .locals 2

    .line 50
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->indexesWithDiscount:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->isBetterMatching(Lcom/squareup/checkout/CartItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    iput-object p2, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchItem:Lcom/squareup/checkout/CartItem;

    .line 56
    iput-object p1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->bestMatchIndex:Ljava/lang/Integer;

    :cond_1
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Lcom/squareup/checkout/CartItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->call(Ljava/lang/Integer;Lcom/squareup/checkout/CartItem;)V

    return-void
.end method

.method public synthetic lambda$apply$0$OnlyOneItemDiscountMatcher(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    iget-object v0, v0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    iget-object v0, v0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    .line 106
    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->removeDiscountAddEvents(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$apply$1$OnlyOneItemDiscountMatcher(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    iget-object v0, v0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    iget-object v0, v0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    .line 117
    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->removeDiscountAddEvents(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$apply$2$OnlyOneItemDiscountMatcher(Lcom/squareup/protos/client/Employee;Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {p2, v0}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    if-eqz p1, :cond_0

    .line 125
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/checkout/util/ItemizationEvents;->discountAddEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_0
    return-object p2
.end method

.method public synthetic lambda$apply$3$OnlyOneItemDiscountMatcher(Lcom/squareup/protos/client/Employee;Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {p2, v0}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    if-eqz p1, :cond_0

    .line 135
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/checkout/util/ItemizationEvents;->discountAddEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_0
    return-object p2
.end method

.method public synthetic lambda$apply$4$OnlyOneItemDiscountMatcher(Lcom/squareup/checkout/CartItem$Builder;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    iget-object v0, v0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->discount:Lcom/squareup/checkout/Discount;

    iget-object v1, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    .line 144
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->removeDiscountAddEvents(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    return-object p1
.end method
