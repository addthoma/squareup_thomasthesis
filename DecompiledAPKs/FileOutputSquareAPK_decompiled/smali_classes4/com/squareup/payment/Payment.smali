.class public abstract Lcom/squareup/payment/Payment;
.super Ljava/lang/Object;
.source "Payment.java"

# interfaces
.implements Lcom/squareup/payment/crm/HoldsCustomer;


# static fields
.field protected static final note:Ljava/lang/String; = ""


# instance fields
.field protected final analytics:Lcom/squareup/analytics/Analytics;

.field private availableCoupon:Lcom/squareup/protos/client/coupons/Coupon;

.field private order:Lcom/squareup/payment/OrderSnapshot;

.field protected tip:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Order;Lcom/squareup/analytics/Analytics;)V
    .locals 1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "order"

    .line 51
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/Order;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->snapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/Payment;->order:Lcom/squareup/payment/OrderSnapshot;

    const/4 p1, 0x0

    .line 52
    iput-object p1, p0, Lcom/squareup/payment/Payment;->tip:Lcom/squareup/protos/common/Money;

    .line 53
    iput-object p2, p0, Lcom/squareup/payment/Payment;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public applyCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/payment/Order;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/payment/Payment;->order:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/OrderSnapshot;->newSnapshotWithCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/Payment;->order:Lcom/squareup/payment/OrderSnapshot;

    .line 79
    iget-object p1, p0, Lcom/squareup/payment/Payment;->order:Lcom/squareup/payment/OrderSnapshot;

    return-object p1
.end method

.method public blockOfflineModeEntry()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public blockOfflineModeExit()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public capture(Z)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 116
    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->isCaptured()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->createTaxRuleAppliedInTransactionEvent()Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 120
    iget-object v0, p0, Lcom/squareup/payment/Payment;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    const/4 p1, 0x1

    return p1

    .line 116
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Default capture check failed"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method abstract endCurrentTenderAndCreateReceipt()Lcom/squareup/payment/PaymentReceipt;
.end method

.method public abstract enqueueAttachContactTask()V
.end method

.method public getAuthorizationId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getAvailableCoupon()Lcom/squareup/protos/client/coupons/Coupon;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/payment/Payment;->availableCoupon:Lcom/squareup/protos/client/coupons/Coupon;

    return-object v0
.end method

.method public getBillId()Lcom/squareup/protos/client/IdPair;
    .locals 2

    .line 201
    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->getUniqueClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public getOrder()Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/payment/Payment;->order:Lcom/squareup/payment/OrderSnapshot;

    return-object v0
.end method

.method public getReceiptNumber()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method

.method public getTenderAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public abstract getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/payment/Payment;->order:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getTicketId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/payment/Payment;->tip:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 101
    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueClientId()Ljava/lang/String;
    .locals 1

    .line 193
    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getSensitiveValues()Lcom/squareup/payment/Order$SensitiveValues;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/payment/Order$SensitiveValues;->uniqueClientId:Ljava/lang/String;

    return-object v0
.end method

.method public abstract isCaptured()Z
.end method

.method public isComplete()Z
    .locals 1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/payment/Payment;->isCaptured()Z

    move-result v0

    return v0
.end method

.method public abstract isLocalPayment()Z
.end method

.method public isStoreAndForward()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setAvailableCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/payment/Payment;->availableCoupon:Lcom/squareup/protos/client/coupons/Coupon;

    return-void
.end method

.method public setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/payment/Payment;->tip:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public shouldAutoSendReceipt()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract shouldSkipReceipt()Z
.end method
