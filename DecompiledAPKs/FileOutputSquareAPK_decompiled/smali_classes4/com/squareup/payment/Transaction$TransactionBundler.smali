.class Lcom/squareup/payment/Transaction$TransactionBundler;
.super Ljava/lang/Object;
.source "Transaction.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TransactionBundler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Transaction;)V
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 193
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 201
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$000(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Order;

    move-result-object v1

    if-nez v1, :cond_0

    .line 202
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$100(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Order;

    invoke-static {v1, v2}, Lcom/squareup/payment/Transaction;->access$002(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order;

    .line 203
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$200(Lcom/squareup/payment/Transaction;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v2}, Lcom/squareup/payment/Transaction;->access$000(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Order;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 204
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$400(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/TipSettings;

    invoke-static {v1, v2}, Lcom/squareup/payment/Transaction;->access$302(Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/TipSettings;)Lcom/squareup/settings/server/TipSettings;

    const-string v1, "SKIP_RECEIPT_SCREEN_STRATEGY"

    .line 205
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 206
    iget-object v2, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {}, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->values()[Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    move-result-object v3

    aget-object v1, v3, v1

    invoke-static {v2, v1}, Lcom/squareup/payment/Transaction;->access$502(Lcom/squareup/payment/Transaction;Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;)Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    .line 207
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    const-string v2, "SKIP_SIGNATURE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/squareup/payment/Transaction;->access$602(Lcom/squareup/payment/Transaction;Z)Z

    .line 208
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    const-string v2, "API_CLIENT_ID"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/payment/Transaction;->access$702(Lcom/squareup/payment/Transaction;Ljava/lang/String;)Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$900(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-static {v1, v2}, Lcom/squareup/payment/Transaction;->access$802(Lcom/squareup/payment/Transaction;Ljava/util/Map;)Ljava/util/Map;

    .line 210
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$1100(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v1, v2}, Lcom/squareup/payment/Transaction;->access$1002(Lcom/squareup/payment/Transaction;Ljava/util/List;)Ljava/util/List;

    .line 211
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$1300(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v1, v2}, Lcom/squareup/payment/Transaction;->access$1202(Lcom/squareup/payment/Transaction;Ljava/util/List;)Ljava/util/List;

    .line 212
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$000(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Order;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$000(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->init()V

    .line 214
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$1400(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Payment;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "HAS_INVOICE_PAYMENT"

    .line 215
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->startInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    move-result-object v1

    .line 218
    invoke-virtual {v1, p1}, Lcom/squareup/payment/InvoicePayment;->restore(Landroid/os/Bundle;)V

    .line 223
    :cond_0
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    const-string v2, "TICKET_PROTO_KEY"

    invoke-static {v2, p1}, Lcom/squareup/tickets/OpenTicket;->loadFromBundle(Ljava/lang/String;Landroid/os/Bundle;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/payment/Transaction;->access$1502(Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/tickets/OpenTicket;

    .line 225
    :cond_1
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$000(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Order;

    move-result-object v1

    if-nez v1, :cond_2

    .line 226
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->reset()V

    .line 229
    :cond_2
    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$1400(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Payment;

    move-result-object v2

    if-nez v2, :cond_3

    if-eqz p1, :cond_3

    const-string v2, "HAD_PAYMENT"

    .line 230
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    .line 229
    :cond_3
    invoke-static {v1, v0}, Lcom/squareup/payment/Transaction;->access$1602(Lcom/squareup/payment/Transaction;Z)Z

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$100(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$000(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$400(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$300(Lcom/squareup/payment/Transaction;)Lcom/squareup/settings/server/TipSettings;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$900(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$800(Lcom/squareup/payment/Transaction;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$1100(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$1000(Lcom/squareup/payment/Transaction;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 238
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$1300(Lcom/squareup/payment/Transaction;)Lcom/squareup/BundleKey;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v1}, Lcom/squareup/payment/Transaction;->access$1200(Lcom/squareup/payment/Transaction;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 239
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$1400(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Payment;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v2, "HAD_PAYMENT"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 240
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$1500(Lcom/squareup/payment/Transaction;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    const-string v2, "TICKET_PROTO_KEY"

    invoke-virtual {v0, v2, p1}, Lcom/squareup/tickets/OpenTicket;->saveToBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 246
    invoke-virtual {v0, p1}, Lcom/squareup/payment/InvoicePayment;->save(Landroid/os/Bundle;)V

    const-string v0, "HAS_INVOICE_PAYMENT"

    .line 247
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$500(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->ordinal()I

    move-result v0

    const-string v1, "SKIP_RECEIPT_SCREEN_STRATEGY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 250
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$600(Lcom/squareup/payment/Transaction;)Z

    move-result v0

    const-string v1, "SKIP_SIGNATURE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 251
    iget-object v0, p0, Lcom/squareup/payment/Transaction$TransactionBundler;->this$0:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/payment/Transaction;->access$700(Lcom/squareup/payment/Transaction;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "API_CLIENT_ID"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
