.class public Lcom/squareup/payment/CardConverter;
.super Ljava/lang/Object;
.source "CardConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/CardConverter$FallbackKeyAdapter;
    }
.end annotation


# instance fields
.field private final queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/offline/QueueBertPublicKeyManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/payment/CardConverter;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/CardConverter;Lcom/squareup/protos/client/bills/CardData$Builder;Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/CardConverter;->addEncryptedCardData(Lcom/squareup/protos/client/bills/CardData$Builder;Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    return-object p0
.end method

.method private addEncryptedCardData(Lcom/squareup/protos/client/bills/CardData$Builder;Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 4

    .line 88
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;-><init>()V

    .line 89
    invoke-virtual {p2}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;-><init>()V

    .line 91
    invoke-virtual {p2}, Lcom/squareup/Card;->getExpirationMonth()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->month(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object v1

    .line 92
    invoke-virtual {p2}, Lcom/squareup/Card;->getExpirationYear()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->year(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {p2}, Lcom/squareup/Card;->getVerification()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->cvv(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v0

    .line 95
    invoke-virtual {p2}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object p2

    .line 96
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object p2

    .line 100
    iget-object v0, p0, Lcom/squareup/payment/CardConverter;->queueBertPublicKeyManager:Lcom/squareup/payment/offline/QueueBertPublicKeyManager;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/squareup/payment/offline/QueueBertPublicKeyManager;->getEncryptor()Lcom/squareup/encryption/JweEncryptor;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {v0}, Lcom/squareup/encryption/JweEncryptor;->isExpired()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 108
    :cond_1
    :try_start_0
    new-instance v0, Lcom/squareup/encryption/JweEncryptor;

    new-instance v2, Lcom/squareup/payment/CardConverter$FallbackKeyAdapter;

    invoke-direct {v2}, Lcom/squareup/payment/CardConverter$FallbackKeyAdapter;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/squareup/encryption/JweEncryptor;-><init>(Ljava/lang/Object;Lcom/squareup/encryption/CryptoKeyAdapter;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 118
    :cond_2
    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    const-string v2, "cty"

    const-string v3, "application/x-protobuf"

    .line 119
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "typ"

    const-string v3, "com.squareup.protos.bills.CardData.KeyedCard"

    .line 120
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-virtual {v0, v1}, Lcom/squareup/encryption/JweEncryptor;->withAdditionalHeaders(Ljava/util/Map;)Lcom/squareup/encryption/JweEncryptor;

    move-result-object v0

    .line 125
    :try_start_1
    sget-object v1, Lcom/squareup/protos/client/bills/CardData$KeyedCard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 126
    invoke-virtual {v1, p2}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/encryption/JweEncryptor;->compute([B)Lcom/squareup/encryption/CryptoResult;

    move-result-object p2

    .line 127
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ENCRYPTED_KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;-><init>()V

    .line 129
    invoke-virtual {p2}, Lcom/squareup/encryption/CryptoResult;->getValue()[B

    move-result-object p2

    invoke-static {p2}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->encrypted_keyed_card_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;

    move-result-object p2

    .line 130
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    move-result-object p2

    .line 128
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bills/CardData$Builder;->encrypted_keyed_card(Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 133
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    move-exception p1

    .line 111
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method


# virtual methods
.method public getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;
    .locals 2

    .line 31
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 32
    new-instance v1, Lcom/squareup/payment/CardConverter$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/payment/CardConverter$1;-><init>(Lcom/squareup/payment/CardConverter;Lcom/squareup/protos/client/bills/CardData$Builder;Lcom/squareup/Card;)V

    invoke-virtual {p1, v1}, Lcom/squareup/Card;->handleInputType(Lcom/squareup/Card$InputType$InputTypeHandler;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/CardData$Builder;

    .line 79
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    return-object p1
.end method
