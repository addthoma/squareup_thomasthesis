.class public interface abstract Lcom/squareup/payment/AcceptsSignature;
.super Ljava/lang/Object;
.source "AcceptsSignature.java"


# virtual methods
.method public abstract askForSignature()Z
.end method

.method public abstract getSignatureRequiredThreshold()Lcom/squareup/protos/common/Money;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract setVectorSignature(Lcom/squareup/signature/SignatureAsJson;)V
.end method

.method public abstract signOnPrintedReceipt()Z
.end method
