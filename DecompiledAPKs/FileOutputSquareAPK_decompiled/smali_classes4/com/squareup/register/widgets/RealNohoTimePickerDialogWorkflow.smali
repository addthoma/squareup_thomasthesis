.class public final Lcom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow;
.super Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;
.source "RealNohoTimePickerDialogWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNohoTimePickerDialogWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNohoTimePickerDialogWorkflow.kt\ncom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,46:1\n149#2,5:47\n*E\n*S KotlinDebug\n*F\n+ 1 RealNohoTimePickerDialogWorkflow.kt\ncom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow\n*L\n34#1,5:47\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016JN\u0010\u0008\u001a$\u0012\u0004\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u000c0\tj\u0008\u0012\u0004\u0012\u00020\n`\r2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00042\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00110\u0010H\u0016J\u0010\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u0004H\u0016\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow;",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;",
        "()V",
        "initialState",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogResult;",
        "snapshotState",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Lcom/squareup/register/widgets/NohoTimePickerDialogWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 42
    sget-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->Companion:Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object p2

    if-eqz p2, :cond_0

    move-object p1, p2

    :cond_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow;->initialState(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    check-cast p2, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow;->render(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
            "-",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    .line 26
    new-instance v1, Lcom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow$render$dialog$1;

    invoke-direct {v1, p1}, Lcom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow$render$dialog$1;-><init>(Lcom/squareup/register/widgets/NohoTimePickerDialogState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 25
    invoke-direct {v0, p2, p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;-><init>(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 48
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 49
    const-class p2, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 50
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 48
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 35
    sget-object p2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 p3, 0x1

    new-array p3, p3, [Lkotlin/Pair;

    sget-object v0, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v0, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    const/4 v0, 0x0

    aput-object p1, p3, v0

    invoke-virtual {p2, p3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/register/widgets/NohoTimePickerDialogState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/RealNohoTimePickerDialogWorkflow;->snapshotState(Lcom/squareup/register/widgets/NohoTimePickerDialogState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
