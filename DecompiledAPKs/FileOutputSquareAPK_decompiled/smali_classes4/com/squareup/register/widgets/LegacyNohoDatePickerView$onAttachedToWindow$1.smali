.class final Lcom/squareup/register/widgets/LegacyNohoDatePickerView$onAttachedToWindow$1;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerView.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$onAttachedToWindow$1;->this$0:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$onAttachedToWindow$1;->call(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;)Z
    .locals 4

    .line 77
    invoke-virtual {p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;->getToken()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView$onAttachedToWindow$1;->this$0:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-static {p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->access$getToken$p(Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
