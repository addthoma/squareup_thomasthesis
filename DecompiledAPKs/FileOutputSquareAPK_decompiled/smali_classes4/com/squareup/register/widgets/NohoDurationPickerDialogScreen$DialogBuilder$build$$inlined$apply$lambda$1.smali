.class final Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "NohoDurationPickerDialogScreen.kt"

# interfaces
.implements Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->build()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoNumberPicker;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "newVal",
        "onValueChange",
        "com/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $minutePicker:Lcom/squareup/noho/NohoNumberPicker;

.field final synthetic $view$inlined:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoNumberPicker;Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;->$minutePicker:Lcom/squareup/noho/NohoNumberPicker;

    iput-object p2, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;->this$0:Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;

    iput-object p3, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;->$view$inlined:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onValueChange(Lcom/squareup/noho/NohoNumberPicker;II)V
    .locals 0

    .line 67
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;->this$0:Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->getRunner()Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->setHourIndex$widgets_pos_release(I)V

    .line 68
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;->this$0:Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder;->getRunner()Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->getAllowZeroDuration$widgets_pos_release()Z

    move-result p1

    if-nez p1, :cond_1

    if-nez p3, :cond_0

    .line 70
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;->$minutePicker:Lcom/squareup/noho/NohoNumberPicker;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    goto :goto_0

    .line 72
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$DialogBuilder$build$$inlined$apply$lambda$1;->$minutePicker:Lcom/squareup/noho/NohoNumberPicker;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    :cond_1
    :goto_0
    return-void
.end method
