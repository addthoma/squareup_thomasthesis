.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerRunner.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;,
        Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyNohoDatePickerRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyNohoDatePickerRunner.kt\ncom/squareup/register/widgets/LegacyNohoDatePickerRunner\n+ 2 Flows.kt\ncom/squareup/container/Flows\n*L\n1#1,111:1\n75#2:112\n*E\n*S KotlinDebug\n*F\n+ 1 LegacyNohoDatePickerRunner.kt\ncom/squareup/register/widgets/LegacyNohoDatePickerRunner\n*L\n42#1:112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use NohoDatePickerDialogWorkflow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001:\u0002\u0016\u0017B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\tJ\u001f\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0000\u00a2\u0006\u0002\u0008\u0010J\r\u0010\u0011\u001a\u00020\u000bH\u0000\u00a2\u0006\u0002\u0008\u0012J\u000e\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0015R2\u0010\u0005\u001a&\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u0007 \u0008*\u0012\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00060\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
        "",
        "flow",
        "Lflow/Flow;",
        "(Lflow/Flow;)V",
        "datePicked",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;",
        "kotlin.jvm.PlatformType",
        "Lrx/Observable;",
        "onDatePickedFromDialog",
        "",
        "token",
        "",
        "date",
        "Lorg/threeten/bp/LocalDate;",
        "onDatePickedFromDialog$widgets_pos_release",
        "onDialogCancelled",
        "onDialogCancelled$widgets_pos_release",
        "showDatePickerDialog",
        "args",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;",
        "DatePicked",
        "DatePickerArgs",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final datePicked:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;->flow:Lflow/Flow;

    .line 34
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;->datePicked:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public final datePicked()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;->datePicked:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    const-string v1, "datePicked.asObservable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onDatePickedFromDialog$widgets_pos_release(JLorg/threeten/bp/LocalDate;)V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;->datePicked:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePicked;-><init>(JLorg/threeten/bp/LocalDate;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public final onDialogCancelled$widgets_pos_release()V
    .locals 4

    .line 42
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    .line 112
    const-class v2, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public final showDatePickerDialog(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;)V
    .locals 2

    const-string v0, "args"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen;

    invoke-direct {v1, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen;-><init>(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
