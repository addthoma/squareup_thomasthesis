.class public Lcom/squareup/register/widgets/AspectRatioImageView;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "AspectRatioImageView.java"


# static fields
.field private static final DEFAULT_ASPECT_RATIO:F = 1.0f

.field private static final DEFAULT_ASPECT_RATIO_ENABLED:Z = false

.field private static final DEFAULT_DOMINANT_MEASUREMENT:I = 0x0

.field public static final MEASUREMENT_HEIGHT:I = 0x1

.field public static final MEASUREMENT_WIDTH:I


# instance fields
.field private aspectRatio:F

.field private aspectRatioEnabled:Z

.field private dominantMeasurement:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, v0}, Lcom/squareup/register/widgets/AspectRatioImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 28
    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioImageView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 31
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioImageView_sq_aspectRatio:I

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatio:F

    .line 32
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioImageView_sq_aspectRatioEnabled:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatioEnabled:Z

    .line 34
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioImageView_dominantMeasurement:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->dominantMeasurement:I

    .line 36
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public getAspectRatio()F
    .locals 1

    .line 65
    iget v0, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatio:F

    return v0
.end method

.method public getAspectRatioEnabled()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatioEnabled:Z

    return v0
.end method

.method public getDominantMeasurement()I
    .locals 1

    .line 89
    iget v0, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->dominantMeasurement:I

    return v0
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 40
    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;->onMeasure(II)V

    .line 41
    iget-boolean p1, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatioEnabled:Z

    if-nez p1, :cond_0

    return-void

    .line 45
    :cond_0
    iget p1, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->dominantMeasurement:I

    if-eqz p1, :cond_2

    const/4 p2, 0x1

    if-ne p1, p2, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getMeasuredHeight()I

    move-result p1

    int-to-float p2, p1

    .line 53
    iget v0, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatio:F

    mul-float p2, p2, v0

    float-to-int p2, p2

    goto :goto_0

    .line 57
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown measurement with ID "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->dominantMeasurement:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 47
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getMeasuredWidth()I

    move-result p2

    int-to-float p1, p2

    .line 48
    iget v0, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatio:F

    mul-float p1, p1, v0

    float-to-int p1, p1

    .line 60
    :goto_0
    invoke-virtual {p0, p2, p1}, Lcom/squareup/register/widgets/AspectRatioImageView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setAspectRatio(F)V
    .locals 0

    .line 70
    iput p1, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatio:F

    .line 71
    iget-boolean p1, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatioEnabled:Z

    if-eqz p1, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioImageView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setAspectRatioEnabled(Z)V
    .locals 0

    .line 83
    iput-boolean p1, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->aspectRatioEnabled:Z

    .line 84
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioImageView;->requestLayout()V

    return-void
.end method

.method public setDominantMeasurement(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 100
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid measurement type."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 102
    :cond_1
    :goto_0
    iput p1, p0, Lcom/squareup/register/widgets/AspectRatioImageView;->dominantMeasurement:I

    .line 103
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioImageView;->requestLayout()V

    return-void
.end method
