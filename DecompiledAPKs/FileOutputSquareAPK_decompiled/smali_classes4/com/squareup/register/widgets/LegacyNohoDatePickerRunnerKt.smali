.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u001a\u0014\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001*\u00020\u0003H\u0002\u001a\u0014\u0010\u0004\u001a\u00020\u0005*\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0001H\u0002\u00a8\u0006\u0007"
    }
    d2 = {
        "readLocalDate",
        "Lorg/threeten/bp/LocalDate;",
        "kotlin.jvm.PlatformType",
        "Landroid/os/Parcel;",
        "writeLocalDate",
        "",
        "localDate",
        "widgets-pos_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$readLocalDate(Landroid/os/Parcel;)Lorg/threeten/bp/LocalDate;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;->readLocalDate(Landroid/os/Parcel;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$writeLocalDate(Landroid/os/Parcel;Lorg/threeten/bp/LocalDate;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;->writeLocalDate(Landroid/os/Parcel;Lorg/threeten/bp/LocalDate;)V

    return-void
.end method

.method private static final readLocalDate(Landroid/os/Parcel;)Lorg/threeten/bp/LocalDate;
    .locals 2

    .line 18
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/LocalDate;->ofEpochDay(J)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    return-object p0
.end method

.method private static final writeLocalDate(Landroid/os/Parcel;Lorg/threeten/bp/LocalDate;)V
    .locals 2

    .line 21
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->toEpochDay()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
