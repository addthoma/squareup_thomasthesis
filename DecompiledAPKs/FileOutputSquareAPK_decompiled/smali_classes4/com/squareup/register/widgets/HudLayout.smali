.class public Lcom/squareup/register/widgets/HudLayout;
.super Landroid/widget/LinearLayout;
.source "HudLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 10
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 0

    .line 14
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 16
    invoke-virtual {p0}, Lcom/squareup/register/widgets/HudLayout;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {p0}, Lcom/squareup/register/widgets/HudLayout;->getMeasuredHeight()I

    move-result p2

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 17
    invoke-virtual {p0, p1, p1}, Lcom/squareup/register/widgets/HudLayout;->setMeasuredDimension(II)V

    return-void
.end method
