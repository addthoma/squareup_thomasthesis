.class public Lcom/squareup/register/widgets/EditCatalogObjectLabel;
.super Landroid/widget/LinearLayout;
.source "EditCatalogObjectLabel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;
    }
.end annotation


# instance fields
.field private final abbreviation:Landroid/widget/EditText;

.field private final animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final defaultColor:I

.field private final errorDrawable:Landroid/graphics/drawable/Drawable;

.field private final image:Landroid/widget/ImageView;

.field private final name:Lcom/squareup/marketfont/MarketTextView;

.field private final selector:Landroid/graphics/drawable/StateListDrawable;

.field private final watcher:Lcom/squareup/text/AutoFitTextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    sget p2, Lcom/squareup/widgets/pos/R$layout;->edit_catalog_object_label:I

    invoke-static {p1, p2, p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    sget p2, Lcom/squareup/widgets/pos/R$drawable;->panel_background:I

    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setBackgroundResource(I)V

    const/4 p2, 0x1

    .line 47
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setOrientation(I)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 50
    sget v0, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->defaultColor:I

    .line 51
    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_dim_translucent_pressed:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    iput-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->selector:Landroid/graphics/drawable/StateListDrawable;

    .line 54
    sget v0, Lcom/squareup/widgets/pos/R$id;->edit_catalog_object_label_animator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 56
    sget v0, Lcom/squareup/widgets/pos/R$id;->edit_catalog_object_label_abbreviation:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    .line 57
    sget v0, Lcom/squareup/glyph/R$dimen;->glyph_shadow_radius:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 58
    sget v1, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dx:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 59
    sget v2, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dy:I

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 60
    sget v3, Lcom/squareup/marin/R$color;->marin_text_shadow:I

    invoke-virtual {p2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 61
    iget-object v4, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    int-to-float v0, v0

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/EditText;->setShadowLayer(FFFI)V

    .line 63
    new-instance v0, Lcom/squareup/text/AutoFitTextWatcher;

    iget-object v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-direct {v0, v1}, Lcom/squareup/text/AutoFitTextWatcher;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->watcher:Lcom/squareup/text/AutoFitTextWatcher;

    .line 64
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;

    invoke-direct {v1, p1, v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel$RestartImeTextWatcher;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->watcher:Lcom/squareup/text/AutoFitTextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$EditCatalogObjectLabel$XbrCf9lbFcPev3zMFfBrpaN-4u0;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/-$$Lambda$EditCatalogObjectLabel$XbrCf9lbFcPev3zMFfBrpaN-4u0;-><init>(Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 72
    sget p1, Lcom/squareup/widgets/pos/R$id;->edit_catalog_object_label_image:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    .line 74
    new-instance p1, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-direct {p1, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->WARNING_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 75
    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$color;->marin_white:I

    .line 76
    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->errorDrawable:Landroid/graphics/drawable/Drawable;

    .line 78
    sget p1, Lcom/squareup/widgets/pos/R$id;->edit_catalog_object_label_name:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->name:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public clearItemBitmap()V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public disableAbbreviationEditing()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 94
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 95
    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->selector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .line 82
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 83
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->selector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 84
    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->invalidate()V

    return-void
.end method

.method public enableAbbreviationEditing()V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    iget-object v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 109
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 113
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 114
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 117
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->watcher:Lcom/squareup/text/AutoFitTextWatcher;

    iget-object v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/text/AutoFitTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    return-void
.end method

.method public getAbbreviationText()Landroid/text/Editable;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public isEditingTitle()Z
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$new$0$EditCatalogObjectLabel(Landroid/view/View;Z)V
    .locals 0

    if-nez p2, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->disableAbbreviationEditing()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$showSoftInput$1$EditCatalogObjectLabel()V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->isEditingTitle()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 104
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .line 88
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 89
    iget-object p3, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->selector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->getPaddingLeft()I

    move-result p4

    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->getPaddingBottom()I

    move-result v1

    sub-int/2addr p2, v1

    .line 89
    invoke-virtual {p3, p4, v0, p1, p2}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    return-void
.end method

.method public setAbbreviationText(Ljava/lang/String;)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setItemBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 154
    iget-object p1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method public setLabelColor(Ljava/lang/String;)V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    iget v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->defaultColor:I

    invoke-static {p1, v1}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setBackgroundColor(I)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->name:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, "Object label\'s name"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public showAbbreviation()V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    iget-object v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public showImage()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    iget-object v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public showItemBitmapError()V
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    iget v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->defaultColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 159
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->errorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    iget-object v1, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->image:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public showProgress()V
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/widgets/pos/R$id;->edit_catalog_object_label_progress:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public showSoftInput()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->abbreviation:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/register/widgets/-$$Lambda$EditCatalogObjectLabel$TSJREiZVlyeBnrXs76mXTAV30EU;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/-$$Lambda$EditCatalogObjectLabel$TSJREiZVlyeBnrXs76mXTAV30EU;-><init>(Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
