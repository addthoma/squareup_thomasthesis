.class Lcom/squareup/register/widgets/card/CardEditor$2;
.super Ljava/lang/Object;
.source "CardEditor.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/CardEditor;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/CardEditor;

.field final synthetic val$initialHint:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/CardEditor;Ljava/lang/CharSequence;)V
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$2;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor$2;->val$initialHint:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 163
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor$2;->val$initialHint:Ljava/lang/CharSequence;

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    invoke-static {}, Lcom/squareup/register/widgets/card/CardEditor;->access$100()I

    move-result p3

    if-gt p2, p3, :cond_0

    return-void

    .line 165
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_1

    .line 167
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$2;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$200(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/CvvEditor;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardEditor$2;->val$initialHint:Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/CvvEditor;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 170
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$2;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CardEditor;->access$200(Lcom/squareup/register/widgets/card/CardEditor;)Lcom/squareup/register/widgets/card/CvvEditor;

    move-result-object p1

    const-string p2, "####"

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/CvvEditor;->setHint(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
