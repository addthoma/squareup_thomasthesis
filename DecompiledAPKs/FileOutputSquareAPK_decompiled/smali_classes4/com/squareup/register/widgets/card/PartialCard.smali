.class public Lcom/squareup/register/widgets/card/PartialCard;
.super Ljava/lang/Object;
.source "PartialCard.java"


# instance fields
.field final brand:Lcom/squareup/Card$Brand;

.field final expiration:Ljava/lang/String;

.field final pan:Ljava/lang/String;

.field final postalCode:Ljava/lang/String;

.field final verification:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PartialCard;->brand:Lcom/squareup/Card$Brand;

    .line 20
    iput-object p2, p0, Lcom/squareup/register/widgets/card/PartialCard;->pan:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Lcom/squareup/register/widgets/card/PartialCard;->verification:Ljava/lang/String;

    .line 22
    iput-object p4, p0, Lcom/squareup/register/widgets/card/PartialCard;->expiration:Ljava/lang/String;

    .line 23
    iput-object p5, p0, Lcom/squareup/register/widgets/card/PartialCard;->postalCode:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isBlank()Z
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PartialCard;->pan:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/card/PartialCard;->verification:Ljava/lang/String;

    .line 29
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/card/PartialCard;->expiration:Ljava/lang/String;

    .line 30
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/card/PartialCard;->postalCode:Ljava/lang/String;

    .line 31
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
