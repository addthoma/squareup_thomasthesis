.class public Lcom/squareup/register/widgets/card/PanEditor;
.super Lcom/squareup/widgets/SensitiveEditText;
.source "PanEditor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/card/PanEditor$SavedState;,
        Lcom/squareup/register/widgets/card/PanEditor$Component;
    }
.end annotation


# static fields
.field public static final OBFUSCATION_PAD_CHAR:C = '\u2022'


# instance fields
.field private final cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

.field public currency:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private currentBrand:Lcom/squareup/Card$Brand;

.field public features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public giftCards:Lcom/squareup/giftcard/GiftCards;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private onBrandListener:Lcom/squareup/register/widgets/card/OnBrandListener;

.field private onPanListener:Lcom/squareup/register/widgets/card/OnPanListener;

.field private final shakeAnim:Landroid/view/animation/Animation;

.field private strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

.field private watcher:Lcom/squareup/text/ScrubbingTextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/SensitiveEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    new-instance p2, Lcom/squareup/text/CardNumberScrubber;

    invoke-direct {p2}, Lcom/squareup/text/CardNumberScrubber;-><init>()V

    iput-object p2, p0, Lcom/squareup/register/widgets/card/PanEditor;->cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

    .line 63
    sget-object p2, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    iput-object p2, p0, Lcom/squareup/register/widgets/card/PanEditor;->currentBrand:Lcom/squareup/Card$Brand;

    .line 70
    const-class p2, Lcom/squareup/register/widgets/card/PanEditor$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/card/PanEditor$Component;

    invoke-interface {p1, p0}, Lcom/squareup/register/widgets/card/PanEditor$Component;->inject(Lcom/squareup/register/widgets/card/PanEditor;)V

    .line 71
    new-instance p1, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    invoke-direct {p1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->shakeAnim:Landroid/view/animation/Animation;

    .line 72
    new-instance p1, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;Z)V

    .line 73
    iget-object p2, p0, Lcom/squareup/register/widgets/card/PanEditor;->shakeAnim:Landroid/view/animation/Animation;

    invoke-virtual {p2, p1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 74
    new-instance p2, Lcom/squareup/register/widgets/card/PanEditor$1;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/PanEditor$1;-><init>(Lcom/squareup/register/widgets/card/PanEditor;)V

    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/card/PanEditor;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 96
    new-instance p2, Lcom/squareup/register/widgets/card/PanEditor$2;

    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

    invoke-direct {p2, p0, v0, p0, p1}, Lcom/squareup/register/widgets/card/PanEditor$2;-><init>(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;Lcom/squareup/register/widgets/validation/ShakeAnimationListener;)V

    iput-object p2, p0, Lcom/squareup/register/widgets/card/PanEditor;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    .line 115
    new-instance p1, Lcom/squareup/register/widgets/card/PermissiveCardPanValidationStrategy;

    invoke-direct {p1}, Lcom/squareup/register/widgets/card/PermissiveCardPanValidationStrategy;-><init>()V

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 116
    invoke-static {p0}, Lcom/squareup/text/SimplePasswordTransformationMethod;->install(Landroid/widget/TextView;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/card/PanEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/register/widgets/card/PanEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/register/widgets/card/PanEditor;)Lcom/squareup/register/widgets/card/OnPanListener;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/register/widgets/card/PanEditor;->onPanListener:Lcom/squareup/register/widgets/card/OnPanListener;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/register/widgets/card/PanEditor;)Landroid/view/animation/Animation;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/register/widgets/card/PanEditor;->shakeAnim:Landroid/view/animation/Animation;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/Card$Brand;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->updateBrand(Lcom/squareup/Card$Brand;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/register/widgets/card/PanEditor;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/PanEditor;->fireCardChanged()V

    return-void
.end method

.method private fireCardChanged()V
    .locals 2

    .line 219
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->onPanListener:Lcom/squareup/register/widgets/card/OnPanListener;

    if-nez v0, :cond_0

    return-void

    .line 220
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/PanEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 222
    iget-object v1, p0, Lcom/squareup/register/widgets/card/PanEditor;->onPanListener:Lcom/squareup/register/widgets/card/OnPanListener;

    invoke-interface {v1, v0}, Lcom/squareup/register/widgets/card/OnPanListener;->onPanValid(Lcom/squareup/Card;)V

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->currentBrand:Lcom/squareup/Card$Brand;

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/PanEditor;->getCardNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Brand;->checkForPanWarning(Ljava/lang/String;)Lcom/squareup/Card$PanWarning;

    move-result-object v0

    .line 225
    iget-object v1, p0, Lcom/squareup/register/widgets/card/PanEditor;->onPanListener:Lcom/squareup/register/widgets/card/OnPanListener;

    invoke-interface {v1, v0}, Lcom/squareup/register/widgets/card/OnPanListener;->onPanInvalid(Lcom/squareup/Card$PanWarning;)V

    :goto_0
    return-void
.end method

.method public static getCard(Ljava/lang/String;Lcom/squareup/giftcard/GiftCards;)Lcom/squareup/Card;
    .locals 1

    .line 206
    invoke-virtual {p1, p0}, Lcom/squareup/giftcard/GiftCards;->isSquareGiftCard(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/squareup/Card;->guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;

    move-result-object p1

    .line 207
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/Card$Brand;->isValidNumberLength(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p0}, Lcom/squareup/Card$Brand;->validateLuhnIfRequired(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    new-instance v0, Lcom/squareup/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/Card$Builder;-><init>()V

    .line 209
    invoke-virtual {v0, p0}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p0

    .line 210
    invoke-virtual {p0, p1}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p0

    sget-object p1, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    .line 211
    invoke-virtual {p0, p1}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p0

    .line 212
    invoke-virtual {p0}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private getCardNumber()Ljava/lang/String;
    .locals 1

    .line 189
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/PanEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->removeSpaces(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setRestartImeAfterScrubChange(Z)V
    .locals 2

    .line 250
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->GIFT_CARDS_THIRD_PARTY_IME:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->watcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-virtual {v0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->setRestartImeAfterScrubChange(Z)V

    :cond_0
    return-void
.end method

.method private updateBrand(Lcom/squareup/Card$Brand;)V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->currentBrand:Lcom/squareup/Card$Brand;

    if-ne p1, v0, :cond_0

    return-void

    .line 195
    :cond_0
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->currentBrand:Lcom/squareup/Card$Brand;

    .line 196
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->onBrandListener:Lcom/squareup/register/widgets/card/OnBrandListener;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lcom/squareup/register/widgets/card/OnBrandListener;->onBrandChanged(Lcom/squareup/Card$Brand;)V

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/CardNumberScrubber;->setBrand(Lcom/squareup/Card$Brand;)V

    return-void
.end method


# virtual methods
.method public getCard()Lcom/squareup/Card;
    .locals 3

    .line 164
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "setStrategy needs to be called"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/PanEditor;->getCardNumber()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/widgets/card/PanEditor;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-interface {v0, v1, v2}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->validateAndBuildCard(Ljava/lang/String;Lcom/squareup/giftcard/GiftCards;)Lcom/squareup/Card;

    move-result-object v0

    return-object v0
.end method

.method public hasCard()Z
    .locals 1

    .line 169
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/PanEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 126
    check-cast p1, Lcom/squareup/register/widgets/card/PanEditor$SavedState;

    .line 130
    invoke-static {}, Lcom/squareup/Card$Brand;->values()[Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/register/widgets/card/PanEditor$SavedState;->access$600(Lcom/squareup/register/widgets/card/PanEditor$SavedState;)I

    move-result v1

    aget-object v0, v0, v1

    .line 131
    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/card/PanEditor;->updateBrand(Lcom/squareup/Card$Brand;)V

    .line 133
    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/PanEditor$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Lcom/squareup/widgets/SensitiveEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 136
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/PanEditor;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->updateBrand(Lcom/squareup/Card$Brand;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/register/widgets/card/PanEditor$SavedState;

    invoke-super {p0}, Lcom/squareup/widgets/SensitiveEditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/widgets/card/PanEditor;->currentBrand:Lcom/squareup/Card$Brand;

    invoke-virtual {v2}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/register/widgets/card/PanEditor$SavedState;-><init>(Landroid/os/Parcelable;ILcom/squareup/register/widgets/card/PanEditor$1;)V

    return-object v0
.end method

.method public setOnBrandListener(Lcom/squareup/register/widgets/card/OnBrandListener;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->onBrandListener:Lcom/squareup/register/widgets/card/OnBrandListener;

    return-void
.end method

.method public setOnPanListener(Lcom/squareup/register/widgets/card/OnPanListener;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->onPanListener:Lcom/squareup/register/widgets/card/OnPanListener;

    return-void
.end method

.method public setShowCard(Z)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 141
    new-instance p1, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/PanEditor;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    iget-object v1, p0, Lcom/squareup/register/widgets/card/PanEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    iget-object v2, p0, Lcom/squareup/register/widgets/card/PanEditor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 142
    invoke-interface {v1, v2}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->getDefaultGlyph(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 143
    invoke-virtual {p1, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p1

    .line 145
    invoke-virtual {p0, p1, v0, v0, v0}, Lcom/squareup/register/widgets/card/PanEditor;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 147
    :cond_0
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/squareup/register/widgets/card/PanEditor;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method public setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V
    .locals 2

    .line 178
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    .line 179
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor;->cardNumberScrubber:Lcom/squareup/text/CardNumberScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/CardNumberScrubber;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 180
    invoke-interface {p1}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->getInputType()I

    move-result v0

    .line 181
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/PanEditor;->getInputType()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 183
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/card/PanEditor;->setInputType(I)V

    .line 185
    :cond_0
    invoke-interface {p1}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->getRestartImeAfterScrubChange()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/card/PanEditor;->setRestartImeAfterScrubChange(Z)V

    return-void
.end method
