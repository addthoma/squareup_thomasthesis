.class public Lcom/squareup/register/widgets/RectangularGridLayout;
.super Landroid/view/ViewGroup;
.source "RectangularGridLayout.java"


# instance fields
.field private columnCount:I

.field private final gap:I

.field private final minCellWidth:I

.field private rowHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->RectangularGridLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 29
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->RectangularGridLayout_minCellWidth:I

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->minCellWidth:I

    .line 30
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->RectangularGridLayout_columnCount:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->columnCount:I

    .line 31
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->RectangularGridLayout_rowHeight:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->rowHeight:I

    .line 33
    iget p2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->minCellWidth:I

    if-ne p2, v0, :cond_1

    iget p2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->columnCount:I

    if-eq p2, v0, :cond_0

    goto :goto_0

    .line 34
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "minCellWidth or columnCount is required."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 36
    :cond_1
    :goto_0
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->RectangularGridLayout_gap:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->gap:I

    .line 38
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 9

    .line 73
    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getMeasuredWidth()I

    move-result v1

    .line 74
    new-instance p1, Lcom/squareup/register/widgets/GridSizer$ParentSize;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getPaddingRight()I

    move-result v4

    .line 76
    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getPaddingBottom()I

    move-result v5

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/GridSizer$ParentSize;-><init>(IIIII)V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getChildCount()I

    move-result p2

    .line 79
    new-array p3, p2, [I

    const/4 p4, 0x0

    const/4 p5, 0x0

    :goto_0
    if-ge p5, p2, :cond_0

    .line 81
    invoke-virtual {p0, p5}, Lcom/squareup/register/widgets/RectangularGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    aput v0, p3, p5

    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    .line 84
    :cond_0
    new-instance p5, Lcom/squareup/register/widgets/GridSizer$Rect;

    invoke-direct {p5}, Lcom/squareup/register/widgets/GridSizer$Rect;-><init>()V

    :goto_1
    if-ge p4, p2, :cond_1

    .line 86
    invoke-virtual {p0, p4}, Lcom/squareup/register/widgets/RectangularGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 87
    iget v4, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->columnCount:I

    iget v6, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->gap:I

    .line 88
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move-object v2, p1

    move-object v3, p5

    move v5, p4

    move-object v7, p3

    .line 87
    invoke-static/range {v2 .. v8}, Lcom/squareup/register/widgets/GridSizer;->getChildBounds(Lcom/squareup/register/widgets/GridSizer$ParentSize;Lcom/squareup/register/widgets/GridSizer$Rect;III[II)V

    .line 90
    iget v1, p5, Lcom/squareup/register/widgets/GridSizer$Rect;->left:I

    iget v2, p5, Lcom/squareup/register/widgets/GridSizer$Rect;->top:I

    iget v3, p5, Lcom/squareup/register/widgets/GridSizer$Rect;->right:I

    iget v4, p5, Lcom/squareup/register/widgets/GridSizer$Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 p4, p4, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .line 42
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 44
    new-instance v6, Lcom/squareup/register/widgets/GridSizer$ParentSize;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getPaddingRight()I

    move-result v4

    .line 46
    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getPaddingBottom()I

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/GridSizer$ParentSize;-><init>(IIIII)V

    .line 47
    iget v0, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->columnCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 48
    iget v0, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->minCellWidth:I

    iget v2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->gap:I

    invoke-static {v6, v0, v2}, Lcom/squareup/register/widgets/GridSizer;->getColumnCount(Lcom/squareup/register/widgets/GridSizer$ParentSize;II)I

    move-result v0

    iput v0, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->columnCount:I

    .line 50
    :cond_0
    iget v0, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->columnCount:I

    iget v2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->gap:I

    invoke-static {v6, v0, v2}, Lcom/squareup/register/widgets/GridSizer;->getDesiredCellSize(Lcom/squareup/register/widgets/GridSizer$ParentSize;II)I

    move-result v0

    .line 51
    iget v2, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->rowHeight:I

    if-ne v2, v1, :cond_1

    move v2, v0

    :cond_1
    const/high16 v1, 0x40000000    # 2.0f

    .line 54
    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 57
    invoke-virtual {p0}, Lcom/squareup/register/widgets/RectangularGridLayout;->getChildCount()I

    move-result v4

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    .line 60
    iget v7, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->columnCount:I

    iget v8, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->gap:I

    .line 61
    invoke-static {v6, v5, v7, v0, v8}, Lcom/squareup/register/widgets/GridSizer;->getActualCellWidth(Lcom/squareup/register/widgets/GridSizer$ParentSize;IIII)I

    move-result v7

    .line 62
    invoke-static {v7, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 64
    invoke-virtual {p0, v5}, Lcom/squareup/register/widgets/RectangularGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v7, v3}, Landroid/view/View;->measure(II)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 67
    :cond_2
    iget v0, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->columnCount:I

    iget v1, p0, Lcom/squareup/register/widgets/RectangularGridLayout;->gap:I

    .line 68
    invoke-static {v6, v4, v0, v1, v2}, Lcom/squareup/register/widgets/GridSizer;->getTotalHeight(Lcom/squareup/register/widgets/GridSizer$ParentSize;IIII)I

    move-result v0

    .line 67
    invoke-static {v0, p2}, Lcom/squareup/register/widgets/RectangularGridLayout;->resolveSize(II)I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/RectangularGridLayout;->setMeasuredDimension(II)V

    return-void
.end method
