.class Lcom/squareup/register/widgets/list/EditQuantityRow$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditQuantityRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/list/EditQuantityRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/list/EditQuantityRow;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$3;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 78
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$3;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-static {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->access$100(Lcom/squareup/register/widgets/list/EditQuantityRow;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 79
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$3;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    goto :goto_0

    .line 81
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$3;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->getValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    .line 83
    :goto_0
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$3;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-static {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->access$100(Lcom/squareup/register/widgets/list/EditQuantityRow;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow$3;->this$0:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-static {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->access$100(Lcom/squareup/register/widgets/list/EditQuantityRow;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->clearFocus()V

    return-void
.end method
