.class public Lcom/squareup/register/widgets/SmartScrollListView;
.super Lcom/squareup/ui/NullStateListView;
.source "SmartScrollListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;,
        Lcom/squareup/register/widgets/SmartScrollListView$EmptyOnScrollListener;,
        Lcom/squareup/register/widgets/SmartScrollListView$EmptySmartScrollHeaderCallback;,
        Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;
    }
.end annotation


# instance fields
.field private onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/NullStateListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    new-instance p1, Lcom/squareup/register/widgets/SmartScrollListView$EmptySmartScrollHeaderCallback;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/squareup/register/widgets/SmartScrollListView$EmptySmartScrollHeaderCallback;-><init>(Lcom/squareup/register/widgets/SmartScrollListView$1;)V

    iput-object p1, p0, Lcom/squareup/register/widgets/SmartScrollListView;->smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;

    .line 28
    new-instance p1, Lcom/squareup/register/widgets/SmartScrollListView$EmptyOnScrollListener;

    invoke-direct {p1, p2}, Lcom/squareup/register/widgets/SmartScrollListView$EmptyOnScrollListener;-><init>(Lcom/squareup/register/widgets/SmartScrollListView$1;)V

    iput-object p1, p0, Lcom/squareup/register/widgets/SmartScrollListView;->onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    return-void
.end method


# virtual methods
.method public getExtraTopPaddingForHeader()I
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/register/widgets/SmartScrollListView;->smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;

    invoke-interface {v0}, Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;->getHeaderHeight()I

    move-result v0

    return v0
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 3

    .line 35
    iput-object p1, p0, Lcom/squareup/register/widgets/SmartScrollListView;->onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 36
    new-instance v0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;

    iget-object v1, p0, Lcom/squareup/register/widgets/SmartScrollListView;->smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;-><init>(Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;Landroid/widget/AbsListView$OnScrollListener;Lcom/squareup/register/widgets/SmartScrollListView$1;)V

    invoke-super {p0, v0}, Lcom/squareup/ui/NullStateListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method public updateForSmartScrollHeader(Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;)V
    .locals 4

    .line 50
    invoke-virtual {p0}, Lcom/squareup/register/widgets/SmartScrollListView;->getPaddingTop()I

    move-result v0

    .line 51
    invoke-virtual {p0}, Lcom/squareup/register/widgets/SmartScrollListView;->getExtraTopPaddingForHeader()I

    move-result v1

    .line 52
    iput-object p1, p0, Lcom/squareup/register/widgets/SmartScrollListView;->smartScrollHeaderCallback:Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;

    sub-int/2addr v0, v1

    .line 54
    invoke-virtual {p0}, Lcom/squareup/register/widgets/SmartScrollListView;->getExtraTopPaddingForHeader()I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    invoke-virtual {p0}, Lcom/squareup/register/widgets/SmartScrollListView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/register/widgets/SmartScrollListView;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/register/widgets/SmartScrollListView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/squareup/register/widgets/SmartScrollListView;->setPadding(IIII)V

    .line 56
    new-instance v0, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;

    iget-object v1, p0, Lcom/squareup/register/widgets/SmartScrollListView;->onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/register/widgets/SmartScrollListView$OnScrollListenerWrapper;-><init>(Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;Landroid/widget/AbsListView$OnScrollListener;Lcom/squareup/register/widgets/SmartScrollListView$1;)V

    invoke-super {p0, v0}, Lcom/squareup/ui/NullStateListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method
