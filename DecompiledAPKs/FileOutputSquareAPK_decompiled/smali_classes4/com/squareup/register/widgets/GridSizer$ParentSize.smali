.class public Lcom/squareup/register/widgets/GridSizer$ParentSize;
.super Ljava/lang/Object;
.source "GridSizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/GridSizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParentSize"
.end annotation


# instance fields
.field private final paddingBottom:I

.field private final paddingLeft:I

.field private final paddingRight:I

.field private final paddingTop:I

.field private final width:I


# direct methods
.method public constructor <init>(IIIII)V
    .locals 0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput p1, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->width:I

    .line 122
    iput p2, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingLeft:I

    .line 123
    iput p3, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingTop:I

    .line 124
    iput p4, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingRight:I

    .line 125
    iput p5, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingBottom:I

    return-void
.end method


# virtual methods
.method public getAvailableWidth()I
    .locals 2

    .line 129
    iget v0, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->width:I

    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingLeft:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingRight:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getPaddingLeft()I
    .locals 1

    .line 133
    iget v0, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingLeft:I

    return v0
.end method

.method public getPaddingTop()I
    .locals 1

    .line 137
    iget v0, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingTop:I

    return v0
.end method

.method public getVerticalPadding()I
    .locals 2

    .line 141
    iget v0, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingTop:I

    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$ParentSize;->paddingBottom:I

    add-int/2addr v0, v1

    return v0
.end method
