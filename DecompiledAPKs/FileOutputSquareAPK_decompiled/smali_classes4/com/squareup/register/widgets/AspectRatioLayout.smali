.class public Lcom/squareup/register/widgets/AspectRatioLayout;
.super Landroid/widget/FrameLayout;
.source "AspectRatioLayout.java"


# static fields
.field private static final DEFAULT_ASPECT_RATIO:F = 1.0f


# instance fields
.field private aspectRatio:F

.field private aspectRatioEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    .line 17
    iput-boolean v0, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatioEnabled:Z

    const/high16 v1, 0x3f800000    # 1.0f

    .line 18
    iput v1, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatio:F

    .line 23
    sget-object v1, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 24
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioLayout_sq_aspectRatio:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-lez v1, :cond_0

    .line 26
    iput p2, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatio:F

    .line 30
    :cond_0
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioLayout_sq_aspectRatioEnabled:I

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatioEnabled:Z

    .line 32
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public disableAspectRatio()V
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatioEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 62
    iput-boolean v0, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatioEnabled:Z

    .line 64
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioLayout;->requestLayout()V

    :cond_0
    return-void
.end method

.method public enableAspectRatio(F)V
    .locals 1

    .line 69
    iget-boolean v0, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatioEnabled:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatio:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 70
    iput-boolean v0, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatioEnabled:Z

    .line 71
    iput p1, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatio:F

    .line 73
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioLayout;->requestLayout()V

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .line 36
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 38
    iget-boolean p1, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatioEnabled:Z

    if-nez p1, :cond_0

    return-void

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioLayout;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioLayout;->getPaddingLeft()I

    move-result p2

    sub-int/2addr p1, p2

    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioLayout;->getPaddingRight()I

    move-result p2

    sub-int/2addr p1, p2

    .line 41
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioLayout;->getMeasuredHeight()I

    move-result p2

    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioLayout;->getPaddingTop()I

    move-result v0

    sub-int/2addr p2, v0

    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioLayout;->getPaddingBottom()I

    move-result v0

    sub-int/2addr p2, v0

    int-to-float v0, p1

    int-to-float v1, p2

    .line 45
    iget v2, p0, Lcom/squareup/register/widgets/AspectRatioLayout;->aspectRatio:F

    mul-float v3, v1, v2

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1

    mul-float v1, v1, v2

    float-to-int p1, v1

    goto :goto_0

    :cond_1
    div-float/2addr v0, v2

    float-to-int p2, v0

    :goto_0
    const/4 v0, 0x0

    .line 53
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/AspectRatioLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 55
    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 56
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 57
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    return-void
.end method
