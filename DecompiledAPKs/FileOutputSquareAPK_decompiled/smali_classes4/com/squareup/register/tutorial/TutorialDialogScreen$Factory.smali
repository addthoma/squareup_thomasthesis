.class public Lcom/squareup/register/tutorial/TutorialDialogScreen$Factory;
.super Ljava/lang/Object;
.source "TutorialDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/TutorialDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 32
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialDialogScreen;

    invoke-static {v0}, Lcom/squareup/register/tutorial/TutorialDialogScreen;->access$000(Lcom/squareup/register/tutorial/TutorialDialogScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    new-instance v0, Lcom/squareup/register/tutorial/X2TutorialDialog;

    invoke-direct {v0, p1}, Lcom/squareup/register/tutorial/X2TutorialDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/X2TutorialDialog;->getDialog()Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 38
    :cond_0
    new-instance v0, Lcom/squareup/register/tutorial/TutorialDialog;

    invoke-direct {v0, p1}, Lcom/squareup/register/tutorial/TutorialDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
