.class public Lcom/squareup/register/tutorial/TutorialApi$NoOpTutorialApi;
.super Lcom/squareup/register/tutorial/TutorialApi;
.source "TutorialApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/TutorialApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoOpTutorialApi"
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Lcom/squareup/register/tutorial/TutorialApi;-><init>()V

    return-void
.end method


# virtual methods
.method public forceStartFirstPaymentTutorial()V
    .locals 0

    return-void
.end method

.method public maybeAutoStartFirstPaymentTutorial()V
    .locals 0

    return-void
.end method

.method public maybeStartFirstPaymentTutorialForDeepLink()Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
