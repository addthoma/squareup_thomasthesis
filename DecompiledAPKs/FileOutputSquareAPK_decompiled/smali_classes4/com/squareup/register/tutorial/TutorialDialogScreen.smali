.class public Lcom/squareup/register/tutorial/TutorialDialogScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "TutorialDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/register/tutorial/TutorialDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/TutorialDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/tutorial/TutorialDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final isX2:Z

.field final prompt:Lcom/squareup/register/tutorial/TutorialDialog$Prompt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/register/tutorial/-$$Lambda$TutorialDialogScreen$lXofTC2GS08KfUmX1NXbWwSH4fk;->INSTANCE:Lcom/squareup/register/tutorial/-$$Lambda$TutorialDialogScreen$lXofTC2GS08KfUmX1NXbWwSH4fk;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/register/tutorial/TutorialDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Z)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialDialogScreen;->prompt:Lcom/squareup/register/tutorial/TutorialDialog$Prompt;

    .line 22
    iput-boolean p2, p0, Lcom/squareup/register/tutorial/TutorialDialogScreen;->isX2:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/tutorial/TutorialDialogScreen;)Z
    .locals 0

    .line 16
    iget-boolean p0, p0, Lcom/squareup/register/tutorial/TutorialDialogScreen;->isX2:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/register/tutorial/TutorialDialogScreen;
    .locals 2

    .line 44
    new-instance v0, Lcom/squareup/register/tutorial/TutorialDialogScreen;

    const-class v1, Lcom/squareup/register/tutorial/TutorialDialogScreen;

    .line 45
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;

    .line 46
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    move-result p0

    invoke-static {p0}, Lcom/squareup/util/Booleans;->toBoolean(B)Z

    move-result p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/register/tutorial/TutorialDialogScreen;-><init>(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Z)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialDialogScreen;->prompt:Lcom/squareup/register/tutorial/TutorialDialog$Prompt;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 27
    iget-boolean p2, p0, Lcom/squareup/register/tutorial/TutorialDialogScreen;->isX2:Z

    invoke-static {p2}, Lcom/squareup/util/Booleans;->toByte(Z)B

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method
