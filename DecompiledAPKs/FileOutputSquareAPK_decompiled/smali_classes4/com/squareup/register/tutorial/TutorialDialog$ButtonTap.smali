.class public final enum Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;
.super Ljava/lang/Enum;
.source "TutorialDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/TutorialDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonTap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

.field public static final enum PRIMARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

.field public static final enum SECONDARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 74
    new-instance v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    const/4 v1, 0x0

    const-string v2, "PRIMARY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->PRIMARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    new-instance v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    const/4 v2, 0x1

    const-string v3, "SECONDARY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->SECONDARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    .line 73
    sget-object v3, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->PRIMARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->SECONDARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->$VALUES:[Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;
    .locals 1

    .line 73
    const-class v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    return-object p0
.end method

.method public static values()[Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->$VALUES:[Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    invoke-virtual {v0}, [Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    return-object v0
.end method
