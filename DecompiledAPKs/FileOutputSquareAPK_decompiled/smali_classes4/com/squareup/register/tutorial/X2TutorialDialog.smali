.class public Lcom/squareup/register/tutorial/X2TutorialDialog;
.super Lcom/squareup/sqwidgets/ConfirmDialog;
.source "X2TutorialDialog.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/ConfirmDialog;-><init>(Landroid/content/Context;)V

    .line 17
    const-class v0, Lcom/squareup/register/tutorial/TutorialsComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialsComponent;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/TutorialsComponent;->tutorialPresenter()Lcom/squareup/register/tutorial/TutorialPresenter;

    move-result-object v0

    .line 18
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/tutorial/TutorialDialogScreen;

    iget-object v1, v1, Lcom/squareup/register/tutorial/TutorialDialogScreen;->prompt:Lcom/squareup/register/tutorial/TutorialDialog$Prompt;

    .line 20
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/X2TutorialDialog;->showLogo()Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 22
    invoke-interface {v1, p1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 24
    invoke-virtual {p0, v2}, Lcom/squareup/register/tutorial/X2TutorialDialog;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 27
    :cond_0
    invoke-interface {v1, p1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getContent(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 29
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/X2TutorialDialog;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 32
    :cond_1
    invoke-interface {v1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getSecondaryButton()I

    move-result p1

    const/4 v2, -0x1

    if-ne p1, v2, :cond_2

    .line 35
    new-instance p1, Lcom/squareup/register/tutorial/-$$Lambda$X2TutorialDialog$vt3Rh9D7BhBEmg4YpV96zfL1Fo4;

    invoke-direct {p1, v0, v1}, Lcom/squareup/register/tutorial/-$$Lambda$X2TutorialDialog$vt3Rh9D7BhBEmg4YpV96zfL1Fo4;-><init>(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V

    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/X2TutorialDialog;->setOnConfirmedBeforeDialogDismiss(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 36
    invoke-interface {v1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getPrimaryButton()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/X2TutorialDialog;->setBlueConfirmButtonAndText(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 38
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/X2TutorialDialog;->hideCancelButton()Lcom/squareup/sqwidgets/ConfirmDialog;

    goto :goto_0

    .line 41
    :cond_2
    new-instance v2, Lcom/squareup/register/tutorial/-$$Lambda$X2TutorialDialog$QF218vJB2Ol9M1_XzLMfqb0fctg;

    invoke-direct {v2, v0, v1}, Lcom/squareup/register/tutorial/-$$Lambda$X2TutorialDialog$QF218vJB2Ol9M1_XzLMfqb0fctg;-><init>(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V

    invoke-virtual {p0, v2}, Lcom/squareup/register/tutorial/X2TutorialDialog;->setOnDismissedBeforeDialogDismiss(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 42
    invoke-interface {v1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getPrimaryButton()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/squareup/register/tutorial/X2TutorialDialog;->setWhiteDismissButtonAndText(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 44
    new-instance v2, Lcom/squareup/register/tutorial/-$$Lambda$X2TutorialDialog$Q408KpodBuq_XmGP7Q-KRFkFhl0;

    invoke-direct {v2, v0, v1}, Lcom/squareup/register/tutorial/-$$Lambda$X2TutorialDialog$Q408KpodBuq_XmGP7Q-KRFkFhl0;-><init>(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V

    invoke-virtual {p0, v2}, Lcom/squareup/register/tutorial/X2TutorialDialog;->setOnConfirmedBeforeDialogDismiss(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/X2TutorialDialog;->setBlueConfirmButtonAndText(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    :goto_0
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->PRIMARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->onDialogButtonTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->PRIMARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->onDialogButtonTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V
    .locals 1

    .line 44
    sget-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->SECONDARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->onDialogButtonTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V

    return-void
.end method
