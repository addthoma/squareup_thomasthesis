.class final Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2;
.super Ljava/lang/Object;
.source "TutorialCogsHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/tutorial/TutorialCogsHelper;->hasItems()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "ignored",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/tutorial/TutorialCogsHelper;


# direct methods
.method constructor <init>(Lcom/squareup/register/tutorial/TutorialCogsHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2;->this$0:Lcom/squareup/register/tutorial/TutorialCogsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "ignored"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2;->this$0:Lcom/squareup/register/tutorial/TutorialCogsHelper;

    invoke-static {p1}, Lcom/squareup/register/tutorial/TutorialCogsHelper;->access$getCogs$p(Lcom/squareup/register/tutorial/TutorialCogsHelper;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cogs/Cogs;

    .line 41
    sget-object v0, Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2$1;->INSTANCE:Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2$1;

    check-cast v0, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {p1, v0}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    .line 42
    sget-object v0, Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2$2;->INSTANCE:Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2$2;

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Single;->map(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    const-string v0, "cogs.get()\n             \u2026          .toObservable()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/TutorialCogsHelper$hasItems$2;->apply(Ljava/lang/Boolean;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
