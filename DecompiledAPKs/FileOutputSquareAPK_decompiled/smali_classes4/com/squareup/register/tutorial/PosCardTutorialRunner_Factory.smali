.class public final Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;
.super Ljava/lang/Object;
.source "PosCardTutorialRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/PosCardTutorialRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final bankAccountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final bankLinkingStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final feeTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)",
            "Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/register/tutorial/PosCardTutorialRunner;
    .locals 7

    .line 59
    new-instance v6, Lcom/squareup/register/tutorial/PosCardTutorialRunner;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/tutorial/PosCardTutorialRunner;-><init>(Lflow/Flow;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/feetutorial/FeeTutorial;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/PosCardTutorialRunner;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v2, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v3, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/banklinking/BankLinkingStarter;

    iget-object v4, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/feetutorial/FeeTutorial;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/register/tutorial/PosCardTutorialRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/PosCardTutorialRunner_Factory;->get()Lcom/squareup/register/tutorial/PosCardTutorialRunner;

    move-result-object v0

    return-object v0
.end method
