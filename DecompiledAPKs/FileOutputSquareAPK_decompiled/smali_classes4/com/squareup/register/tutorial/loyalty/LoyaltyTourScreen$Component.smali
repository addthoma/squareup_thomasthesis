.class public interface abstract Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;
.super Ljava/lang/Object;
.source "LoyaltyTourScreen.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;
.end annotation

.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Component;",
        "",
        "coordinator",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;",
        "runner",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract coordinator()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;
.end method

.method public abstract runner()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;
.end method
