.class public final Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;
.super Ljava/lang/Object;
.source "LoyaltyTourCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final screenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;->screenRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;",
            ">;)",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;)Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;

    invoke-direct {v0, p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;-><init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;->screenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    invoke-static {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;->newInstance(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;)Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator_Factory;->get()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;

    move-result-object v0

    return-object v0
.end method
