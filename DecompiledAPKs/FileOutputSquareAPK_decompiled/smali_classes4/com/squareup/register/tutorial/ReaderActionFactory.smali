.class public Lcom/squareup/register/tutorial/ReaderActionFactory;
.super Ljava/lang/Object;
.source "ReaderActionFactory.java"


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;

.field private final customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

.field private final resources:Landroid/content/res/Resources;

.field private final tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/payment/Transaction;Landroid/content/res/Resources;Lcom/squareup/settings/server/Features;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/CountryCode;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->transaction:Lcom/squareup/payment/Transaction;

    .line 47
    iput-object p2, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->resources:Landroid/content/res/Resources;

    .line 48
    iput-object p7, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->countryCode:Lcom/squareup/CountryCode;

    .line 49
    iput-object p3, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->features:Lcom/squareup/settings/server/Features;

    .line 50
    iput-object p4, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    .line 51
    iput-object p5, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 52
    iput-object p6, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    return-void
.end method

.method static getReaderActions(Ljava/util/EnumSet;ZZ)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;ZZ)I"
        }
    .end annotation

    .line 114
    invoke-virtual {p0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 117
    sget-object v0, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_DIP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_SWIPE:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    sget-object v2, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_1

    if-eqz p1, :cond_0

    .line 120
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_swipe_dip_contactless:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_swipe_dip_tap:I

    :goto_0
    return p0

    :cond_1
    if-eqz p1, :cond_2

    .line 124
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_dip_swipe_contactless:I

    goto :goto_1

    :cond_2
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_tap_dip_swipe:I

    :goto_1
    return p0

    .line 128
    :cond_3
    sget-object v0, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_DIP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_SWIPE:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    .line 129
    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 128
    invoke-virtual {p0, v0}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p2, :cond_4

    .line 130
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_swipe_dip:I

    goto :goto_2

    :cond_4
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_dip_swipe:I

    :goto_2
    return p0

    .line 134
    :cond_5
    sget-object v0, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_DIP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/EnumSet;->containsAll(Ljava/util/Collection;)Z

    move-result p0

    if-eqz p0, :cond_8

    if-eqz p1, :cond_6

    .line 137
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_dip_contactless:I

    return p0

    :cond_6
    if-eqz p2, :cond_7

    .line 139
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_dip_tap:I

    goto :goto_3

    :cond_7
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_tap_dip:I

    :goto_3
    return p0

    .line 145
    :cond_8
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_swipe:I

    return p0

    .line 150
    :cond_9
    sget p0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_actions_connect:I

    return p0
.end method

.method private hasNoTenderOptions()Z
    .locals 3

    .line 88
    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    iget-object v1, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->customerManagementSettings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 89
    invoke-interface {v1}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->invoiceTenderSetting:Lcom/squareup/tenderpayment/InvoiceTenderSetting;

    .line 90
    invoke-interface {v2}, Lcom/squareup/tenderpayment/InvoiceTenderSetting;->canUseInvoiceAsTender()Z

    move-result v2

    .line 88
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderSettings(ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    .line 91
    iget-object v1, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    .line 92
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private mentionStartContactless()Z
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 98
    iget-object v1, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private useUsOrdering()Z
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->countryCode:Lcom/squareup/CountryCode;

    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public paymentTypePromptFor(Ljava/util/EnumSet;)Ljava/lang/CharSequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 74
    invoke-direct {p0}, Lcom/squareup/register/tutorial/ReaderActionFactory;->hasNoTenderOptions()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_action_or_enable:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_action_or_choose:I

    .line 78
    :goto_0
    invoke-direct {p0}, Lcom/squareup/register/tutorial/ReaderActionFactory;->mentionStartContactless()Z

    move-result v1

    .line 80
    invoke-direct {p0}, Lcom/squareup/register/tutorial/ReaderActionFactory;->useUsOrdering()Z

    move-result v2

    invoke-static {p1, v1, v2}, Lcom/squareup/register/tutorial/ReaderActionFactory;->getReaderActions(Ljava/util/EnumSet;ZZ)I

    move-result p1

    .line 82
    iget-object v1, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->resources:Landroid/content/res/Resources;

    invoke-static {v1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/tutorial/ReaderActionFactory;->resources:Landroid/content/res/Resources;

    .line 83
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "actions"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 84
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
