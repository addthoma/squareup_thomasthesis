.class public Lcom/squareup/register/tutorial/TutorialView;
.super Landroid/widget/LinearLayout;
.source "TutorialView.java"


# instance fields
.field private closeButton:Lcom/squareup/glyph/SquareGlyphView;

.field private defaultBackground:Landroid/graphics/drawable/Drawable;

.field presenter:Lcom/squareup/register/tutorial/TutorialPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private text:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const-class p2, Lcom/squareup/register/tutorial/TutorialsComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/tutorial/TutorialsComponent;

    invoke-interface {p1, p0}, Lcom/squareup/register/tutorial/TutorialsComponent;->inject(Lcom/squareup/register/tutorial/TutorialView;)V

    return-void
.end method


# virtual methods
.method public hideTutorialBar()V
    .locals 1

    const/16 v0, 0x8

    .line 62
    invoke-virtual {p0, v0}, Lcom/squareup/register/tutorial/TutorialView;->setVisibility(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialView;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->dropView(Ljava/lang/Object;)V

    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 33
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 34
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial_bar_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/register/tutorial/TutorialView;->text:Landroid/widget/TextView;

    .line 35
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/tutorial/TutorialView;->defaultBackground:Landroid/graphics/drawable/Drawable;

    .line 36
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialView;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->takeView(Ljava/lang/Object;)V

    .line 38
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial_bar_cancel:I

    invoke-virtual {p0, v0}, Lcom/squareup/register/tutorial/TutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/register/tutorial/TutorialView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 39
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/register/tutorial/TutorialView$1;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/TutorialView$1;-><init>(Lcom/squareup/register/tutorial/TutorialView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    new-instance v0, Lcom/squareup/register/tutorial/TutorialView$2;

    invoke-direct {v0, p0}, Lcom/squareup/register/tutorial/TutorialView$2;-><init>(Lcom/squareup/register/tutorial/TutorialView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/register/tutorial/TutorialView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setBarContent(Ljava/lang/CharSequence;Ljava/lang/Integer;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialView;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 70
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/TutorialView;->setBackgroundResource(I)V

    .line 71
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    goto :goto_0

    .line 73
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialView;->defaultBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/TutorialView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/register/tutorial/TutorialView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_green_pressed:I

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    :goto_0
    return-void
.end method

.method public showTutorialBar()V
    .locals 1

    const/4 v0, 0x0

    .line 58
    invoke-virtual {p0, v0}, Lcom/squareup/register/tutorial/TutorialView;->setVisibility(I)V

    return-void
.end method
