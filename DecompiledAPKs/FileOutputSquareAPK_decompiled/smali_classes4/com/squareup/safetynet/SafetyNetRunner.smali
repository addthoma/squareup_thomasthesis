.class public Lcom/squareup/safetynet/SafetyNetRunner;
.super Ljava/lang/Object;
.source "SafetyNetRunner.java"


# static fields
.field private static final RETRIES:I = 0x3

.field private static final RETRY_DELAY:I = 0xf


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final safetyNetService:Lcom/squareup/server/SafetyNetService;

.field private final safetyNetWrapper:Lcom/squareup/safetynet/SafetyNetWrapper;


# direct methods
.method constructor <init>(Lcom/squareup/server/SafetyNetService;Ljava/util/concurrent/Executor;Lcom/squareup/safetynet/SafetyNetWrapper;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/safetynet/SafetyNetRunner;->safetyNetService:Lcom/squareup/server/SafetyNetService;

    .line 39
    iput-object p2, p0, Lcom/squareup/safetynet/SafetyNetRunner;->executor:Ljava/util/concurrent/Executor;

    .line 40
    iput-object p3, p0, Lcom/squareup/safetynet/SafetyNetRunner;->safetyNetWrapper:Lcom/squareup/safetynet/SafetyNetWrapper;

    .line 41
    iput-object p4, p0, Lcom/squareup/safetynet/SafetyNetRunner;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private getAttestationParameters()Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;
    .locals 3

    .line 108
    :try_start_0
    iget-object v0, p0, Lcom/squareup/safetynet/SafetyNetRunner;->safetyNetService:Lcom/squareup/server/SafetyNetService;

    new-instance v1, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationRequest;

    invoke-direct {v1}, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationRequest;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/server/SafetyNetService;->startAttestation(Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationRequest;)Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;

    move-result-object v0
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 110
    invoke-virtual {v0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v1

    sget-object v2, Lretrofit/RetrofitError$Kind;->UNEXPECTED:Lretrofit/RetrofitError$Kind;

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Problem performing StartAttestation RPC. Skipping SafetyNet run."

    .line 113
    invoke-static {v0, v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0

    .line 111
    :cond_0
    throw v0
.end method

.method private submitAttestationResults()V
    .locals 4

    .line 119
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getAttestationResult()Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getSafetyNetWrapperStatus()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object v1

    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getGmsApiStatus()I

    move-result v2

    .line 121
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getSafetyNetApiStatus()I

    move-result v3

    .line 119
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/squareup/safetynet/SafetyNetRunner;->validateAttestation(Ljava/lang/String;Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;II)V

    return-void
.end method

.method private validateAttestation(Ljava/lang/String;Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;II)V
    .locals 2

    .line 132
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;-><init>()V

    if-eqz p1, :cond_0

    .line 135
    invoke-static {p1}, Lokio/ByteString;->encodeUtf8(Ljava/lang/String;)Lokio/ByteString;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 134
    :goto_0
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->attestation_result(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;

    move-result-object p1

    .line 136
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->safetynet_api_status(Ljava/lang/Integer;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;

    move-result-object p1

    .line 137
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->gms_api_status(Ljava/lang/Integer;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;

    move-result-object p1

    .line 138
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->safetynet_wrapper_status(Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->build()Lcom/squareup/protos/client/flipper/SafetyNetAttestation;

    move-result-object p1

    .line 133
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest$Builder;->attestation(Lcom/squareup/protos/client/flipper/SafetyNetAttestation;)Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest$Builder;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest$Builder;->build()Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest;

    move-result-object p1

    .line 142
    :try_start_0
    iget-object p2, p0, Lcom/squareup/safetynet/SafetyNetRunner;->safetyNetService:Lcom/squareup/server/SafetyNetService;

    invoke-interface {p2, p1}, Lcom/squareup/server/SafetyNetService;->validateAttestation(Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationRequest;)Lcom/squareup/protos/client/flipper/SafetyNetValidateAttestationResponse;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 144
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object p2

    sget-object p3, Lretrofit/RetrofitError$Kind;->UNEXPECTED:Lretrofit/RetrofitError$Kind;

    if-eq p2, p3, :cond_1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string p3, "Problem performing ValidateAttestation RPC. Failing silently."

    .line 147
    invoke-static {p1, p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void

    .line 145
    :cond_1
    throw p1
.end method


# virtual methods
.method public runSafetyNetAttestationAsync()V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/safetynet/SafetyNetRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SAFETYNET:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/squareup/safetynet/SafetyNetRunner;->executor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/safetynet/-$$Lambda$V6qF88JZB_EazU_LNbkHlCDEY_w;

    invoke-direct {v1, p0}, Lcom/squareup/safetynet/-$$Lambda$V6qF88JZB_EazU_LNbkHlCDEY_w;-><init>(Lcom/squareup/safetynet/SafetyNetRunner;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method runSafetyNetAttestationBlocking()V
    .locals 5

    .line 66
    invoke-direct {p0}, Lcom/squareup/safetynet/SafetyNetRunner;->getAttestationParameters()Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 77
    :cond_0
    iget-object v1, v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    if-nez v1, :cond_1

    return-void

    .line 82
    :cond_1
    iget-object v1, p0, Lcom/squareup/safetynet/SafetyNetRunner;->safetyNetWrapper:Lcom/squareup/safetynet/SafetyNetWrapper;

    iget-object v2, v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->nonce:Lokio/ByteString;

    .line 83
    invoke-virtual {v2}, Lokio/ByteString;->toByteArray()[B

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->api_key:Ljava/lang/String;

    const/4 v3, 0x3

    const/16 v4, 0xf

    .line 82
    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/squareup/safetynet/SafetyNetWrapper;->attestWithRetry([BLjava/lang/String;II)V

    .line 89
    invoke-direct {p0}, Lcom/squareup/safetynet/SafetyNetRunner;->submitAttestationResults()V

    return-void
.end method
