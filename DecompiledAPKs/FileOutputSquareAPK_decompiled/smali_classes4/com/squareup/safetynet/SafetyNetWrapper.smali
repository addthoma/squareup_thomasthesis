.class public Lcom/squareup/safetynet/SafetyNetWrapper;
.super Ljava/lang/Object;
.source "SafetyNetWrapper.java"


# static fields
.field private static final NONCE_MIN_LENGTH:I = 0x10

.field static final STATUS_INITIAL_VALUE:I = -0x1

.field private static attestationResult:Ljava/lang/String; = null

.field private static gmsApiStatus:I = -0x1

.field private static safetynetApiStatus:I = -0x1

.field private static safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;


# instance fields
.field private final application:Landroid/app/Application;

.field private final googleApiAvailability:Lcom/google/android/gms/common/GoogleApiAvailability;

.field private final safetyNetClient:Lcom/google/android/gms/safetynet/SafetyNetClient;

.field private final taskWaiter:Lcom/squareup/safetynet/TaskWaiter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INIT:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-void
.end method

.method constructor <init>(Landroid/app/Application;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/squareup/safetynet/TaskWaiter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/safetynet/SafetyNetWrapper;->application:Landroid/app/Application;

    .line 60
    iput-object p2, p0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetyNetClient:Lcom/google/android/gms/safetynet/SafetyNetClient;

    .line 61
    iput-object p3, p0, Lcom/squareup/safetynet/SafetyNetWrapper;->googleApiAvailability:Lcom/google/android/gms/common/GoogleApiAvailability;

    .line 62
    iput-object p4, p0, Lcom/squareup/safetynet/SafetyNetWrapper;->taskWaiter:Lcom/squareup/safetynet/TaskWaiter;

    return-void
.end method

.method private static attestationNeedsToBeRetried(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x1

    if-nez p0, :cond_0

    return v0

    .line 189
    :cond_0
    invoke-static {p0}, Lcom/squareup/safetynet/SafetyNetWrapper;->getJson(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_1

    return v0

    .line 196
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const-string p0, "error"

    .line 200
    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    return v0

    :cond_2
    const/4 p0, 0x0

    return p0

    :catch_0
    return v0
.end method

.method private checkNonce([B)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 286
    array-length p1, p1

    const/16 v0, 0x10

    if-lt p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private connectGoogleApiClient()Z
    .locals 3

    .line 119
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTING:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 121
    iget-object v0, p0, Lcom/squareup/safetynet/SafetyNetWrapper;->googleApiAvailability:Lcom/google/android/gms/common/GoogleApiAvailability;

    iget-object v1, p0, Lcom/squareup/safetynet/SafetyNetWrapper;->application:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/squareup/safetynet/SafetyNetWrapper;->gmsApiStatus:I

    .line 122
    sget v0, Lcom/squareup/safetynet/SafetyNetWrapper;->gmsApiStatus:I

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 123
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_REQUIRES_UPDATE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return v1

    :cond_0
    if-nez v0, :cond_1

    .line 127
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v0, 0x1

    return v0

    .line 130
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->CONNECTION_FAILED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return v1
.end method

.method private doAttestation([BLjava/lang/String;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;
    .locals 1

    .line 142
    invoke-direct {p0}, Lcom/squareup/safetynet/SafetyNetWrapper;->connectGoogleApiClient()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getSafetyNetWrapperStatus()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object p1

    return-object p1

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetyNetClient:Lcom/google/android/gms/safetynet/SafetyNetClient;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/safetynet/SafetyNetClient;->attest([BLjava/lang/String;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    .line 148
    :try_start_0
    iget-object p2, p0, Lcom/squareup/safetynet/SafetyNetWrapper;->taskWaiter:Lcom/squareup/safetynet/TaskWaiter;

    .line 149
    invoke-virtual {p2, p1}, Lcom/squareup/safetynet/TaskWaiter;->awaitTaskResult(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/safetynet/SafetyNetApi$AttestationResponse;

    const/4 p2, 0x0

    .line 150
    sput p2, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetApiStatus:I

    .line 151
    sget-object p2, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->SUCCESS:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object p2, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 152
    invoke-virtual {p1}, Lcom/google/android/gms/safetynet/SafetyNetApi$AttestationResponse;->getJwsResult()Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/squareup/safetynet/SafetyNetWrapper;->attestationResult:Ljava/lang/String;

    .line 153
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getSafetyNetWrapperStatus()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 158
    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    instance-of p2, p2, Lcom/google/android/gms/common/api/ApiException;

    if-eqz p2, :cond_2

    .line 159
    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/common/api/ApiException;

    .line 161
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/ApiException;->getStatusCode()I

    move-result p1

    sput p1, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetApiStatus:I

    .line 165
    sget p1, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetApiStatus:I

    const/16 p2, 0x10

    if-eq p1, p2, :cond_1

    .line 170
    sget-object p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object p1, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    goto :goto_0

    .line 167
    :cond_1
    sget-object p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_RATE_LIMITED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object p1, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 173
    :goto_0
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getSafetyNetWrapperStatus()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object p1

    return-object p1

    .line 175
    :cond_2
    sget-object p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object p1, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 176
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getSafetyNetWrapperStatus()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object p1

    return-object p1

    .line 155
    :catch_1
    sget-object p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object p1, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 156
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getSafetyNetWrapperStatus()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object p1

    return-object p1
.end method

.method public static getAttestationResult()Ljava/lang/String;
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->attestationResult:Ljava/lang/String;

    return-object v0
.end method

.method public static getGmsApiStatus()I
    .locals 1

    .line 78
    sget v0, Lcom/squareup/safetynet/SafetyNetWrapper;->gmsApiStatus:I

    return v0
.end method

.method private static getJson(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const-string v1, "."

    .line 256
    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 257
    array-length v1, p0

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    .line 263
    :try_start_0
    aget-object p0, p0, v1

    const/4 v1, 0x0

    invoke-static {p0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p0, :cond_3

    .line 267
    array-length v1, p0

    if-nez v1, :cond_2

    goto :goto_0

    .line 271
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    :catch_0
    :cond_3
    :goto_0
    return-object v0
.end method

.method public static getSafetyNetApiStatus()I
    .locals 1

    .line 85
    sget v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetApiStatus:I

    return v0
.end method

.method public static getSafetyNetWrapperStatus()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;
    .locals 1

    .line 92
    sget-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object v0
.end method

.method public static reinitializeStaticMembers()V
    .locals 1

    const/4 v0, -0x1

    .line 276
    sput v0, Lcom/squareup/safetynet/SafetyNetWrapper;->gmsApiStatus:I

    .line 277
    sput v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetApiStatus:I

    .line 278
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INIT:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v0, 0x0

    .line 279
    sput-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->attestationResult:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public attest([BLjava/lang/String;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;
    .locals 1

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/safetynet/SafetyNetWrapper;->checkNonce([B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    sget-object p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->NONCE_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object p1, Lcom/squareup/safetynet/SafetyNetWrapper;->safetynetWrapperStatus:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 110
    invoke-static {}, Lcom/squareup/safetynet/SafetyNetWrapper;->getSafetyNetWrapperStatus()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object p1

    return-object p1

    .line 112
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/safetynet/SafetyNetWrapper;->doAttestation([BLjava/lang/String;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object p1

    return-object p1
.end method

.method public attestWithRetry([BLjava/lang/String;II)V
    .locals 4

    if-lez p4, :cond_0

    goto :goto_0

    :cond_0
    const/4 p4, 0x1

    .line 217
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/safetynet/SafetyNetWrapper;->attest([BLjava/lang/String;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p3, :cond_3

    .line 220
    sget-object v2, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_REQUIRES_UPDATE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-ne v0, v2, :cond_1

    goto :goto_2

    .line 223
    :cond_1
    sget-object v2, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INTERNAL_FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTING:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->PROGRESS_CONNECTED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->CONNECTION_FAILED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->GMS_RATE_LIMITED:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->FAILURE:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-eq v0, v2, :cond_2

    sget-object v0, Lcom/squareup/safetynet/SafetyNetWrapper;->attestationResult:Ljava/lang/String;

    .line 229
    invoke-static {v0}, Lcom/squareup/safetynet/SafetyNetWrapper;->attestationNeedsToBeRetried(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    mul-int/lit16 v0, p4, 0x3e8

    int-to-long v2, v0

    .line 235
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    mul-int/lit8 p4, p4, 0x2

    .line 242
    invoke-virtual {p0, p1, p2}, Lcom/squareup/safetynet/SafetyNetWrapper;->attest([BLjava/lang/String;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    :cond_3
    :goto_2
    return-void
.end method
