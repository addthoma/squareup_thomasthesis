.class public final Lcom/squareup/safetynet/SafetyNetRunner_Factory;
.super Ljava/lang/Object;
.source "SafetyNetRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/safetynet/SafetyNetRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final executorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final safetyNetServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/SafetyNetService;",
            ">;"
        }
    .end annotation
.end field

.field private final safetyNetWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/SafetyNetWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/SafetyNetService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/SafetyNetWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->safetyNetServiceProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->executorProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->safetyNetWrapperProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/safetynet/SafetyNetRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/SafetyNetService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynet/SafetyNetWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/safetynet/SafetyNetRunner_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/safetynet/SafetyNetRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/SafetyNetService;Ljava/util/concurrent/Executor;Lcom/squareup/safetynet/SafetyNetWrapper;Lcom/squareup/settings/server/Features;)Lcom/squareup/safetynet/SafetyNetRunner;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/safetynet/SafetyNetRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/safetynet/SafetyNetRunner;-><init>(Lcom/squareup/server/SafetyNetService;Ljava/util/concurrent/Executor;Lcom/squareup/safetynet/SafetyNetWrapper;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/safetynet/SafetyNetRunner;
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->safetyNetServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/SafetyNetService;

    iget-object v1, p0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->executorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->safetyNetWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/safetynet/SafetyNetWrapper;

    iget-object v3, p0, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->newInstance(Lcom/squareup/server/SafetyNetService;Ljava/util/concurrent/Executor;Lcom/squareup/safetynet/SafetyNetWrapper;Lcom/squareup/settings/server/Features;)Lcom/squareup/safetynet/SafetyNetRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/safetynet/SafetyNetRunner_Factory;->get()Lcom/squareup/safetynet/SafetyNetRunner;

    move-result-object v0

    return-object v0
.end method
