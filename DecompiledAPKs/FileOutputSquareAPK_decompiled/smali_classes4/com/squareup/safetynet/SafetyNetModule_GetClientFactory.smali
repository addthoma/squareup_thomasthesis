.class public final Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;
.super Ljava/lang/Object;
.source "SafetyNetModule_GetClientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/google/android/gms/safetynet/SafetyNetClient;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;

    invoke-direct {v0, p0}, Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static getClient(Landroid/app/Application;)Lcom/google/android/gms/safetynet/SafetyNetClient;
    .locals 1

    .line 31
    invoke-static {p0}, Lcom/squareup/safetynet/SafetyNetModule;->getClient(Landroid/app/Application;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/android/gms/safetynet/SafetyNetClient;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/google/android/gms/safetynet/SafetyNetClient;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;->getClient(Landroid/app/Application;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/safetynet/SafetyNetModule_GetClientFactory;->get()Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v0

    return-object v0
.end method
