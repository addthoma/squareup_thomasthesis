.class public Lcom/squareup/radiography/Xrays$Builder;
.super Ljava/lang/Object;
.source "Xrays.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/radiography/Xrays;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private showTextFieldContent:Z

.field private skippedIds:[I

.field private textFieldMaxLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    .line 35
    iput v0, p0, Lcom/squareup/radiography/Xrays$Builder;->textFieldMaxLength:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/radiography/Xrays$Builder;)Z
    .locals 0

    .line 33
    iget-boolean p0, p0, Lcom/squareup/radiography/Xrays$Builder;->showTextFieldContent:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/radiography/Xrays$Builder;)I
    .locals 0

    .line 33
    iget p0, p0, Lcom/squareup/radiography/Xrays$Builder;->textFieldMaxLength:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/radiography/Xrays$Builder;)[I
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/radiography/Xrays$Builder;->skippedIds:[I

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/radiography/Xrays;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/radiography/Xrays;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/radiography/Xrays;-><init>(Lcom/squareup/radiography/Xrays$Builder;Lcom/squareup/radiography/Xrays$1;)V

    return-object v0
.end method

.method public showTextFieldContent(Z)Lcom/squareup/radiography/Xrays$Builder;
    .locals 0

    .line 39
    iput-boolean p1, p0, Lcom/squareup/radiography/Xrays$Builder;->showTextFieldContent:Z

    return-object p0
.end method

.method public varargs skippedIds([I)Lcom/squareup/radiography/Xrays$Builder;
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {p1}, [I->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [I

    :goto_0
    iput-object p1, p0, Lcom/squareup/radiography/Xrays$Builder;->skippedIds:[I

    return-object p0
.end method

.method public textFieldMaxLength(I)Lcom/squareup/radiography/Xrays$Builder;
    .locals 3

    if-lez p1, :cond_0

    .line 47
    iput p1, p0, Lcom/squareup/radiography/Xrays$Builder;->textFieldMaxLength:I

    return-object p0

    .line 45
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "textFieldMaxLength="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " <= 0"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
