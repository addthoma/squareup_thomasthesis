.class public final Lcom/squareup/salesreport/util/SalesSummaryReportsKt;
.super Ljava/lang/Object;
.source "SalesSummaryReports.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesSummaryReports.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesSummaryReports.kt\ncom/squareup/salesreport/util/SalesSummaryReportsKt\n*L\n1#1,219:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0004\n\u0002\u0008\u0002\u001a\u001a\u0010\u0005\u001a\u0004\u0018\u00010\u0002*\u0004\u0018\u00010\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u0002\u001a8\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t*\u00020\u000b2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00060\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0000\u001a6\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\t*\u00020\u000b2\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00170\r2\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0000\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0019"
    }
    d2 = {
        "color",
        "",
        "Lcom/squareup/util/Percentage;",
        "getColor",
        "(Lcom/squareup/util/Percentage;)I",
        "percentageChange",
        "Lcom/squareup/protos/common/Money;",
        "newValue",
        "salesDetailsRows",
        "",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;",
        "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "percentageChangeFormatter",
        "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "salesOverviewRows",
        "Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;",
        "compactNumberFormatter",
        "",
        "compactMoneyFormatter",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final getColor(Lcom/squareup/util/Percentage;)I
    .locals 0

    .line 216
    invoke-virtual {p0}, Lcom/squareup/util/Percentage;->isNegative()Z

    move-result p0

    if-eqz p0, :cond_0

    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_summary_negative_percentage_color:I

    goto :goto_0

    .line 217
    :cond_0
    sget p0, Lcom/squareup/salesreport/impl/R$color;->sales_report_summary_positive_percentage_color:I

    :goto_0
    return p0
.end method

.method private static final percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;
    .locals 2

    if-eqz p0, :cond_3

    if-nez p1, :cond_0

    goto :goto_1

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 207
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v0, "amount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Number;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v0, "newValue.amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Number;

    invoke-static {p0, p1}, Lcom/squareup/util/math/NumbersKt;->percentageChange(Ljava/lang/Number;Ljava/lang/Number;)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0

    .line 205
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Currencies don\'t match: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " and "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 204
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_3
    :goto_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static final salesDetailsRows(Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)Ljava/util/List;
    .locals 38
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    const-string v3, "$this$salesDetailsRows"

    move-object/from16 v4, p0

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "moneyFormatter"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "percentageChangeFormatter"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "settings"

    move-object/from16 v5, p3

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "features"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v3, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;

    invoke-direct {v3, v0}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;-><init>(Lcom/squareup/text/Formatter;)V

    .line 92
    new-instance v0, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$2;

    invoke-direct {v0, v1}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$2;-><init>(Lcom/squareup/salesreport/util/PercentageChangeFormatter;)V

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object v1

    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getComparisonSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object v4

    const/4 v6, 0x0

    if-eqz v4, :cond_0

    .line 98
    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v7

    goto :goto_0

    :cond_0
    move-object v7, v6

    :goto_0
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;

    move-result-object v7

    if-eqz v4, :cond_1

    .line 99
    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object v8

    goto :goto_1

    :cond_1
    move-object v8, v6

    :goto_1
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;

    move-result-object v8

    if-eqz v4, :cond_2

    .line 100
    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getTips()Lcom/squareup/protos/common/Money;

    move-result-object v9

    goto :goto_2

    :cond_2
    move-object v9, v6

    :goto_2
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getTips()Lcom/squareup/protos/common/Money;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;

    move-result-object v9

    if-eqz v4, :cond_3

    .line 101
    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v10

    goto :goto_3

    :cond_3
    move-object v10, v6

    :goto_3
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;

    move-result-object v10

    if-eqz v4, :cond_4

    .line 102
    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getGiftCardSales()Lcom/squareup/protos/common/Money;

    move-result-object v4

    goto :goto_4

    :cond_4
    move-object v4, v6

    :goto_4
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getGiftCardSales()Lcom/squareup/protos/common/Money;

    move-result-object v11

    invoke-static {v4, v11}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;

    move-result-object v4

    const/16 v11, 0xa

    new-array v11, v11, [Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    const/4 v12, 0x0

    .line 105
    new-instance v25, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 106
    new-instance v13, Lcom/squareup/util/ViewString$ResourceString;

    sget v14, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_gross_sales:I

    invoke-direct {v13, v14}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v14, v13

    check-cast v14, Lcom/squareup/util/ViewString;

    .line 107
    invoke-virtual {v0, v7}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$2;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v17

    if-eqz v7, :cond_5

    .line 108
    invoke-static {v7}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v18, v7

    goto :goto_5

    :cond_5
    move-object/from16 v18, v6

    .line 109
    :goto_5
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v15

    const/16 v19, 0x0

    const/16 v16, 0x1

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1c0

    const/16 v24, 0x0

    move-object/from16 v13, v25

    .line 105
    invoke-direct/range {v13 .. v24}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v25, v11, v12

    .line 113
    new-instance v7, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 114
    new-instance v12, Lcom/squareup/util/ViewString$ResourceString;

    sget v13, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_refund:I

    invoke-direct {v12, v13}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object/from16 v27, v12

    check-cast v27, Lcom/squareup/util/ViewString;

    .line 116
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getRefunds()Lcom/squareup/protos/common/Money;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v28

    const/16 v32, 0x0

    const/16 v31, 0x0

    const/16 v29, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x1d0

    const/16 v37, 0x0

    const-string v30, ""

    move-object/from16 v26, v7

    .line 113
    invoke-direct/range {v26 .. v37}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v12, 0x1

    aput-object v7, v11, v12

    const/4 v7, 0x2

    .line 120
    new-instance v25, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 121
    new-instance v13, Lcom/squareup/util/ViewString$ResourceString;

    sget v14, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_discounts_and_comps:I

    invoke-direct {v13, v14}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v14, v13

    check-cast v14, Lcom/squareup/util/ViewString;

    .line 123
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v15

    const/16 v18, 0x0

    const/16 v16, 0x0

    const/16 v23, 0x1d0

    const-string v17, ""

    move-object/from16 v13, v25

    .line 120
    invoke-direct/range {v13 .. v24}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v25, v11, v7

    const/4 v7, 0x3

    .line 127
    new-instance v25, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 128
    new-instance v13, Lcom/squareup/util/ViewString$ResourceString;

    sget v14, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_net_sales:I

    invoke-direct {v13, v14}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v14, v13

    check-cast v14, Lcom/squareup/util/ViewString;

    .line 129
    invoke-virtual {v0, v8}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$2;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v17

    if-eqz v8, :cond_6

    .line 130
    invoke-static {v8}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v18, v8

    goto :goto_6

    :cond_6
    move-object/from16 v18, v6

    .line 131
    :goto_6
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v15

    const/16 v19, 0x0

    const/16 v16, 0x1

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1c0

    const/16 v24, 0x0

    move-object/from16 v13, v25

    .line 127
    invoke-direct/range {v13 .. v24}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v25, v11, v7

    const/4 v7, 0x4

    .line 135
    new-instance v8, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 136
    new-instance v13, Lcom/squareup/util/ViewString$ResourceString;

    sget v14, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_tax:I

    invoke-direct {v13, v14}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v14, v13

    check-cast v14, Lcom/squareup/util/ViewString;

    .line 138
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getTax()Lcom/squareup/protos/common/Money;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v15

    const/16 v18, 0x0

    const/16 v16, 0x0

    const/16 v23, 0x1d0

    const-string v17, ""

    move-object v13, v8

    .line 135
    invoke-direct/range {v13 .. v24}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v8, v11, v7

    const/4 v7, 0x5

    .line 142
    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->HIDE_TIPS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 143
    new-instance v2, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 144
    new-instance v8, Lcom/squareup/util/ViewString$ResourceString;

    sget v13, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_tips:I

    invoke-direct {v8, v13}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v14, v8

    check-cast v14, Lcom/squareup/util/ViewString;

    .line 145
    invoke-virtual {v0, v9}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$2;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v17

    if-eqz v9, :cond_7

    .line 146
    invoke-static {v9}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v18, v8

    goto :goto_7

    :cond_7
    move-object/from16 v18, v6

    .line 147
    :goto_7
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getTips()Lcom/squareup/protos/common/Money;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v15

    const/16 v19, 0x0

    const/16 v16, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1c0

    const/16 v24, 0x0

    move-object v13, v2

    .line 143
    invoke-direct/range {v13 .. v24}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_8

    :cond_8
    move-object v2, v6

    :goto_8
    aput-object v2, v11, v7

    const/4 v2, 0x6

    .line 154
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 155
    new-instance v5, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 156
    new-instance v7, Lcom/squareup/util/ViewString$ResourceString;

    sget v8, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_gift_cards:I

    invoke-direct {v7, v8}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v14, v7

    check-cast v14, Lcom/squareup/util/ViewString;

    .line 157
    invoke-virtual {v0, v4}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$2;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v17

    if-eqz v4, :cond_9

    .line 158
    invoke-static {v4}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v18, v4

    goto :goto_9

    :cond_9
    move-object/from16 v18, v6

    .line 159
    :goto_9
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getGiftCardSales()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v15

    const/16 v19, 0x0

    const/16 v16, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1c0

    const/16 v24, 0x0

    move-object v13, v5

    .line 155
    invoke-direct/range {v13 .. v24}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_a

    :cond_a
    move-object v5, v6

    :goto_a
    aput-object v5, v11, v2

    const/4 v2, 0x7

    .line 166
    new-instance v4, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 167
    new-instance v5, Lcom/squareup/util/ViewString$ResourceString;

    sget v7, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_refunds_by_amount:I

    invoke-direct {v5, v7}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v14, v5

    check-cast v14, Lcom/squareup/util/ViewString;

    .line 169
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getCredit()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v15

    const/16 v19, 0x0

    const/16 v18, 0x0

    const/16 v16, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1d0

    const/16 v24, 0x0

    const-string v17, ""

    move-object v13, v4

    .line 166
    invoke-direct/range {v13 .. v24}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v4, v11, v2

    const/16 v2, 0x8

    .line 173
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getCashRounding()Lcom/squareup/protos/common/Money;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v4, :cond_b

    invoke-static {v4}, Lcom/squareup/currency/CurrencyCodesKt;->getDoesSwedishRounding(Lcom/squareup/protos/common/CurrencyCode;)Z

    move-result v4

    if-ne v4, v12, :cond_b

    .line 174
    new-instance v4, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 175
    new-instance v5, Lcom/squareup/util/ViewString$ResourceString;

    sget v7, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_cash_rounding:I

    invoke-direct {v5, v7}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v14, v5

    check-cast v14, Lcom/squareup/util/ViewString;

    .line 177
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getCashRounding()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v15

    const/16 v19, 0x0

    const/16 v18, 0x0

    const/16 v16, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1d0

    const/16 v24, 0x0

    const-string v17, ""

    move-object v13, v4

    .line 174
    invoke-direct/range {v13 .. v24}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_b

    :cond_b
    move-object v4, v6

    :goto_b
    aput-object v4, v11, v2

    const/16 v2, 0x9

    .line 184
    new-instance v4, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;

    .line 185
    new-instance v5, Lcom/squareup/util/ViewString$ResourceString;

    sget v7, Lcom/squareup/salesreport/impl/R$string;->sales_report_details_total:I

    invoke-direct {v5, v7}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v13, v5

    check-cast v13, Lcom/squareup/util/ViewString;

    .line 186
    invoke-virtual {v0, v10}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$2;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v16

    if-eqz v10, :cond_c

    .line 187
    invoke-static {v10}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    :cond_c
    move-object/from16 v17, v6

    .line 188
    invoke-virtual {v1}, Lcom/squareup/customreport/data/SalesSummary;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v14

    const/16 v18, 0x0

    const/4 v15, 0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x1c0

    const/16 v23, 0x0

    move-object v12, v4

    .line 184
    invoke-direct/range {v12 .. v23}, Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow$FullSalesDetailsRow;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Integer;ZZZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v4, v11, v2

    .line 104
    invoke-static {v11}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static final salesOverviewRows(Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "$this$salesOverviewRows"

    move-object/from16 v4, p0

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "compactNumberFormatter"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "compactMoneyFormatter"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "percentageChangeFormatter"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v3, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$1;

    invoke-direct {v3, v0}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$1;-><init>(Lcom/squareup/text/Formatter;)V

    .line 30
    new-instance v0, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$2;

    invoke-direct {v0, v1}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$2;-><init>(Lcom/squareup/text/Formatter;)V

    .line 33
    new-instance v1, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$3;

    invoke-direct {v1, v2}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$3;-><init>(Lcom/squareup/salesreport/util/PercentageChangeFormatter;)V

    .line 37
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getComparisonSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object v2

    .line 38
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object v4

    const/4 v5, 0x0

    if-eqz v2, :cond_0

    .line 39
    invoke-virtual {v2}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;

    move-result-object v6

    goto :goto_0

    :cond_0
    move-object v6, v5

    :goto_0
    if-eqz v2, :cond_1

    .line 40
    invoke-virtual {v2}, Lcom/squareup/customreport/data/SalesSummary;->getSalesCount()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getSalesCount()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    check-cast v8, Ljava/lang/Number;

    invoke-static {v7, v8}, Lcom/squareup/util/math/NumbersKt;->percentageChange(Ljava/lang/Number;Ljava/lang/Number;)Lcom/squareup/util/Percentage;

    move-result-object v7

    goto :goto_1

    :cond_1
    move-object v7, v5

    :goto_1
    if-eqz v2, :cond_2

    .line 41
    invoke-virtual {v2}, Lcom/squareup/customreport/data/SalesSummary;->getAverageSale()Lcom/squareup/protos/common/Money;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getAverageSale()Lcom/squareup/protos/common/Money;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;

    move-result-object v8

    goto :goto_2

    :cond_2
    move-object v8, v5

    :goto_2
    if-eqz v2, :cond_3

    .line 42
    invoke-virtual {v2}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->percentageChange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/util/Percentage;

    move-result-object v2

    goto :goto_3

    :cond_3
    move-object v2, v5

    .line 45
    :goto_3
    new-instance v16, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;

    .line 46
    new-instance v10, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    .line 47
    new-instance v9, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getGrossSales()Lcom/squareup/protos/common/Money;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$2;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v11

    check-cast v11, Ljava/lang/CharSequence;

    invoke-direct {v9, v11}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v9, Lcom/squareup/util/ViewString;

    .line 48
    invoke-virtual {v1, v6}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$3;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v11

    if-eqz v6, :cond_4

    .line 49
    invoke-static {v6}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_4

    :cond_4
    move-object v6, v5

    .line 46
    :goto_4
    invoke-direct {v10, v9, v11, v6}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 51
    new-instance v11, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    .line 52
    new-instance v6, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getSalesCount()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$1;->invoke(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v6, v3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v6, Lcom/squareup/util/ViewString;

    .line 53
    invoke-virtual {v1, v7}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$3;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v3

    if-eqz v7, :cond_5

    .line 54
    invoke-static {v7}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_5

    :cond_5
    move-object v7, v5

    .line 51
    :goto_5
    invoke-direct {v11, v6, v3, v7}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 56
    new-instance v12, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    .line 57
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getAverageSale()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$2;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-direct {v3, v6}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 58
    invoke-virtual {v1, v8}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$3;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v6

    if-eqz v8, :cond_6

    .line 59
    invoke-static {v8}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_6

    :cond_6
    move-object v7, v5

    .line 56
    :goto_6
    invoke-direct {v12, v3, v6, v7}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 61
    new-instance v13, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    .line 62
    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getNetSales()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$2;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-direct {v3, v6}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 63
    invoke-virtual {v1, v2}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$3;->invoke(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_7

    .line 64
    invoke-static {v2}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->getColor(Lcom/squareup/util/Percentage;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_7

    :cond_7
    move-object v2, v5

    .line 61
    :goto_7
    invoke-direct {v13, v3, v1, v2}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 66
    new-instance v14, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    .line 67
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getRefunds()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$2;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 66
    invoke-direct {v14, v1, v5, v5}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 71
    new-instance v15, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    .line 72
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {v4}, Lcom/squareup/customreport/data/SalesSummary;->getDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesOverviewRows$2;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {v1, v0}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 71
    invoke-direct {v15, v1, v5, v5}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;-><init>(Lcom/squareup/util/ViewString;Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v9, v16

    .line 45
    invoke-direct/range {v9 .. v15}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;-><init>(Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)V

    .line 44
    invoke-static/range {v16 .. v16}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
