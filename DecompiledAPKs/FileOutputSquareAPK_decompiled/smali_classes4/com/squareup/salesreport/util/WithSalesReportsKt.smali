.class public final Lcom/squareup/salesreport/util/WithSalesReportsKt;
.super Ljava/lang/Object;
.source "WithSalesReports.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0005\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0004\u00a8\u0006\u0007"
    }
    d2 = {
        "categoryCount",
        "",
        "Lcom/squareup/customreport/data/WithSalesReport;",
        "getCategoryCount",
        "(Lcom/squareup/customreport/data/WithSalesReport;)I",
        "itemCount",
        "getItemCount",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getCategoryCount(Lcom/squareup/customreport/data/WithSalesReport;)I
    .locals 1

    const-string v0, "$this$categoryCount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p0}, Lcom/squareup/customreport/data/WithSalesReport;->getTopCategoriesReport()Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    move-result-object p0

    .line 17
    instance-of v0, p0, Lcom/squareup/customreport/data/NoSalesTopCategoriesReport;

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 18
    :cond_0
    instance-of v0, p0, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;

    invoke-virtual {p0}, Lcom/squareup/customreport/data/WithSalesTopCategoriesReport;->getTopCategories()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    :goto_0
    return p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final getItemCount(Lcom/squareup/customreport/data/WithSalesReport;)I
    .locals 1

    const-string v0, "$this$itemCount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-virtual {p0}, Lcom/squareup/customreport/data/WithSalesReport;->getTopItemsReport()Lcom/squareup/customreport/data/SalesTopItemsReport;

    move-result-object p0

    .line 11
    instance-of v0, p0, Lcom/squareup/customreport/data/NoSalesTopItemsReport;

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 12
    :cond_0
    instance-of v0, p0, Lcom/squareup/customreport/data/WithSalesTopItemsReport;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/customreport/data/WithSalesTopItemsReport;

    invoke-virtual {p0}, Lcom/squareup/customreport/data/WithSalesTopItemsReport;->getTopItems()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    :goto_0
    return p0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
