.class public final Lcom/squareup/salesreport/util/DashboardUrlLauncher;
.super Ljava/lang/Object;
.source "DashboardUrlLauncher.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u000b\u001a\u00020\u000cJ\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/salesreport/util/DashboardUrlLauncher;",
        "",
        "context",
        "Landroid/content/Context;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Landroid/content/Context;Lcom/squareup/util/Res;)V",
        "getContext",
        "()Landroid/content/Context;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "canOpenUrls",
        "",
        "open",
        "",
        "itemOrCategory",
        "Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/util/Res;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final canOpenUrls()Z
    .locals 2

    .line 37
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->context:Landroid/content/Context;

    return-object v0
.end method

.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public final open(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V
    .locals 4

    const-string v0, "itemOrCategory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->context:Landroid/content/Context;

    .line 23
    new-instance v1, Landroid/content/Intent;

    .line 26
    iget-object v2, p0, Lcom/squareup/salesreport/util/DashboardUrlLauncher;->res:Lcom/squareup/util/Res;

    .line 28
    sget-object v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget p1, Lcom/squareup/salesreport/impl/R$string;->sales_report_dashboard_item_sales_url:I

    goto :goto_0

    .line 29
    :cond_0
    sget-object v3, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lcom/squareup/salesreport/impl/R$string;->sales_report_dashboard_category_sales_url:I

    .line 26
    :goto_0
    invoke-interface {v2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 25
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v2, "android.intent.action.VIEW"

    .line 23
    invoke-direct {v1, v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 22
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    .line 29
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
