.class public final Lcom/squareup/salesreport/util/SalesReportBackButton;
.super Ljava/lang/Object;
.source "SalesReportBackButton.kt"

# interfaces
.implements Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/salesreport/util/SalesReportBackButton;",
        "Lcom/squareup/salesreport/util/SalesReportBackButtonConfig;",
        "()V",
        "alwaysShowBackButton",
        "",
        "getAlwaysShowBackButton",
        "()Z",
        "icon",
        "Lcom/squareup/noho/UpIcon;",
        "getIcon",
        "()Lcom/squareup/noho/UpIcon;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final alwaysShowBackButton:Z

.field private final icon:Lcom/squareup/noho/UpIcon;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    sget-object v0, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    iput-object v0, p0, Lcom/squareup/salesreport/util/SalesReportBackButton;->icon:Lcom/squareup/noho/UpIcon;

    return-void
.end method


# virtual methods
.method public getAlwaysShowBackButton()Z
    .locals 1

    .line 7
    iget-boolean v0, p0, Lcom/squareup/salesreport/util/SalesReportBackButton;->alwaysShowBackButton:Z

    return v0
.end method

.method public getIcon()Lcom/squareup/noho/UpIcon;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportBackButton;->icon:Lcom/squareup/noho/UpIcon;

    return-object v0
.end method
