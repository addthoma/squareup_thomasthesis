.class public final synthetic Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->values()[Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->GROSS_SALES:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->NET_SALES:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->SALES_COUNT:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->values()[Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->GROSS_SALES:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->NET_SALES:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/salesreport/util/SalesSummariesKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->SALES_COUNT:Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
