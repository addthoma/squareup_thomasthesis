.class public final Lcom/squareup/prices/PricingEngineService_Factory;
.super Ljava/lang/Object;
.source "PricingEngineService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/prices/PricingEngineService;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final detailsLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final pricingEngineProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/pricing/engine/PricingEngine;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/pricing/engine/PricingEngine;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineService_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/prices/PricingEngineService_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/prices/PricingEngineService_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/prices/PricingEngineService_Factory;->detailsLoaderProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/prices/PricingEngineService_Factory;->pricingEngineProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p6, p0, Lcom/squareup/prices/PricingEngineService_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/prices/PricingEngineService_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/pricing/engine/PricingEngine;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Lcom/squareup/prices/PricingEngineService_Factory;"
        }
    .end annotation

    .line 50
    new-instance v7, Lcom/squareup/prices/PricingEngineService_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/prices/PricingEngineService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/util/Clock;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;Lcom/squareup/shared/pricing/engine/PricingEngine;Lrx/Scheduler;)Lcom/squareup/prices/PricingEngineService;
    .locals 8

    .line 56
    new-instance v7, Lcom/squareup/prices/PricingEngineService;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/prices/PricingEngineService;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;Lcom/squareup/shared/pricing/engine/PricingEngine;Lrx/Scheduler;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/prices/PricingEngineService;
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService_Factory;->detailsLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;

    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService_Factory;->pricingEngineProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/shared/pricing/engine/PricingEngine;

    iget-object v0, p0, Lcom/squareup/prices/PricingEngineService_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lrx/Scheduler;

    invoke-static/range {v1 .. v6}, Lcom/squareup/prices/PricingEngineService_Factory;->newInstance(Lcom/squareup/util/Clock;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;Lcom/squareup/shared/pricing/engine/clienthelpers/ItemizationDetailsLoader;Lcom/squareup/shared/pricing/engine/PricingEngine;Lrx/Scheduler;)Lcom/squareup/prices/PricingEngineService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/prices/PricingEngineService_Factory;->get()Lcom/squareup/prices/PricingEngineService;

    move-result-object v0

    return-object v0
.end method
