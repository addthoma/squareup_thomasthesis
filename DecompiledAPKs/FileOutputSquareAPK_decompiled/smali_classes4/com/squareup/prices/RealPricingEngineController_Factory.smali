.class public final Lcom/squareup/prices/RealPricingEngineController_Factory;
.super Ljava/lang/Object;
.source "RealPricingEngineController_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/prices/RealPricingEngineController;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final pricingEngineServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/PricingEngineService;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/PricingEngineService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/prices/RealPricingEngineController_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/prices/RealPricingEngineController_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/prices/RealPricingEngineController_Factory;->pricingEngineServiceProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/prices/RealPricingEngineController_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/prices/RealPricingEngineController_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/PricingEngineService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/prices/RealPricingEngineController_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/prices/RealPricingEngineController_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/prices/RealPricingEngineController_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lrx/Scheduler;Lcom/squareup/payment/Transaction;Lcom/squareup/prices/PricingEngineService;Lcom/squareup/settings/server/Features;)Lcom/squareup/prices/RealPricingEngineController;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/prices/RealPricingEngineController;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/prices/RealPricingEngineController;-><init>(Lrx/Scheduler;Lcom/squareup/payment/Transaction;Lcom/squareup/prices/PricingEngineService;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/prices/RealPricingEngineController;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/prices/RealPricingEngineController_Factory;->schedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    iget-object v1, p0, Lcom/squareup/prices/RealPricingEngineController_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v2, p0, Lcom/squareup/prices/RealPricingEngineController_Factory;->pricingEngineServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/prices/PricingEngineService;

    iget-object v3, p0, Lcom/squareup/prices/RealPricingEngineController_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/prices/RealPricingEngineController_Factory;->newInstance(Lrx/Scheduler;Lcom/squareup/payment/Transaction;Lcom/squareup/prices/PricingEngineService;Lcom/squareup/settings/server/Features;)Lcom/squareup/prices/RealPricingEngineController;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/prices/RealPricingEngineController_Factory;->get()Lcom/squareup/prices/RealPricingEngineController;

    move-result-object v0

    return-object v0
.end method
