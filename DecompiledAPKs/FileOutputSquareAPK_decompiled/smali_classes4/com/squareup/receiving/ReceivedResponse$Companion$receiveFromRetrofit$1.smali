.class final Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;
.super Ljava/lang/Object;
.source "ReceivedResponse.kt"

# interfaces
.implements Lio/reactivex/SingleTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/receiving/ReceivedResponse$Companion;->receiveFromRetrofit(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Upstream:",
        "Ljava/lang/Object;",
        "Downstream:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleTransformer<",
        "TT;",
        "Lcom/squareup/receiving/ReceivedResponse<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0005\"\u0006\u0008\u0001\u0010\u0003 \u00012\u0014\u0010\u0006\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u0001H\u0003H\u00030\u0001H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "T",
        "kotlin.jvm.PlatformType",
        "",
        "single",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $annotations:[Ljava/lang/annotation/Annotation;

.field final synthetic $isSuccessful:Lkotlin/jvm/functions/Function1;

.field final synthetic $retrofit:Lretrofit2/Retrofit;

.field final synthetic $type:Ljava/lang/reflect/Type;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->$isSuccessful:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->$retrofit:Lretrofit2/Retrofit;

    iput-object p3, p0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->$type:Ljava/lang/reflect/Type;

    iput-object p4, p0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->$annotations:[Ljava/lang/annotation/Annotation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Single;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Single<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "single"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$1;-><init>(Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 132
    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$2;-><init>(Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->onErrorResumeNext(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Lio/reactivex/Single;)Lio/reactivex/SingleSource;
    .locals 0

    .line 113
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->apply(Lio/reactivex/Single;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    return-object p1
.end method
