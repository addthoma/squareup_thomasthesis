.class public final Lcom/squareup/receiving/FailureMessageFactory;
.super Ljava/lang/Object;
.source "FailureMessageFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFailureMessageFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FailureMessageFactory.kt\ncom/squareup/receiving/FailureMessageFactory\n*L\n1#1,66:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004JQ\u0010\u0005\u001a\u00020\u0006\"\u0004\u0008\u0000\u0010\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00070\t2\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b2+\u0010\u000c\u001a\'\u0012\u0013\u0012\u0011H\u0007\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0004\u0012\u00020\u00110\rj\u0008\u0012\u0004\u0012\u0002H\u0007`\u0012JX\u0010\u0013\u001a\u00020\u0006\"\u0004\u0008\u0000\u0010\u00072\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0010\u001a\u0002H\u00072\u0006\u0010\u0016\u001a\u00020\u00172+\u0010\u000c\u001a\'\u0012\u0013\u0012\u0011H\u0007\u00a2\u0006\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010\u0012\u0004\u0012\u00020\u00110\rj\u0008\u0012\u0004\u0012\u0002H\u0007`\u0012H\u0002\u00a2\u0006\u0002\u0010\u0018R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "get",
        "Lcom/squareup/receiving/FailureMessage;",
        "T",
        "failure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "defaultTitle",
        "",
        "parser",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "response",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        "Lcom/squareup/receiving/ResponseToParts;",
        "messageFromResponse",
        "retryable",
        "",
        "messages",
        "Lcom/squareup/request/RequestMessages;",
        "(ZLjava/lang/Object;Lcom/squareup/request/RequestMessages;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/receiving/FailureMessageFactory;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private final messageFromResponse(ZLjava/lang/Object;Lcom/squareup/request/RequestMessages;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(ZTT;",
            "Lcom/squareup/request/RequestMessages;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lcom/squareup/receiving/FailureMessage$Parts;",
            ">;)",
            "Lcom/squareup/receiving/FailureMessage;"
        }
    .end annotation

    .line 57
    invoke-interface {p4, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/receiving/FailureMessage$Parts;

    .line 58
    new-instance p4, Lcom/squareup/receiving/FailureMessage;

    .line 59
    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage$Parts;->getTitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lcom/squareup/request/RequestMessages;->getDefaultFailureTitle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "messages.defaultFailureTitle"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage$Parts;->getBody()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p3}, Lcom/squareup/request/RequestMessages;->getServerErrorMessage()Ljava/lang/String;

    move-result-object p2

    const-string p3, "messages.serverErrorMessage"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    :goto_1
    invoke-direct {p4, v0, p2, p1}, Lcom/squareup/receiving/FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object p4
.end method


# virtual methods
.method public final get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;I",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lcom/squareup/receiving/FailureMessage$Parts;",
            ">;)",
            "Lcom/squareup/receiving/FailureMessage;"
        }
    .end annotation

    const-string v0, "failure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parser"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lcom/squareup/request/RequestMessages;

    iget-object v1, p0, Lcom/squareup/receiving/FailureMessageFactory;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v1, p2}, Lcom/squareup/request/RequestMessages;-><init>(Lcom/squareup/util/Res;I)V

    .line 28
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p2

    .line 30
    instance-of v1, p2, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-nez v1, :cond_5

    .line 31
    instance-of v1, p2, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-nez v1, :cond_5

    .line 33
    instance-of v1, p2, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result p1

    check-cast p2, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p2

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/receiving/FailureMessageFactory;->messageFromResponse(ZLjava/lang/Object;Lcom/squareup/request/RequestMessages;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    move-object p2, p1

    goto/16 :goto_0

    .line 35
    :cond_0
    sget-object v1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance p2, Lcom/squareup/receiving/FailureMessage;

    .line 36
    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getNetworkErrorTitle()Ljava/lang/String;

    move-result-object p3

    const-string v1, "messages.networkErrorTitle"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getNetworkErrorMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "messages.networkErrorMessage"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result p1

    .line 35
    invoke-direct {p2, p3, v0, p1}, Lcom/squareup/receiving/FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 39
    :cond_1
    instance-of v1, p2, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    const-string v2, "messages.serverErrorMessage"

    if-eqz v1, :cond_2

    new-instance p2, Lcom/squareup/receiving/FailureMessage;

    .line 40
    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getServerErrorTitle()Ljava/lang/String;

    move-result-object p3

    const-string v1, "messages.serverErrorTitle"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getServerErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result p1

    .line 39
    invoke-direct {p2, p3, v0, p1}, Lcom/squareup/receiving/FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 43
    :cond_2
    instance-of v1, p2, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v1, :cond_4

    .line 44
    check-cast p2, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {p2}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result v1

    invoke-direct {p0, v1, p2, v0, p3}, Lcom/squareup/receiving/FailureMessageFactory;->messageFromResponse(ZLjava/lang/Object;Lcom/squareup/request/RequestMessages;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p2

    if-eqz p2, :cond_3

    goto :goto_0

    .line 45
    :cond_3
    new-instance p2, Lcom/squareup/receiving/FailureMessage;

    .line 46
    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getDefaultFailureTitle()Ljava/lang/String;

    move-result-object p3

    const-string v1, "messages.defaultFailureTitle"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/request/RequestMessages;->getServerErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result p1

    .line 45
    invoke-direct {p2, p3, v0, p1}, Lcom/squareup/receiving/FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_0
    return-object p2

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 31
    :cond_5
    new-instance p1, Ljava/lang/AssertionError;

    const-string p2, "ShowFailure cannot wrap Accepted or SessionExpired"

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
