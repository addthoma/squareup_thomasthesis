.class final Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$2;
.super Ljava/lang/Object;
.source "ReceivedResponse.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->apply(Lio/reactivex/Single;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Ljava/lang/Throwable;",
        "Lio/reactivex/SingleSource<",
        "+",
        "Lcom/squareup/receiving/ReceivedResponse<",
        "+TT;>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001aB\u0012\u001a\u0008\u0001\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u0002 \u0004* \u0012\u001a\u0008\u0001\u0012\u0016\u0012\u0004\u0012\u0002H\u0003 \u0004*\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0005\"\u0006\u0008\u0001\u0010\u0003 \u00012\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/SingleSource;",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "T",
        "kotlin.jvm.PlatformType",
        "",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;


# direct methods
.method constructor <init>(Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$2;->this$0:Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Throwable;)Lio/reactivex/SingleSource;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/SingleSource<",
            "+",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    instance-of v0, p1, Lretrofit2/HttpException;

    if-eqz v0, :cond_0

    .line 137
    :try_start_0
    sget-object v0, Lcom/squareup/receiving/ReceivedResponse;->Companion:Lcom/squareup/receiving/ReceivedResponse$Companion;

    check-cast p1, Lretrofit2/HttpException;

    iget-object v1, p0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$2;->this$0:Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;

    iget-object v1, v1, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->$retrofit:Lretrofit2/Retrofit;

    iget-object v2, p0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$2;->this$0:Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;

    iget-object v2, v2, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->$type:Ljava/lang/reflect/Type;

    iget-object v3, p0, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$2;->this$0:Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;

    iget-object v3, v3, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1;->$annotations:[Ljava/lang/annotation/Annotation;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/receiving/ReceivedResponse$Companion;->access$fromHttpException(Lcom/squareup/receiving/ReceivedResponse$Companion;Lretrofit2/HttpException;Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lcom/squareup/receiving/ReceivedResponse$Error;

    move-result-object p1

    .line 136
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 142
    check-cast p1, Ljava/lang/Throwable;

    invoke-static {p1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object p1

    .line 135
    :goto_0
    check-cast p1, Lio/reactivex/SingleSource;

    goto :goto_1

    .line 145
    :cond_0
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    goto :goto_1

    .line 146
    :cond_1
    new-instance v0, Lio/reactivex/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p1}, Lio/reactivex/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    invoke-static {v0}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    :goto_1
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 113
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/receiving/ReceivedResponse$Companion$receiveFromRetrofit$1$2;->apply(Ljava/lang/Throwable;)Lio/reactivex/SingleSource;

    move-result-object p1

    return-object p1
.end method
