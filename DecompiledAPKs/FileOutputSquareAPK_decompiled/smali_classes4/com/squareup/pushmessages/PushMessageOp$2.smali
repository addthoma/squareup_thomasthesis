.class final enum Lcom/squareup/pushmessages/PushMessageOp$2;
.super Lcom/squareup/pushmessages/PushMessageOp;
.source "PushMessageOp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pushmessages/PushMessageOp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/pushmessages/PushMessageOp;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/pushmessages/PushMessageOp$1;)V

    return-void
.end method


# virtual methods
.method createMessage(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessage;"
        }
    .end annotation

    .line 41
    invoke-static {p1}, Lcom/squareup/pushmessages/PushMessageOp;->access$100(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessageOp$Alert;

    move-result-object v0

    const-string v1, "url"

    .line 42
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    .line 47
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 48
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    const-string v2, "appointments"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    new-instance v1, Lcom/squareup/pushmessages/PushMessage$AppointmentsPushNotification;

    iget-object v2, v0, Lcom/squareup/pushmessages/PushMessageOp$Alert;->title:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/pushmessages/PushMessageOp$Alert;->body:Ljava/lang/String;

    invoke-direct {v1, v2, v0, p1}, Lcom/squareup/pushmessages/PushMessage$AppointmentsPushNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 52
    :cond_1
    new-instance v1, Lcom/squareup/pushmessages/PushMessage$PushNotification;

    iget-object v2, v0, Lcom/squareup/pushmessages/PushMessageOp$Alert;->title:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/pushmessages/PushMessageOp$Alert;->body:Ljava/lang/String;

    invoke-direct {v1, v2, v0, p1}, Lcom/squareup/pushmessages/PushMessage$PushNotification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method
