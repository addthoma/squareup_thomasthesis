.class final enum Lcom/squareup/pushmessages/PushMessageOp$12;
.super Lcom/squareup/pushmessages/PushMessageOp;
.source "PushMessageOp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pushmessages/PushMessageOp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 111
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/pushmessages/PushMessageOp;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/pushmessages/PushMessageOp$1;)V

    return-void
.end method


# virtual methods
.method createMessage(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessage;"
        }
    .end annotation

    const-string/jumbo v0, "version"

    .line 113
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-nez p1, :cond_0

    const-string p1, "0"

    .line 117
    :cond_0
    new-instance v0, Lcom/squareup/pushmessages/PushMessage$InventoryAvailabilityUpdated;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/squareup/pushmessages/PushMessage$InventoryAvailabilityUpdated;-><init>(J)V

    return-object v0
.end method
