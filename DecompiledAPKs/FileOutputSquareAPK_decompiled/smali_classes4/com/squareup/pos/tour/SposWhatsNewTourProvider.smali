.class public final Lcom/squareup/pos/tour/SposWhatsNewTourProvider;
.super Ljava/lang/Object;
.source "SposWhatsNewTourProvider.kt"

# interfaces
.implements Lcom/squareup/tour/WhatsNewTourProvider;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSposWhatsNewTourProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SposWhatsNewTourProvider.kt\ncom/squareup/pos/tour/SposWhatsNewTourProvider\n*L\n1#1,37:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/pos/tour/SposWhatsNewTourProvider;",
        "Lcom/squareup/tour/WhatsNewTourProvider;",
        "locale",
        "Ljava/util/Locale;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Ljava/util/Locale;Lcom/squareup/settings/server/Features;)V",
        "displayAfterOnboarding",
        "",
        "getDisplayAfterOnboarding",
        "()Z",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "getLocale",
        "()Ljava/util/Locale;",
        "tourPages",
        "",
        "Lcom/squareup/tour/SerializableHasTourPages;",
        "getTourPages",
        "()Ljava/util/List;",
        "tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayAfterOnboarding:Z

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;->locale:Ljava/util/Locale;

    iput-object p2, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public getDisplayAfterOnboarding()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;->displayAfterOnboarding:Z

    return v0
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;->features:Lcom/squareup/settings/server/Features;

    return-object v0
.end method

.method public final getLocale()Ljava/util/Locale;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getTourPages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/SerializableHasTourPages;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 27
    iget-object v1, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    iget-object v1, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->SALES_REPORT_V2_FEATURE_CAROUSEL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    .line 31
    new-instance v2, Lcom/squareup/tour/SerializableHasTourPages;

    sget-object v3, Lcom/squareup/pos/tour/SposWhatsNewTour;->SPOS_WHATS_NEW_SALES_REPORTS:Lcom/squareup/pos/tour/SposWhatsNewTour;

    invoke-virtual {v3}, Lcom/squareup/pos/tour/SposWhatsNewTour;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/squareup/pos/tour/SposWhatsNewTour;->SPOS_WHATS_NEW_SALES_REPORTS:Lcom/squareup/pos/tour/SposWhatsNewTour;

    check-cast v4, Lcom/squareup/tour/Tour$HasTourPages;

    invoke-direct {v2, v3, v4}, Lcom/squareup/tour/SerializableHasTourPages;-><init>(Ljava/lang/String;Lcom/squareup/tour/Tour$HasTourPages;)V

    .line 30
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method
