.class public abstract enum Lcom/squareup/pos/tour/SposWhatsNewTour;
.super Ljava/lang/Enum;
.source "SposWhatsNewTour.kt"

# interfaces
.implements Lcom/squareup/tour/Tour$HasTourPages;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pos/tour/SposWhatsNewTour$SPOS_WHATS_NEW_SALES_REPORTS;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/pos/tour/SposWhatsNewTour;",
        ">;",
        "Lcom/squareup/tour/Tour$HasTourPages;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003j\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/pos/tour/SposWhatsNewTour;",
        "",
        "Lcom/squareup/tour/Tour$HasTourPages;",
        "(Ljava/lang/String;I)V",
        "SPOS_WHATS_NEW_SALES_REPORTS",
        "tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/pos/tour/SposWhatsNewTour;

.field public static final enum SPOS_WHATS_NEW_SALES_REPORTS:Lcom/squareup/pos/tour/SposWhatsNewTour;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/pos/tour/SposWhatsNewTour;

    new-instance v1, Lcom/squareup/pos/tour/SposWhatsNewTour$SPOS_WHATS_NEW_SALES_REPORTS;

    const/4 v2, 0x0

    const-string v3, "SPOS_WHATS_NEW_SALES_REPORTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/pos/tour/SposWhatsNewTour$SPOS_WHATS_NEW_SALES_REPORTS;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/pos/tour/SposWhatsNewTour;->SPOS_WHATS_NEW_SALES_REPORTS:Lcom/squareup/pos/tour/SposWhatsNewTour;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/pos/tour/SposWhatsNewTour;->$VALUES:[Lcom/squareup/pos/tour/SposWhatsNewTour;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/squareup/pos/tour/SposWhatsNewTour;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/pos/tour/SposWhatsNewTour;
    .locals 1

    const-class v0, Lcom/squareup/pos/tour/SposWhatsNewTour;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/pos/tour/SposWhatsNewTour;

    return-object p0
.end method

.method public static values()[Lcom/squareup/pos/tour/SposWhatsNewTour;
    .locals 1

    sget-object v0, Lcom/squareup/pos/tour/SposWhatsNewTour;->$VALUES:[Lcom/squareup/pos/tour/SposWhatsNewTour;

    invoke-virtual {v0}, [Lcom/squareup/pos/tour/SposWhatsNewTour;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/pos/tour/SposWhatsNewTour;

    return-object v0
.end method
