.class public final Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;
.super Ljava/lang/Object;
.source "PosHelpAppletSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/pos/help/PosHelpAppletSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final aboutSectionListEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/about/AboutSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustPointsLoyaltyTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final announcementsSectionListEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final createItemTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final enrollLoyaltyCustomerTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final feeTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final firstInvoiceTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final firstPaymentTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final helpSectionListEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final legalSectionListEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final ordersSectionListEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final r12TutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/R12Tutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final r6TutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/R6Tutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemRewardsTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final referralsSectionListEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final registerVersionNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final troubleshootingSectionEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialsSectionEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final whatsNewTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/about/AboutSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/R12Tutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/R6Tutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 94
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->helpSectionListEntryProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 95
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->tutorialsSectionEntryProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 96
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->ordersSectionListEntryProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 97
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->announcementsSectionListEntryProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 98
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->troubleshootingSectionEntryProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 99
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->aboutSectionListEntryProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 100
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->legalSectionListEntryProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 101
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->referralsSectionListEntryProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 102
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->whatsNewTutorialProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 103
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->firstPaymentTutorialProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 104
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->r12TutorialProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 105
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->r6TutorialProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 106
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->firstInvoiceTutorialProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 107
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->createItemTutorialProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 108
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->enrollLoyaltyCustomerTutorialProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 109
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->adjustPointsLoyaltyTutorialProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 110
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->redeemRewardsTutorialProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 111
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 112
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->quickAmountsTutorialProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 113
    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->registerVersionNameProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/help/HelpSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/about/AboutSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/R12Tutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/R6Tutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    .line 141
    new-instance v21, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;

    move-object/from16 v0, v21

    invoke-direct/range {v0 .. v20}, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v21
.end method

.method public static newInstance(Lcom/squareup/ui/help/help/HelpSection$ListEntry;Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;Lcom/squareup/ui/help/about/AboutSection$ListEntry;Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;Lcom/squareup/ui/help/tutorials/content/R12Tutorial;Lcom/squareup/ui/help/tutorials/content/R6Tutorial;Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;Ljava/lang/String;)Lcom/squareup/pos/help/PosHelpAppletSettings;
    .locals 22

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    .line 157
    new-instance v21, Lcom/squareup/pos/help/PosHelpAppletSettings;

    move-object/from16 v0, v21

    invoke-direct/range {v0 .. v20}, Lcom/squareup/pos/help/PosHelpAppletSettings;-><init>(Lcom/squareup/ui/help/help/HelpSection$ListEntry;Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;Lcom/squareup/ui/help/about/AboutSection$ListEntry;Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;Lcom/squareup/ui/help/tutorials/content/R12Tutorial;Lcom/squareup/ui/help/tutorials/content/R6Tutorial;Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;Ljava/lang/String;)V

    return-object v21
.end method


# virtual methods
.method public get()Lcom/squareup/pos/help/PosHelpAppletSettings;
    .locals 22

    move-object/from16 v0, p0

    .line 118
    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->helpSectionListEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/help/help/HelpSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->tutorialsSectionEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->ordersSectionListEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->announcementsSectionListEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->troubleshootingSectionEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->aboutSectionListEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/help/about/AboutSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->legalSectionListEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->referralsSectionListEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->whatsNewTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->firstPaymentTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->r12TutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/help/tutorials/content/R12Tutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->r6TutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/help/tutorials/content/R6Tutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->firstInvoiceTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->createItemTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->enrollLoyaltyCustomerTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->adjustPointsLoyaltyTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->redeemRewardsTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->quickAmountsTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;

    iget-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->registerVersionNameProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Ljava/lang/String;

    invoke-static/range {v2 .. v21}, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->newInstance(Lcom/squareup/ui/help/help/HelpSection$ListEntry;Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;Lcom/squareup/ui/help/about/AboutSection$ListEntry;Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;Lcom/squareup/ui/help/tutorials/content/R12Tutorial;Lcom/squareup/ui/help/tutorials/content/R6Tutorial;Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;Ljava/lang/String;)Lcom/squareup/pos/help/PosHelpAppletSettings;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/pos/help/PosHelpAppletSettings_Factory;->get()Lcom/squareup/pos/help/PosHelpAppletSettings;

    move-result-object v0

    return-object v0
.end method
