.class public abstract Lcom/squareup/pos/help/SposJumbotronModule;
.super Ljava/lang/Object;
.source "SposJumbotronModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideJumbotronServiceKey()Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 11
    sget-object v0, Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;->INSTANCE:Lcom/squareup/ui/help/jumbotron/PosJumbotronServiceKey;

    return-object v0
.end method
