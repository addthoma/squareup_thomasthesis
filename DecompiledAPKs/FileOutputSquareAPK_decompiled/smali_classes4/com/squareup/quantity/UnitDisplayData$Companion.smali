.class public final Lcom/squareup/quantity/UnitDisplayData$Companion;
.super Ljava/lang/Object;
.source "UnitDisplayData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quantity/UnitDisplayData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnitDisplayData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnitDisplayData.kt\ncom/squareup/quantity/UnitDisplayData$Companion\n*L\n1#1,123:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u001a\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0007J\u0018\u0010\n\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u000cH\u0007J\u001a\u0010\r\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0007J \u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0007\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/quantity/UnitDisplayData$Companion;",
        "",
        "()V",
        "empty",
        "Lcom/squareup/quantity/UnitDisplayData;",
        "fromCatalogMeasurementUnit",
        "res",
        "Lcom/squareup/util/Res;",
        "catalogMeasurementUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
        "fromItemization",
        "itemization",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "fromQuantityUnit",
        "quantityUnit",
        "Lcom/squareup/orders/model/Order$QuantityUnit;",
        "of",
        "unitName",
        "",
        "unitAbbreviation",
        "quantityPrecision",
        "",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/quantity/UnitDisplayData$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final empty()Lcom/squareup/quantity/UnitDisplayData;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/quantity/UnitDisplayData;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {v0, v1, v1, v2}, Lcom/squareup/quantity/UnitDisplayData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public final fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 84
    new-instance v0, Lcom/squareup/quantity/UnitDisplayData;

    .line 85
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const-string v2, "it.measurementUnit"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    .line 87
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getPrecision()I

    move-result p2

    .line 84
    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/quantity/UnitDisplayData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 89
    :cond_0
    move-object p1, p0

    check-cast p1, Lcom/squareup/quantity/UnitDisplayData$Companion;

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData$Companion;->empty()Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final fromItemization(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemization"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    move-object v0, p0

    check-cast v0, Lcom/squareup/quantity/UnitDisplayData$Companion;

    .line 114
    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 119
    :cond_0
    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz p2, :cond_1

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz p2, :cond_1

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    if-eqz p2, :cond_1

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    if-eqz p2, :cond_1

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 113
    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromQuantityUnit(Lcom/squareup/util/Res;Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p1

    return-object p1
.end method

.method public final fromQuantityUnit(Lcom/squareup/util/Res;Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/quantity/UnitDisplayData;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    .line 99
    new-instance v0, Lcom/squareup/quantity/UnitDisplayData;

    .line 100
    iget-object v1, p2, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const-string v2, "it.measurement_unit"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    .line 101
    iget-object v3, p2, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, p1}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    .line 102
    iget-object p2, p2, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 99
    :goto_0
    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/quantity/UnitDisplayData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 104
    :cond_1
    move-object p1, p0

    check-cast p1, Lcom/squareup/quantity/UnitDisplayData$Companion;

    invoke-virtual {p1}, Lcom/squareup/quantity/UnitDisplayData$Companion;->empty()Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public final of(Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/quantity/UnitDisplayData;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "unitName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitAbbreviation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/squareup/quantity/UnitDisplayData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/quantity/UnitDisplayData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
