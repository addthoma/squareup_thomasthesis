.class public final Lcom/squareup/quantity/SharedCalculationsKt;
.super Ljava/lang/Object;
.source "SharedCalculations.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a*\u0010\u0000\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u001a\u0016\u0010\u000c\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0007\u001a\u001e\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u001e\u0010\u0013\u001a\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u00012\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u0016\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0007\u00a8\u0006\u0016"
    }
    d2 = {
        "itemBaseAmount",
        "",
        "item",
        "Lcom/squareup/calc/order/Item;",
        "Lcom/squareup/protos/common/Money;",
        "itemPrice",
        "itemQuantity",
        "Ljava/math/BigDecimal;",
        "appliedModifiers",
        "",
        "",
        "Lcom/squareup/calc/order/Modifier;",
        "itemVariationPriceTimesQuantityMoney",
        "itemVariationPrice",
        "itemVariationQuantity",
        "modifierOptionTimesQuantityMoney",
        "modifierPrice",
        "modifierQuantity",
        "",
        "modifierOptionTimesQuantityMoneyAsLong",
        "modifierQuantityTimesItemizationQuantity",
        "itemizationQuantity",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final itemBaseAmount(Lcom/squareup/calc/order/Item;)J
    .locals 2

    const-string v0, "item"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-static {p0}, Lcom/squareup/calc/util/CalculationHelper;->itemBaseAmount(Lcom/squareup/calc/order/Item;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final itemBaseAmount(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/util/Map;)Lcom/squareup/protos/common/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/math/BigDecimal;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/calc/order/Modifier;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    const-string v0, "itemPrice"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemQuantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appliedModifiers"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v1, "itemPrice.amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lcom/squareup/calc/util/CalculationHelper;->itemBaseAmount(JLjava/math/BigDecimal;Ljava/util/Map;)J

    move-result-wide p1

    .line 110
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v0, "itemPrice.currency_code"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-static {p1, p2, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final itemVariationPriceTimesQuantityMoney(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "itemVariationPrice"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemVariationQuantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v1, "itemVariationPrice.amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/calc/util/CalculationHelper;->itemPriceTimesItemQuantity(JLjava/math/BigDecimal;)J

    move-result-wide v0

    .line 81
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string p1, "itemVariationPrice.currency_code"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final modifierOptionTimesQuantityMoney(Lcom/squareup/protos/common/Money;ILjava/math/BigDecimal;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "modifierPrice"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemQuantity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v1, "modifierPrice.amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lcom/squareup/calc/util/CalculationHelper;->totalModifierPrice(JILjava/math/BigDecimal;)J

    move-result-wide p1

    .line 51
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v0, "modifierPrice.currency_code"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {p1, p2, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method public static final modifierOptionTimesQuantityMoneyAsLong(JILjava/math/BigDecimal;)J
    .locals 1

    const-string v0, "itemQuantity"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/calc/util/CalculationHelper;->totalModifierPrice(JILjava/math/BigDecimal;)J

    move-result-wide p0

    return-wide p0
.end method

.method public static final modifierQuantityTimesItemizationQuantity(ILjava/math/BigDecimal;)Ljava/lang/String;
    .locals 1

    const-string v0, "itemizationQuantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {p0, p1}, Lcom/squareup/calc/util/CalculationHelper;->modifierQuantityTimesItemQuantity(ILjava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p0

    .line 35
    invoke-virtual {p0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "CalculationHelper.modifi\u2026uantity\n).toPlainString()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
