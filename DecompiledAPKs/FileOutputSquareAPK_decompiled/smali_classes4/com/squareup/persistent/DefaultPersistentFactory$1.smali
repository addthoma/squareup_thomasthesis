.class Lcom/squareup/persistent/DefaultPersistentFactory$1;
.super Lcom/squareup/persistent/PersistentFile;
.source "DefaultPersistentFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/persistent/DefaultPersistentFactory;->getJsonFile(Ljava/io/File;Ljava/lang/reflect/Type;)Lcom/squareup/persistent/Persistent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/persistent/PersistentFile<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/persistent/DefaultPersistentFactory;

.field final synthetic val$type:Ljava/lang/reflect/Type;


# direct methods
.method constructor <init>(Lcom/squareup/persistent/DefaultPersistentFactory;Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/lang/reflect/Type;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/persistent/DefaultPersistentFactory$1;->this$0:Lcom/squareup/persistent/DefaultPersistentFactory;

    iput-object p5, p0, Lcom/squareup/persistent/DefaultPersistentFactory$1;->val$type:Ljava/lang/reflect/Type;

    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/persistent/PersistentFile;-><init>(Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 48
    new-instance v0, Ljava/io/InputStreamReader;

    sget-object v1, Lcom/squareup/util/Strings;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 49
    iget-object p1, p0, Lcom/squareup/persistent/DefaultPersistentFactory$1;->this$0:Lcom/squareup/persistent/DefaultPersistentFactory;

    invoke-static {p1}, Lcom/squareup/persistent/DefaultPersistentFactory;->access$000(Lcom/squareup/persistent/DefaultPersistentFactory;)Lcom/google/gson/Gson;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/persistent/DefaultPersistentFactory$1;->val$type:Ljava/lang/reflect/Type;

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/persistent/DefaultPersistentFactory$1;->this$0:Lcom/squareup/persistent/DefaultPersistentFactory;

    invoke-static {v0}, Lcom/squareup/persistent/DefaultPersistentFactory;->access$000(Lcom/squareup/persistent/DefaultPersistentFactory;)Lcom/google/gson/Gson;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 54
    invoke-static {p1}, Lcom/squareup/util/Strings;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method
