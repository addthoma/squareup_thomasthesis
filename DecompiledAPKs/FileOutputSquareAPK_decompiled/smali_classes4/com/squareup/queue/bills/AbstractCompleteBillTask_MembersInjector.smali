.class public final Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;
.super Ljava/lang/Object;
.source "AbstractCompleteBillTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/bills/AbstractCompleteBillTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final danglingAuthProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->serviceProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->danglingAuthProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/bills/AbstractCompleteBillTask;",
            ">;"
        }
    .end annotation

    .line 54
    new-instance v7, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static injectDanglingAuth(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V
    .locals 0
    .annotation runtime Lcom/squareup/payment/DanglingPayment;
    .end annotation

    .line 81
    iput-object p1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    return-void
.end method

.method public static injectService(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/server/bills/BillCreationService;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->service:Lcom/squareup/server/bills/BillCreationService;

    return-void
.end method

.method public static injectTransactionLedgerManager(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/bills/AbstractCompleteBillTask;)V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/BillTask_MembersInjector;->injectTickets(Lcom/squareup/queue/bills/BillTask;Lcom/squareup/tickets/Tickets;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/BillCreationService;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->injectService(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/server/bills/BillCreationService;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->injectTransactionLedgerManager(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->danglingAuthProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->injectDanglingAuth(Lcom/squareup/queue/bills/AbstractCompleteBillTask;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/queue/bills/AbstractCompleteBillTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/AbstractCompleteBillTask_MembersInjector;->injectMembers(Lcom/squareup/queue/bills/AbstractCompleteBillTask;)V

    return-void
.end method
