.class public final Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;
.super Ljava/lang/Object;
.source "RemoveTendersTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/bills/RemoveTendersTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final removeTenderServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenders/RemoveTenderService;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenders/RemoveTenderService;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->removeTenderServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenders/RemoveTenderService;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/bills/RemoveTendersTask;",
            ">;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectRemoveTenderService(Lcom/squareup/queue/bills/RemoveTendersTask;Lcom/squareup/tenders/RemoveTenderService;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->removeTenderService:Lcom/squareup/tenders/RemoveTenderService;

    return-void
.end method

.method public static injectTransactionLedgerManager(Lcom/squareup/queue/bills/RemoveTendersTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/queue/bills/RemoveTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/bills/RemoveTendersTask;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->injectTransactionLedgerManager(Lcom/squareup/queue/bills/RemoveTendersTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->removeTenderServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenders/RemoveTenderService;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->injectRemoveTenderService(Lcom/squareup/queue/bills/RemoveTendersTask;Lcom/squareup/tenders/RemoveTenderService;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/queue/bills/RemoveTendersTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/RemoveTendersTask_MembersInjector;->injectMembers(Lcom/squareup/queue/bills/RemoveTendersTask;)V

    return-void
.end method
