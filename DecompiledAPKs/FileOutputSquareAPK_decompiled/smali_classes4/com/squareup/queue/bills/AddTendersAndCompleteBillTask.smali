.class public Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;
.super Lcom/squareup/queue/bills/AbstractCompleteBillTask;
.source "AddTendersAndCompleteBillTask.java"

# interfaces
.implements Lcom/squareup/queue/LocalPaymentsQueueTask;


# instance fields
.field public final addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

.field transient addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

.field private addedToTheLocalPaymentsQueue:Z

.field transient billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient localTenderCache:Lcom/squareup/print/LocalTenderCache;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private onTheTaskQueue:Z

.field transient settings:Lcom/squareup/settings/server/AccountStatusSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/AddTendersRequest;Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    .line 61
    invoke-direct/range {v0 .. v7}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;-><init>(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    .line 63
    invoke-static {p1}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->sanityCheckAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V

    move-object v1, p1

    .line 64
    iput-object v1, v0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    return-void
.end method

.method private cleanBillAndTenderServerId()V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private getCachedBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 188
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/settings/AddTendersRequestServerIds;->getBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private getCachedTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 193
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/settings/AddTendersRequestServerIds;->getTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private getTenderClientId()Ljava/lang/String;
    .locals 2

    .line 207
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    return-object v0
.end method

.method private static sanityCheckAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V
    .locals 5

    .line 72
    iget-object v0, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 73
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v2, 0x0

    .line 76
    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 77
    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 78
    iget-object p0, p0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/AddTender;

    .line 79
    iget-object v3, v2, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {v4, v3}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 80
    iget-object v3, v2, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v3, :cond_0

    .line 81
    iget-object v2, v2, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    goto :goto_0

    .line 85
    :cond_1
    invoke-static {v0, v4, v1}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->sanityCheck(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method private saveServerIds(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    .line 197
    new-instance v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    invoke-direct {v0, p1, p2}, Lcom/squareup/settings/AddTendersRequestServerIds;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    .line 199
    iget-object p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/protos/client/bills/CompleteBillResponse;
    .locals 7

    .line 89
    invoke-virtual {p0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->getCachedBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 90
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->getTenderClientId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->getCachedTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 96
    iget-boolean v2, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->onTheTaskQueue:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addedToTheLocalPaymentsQueue:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 97
    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteBillResponse;

    new-instance v1, Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "Success"

    invoke-direct {v1, v4, v4, v2, v3}, Lcom/squareup/protos/client/Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/resources/Error;)V

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/bills/CompleteBillResponse;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0

    :cond_0
    const/4 v2, 0x0

    if-nez v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->service:Lcom/squareup/server/bills/BillCreationService;

    iget-object v1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    iget-object v3, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->clientId:Ljava/lang/String;

    invoke-static {v3}, Lcom/squareup/server/bills/ApiClientId;->clientIdOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/squareup/server/bills/BillCreationService;->addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    .line 108
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    invoke-interface {v0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 117
    iget-object v1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 121
    iget-object v3, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    iget-object v4, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 125
    invoke-direct {p0, v0, v1}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->saveServerIds(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    .line 129
    iget-object v3, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    iget-object v4, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    .line 130
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    iget-object v6, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 129
    invoke-virtual {v3, v4, v5, v6}, Lcom/squareup/print/LocalTenderCache;->setLast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddTenders call failed for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 113
    throw v0

    .line 134
    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->newBuilder()Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v3

    .line 135
    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    .line 136
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/CompleteTender;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CompleteTender;->newBuilder()Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v2

    .line 137
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v1

    .line 138
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object v1

    .line 136
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 139
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseBillAmendments()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->is_amendable(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->service:Lcom/squareup/server/bills/BillCreationService;

    iget-object v2, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->clientId:Ljava/lang/String;

    .line 143
    invoke-static {v2}, Lcom/squareup/server/bills/ApiClientId;->clientIdOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/squareup/server/bills/BillCreationService;->completeBill(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteBillResponse;

    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCompleteBillResponse(Lcom/squareup/protos/client/bills/CompleteBillResponse;)V

    .line 146
    iget-object v1, v0, Lcom/squareup/protos/client/bills/CompleteBillResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 153
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->cleanBillAndTenderServerId()V

    return-object v0

    .line 147
    :cond_3
    new-instance v1, Ljava/lang/RuntimeException;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CompleteBill call failed for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 149
    throw v1
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->callOnRpcThread()Lcom/squareup/protos/client/bills/CompleteBillResponse;

    move-result-object v0

    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 173
    invoke-virtual {p0}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->getClientId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CompleteBillResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 4

    .line 163
    invoke-super {p0, p1}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CompleteBillResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    .line 165
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 166
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    iget-object v1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v2, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/payment/BillPaymentEvents;->notifyAddTenderResponseReceived(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/IdPair;)V

    :cond_0
    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/protos/client/bills/CompleteBillResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CompleteBillResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 211
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V

    .line 178
    invoke-super {p0, p1}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAddedToTheLocalPaymentsQueue(Z)V
    .locals 0

    .line 219
    iput-boolean p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->addedToTheLocalPaymentsQueue:Z

    return-void
.end method

.method public setOnTheTaskQueue(Z)V
    .locals 0

    .line 215
    iput-boolean p1, p0, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;->onTheTaskQueue:Z

    return-void
.end method
