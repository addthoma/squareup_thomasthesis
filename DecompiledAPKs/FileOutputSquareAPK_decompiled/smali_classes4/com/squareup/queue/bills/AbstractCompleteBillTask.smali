.class public abstract Lcom/squareup/queue/bills/AbstractCompleteBillTask;
.super Lcom/squareup/queue/bills/BillTask;
.source "AbstractCompleteBillTask.java"

# interfaces
.implements Lcom/squareup/queue/EnqueueableTask;


# instance fields
.field public final adjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final clientId:Ljava/lang/String;

.field private transient completeTenders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation
.end field

.field transient danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
    .annotation runtime Lcom/squareup/payment/DanglingPayment;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final itemizations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final paymentConfigAmountPaidOnBill:Lcom/squareup/protos/common/Money;

.field public final request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

.field transient service:Lcom/squareup/server/bills/BillCreationService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation
.end field

.field transient transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CompleteBillRequest;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    .line 68
    invoke-direct {p0, p5}, Lcom/squareup/queue/bills/BillTask;-><init>(Ljava/lang/String;)V

    .line 69
    iput-object p6, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->clientId:Ljava/lang/String;

    .line 70
    iput-object p7, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->paymentConfigAmountPaidOnBill:Lcom/squareup/protos/common/Money;

    .line 71
    invoke-static {p1, p7}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->sanityCheckCompleteBillRequest(Lcom/squareup/protos/client/bills/CompleteBillRequest;Lcom/squareup/protos/common/Money;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    .line 73
    iput-object p2, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->itemizations:Ljava/util/List;

    .line 74
    iput-object p3, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->adjustments:Ljava/util/List;

    .line 75
    iput-object p4, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->tenders:Ljava/util/List;

    return-void
.end method

.method private getCompletionDate()Ljava/util/Date;
    .locals 2

    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 205
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private requireCompleteTenders()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation

    .line 158
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->completeTenders:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->completeTenders:Ljava/util/Map;

    .line 160
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/CompleteTender;

    .line 161
    iget-object v2, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->completeTenders:Ljava/util/Map;

    iget-object v3, v1, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->completeTenders:Ljava/util/Map;

    return-object v0
.end method

.method private static sanityCheckCompleteBillRequest(Lcom/squareup/protos/client/bills/CompleteBillRequest;Lcom/squareup/protos/common/Money;)V
    .locals 10

    .line 84
    iget-object v0, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 85
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v2, 0x0

    .line 87
    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 92
    iget-object p0, p0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->tender:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v7, v6

    move-object v6, v4

    const/4 v4, 0x1

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/protos/client/bills/CompleteTender;

    .line 93
    iget-object v9, v8, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    iget-object v9, v9, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    invoke-static {v6, v9}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 94
    iget-object v9, v8, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    iget-object v9, v9, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {v7, v9}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v7

    .line 95
    iget-object v8, v8, Lcom/squareup/protos/client/bills/CompleteTender;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    if-nez v8, :cond_0

    const/4 v8, 0x1

    goto :goto_1

    :cond_0
    const/4 v8, 0x0

    :goto_1
    and-int/2addr v4, v8

    goto :goto_0

    :cond_1
    if-eqz v4, :cond_2

    if-nez v7, :cond_2

    .line 101
    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v7

    :cond_2
    if-eqz p1, :cond_3

    .line 106
    invoke-static {v6, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 109
    :cond_3
    invoke-static {v0, v6, v7}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->sanityCheck(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-void
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 0

    .line 168
    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->asBillBuilder(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public asBillBuilder(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forBillIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->asBillBuilder(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public asBillBuilder(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 9

    .line 176
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 177
    invoke-direct {p0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->requireCompleteTenders()Ljava/util/Map;

    move-result-object v0

    .line 179
    iget-object v1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender;

    .line 180
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CompleteTender;

    .line 181
    iget-object v4, v4, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    const/4 v5, 0x0

    .line 183
    iget-object v6, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 185
    :try_start_0
    iget-object v6, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    invoke-static {v6}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v6

    .line 187
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to parse tip percentage "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 190
    :cond_0
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v6

    iget-object v7, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    invoke-static {v3, v6, v7, v4, v5}, Lcom/squareup/billhistory/model/TenderHistory;->fromTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v4, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 195
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v0, p1}, Lcom/squareup/billhistory/Bills;->createBillNoteFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object v6

    .line 196
    new-instance p1, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-static {p0}, Lcom/squareup/payment/OrderTaskHelper;->orderSnapshotForTask(Lcom/squareup/queue/PaymentTask;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v3

    .line 197
    invoke-direct {p0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->getCompletionDate()Ljava/util/Date;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p1

    move-object v1, p2

    invoke-direct/range {v0 .. v8}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    iget-object p2, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 198
    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 210
    instance-of v0, p1, Lcom/squareup/queue/bills/AbstractCompleteBillTask;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 213
    :cond_0
    check-cast p1, Lcom/squareup/queue/bills/AbstractCompleteBillTask;

    .line 214
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v2, p1, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/CompleteBillRequest;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->itemizations:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->itemizations:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->adjustments:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->adjustments:Ljava/util/List;

    .line 215
    invoke-interface {v0, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->tenders:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->tenders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getAdjustments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->adjustments:Ljava/util/List;

    return-object v0
.end method

.method public getBillId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    return-object v0
.end method

.method public getCompleteBillRequest()Lcom/squareup/protos/client/bills/CompleteBillRequest;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    return-object v0
.end method

.method public getItemizations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->itemizations:Ljava/util/List;

    return-object v0
.end method

.method public getTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->tenders:Ljava/util/List;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .line 146
    invoke-direct {p0}, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->getCompletionDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 219
    iget-object v1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->itemizations:Ljava/util/List;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->adjustments:Ljava/util/List;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->tenders:Ljava/util/List;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCompleteBillEnqueued(Lcom/squareup/protos/client/bills/CompleteBillRequest;)V

    return-void
.end method

.method public secureCopyWithoutPIIForStoreAndForwardPayments()Lcom/squareup/queue/bills/CompleteBill;
    .locals 9

    .line 113
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 114
    iget-object v0, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Tender;

    .line 115
    sget-object v2, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 117
    :cond_0
    new-instance v8, Lcom/squareup/queue/bills/CompleteBill;

    sget-object v0, Lcom/squareup/protos/client/bills/CompleteBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->request:Lcom/squareup/protos/client/bills/CompleteBillRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/client/bills/CompleteBillRequest;

    iget-object v2, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->itemizations:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->adjustments:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->ticketId:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/squareup/queue/bills/AbstractCompleteBillTask;->paymentConfigAmountPaidOnBill:Lcom/squareup/protos/common/Money;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/queue/bills/CompleteBill;-><init>(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v8
.end method
