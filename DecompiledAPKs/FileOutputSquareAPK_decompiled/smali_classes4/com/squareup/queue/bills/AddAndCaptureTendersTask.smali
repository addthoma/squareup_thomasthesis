.class public final Lcom/squareup/queue/bills/AddAndCaptureTendersTask;
.super Lcom/squareup/queue/TransactionRpcThreadTask;
.source "AddAndCaptureTendersTask.kt"

# interfaces
.implements Lcom/squareup/queue/LocalPaymentsQueueTask;
.implements Lcom/squareup/queue/PaymentTask;
.implements Lcom/squareup/queue/EnqueueableTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/TransactionRpcThreadTask<",
        "Lcom/squareup/protos/client/bills/CaptureTendersResponse;",
        ">;",
        "Lcom/squareup/queue/LocalPaymentsQueueTask;",
        "Lcom/squareup/queue/PaymentTask;",
        "Lcom/squareup/queue/EnqueueableTask;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00f8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u00032\u00020\u00042\u00020\u0005BO\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\u000b\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0011\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010T\u001a\u00020U2\u0006\u0010V\u001a\u00020WH\u0016J\u0010\u0010X\u001a\u00020Y2\u0006\u0010V\u001a\u00020WH\u0002J\u0018\u0010X\u001a\u00020Y2\u0006\u0010V\u001a\u00020W2\u0006\u0010Z\u001a\u00020[H\u0002J\u0008\u0010\\\u001a\u00020\u0002H\u0014J\u0008\u0010]\u001a\u00020^H\u0002J\u000e\u0010_\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u000eH\u0016J\u0012\u0010`\u001a\u0004\u0018\u00010a2\u0006\u0010b\u001a\u00020\u000bH\u0002J\u0012\u0010c\u001a\u0004\u0018\u00010a2\u0006\u0010b\u001a\u00020\u000bH\u0002J\u0008\u0010d\u001a\u00020\u000bH\u0002J\u0008\u0010e\u001a\u00020fH\u0002J\u000e\u0010g\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u000eH\u0016J\u0008\u0010h\u001a\u00020\u000bH\u0016J\u0008\u0010i\u001a\u00020jH\u0016J\u0008\u0010k\u001a\u00020lH\u0016J\u0008\u0010m\u001a\u00020\u000bH\u0016J\u0010\u0010n\u001a\u00020o2\u0006\u0010p\u001a\u00020\u0002H\u0014J\u0010\u0010q\u001a\u00020^2\u0006\u0010r\u001a\u00020sH\u0016J\u0010\u0010t\u001a\u00020^2\u0006\u0010u\u001a\u00020OH\u0016J\u0014\u0010v\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020x0wH\u0002J\u001a\u0010y\u001a\u00020^2\u0008\u0010z\u001a\u0004\u0018\u00010a2\u0006\u0010{\u001a\u00020aH\u0002J\u0008\u0010|\u001a\u00020}H\u0016J\u0010\u0010~\u001a\u00020^2\u0006\u0010%\u001a\u00020&H\u0016J\u0010\u0010\u007f\u001a\u00020^2\u0006\u0010D\u001a\u00020&H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R$\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00198\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008!\u0010\"\"\u0004\u0008#\u0010$R\u000e\u0010%\u001a\u00020&X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\'\u001a\u00020(8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008)\u0010*\"\u0004\u0008+\u0010,R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010.R\u001e\u0010/\u001a\u0002008\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00081\u00102\"\u0004\u00083\u00104R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u00105\u001a\u0002068\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00087\u00108\"\u0004\u00089\u0010:R$\u0010;\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00198\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008<\u0010\u001c\"\u0004\u0008=\u0010\u001eR\u001e\u0010>\u001a\u00020?8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008@\u0010A\"\u0004\u0008B\u0010CR\u000e\u0010D\u001a\u00020&X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010E\u001a\u00020\u000b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008F\u0010GR\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010H\u001a\u00020I8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008J\u0010K\"\u0004\u0008L\u0010MR\u001e\u0010N\u001a\u00020O8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008P\u0010Q\"\u0004\u0008R\u0010S\u00a8\u0006\u0080\u0001"
    }
    d2 = {
        "Lcom/squareup/queue/bills/AddAndCaptureTendersTask;",
        "Lcom/squareup/queue/TransactionRpcThreadTask;",
        "Lcom/squareup/protos/client/bills/CaptureTendersResponse;",
        "Lcom/squareup/queue/LocalPaymentsQueueTask;",
        "Lcom/squareup/queue/PaymentTask;",
        "Lcom/squareup/queue/EnqueueableTask;",
        "addTendersRequest",
        "Lcom/squareup/protos/client/bills/AddTendersRequest;",
        "captureTenderRequest",
        "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
        "ticketId",
        "",
        "currentClientId",
        "tenders",
        "",
        "Lcom/squareup/protos/client/bills/Tender;",
        "captureAdjustments",
        "",
        "Lcom/squareup/server/payment/AdjustmentMessage;",
        "capturedItemizations",
        "Lcom/squareup/server/payment/ItemizationMessage;",
        "(Lcom/squareup/protos/client/bills/AddTendersRequest;Lcom/squareup/protos/client/bills/CaptureTendersRequest;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V",
        "getAddTendersRequest",
        "()Lcom/squareup/protos/client/bills/AddTendersRequest;",
        "addTendersRequestServerIds",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/settings/AddTendersRequestServerIds;",
        "getAddTendersRequestServerIds",
        "()Lcom/squareup/settings/LocalSetting;",
        "setAddTendersRequestServerIds",
        "(Lcom/squareup/settings/LocalSetting;)V",
        "addTendersResponse",
        "Lcom/squareup/protos/client/bills/AddTendersResponse;",
        "getAddTendersResponse",
        "()Lcom/squareup/protos/client/bills/AddTendersResponse;",
        "setAddTendersResponse",
        "(Lcom/squareup/protos/client/bills/AddTendersResponse;)V",
        "addedToTheLocalPaymentsQueue",
        "",
        "billCreationService",
        "Lcom/squareup/server/bills/BillCreationService;",
        "getBillCreationService",
        "()Lcom/squareup/server/bills/BillCreationService;",
        "setBillCreationService",
        "(Lcom/squareup/server/bills/BillCreationService;)V",
        "getCaptureTenderRequest",
        "()Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
        "captureTenderService",
        "Lcom/squareup/server/tenders/CaptureTenderService;",
        "getCaptureTenderService",
        "()Lcom/squareup/server/tenders/CaptureTenderService;",
        "setCaptureTenderService",
        "(Lcom/squareup/server/tenders/CaptureTenderService;)V",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "setFeatures",
        "(Lcom/squareup/settings/server/Features;)V",
        "lastLocalPaymentServerId",
        "getLastLocalPaymentServerId",
        "setLastLocalPaymentServerId",
        "localTenderCache",
        "Lcom/squareup/print/LocalTenderCache;",
        "getLocalTenderCache",
        "()Lcom/squareup/print/LocalTenderCache;",
        "setLocalTenderCache",
        "(Lcom/squareup/print/LocalTenderCache;)V",
        "onTheTaskQueue",
        "tenderClientId",
        "getTenderClientId",
        "()Ljava/lang/String;",
        "tickets",
        "Lcom/squareup/tickets/Tickets;",
        "getTickets",
        "()Lcom/squareup/tickets/Tickets;",
        "setTickets",
        "(Lcom/squareup/tickets/Tickets;)V",
        "transactionLedgerManager",
        "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
        "getTransactionLedgerManager",
        "()Lcom/squareup/payment/ledger/TransactionLedgerManager;",
        "setTransactionLedgerManager",
        "(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V",
        "asBill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "res",
        "Lcom/squareup/util/Res;",
        "asBillBuilder",
        "Lcom/squareup/billhistory/model/BillHistory$Builder;",
        "billHistoryId",
        "Lcom/squareup/billhistory/model/BillHistoryId;",
        "callOnRpcThread",
        "cleanBillAndTenderServerId",
        "",
        "getAdjustments",
        "getCachedBillServerId",
        "Lcom/squareup/protos/client/IdPair;",
        "clientId",
        "getCachedTenderServerId",
        "getClientId",
        "getCompletionDate",
        "Ljava/util/Date;",
        "getItemizations",
        "getTicketId",
        "getTime",
        "",
        "getTotal",
        "Lcom/squareup/protos/common/Money;",
        "getUuid",
        "handleResponseOnMainThread",
        "Lcom/squareup/server/SimpleResponse;",
        "response",
        "inject",
        "component",
        "Lcom/squareup/queue/TransactionTasksComponent;",
        "logEnqueued",
        "ledger",
        "requireCompleteTenders",
        "",
        "Lcom/squareup/protos/client/bills/CompleteTender;",
        "saveServerIds",
        "billServerId",
        "tenderServerId",
        "secureCopyWithoutPIIForLogs",
        "",
        "setAddedToTheLocalPaymentsQueue",
        "setOnTheTaskQueue",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

.field public transient addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public transient addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

.field private addedToTheLocalPaymentsQueue:Z

.field public transient billCreationService:Lcom/squareup/server/bills/BillCreationService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final captureAdjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

.field public transient captureTenderService:Lcom/squareup/server/tenders/CaptureTenderService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final capturedItemizations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final currentClientId:Ljava/lang/String;

.field public transient features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public transient lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public transient localTenderCache:Lcom/squareup/print/LocalTenderCache;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private onTheTaskQueue:Z

.field private final tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketId:Ljava/lang/String;

.field public transient tickets:Lcom/squareup/tickets/Tickets;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public transient transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/AddTendersRequest;Lcom/squareup/protos/client/bills/CaptureTendersRequest;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;)V"
        }
    .end annotation

    const-string v0, "addTendersRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "captureTenderRequest"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ticketId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentClientId"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenders"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "captureAdjustments"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capturedItemizations"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/queue/TransactionRpcThreadTask;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    iput-object p2, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iput-object p3, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->ticketId:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->currentClientId:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->tenders:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureAdjustments:Ljava/util/List;

    iput-object p7, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->capturedItemizations:Ljava/util/List;

    return-void
.end method

.method private final asBillBuilder(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forBillIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    const-string v1, "BillHistoryId.forBillIdP\u2026nderRequest.bill_id_pair)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->asBillBuilder(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method private final asBillBuilder(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 11

    .line 249
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 250
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->requireCompleteTenders()Ljava/util/Map;

    move-result-object v1

    .line 252
    iget-object v2, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->tenders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender;

    .line 253
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CompleteTender;

    if-nez v4, :cond_0

    .line 254
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v4, v4, Lcom/squareup/protos/client/bills/CompleteTender;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    const/4 v5, 0x0

    .line 255
    check-cast v5, Lcom/squareup/util/Percentage;

    .line 256
    iget-object v6, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 258
    :try_start_0
    sget-object v6, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v7, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    const-string v8, "amounts.tip_percentage"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lcom/squareup/util/Percentage$Companion;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v6

    .line 260
    check-cast v6, Ljava/lang/Throwable;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to parse tip percentage "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_percentage:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 265
    :cond_1
    :goto_1
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v6

    iget-object v7, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 266
    iget-object v4, v4, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;->total_charged_money:Lcom/squareup/protos/common/Money;

    .line 264
    invoke-static {v3, v6, v7, v4, v5}, Lcom/squareup/billhistory/model/TenderHistory;->fromTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v3

    .line 263
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 271
    :cond_2
    iget-object v1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v6, v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 272
    iget-object v1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v1, p1}, Lcom/squareup/billhistory/Bills;->createBillNoteFromCart(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object v8

    .line 273
    new-instance p1, Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 274
    move-object v4, v0

    check-cast v4, Ljava/util/List;

    move-object v0, p0

    check-cast v0, Lcom/squareup/queue/PaymentTask;

    invoke-static {v0}, Lcom/squareup/payment/OrderTaskHelper;->orderSnapshotForTask(Lcom/squareup/queue/PaymentTask;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/Order;

    .line 275
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->getCompletionDate()Ljava/util/Date;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v2, p1

    move-object v3, p2

    .line 273
    invoke-direct/range {v2 .. v10}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    .line 277
    iget-object p2, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {p1, p2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    const-string p2, "BillHistory.Builder(\n   \u2026aptureTenderRequest.cart)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final cleanBillAndTenderServerId()V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    if-nez v0, :cond_0

    const-string v1, "addTendersRequestServerIds"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final getCachedBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    if-nez v0, :cond_0

    const-string v1, "addTendersRequestServerIds"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {v0, p1}, Lcom/squareup/settings/AddTendersRequestServerIds;->getBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final getCachedTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    if-nez v0, :cond_0

    const-string v1, "addTendersRequestServerIds"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {v0, p1}, Lcom/squareup/settings/AddTendersRequestServerIds;->getTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final getClientId()Ljava/lang/String;
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v1, "captureTenderRequest.bill_id_pair.client_id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getCompletionDate()Ljava/util/Date;
    .locals 2

    .line 227
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v0, v0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    const-string v1, "Times.parseIso8601Date(c\u2026completed_at.date_string)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 229
    new-instance v1, Ljava/lang/RuntimeException;

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method private final getTenderClientId()Ljava/lang/String;
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v1, "addTendersRequest.add_te\u2026.tender_id_pair.client_id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final requireCompleteTenders()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/CompleteTender;",
            ">;"
        }
    .end annotation

    .line 281
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 282
    iget-object v1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/CompleteTender;

    .line 283
    move-object v3, v0

    check-cast v3, Ljava/util/Map;

    iget-object v4, v2, Lcom/squareup/protos/client/bills/CompleteTender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v5, "completeTender.tender_id_pair.client_id"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "completeTender"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 285
    :cond_0
    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method private final saveServerIds(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V
    .locals 1

    .line 197
    new-instance v0, Lcom/squareup/settings/AddTendersRequestServerIds;

    invoke-direct {v0, p1, p2}, Lcom/squareup/settings/AddTendersRequestServerIds;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    .line 198
    iget-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    if-nez p1, :cond_0

    const-string p2, "addTendersRequestServerIds"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    invoke-direct {p0, p1}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->asBillBuilder(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    const-string v0, "asBillBuilder(res).build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected callOnRpcThread()Lcom/squareup/protos/client/bills/CaptureTendersResponse;
    .locals 8

    .line 82
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->getCachedBillServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 83
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->getTenderClientId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->getCachedTenderServerId(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 89
    iget-boolean v2, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->onTheTaskQueue:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addedToTheLocalPaymentsQueue:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->features:Lcom/squareup/settings/server/Features;

    if-nez v2, :cond_0

    const-string v3, "features"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 90
    :cond_0
    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    .line 89
    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    new-instance v0, Lcom/squareup/protos/client/bills/CaptureTendersResponse;

    new-instance v1, Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "Success"

    invoke-direct {v1, v4, v4, v2, v3}, Lcom/squareup/protos/client/Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/resources/Error;)V

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/bills/CaptureTendersResponse;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0

    :cond_1
    const-string v2, "transactionLedgerManager"

    if-nez v0, :cond_e

    .line 100
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    const-string v1, "lastLocalPaymentServerId"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const-string v3, ""

    invoke-interface {v0, v3}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    if-nez v0, :cond_3

    const-string v3, "billCreationService"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v3, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    iget-object v4, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->currentClientId:Ljava/lang/String;

    invoke-static {v4}, Lcom/squareup/server/bills/ApiClientId;->clientIdOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/squareup/server/bills/BillCreationService;->addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersResponse;

    move-result-object v0

    const-string v3, "billCreationService.addT\u2026dOrNull(currentClientId))"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    .line 104
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    if-nez v0, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v3, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    const-string v4, "addTendersResponse"

    if-nez v3, :cond_5

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-interface {v0, v3}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    if-nez v0, :cond_6

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_8

    .line 107
    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    if-nez v1, :cond_7

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    iget-object v1, v1, Lcom/squareup/protos/client/bills/AddTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 108
    check-cast v0, Ljava/lang/Throwable;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddTenders call failed for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 109
    throw v0

    .line 112
    :cond_8
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    if-nez v0, :cond_9

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    iget-object v0, v0, Lcom/squareup/protos/client/bills/AddTendersResponse;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 113
    iget-object v3, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    if-nez v3, :cond_a

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    iget-object v3, v3, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 117
    iget-object v6, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    if-nez v6, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    iget-object v1, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-interface {v6, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    const-string v1, "tenderIdPair"

    .line 121
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v3}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->saveServerIds(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    .line 125
    iget-object v1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    if-nez v1, :cond_c

    const-string v6, "localTenderCache"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 126
    :cond_c
    iget-object v6, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 127
    iget-object v7, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    if-nez v7, :cond_d

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    iget-object v4, v7, Lcom/squareup/protos/client/bills/AddTendersResponse;->tender:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/AddedTender;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    iget-object v5, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 125
    invoke-virtual {v1, v6, v4, v5}, Lcom/squareup/print/LocalTenderCache;->setLast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 131
    :cond_e
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 132
    iget-object v4, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->tenders:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/bills/CompleteTender;

    .line 133
    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/CompleteTender;->newBuilder()Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 137
    :cond_f
    iget-object v1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object v1

    .line 138
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object v0

    .line 139
    check-cast v3, Ljava/util/List;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tenders(Ljava/util/List;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderService:Lcom/squareup/server/tenders/CaptureTenderService;

    if-nez v1, :cond_10

    const-string v3, "captureTenderService"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    const-string v3, "newRequest"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/tenders/CaptureTenderService;->captureTenders(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)Lcom/squareup/protos/client/bills/CaptureTendersResponse;

    move-result-object v0

    .line 143
    iget-object v1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    if-nez v1, :cond_11

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCaptureTenderResponse(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)V

    .line 145
    iget-object v1, v0, Lcom/squareup/protos/client/bills/CaptureTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 152
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->cleanBillAndTenderServerId()V

    return-object v0

    .line 146
    :cond_12
    new-instance v1, Ljava/lang/RuntimeException;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 147
    check-cast v1, Ljava/lang/Throwable;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CompleteBill call failed for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 148
    throw v1
.end method

.method public bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->callOnRpcThread()Lcom/squareup/protos/client/bills/CaptureTendersResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getAddTendersRequest()Lcom/squareup/protos/client/bills/AddTendersRequest;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    return-object v0
.end method

.method public final getAddTendersRequestServerIds()Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    if-nez v0, :cond_0

    const-string v1, "addTendersRequestServerIds"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getAddTendersResponse()Lcom/squareup/protos/client/bills/AddTendersResponse;
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    if-nez v0, :cond_0

    const-string v1, "addTendersResponse"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getAdjustments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 221
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureAdjustments:Ljava/util/List;

    return-object v0
.end method

.method public final getBillCreationService()Lcom/squareup/server/bills/BillCreationService;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    if-nez v0, :cond_0

    const-string v1, "billCreationService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getCaptureTenderRequest()Lcom/squareup/protos/client/bills/CaptureTendersRequest;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    return-object v0
.end method

.method public final getCaptureTenderService()Lcom/squareup/server/tenders/CaptureTenderService;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderService:Lcom/squareup/server/tenders/CaptureTenderService;

    if-nez v0, :cond_0

    const-string v1, "captureTenderService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->features:Lcom/squareup/settings/server/Features;

    if-nez v0, :cond_0

    const-string v1, "features"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getItemizations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 219
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->capturedItemizations:Ljava/util/List;

    return-object v0
.end method

.method public final getLastLocalPaymentServerId()Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    if-nez v0, :cond_0

    const-string v1, "lastLocalPaymentServerId"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getLocalTenderCache()Lcom/squareup/print/LocalTenderCache;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    if-nez v0, :cond_0

    const-string v1, "localTenderCache"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->ticketId:Ljava/lang/String;

    return-object v0
.end method

.method public final getTickets()Lcom/squareup/tickets/Tickets;
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->tickets:Lcom/squareup/tickets/Tickets;

    if-nez v0, :cond_0

    const-string v1, "tickets"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getTime()J
    .locals 2

    .line 223
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->getCompletionDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 217
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CaptureTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const-string v1, "captureTenderRequest.cart.amounts.total_money"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    if-nez v0, :cond_0

    const-string v1, "transactionLedgerManager"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 166
    invoke-direct {p0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->getClientId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CaptureTendersResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p1}, Lcom/squareup/server/SimpleResponse;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method public bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/protos/client/bills/CaptureTendersResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    const-string v0, "ledger"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderRequest:Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCaptureTenderRequest(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 2

    .line 180
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequest:Lcom/squareup/protos/client/bills/AddTendersRequest;

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "addTendersRequest.toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setAddTendersRequestServerIds(Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public final setAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersResponse:Lcom/squareup/protos/client/bills/AddTendersResponse;

    return-void
.end method

.method public setAddedToTheLocalPaymentsQueue(Z)V
    .locals 0

    .line 214
    iput-boolean p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addedToTheLocalPaymentsQueue:Z

    return-void
.end method

.method public final setBillCreationService(Lcom/squareup/server/bills/BillCreationService;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    return-void
.end method

.method public final setCaptureTenderService(Lcom/squareup/server/tenders/CaptureTenderService;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderService:Lcom/squareup/server/tenders/CaptureTenderService;

    return-void
.end method

.method public final setFeatures(Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public final setLastLocalPaymentServerId(Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public final setLocalTenderCache(Lcom/squareup/print/LocalTenderCache;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    return-void
.end method

.method public setOnTheTaskQueue(Z)V
    .locals 0

    .line 210
    iput-boolean p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->onTheTaskQueue:Z

    return-void
.end method

.method public final setTickets(Lcom/squareup/tickets/Tickets;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->tickets:Lcom/squareup/tickets/Tickets;

    return-void
.end method

.method public final setTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method
