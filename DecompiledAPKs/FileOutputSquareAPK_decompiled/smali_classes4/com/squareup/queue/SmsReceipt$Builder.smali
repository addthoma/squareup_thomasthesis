.class public Lcom/squareup/queue/SmsReceipt$Builder;
.super Ljava/lang/Object;
.source "SmsReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/SmsReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private legacyBillId:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private paymentId:Ljava/lang/String;

.field private phone:Ljava/lang/String;

.field private resend:Z

.field private uniqueKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/SmsReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/queue/SmsReceipt$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/SmsReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/queue/SmsReceipt$Builder;->legacyBillId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/queue/SmsReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/queue/SmsReceipt$Builder;->phone:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/queue/SmsReceipt$Builder;)Z
    .locals 0

    .line 62
    iget-boolean p0, p0, Lcom/squareup/queue/SmsReceipt$Builder;->resend:Z

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/queue/SmsReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/queue/SmsReceipt$Builder;->uniqueKey:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/queue/SmsReceipt$Builder;Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;
    .locals 0

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/queue/SmsReceipt$Builder;->legacyBillId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;

    move-result-object p0

    return-object p0
.end method

.method private legacyBillId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/squareup/queue/SmsReceipt$Builder;->legacyBillId:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/queue/SmsReceipt;
    .locals 2

    .line 105
    new-instance v0, Lcom/squareup/queue/SmsReceipt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/SmsReceipt;-><init>(Lcom/squareup/queue/SmsReceipt$Builder;Lcom/squareup/queue/SmsReceipt$1;)V

    return-object v0
.end method

.method public paymentId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/queue/SmsReceipt$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method public paymentIdOrLegacyBillId(Ljava/lang/String;Lcom/squareup/PaymentType;)Lcom/squareup/queue/SmsReceipt$Builder;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 70
    sget-object v0, Lcom/squareup/PaymentType;->BILL:Lcom/squareup/PaymentType;

    if-ne p2, v0, :cond_0

    .line 71
    iput-object p1, p0, Lcom/squareup/queue/SmsReceipt$Builder;->legacyBillId:Ljava/lang/String;

    goto :goto_0

    .line 73
    :cond_0
    iput-object p1, p0, Lcom/squareup/queue/SmsReceipt$Builder;->paymentId:Ljava/lang/String;

    :goto_0
    return-object p0
.end method

.method public phone(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/queue/SmsReceipt$Builder;->phone:Ljava/lang/String;

    return-object p0
.end method

.method resend(Z)Lcom/squareup/queue/SmsReceipt$Builder;
    .locals 0

    .line 95
    iput-boolean p1, p0, Lcom/squareup/queue/SmsReceipt$Builder;->resend:Z

    return-object p0
.end method

.method uniqueKey(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/queue/SmsReceipt$Builder;->uniqueKey:Ljava/lang/String;

    return-object p0
.end method
