.class public Lcom/squareup/queue/crm/EmailCollectionTask;
.super Lcom/squareup/queue/RpcThreadTask;
.source "EmailCollectionTask.java"

# interfaces
.implements Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/crm/EmailCollectionTask$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/server/SimpleResponse;",
        "Lcom/squareup/queue/QueueModule$Component;",
        ">;",
        "Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;"
    }
.end annotation


# instance fields
.field private final buyerEmailAddress:Ljava/lang/String;

.field private final customerContact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final paymentToken:Ljava/lang/String;

.field private final requestToken:Ljava/util/UUID;

.field transient rolodex:Lcom/squareup/crm/RolodexServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)V
    .locals 1

    .line 33
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    .line 34
    invoke-static {p1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->access$000(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->requestToken:Ljava/util/UUID;

    .line 35
    invoke-static {p1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->access$100(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->paymentToken:Ljava/lang/String;

    .line 36
    invoke-static {p1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->access$200(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->buyerEmailAddress:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->access$300(Lcom/squareup/queue/crm/EmailCollectionTask$Builder;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/server/SimpleResponse;
    .locals 4

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    if-nez v0, :cond_0

    new-instance v0, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    .line 94
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    :goto_0
    iget-object v1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->buyerEmailAddress:Ljava/lang/String;

    .line 93
    invoke-static {v0, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->withEmail(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v2, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->requestToken:Ljava/util/UUID;

    iget-object v3, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->paymentToken:Ljava/lang/String;

    .line 98
    invoke-interface {v1, v2, v3, v0}, Lcom/squareup/crm/RolodexServiceHelper;->upsertContactForEmailCollection(Ljava/util/UUID;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    .line 102
    new-instance v1, Lcom/squareup/server/SimpleResponse;

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-direct {v1, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    .line 104
    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 105
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/queue/crm/EmailCollectionTask;->callOnRpcThread()Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    return-object v0
.end method

.method public getBuyerEmailAddress()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->buyerEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getPaymentToken()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->paymentToken:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestToken()Ljava/util/UUID;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->requestToken:Ljava/util/UUID;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/EmailCollectionTask;->handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 0

    .line 75
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/queue/crm/EmailCollectionTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/EmailCollectionTask;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 2

    .line 85
    new-instance v0, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->paymentToken:Ljava/lang/String;

    .line 86
    invoke-virtual {v0, v1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->paymentToken(Ljava/lang/String;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;

    move-result-object v0

    const-string v1, "[REDACTED_EMAIL]"

    .line 87
    invoke-virtual {v0, v1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->emailAddress(Ljava/lang/String;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->requestToken:Ljava/util/UUID;

    .line 88
    invoke-virtual {v0, v1}, Lcom/squareup/queue/crm/EmailCollectionTask$Builder;->requestToken(Ljava/util/UUID;)Lcom/squareup/queue/crm/EmailCollectionTask$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 79
    iget-object v1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->paymentToken:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->buyerEmailAddress:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/EmailCollectionTask;->requestToken:Ljava/util/UUID;

    .line 81
    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "EmailCollectionTask{paymentToken=\'%s\', buyerEmailAddress=\'%s\', customerContact=\'%s\', requestToken=\'%s\'}"

    .line 79
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
