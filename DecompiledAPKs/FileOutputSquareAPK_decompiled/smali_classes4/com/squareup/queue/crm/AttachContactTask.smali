.class public Lcom/squareup/queue/crm/AttachContactTask;
.super Lcom/squareup/queue/RpcThreadTask;
.source "AttachContactTask.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/crm/AttachContactTask$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "Lcom/squareup/protos/client/rolodex/SetContactForPaymentResponse;",
        ">;",
        "Lcom/squareup/queue/TransactionTasksComponent;",
        ">;",
        "Lcom/squareup/queue/LoggedInTransactionTask;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final contactToken:Ljava/lang/String;

.field private final merchantToken:Ljava/lang/String;

.field private final paymentId:Ljava/lang/String;

.field transient rolodexService:Lcom/squareup/server/crm/RolodexService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/queue/crm/AttachContactTask$Builder;)V
    .locals 2

    .line 69
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    .line 70
    invoke-static {p1}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->access$100(Lcom/squareup/queue/crm/AttachContactTask$Builder;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "paymentId"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/crm/AttachContactTask;->paymentId:Ljava/lang/String;

    .line 71
    invoke-static {p1}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->access$200(Lcom/squareup/queue/crm/AttachContactTask$Builder;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "contactToken"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/crm/AttachContactTask;->contactToken:Ljava/lang/String;

    .line 72
    invoke-static {p1}, Lcom/squareup/queue/crm/AttachContactTask$Builder;->access$300(Lcom/squareup/queue/crm/AttachContactTask$Builder;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "merchantToken"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/queue/crm/AttachContactTask;->merchantToken:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/queue/crm/AttachContactTask$Builder;Lcom/squareup/queue/crm/AttachContactTask$1;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/queue/crm/AttachContactTask;-><init>(Lcom/squareup/queue/crm/AttachContactTask$Builder;)V

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/SetContactForPaymentResponse;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/queue/crm/AttachContactTask;->rolodexService:Lcom/squareup/server/crm/RolodexService;

    new-instance v1, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/queue/crm/AttachContactTask;->paymentId:Ljava/lang/String;

    .line 77
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/crm/AttachContactTask;->contactToken:Ljava/lang/String;

    .line 78
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/crm/AttachContactTask;->merchantToken:Ljava/lang/String;

    .line 79
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;

    move-result-object v1

    .line 80
    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;

    move-result-object v1

    .line 76
    invoke-interface {v0, v1}, Lcom/squareup/server/crm/RolodexService;->setContact(Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/queue/crm/AttachContactTask;->callOnRpcThread()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    return-object v0
.end method

.method public getContactToken()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/queue/crm/AttachContactTask;->contactToken:Ljava/lang/String;

    return-object v0
.end method

.method public getMerchantToken()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/queue/crm/AttachContactTask;->merchantToken:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentId()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/queue/crm/AttachContactTask;->paymentId:Ljava/lang/String;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/server/SimpleResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/SetContactForPaymentResponse;",
            ">;)",
            "Lcom/squareup/server/SimpleResponse;"
        }
    .end annotation

    .line 89
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentResponse;

    .line 91
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 92
    new-instance p1, Lcom/squareup/server/SimpleResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {p1, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object p1

    .line 94
    :cond_0
    new-instance p1, Lcom/squareup/server/SimpleResponse;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/AttachContactTask;->handleResponseOnMainThread(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 41
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/crm/AttachContactTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/AttachContactTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AttachContactTask{paymentId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/crm/AttachContactTask;->paymentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", contactToken=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/crm/AttachContactTask;->contactToken:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", merchantToken=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/crm/AttachContactTask;->merchantToken:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
