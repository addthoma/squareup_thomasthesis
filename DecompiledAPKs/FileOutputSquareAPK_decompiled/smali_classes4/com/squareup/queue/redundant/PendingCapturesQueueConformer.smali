.class public Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;
.super Lcom/squareup/queue/redundant/QueueConformer;
.source "PendingCapturesQueueConformer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/redundant/QueueConformer<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/queue/redundant/RedundantRetrofitQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/queue/CorruptQueueRecorder;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-virtual {p1}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->getTapeQueue()Lcom/squareup/tape/FileObjectQueue;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->getSqliteQueue()Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    move-result-object v2

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/redundant/QueueConformer;-><init>(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/queue/CorruptQueueRecorder;)V

    return-void
.end method
