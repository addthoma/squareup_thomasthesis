.class public Lcom/squareup/queue/redundant/QueueConformerWatcher;
.super Ljava/lang/Object;
.source "QueueConformerWatcher.java"


# instance fields
.field private final mainScheduler:Lrx/Scheduler;

.field private final pendingCapturesQueueConformer:Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;

.field private final storedPaymentsQueueConformer:Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

.field private final tasksQueueConformer:Lcom/squareup/queue/redundant/TasksQueueConformer;


# direct methods
.method constructor <init>(Lrx/Scheduler;Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;Lcom/squareup/queue/redundant/TasksQueueConformer;)V
    .locals 0
    .param p1    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher;->mainScheduler:Lrx/Scheduler;

    .line 29
    iput-object p2, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher;->pendingCapturesQueueConformer:Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;

    .line 30
    iput-object p3, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher;->storedPaymentsQueueConformer:Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    .line 31
    iput-object p4, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher;->tasksQueueConformer:Lcom/squareup/queue/redundant/TasksQueueConformer;

    return-void
.end method

.method static synthetic lambda$queuesConformed$0(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 42
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 43
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 44
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 42
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public queuesConformed()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher;->pendingCapturesQueueConformer:Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;

    invoke-virtual {v0}, Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;->conformed()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher;->storedPaymentsQueueConformer:Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    .line 40
    invoke-virtual {v1}, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;->conformed()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher;->tasksQueueConformer:Lcom/squareup/queue/redundant/TasksQueueConformer;

    invoke-virtual {v2}, Lcom/squareup/queue/redundant/TasksQueueConformer;->conformed()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformerWatcher$OlTsfJcN8-pA9wodpNefp64kj6w;->INSTANCE:Lcom/squareup/queue/redundant/-$$Lambda$QueueConformerWatcher$OlTsfJcN8-pA9wodpNefp64kj6w;

    .line 39
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher;->mainScheduler:Lrx/Scheduler;

    .line 46
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
