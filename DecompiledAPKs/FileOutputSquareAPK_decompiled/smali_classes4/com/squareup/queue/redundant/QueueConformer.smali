.class public abstract Lcom/squareup/queue/redundant/QueueConformer;
.super Ljava/lang/Object;
.source "QueueConformer.java"

# interfaces
.implements Lcom/squareup/tape/ObjectQueue$Listener;
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;,
        Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/ObjectQueue$Listener<",
        "TT;>;",
        "Lmortar/Scoped;"
    }
.end annotation


# instance fields
.field final addDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final conformed:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

.field private final deferredFinish:Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/redundant/QueueConformer<",
            "TT;>.DeferredFinish;"
        }
    .end annotation
.end field

.field private entriesAddedCount:I

.field private final queueExecutor:Lcom/squareup/thread/executor/SerialExecutor;

.field final removeAllDisposable:Lio/reactivex/disposables/SerialDisposable;

.field private final sqliteQueue:Lcom/squareup/queue/sqlite/shared/SqliteQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final tapeQueue:Lcom/squareup/tape/ObjectQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final tapeQueueListener:Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "TT;>;"
        }
    .end annotation
.end field

.field private tapeQueueSize:I


# direct methods
.method constructor <init>(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/queue/CorruptQueueRecorder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "TT;>;",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "TT;>;",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")V"
        }
    .end annotation

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 96
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->conformed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 97
    new-instance v1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 98
    new-instance v1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->removeAllDisposable:Lio/reactivex/disposables/SerialDisposable;

    .line 99
    new-instance v1, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;

    invoke-direct {v1, p0}, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;-><init>(Lcom/squareup/queue/redundant/QueueConformer;)V

    iput-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->deferredFinish:Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;

    .line 101
    iput v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueSize:I

    .line 102
    iput v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->entriesAddedCount:I

    .line 107
    iput-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueue:Lcom/squareup/tape/ObjectQueue;

    .line 108
    iput-object p2, p0, Lcom/squareup/queue/redundant/QueueConformer;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    .line 109
    iput-object p3, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueListener:Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    .line 110
    iput-object p4, p0, Lcom/squareup/queue/redundant/QueueConformer;->queueExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    .line 111
    iput-object p5, p0, Lcom/squareup/queue/redundant/QueueConformer;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/queue/redundant/QueueConformer;)Lcom/squareup/thread/executor/SerialExecutor;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/queue/redundant/QueueConformer;->queueExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    return-object p0
.end method

.method private clearSqliteQueueAndCopyItemsFromTapeQueue(II)V
    .locals 3

    .line 176
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->removeAllDisposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v1}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->removeAll()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformer$p0_wGMA6sN5IxXQDjDRGV8NqA-4;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformer$p0_wGMA6sN5IxXQDjDRGV8NqA-4;-><init>(Lcom/squareup/queue/redundant/QueueConformer;II)V

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private clearSqliteQueueBecauseTapeIsCorrupt()Z
    .locals 3

    .line 135
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-virtual {v0}, Lcom/squareup/queue/CorruptQueueRecorder;->hasCorruptQueue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->queueExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v2, Lcom/squareup/queue/redundant/-$$Lambda$-RaTIcnuvXr4kxtRpVqgVOLSYEg;

    invoke-direct {v2, p0}, Lcom/squareup/queue/redundant/-$$Lambda$-RaTIcnuvXr4kxtRpVqgVOLSYEg;-><init>(Lcom/squareup/queue/redundant/QueueConformer;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return v0
.end method

.method private delegateToTapeQueueListenerOnceConformed()V
    .locals 4

    .line 224
    iget v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->entriesAddedCount:I

    iget v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueSize:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "QueueConformer::delegateToTapeQueueListenerOnceConformed entries >= tape queue size"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 226
    iget v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->entriesAddedCount:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->entriesAddedCount:I

    iget v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueSize:I

    if-ne v0, v1, :cond_1

    .line 227
    iput v3, p0, Lcom/squareup/queue/redundant/QueueConformer;->entriesAddedCount:I

    .line 228
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->deferredFinish:Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;

    invoke-virtual {v0}, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->finish()V

    :cond_1
    return-void
.end method

.method public static synthetic lambda$vZljNiVBZTeoDE5k9SvW2W7OqW4(Lcom/squareup/queue/redundant/QueueConformer;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/queue/redundant/QueueConformer;->delegateToTapeQueueListenerOnceConformed()V

    return-void
.end method

.method private logMismatch(II)V
    .locals 6

    const-string v0, "Mismatch between Tape and SQLite queues. %s Tape queue has %d entries, but %s SQLite queue has %d entries."

    if-lez p2, :cond_0

    goto :goto_0

    .line 264
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " (May be due to ordinary course version upgrade.)"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 267
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueue:Lcom/squareup/tape/ObjectQueue;

    .line 268
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v4

    const/4 p1, 0x2

    iget-object v4, p0, Lcom/squareup/queue/redundant/QueueConformer;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    .line 269
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p1

    const/4 p1, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v3, p1

    .line 268
    invoke-static {v2, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 267
    invoke-static {v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void
.end method

.method private verifyRemovedAll(II)V
    .locals 4

    if-ne p1, p2, :cond_0

    return-void

    .line 274
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 275
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v3

    const/4 p2, 0x1

    .line 276
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, p2

    const-string p1, "Expected to remove all %d entry(ies) from SQLite queue, but removed %d"

    .line 274
    invoke-static {v1, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method clearSqliteQueueAndFinish()V
    .locals 4

    .line 143
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    .line 144
    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->size()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    .line 143
    invoke-static {v0}, Lcom/squareup/util/rx/RxBlockingSupport;->getValueOrThrow(Lrx/Observable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/QueueConformer;->finish()V

    return-void

    .line 150
    :cond_0
    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->removeAllDisposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v2, p0, Lcom/squareup/queue/redundant/QueueConformer;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v2}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->removeAll()Lio/reactivex/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformer$Q5s3Oap2gR213wUVf9wbW1gV5Uw;

    invoke-direct {v3, p0, v0}, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformer$Q5s3Oap2gR213wUVf9wbW1gV5Uw;-><init>(Lcom/squareup/queue/redundant/QueueConformer;I)V

    .line 151
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 150
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method conformQueuesIfNeeded()V
    .locals 3

    .line 159
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueue:Lcom/squareup/tape/ObjectQueue;

    invoke-interface {v0}, Lcom/squareup/tape/ObjectQueue;->size()I

    move-result v0

    iput v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueSize:I

    .line 160
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    .line 161
    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->size()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    .line 160
    invoke-static {v0}, Lcom/squareup/util/rx/RxBlockingSupport;->getValueOrThrow(Lrx/Observable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 164
    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->conformed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget v2, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueSize:I

    if-ne v2, v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 166
    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->conformed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 167
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/QueueConformer;->finish()V

    return-void

    .line 171
    :cond_1
    iget v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueSize:I

    invoke-direct {p0, v1, v0}, Lcom/squareup/queue/redundant/QueueConformer;->logMismatch(II)V

    .line 172
    iget v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueSize:I

    invoke-direct {p0, v1, v0}, Lcom/squareup/queue/redundant/QueueConformer;->clearSqliteQueueAndCopyItemsFromTapeQueue(II)V

    return-void
.end method

.method conformed()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 258
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->conformed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method finish()V
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueListener:Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueue:Lcom/squareup/tape/ObjectQueue;

    invoke-virtual {v0, v1}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->startListeningTo(Lcom/squareup/tape/ObjectQueue;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->conformed:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$clearSqliteQueueAndCopyItemsFromTapeQueue$1$QueueConformer(IILjava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 177
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-direct {p0, p3, p1}, Lcom/squareup/queue/redundant/QueueConformer;->verifyRemovedAll(II)V

    if-lez p2, :cond_0

    .line 181
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueue:Lcom/squareup/tape/ObjectQueue;

    invoke-interface {p1, p0}, Lcom/squareup/tape/ObjectQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void

    .line 185
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/QueueConformer;->finish()V

    return-void
.end method

.method public synthetic lambda$clearSqliteQueueAndFinish$0$QueueConformer(ILjava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 152
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-direct {p0, p2, p1}, Lcom/squareup/queue/redundant/QueueConformer;->verifyRemovedAll(II)V

    .line 153
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/QueueConformer;->finish()V

    return-void
.end method

.method public synthetic lambda$onExitScope$2$QueueConformer()V
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueListener:Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueue:Lcom/squareup/tape/ObjectQueue;

    invoke-virtual {v0, v1}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->stopListeningTo(Lcom/squareup/tape/ObjectQueue;)V

    return-void
.end method

.method public final onAdd(Lcom/squareup/tape/ObjectQueue;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 194
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->sqliteQueue:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    .line 195
    invoke-interface {v0, p2}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->add(Ljava/lang/Object;)Lio/reactivex/Completable;

    move-result-object p2

    new-instance v0, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformer$vZljNiVBZTeoDE5k9SvW2W7OqW4;

    invoke-direct {v0, p0}, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformer$vZljNiVBZTeoDE5k9SvW2W7OqW4;-><init>(Lcom/squareup/queue/redundant/QueueConformer;)V

    invoke-virtual {p2, v0}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object p2

    .line 194
    invoke-virtual {p1, p2}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 115
    invoke-direct {p0}, Lcom/squareup/queue/redundant/QueueConformer;->clearSqliteQueueBecauseTapeIsCorrupt()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 119
    :cond_0
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueListener:Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->setEnteringScope(Z)V

    .line 120
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer;->queueExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v0, Lcom/squareup/queue/redundant/-$$Lambda$zrxOFfl8bMVz85HEa4Lc80YNdO8;

    invoke-direct {v0, p0}, Lcom/squareup/queue/redundant/-$$Lambda$zrxOFfl8bMVz85HEa4Lc80YNdO8;-><init>(Lcom/squareup/queue/redundant/QueueConformer;)V

    invoke-interface {p1, v0}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    .line 121
    iget-object p1, p0, Lcom/squareup/queue/redundant/QueueConformer;->tapeQueueListener:Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;->setEnteringScope(Z)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->queueExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformer$dFW5z9qYbC-1QuInOpbbc9rG0qc;

    invoke-direct {v1, p0}, Lcom/squareup/queue/redundant/-$$Lambda$QueueConformer$dFW5z9qYbC-1QuInOpbbc9rG0qc;-><init>(Lcom/squareup/queue/redundant/QueueConformer;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    .line 242
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->addDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 243
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->removeAllDisposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    .line 244
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformer;->deferredFinish:Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;

    invoke-virtual {v0}, Lcom/squareup/queue/redundant/QueueConformer$DeferredFinish;->cancel()V

    return-void
.end method

.method public onRemove(Lcom/squareup/tape/ObjectQueue;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;)V"
        }
    .end annotation

    .line 211
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    .line 213
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-interface {p1}, Lcom/squareup/tape/ObjectQueue;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const-string p1, "%s should never be listening to Tape queue (%s, size = %d) while entries are removed"

    .line 211
    invoke-static {v1, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
