.class public final Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;
.super Lcom/squareup/queue/QueueModuleRpcThreadTask;
.source "CashDrawerShiftUpdate.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/QueueModuleRpcThreadTask<",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCashDrawerShiftUpdate.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CashDrawerShiftUpdate.kt\ncom/squareup/queue/cashmanagement/CashDrawerShiftUpdate\n*L\n1#1,75:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\u000f\u0008\u0012\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u000f\u001a\u00020\u0002H\u0014J\u0006\u0010\u0010\u001a\u00020\u0000J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0014J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u0019H\u0016R\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000b\u0010\u000c\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;",
        "Lcom/squareup/queue/QueueModuleRpcThreadTask;",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;",
        "shift",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
        "(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V",
        "request",
        "Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;",
        "(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)V",
        "cashManagementService",
        "Lcom/squareup/server/cashmanagement/CashManagementService;",
        "request$annotations",
        "()V",
        "getRequest",
        "()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;",
        "callOnRpcThread",
        "createForTest",
        "handleResponseOnMainThread",
        "Lcom/squareup/server/SimpleResponse;",
        "response",
        "inject",
        "",
        "component",
        "Lcom/squareup/queue/QueueModule$Component;",
        "secureCopyWithoutPIIForLogs",
        "",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public transient cashManagementService:Lcom/squareup/server/cashmanagement/CashManagementService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 2

    const-string v0, "shift"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/queue/QueueModuleRpcThreadTask;-><init>()V

    new-instance v0, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;-><init>()V

    .line 25
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 26
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 27
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 28
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->cash_drawer_shift_description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 29
    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object v0

    .line 32
    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    const-string v1, "shift.events"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->lastOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->events(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    move-result-object p1

    const-string v0, "UpdateCashDrawerShiftReq\u2026Null()))\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/queue/QueueModuleRpcThreadTask;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    return-void
.end method

.method public static synthetic request$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->cashManagementService:Lcom/squareup/server/cashmanagement/CashManagementService;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    invoke-interface {v0, v1}, Lcom/squareup/server/cashmanagement/CashManagementService;->updateDrawer(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    move-result-object v0

    const-string v1, "cashManagementService!!.updateDrawer(request)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->callOnRpcThread()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    move-result-object v0

    return-object v0
.end method

.method public final createForTest()Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;
    .locals 3

    .line 68
    new-instance v0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;

    .line 69
    iget-object v1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object v1

    const-string v2, "update_cash_drawer_shift_request_uuid"

    .line 70
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    move-result-object v1

    const-string v2, "request.newBuilder()\n   \u2026id\")\n            .build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-direct {v0, v1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;-><init>(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;)V

    return-object v0
.end method

.method public final getRequest()Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;->status:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;->UPDATED:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse$Status;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {v0, p1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method public bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->handleResponseOnMainThread(Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 15

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    .line 52
    iget-object v3, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    iget-object v3, v3, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "client_unique_key="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v4

    :goto_0
    const/4 v5, 0x0

    aput-object v3, v2, v5

    .line 53
    iget-object v3, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    iget-object v3, v3, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    if-eqz v3, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "merchant_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, v4

    :goto_1
    aput-object v3, v2, v0

    const/4 v0, 0x2

    .line 54
    iget-object v3, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    iget-object v3, v3, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v3, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "client_cash_drawer_shift_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_2
    move-object v3, v4

    :goto_2
    aput-object v3, v2, v0

    const/4 v0, 0x3

    .line 55
    iget-object v3, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    iget-object v3, v3, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v3, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "expected_cash_money="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_3
    move-object v3, v4

    :goto_3
    aput-object v3, v2, v0

    const/4 v0, 0x4

    .line 56
    iget-object v3, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;->request:Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;

    iget-object v3, v3, Lcom/squareup/protos/client/cashdrawers/UpdateCashDrawerShiftRequest;->events:Ljava/util/List;

    if-eqz v3, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "number_of_events="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_4
    aput-object v4, v2, v0

    .line 51
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 58
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->filterNotNull(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/Iterable;

    const-string v0, ", "

    .line 59
    move-object v7, v0

    check-cast v7, Ljava/lang/CharSequence;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x3e

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    .line 50
    array-length v0, v1

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    const-string v1, "UpdateCashDrawerShiftRequest{%s}"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.String.format(this, *args)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
