.class public interface abstract Lcom/squareup/queue/QueueModule$Component;
.super Ljava/lang/Object;
.source "QueueModule.java"

# interfaces
.implements Lcom/squareup/queue/TransactionTasksComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/queue/UpdateProfileImage;)V
.end method

.method public abstract inject(Lcom/squareup/queue/UploadItemizationPhoto;)V
.end method

.method public abstract inject(Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;)V
.end method

.method public abstract inject(Lcom/squareup/queue/cashmanagement/CashDrawerShiftCreate;)V
.end method

.method public abstract inject(Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;)V
.end method

.method public abstract inject(Lcom/squareup/queue/cashmanagement/CashDrawerShiftEnd;)V
.end method

.method public abstract inject(Lcom/squareup/queue/cashmanagement/CashDrawerShiftUpdate;)V
.end method

.method public abstract inject(Lcom/squareup/queue/crm/AccumulateStatusViaEmailTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/crm/EmailCollectionTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/crm/ReturnCouponTask;)V
.end method

.method public abstract inject(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;)V
.end method

.method public abstract inject(Lcom/squareup/ui/library/UploadItemBitmapTask;)V
.end method
