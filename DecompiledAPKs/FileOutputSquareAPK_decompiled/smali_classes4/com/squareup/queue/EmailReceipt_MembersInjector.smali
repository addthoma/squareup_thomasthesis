.class public final Lcom/squareup/queue/EmailReceipt_MembersInjector;
.super Ljava/lang/Object;
.source "EmailReceipt_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/EmailReceipt;",
        ">;"
    }
.end annotation


# instance fields
.field private final paymentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt_MembersInjector;->paymentServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/EmailReceipt;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/queue/EmailReceipt_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/queue/EmailReceipt_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPaymentService(Lcom/squareup/queue/EmailReceipt;Lcom/squareup/server/payment/PaymentService;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt;->paymentService:Lcom/squareup/server/payment/PaymentService;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/EmailReceipt;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/queue/EmailReceipt_MembersInjector;->paymentServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/PaymentService;

    invoke-static {p1, v0}, Lcom/squareup/queue/EmailReceipt_MembersInjector;->injectPaymentService(Lcom/squareup/queue/EmailReceipt;Lcom/squareup/server/payment/PaymentService;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/queue/EmailReceipt;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EmailReceipt_MembersInjector;->injectMembers(Lcom/squareup/queue/EmailReceipt;)V

    return-void
.end method
