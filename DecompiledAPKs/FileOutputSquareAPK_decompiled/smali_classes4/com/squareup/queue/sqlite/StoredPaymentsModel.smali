.class public interface abstract Lcom/squareup/queue/sqlite/StoredPaymentsModel;
.super Ljava/lang/Object;
.source "StoredPaymentsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteAllEntries;,
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteFirstEntry;,
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$InsertEntry;,
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;,
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;,
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator;,
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper;,
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;,
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;
    }
.end annotation


# static fields
.field public static final CREATE_TABLE:Ljava/lang/String; = "CREATE TABLE stored_payments (\n  -- Alias for ROWID.\n  _id INTEGER PRIMARY KEY,\n\n  -- Identifier associated with the stored payment represented by this entry. Possibly non-unique.\n  entry_id TEXT NOT NULL,\n\n  -- Timestamp of the stored payment, in milliseconds since epoch.\n  timestamp_ms INTEGER NOT NULL CHECK (timestamp_ms >= 0),\n\n  -- Binary representation of the stored payment.\n  data BLOB NOT NULL\n)"

.field public static final DATA:Ljava/lang/String; = "data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ENTRY_ID:Ljava/lang/String; = "entry_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TABLE_NAME:Ljava/lang/String; = "stored_payments"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TIMESTAMP_MS:Ljava/lang/String; = "timestamp_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final _ID:Ljava/lang/String; = "_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# virtual methods
.method public abstract _id()Ljava/lang/Long;
.end method

.method public abstract data()[B
.end method

.method public abstract entry_id()Ljava/lang/String;
.end method

.method public abstract timestamp_ms()J
.end method
