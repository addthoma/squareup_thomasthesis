.class public final Lcom/squareup/queue/sqlite/TasksModel$Factory;
.super Ljava/lang/Object;
.source "TasksModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/TasksModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/TasksModel$Factory$GetEntryQuery;,
        Lcom/squareup/queue/sqlite/TasksModel$Factory$RipenedLocalPaymentsCountQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/TasksModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final creator:Lcom/squareup/queue/sqlite/TasksModel$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/TasksModel$Creator<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/TasksModel$Creator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/TasksModel$Creator<",
            "TT;>;)V"
        }
    .end annotation

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/squareup/queue/sqlite/TasksModel$Factory;->creator:Lcom/squareup/queue/sqlite/TasksModel$Creator;

    return-void
.end method


# virtual methods
.method public allEntries()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 149
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "tasks"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM tasks\nORDER BY timestamp_ms DESC"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public allEntriesMapper()Lcom/squareup/queue/sqlite/TasksModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/TasksModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 200
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/TasksModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;)V

    return-object v0
.end method

.method public allLocalPaymentEntries()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 139
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "tasks"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM tasks\nWHERE is_local_payment = 1\nORDER BY timestamp_ms DESC"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public allLocalPaymentEntriesMapper()Lcom/squareup/queue/sqlite/TasksModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/TasksModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 195
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/TasksModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;)V

    return-object v0
.end method

.method public count()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 103
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "tasks"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT COUNT(*)\nFROM tasks"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public countMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 157
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Factory$1;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/TasksModel$Factory$1;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;)V

    return-object v0
.end method

.method public firstEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 125
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "tasks"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM tasks\nWHERE _id = ( SELECT MIN(_id) FROM tasks )"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public firstEntryMapper()Lcom/squareup/queue/sqlite/TasksModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/TasksModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 185
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/TasksModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;)V

    return-object v0
.end method

.method public getEntry(Ljava/lang/String;)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 1

    .line 134
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Factory$GetEntryQuery;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/TasksModel$Factory$GetEntryQuery;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEntryMapper()Lcom/squareup/queue/sqlite/TasksModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/TasksModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 190
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/TasksModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;)V

    return-object v0
.end method

.method public localPaymentsCount()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 111
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "tasks"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT COUNT(*)\nFROM tasks\nWHERE is_local_payment = 1"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public localPaymentsCountMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 166
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Factory$2;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/TasksModel$Factory$2;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;)V

    return-object v0
.end method

.method public ripenedLocalPaymentsCount(J)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 1

    .line 120
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Factory$RipenedLocalPaymentsCountQuery;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/sqlite/TasksModel$Factory$RipenedLocalPaymentsCountQuery;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;J)V

    return-object v0
.end method

.method public ripenedLocalPaymentsCountMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 175
    new-instance v0, Lcom/squareup/queue/sqlite/TasksModel$Factory$3;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/TasksModel$Factory$3;-><init>(Lcom/squareup/queue/sqlite/TasksModel$Factory;)V

    return-object v0
.end method
