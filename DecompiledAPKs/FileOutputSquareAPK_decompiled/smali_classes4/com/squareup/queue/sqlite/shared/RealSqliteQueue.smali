.class public final Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;
.super Ljava/lang/Object;
.source "RealSqliteQueue.kt"

# interfaces
.implements Lcom/squareup/queue/sqlite/shared/SqliteQueue;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "Q:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
        "TQ;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00030\u0004B5\u0008\u0007\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\u0015\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u000fJ\u0014\u0010\u0010\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00120\u0011H\u0016J\u0008\u0010\u0013\u001a\u00020\rH\u0016J\u001c\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0014\u0010\u0019\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00160\u0011H\u0016J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u0015H\u0016J\u000e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u0015H\u0016J\u000e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u0011H\u0016R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;",
        "S",
        "",
        "Q",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueue;",
        "store",
        "Lcom/squareup/queue/sqlite/SqliteQueueStore;",
        "converter",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V",
        "add",
        "Lio/reactivex/Completable;",
        "entry",
        "(Ljava/lang/Object;)Lio/reactivex/Completable;",
        "allEntries",
        "Lio/reactivex/Observable;",
        "",
        "close",
        "fetchEntry",
        "Lio/reactivex/Single;",
        "Lcom/squareup/util/Optional;",
        "entryId",
        "",
        "peekFirst",
        "removeAll",
        "",
        "removeFirst",
        "size",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter<",
            "TS;TQ;>;"
        }
    .end annotation
.end field

.field private final store:Lcom/squareup/queue/sqlite/SqliteQueueStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/SqliteQueueStore<",
            "TS;>;"
        }
    .end annotation
.end field

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/SqliteQueueStore<",
            "TS;>;",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter<",
            "TS;TQ;>;)V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/SqliteQueueStore<",
            "TS;>;",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter<",
            "TS;TQ;>;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")V"
        }
    .end annotation

    const-string v0, "store"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "converter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    iput-object p2, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->converter:Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;

    iput-object p3, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 22
    check-cast p3, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-void
.end method

.method public static final synthetic access$getConverter$p(Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;)Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->converter:Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;

    return-object p0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Lio/reactivex/Completable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    .line 33
    iget-object v1, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->converter:Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;

    invoke-interface {v1, p1}, Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;->toStoreEntry(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/queue/sqlite/SqliteQueueStore;->insert(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lio/reactivex/Single;->ignoreElement()Lio/reactivex/Completable;

    move-result-object p1

    const-string v0, "store\n        .insert(co\u2026\n        .ignoreElement()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public allEntries()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "TQ;>;>;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/SqliteQueueStore;->allEntries()Lio/reactivex/Observable;

    move-result-object v0

    .line 69
    new-instance v1, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$allEntries$1;

    invoke-direct {v1, p0}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$allEntries$1;-><init>(Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "store.allEntries()\n     \u2026rter.toQueueEntries(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public close()Lio/reactivex/Completable;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/SqliteQueueStore;->close()Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method public fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "TQ;>;>;"
        }
    .end annotation

    const-string v0, "entryId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    .line 47
    invoke-interface {v0, p1}, Lcom/squareup/queue/sqlite/SqliteQueueStore;->fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 48
    new-instance v0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$fetchEntry$1;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$fetchEntry$1;-><init>(Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "store\n        .fetchEntr\u2026onverter::toQueueEntry) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public peekFirst()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "TQ;>;>;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    .line 40
    invoke-interface {v0}, Lcom/squareup/queue/sqlite/SqliteQueueStore;->firstEntry()Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    new-instance v1, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$peekFirst$1;

    invoke-direct {v1, p0}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue$peekFirst$1;-><init>(Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "store\n        .firstEntr\u2026onverter::toQueueEntry) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public removeAll()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/SqliteQueueStore;->deleteAllEntries()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public removeFirst()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/SqliteQueueStore;->deleteFirstEntry()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public size()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;->store:Lcom/squareup/queue/sqlite/SqliteQueueStore;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/SqliteQueueStore;->count()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
