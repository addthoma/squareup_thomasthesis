.class public interface abstract Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;
.super Ljava/lang/Object;
.source "StoredPaymentsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AllDistinctEntriesModel"
.end annotation


# virtual methods
.method public abstract _id()Ljava/lang/Long;
.end method

.method public abstract data()[B
.end method

.method public abstract entry_id()Ljava/lang/String;
.end method

.method public abstract timestamp_ms()J
.end method
