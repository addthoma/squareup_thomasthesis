.class public Lcom/squareup/queue/sqlite/TasksSqliteQueue;
.super Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
.source "TasksSqliteQueue.java"

# interfaces
.implements Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;",
        "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;"
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/queue/sqlite/TasksConverter;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final store:Lcom/squareup/queue/sqlite/TasksSqliteStore;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/sqlite/TasksSqliteStore;Lcom/squareup/queue/sqlite/TasksConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p4    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/queue/sqlite/TasksSqliteStore;",
            "Lcom/squareup/queue/sqlite/TasksConverter;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")V"
        }
    .end annotation

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;)V

    .line 40
    iput-object p2, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->store:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    .line 41
    iput-object p3, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->converter:Lcom/squareup/queue/sqlite/TasksConverter;

    .line 42
    iput-object p4, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/queue/retrofit/RetrofitTask;)Lio/reactivex/Completable;
    .locals 0

    .line 46
    invoke-super {p0, p1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->add(Ljava/lang/Object;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Lio/reactivex/Completable;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public allLocalPaymentsAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 61
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->localPaymentsCount()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->store:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    .line 63
    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->allLocalPaymentEntriesAsStream()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;

    iget-object v3, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->converter:Lcom/squareup/queue/sqlite/TasksConverter;

    invoke-direct {v2, p1, v3}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    .line 62
    invoke-static {v1, v2}, Lcom/squareup/queue/sqlite/SqliteQueues;->convertStreamAndBuffer(Lrx/Observable;Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public fetchTransaction(Lcom/squareup/util/Res;Ljava/lang/String;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 71
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->store:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    invoke-virtual {v0, p2}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/queue/sqlite/-$$Lambda$TasksSqliteQueue$IHJt2YdrmjVS75KZmuuLCYqX8VE;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/-$$Lambda$TasksSqliteQueue$IHJt2YdrmjVS75KZmuuLCYqX8VE;-><init>(Lcom/squareup/queue/sqlite/TasksSqliteQueue;Lcom/squareup/util/Res;)V

    .line 72
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 71
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$fetchTransaction$0$TasksSqliteQueue(Lcom/squareup/util/Res;Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 72
    new-instance v0, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->converter:Lcom/squareup/queue/sqlite/TasksConverter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p1, Lcom/squareup/queue/sqlite/-$$Lambda$yjgIX6fsKbh8wcKssNUjUEBilAY;

    invoke-direct {p1, v0}, Lcom/squareup/queue/sqlite/-$$Lambda$yjgIX6fsKbh8wcKssNUjUEBilAY;-><init>(Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;)V

    invoke-virtual {p2, p1}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public localPaymentsCount()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 51
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->store:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->localPaymentsCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public ripenedLocalPaymentsCount()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 56
    iget-object v0, p0, Lcom/squareup/queue/sqlite/TasksSqliteQueue;->store:Lcom/squareup/queue/sqlite/TasksSqliteStore;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->ripenedLocalPaymentsCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
