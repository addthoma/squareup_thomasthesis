.class final Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;
.super Ljava/lang/Object;
.source "TasksEntry.kt"

# interfaces
.implements Lcom/squareup/queue/sqlite/TasksModel$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/TasksEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/TasksModel;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/sqlite/TasksModel$Creator<",
        "Lcom/squareup/queue/sqlite/TasksEntry;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0012\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0004\u0008\n\u0010\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/queue/sqlite/TasksEntry;",
        "_id",
        "",
        "entry_id",
        "",
        "timestamp_ms",
        "is_local_payment",
        "data",
        "",
        "create",
        "(Ljava/lang/Long;Ljava/lang/String;JJ[B)Lcom/squareup/queue/sqlite/TasksEntry;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;

    invoke-direct {v0}, Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;-><init>()V

    sput-object v0, Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;->INSTANCE:Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Long;Ljava/lang/String;JJ[B)Lcom/squareup/queue/sqlite/TasksEntry;
    .locals 10

    const-string v0, "entry_id"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v0, Lcom/squareup/queue/sqlite/TasksEntry;

    const/4 v9, 0x0

    move-object v1, v0

    move-object v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v9}, Lcom/squareup/queue/sqlite/TasksEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;JJ[BLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public bridge synthetic create(Ljava/lang/Long;Ljava/lang/String;JJ[B)Lcom/squareup/queue/sqlite/TasksModel;
    .locals 0

    .line 65
    invoke-virtual/range {p0 .. p7}, Lcom/squareup/queue/sqlite/TasksEntry$Companion$FACTORY$1;->create(Ljava/lang/Long;Ljava/lang/String;JJ[B)Lcom/squareup/queue/sqlite/TasksEntry;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/sqlite/TasksModel;

    return-object p1
.end method
