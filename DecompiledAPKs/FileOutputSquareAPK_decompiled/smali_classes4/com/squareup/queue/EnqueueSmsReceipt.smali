.class public Lcom/squareup/queue/EnqueueSmsReceipt;
.super Lcom/squareup/queue/PostPaymentTask;
.source "EnqueueSmsReceipt.java"


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final phone:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/EnqueueSmsReceipt;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/queue/PostPaymentTask;-><init>()V

    .line 18
    iget-object p1, p1, Lcom/squareup/queue/EnqueueSmsReceipt;->phone:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string p1, "REDACTED_phone"

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/queue/EnqueueSmsReceipt;->phone:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/queue/PostPaymentTask;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/queue/EnqueueSmsReceipt;->phone:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPhone()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/queue/EnqueueSmsReceipt;->phone:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 43
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/EnqueueSmsReceipt;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EnqueueSmsReceipt;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/queue/EnqueueSmsReceipt;

    invoke-direct {v0, p0}, Lcom/squareup/queue/EnqueueSmsReceipt;-><init>(Lcom/squareup/queue/EnqueueSmsReceipt;)V

    return-object v0
.end method

.method protected taskFor(Ljava/lang/String;)Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/queue/SmsReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/SmsReceipt$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/EnqueueSmsReceipt;->paymentType:Lcom/squareup/PaymentType;

    .line 23
    invoke-virtual {v0, p1, v1}, Lcom/squareup/queue/SmsReceipt$Builder;->paymentIdOrLegacyBillId(Ljava/lang/String;Lcom/squareup/PaymentType;)Lcom/squareup/queue/SmsReceipt$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/EnqueueSmsReceipt;->phone:Ljava/lang/String;

    .line 24
    invoke-virtual {p1, v0}, Lcom/squareup/queue/SmsReceipt$Builder;->phone(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;

    move-result-object p1

    .line 25
    invoke-virtual {p1}, Lcom/squareup/queue/SmsReceipt$Builder;->build()Lcom/squareup/queue/SmsReceipt;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnqueueSmsReceipt{phone=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/EnqueueSmsReceipt;->phone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
