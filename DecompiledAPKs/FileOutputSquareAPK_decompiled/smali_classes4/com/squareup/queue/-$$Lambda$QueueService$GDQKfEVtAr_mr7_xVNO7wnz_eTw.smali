.class public final synthetic Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Action;


# instance fields
.field private final synthetic f$0:Lcom/squareup/queue/QueueService;

.field private final synthetic f$1:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final synthetic f$2:Lcom/squareup/queue/QueueService$TaskCallback;

.field private final synthetic f$3:Lmortar/MortarScope;

.field private final synthetic f$4:Lcom/squareup/queue/retrofit/RetrofitTask;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/QueueService$TaskCallback;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$0:Lcom/squareup/queue/QueueService;

    iput-object p2, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$1:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iput-object p3, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$2:Lcom/squareup/queue/QueueService$TaskCallback;

    iput-object p4, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$3:Lmortar/MortarScope;

    iput-object p5, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$4:Lcom/squareup/queue/retrofit/RetrofitTask;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$0:Lcom/squareup/queue/QueueService;

    iget-object v1, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$1:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v2, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$2:Lcom/squareup/queue/QueueService$TaskCallback;

    iget-object v3, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$3:Lmortar/MortarScope;

    iget-object v4, p0, Lcom/squareup/queue/-$$Lambda$QueueService$GDQKfEVtAr_mr7_xVNO7wnz_eTw;->f$4:Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/queue/QueueService;->lambda$startTaskFrom$0$QueueService(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/QueueService$TaskCallback;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method
