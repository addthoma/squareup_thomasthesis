.class public Lcom/squareup/queue/EnqueueSmsReceiptById;
.super Lcom/squareup/queue/PostPaymentTask;
.source "EnqueueSmsReceiptById.java"


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final phoneId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/queue/PostPaymentTask;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/queue/EnqueueSmsReceiptById;->phoneId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPhoneId()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/queue/EnqueueSmsReceiptById;->phoneId:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 38
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/EnqueueSmsReceiptById;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 7
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EnqueueSmsReceiptById;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method protected taskFor(Ljava/lang/String;)Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 2

    .line 17
    new-instance v0, Lcom/squareup/queue/SmsReceiptById$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/SmsReceiptById$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/EnqueueSmsReceiptById;->paymentType:Lcom/squareup/PaymentType;

    .line 18
    invoke-virtual {v0, p1, v1}, Lcom/squareup/queue/SmsReceiptById$Builder;->paymentIdOrLegacyBillId(Ljava/lang/String;Lcom/squareup/PaymentType;)Lcom/squareup/queue/SmsReceiptById$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/EnqueueSmsReceiptById;->phoneId:Ljava/lang/String;

    .line 19
    invoke-virtual {p1, v0}, Lcom/squareup/queue/SmsReceiptById$Builder;->phoneId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceiptById$Builder;

    move-result-object p1

    .line 20
    invoke-virtual {p1}, Lcom/squareup/queue/SmsReceiptById$Builder;->build()Lcom/squareup/queue/SmsReceiptById;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnqueueSmsReceiptById{phoneId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/EnqueueSmsReceiptById;->phoneId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
