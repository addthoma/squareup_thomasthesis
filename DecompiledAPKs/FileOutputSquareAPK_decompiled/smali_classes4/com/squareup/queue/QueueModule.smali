.class public Lcom/squareup/queue/QueueModule;
.super Ljava/lang/Object;
.source "QueueModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/QueueModule$Component;,
        Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$localPaymentsSqliteQueueFactory$2(Landroid/app/Application;Lcom/squareup/util/Clock;Lio/reactivex/Scheduler;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/io/File;)Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
    .locals 1

    .line 414
    invoke-virtual {p5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p5

    const-string v0, "Parent directory of Tape queue file"

    invoke-static {p5, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/io/File;

    .line 416
    invoke-static {p0, p1, p5, p2}, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;->create(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;

    move-result-object p0

    .line 417
    new-instance p1, Lcom/squareup/queue/sqlite/LocalPaymentConverter;

    invoke-direct {p1, p3}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;-><init>(Lcom/squareup/tape/FileObjectQueue$Converter;)V

    .line 418
    new-instance p2, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;

    invoke-direct {p2, p0, p1, p4}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    .line 420
    new-instance p3, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;

    invoke-direct {p3, p2, p0, p1, p4}, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;Lcom/squareup/queue/sqlite/LocalPaymentConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object p3
.end method

.method static synthetic lambda$pendingCapturesSqliteQueueFactory$0(Landroid/app/Application;Lcom/squareup/util/Clock;Lio/reactivex/Scheduler;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/io/File;)Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
    .locals 1

    .line 385
    invoke-virtual {p5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p5

    const-string v0, "Parent directory of Tape queue file"

    invoke-static {p5, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/io/File;

    .line 387
    invoke-static {p0, p1, p5, p2}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->create(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    move-result-object p0

    .line 388
    new-instance p1, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;

    invoke-direct {p1, p3}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;-><init>(Lcom/squareup/tape/FileObjectQueue$Converter;)V

    .line 389
    new-instance p2, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;

    invoke-direct {p2, p0, p1, p4}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    .line 391
    new-instance p1, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;

    invoke-direct {p1, p2, p0, p3, p4}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object p1
.end method

.method static synthetic lambda$tasksSqliteQueueFactory$1(Landroid/app/Application;Lcom/squareup/util/Clock;Lio/reactivex/Scheduler;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/io/File;)Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
    .locals 1

    .line 400
    invoke-virtual {p5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p5

    const-string v0, "Parent directory of Tape queue file"

    invoke-static {p5, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/io/File;

    .line 401
    invoke-static {p0, p1, p5, p2}, Lcom/squareup/queue/sqlite/TasksSqliteStore;->create(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/TasksSqliteStore;

    move-result-object p0

    .line 402
    new-instance p1, Lcom/squareup/queue/sqlite/TasksConverter;

    invoke-direct {p1, p3}, Lcom/squareup/queue/sqlite/TasksConverter;-><init>(Lcom/squareup/tape/FileObjectQueue$Converter;)V

    .line 403
    new-instance p2, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;

    invoke-direct {p2, p0, p1, p4}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    .line 405
    new-instance p3, Lcom/squareup/queue/sqlite/TasksSqliteQueue;

    invoke-direct {p3, p2, p0, p1, p4}, Lcom/squareup/queue/sqlite/TasksSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/sqlite/TasksSqliteStore;Lcom/squareup/queue/sqlite/TasksConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object p3
.end method

.method private static localPaymentsSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lrx/functions/Func1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lrx/functions/Func1<",
            "Ljava/io/File;",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation

    .line 412
    new-instance v6, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;-><init>(Landroid/app/Application;Lcom/squareup/util/Clock;Lio/reactivex/Scheduler;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v6
.end method

.method private static pendingCapturesSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lrx/functions/Func1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lrx/functions/Func1<",
            "Ljava/io/File;",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation

    .line 383
    new-instance v6, Lcom/squareup/queue/-$$Lambda$QueueModule$ShqErlTL4cZeV22ViBly0Pif3Jk;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/-$$Lambda$QueueModule$ShqErlTL4cZeV22ViBly0Pif3Jk;-><init>(Landroid/app/Application;Lcom/squareup/util/Clock;Lio/reactivex/Scheduler;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v6
.end method

.method static provideLocalMonitorSqliteQueue(Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/queue/sqlite/MonitorSqliteQueue;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 213
    check-cast p0, Lcom/squareup/queue/sqlite/MonitorSqliteQueue;

    return-object p0
.end method

.method static provideLocalPaymentsMonitor(Lcom/squareup/queue/sqlite/MonitorSqliteQueue;)Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 223
    invoke-interface {p0}, Lcom/squareup/queue/sqlite/MonitorSqliteQueue;->getSqliteQueueMonitor()Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    return-object p0
.end method

.method static provideLocalPaymentsQueue(Lcom/squareup/settings/server/Features;Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljava/io/File;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ")",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;"
        }
    .end annotation

    .line 183
    new-instance v0, Ljava/io/File;

    const-string v1, "local-payment-tasks.db"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 184
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p0, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 185
    invoke-virtual {p2, v0}, Lcom/squareup/queue/retrofit/QueueCache;->getOrOpen(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0

    .line 187
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 190
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_1
    return-object p3
.end method

.method static provideLocalPaymentsQueueListener(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    .locals 1
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 256
    new-instance v0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;-><init>(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v0
.end method

.method static provideLocalPaymentsSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;
    .locals 0
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/LegacyMainScheduler;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ")",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;"
        }
    .end annotation

    .line 334
    invoke-static {p0, p1, p3, p4, p5}, Lcom/squareup/queue/QueueModule;->localPaymentsSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lrx/functions/Func1;

    move-result-object p0

    .line 345
    new-instance p1, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;

    invoke-direct {p1, p0, p2, p3, p6}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;-><init>(Lrx/functions/Func1;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/queue/QueueServiceStarter;)V

    return-object p1
.end method

.method static provideLoggedInPendingCapturesQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/CorruptQueueHelper;)Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;
    .locals 0
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/LegacyMainScheduler;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ")",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;"
        }
    .end annotation

    .line 301
    invoke-static {p0, p1, p3, p4, p5}, Lcom/squareup/queue/QueueModule;->pendingCapturesSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lrx/functions/Func1;

    move-result-object p0

    .line 303
    new-instance p1, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;

    invoke-direct {p1, p0, p2, p3, p6}, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;-><init>(Lrx/functions/Func1;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/queue/CorruptQueueHelper;)V

    return-object p1
.end method

.method static provideLoggedInQueuesEmpty(Lcom/squareup/queue/retrofit/RetrofitQueue;Ljavax/inject/Provider;)Z
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)Z"
        }
    .end annotation

    .line 364
    invoke-interface {p0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->size()I

    move-result p0

    if-nez p0, :cond_0

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {p0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->size()I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static provideLoggedInSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;
    .locals 0
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/LegacyMainScheduler;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ")",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;"
        }
    .end annotation

    .line 314
    invoke-static {p0, p1, p3, p4, p5}, Lcom/squareup/queue/QueueModule;->tasksSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lrx/functions/Func1;

    move-result-object p0

    .line 324
    new-instance p1, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;

    invoke-direct {p1, p0, p2, p3, p6}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;-><init>(Lrx/functions/Func1;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/queue/QueueServiceStarter;)V

    return-object p1
.end method

.method static provideLoggedInTaskWatcher(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Landroid/content/SharedPreferences;Lcom/google/gson/Gson;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)Lcom/squareup/queue/TaskWatcher;
    .locals 9
    .param p0    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/gson/Gson;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lio/reactivex/Scheduler;",
            ")",
            "Lcom/squareup/queue/TaskWatcher;"
        }
    .end annotation

    .line 373
    new-instance v3, Lcom/squareup/settings/StringLocalSetting;

    const-string v0, "last-task"

    invoke-direct {v3, p2, v0}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 374
    new-instance v4, Lcom/squareup/settings/IntegerLocalSetting;

    const-string v0, "last-task-repeated-count"

    invoke-direct {v4, p2, v0}, Lcom/squareup/settings/IntegerLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 376
    new-instance p2, Lcom/squareup/queue/TaskWatcher;

    move-object v0, p2

    move-object v1, p0

    move-object v2, p1

    move-object v5, p4

    move-object v6, p3

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/squareup/queue/TaskWatcher;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/f2prateek/rx/preferences2/Preference;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)V

    return-object p2
.end method

.method static provideLoggedInTasksQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/CorruptQueueHelper;)Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;
    .locals 0
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/LegacyMainScheduler;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ")",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;"
        }
    .end annotation

    .line 355
    invoke-static {p0, p1, p3, p4, p5}, Lcom/squareup/queue/QueueModule;->tasksSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lrx/functions/Func1;

    move-result-object p0

    .line 356
    new-instance p1, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;

    invoke-direct {p1, p0, p2, p3, p6}, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;-><init>(Lrx/functions/Func1;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/queue/CorruptQueueHelper;)V

    return-object p1
.end method

.method static provideMaybeSqliteTasksQueue(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/Features;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 174
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->PREFER_SQLITE_TASKS_QUEUE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->size()I

    move-result p2

    if-nez p2, :cond_0

    move-object p0, p1

    :cond_0
    return-object p0
.end method

.method static providePendingCapturesMonitor(Lcom/squareup/queue/redundant/RedundantRetrofitQueue;)Lcom/squareup/queue/sqlite/PendingCapturesMonitor;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 142
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->getSqliteQueueMonitor()Lcom/squareup/queue/sqlite/shared/SqliteQueueMonitor;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    return-object p0
.end method

.method static providePendingCapturesQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;"
        }
    .end annotation

    .line 198
    new-instance v0, Ljava/io/File;

    const-string v1, "pending-captures"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 199
    invoke-virtual {p1, v0}, Lcom/squareup/queue/retrofit/QueueCache;->getOrOpen(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method

.method static providePendingCapturesQueueListener(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    .locals 1
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 250
    new-instance v0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;-><init>(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v0
.end method

.method static provideRedundantPendingCapturesQueue(Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/queue/redundant/RedundantRetrofitQueue;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 152
    check-cast p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;

    return-object p0
.end method

.method static provideRedundantRetrofitTasksQueue(Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/queue/redundant/RedundantRetrofitQueue;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 162
    check-cast p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;

    return-object p0
.end method

.method static provideRedundantStoredPaymentsQueue(Lcom/squareup/queue/StoredPaymentsQueue;)Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 127
    check-cast p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;

    return-object p0
.end method

.method static provideRedundantTasksQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;"
        }
    .end annotation

    .line 230
    new-instance v0, Ljava/io/File;

    const-string v1, "tasks"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 231
    invoke-virtual {p1, v0}, Lcom/squareup/queue/retrofit/QueueCache;->getOrOpen(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method

.method static provideSqliteQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;"
        }
    .end annotation

    .line 237
    new-instance v0, Ljava/io/File;

    const-string v1, "sqlite-tasks"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 238
    invoke-virtual {p1, v0}, Lcom/squareup/queue/retrofit/QueueCache;->getOrOpen(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method

.method static provideSqliteQueueListener(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    .locals 1
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 268
    new-instance v0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;-><init>(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v0
.end method

.method static provideStoredPaymentsMonitor(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;)Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 118
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->getSqliteQueueMonitor()Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;

    move-result-object p0

    return-object p0
.end method

.method static provideStoredPaymentsQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/StoredPaymentsQueue;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;)",
            "Lcom/squareup/queue/StoredPaymentsQueue;"
        }
    .end annotation

    .line 132
    invoke-static {p0, p1}, Lcom/squareup/queue/StoredPaymentsQueue;->getStoredPaymentsQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object p0

    return-object p0
.end method

.method static provideStoredPaymentsQueueListener(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    .locals 1
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;"
        }
    .end annotation

    .line 244
    new-instance v0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;-><init>(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v0
.end method

.method static provideTasksQueueListener(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;
    .locals 1
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueServiceStarter;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 262
    new-instance v0, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;-><init>(Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v0
.end method

.method private static tasksSqliteQueueFactory(Landroid/app/Application;Lcom/squareup/util/Clock;Lcom/squareup/tape/FileObjectQueue$Converter;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lrx/functions/Func1;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lrx/functions/Func1<",
            "Ljava/io/File;",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation

    .line 398
    new-instance v6, Lcom/squareup/queue/-$$Lambda$QueueModule$v2EdcaQDFPXTtvTQt5OmMBj-z9I;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/-$$Lambda$QueueModule$v2EdcaQDFPXTtvTQt5OmMBj-z9I;-><init>(Landroid/app/Application;Lcom/squareup/util/Clock;Lio/reactivex/Scheduler;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v6
.end method


# virtual methods
.method provideLocalPaymentsQueueCache(Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;",
            ")",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation

    .line 280
    new-instance v0, Lcom/squareup/queue/retrofit/QueueCache;

    invoke-direct {v0, p1}, Lcom/squareup/queue/retrofit/QueueCache;-><init>(Lcom/squareup/queue/retrofit/QueueFactory;)V

    return-object v0
.end method

.method providePendingCapturesQueueCache(Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;",
            ")",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation

    .line 274
    new-instance v0, Lcom/squareup/queue/retrofit/QueueCache;

    invoke-direct {v0, p1}, Lcom/squareup/queue/retrofit/QueueCache;-><init>(Lcom/squareup/queue/retrofit/QueueFactory;)V

    return-object v0
.end method

.method provideSqliteQueueCache(Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;",
            ")",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation

    .line 286
    new-instance v0, Lcom/squareup/queue/retrofit/QueueCache;

    invoke-direct {v0, p1}, Lcom/squareup/queue/retrofit/QueueCache;-><init>(Lcom/squareup/queue/retrofit/QueueFactory;)V

    return-object v0
.end method

.method provideTasksQueueCache(Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;",
            ")",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation

    .line 292
    new-instance v0, Lcom/squareup/queue/retrofit/QueueCache;

    invoke-direct {v0, p1}, Lcom/squareup/queue/retrofit/QueueCache;-><init>(Lcom/squareup/queue/retrofit/QueueFactory;)V

    return-object v0
.end method
