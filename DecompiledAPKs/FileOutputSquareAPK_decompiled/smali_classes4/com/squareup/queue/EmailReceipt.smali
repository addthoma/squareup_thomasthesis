.class public Lcom/squareup/queue/EmailReceipt;
.super Ljava/lang/Object;
.source "EmailReceipt.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/EmailReceipt$Builder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final billId:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final email:Ljava/lang/String;

.field private final paymentId:Ljava/lang/String;

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final resend:Z

.field private final uniqueKey:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/EmailReceipt$Builder;)V
    .locals 1

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    invoke-static {p1}, Lcom/squareup/queue/EmailReceipt$Builder;->access$100(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/EmailReceipt;->paymentId:Ljava/lang/String;

    .line 113
    invoke-static {p1}, Lcom/squareup/queue/EmailReceipt$Builder;->access$200(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/EmailReceipt;->billId:Ljava/lang/String;

    .line 114
    invoke-static {p1}, Lcom/squareup/queue/EmailReceipt$Builder;->access$300(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/EmailReceipt;->email:Ljava/lang/String;

    .line 115
    invoke-static {p1}, Lcom/squareup/queue/EmailReceipt$Builder;->access$400(Lcom/squareup/queue/EmailReceipt$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/queue/EmailReceipt;->resend:Z

    .line 116
    invoke-static {p1}, Lcom/squareup/queue/EmailReceipt$Builder;->access$500(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/queue/EmailReceipt$Builder;->access$500(Lcom/squareup/queue/EmailReceipt$Builder;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/queue/EmailReceipt;->uniqueKey:Ljava/lang/String;

    .line 118
    iget-object p1, p0, Lcom/squareup/queue/EmailReceipt;->paymentId:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    iget-object v0, p0, Lcom/squareup/queue/EmailReceipt;->billId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr p1, v0

    const-string v0, "Either paymentId or billId must be set."

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/queue/EmailReceipt$Builder;Lcom/squareup/queue/EmailReceipt$1;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/queue/EmailReceipt;-><init>(Lcom/squareup/queue/EmailReceipt$Builder;)V

    return-void
.end method

.method public static taskToResend(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt;
    .locals 2

    .line 41
    new-instance v0, Lcom/squareup/queue/EmailReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/EmailReceipt$Builder;-><init>()V

    const-string v1, "paymentId"

    .line 42
    invoke-static {p0, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/squareup/queue/EmailReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object p0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/queue/EmailReceipt$Builder;->email(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object p0

    const/4 p1, 0x1

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/queue/EmailReceipt$Builder;->resend(Z)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object p0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/queue/EmailReceipt$Builder;->build()Lcom/squareup/queue/EmailReceipt;

    move-result-object p0

    return-object p0
.end method

.method private toBuilder()Lcom/squareup/queue/EmailReceipt$Builder;
    .locals 2

    .line 122
    new-instance v0, Lcom/squareup/queue/EmailReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/EmailReceipt$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/EmailReceipt;->paymentId:Ljava/lang/String;

    .line 123
    invoke-virtual {v0, v1}, Lcom/squareup/queue/EmailReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/EmailReceipt;->billId:Ljava/lang/String;

    .line 124
    invoke-static {v0, v1}, Lcom/squareup/queue/EmailReceipt$Builder;->access$600(Lcom/squareup/queue/EmailReceipt$Builder;Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/EmailReceipt;->email:Ljava/lang/String;

    .line 125
    invoke-virtual {v0, v1}, Lcom/squareup/queue/EmailReceipt$Builder;->email(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/queue/EmailReceipt;->resend:Z

    .line 126
    invoke-virtual {v0, v1}, Lcom/squareup/queue/EmailReceipt$Builder;->resend(Z)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/EmailReceipt;->uniqueKey:Ljava/lang/String;

    .line 127
    invoke-virtual {v0, v1}, Lcom/squareup/queue/EmailReceipt$Builder;->uniqueKey(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/squareup/queue/EmailReceipt;->paymentService:Lcom/squareup/server/payment/PaymentService;

    iget-object v1, p0, Lcom/squareup/queue/EmailReceipt;->paymentId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/queue/EmailReceipt;->billId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/queue/EmailReceipt;->email:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/squareup/queue/EmailReceipt;->resend:Z

    iget-object v5, p0, Lcom/squareup/queue/EmailReceipt;->uniqueKey:Ljava/lang/String;

    move-object v6, p1

    invoke-interface/range {v0 .. v6}, Lcom/squareup/server/payment/PaymentService;->emailReceipt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EmailReceipt;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/queue/EmailReceipt;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentId()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/queue/EmailReceipt;->paymentId:Ljava/lang/String;

    return-object v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/queue/EmailReceipt;->uniqueKey:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 61
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/EmailReceipt;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EmailReceipt;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 2

    .line 145
    invoke-direct {p0}, Lcom/squareup/queue/EmailReceipt;->toBuilder()Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/EmailReceipt;->email:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const-string v1, "REDACTED_email"

    .line 146
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/queue/EmailReceipt$Builder;->email(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lcom/squareup/queue/EmailReceipt$Builder;->build()Lcom/squareup/queue/EmailReceipt;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EmailReceipt{paymentId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/EmailReceipt;->paymentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", billId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/EmailReceipt;->billId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", email=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/EmailReceipt;->email:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", resend=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/squareup/queue/EmailReceipt;->resend:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", uniqueKey=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/EmailReceipt;->uniqueKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
