.class public final Lcom/squareup/receipt/ReceiptValidator_Factory;
.super Ljava/lang/Object;
.source "ReceiptValidator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/receipt/ReceiptValidator;",
        ">;"
    }
.end annotation


# instance fields
.field private final billServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final computationSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedInExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->loggedInExecutorProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->billServiceProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->settingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/receipt/ReceiptValidator_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/StoppableSerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/receipt/ReceiptValidator_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/receipt/ReceiptValidator_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/receipt/ReceiptValidator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lrx/Scheduler;Lrx/Scheduler;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/receipt/ReceiptValidator;
    .locals 7

    .line 57
    new-instance v6, Lcom/squareup/receipt/ReceiptValidator;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/receipt/ReceiptValidator;-><init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lrx/Scheduler;Lrx/Scheduler;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/receipt/ReceiptValidator;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->loggedInExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/StoppableSerialExecutor;

    iget-object v1, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx/Scheduler;

    iget-object v2, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lrx/Scheduler;

    iget-object v3, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->billServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v4, p0, Lcom/squareup/receipt/ReceiptValidator_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/receipt/ReceiptValidator_Factory;->newInstance(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lrx/Scheduler;Lrx/Scheduler;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/receipt/ReceiptValidator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/receipt/ReceiptValidator_Factory;->get()Lcom/squareup/receipt/ReceiptValidator;

    move-result-object v0

    return-object v0
.end method
