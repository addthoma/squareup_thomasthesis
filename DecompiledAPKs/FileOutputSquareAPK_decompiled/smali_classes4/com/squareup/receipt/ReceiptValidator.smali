.class public Lcom/squareup/receipt/ReceiptValidator;
.super Ljava/lang/Object;
.source "ReceiptValidator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;
    }
.end annotation


# static fields
.field static final RECEIPT_MAX_RETRIES:I = 0xc

.field static final RECEIPT_MAX_WAIT_MS:J = 0xbb8L

.field static final RECEIPT_RETRY_MS:J = 0xfaL

.field static final SIGNATURE_MAX_RETRIES:I = 0xa

.field static final SIGNATURE_RETRY_MS:J = 0x3e8L


# instance fields
.field private final billService:Lcom/squareup/server/bills/BillListServiceHelper;

.field private final computationScheduler:Lrx/Scheduler;

.field private final loggedInExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

.field private final mainScheduler:Lrx/Scheduler;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/StoppableSerialExecutor;Lrx/Scheduler;Lrx/Scheduler;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/receipt/ReceiptValidator;->loggedInExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    .line 43
    iput-object p2, p0, Lcom/squareup/receipt/ReceiptValidator;->mainScheduler:Lrx/Scheduler;

    .line 44
    iput-object p3, p0, Lcom/squareup/receipt/ReceiptValidator;->computationScheduler:Lrx/Scheduler;

    .line 45
    iput-object p4, p0, Lcom/squareup/receipt/ReceiptValidator;->billService:Lcom/squareup/server/bills/BillListServiceHelper;

    .line 46
    iput-object p5, p0, Lcom/squareup/receipt/ReceiptValidator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private hasReceiptNumber(Lcom/squareup/payment/PaymentReceipt;)Z
    .locals 2

    .line 89
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->getReceiptNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-nez v0, :cond_1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->isLocalPayment()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method static synthetic lambda$null$3(Ljava/lang/Throwable;)V
    .locals 1

    .line 99
    instance-of v0, p0, Lcom/squareup/receipt/ReceiptNotPrintableException;

    if-eqz v0, :cond_0

    .line 101
    invoke-static {p0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$waitForReceiptNumber$0(Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 0

    .line 65
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;->onReceiptNumberReady(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    return-void
.end method

.method static synthetic lambda$waitForReceiptNumber$2(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/Throwable;)Lcom/squareup/payment/PaymentReceipt;
    .locals 0

    return-object p0
.end method

.method private withSignatureRetry()Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 95
    new-instance v0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$DLanK5W_xgJIAhV7dVJx6_iYW14;

    invoke-direct {v0, p0}, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$DLanK5W_xgJIAhV7dVJx6_iYW14;-><init>(Lcom/squareup/receipt/ReceiptValidator;)V

    return-object v0
.end method


# virtual methods
.method public synthetic lambda$waitForReceiptNumber$1$ReceiptValidator(Lcom/squareup/payment/PaymentReceipt;)Lrx/Observable;
    .locals 1

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/receipt/ReceiptValidator;->hasReceiptNumber(Lcom/squareup/payment/PaymentReceipt;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/squareup/receipt/ReceiptNotPrintableException;

    const-string v0, "Receipt number is missing"

    invoke-direct {p1, v0}, Lcom/squareup/receipt/ReceiptNotPrintableException;-><init>(Ljava/lang/String;)V

    .line 81
    invoke-static {p1}, Lrx/Observable;->error(Ljava/lang/Throwable;)Lrx/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public synthetic lambda$withSignatureRetry$4$ReceiptValidator(Lrx/Observable;)Lrx/Observable;
    .locals 5

    .line 95
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/receipt/ReceiptValidator;->computationScheduler:Lrx/Scheduler;

    const/16 v2, 0xa

    const-wide/16 v3, 0x3e8

    .line 96
    invoke-static {v2, v3, v4, v0, v1}, Lcom/squareup/util/rx/RxRetryStrategies;->noBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/functions/Func1;

    move-result-object v0

    .line 95
    invoke-virtual {p1, v0}, Lrx/Observable;->retryWhen(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$wO-Kzg-Vq6Hn5ZJFtIXeLnu0Yvc;->INSTANCE:Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$wO-Kzg-Vq6Hn5ZJFtIXeLnu0Yvc;

    .line 98
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnError(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/receipt/ReceiptValidator;->mainScheduler:Lrx/Scheduler;

    .line 104
    invoke-virtual {p1, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public waitForReceiptNumber(Lcom/squareup/payment/PaymentReceipt;)Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/payment/PaymentReceipt;",
            ">;"
        }
    .end annotation

    .line 78
    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$qJthkm0YXTxVBiww7_283QVKCrk;

    invoke-direct {v1, p0}, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$qJthkm0YXTxVBiww7_283QVKCrk;-><init>(Lcom/squareup/receipt/ReceiptValidator;)V

    .line 79
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/receipt/ReceiptValidator;->computationScheduler:Lrx/Scheduler;

    const/16 v3, 0xc

    const-wide/16 v4, 0xfa

    .line 82
    invoke-static {v3, v4, v5, v1, v2}, Lcom/squareup/util/rx/RxRetryStrategies;->noBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/functions/Func1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->retryWhen(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/receipt/ReceiptValidator;->mainScheduler:Lrx/Scheduler;

    .line 84
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$YQJXgG55lpHkrvOvlW9glxhigmw;

    invoke-direct {v1, p1}, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$YQJXgG55lpHkrvOvlW9glxhigmw;-><init>(Lcom/squareup/payment/PaymentReceipt;)V

    .line 85
    invoke-virtual {v0, v1}, Lrx/Observable;->onErrorReturn(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public waitForReceiptNumber(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;)V
    .locals 3

    .line 58
    invoke-static {p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/receipt/ReceiptValidator;->hasReceiptNumber(Lcom/squareup/payment/PaymentReceipt;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/squareup/receipt/ReceiptValidator;->loggedInExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    new-instance v2, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;

    invoke-direct {v2, p3, p1, v0, p2}, Lcom/squareup/receipt/-$$Lambda$ReceiptValidator$2SE-3EQBCvGQ_PgdTT_RERR6onM;-><init>(Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    const-wide/16 p1, 0xbb8

    invoke-interface {v1, v2, p1, p2}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 68
    :cond_0
    invoke-interface {p3, p1, v0, p2}, Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;->onReceiptNumberReady(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    :goto_0
    return-void
.end method
