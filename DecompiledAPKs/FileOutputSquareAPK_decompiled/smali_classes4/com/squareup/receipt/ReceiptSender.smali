.class public interface abstract Lcom/squareup/receipt/ReceiptSender;
.super Ljava/lang/Object;
.source "ReceiptSender.java"


# virtual methods
.method public abstract autoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/payment/tender/BaseTender;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract autoPrintValidatedReceipt(Lcom/squareup/payment/PaymentReceipt;)V
.end method

.method public abstract isItemizedReceiptsEnabled()Z
.end method

.method public abstract onMaybeAutoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;)Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onMaybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onMaybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract printValidatedReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
.end method

.method public abstract receiptDeclined(Lcom/squareup/payment/PaymentReceipt;)V
.end method

.method public abstract sendEmailReceiptToDefault(Lcom/squareup/payment/PaymentReceipt;)V
.end method

.method public abstract sendSmsReceiptToDefault(Lcom/squareup/payment/PaymentReceipt;)V
.end method

.method public abstract trySendEmailReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z
.end method

.method public abstract trySendSmsReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z
.end method
