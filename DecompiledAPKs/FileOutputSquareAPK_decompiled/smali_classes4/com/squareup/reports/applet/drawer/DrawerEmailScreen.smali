.class public final Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "DrawerEmailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Component;,
        Lcom/squareup/reports/applet/drawer/DrawerEmailScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final shiftId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 130
    sget-object v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerEmailScreen$K0qccHHts605eFMjDenKbEophTI;->INSTANCE:Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerEmailScreen$K0qccHHts605eFMjDenKbEophTI;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;->shiftId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;)Ljava/lang/String;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;->shiftId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;
    .locals 1

    .line 131
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 132
    new-instance v0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 127
    iget-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerEmailScreen;->shiftId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 136
    sget v0, Lcom/squareup/reports/applet/R$layout;->drawer_email_card_view:I

    return v0
.end method
