.class public Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;
.super Lmortar/ViewPresenter;
.source "CashDrawerDetailsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;",
        ">;"
    }
.end annotation


# instance fields
.field private actualInDrawerEditTextString:Ljava/lang/String;

.field private canViewExpectedInCashDrawer:Z

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private final cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 50
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->dateFormatter:Ljava/text/DateFormat;

    .line 51
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 52
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 53
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->res:Lcom/squareup/util/Res;

    .line 54
    iput-object p6, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 55
    iput-object p7, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 56
    iput-object p8, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    return-void
.end method

.method private formatCommonFields()V
    .locals 2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    .line 94
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getDateString(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowStartOfDrawer(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowStartingCash(Ljava/lang/String;)V

    .line 96
    iget-boolean v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->canViewExpectedInCashDrawer:Z

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowCashSales(Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowCashRefunds(Ljava/lang/String;)V

    .line 99
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowExpectedInDrawer(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->setAndShowPaidInOut(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->hideCashReports()V

    :goto_0
    return-void
.end method

.method private formatForClosedDrawer()V
    .locals 3

    .line 126
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    .line 127
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowDescription(Ljava/lang/String;)V

    .line 128
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getDateString(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowEndOfDrawer(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowActualInDrawer(Ljava/lang/String;)V

    .line 130
    iget-boolean v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->canViewExpectedInCashDrawer:Z

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v2, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 132
    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowDifference(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->setAndShowPaidInOut(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->hideDifferences()V

    .line 136
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->hideCashReports()V

    :goto_0
    return-void
.end method

.method private formatForEndedDrawer()V
    .locals 3

    .line 107
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    .line 108
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowDescription(Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getDateString(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowEndOfDrawer(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    .line 110
    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showActualInDrawerContainer(Z)V

    .line 111
    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->actualInDrawerEditTextString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setActualInDrawerEditText(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getActualInDrawer()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 115
    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showSaveButton(Z)V

    .line 116
    iget-boolean v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->canViewExpectedInCashDrawer:Z

    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getActualInDrawer()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v2, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 118
    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowDifference(Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->hideDifferences()V

    :cond_1
    :goto_0
    return-void
.end method

.method private getDateString(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/CharSequence;
    .locals 3

    .line 141
    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    .line 142
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->date_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->dateFormatter:Ljava/text/DateFormat;

    .line 143
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->timeFormatter:Ljava/text/DateFormat;

    .line 144
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 145
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private setAndShowPaidInOut(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 1

    .line 153
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 154
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowPaidInOut(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public closeEndedDrawer()V
    .locals 3

    .line 192
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getActualInDrawer()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v2, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->closeCashDrawerShift(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    .line 194
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->CLOSED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 195
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v1

    .line 196
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object v1

    .line 197
    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    .line 200
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    const/4 v2, 0x0

    .line 201
    invoke-virtual {v1, v2}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showSaveButton(Z)V

    .line 202
    invoke-virtual {v1, v2}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showActualInDrawerContainer(Z)V

    .line 203
    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowActualInDrawer(Ljava/lang/String;)V

    return-void
.end method

.method public configurePriceLocalHelper(Lcom/squareup/widgets/SelectableEditText;)V
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->removeScrubbingTextWatcher(Lcom/squareup/text/HasSelectableText;)V

    .line 208
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    return-void
.end method

.method public formatForCurrentDrawer()V
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->formatCommonFields()V

    return-void
.end method

.method public formatForEndCurrentDrawer()V
    .locals 3

    .line 74
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->formatCommonFields()V

    .line 75
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    .line 76
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowDescription(Ljava/lang/String;)V

    const/4 v1, 0x1

    .line 77
    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showActualInDrawerContainer(Z)V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getActualInDrawer()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showActualInDrawerHint(Z)V

    return-void
.end method

.method public formatInDrawerHistory()V
    .locals 2

    .line 84
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->formatCommonFields()V

    .line 85
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ENDED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    if-ne v0, v1, :cond_0

    .line 86
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->formatForEndedDrawer()V

    goto :goto_0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->CLOSED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    if-ne v0, v1, :cond_1

    .line 88
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->formatForClosedDrawer()V

    :cond_1
    :goto_0
    return-void
.end method

.method public getActualInDrawer()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 159
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->getActualInDrawerEditTextString()Ljava/lang/String;

    move-result-object v0

    .line 160
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 60
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v0, Lcom/squareup/permissions/Permission;->VIEW_EXPECTED_IN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    invoke-interface {p1, v0}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    .line 62
    invoke-interface {p1}, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;->getHasViewAmountInCashDrawerPermission()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->canViewExpectedInCashDrawer:Z

    return-void
.end method

.method public setShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    return-void
.end method

.method public updateDifference()V
    .locals 5

    .line 165
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    .line 166
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getActualInDrawer()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 167
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->getActualInDrawerEditTextString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->actualInDrawerEditTextString:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_1

    .line 170
    invoke-virtual {v0, v3}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showDifference(Z)V

    .line 172
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->OPEN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    if-ne v1, v4, :cond_0

    .line 173
    invoke-virtual {v0, v2}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showActualInDrawerHint(Z)V

    goto :goto_0

    .line 174
    :cond_0
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    sget-object v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ENDED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    if-ne v1, v2, :cond_4

    .line 175
    invoke-virtual {v0, v3}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showSaveButton(Z)V

    goto :goto_0

    .line 178
    :cond_1
    iget-boolean v4, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->canViewExpectedInCashDrawer:Z

    if-eqz v4, :cond_2

    .line 179
    iget-object v4, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v4, v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v4}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 180
    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->setAndShowDifference(Ljava/lang/String;)V

    .line 183
    :cond_2
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->OPEN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    if-ne v1, v4, :cond_3

    .line 184
    invoke-virtual {v0, v3}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showActualInDrawerHint(Z)V

    goto :goto_0

    .line 185
    :cond_3
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->shift:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    sget-object v3, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ENDED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    if-ne v1, v3, :cond_4

    .line 186
    invoke-virtual {v0, v2}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->showSaveButton(Z)V

    :cond_4
    :goto_0
    return-void
.end method
