.class Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "DrawerHistoryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/drawer/DrawerHistoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final timeFormatter:Ljava/text/DateFormat;

.field unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashmanagement/CashManagementSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->flow:Lflow/Flow;

    .line 63
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 64
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->device:Lcom/squareup/util/Device;

    .line 65
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    .line 66
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 67
    iput-object p6, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 68
    iput-object p7, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    return-void
.end method

.method private hasCursors()Z
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private setCursors()V
    .locals 3

    .line 124
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    if-nez v0, :cond_0

    return-void

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->setCursors(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V

    .line 127
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->hideSpinner()V

    .line 128
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->size()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    invoke-virtual {v2}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->size()I

    move-result v2

    add-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 129
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->showNoHistoryNullState()V

    :cond_1
    return-void
.end method


# virtual methods
.method getDateString(Lcom/squareup/cashmanagement/CashDrawerShiftRow;)Ljava/lang/CharSequence;
    .locals 3

    .line 108
    invoke-interface {p1}, Lcom/squareup/cashmanagement/CashDrawerShiftRow;->getDate()Ljava/util/Date;

    move-result-object p1

    .line 109
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->date_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    .line 110
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 111
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$0$DrawerHistoryScreen$Presenter()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$DrawerHistoryScreen$Presenter(Ljava/util/concurrent/atomic/AtomicInteger;Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V
    .locals 0

    .line 93
    invoke-interface {p2}, Lcom/squareup/cashmanagement/CashDrawerShiftsResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->unclosedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    .line 94
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result p1

    const/4 p2, 0x2

    if-ne p1, p2, :cond_0

    .line 95
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->setCursors()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$2$DrawerHistoryScreen$Presenter(Ljava/util/concurrent/atomic/AtomicInteger;Lcom/squareup/cashmanagement/CashDrawerShiftsResult;)V
    .locals 0

    .line 99
    invoke-interface {p2}, Lcom/squareup/cashmanagement/CashDrawerShiftsResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->closedCursor:Lcom/squareup/cashmanagement/CashDrawerShiftCursor;

    .line 100
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result p1

    const/4 p2, 0x2

    if-ne p1, p2, :cond_0

    .line 101
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->setCursors()V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 72
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->resetConfig()V

    .line 74
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->reports_drawer_history:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 76
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 77
    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryScreen$Presenter$xfX4-00csbBNfJVBlxfmSsCo_hk;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryScreen$Presenter$xfX4-00csbBNfJVBlxfmSsCo_hk;-><init>(Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 79
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/reports/applet/R$string;->reports_drawer_history:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideUpButton()V

    .line 83
    :goto_0
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {p1}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result p1

    if-nez p1, :cond_1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->showCashManagementDisabledState()V

    return-void

    .line 88
    :cond_1
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->hasCursors()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 89
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->setCursors()V

    goto :goto_1

    .line 91
    :cond_2
    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 92
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ENDED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    new-instance v2, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryScreen$Presenter$E6h1cSxMGYCj2IVUSbd7AuAZDiE;

    invoke-direct {v2, p0, p1}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryScreen$Presenter$E6h1cSxMGYCj2IVUSbd7AuAZDiE;-><init>(Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->CLOSED:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    new-instance v2, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryScreen$Presenter$8mNJV_e4zFvLnub6QRmXfznSiYs;

    invoke-direct {v2, p0, p1}, Lcom/squareup/reports/applet/drawer/-$$Lambda$DrawerHistoryScreen$Presenter$8mNJV_e4zFvLnub6QRmXfznSiYs;-><init>(Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Lcom/squareup/cashmanagement/CashDrawerShiftsCallback;)V

    :goto_1
    return-void
.end method

.method onProgressHidden()V
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->showList()V

    return-void
.end method

.method onRowClicked(Ljava/lang/String;)V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;

    invoke-direct {v1, p1}, Lcom/squareup/reports/applet/drawer/DrawerReportScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
