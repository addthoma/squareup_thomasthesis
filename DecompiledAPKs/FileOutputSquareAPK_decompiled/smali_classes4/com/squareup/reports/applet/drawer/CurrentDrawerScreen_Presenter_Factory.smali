.class public final Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "CurrentDrawerScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cashDrawerShiftManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementExtraPermissionsHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final detailsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final fileSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final reportsAppletSectionsListPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 80
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 81
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 82
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 83
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->userTokenProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 84
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->fileSchedulerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 85
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 86
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 87
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 88
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->reportsAppletSectionsListPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 89
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->detailsPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 90
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->cashManagementSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 91
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->serviceProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 92
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 93
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 94
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 95
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->cashManagementExtraPermissionsHolderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 96
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 119
    new-instance v18, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Ljava/lang/String;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/server/cashmanagement/CashManagementService;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/text/Formatter;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Ljava/lang/String;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ")",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 131
    new-instance v18, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Ljava/lang/String;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/server/cashmanagement/CashManagementService;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/text/Formatter;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/permissions/EmployeeManagement;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;
    .locals 19

    move-object/from16 v0, p0

    .line 101
    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->userTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->fileSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->reportsAppletSectionsListPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->detailsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->cashManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/cashmanagement/CashManagementSettings;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/server/cashmanagement/CashManagementService;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->cashManagementExtraPermissionsHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    iget-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/permissions/EmployeeManagement;

    invoke-static/range {v2 .. v18}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Ljava/lang/String;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/server/cashmanagement/CashManagementService;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/text/Formatter;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen_Presenter_Factory;->get()Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
