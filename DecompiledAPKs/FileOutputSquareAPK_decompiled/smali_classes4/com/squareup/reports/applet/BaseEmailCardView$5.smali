.class Lcom/squareup/reports/applet/BaseEmailCardView$5;
.super Lcom/squareup/ui/EmptyAnimationListener;
.source "BaseEmailCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/BaseEmailCardView;->showEmailMessage(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/BaseEmailCardView;

.field final synthetic val$showClose:Z


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/BaseEmailCardView;Z)V
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView$5;->this$0:Lcom/squareup/reports/applet/BaseEmailCardView;

    iput-boolean p2, p0, Lcom/squareup/reports/applet/BaseEmailCardView$5;->val$showClose:Z

    invoke-direct {p0}, Lcom/squareup/ui/EmptyAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .line 131
    iget-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView$5;->this$0:Lcom/squareup/reports/applet/BaseEmailCardView;

    invoke-static {p1}, Lcom/squareup/reports/applet/BaseEmailCardView;->access$100(Lcom/squareup/reports/applet/BaseEmailCardView;)Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    move-result-object p1

    iget-boolean v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView$5;->val$showClose:Z

    invoke-virtual {p1, v0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->onEmailAnimationComplete(Z)V

    return-void
.end method
