.class public Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;
.super Lcom/squareup/reports/applet/sales/v1/InReportConfigScope;
.source "ReportConfigEmployeeFilterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;

    .line 53
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/InReportConfigScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 49
    const-class v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;

    invoke-interface {p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;->reportConfigEmployeeFilterCoordinator()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 36
    const-class v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;

    .line 37
    invoke-interface {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;->reportConfigEmployeeFilterRunner()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;

    move-result-object v0

    .line 36
    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 32
    sget v0, Lcom/squareup/reports/applet/R$layout;->report_config_employee_filter_card_view:I

    return v0
.end method
