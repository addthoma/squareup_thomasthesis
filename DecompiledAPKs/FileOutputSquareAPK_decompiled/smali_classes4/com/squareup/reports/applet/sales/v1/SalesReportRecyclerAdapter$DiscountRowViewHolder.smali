.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;
.super Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiscountRowViewHolder"
.end annotation


# instance fields
.field private final nameView:Landroid/widget/TextView;

.field private final priceView:Landroid/widget/TextView;

.field private final quantityView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

.field private final threeColumns:Z


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 551
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 552
    invoke-direct {p0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 553
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->itemView:Landroid/view/View;

    sget p2, Lcom/squareup/reports/applet/R$id;->sales_discount_name:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->nameView:Landroid/widget/TextView;

    .line 554
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->itemView:Landroid/view/View;

    sget p2, Lcom/squareup/reports/applet/R$id;->sales_discount_quantity:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->quantityView:Landroid/widget/TextView;

    .line 555
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->itemView:Landroid/view/View;

    sget p2, Lcom/squareup/reports/applet/R$id;->sales_discount_price:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->priceView:Landroid/widget/TextView;

    .line 556
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->itemView:Landroid/view/View;

    instance-of p1, p1, Lcom/squareup/widgets/HorizontalThreeChildLayout;

    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->threeColumns:Z

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 5

    .line 560
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 561
    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->getPosition()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$1000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getDiscountRow(I)Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;

    move-result-object v0

    .line 562
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->nameView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->discountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 563
    iget-boolean v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->threeColumns:Z

    if-eqz v1, :cond_0

    .line 564
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->quantityView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->formattedQuantity:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 566
    :cond_0
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->quantityView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 567
    invoke-static {v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$1100(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/util/Res;

    move-result-object v2

    sget v3, Lcom/squareup/checkout/R$string;->configure_item_summary_quantity_only:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->formattedQuantity:Ljava/lang/String;

    const-string v4, "quantity"

    .line 568
    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 569
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 566
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 571
    :goto_0
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;->priceView:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/DiscountReportRow;->formattedDiscountMoney:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
