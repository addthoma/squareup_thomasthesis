.class final Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;
.super Ljava/lang/Object;
.source "ReportConfigScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MonthAndYear"
.end annotation


# instance fields
.field public final month:I

.field public final year:I


# direct methods
.method constructor <init>(II)V
    .locals 0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->month:I

    .line 156
    iput p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->year:I

    return-void
.end method

.method public static fromCalendar(Ljava/util/Calendar;)Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;
    .locals 2

    const/4 v0, 0x1

    .line 160
    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    .line 161
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result p0

    .line 162
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;

    invoke-direct {v1, p0, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;-><init>(II)V

    return-object v1
.end method


# virtual methods
.method public asCalendarDate()Ljava/util/Calendar;
    .locals 8

    .line 166
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 167
    iget v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->year:I

    iget v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter$MonthAndYear;->month:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    return-object v7
.end method
