.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$PaymentsRowViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;,
        Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CustomizeViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final CATEGORY_SALES_CATEGORY_TYPE:I = 0x7

.field private static final CATEGORY_SALES_HEADER_TYPE:I = 0x6

.field private static final CATEGORY_SALES_ITEM_TYPE:I = 0x8

.field private static final CATEGORY_SALES_ITEM_VARIATION_TYPE:I = 0x9

.field private static final CHART_TYPE:I = 0xb

.field private static final CHART_VALUE_TEXT_SIZE_DP:F = 16.0f

.field private static final CUSTOMIZE_DETAILS_TYPE:I = 0xa

.field private static final DISCOUNT_HEADER_TYPE:I = 0x4

.field private static final DISCOUNT_ROW_TYPE:I = 0x5

.field private static final DIVIDER_LARGE_TOP_TYPE:I = 0xd

.field private static final DIVIDER_TYPE:I = 0xc

.field private static final OVERVIEW_VALUES_VIEW_TYPE:I = 0x1

.field private static final PAYMENTS_HEADER_VIEW_TYPE:I = 0xe

.field private static final PAYMENTS_ROW_TYPE:I = 0xf

.field private static final SALES_SUMMARY_HEADER_VIEW_TYPE:I = 0x2

.field private static final SALES_SUMMARY_ROW_TYPE:I = 0x3


# instance fields
.field private barDisplayData:Lcom/github/mikephil/charting/data/BarData;

.field private final chartDateFormatter:Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

.field private discountEndPositionExclusive:I

.field private discountHeaderRowPosition:I

.field private discountStartPositionInclusive:I

.field private groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field private itemCategoryHeaderRowPosition:I

.field private itemCategoryStartPositionInclusive:I

.field private final marinBlue:I

.field private final marinDarkGray:I

.field private final marinMediumGray:I

.field private final marinUltraLightGray:I

.field private final numberFormat:Ljava/text/NumberFormat;

.field private paymentsEndPositionExclusive:I

.field private paymentsHeaderRowPosition:I

.field private paymentsStartPositionInclusive:I

.field private perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

.field private final res:Lcom/squareup/util/Res;

.field private salesEndPositionExclusive:I

.field private salesStartPositionInclusive:I

.field private final showDividers:Z

.field private final staticRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;Lcom/squareup/util/Res;ZLcom/squareup/quantity/PerUnitFormatter;)V
    .locals 1

    .line 92
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    .line 93
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    .line 94
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->chartDateFormatter:Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    .line 95
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->res:Lcom/squareup/util/Res;

    .line 96
    iput-boolean p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->showDividers:Z

    .line 97
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 98
    sget p1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->marinBlue:I

    .line 99
    sget p1, Lcom/squareup/marin/R$color;->marin_ultra_light_gray:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->marinUltraLightGray:I

    .line 100
    sget p1, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->marinMediumGray:I

    .line 101
    sget p1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->marinDarkGray:I

    .line 102
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-static {p1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->numberFormat:Ljava/text/NumberFormat;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->marinUltraLightGray:I

    return p0
.end method

.method static synthetic access$1000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountStartPositionInclusive:I

    return p0
.end method

.method static synthetic access$1100(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/util/Res;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryStartPositionInclusive:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->marinMediumGray:I

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->chartDateFormatter:Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Ljava/text/NumberFormat;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->numberFormat:Ljava/text/NumberFormat;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/github/mikephil/charting/data/BarData;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->barDisplayData:Lcom/github/mikephil/charting/data/BarData;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Ljava/util/List;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I
    .locals 0

    .line 45
    iget p0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsStartPositionInclusive:I

    return p0
.end method

.method private updatePositionBoundaries()V
    .locals 3

    .line 232
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->salesStartPositionInclusive:I

    .line 233
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->salesStartPositionInclusive:I

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    .line 234
    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getSalesSummaryRowCount()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->salesEndPositionExclusive:I

    .line 235
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->salesEndPositionExclusive:I

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsHeaderRowPosition:I

    .line 236
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsHeaderRowPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsStartPositionInclusive:I

    .line 237
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsStartPositionInclusive:I

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    .line 238
    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getPaymentsRowCount()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsEndPositionExclusive:I

    .line 240
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getDiscountsRowCount()I

    move-result v0

    const/4 v1, -0x1

    if-lez v0, :cond_1

    .line 241
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->showDividers:Z

    if-eqz v0, :cond_0

    .line 242
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsEndPositionExclusive:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountHeaderRowPosition:I

    goto :goto_0

    .line 244
    :cond_0
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsEndPositionExclusive:I

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountHeaderRowPosition:I

    .line 246
    :goto_0
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountHeaderRowPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountStartPositionInclusive:I

    .line 247
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountStartPositionInclusive:I

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    .line 248
    invoke-virtual {v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getDiscountsRowCount()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountEndPositionExclusive:I

    .line 249
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountEndPositionExclusive:I

    goto :goto_1

    .line 251
    :cond_1
    iput v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountHeaderRowPosition:I

    .line 252
    iput v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountStartPositionInclusive:I

    .line 253
    iput v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountEndPositionExclusive:I

    .line 254
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsEndPositionExclusive:I

    .line 258
    :goto_1
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getCategorySalesRowCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 259
    iget-boolean v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->showDividers:Z

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    .line 260
    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryHeaderRowPosition:I

    goto :goto_2

    .line 262
    :cond_2
    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryHeaderRowPosition:I

    .line 264
    :goto_2
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryHeaderRowPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryStartPositionInclusive:I

    goto :goto_3

    .line 266
    :cond_3
    iput v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryHeaderRowPosition:I

    .line 267
    iput v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryStartPositionInclusive:I

    :goto_3
    return-void
.end method

.method private updateStaticRows()V
    .locals 12

    .line 181
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 182
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getChartData()Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 188
    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;->chartEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    .line 189
    iget-wide v5, v3, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->transactionCount:J

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-lez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_3

    .line 196
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 197
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 199
    iget-object v5, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;->chartEntries:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    .line 200
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    .line 201
    iget-object v8, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->chartDateFormatter:Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    iget-boolean v9, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;->overlap:Z

    invoke-virtual {v8, v6, v1, v9}, Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;->format(Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;ZZ)Ljava/lang/String;

    move-result-object v8

    .line 202
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v8, v6, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->grossSalesMoney:Lcom/squareup/protos/common/Money;

    .line 205
    new-instance v9, Lcom/github/mikephil/charting/data/BarEntry;

    iget-object v8, v8, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-float v8, v10

    invoke-direct {v9, v8, v7, v6}, Lcom/github/mikephil/charting/data/BarEntry;-><init>(FILjava/lang/Object;)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 207
    :cond_2
    new-instance v1, Lcom/github/mikephil/charting/data/BarDataSet;

    const-string v5, ""

    invoke-direct {v1, v3, v5}, Lcom/github/mikephil/charting/data/BarDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 208
    iget v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->marinBlue:I

    invoke-virtual {v1, v3}, Lcom/github/mikephil/charting/data/BarDataSet;->setColor(I)V

    .line 209
    iget v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->marinDarkGray:I

    invoke-virtual {v1, v3}, Lcom/github/mikephil/charting/data/BarDataSet;->setHighLightColor(I)V

    const/16 v3, 0xff

    .line 210
    invoke-virtual {v1, v3}, Lcom/github/mikephil/charting/data/BarDataSet;->setHighLightAlpha(I)V

    const/high16 v3, 0x41800000    # 16.0f

    .line 211
    invoke-virtual {v1, v3}, Lcom/github/mikephil/charting/data/BarDataSet;->setValueTextSize(F)V

    .line 213
    new-instance v3, Lcom/github/mikephil/charting/data/BarData;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v3, v2, v1}, Lcom/github/mikephil/charting/data/BarData;-><init>(Ljava/util/List;Ljava/util/List;)V

    iput-object v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->barDisplayData:Lcom/github/mikephil/charting/data/BarData;

    .line 214
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->barDisplayData:Lcom/github/mikephil/charting/data/BarData;

    invoke-virtual {v1, v4}, Lcom/github/mikephil/charting/data/BarData;->setDrawValues(Z)V

    .line 216
    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;->chartEntries:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;

    .line 217
    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 219
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_3
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->showDividers:Z

    if-eqz v0, :cond_4

    .line 224
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_4
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method categoryRangeInserted(II)V
    .locals 1

    .line 331
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryStartPositionInclusive:I

    add-int/2addr v0, p1

    if-lez p2, :cond_0

    .line 334
    invoke-virtual {p0, v0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->notifyItemRangeInserted(II)V

    :cond_0
    return-void
.end method

.method categoryRangeRemoved(II)V
    .locals 1

    .line 323
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryStartPositionInclusive:I

    add-int/2addr v0, p1

    if-lez p2, :cond_0

    .line 326
    invoke-virtual {p0, v0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->notifyItemRangeRemoved(II)V

    :cond_0
    return-void
.end method

.method public getItemCount()I
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getSalesSummaryRowCount()I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getPaymentsRowCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 155
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getDiscountsRowCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 157
    iget-boolean v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->showDividers:Z

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getCategorySalesRowCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 165
    iget-boolean v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->showDividers:Z

    if-eqz v2, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    return v0
.end method

.method public getItemViewType(I)I
    .locals 4

    .line 272
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->staticRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    .line 276
    :cond_0
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->salesStartPositionInclusive:I

    const/4 v1, 0x3

    if-lt p1, v0, :cond_1

    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->salesEndPositionExclusive:I

    if-ge p1, v0, :cond_1

    return v1

    .line 280
    :cond_1
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsHeaderRowPosition:I

    if-ne p1, v0, :cond_2

    const/16 p1, 0xe

    return p1

    .line 284
    :cond_2
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsStartPositionInclusive:I

    if-lt p1, v0, :cond_3

    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->paymentsEndPositionExclusive:I

    if-ge p1, v0, :cond_3

    const/16 p1, 0xf

    return p1

    .line 288
    :cond_3
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->showDividers:Z

    const/16 v2, 0xc

    const/4 v3, 0x1

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountHeaderRowPosition:I

    sub-int/2addr v0, v3

    if-ne p1, v0, :cond_4

    return v2

    .line 292
    :cond_4
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountHeaderRowPosition:I

    if-ne p1, v0, :cond_5

    const/4 p1, 0x4

    return p1

    .line 296
    :cond_5
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountStartPositionInclusive:I

    if-lt p1, v0, :cond_6

    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->discountEndPositionExclusive:I

    if-ge p1, v0, :cond_6

    const/4 p1, 0x5

    return p1

    .line 300
    :cond_6
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->showDividers:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryHeaderRowPosition:I

    sub-int/2addr v0, v3

    if-ne p1, v0, :cond_7

    return v2

    .line 304
    :cond_7
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryHeaderRowPosition:I

    if-ne p1, v0, :cond_8

    const/4 p1, 0x6

    return p1

    .line 307
    :cond_8
    iget v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->itemCategoryStartPositionInclusive:I

    if-lt p1, v0, :cond_b

    sub-int v0, p1, v0

    .line 309
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v2, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getCategorySalesRow(I)Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    move-result-object v0

    .line 310
    sget-object v2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$CategorySalesRow$CategorySalesRowType:[I

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->rowType:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    if-eq v0, v3, :cond_a

    const/4 v2, 0x2

    if-eq v0, v2, :cond_9

    if-ne v0, v1, :cond_b

    const/16 p1, 0x9

    return p1

    :cond_9
    const/16 p1, 0x8

    return p1

    :cond_a
    const/4 p1, 0x7

    return p1

    .line 319
    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 45
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->onBindViewHolder(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;I)V
    .locals 0

    .line 146
    invoke-virtual {p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;->bind()V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 45
    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;
    .locals 1

    packed-switch p2, :pswitch_data_0

    .line 141
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Unsupported"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 122
    :pswitch_0
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$PaymentsRowViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_sales_summary_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$PaymentsRowViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 117
    :pswitch_1
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_sales_summary_payments_table_header:I

    .line 118
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 139
    :pswitch_2
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_divider_large_top:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 137
    :pswitch_3
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_divider:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 112
    :pswitch_4
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_chart:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ChartViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 108
    :pswitch_5
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CustomizeViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_customize_details:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CustomizeViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 134
    :pswitch_6
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_item_variation_row:I

    .line 135
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 132
    :pswitch_7
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_item_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 130
    :pswitch_8
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_category_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesRowViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 128
    :pswitch_9
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_category_sales_header:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 124
    :pswitch_a
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_discounts_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$DiscountRowViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 126
    :pswitch_b
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_discounts_header:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 120
    :pswitch_c
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_sales_summary_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$SalesRowViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    .line 114
    :pswitch_d
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_sales_summary_sales_table_header:I

    .line 115
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 110
    :pswitch_e
    new-instance p2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;

    sget v0, Lcom/squareup/reports/applet/R$layout;->sales_report_overview:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V

    return-object p2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method setQuantityViewText(Landroid/widget/TextView;Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;Z)V
    .locals 2

    .line 340
    iget-object v0, p2, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->quantity:Ljava/math/BigDecimal;

    if-nez v0, :cond_0

    const/4 p2, 0x0

    .line 341
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p2, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->quantity:Ljava/math/BigDecimal;

    .line 346
    invoke-virtual {v0, v1}, Lcom/squareup/quantity/PerUnitFormatter;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    iget-object p2, p2, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMeasurementUnit:Ljava/lang/String;

    .line 347
    invoke-virtual {v0, p2}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    if-nez p3, :cond_1

    .line 350
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {p2}, Lcom/squareup/quantity/PerUnitFormatter;->asSuffix()Lcom/squareup/quantity/PerUnitFormatter;

    .line 353
    :cond_1
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {p2}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public update()V
    .locals 0

    .line 175
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->updateStaticRows()V

    .line 176
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->updatePositionBoundaries()V

    .line 177
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method
